<div style="border: 1px solid #DDD; padding: 5px;">
<table class="table table-bordered" style="width: 1000px;">
    <!-- <tr>
      <td colspan="6" style="text-align: center;"><b>BUKTI PEMBAYARAN TAGIHAN TELKOM</b></td>
    </tr> -->
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="width: 150px;">TGL PEMBELIAN</td>
      <td style="width: 10px;">:</td>
      <td><?php echo e($tanggal); ?></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>TIPE PEMBAYARAN</td>
      <td>:</td>
      <td>PULSA PRABAYAR</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>BUKTI PEMBELIAN PULSA PRABAYAR</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NAMA PRODUK</td>
      <td>:</td>
      <td><?php echo e($product_nama); ?></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NO HP</td>
      <td>:</td>
      <td><?php echo e($no_hp); ?></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>SERIAL NUMBER</td>
      <td>:</td>
      <td><?php echo e($sn); ?></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>TOTAL BAYAR</td>
      <td>:</td>
      <td><?php echo e($product_amount_rp); ?></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>MOHON STRUK INI DISIMPAN UNTUK BUKTI PEMBELIAN YANG SAH.</b></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center; font-size: 19px;">Terima Kasih</td>
    </tr>
    <!-- <tr>
      <td colspan="6" style="text-align: center;"><?php echo e($info); ?></td>
    </tr> -->
</table>
</div>