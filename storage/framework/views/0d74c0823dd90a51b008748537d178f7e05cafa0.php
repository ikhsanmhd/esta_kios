<div style="border: 1px solid #DDD; padding: 5px;">
<table class="table table-bordered" style="width: 1000px;">
    <tr>
      <td colspan="6" style="text-align: left;"><b>BANK MANDIRI</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="width: 140px;">TGL</td>
      <td style="width: 10px;">:</td>
      <td><?php echo e($pln_pra_tgl_lunas); ?></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>TIPE PEMBAYARAN</td>
      <td>:</td>
      <td>PLN PRABAYAR</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>STRUK PEMBELIAN LISTRIK PRABAYAR</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NO METER</td>
      <td>:</td>
      <td><?php echo e($pln_pra_meter_id); ?></td>
      <td>METERAI</td>
      <td>:</td>
      <td><?php echo e($pln_pra_biaya_materai); ?></td>
    </tr>
    <tr>
      <td>IDPEL</td>
      <td>:</td>
      <td><?php echo e($pln_pra_id_pel); ?></td>
      <td>PPN</td>
      <td>:</td>
      <td><?php echo e($pln_pra_ppn); ?></td>
    </tr>
    <tr>
      <td>NAMA</td>
      <td>:</td>
      <td><?php echo e($pln_pra_nama); ?></td>
      <td>PPJ</td>
      <td>:</td>
      <td><?php echo e($pln_pra_ppju); ?></td>
    </tr>
    <tr>
      <td>TARIF/DAYA</td>
      <td>:</td>
      <td><?php echo e($pln_pra_tarif.'/'.$pln_pra_kategori_daya.'VA'); ?></td>
      <td>ANGSURAN</td>
      <td>:</td>
      <td><?php echo e($pln_pra_angsuran); ?></td>
    </tr>
    <tr>
      <td>NO REF</td>
      <td>:</td>
      <td><?php echo e($pln_pra_ref_no); ?></td>
      <td>RP STROOM/TOKEN</td>
      <td>:</td>
      <td><?php echo e($pln_pra_rp_stroom); ?></td>
    </tr>
    <tr>
      <td>RP BAYAR</td>
      <td>:</td>
      <td><?php echo e($rp_bayar); ?></td>
      <td style="width: 150px;">JML KWH</td>
      <td>:</td>
      <td><?php echo e($pln_pra_jml_kwh); ?></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td>ADMIN BANK</td>
      <td style="width: 10px;">:</td>
      <td><?php echo e($pln_pra_biaya_admin); ?></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6"><b>STROOM/TOKEN: <?php echo e($pln_pra_token_number); ?></b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><?php echo e($info); ?></td>
    </tr>
</table>
</div>