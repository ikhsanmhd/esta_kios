let boxKTP      = '#tableKTP';
let boxSelfie   = '#tableSelfie';
let boxRekening = '#tableRekening';
let urlPath 		= 'http://'+window.location.host+window.location.pathname+'/';

/**
 * hide remark if approved
 */
$('select[data-type="approved"]').on('change',function(){
	let target = $(this).data('target');
	let value = $(this).val();

	if (value == 'Approved') {
		hideTarget(target);
	}else{
		showTarget(target);
	}
})

/**
 * Validation submit
 */
$('#modalSave').click(function(){
	// checkForm();
})
$('#modalSubmit').click(function(){
	// checkForm();
})

/**
 * open searching for indonesia search
 */
$('.btn-search-indoensia').on('click',function(){
	if (!$('#searchIndonesia').is(":visible")) {
		$('#searchIndonesia').show(500);
	}
})

/**
 * make class select2 full 100% in modal
 */
$('.modal select').css('width', '100%');

/**
 * select2 ajax from table kelurahan
 */
$('#kelSearch').select2({
  ajax: {
    url: urlPath+"kelurahan",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        q: params.term
      }; 
    },
    processResults: function (data, params) {
      return {
        results: data.items
      };
    },
    cache: true
  },
  placeholder: 'Search for a repository',
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 3,
  templateResult: formatRepo,
	templateSelection: formatRepoSelection 
});

/**
 * if select 2 has been selected
 * show kecamatan kabupaten provinsi
 */
$('#kelSearch').on('change',function(){
	let id = $(this).val();
	var request = $.ajax({
	  url: urlPath+"indonesia",
	  type: "GET",
	  data: {id : id}
	});
	request.done(function(msg) {
		let kelName  = msg.kelurahan.name;
		let kel      = msg.kelurahan.id;
		let kecName  = msg.kecamatan.name;
		let kec      = msg.kecamatan.id;
		let kabName  = msg.kabupaten.name;
		let kab      = msg.kabupaten.id;
		let provName = msg.provinsi.name;
		let prov     = msg.provinsi.id;

		$('#kelName').val(kelName);
		$('#kel').val(kel); 
		$('#kecName').val(kecName);
		$('#kec').val(kec);
		$('#kabName').val(kabName);
		$('#kab').val(kab);
		$('#provName').val(provName);
		$('#prov').val(prov);

		$('#searchIndonesia').hide(500);
	});
	request.fail(function(jqXHR, textStatus) {
	  alert( "Request failed: " + textStatus ); 
	});
})

/**
 * class date picker has open show bootstrap single daterange picker
 */
/*$(document).ready(function(){
	$(".datepicker").datepicker({
    locale: {
        format: "YYYY-MM-DD"
    }
  });
})*/

/**
 * Lock Keyboard just jumber
 */
$("#rt").keydown(function (e) {
  // Allow: backspace, delete, tab, escape, enter and .
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
     // Allow: Ctrl/cmd+A
    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
     // Allow: Ctrl/cmd+C
    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
     // Allow: Ctrl/cmd+X
    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
     // Allow: home, end, left, right
    (e.keyCode >= 35 && e.keyCode <= 39)) {
         // let it happen, don't do anything
         return;
  }
  // Ensure that it is a number and stop the keypress
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
});
$("#rw").keydown(function (e) {
  // Allow: backspace, delete, tab, escape, enter and .
  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
     // Allow: Ctrl/cmd+A
    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
     // Allow: Ctrl/cmd+C
    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
     // Allow: Ctrl/cmd+X
    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
     // Allow: home, end, left, right
    (e.keyCode >= 35 && e.keyCode <= 39)) {
         // let it happen, don't do anything
         return;
  }
  // Ensure that it is a number and stop the keypress
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
});

/**
 * load data user selected
 */
$('.btn-info').on('click',function(){
	let id = $(this).attr('href');
	id     = id.replace('#','');

	emptyModal();

	$('#modalVerifikasi').modal('show');
	$('#content').hide();
	$('#loading').show();
	$('#idAgen').val(id);

	/**
	 * AJAX GET DATA
	 */
	var request = $.ajax({
		url: urlPath+"load-data",
		type: "GET",	
		data: {
			id : id
		}
	});
	request.done(function(msg) {
		$('#loading').hide();
		$('#content').show( "slow" );
		let ktp      = msg.table.ktp;
		let selfie   = msg.table.selfie;
		let rekening = msg.table.rekening;
		let agen     = msg.agen;

		changeRow(boxKTP,ktp);
		changeRow(boxSelfie,selfie);
		changeRow(boxRekening,rekening);
		changeContent(agen);
	});
	request.fail(function(jqXHR, textStatus) {
		console.log( "Request failed: " + textStatus );
	});
})

function emptyModal(){
	changeRow(boxKTP,null)
	changeRow(boxSelfie,null)
	changeRow(boxRekening,null)
	changeContent(null);
}

function changeRow(box,data){
	let image         = (data === null ? 'javascript:void(0)' : data.image);
	let lastSubmitted = (data === null ? '' : data.last_submitted);
	let lastApproval  = (data === null ? '' : data.last_approved);
	let status        = (data === null ? 'Submitted' : data.status);
	let remark        = (data === null ? '' : data.remark);

	if (status == 'Approved') { //hide if status approved
		let target = $(box).find('.statusDocument').find('select').data("target");
		hideTarget(target);
	}

	$(box).find('.image').find('a').attr('href',image);
	$(box).find('.lastSubmitted').text(lastSubmitted);
	$(box).find('.lastApproval').text(lastApproval);
	$(box).find('.statusDocument').find('select').val(status);
	$(box).find('.remark').find('select').val(remark);
}

function changeContent(data){
	if (data === null) {
		$('#content').find('input').val("");
		$('#content').find('textarea').val("");
		$('#content').find('select').val("");
	}else{
		$.each(data, function(index,data){
			setValue(index,data);
		});
	}
}

function formatRepo(repo) {
  if (repo.loading) {
    return repo.text;
  }

  var markup = 
	  "<div class='select2-result-repository clearfix'>" +
	    "<div class='select2-result-repository__meta'>" +
	      "<div class='select2-result-repository__title'>" + repo.name + "</div>" +
	     "</div>" +
	  "</div>";

	  return markup;
}

function formatRepoSelection (repo) {
  return repo.full_name;
}

function setValue(element,value){
	$('#'+element).val(value);
}

function checkForm(){
	let tabIdentitas = checkTabIdentitas();
	let tabRekening = checkTabRekening();

	if (!tabIdentitas) {
		$('a[href="#tabIdentitas"]').click();
	}else if (!tabRekening) {
		$('a[href="#tabRekening"]').click();
	}
}

function checkTabIdentitas(){
	let result = true;
	$('#tabIdentitas').find('input').each(function(){
		let id = $(this).attr('id');
		let value = $(this).val();
		if (id !== 'kelSearch') {
			if (value == '') {
				result = false;
			}
			console.log(id,value,result)
		}
	})

	$('#tabIdentitas').find('select').each(function(){
		let id = $(this).attr('id');
		let value = $(this).val();
		if (id !== 'kelSearch') {
			if (value == '') {
				result = false;
			}
			console.log(id,value,result)
		}
	})

	$('#tabIdentitas').find('textarea').each(function(){
		let id = $(this).attr('id');
		let value = $(this).val();
		if (id !== 'kelSearch') {
			if (value == '') {
				result = false;
			}
			console.log(id,value,result)
		}
	})

	return result;
}

function checkTabRekening(){
	let result = true;
	$('#tabRekening').find('input').each(function(){
		let value = $(this).val();
		if (value == '') {
			result = false;
		}
	})

	$('#tabIdentitas').find('select').each(function(){
		let value = $(this).val();
		if (value == '') {
			result = false;
		}
	})

	$('#tabIdentitas').find('textarea').each(function(){
		let value = $(this).val();
		if (value == '') {
			result = false;
		}
	})

	return result;
}

function hideTarget(target){
	$(target).hide(500);
}

function showTarget(target){
	$(target).show(500);
}