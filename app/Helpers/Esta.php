<?php  
namespace App\Helpers;
use Session;
use Request;
use DB;
use CRUDBooster;
/**
* 
*/
class Esta
{	
	public static function kirimemail($config=[]) {   

	    \Config::set('mail.driver',CRUDBooster::getSetting('smtp_driver'));
	    \Config::set('mail.host',CRUDBooster::getSetting('smtp_host'));
	    \Config::set('mail.port',CRUDBooster::getSetting('smtp_port'));
	    \Config::set('mail.username',CRUDBooster::getSetting('smtp_username'));
	    \Config::set('mail.password',CRUDBooster::getSetting('smtp_password'));
	    \Config::set('mail.encryption', 'tls');

	    /*\Config::set('mail.driver', 'smtp');
	    \Config::set('mail.host', 'mail.ayokenalin.co.id');
	    \Config::set('mail.port', 587);
	    \Config::set('mail.username','notifikasi@ayokenalin.co.id');
	    \Config::set('mail.password', 'digitalesta#2021');
	    \Config::set('mail.encryption', 'tls');*/

	    $to = $config['to'];
	    $data = $config['data'];
	    $template = $config['template'];
	    //return $data;

	    $template = CRUDBooster::first('cms_email_templates',['slug'=>$template]);
	    $html = $template->content;
		foreach($data as $key=>$val) {
			$html = str_replace('['.$key.']',$val,$html);
			$template->subject = str_replace('['.$key.']', $val, $template->subject);
		}
		$subject = $template->subject;
	    $attachments = ($config['attachments'])?:array();

	    if($config['send_at']!=NULL) {
			$a                      = array();
			$a['send_at']           = $config['send_at'];
			$a['email_recipient']   = $to;
			$a['email_from_email']  = $template->from_email?:CRUDBooster::getSetting('email_sender');
			$a['email_from_name']   = $template->from_name?:CRUDBooster::getSetting('appname');
			$a['email_cc_email']    = $template->cc_email;				
			$a['email_subject']     = $subject;
			$a['email_content']     = $html;
			$a['email_attachments'] = serialize($attachments);
			$a['is_sent']           = 0;
	        DB::table('cms_email_queues')->insert($a);
	        return true;
	    }

	    \Mail::send("crudbooster::emails.blank",['content'=>$html],function($message) use ($to,$subject,$template,$attachments) {
	    	$message->priority(3);
			$email_all = explode(';',$to);
	        $message->to($email_all);

	        if($template->from_email) {
	        	$from_name = ($template->from_name)?:CRUDBooster::getSetting('appname');
	        	$message->from($template->from_email,$from_name);
	        }

	        if($template->cc_email) {
				$cc_email_split = explode(';',$template->cc_email);
	        	$message->cc($cc_email_split);
	        }

	        if(count($attachments)) {
		    	foreach($attachments as $attachment) {
		    		$message->attach($attachment);
		    	}   		        		        
	        }

	        $message->subject($subject);
	    });
	}

	public static function daterange( $first, $last, $step = '+1 day', $format = 'Y/m/d' ) {

		$dates = array();
		$current = strtotime( $first );
		$last = strtotime( $last );

		while( $current <= $last ) {

			$dates[] = date( $format, $current );
			$current = strtotime( $step, $current );
		}

		return $dates;
	}

	public static function user($id) {
		$id_user = ($id >= 1 ? $id : CRUDBooster::myId());
		$array = DB::table(($id >= 1 ? 'agen' : 'cms_users'))->where('id',$id_user)->first();
		return ($id >= 1 ? $array->nama : $array->name);
	}

	public static function check_token($token,$token_time) {
		$key = 'estakios2018';
		$token_create = md5($key.$token_time);
		if($token == $token_create) {
			$check = DB::table('token')
				->where('token',$token)
				->first();

			if(empty($check)) {
				$post['created_at'] = date('Y-m-d H:i:s');
				$post['token'] = $token;
				$post['created_user'] = Esta::user('0');
				$post['updated_user'] = Esta::user('0');
				$save = DB::table('token')
					->insert($post);
			} else {
				exit();
			}
		} else {
			exit();
		}
	}

	public static function view_bit63() {
		$loket = CRUDBooster::getsetting('pln_kode_loket');
		$nama = CRUDBooster::getsetting('pln_nama_loket');
		$alamat = CRUDBooster::getsetting('pln_alamat_loket');
		$telp = '+62-21 5316.0544##';//CRUDBooster::getsetting('no_telp_loket');

		$length_loket = strlen($loket);
		$length_nama = strlen($nama);
		$length_alamat = strlen($alamat);
		$length_telp = strlen($telp);

		$spasi_loket = 32-$length_loket;
		$spasi_nama = 30-$length_nama;
		$spasi_alamat = 50-$length_alamat;
		$spasi_telp = 18-$length_telp;

		$view_spasi_loket = '';
		for ($i=0; $i < $spasi_loket ; $i++) { 
			$view_spasi_loket .= ' ';
		}
		$view_spasi_nama = '';
		for ($i=0; $i < $spasi_nama ; $i++) { 
			$view_spasi_nama .= ' ';
		}
		$view_spasi_alamat = '';
		for ($i=0; $i < $spasi_alamat ; $i++) { 
			$view_spasi_alamat .= ' ';
		}
		$view_spasi_telp = '';
		for ($i=0; $i < $spasi_telp ; $i++) { 
			$view_spasi_telp .= ' ';
		}

		$view_loket = $loket.$view_spasi_loket;
		$view_nama = $nama.$view_spasi_nama;
		$view_alamat = $alamat.$view_spasi_alamat;
		$view_telp = $telp.$view_spasi_telp;
		return $view_loket.$view_nama.$view_alamat.$view_telp.',#';
	}

	public static function left_padding($string,$length) {
		$string_view = strlen($string);
		$spasi_string = $length-$string_view;
		$view_spasi = '';
		for ($i=0; $i < $spasi_string ; $i++) { 
			$view_spasi .= ' ';
		}
		if($string_view == $length) {
			return $string;
		} else {
			return $string.$view_spasi;
		}
	}

	public static function date_indo($bln,$view) {
		switch ($bln) {
			case '01':
				$return = 'JAN';
				$return_lengkap = 'Januari';
				break;
			case '02':
				$return = 'FEB';
				$return_lengkap = 'Februari';
				break;
			case '03':
				$return = 'MAR';
				$return_lengkap = 'Maret';
				break;
			case '04':
				$return = 'APR';
				$return_lengkap = 'April';
				break;
			case '05':
				$return = 'MEI';
				$return_lengkap = 'Mei';
				break;
			case '06':
				$return = 'JUN';
				$return_lengkap = 'Juni';
				break;
			case '07':
				$return = 'JUL';
				$return_lengkap = 'Juli';
				break;
			case '08':
				$return = 'AGU';
				$return_lengkap = 'Agustus';
				break;
			case '09':
				$return = 'SEP';
				$return_lengkap = 'September';
				break;
			case '10':
				$return = 'OKT';
				$return_lengkap = 'Oktober';
				break;
			case '11':
				$return = 'NOV';
				$return_lengkap = 'November';
				break;
			case '12':
				$return = 'DES';
				$return_lengkap = 'Desember';
				break;
			default:
				$return = '-';
				$return_lengkap = '-';
				break;
		}

		return ($view == 'lengkap' ? $return_lengkap : $return);
	}

	public static function sendFCM($regID=[],$data){
         if(!$data['title'] || !$data['content']) return 'title , content null !';

         $apikey = CRUDBooster::getSetting('google_fcm_key');
         $url    = 'https://fcm.googleapis.com/fcm/send';
         $msg = array
			(
			  'title'   => $data['title'],
			  'content'    => $data['content'],
			  'type'   => (!empty($data['type']) ? $data['type'] : '""')
			);
         $fields = array(
			'registration_ids' => $regID,
			'data' => $msg,
			'content_available'=>true,
			'priority'=>'high'
		   );
         $headers = array(
           'Authorization:key=' . $apikey,
           'Content-Type:application/json'
         );

         $ch = curl_init($url); 
         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0 );
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0 );
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $fields));
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $chresult = curl_exec($ch);
         curl_close($ch);    

         $sv['created_at'] = date('Y-m-d H:i:s');
         $sv['log'] = $chresult;
         $sv['regid'] = json_encode($regID);
         $sv['data'] = json_encode($data);
         $save = DB::table('log_fcm')
         	->insert($sv);

         return $chresult;

     }

	public static function amount_product($id_product) {
		$prod = DB::table('pan_product')->where('id',$id_product)->first();
		/*if($prod->id_tipe_layanan != 4 || $prod->id_tipe_layanan != 1) {
			return $prod->amount;
		} else {*/
			return $prod->amount+$prod->margin+$prod->admin_edn+$prod->admin_1+$prod->admin_2+$prod->admin_3-$prod->potongan;
		//}
	}

	public static function amount_product_trans($id_product,$id_agen,$id_voucher) {
		$amount_prod = Esta::amount_product($id_product);
		$agen = DB::table('agen')->where('id',$id_agen)->first();
		$product = DB::table('pan_product')->where('id',$id_product)->first();
		$voucher = DB::table('voucher')->where('id',$id_voucher)->first();
		$komisi = ($agen->status_agen == 'Basic' ? $product->komisi_basic : $product->komisi_premium);

		$total_pembayaran = $amount_prod/*-$komisi*/-$voucher->amount;
		return ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
	}

	public static function amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount) {
		$amount_prod = 0;//Esta::amount_product($id_product);
		$agen = DB::table('agen')->where('id',$id_agen)->first();
		$product = DB::table('pan_product')->where('id',$id_product)->first();
		$voucher = DB::table('voucher')->where('id',$id_voucher)->first();
		$komisi = ($agen->status_agen == 'Basic' ? $product->komisi_basic : $product->komisi_premium);

		$total_pembayaran = $amount_prod+$amount/*-$komisi*/-$voucher->amount;
		return ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
	}

	public static function amount_product_pln_pascabayar($id_product,$id_agen,$id_voucher,$amount) {
		$prod = DB::table('pan_product')->where('id',$id_product)->first();
		$biaya_admin = $prod->margin+$prod->admin_edn+$prod->admin_1+$prod->admin_2+$prod->admin_3-$prod->potongan;

		
		$agen = DB::table('agen')->where('id',$id_agen)->first();
		$product = DB::table('pan_product')->where('id',$id_product)->first();
		$voucher = DB::table('voucher')->where('id',$id_voucher)->first();
		$komisi = ($agen->status_agen == 'Basic' ? $product->komisi_basic : $product->komisi_premium);

		$total_pembayaran = $amount-$komisi/*+$biaya_admin$voucher->amount*/;
		return ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
	}

	public static function amount_product_trans_pln_prabayar($id_product,$id_agen,$id_voucher,$amount) {
		$amount_prod = Esta::amount_product($id_product);
		$agen = DB::table('agen')->where('id',$id_agen)->first();
		$product = DB::table('pan_product')->where('id',$id_product)->first();
		$voucher = DB::table('voucher')->where('id',$id_voucher)->first();
		$komisi = ($agen->status_agen == 'Basic' ? $product->komisi_basic : $product->komisi_premium);

		$total_pembayaran = $amount_prod+$amount-$komisi-$voucher->amount;
		return ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
	}

	public static function change_date_format($date,$format) {
		$originalDate = $date;
		if($date != '') {
			$newDate = date($format, strtotime($originalDate));
		} else {
			$newDate = '';
		}

		return $newDate;
	}

	public static function nomor_transaksi($table,$prefix) {
		$kode_before = DB::table($table)
			//->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'));
			if($table == 'agen') {
				$kode_before = $kode_before->where('kode','!=',NULL);
			} elseif($table == 'tu_res_sprint') {
				$kode_before = $kode_before->where('transactionNo','!=',NULL);
			}
			$kode_before = $kode_before
			->orderBy('id','desc')
			->limit(1)
			->first();

		if($table == 'agen') {
			$kode_sebelum = $kode_before->kode;
		} elseif($table == 'tu_res_sprint') {
			$kode_sebelum = $kode_before->transactionNo;
		} elseif($table == 'trans_topup_batch' || $table == 'trans_tarik_tunai_batch' || $table == 'trans_pulsa_batch' || $table == 'trans_pln_batch' || $table == 'trans_pdam_batch' || $table == 'trans_bpjs_batch') {
			$kode_sebelum = $kode_before->no_batch;
		} else {
			$kode_sebelum = $kode_before->trans_no;
		}
		$check_month = substr($kode_sebelum,4,2);

		if($check_month != date('m')) {
			$kode = $prefix.Date('ym').'000001';
		} else {
			$kode = $prefix.(!empty($kode_before) ? Date('ym').sprintf("%06s", substr($kode_sebelum, -6)+1) : Date('dm').'000001');
		}

		return $kode;
	}

	public static function nomor_transaksi2($table,$prefix) {
		$kode_before = DB::table($table)
			//->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'));
			if($table == 'agen') {
				$kode_before = $kode_before->where('kode','!=',NULL);
			} elseif($table == 'tu_res_sprint') {
				$kode_before = $kode_before->where('transactionNo','!=',NULL);
			}
			$kode_before = $kode_before
			->orderBy('id','desc')
			->limit(1)
			->first();

		if($table == 'agen') {
			$kode_sebelum = $kode_before->kode;
		} elseif($table == 'tu_res_sprint') {
			$kode_sebelum = $kode_before->transactionNo;
		} elseif($table == 'trans_topup_batch' || $table == 'trans_tarik_tunai_batch' || $table == 'trans_pulsa_batch' || $table == 'trans_pln_batch' || $table == 'trans_pdam_batch' || $table == 'trans_bpjs_batch') {
			$kode_sebelum = $kode_before->no_batch;
		} else {
			$kode_sebelum = $kode_before->trans_no;
		}
		$check_month = substr($kode_sebelum,4,2);

		if($check_month != date('m')) {
			$kode = $prefix.Date('ym').'000001';
		} else {
			$kode = $prefix.(!empty($kode_before) ? Date('ym').sprintf("%06s", substr($kode_sebelum, -6)+1) : Date('dm').'000001');
		}

		return $kode;
	}

	public static function kode_referall() {
		$kode_referall_agen = 'RE'.rand(1111, 9999);
		$check = DB::table('agen')
			->where('kode_referall_agen',$kode_referall_agen)
			->first();
		if(!empty($check)) {
			return Esta::kode_referall();
		} else {
			return $kode_referall_agen;
		}
	}

	public static function jurnal($data){
		/*$jl = DB::table('jurnal')
			->orderBy('id_bundle','desc')
			->orderBy('id','desc')
			->first();
		if(!empty($jl)) {
			$id_bundle = $jl->id_bundle+1;
		} else {
			$id_bundle = 1;
		}*/

		$cek = DB::table('jurnal')
			->where('account_code',$data['account_code'])
			->where('id_transaksi',$data['id_transaksi'])
			->where('kategori',$data['kategori'])
			->get();

		if(empty($cek[0]->id)) {
			$sv['created_user'] = Esta::user('0');
			$sv['updated_user'] = Esta::user('0');
			$sv['created_at'] = date('Y-m-d H:i:s');
			$sv['id_transaksi'] = $data['id_transaksi'];
			$sv['description'] = $data['description'];
			$sv['type'] = $data['type'];
			$sv['transaction_number'] = $data['transaction_number'];
			$sv['amount'] = $data['amount'];
			$sv['kategori'] = $data['kategori'];
			$sv['transaction_date'] = $data['transaction_date'];
			$sv['account_code'] = $data['account_code'];
			$sv['memo'] = $data['memo'];
			$sv['id_bundle'] = $data['id_bundle'];

			$sv['created_user'] = Esta::user('0');
			$sv['updated_user'] = Esta::user('0');
			$save = DB::table('jurnal')
				->insert($sv);
		}
	}

	public static function jurnal_header($nama){
		$jh['created_at'] = date('Y-m-d H:i:s');
		$jh['updated_at'] = date('Y-m-d H:i:s');
		$jh['nama'] = $nama;

		$sv = DB::table('jurnal_header')
			->insertGetId($jh);

		return $sv;
	}

	public static function undo_jurnal($id_transaksi,$kategori) {
		$delete = DB::table('jurnal')
			->where('id_transaksi',$id_transaksi)
			->where('kategori',$kategori)
			->delete();
	}

	/*public static function log_money($id_agen,$nominal,$datetime,$title,$description,$type,$kategori,$tbl_transaksi,$id_transaksi){
		$sv['created_at'] = $datetime;
		$sv['id_agen'] = $id_agen;
		$sv['title'] = $title;
		$sv['description'] = $description;
		$sv['nominal'] = $nominal;
		$sv['type'] = $type;
		$sv['kategori'] = $kategori;

		$agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$saldo_sekarang = $agen->saldo;
		$komisi_sekarang = $agen->komisi;
		if($type == 'In') {
			if($kategori == 'Transaksi') {
				$up['saldo'] = $saldo_sekarang+$nominal;
				$up2['saldo'] = $saldo_sekarang+$nominal;
			}
			if($kategori == 'Komisi') {
				$up['komisi'] = $komisi_sekarang+$nominal;
				$up2['saldo'] = $saldo_sekarang+$nominal;
			}
		} else {
			if($kategori == 'Transaksi') {
				$up['saldo'] = $saldo_sekarang-$nominal;
				$up2['saldo'] = $saldo_sekarang-$nominal;
			}
			if($kategori == 'Komisi') {
				$up['komisi'] = $komisi_sekarang-$nominal;
				$up2['saldo'] = $saldo_sekarang-$nominal;
			}
		}

		if($kategori == 'Transaksi' || $kategori == 'Komisi') {
			$up['updated_user'] = Esta::user('0');
			$update_saldo = DB::table('agen')
				->where('id',$id_agen)
				->update($up);
		}

		$sv['created_user'] = Esta::user('0');
		$sv['updated_user'] = Esta::user('0');
		$sv['saldo_sesudah'] = $up2['saldo'];
		$sv['saldo_sebelum'] = $saldo_sekarang;
		$sv['tbl_transaksi'] = $tbl_transaksi;
		$sv['id_transaksi'] = $id_transaksi;

		$sv['created_user'] = Esta::user('0');
		$sv['updated_user'] = Esta::user('0');
		if($sv['saldo_sebelum'] < 0) {
			return 0;
		} else {

			$save = DB::table('log_money')
			->insert($sv);
			return 1;
		}
	}*/

	public static function log_money_saldo($id_agen,$nominal,$datetime,$title,$description,$type,$kategori){
		$sv['created_at'] = $datetime;
		$sv['id_agen'] = $id_agen;
		$sv['title'] = $title;
		$sv['description'] = $description;
		$sv['nominal'] = $nominal;
		$sv['type'] = $type;
		$sv['kategori'] = $kategori;

		$agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$saldo_sekarang = $agen->saldo;
		$komisi_sekarang = $agen->komisi;
		if($type == 'In') {
			if($kategori == 'Transaksi') {
				$up['saldo'] = $saldo_sekarang+$nominal;
			}
			if($kategori == 'Komisi') {
				$up['komisi'] = $komisi_sekarang+$nominal;
				$up['saldo'] = $saldo_sekarang+$nominal;
			}
		} else {
			if($kategori == 'Transaksi') {
				$up['saldo'] = $saldo_sekarang-$nominal;
			}
			if($kategori == 'Komisi') {
				$up['komisi'] = $komisi_sekarang-$nominal;
				$up['saldo'] = $saldo_sekarang-$nominal;
			}
		}

		if($kategori == 'Transaksi' || $kategori == 'Komisi') {
			$up['created_user'] = Esta::user('0');
			$up['updated_user'] = Esta::user('0');
			$update_saldo = DB::table('agen')
				->where('id',$id_agen)
				->update($up);
		}

		$sv['created_user'] = Esta::user('0');
		$sv['updated_user'] = Esta::user('0');
		$sv['saldo_sesudah'] = $up['saldo'];
		$sv['saldo_sebelum'] = $saldo_sekarang;

		$sv['created_user'] = Esta::user('0');
		$sv['updated_user'] = Esta::user('0');
		$save = DB::table('log_money')
			->insert($sv);
	}

	public static function send_sms($to,$msg) {
		/*$to = '6285287882816';
		$msg = 'yaaa ok';*/
		$msg = str_replace(' ', '%20', $msg);
		$too = substr($to, 0,2);
		if($too == '08') {
			$to = '628'.substr($to, 2);
		}
		$url = 'http://103.81.246.59:20003/sendsms?account=eimsestadig2&password=123456&numbers='.$to.'&content='.$msg;
		//$url = 'http://platform.smsmasking.id:20003/sendsms?account=eimsestadig2&password=123456&numbers='.$to.'&content='.$msg;		
		
		$result = file_get_contents($url);
		
		// KALAU PAKE HTTPS
		/* $arrContextOptions=array(
			"ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			),
		); 
		$url = 'https://numberic1.tcastsms.net:20005/sendsms?account=numb_esta3&password=123456&numbers='.$to.'&content='.$msg;
		$result = file_get_contents($url, false, stream_context_create($arrContextOptions)); */

		// Will dump a beauty json :3
		return json_decode($result, true);
	}

	public static function send_smsx($to,$msg) {
		$pecah    = explode( ",", $to );
		$jumlah   = count( $pecah );
		$from     = ""; //Sender ID or SMS Masking Name, if leave blank, it will use default from telco
		$username = "eimsestadig2"; //your eims username
		$password = "123456"; //your eims password yang didapat dari http://www.cmd5.org/hash.aspx
		$postUrl  = "http://103.81.246.59:8080/EIMSII"; # DO NOT CHANGE THIS

		for ( $i = 0; $i < $jumlah; $i ++ ) {
			if ( substr( $pecah[ $i ], 0, 2 ) == "62" || substr( $pecah[ $i ], 0, 3 ) == "+62" ) {
				$pecah = $pecah;
			} elseif ( substr( $pecah[ $i ], 0, 1 ) == "0" ) {
				$pecah[ $i ][0] = "X";
				$pecah          = str_replace( "X", "62", $pecah );
			} else {
				//echo "Invalid mobile number format";
			}

			$nohp_before = substr($to, 0, 2);
			$nohp_after = substr($to, 2);
			if($nohp_before == '08') {
				$to = '628'.$nohp_after;
			}
			/*return $to;
			exit();*/

			$request = [
				'version'  => '2.0',
				'username' => $username,
				'password' => $password,
				'key'      => '1527045945', //key yang didapatkan dari https://tool.lu/timestamp/
				'task_num' => '1',
				'tasks'    => [
					[
						'tid'     => 1234567,
						'to'      => $to,
						'sms'     => $msg
					]
				]
			];

			$data_string = json_encode( $request );

			$ch = curl_init( $postUrl );
			curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_string );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Accept:application/json'
				)
			);

			$response = curl_exec( $ch );
			$httpCode     = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
			$responseBody = json_decode( $response );

			if ( $httpCode == 200 ) {
				//return print_r((array)$responseBody);
				//return $responseBody.$response.$httpCode;
			}
			curl_close( $ch );
		}
		
	}

	public static function send_iso_pulsa_prabayar($id_product,$no_hp,$mti,$status,$id_transaksi) {
		$product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$kode_biller = DB::table('pan_kode_biller')
			->where('id',$product->id_kode_biller)
			->first()->kode;

		$kode_layanan = DB::table('pan_kode_layanan')
			->where('id',$product->id_kode_layanan)
			->first()->kode;

		$tipe_layanan = DB::table('pan_tipe_layanan')
			->where('id',$product->id_tipe_layanan)
			->first()->kode;

		$detail_transaksi = DB::table('trans_pulsa')
			->where('id',$id_transaksi)
			->first();

		$detail_inquiry = DB::table('log_jatelindo_bit')
			->where('id_transaksi',$id_transaksi)
			->where('type','Pulsa Prepaid')
			->where('status','Purchase')
			->where('jenis','req')
			->first();

		$kode_transaksi = $detail_transaksi->trans_no;
		//$kode_transaksi = $kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));

		switch ($status) {
			case 'Purchase':
				$bit2 = $tipe_layanan.$kode_layanan.$kode_biller;/*fix*/
				//echo $bit2;
				//exit();
				//$bit48 = "'".$product->product_id."'"; /*private data.*/
				$bit3 =  '171000'; /*Processing Code fix 380000 : inquiry, 170000/171000/172000 : payment/Purchase/advice, 000200/000300 : reversal/reversal repeat*/
				$bit4 = sprintf("%012s", $product->amount); /*amount trx*/
				$bit7 = date('mdhms'); /*Transmission date/time MMDDhhmmss*/
				$bit11 = date('sd').rand(11,99);//date('mds'); /*STAN unik*/
				$bit12 = date('hms'); /*Local Time hhmmss*/
				$bit13 = date('md'); /*Local Date MMDD*/
				$bit15 = date('md'); /*Settlement Date kapan transaksi akan disettle. (MMDD)*/
				$bit18 = CRUDBooster::getsetting('multibiller_bit_18'); /*Merchant Type. klasifikasi tipe merchant untuk setiap bisnis produk layanan*/
				$bit32 = CRUDBooster::getsetting('multibiller_bit_32'); /*Acquiring Institution ID/Banking Code Nilai ini unik untuk setiap institusi. Untuk bangking nilai ini mengacu pada kode bank Indonesia.*/
				$bit37 = $kode_transaksi; /*Retrieval Reference Number. nomor referensi yang dipergunakan oleh system untuk mengacu pada transaksi tersebut.*/
				//$bit39 = '00'; /*Response Code*/
				$bit41 = CRUDBooster::getsetting('multibiller_bit_41'); /*Terminal ID. ID ini diserahkan pada setiap pihak acquirer sebagai keterangan untuk setiap pesan.*/
				$bit42 = CRUDBooster::getsetting('multibiller_bit_42'); /*Acceptor ID. diberikan oleh pihak jatelindo untuk setiap acquirer*/
				//$bit48 = 'JTL53L3149876543210000000000000';
				$no_akhir1 = substr($no_hp, 4);
				$no_akhir = substr($no_akhir1, 0, 10);

				$telp = $no_akhir;
				$length_telp = strlen($telp);
				$spasi_telp = 10-$length_telp;
				$view_spasi_telp = '';
				for ($i=0; $i < $spasi_telp ; $i++) { 
					$view_spasi_telp .= ' ';
				}
				$view_telp = $telp.$view_spasi_telp;


				$bit48 = sprintf("%04s", $product->product_id).substr($no_hp, 0, 4).$view_telp; /*Private Data. 4 Product ID. 4 kode operator. 10 Phone Number*/
				$bit49 = '360'; /*Transaction Currency Code*/
				$bit61 = ''; /*Private Use*/
				//$mti = '0200'; 
				$bit62 = '';
				$bit63 = 'Esta';//sprintf("%32s\n", CRUDBooster::getsetting('pln_kode_loket')).sprintf("%30s\n", CRUDBooster::getsetting('pln_nama_loket')).sprintf("%50s\n", CRUDBooster::getsetting('pln_alamat_loket')).sprintf("%18s\n", CRUDBooster::getsetting('pln_no_telp_loket'));
				/*Pesan ini otomatis dikirimkan jika dan hanya jika purchase tidak mendapat response atau mendapat response code 18.*/
				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-inquiry';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 60,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0220';
						$lab['status'] = 'Advice purchase';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;
						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);

						$svvv['created_at'] = date('Y-m-d H:i:s');
						$svvv['bit2 '] = $bit2;
						$svvv['bit3 '] = $bit3;
						$svvv['bit4 '] = $bit4;
						$svvv['bit7 '] = $bit7;
						$svvv['bit11'] = $bit11;
						$svvv['bit12'] = $bit12;
						$svvv['bit13'] = $bit13;
						$svvv['bit15'] = $bit15;
						$svvv['bit18'] = $bit18; 
						$svvv['bit32'] = $bit32;
						$svvv['bit37'] = $bit37;
						$svvv['bit41'] = $bit41;
						$svvv['bit42'] = $bit42;
						$svvv['bit48'] = $bit48; 
						$svvv['bit49'] = $bit49;
						$svvv['bit61'] = $bit61;  
						$svvv['bit62'] = $bit62;
						$svvv['bit63'] = $bit63;
						$svvv['type'] = 'Pulsa Prepaid';
						$svvv['id_transaksi'] = $id_transaksi;
						$svvv['jenis'] = 'req';
						$svvv['status'] = 'Purchase';
						$svvv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
						$svvv['created_user'] = Esta::user('0');
						$svvv['updated_user'] = Esta::user('0');
						$savev = DB::table('log_jatelindo_bit')
							->insertGetId($svvv);
						return Esta::send_iso_pulsa_prabayar($id_product,$no_hp,'0220','Advice',$id_transaksi);
						//exit();
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'Pulsa Prepaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($errr) {
					$svv['res'] = "cURL Error #:" . $errr;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pls_pra_product_id'] = substr($svv['bit48'], 0, 4); 
				$svv['pls_pra_code_area'] = substr($svv['bit48'], 4, 4); 
				$svv['pls_pra_phone_no'] = substr($svv['bit48'], 8, 10); 
				$svv['pls_pra_voucher_nominal'] = substr($svv['bit48'], 18, 12); 
				$svv['pls_pra_biaya_admin'] = substr($svv['bit48'], 30, 8); 
				$svv['pls_pra_expire'] = substr($svv['bit48'], 38, 8); 
				$svv['pls_pra_serial_no'] = substr($svv['bit48'], 46, 16); 
				$svv['pls_pra_no_ref_switching'] = substr($svv['bit48'], 62, 32); 
				$svv['pls_pra_waktu_lunas'] = substr($svv['bit48'], 94, 14); 
				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				$json['sn'] = $svv['pls_pra_serial_no'];
				$json['no_ref'] = $svv['pls_pra_no_ref_switching'];
				$th = substr($svv['pls_pra_waktu_lunas'], 0,4);
				$bln = substr($svv['pls_pra_waktu_lunas'], 4,2);
				$tgl = substr($svv['pls_pra_waktu_lunas'], 6,2);
				$jam = substr($svv['pls_pra_waktu_lunas'], 8,2);
				$menit = substr($svv['pls_pra_waktu_lunas'], 10,2);
				$detik = substr($svv['pls_pra_waktu_lunas'], 12,2);
				$json['tanggal'] = $tgl.'/'.$bln.'/'.$th.' '.$jam.':'.$menit;
				/*log res*/
				$up['jpa_ref'] = $svv['pls_pra_no_ref_switching'];

				$update = DB::table('trans_pulsa')
					->where('id',$id_transaksi)
					->update($up);	
				break;
			case 'Advice':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '172000';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 60,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0220';
						$lab['status'] = 'Advice Repeat 1';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;
						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');

						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pulsa_prabayar($id_product,$no_hp,'0220','Advice Repeat 1',$id_transaksi);
						//exit();
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				$svv['type'] = 'Pulsa Prepaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($errr) {
					$svv['res'] = "cURL Error #:" . $errr;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pls_pra_product_id'] = substr($svv['bit48'], 0, 4); 
				$svv['pls_pra_code_area'] = substr($svv['bit48'], 4, 4); 
				$svv['pls_pra_phone_no'] = substr($svv['bit48'], 8, 10); 
				$svv['pls_pra_voucher_nominal'] = substr($svv['bit48'], 18, 12); 
				$svv['pls_pra_biaya_admin'] = substr($svv['bit48'], 30, 8); 
				$svv['pls_pra_expire'] = substr($svv['bit48'], 38, 8); 
				$svv['pls_pra_serial_no'] = substr($svv['bit48'], 46, 16); 
				$svv['pls_pra_no_ref_switching'] = substr($svv['bit48'], 62, 32); 
				$svv['pls_pra_waktu_lunas'] = substr($svv['bit48'], 94, 14);
				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				$json['sn'] = $svv['pls_pra_serial_no'];
				$json['no_ref'] = $svv['pls_pra_no_ref_switching'];
				$th = substr($svv['pls_pra_waktu_lunas'], 0,4);
				$bln = substr($svv['pls_pra_waktu_lunas'], 4,2);
				$tgl = substr($svv['pls_pra_waktu_lunas'], 6,2);
				$jam = substr($svv['pls_pra_waktu_lunas'], 8,2);
				$menit = substr($svv['pls_pra_waktu_lunas'], 10,2);
				$detik = substr($svv['pls_pra_waktu_lunas'], 12,2);
				$json['tanggal'] = $tgl.'/'.$bln.'/'.$th.' '.$jam.':'.$menit;
				/*log res*/
				$up['jpa_ref'] = $svv['pls_pra_no_ref_switching'];

				$update = DB::table('trans_pulsa')
					->where('id',$id_transaksi)
					->update($up);	
				break;

			case 'Advice Repeat 1':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '172000';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 60,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0220';
						$lab['status'] = 'Pending';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;
						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						//return Esta::send_iso_pulsa_prabayar($id_product,$no_hp,'0220','Advice Repeat 2',$id_transaksi);
						$json['39'] = 'Pending';
						return $json;
						exit();
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				$svv['type'] = 'Pulsa Prepaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($errr) {
					$svv['res'] = "cURL Error #:" . $errr;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pls_pra_product_id'] = substr($svv['bit48'], 0, 4); 
				$svv['pls_pra_code_area'] = substr($svv['bit48'], 4, 4); 
				$svv['pls_pra_phone_no'] = substr($svv['bit48'], 8, 10); 
				$svv['pls_pra_voucher_nominal'] = substr($svv['bit48'], 18, 12); 
				$svv['pls_pra_biaya_admin'] = substr($svv['bit48'], 30, 8); 
				$svv['pls_pra_expire'] = substr($svv['bit48'], 38, 8); 
				$svv['pls_pra_serial_no'] = substr($svv['bit48'], 46, 16); 
				$svv['pls_pra_no_ref_switching'] = substr($svv['bit48'], 62, 32); 
				$svv['pls_pra_waktu_lunas'] = substr($svv['bit48'], 94, 14);
				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				$json['sn'] = $svv['pls_pra_serial_no'];
				$json['no_ref'] = $svv['pls_pra_no_ref_switching'];
				$th = substr($svv['pls_pra_waktu_lunas'], 0,4);
				$bln = substr($svv['pls_pra_waktu_lunas'], 4,2);
				$tgl = substr($svv['pls_pra_waktu_lunas'], 6,2);
				$jam = substr($svv['pls_pra_waktu_lunas'], 8,2);
				$menit = substr($svv['pls_pra_waktu_lunas'], 10,2);
				$detik = substr($svv['pls_pra_waktu_lunas'], 12,2);
				$json['tanggal'] = $tgl.'/'.$bln.'/'.$th.' '.$jam.':'.$menit;
				/*log res*/
				$up['jpa_ref'] = $svv['pls_pra_no_ref_switching'];

				$update = DB::table('trans_pulsa')
					->where('id',$id_transaksi)
					->update($up);	
				break;

			case 'Advice Repeat 2':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '172000';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 60,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0220';
						$lab['status'] = 'Pending';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;
						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						//$json = array();
						$json['39'] = 'Pending';
						return $json;
						exit();
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				$svv['type'] = 'Pulsa Prepaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($errr) {
					$svv['res'] = "cURL Error #:" . $errr;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pls_pra_product_id'] = substr($svv['bit48'], 0, 4); 
				$svv['pls_pra_code_area'] = substr($svv['bit48'], 4, 4); 
				$svv['pls_pra_phone_no'] = substr($svv['bit48'], 8, 10); 
				$svv['pls_pra_voucher_nominal'] = substr($svv['bit48'], 18, 12); 
				$svv['pls_pra_biaya_admin'] = substr($svv['bit48'], 30, 8); 
				$svv['pls_pra_expire'] = substr($svv['bit48'], 38, 8); 
				$svv['pls_pra_serial_no'] = substr($svv['bit48'], 46, 16); 
				$svv['pls_pra_no_ref_switching'] = substr($svv['bit48'], 62, 32); 
				$svv['pls_pra_waktu_lunas'] = substr($svv['bit48'], 94, 14);

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				$json['sn'] = $svv['pls_pra_serial_no'];
				$json['no_ref'] = $svv['pls_pra_no_ref_switching'];
				$th = substr($svv['pls_pra_waktu_lunas'], 0,4);
				$bln = substr($svv['pls_pra_waktu_lunas'], 4,2);
				$tgl = substr($svv['pls_pra_waktu_lunas'], 6,2);
				$jam = substr($svv['pls_pra_waktu_lunas'], 8,2);
				$menit = substr($svv['pls_pra_waktu_lunas'], 10,2);
				$detik = substr($svv['pls_pra_waktu_lunas'], 12,2);
				$json['tanggal'] = $tgl.'/'.$bln.'/'.$th.' '.$jam.':'.$menit;
				/*log res*/
				$up['jpa_ref'] = $svv['pls_pra_no_ref_switching'];

				$update = DB::table('trans_pulsa')
					->where('id',$id_transaksi)
					->update($up);	
				break;
		}

		/*log req*/
		$sv['type'] = 'Pulsa Prepaid';
		$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
		if ($err) {
			$sv['res'] = "cURL Error #:" . $err;
		} else {
			$sv['res'] = $response;
		}
		$sv['created_at'] = date('Y-m-d H:i:s');
		$sv['bit2 '] = $bit2;
		$sv['bit3 '] = $bit3;
		$sv['bit4 '] = $bit4;
		$sv['bit7 '] = $bit7;
		$sv['bit11'] = $bit11;
		$sv['bit12'] = $bit12;
		$sv['bit13'] = $bit13;
		$sv['bit15'] = $bit15;
		$sv['bit18'] = $bit18; 
		$sv['bit32'] = $bit32;
		$sv['bit37'] = $bit37;
		$sv['bit41'] = $bit41;
		$sv['bit42'] = $bit42;
		$sv['bit48'] = $bit48; 
		$sv['bit49'] = $bit49;
		$sv['bit61'] = $bit61;  
		$sv['bit62'] = $bit62;
		$sv['bit63'] = $bit63;
		$sv['mti'] = $mti;
		$sv['status'] = $status;
		$sv['id_transaksi'] = $id_transaksi;
		$sv['jenis'] = 'req';

		$sv['created_user'] = Esta::user('0');
		$sv['updated_user'] = Esta::user('0');
		$save = DB::table('log_jatelindo_bit')
			->insert($sv);
		/*log req*/

		if ($err) {
		  	return $err;
		} else {
			if($json['39'] == '18' || $json['39'] == '06') {

				if($status == 'Purchase') {
					$lab['created_at'] = date('Y-m-d H:i:s');
					$lab['mti'] = '0220';
					$lab['status'] = 'Advice';
					$lab['id_log'] = $id_transaksi;
					$lab['id_product'] = $id_product;
					$lab['json'] = $status;

					$lab['created_user'] = Esta::user('0');
					$lab['updated_user'] = Esta::user('0');
					$sv_lab = DB::table('log_abnormal')
						->insert($lab);
					return Esta::send_iso_pulsa_prabayar($id_product,$no_hp,'0220','Advice',$id_transaksi);
				} else {
					if($status == 'Advice') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0220';
						$lab['status'] = 'Advice Repeat 1';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pulsa_prabayar($id_product,$no_hp,'0220','Advice Repeat 1',$id_transaksi);
					} else {
						if($status == 'Advice Repeat 1') {
							$lab['created_at'] = date('Y-m-d H:i:s');
							$lab['mti'] = '0220';
							$lab['status'] = 'Pending';
							$lab['id_log'] = $id_transaksi;
							$lab['id_product'] = $id_product;
							$lab['json'] = $errr;

							$lab['created_user'] = Esta::user('0');
							$lab['updated_user'] = Esta::user('0');

							$sv_lab = DB::table('log_abnormal')
								->insert($lab);
							//return Esta::send_iso_pulsa_prabayar($id_product,$no_hp,'0220','Advice Repeat 2',$id_transaksi);
							$json['39'] = 'Pending';
							return $json;
						} else {
							/*if($json['39'] == '18' || $json['39'] == '06' && $status == 'Advice Repeat 2') {
								$json['39'] = 'Pending';
								return $json;
							} else {*/
								return $json;
							//}
						}
					}
				}
			} else {
				return $json;
			}
		}
	}

	public static function send_iso_pulsa_pascabayar($id_product,$no_hp,$mti,$status,$id_transaksi) {
		$product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$kode_biller = DB::table('pan_kode_biller')
			->where('id',$product->id_kode_biller)
			->first()->kode;

		$kode_layanan = DB::table('pan_kode_layanan')
			->where('id',$product->id_kode_layanan)
			->first()->kode;

		$tipe_layanan = DB::table('pan_tipe_layanan')
			->where('id',$product->id_tipe_layanan)
			->first()->kode;

		$detail_transaksi = DB::table('trans_pulsa')
			->where('id',$id_transaksi)
			->first();

		$detail_inquiry = DB::table('log_jatelindo_bit')
			->where('id',$id_transaksi)
			->where('type','Pulsa Postpaid')
			->where('status','Inquiry')
			->where('jenis','res')
			->first();

		//$kode_transaksi = $detail_transaksi->trans_no;
		//$kode_transaksi = $kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));
		

		switch ($status) {
			case 'Inquiry':
				$bit2 = $tipe_layanan.$kode_layanan.$kode_biller;/*fix*/
				//echo $bit2;
				//exit();
				//$bit48 = "'".$product->product_id."'"; /*private data.*/
				$bit3 =  '380000'; /*Processing Code fix 380000 : inquiry, 170000/171000/172000 : payment/Purchase/advice, 000200/000300 : reversal/reversal repeat*/
				$bit4 = 0; /*amount trx*/
				$bit7 = date('mdhms'); /*Transmission date/time MMDDhhmmss*/
				$bit11 = date('sd').rand(11,99);//date('mds'); /*STAN unik*/
				$bit12 = date('hms'); /*Local Time hhmmss*/
				$bit13 = date('md'); /*Local Date MMDD*/
				$bit15 = date('md'); /*Settlement Date kapan transaksi akan disettle. (MMDD)*/
				$bit18 = CRUDBooster::getsetting('multibiller_bit_18'); /*Merchant Type. klasifikasi tipe merchant untuk setiap bisnis produk layanan*/
				$bit32 = CRUDBooster::getsetting('multibiller_bit_32'); /*Acquiring Institution ID/Banking Code Nilai ini unik untuk setiap institusi. Untuk bangking nilai ini mengacu pada kode bank Indonesia.*/
				
				//$bit39 = '00'; /*Response Code*/
				$bit41 = CRUDBooster::getsetting('multibiller_bit_41'); /*Terminal ID. ID ini diserahkan pada setiap pihak acquirer sebagai keterangan untuk setiap pesan.*/
				$bit42 = CRUDBooster::getsetting('multibiller_bit_42'); /*Acceptor ID. diberikan oleh pihak jatelindo untuk setiap acquirer*/
				//$bit48 = 'JTL53L3149876543210000000000000';
				$no_akhir1 = substr($no_hp, 4);
				$no_akhir = substr($no_akhir1, 0, 10);
				$bit48 = sprintf("%04s", $product->product_id).substr($no_hp, 0, 4).sprintf("%-10d", $no_akhir);
				//$bit48 = sprintf("%04s", $product->product_id).substr($no_hp, 0, 4).sprintf("0%10s", substr($no_hp, 4)); /*Private Data. 4 Product ID. 4 kode operator. 10 Phone Number*/
				$bit49 = '360'; /*Transaction Currency Code*/
				$bit61 = '1'; /*Private Use*/
				//$mti = '0200'; 
				$bit62 = '';
				$bit63 = str_replace(',#','',Esta::view_bit63());
				/*Pesan ini otomatis dikirimkan jika dan hanya jika purchase tidak mendapat response atau mendapat response code 18.*/
				
				/*log req*/
				$sv['type'] = 'Pulsa Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$kode_transaksi = Esta::nomor_transaksi_ppobnew('log_jatelindo_bit',CRUDBooster::getsetting('transaksi_pulsa'),'Pulsa Postpaid');
				$bit37 = $kode_transaksi; /*Retrieval Reference Number. nomor referensi yang dipergunakan oleh system untuk mengacu pada transaksi tersebut.*/
				$sv['bit37'] = $bit37;
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/
				
				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-inquiry';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'Pulsa Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pls_psc_product_id'] = substr($svv['bit48'], 0, 4); 
				$svv['pls_psc_kode_area'] = substr($svv['bit48'], 4, 4); 
				$svv['pls_psc_phone_no'] = substr($svv['bit48'], 8, 10); 
				$svv['pls_psc_customer_name'] = substr($svv['bit48'], 18, 30); 
				$svv['pls_psc_no_ref_switching'] = substr($svv['bit48'], 48, 32); 
				$svv['pls_psc_total_bill'] = substr($svv['bit48'], 80, 1); 
				$svv['pls_psc_biaya_admin'] = substr($svv['bit48'], 81, 8); 
				$svv['pls_psc_expire'] = substr($svv['bit48'], 89, 8); 
				$svv['pls_psc_bill_ref_1'] = substr($svv['bit48'], 97, 11); 
				$svv['pls_psc_bill_amount_1'] = substr($svv['bit48'], 108, 12); 
				$svv['pls_psc_bill_ref_2'] = substr($svv['bit48'], 120, 11); 
				$svv['pls_psc_bill_amount_2'] = substr($svv['bit48'], 131, 12); 
				$svv['pls_psc_bill_ref_3'] = substr($svv['bit48'], 143, 11); 
				$svv['pls_psc_bill_amount_3'] = substr($svv['bit48'], 155, 12); 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				switch ($svv['pls_psc_total_bill']) {
					case '1':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_1'];
						break;
					case '2':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_2'];
						break;
					case '3':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_3'];
						break;
				}
				$json['pls_psc_customer_name'] = $svv['pls_psc_customer_name'];
				$json['pls_psc_phone_no'] = $svv['pls_psc_phone_no'];
				$json['pls_psc_no_ref_switching'] = $svv['pls_psc_no_ref_switching'];
				$json['pls_psc_total_bill'] = $svv['pls_psc_total_bill'];
				$json['pls_psc_biaya_admin'] = $svv['pls_psc_biaya_admin'];
				$json['id_log'] = $savev;

				$json['pls_psc_blth_1'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_1'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_1'], 0,1);
				$json['pls_psc_blth_2'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_2'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_2'], 0,1);
				$json['pls_psc_blth_3'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_3'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_3'], 0,1);

				$json['pls_psc_jml_tagihan_1'] = $svv['pls_psc_bill_amount_1'];
				$json['pls_psc_jml_tagihan_2'] = $svv['pls_psc_bill_amount_2'];
				$json['pls_psc_jml_tagihan_3'] = $svv['pls_psc_bill_amount_3'];

				$json['total_tagihan'] = $json['pls_psc_jml_tagihan_1']+$json['pls_psc_jml_tagihan_2']+$json['pls_psc_jml_tagihan_3'];
				$json['total_bayar'] = $json['total_tagihan']+$json['pls_psc_biaya_admin'];
				$json['header_msg'] = 'Yes';
				break;
			case 'Payment':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '170000';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;
				
				/*log req*/
				$sv['type'] = 'Pulsa Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0400';
						$lab['status'] = 'Reversal';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0400','Reversal',$id_transaksi);
						//exit();
					}
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'Pulsa Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pls_psc_product_id'] = substr($svv['bit48'], 0, 4); 
				$svv['pls_psc_kode_area'] = substr($svv['bit48'], 4, 4); 
				$svv['pls_psc_phone_no'] = substr($svv['bit48'], 8, 10); 
				$svv['pls_psc_customer_name'] = substr($svv['bit48'], 18, 30); 
				$svv['pls_psc_no_ref_switching'] = substr($svv['bit48'], 48, 32); 
				$svv['pls_psc_total_bill'] = substr($svv['bit48'], 80, 1); 
				$svv['pls_psc_biaya_admin'] = substr($svv['bit48'], 81, 8); 
				$svv['pls_psc_expire'] = substr($svv['bit48'], 89, 8); 
				$svv['pls_psc_bill_ref_1'] = substr($svv['bit48'], 97, 11); 
				$svv['pls_psc_bill_amount_1'] = substr($svv['bit48'], 108, 12); 
				$svv['pls_psc_bill_ref_2'] = substr($svv['bit48'], 120, 11); 
				$svv['pls_psc_bill_amount_2'] = substr($svv['bit48'], 131, 12); 
				$svv['pls_psc_bill_ref_3'] = substr($svv['bit48'], 143, 11); 
				$svv['pls_psc_bill_amount_3'] = substr($svv['bit48'], 154, 12); 
				$svv['pls_psc_waktu_lunas'] = substr($svv['bit48'], 166, 14);
				switch ($svv['pls_psc_total_bill']) {
					case '1':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_1'];
						break;
					case '2':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_2'];
						break;
					case '3':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_3'];
						break;
				}

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				
				$json['pls_psc_customer_name'] = $svv['pls_psc_customer_name'];
				$json['pls_psc_phone_no'] = $svv['pls_psc_phone_no'];
				$json['pls_psc_no_ref_switching'] = $svv['pls_psc_no_ref_switching'];
				$json['pls_psc_total_bill'] = $svv['pls_psc_total_bill'];
				$json['pls_psc_biaya_admin'] = $svv['pls_psc_biaya_admin'];
				$json['id_log'] = $savev;

				$json['pls_psc_blth_1'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_1'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_1'], 0,1);
				$json['pls_psc_blth_2'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_2'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_2'], 0,1);
				$json['pls_psc_blth_3'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_3'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_3'], 0,1);

				$json['pls_psc_jml_tagihan_1'] = $svv['pls_psc_bill_amount_1'];
				$json['pls_psc_jml_tagihan_2'] = $svv['pls_psc_bill_amount_2'];
				$json['pls_psc_jml_tagihan_3'] = $svv['pls_psc_bill_amount_3'];

				$json['total_tagihan'] = $json['pls_psc_jml_tagihan_1']+$json['pls_psc_jml_tagihan_2']+$json['pls_psc_jml_tagihan_3'];
				$json['total_bayar'] = $json['total_tagihan']+$json['pls_psc_biaya_admin'];
				$json['pls_psc_waktu_lunas'] = $svv['pls_psc_waktu_lunas'];
				$json['status'] = $status;
				$json['header_msg'] = 'Yes';
				/*log res*/
				break;

			case 'Reversal':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '000200';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				/*log req*/
				$sv['type'] = 'Pulsa Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0420';
						$lab['status'] = 'Reversal Repeat';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0420','Reversal Repeat',$id_transaksi);
						//exit();
					}
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'Pulsa Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pls_psc_product_id'] = substr($svv['bit48'], 0, 4); 
				$svv['pls_psc_kode_area'] = substr($svv['bit48'], 4, 4); 
				$svv['pls_psc_phone_no'] = substr($svv['bit48'], 8, 10); 
				$svv['pls_psc_customer_name'] = substr($svv['bit48'], 18, 30); 
				$svv['pls_psc_no_ref_switching'] = substr($svv['bit48'], 48, 32); 
				$svv['pls_psc_total_bill'] = substr($svv['bit48'], 80, 1); 
				$svv['pls_psc_biaya_admin'] = substr($svv['bit48'], 81, 8); 
				$svv['pls_psc_expire'] = substr($svv['bit48'], 89, 8); 
				$svv['pls_psc_bill_ref_1'] = substr($svv['bit48'], 97, 11); 
				$svv['pls_psc_bill_amount_1'] = substr($svv['bit48'], 108, 12); 
				$svv['pls_psc_bill_ref_2'] = substr($svv['bit48'], 120, 11); 
				$svv['pls_psc_bill_amount_2'] = substr($svv['bit48'], 131, 12); 
				$svv['pls_psc_bill_ref_3'] = substr($svv['bit48'], 143, 11); 
				$svv['pls_psc_bill_amount_3'] = substr($svv['bit48'], 154, 12); 
				$svv['pls_psc_waktu_lunas'] = substr($svv['bit48'], 166, 14);
				switch ($svv['pls_psc_total_bill']) {
					case '1':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_1'];
						break;
					case '2':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_2'];
						break;
					case '3':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_3'];
						break;
				}
				
				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				
				$json['pls_psc_customer_name'] = $svv['pls_psc_customer_name'];
				$json['pls_psc_phone_no'] = $svv['pls_psc_phone_no'];
				$json['pls_psc_no_ref_switching'] = $svv['pls_psc_no_ref_switching'];
				$json['pls_psc_total_bill'] = $svv['pls_psc_total_bill'];
				$json['pls_psc_biaya_admin'] = $svv['pls_psc_biaya_admin'];
				$json['id_log'] = $savev;

				$json['pls_psc_blth_1'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_1'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_1'], 0,1);
				$json['pls_psc_blth_2'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_2'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_2'], 0,1);
				$json['pls_psc_blth_3'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_3'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_3'], 0,1);

				$json['pls_psc_jml_tagihan_1'] = $svv['pls_psc_bill_amount_1'];
				$json['pls_psc_jml_tagihan_2'] = $svv['pls_psc_bill_amount_2'];
				$json['pls_psc_jml_tagihan_3'] = $svv['pls_psc_bill_amount_3'];

				$json['total_tagihan'] = $json['pls_psc_jml_tagihan_1']+$json['pls_psc_jml_tagihan_2']+$json['pls_psc_jml_tagihan_3'];
				$json['total_bayar'] = $json['total_tagihan']+$json['pls_psc_biaya_admin'];
				$json['pls_psc_waktu_lunas'] = $svv['pls_psc_waktu_lunas'];
				$json['status'] = $status;
				$json['header_msg'] = 'No';
				/*log res*/
				break;

			case 'Reversal Repeat':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '000300';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				/*log req*/
				$sv['type'] = 'Pulsa Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0420';
						$lab['status'] = 'Reversal Repeat 1';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0420','Reversal Repeat 1',$id_transaksi);
						//exit();
					}
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'Pulsa Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pls_psc_product_id'] = substr($svv['bit48'], 0, 4); 
				$svv['pls_psc_kode_area'] = substr($svv['bit48'], 4, 4); 
				$svv['pls_psc_phone_no'] = substr($svv['bit48'], 8, 10); 
				$svv['pls_psc_customer_name'] = substr($svv['bit48'], 18, 30); 
				$svv['pls_psc_no_ref_switching'] = substr($svv['bit48'], 48, 32); 
				$svv['pls_psc_total_bill'] = substr($svv['bit48'], 80, 1); 
				$svv['pls_psc_biaya_admin'] = substr($svv['bit48'], 81, 8); 
				$svv['pls_psc_expire'] = substr($svv['bit48'], 89, 8); 
				$svv['pls_psc_bill_ref_1'] = substr($svv['bit48'], 97, 11); 
				$svv['pls_psc_bill_amount_1'] = substr($svv['bit48'], 108, 12); 
				$svv['pls_psc_bill_ref_2'] = substr($svv['bit48'], 120, 11); 
				$svv['pls_psc_bill_amount_2'] = substr($svv['bit48'], 131, 12); 
				$svv['pls_psc_bill_ref_3'] = substr($svv['bit48'], 143, 11); 
				$svv['pls_psc_bill_amount_3'] = substr($svv['bit48'], 154, 12); 
				$svv['pls_psc_waktu_lunas'] = substr($svv['bit48'], 166, 14);
				switch ($svv['pls_psc_total_bill']) {
					case '1':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_1'];
						break;
					case '2':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_2'];
						break;
					case '3':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_3'];
						break;
				}
				
				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				
				$json['pls_psc_customer_name'] = $svv['pls_psc_customer_name'];
				$json['pls_psc_phone_no'] = $svv['pls_psc_phone_no'];
				$json['pls_psc_no_ref_switching'] = $svv['pls_psc_no_ref_switching'];
				$json['pls_psc_total_bill'] = $svv['pls_psc_total_bill'];
				$json['pls_psc_biaya_admin'] = $svv['pls_psc_biaya_admin'];
				$json['id_log'] = $savev;
				
				$json['pls_psc_blth_1'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_1'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_1'], 0,1);
				$json['pls_psc_blth_2'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_2'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_2'], 0,1);
				$json['pls_psc_blth_3'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_3'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_3'], 0,1);

				$json['pls_psc_jml_tagihan_1'] = $svv['pls_psc_bill_amount_1'];
				$json['pls_psc_jml_tagihan_2'] = $svv['pls_psc_bill_amount_2'];
				$json['pls_psc_jml_tagihan_3'] = $svv['pls_psc_bill_amount_3'];

				$json['total_tagihan'] = $json['pls_psc_jml_tagihan_1']+$json['pls_psc_jml_tagihan_2']+$json['pls_psc_jml_tagihan_3'];
				$json['total_bayar'] = $json['total_tagihan']+$json['pls_psc_biaya_admin'];
				$json['pls_psc_waktu_lunas'] = $svv['pls_psc_waktu_lunas'];
				$json['status'] = $status;
				$json['header_msg'] = 'No';
				/*log res*/
				break;
			case 'Reversal Repeat 1':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '000300';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				/*log req*/
				$sv['type'] = 'Pulsa Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0420';
						$lab['status'] = 'Reversal Repeat 2';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0420','Reversal Repeat 2',$id_transaksi);
						//exit();
					}
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'Pulsa Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pls_psc_product_id'] = substr($svv['bit48'], 0, 4); 
				$svv['pls_psc_kode_area'] = substr($svv['bit48'], 4, 4); 
				$svv['pls_psc_phone_no'] = substr($svv['bit48'], 8, 10); 
				$svv['pls_psc_customer_name'] = substr($svv['bit48'], 18, 30); 
				$svv['pls_psc_no_ref_switching'] = substr($svv['bit48'], 48, 32); 
				$svv['pls_psc_total_bill'] = substr($svv['bit48'], 80, 1); 
				$svv['pls_psc_biaya_admin'] = substr($svv['bit48'], 81, 8); 
				$svv['pls_psc_expire'] = substr($svv['bit48'], 89, 8); 
				$svv['pls_psc_bill_ref_1'] = substr($svv['bit48'], 97, 11); 
				$svv['pls_psc_bill_amount_1'] = substr($svv['bit48'], 108, 12); 
				$svv['pls_psc_bill_ref_2'] = substr($svv['bit48'], 120, 11); 
				$svv['pls_psc_bill_amount_2'] = substr($svv['bit48'], 131, 12); 
				$svv['pls_psc_bill_ref_3'] = substr($svv['bit48'], 143, 11); 
				$svv['pls_psc_bill_amount_3'] = substr($svv['bit48'], 154, 12); 
				$svv['pls_psc_waktu_lunas'] = substr($svv['bit48'], 166, 14);
				switch ($svv['pls_psc_total_bill']) {
					case '1':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_1'];
						break;
					case '2':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_2'];
						break;
					case '3':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_3'];
						break;
				}
				
				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				
				$json['pls_psc_customer_name'] = $svv['pls_psc_customer_name'];
				$json['pls_psc_phone_no'] = $svv['pls_psc_phone_no'];
				$json['pls_psc_no_ref_switching'] = $svv['pls_psc_no_ref_switching'];
				$json['pls_psc_total_bill'] = $svv['pls_psc_total_bill'];
				$json['pls_psc_biaya_admin'] = $svv['pls_psc_biaya_admin'];
				$json['id_log'] = $savev;
				
				$json['pls_psc_blth_1'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_1'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_1'], 0,1);
				$json['pls_psc_blth_2'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_2'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_2'], 0,1);
				$json['pls_psc_blth_3'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_3'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_3'], 0,1);

				$json['pls_psc_jml_tagihan_1'] = $svv['pls_psc_bill_amount_1'];
				$json['pls_psc_jml_tagihan_2'] = $svv['pls_psc_bill_amount_2'];
				$json['pls_psc_jml_tagihan_3'] = $svv['pls_psc_bill_amount_3'];

				$json['total_tagihan'] = $json['pls_psc_jml_tagihan_1']+$json['pls_psc_jml_tagihan_2']+$json['pls_psc_jml_tagihan_3'];
				$json['total_bayar'] = $json['total_tagihan']+$json['pls_psc_biaya_admin'];
				$json['pls_psc_waktu_lunas'] = $svv['pls_psc_waktu_lunas'];
				$json['status'] = $status;
				$json['header_msg'] = 'No';
				/*log res*/
				break;
			case 'Reversal Repeat 2':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '000300';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				/*log req*/
				$sv['type'] = 'Pulsa Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0';
						$lab['status'] = 'Pending';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						//$json = array();
						$json['39'] = 'Pending';
						return $json;
						exit();
					}
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'Pulsa Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pls_psc_product_id'] = substr($svv['bit48'], 0, 4); 
				$svv['pls_psc_kode_area'] = substr($svv['bit48'], 4, 4); 
				$svv['pls_psc_phone_no'] = substr($svv['bit48'], 8, 10); 
				$svv['pls_psc_customer_name'] = substr($svv['bit48'], 18, 30); 
				$svv['pls_psc_no_ref_switching'] = substr($svv['bit48'], 48, 32); 
				$svv['pls_psc_total_bill'] = substr($svv['bit48'], 80, 1); 
				$svv['pls_psc_biaya_admin'] = substr($svv['bit48'], 81, 8); 
				$svv['pls_psc_expire'] = substr($svv['bit48'], 89, 8); 
				$svv['pls_psc_bill_ref_1'] = substr($svv['bit48'], 97, 11); 
				$svv['pls_psc_bill_amount_1'] = substr($svv['bit48'], 108, 12); 
				$svv['pls_psc_bill_ref_2'] = substr($svv['bit48'], 120, 11); 
				$svv['pls_psc_bill_amount_2'] = substr($svv['bit48'], 131, 12); 
				$svv['pls_psc_bill_ref_3'] = substr($svv['bit48'], 143, 11); 
				$svv['pls_psc_bill_amount_3'] = substr($svv['bit48'], 154, 12); 
				$svv['pls_psc_waktu_lunas'] = substr($svv['bit48'], 166, 14);
				switch ($svv['pls_psc_total_bill']) {
					case '1':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_1'];
						break;
					case '2':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_2'];
						break;
					case '3':
						$json['jml_tagihan'] = $svv['pls_psc_bill_amount_3'];
						break;
				}
				
				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				
				$json['pls_psc_customer_name'] = $svv['pls_psc_customer_name'];
				$json['pls_psc_phone_no'] = $svv['pls_psc_phone_no'];
				$json['pls_psc_no_ref_switching'] = $svv['pls_psc_no_ref_switching'];
				$json['pls_psc_total_bill'] = $svv['pls_psc_total_bill'];
				$json['pls_psc_biaya_admin'] = $svv['pls_psc_biaya_admin'];
				$json['id_log'] = $savev;
				
				$json['pls_psc_blth_1'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_1'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_1'], 0,1);
				$json['pls_psc_blth_2'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_2'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_2'], 0,1);
				$json['pls_psc_blth_3'] = Esta::date_indo(substr($svv['pls_psc_bill_ref_3'], 1,2),'singkat').' 201'.substr($svv['pls_psc_bill_ref_3'], 0,1);

				$json['pls_psc_jml_tagihan_1'] = $svv['pls_psc_bill_amount_1'];
				$json['pls_psc_jml_tagihan_2'] = $svv['pls_psc_bill_amount_2'];
				$json['pls_psc_jml_tagihan_3'] = $svv['pls_psc_bill_amount_3'];

				$json['total_tagihan'] = $json['pls_psc_jml_tagihan_1']+$json['pls_psc_jml_tagihan_2']+$json['pls_psc_jml_tagihan_3'];
				$json['total_bayar'] = $json['total_tagihan']+$json['pls_psc_biaya_admin'];
				$json['pls_psc_waktu_lunas'] = $svv['pls_psc_waktu_lunas'];
				$json['status'] = $status;
				$json['header_msg'] = 'No';
				/*log res*/
				break;
		}

		

		if ($err) {
		  	return $err;
		} else {

			if($json['39'] == '18' || $json['39'] == '06') {

				if($status == 'Payment') {
					$lab['created_at'] = date('Y-m-d H:i:s');
					$lab['mti'] = '0400';
					$lab['status'] = 'Reversal';
					$lab['id_log'] = $id_transaksi;
					$lab['id_product'] = $id_product;
					$lab['json'] = $status;

					$lab['created_user'] = Esta::user('0');
					$lab['updated_user'] = Esta::user('0');
					$sv_lab = DB::table('log_abnormal')
						->insert($lab);
					return Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0400','Reversal',$id_transaksi);
				} else {
					if($status == 'Reversal') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0420';
						$lab['status'] = 'Reversal Repeat';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0420','Reversal Repeat',$id_transaksi);
					} else {
						if($status == 'Reversal Repeat') {
							$lab['created_at'] = date('Y-m-d H:i:s');
							$lab['mti'] = '0420';
							$lab['status'] = 'Reversal Repeat 1';
							$lab['id_log'] = $id_transaksi;
							$lab['id_product'] = $id_product;
							$lab['json'] = $errr;

							$lab['created_user'] = Esta::user('0');
							$lab['updated_user'] = Esta::user('0');
							$sv_lab = DB::table('log_abnormal')
								->insert($lab);
							return Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0420','Reversal Repeat 1',$id_transaksi);
						} else {
							if($status == 'Reversal Repeat 1') {
								$lab['created_at'] = date('Y-m-d H:i:s');
								$lab['mti'] = '0420';
								$lab['status'] = 'Reversal Repeat 2';
								$lab['id_log'] = $id_transaksi;
								$lab['id_product'] = $id_product;
								$lab['json'] = $errr;

								$lab['created_user'] = Esta::user('0');
								$lab['updated_user'] = Esta::user('0');
								$sv_lab = DB::table('log_abnormal')
									->insert($lab);
								return Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0420','Reversal Repeat 2',$id_transaksi);
							} else {
								if($status == 'Reversal Repeat 2') {
									$lab['created_at'] = date('Y-m-d H:i:s');
									$lab['mti'] = '0';
									$lab['status'] = 'Pending';
									$lab['id_log'] = $id_transaksi;
									$lab['id_product'] = $id_product;
									$lab['json'] = $errr;

									$lab['created_user'] = Esta::user('0');
									$lab['updated_user'] = Esta::user('0');
									$sv_lab = DB::table('log_abnormal')
										->insert($lab);
									$json['39'] = 'Pending';
									return $json;
								} else {
									return $json;
								}
							}
						}
					}
				}
			} else {
				return $json;
			}

		}
	}

	public static function send_iso_pdam($id_product,$no_hp,$mti,$status,$id_transaksi,$no_meter) {
		$product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$kode_biller = DB::table('pan_kode_biller')
			->where('id',$product->id_kode_biller)
			->first()->kode;

		$kode_layanan = DB::table('pan_kode_layanan')
			->where('id',$product->id_kode_layanan)
			->first()->kode;

		$tipe_layanan = DB::table('pan_tipe_layanan')
			->where('id',$product->id_tipe_layanan)
			->first()->kode;

		$detail_inquiry = DB::table('log_jatelindo_bit')
			->where('id',$id_transaksi)
			->where('type','PDAM Postpaid')
			->where('status','Inquiry')
			->where('jenis','res')
			->first();

		//$kode_transaksi = Esta::nomor_transaksi('trans_pdam',CRUDBooster::getsetting('transaksi_pdam'));
		

		switch ($status) {
			case 'Inquiry':
				$bit2 = $tipe_layanan.$kode_layanan.$kode_biller;/*fix*/
				//echo $bit2;
				//exit();
				//$bit48 = "'".$product->product_id."'"; /*private data.*/
				$bit3 =  '380000'; /*Processing Code fix 380000 : inquiry, 170000/171000/172000 : payment/Purchase/advice, 000200/000300 : reversal/reversal repeat*/
				$bit4 = 0; /*amount trx*/
				$bit7 = date('mdhms'); /*Transmission date/time MMDDhhmmss*/
				$bit11 = date('sd').rand(11,99); /*STAN unik*/
				$bit12 = date('hms'); /*Local Time hhmmss*/
				$bit13 = date('md'); /*Local Date MMDD*/
				$bit15 = date('md'); /*Settlement Date kapan transaksi akan disettle. (MMDD)*/
				$bit18 = CRUDBooster::getsetting('multibiller_bit_18'); /*Merchant Type. klasifikasi tipe merchant untuk setiap bisnis produk layanan*/
				$bit32 = CRUDBooster::getsetting('multibiller_bit_32'); /*Acquiring Institution ID/Banking Code Nilai ini unik untuk setiap institusi. Untuk bangking nilai ini mengacu pada kode bank Indonesia.*/
				//$bit39 = '00'; /*Response Code*/
				$bit41 = CRUDBooster::getsetting('multibiller_bit_41'); /*Terminal ID. ID ini diserahkan pada setiap pihak acquirer sebagai keterangan untuk setiap pesan.*/
				$bit42 = CRUDBooster::getsetting('multibiller_bit_42'); /*Acceptor ID. diberikan oleh pihak jatelindo untuk setiap acquirer*/
				//$bit48 = 'JTL53L3149876543210000000000000';
				$length_no_meter = strlen($no_meter);
				$spasi_no_meter = 15-$length_no_meter;
				for ($i=0; $i < $spasi_no_meter ; $i++) { 
					$view_spasi_no_meter .= ' ';
				}
				$view_no_meter = $no_meter.$view_spasi_no_meter;
				$bit48 = ($length_no_meter > 16 ? '0000000000000000' : $view_no_meter); /*Private Data. 4 Product ID. 4 kode operator. 10 Phone Number*/
				$bit49 = '360'; /*Transaction Currency Code*/
				$bit61 = '1'; /*Private Use*/
				//$mti = '0200'; 
				$bit62 = '';
				$bit63 = 'Esta';//sprintf("%32s\n", CRUDBooster::getsetting('pln_kode_loket')).sprintf("%30s\n", CRUDBooster::getsetting('pln_nama_loket')).sprintf("%50s\n", CRUDBooster::getsetting('pln_alamat_loket')).sprintf("%18s\n", CRUDBooster::getsetting('pln_no_telp_loket'));
				/*Pesan ini otomatis dikirimkan jika dan hanya jika purchase tidak mendapat response atau mendapat response code 18.*/
				
				/*log req*/
				$sv['type'] = 'PDAM Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$kode_transaksi = Esta::nomor_transaksi_ppobnew('log_jatelindo_bit',CRUDBooster::getsetting('transaksi_pdam'),'PDAM Postpaid');
				$bit37 = $kode_transaksi; /*Retrieval Reference Number. nomor referensi yang dipergunakan oleh system untuk mengacu pada transaksi tersebut.*/
				$sv['bit37'] = $bit37;
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/
				
				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-inquiry';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				/*log res*/
				$svv['type'] = 'PDAM Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pdam_idpel'] = substr($svv['bit48'], 0, 15); 
				$svv['pdam_blth'] = substr($svv['bit48'], 15, 12); 
				$svv['pdam_customer_name'] = substr($svv['bit48'], 27, 30); 
				$svv['pdam_bill_count'] = substr($svv['bit48'], 57, 2); 
				$svv['pdam_bill_repeat_count'] = substr($svv['bit48'], 59, 2); 
				$svv['pdam_rupiah_tagihan'] = substr($svv['bit48'], 61, 12); 
				$svv['pdam_biaya_admin'] = substr($svv['bit48'], 73, 8); 
				$svv['pdam_bill_date_1'] = substr($svv['bit48'], 81, 6); 
				$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 87, 12); 
				$svv['pdam_penalty_1'] = substr($svv['bit48'], 99, 8); 
				$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 107, 17); 
				$svv['pdam_bill_date_2'] = substr($svv['bit48'], 124, 6); 
				$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 130, 12); 
				$svv['pdam_penalty_2'] = substr($svv['bit48'], 142, 8); 
				$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 150, 17); 
				$svv['pdam_bill_date_3'] = substr($svv['bit48'], 167, 6); 
				$svv['pdam_bill_amount_3'] = substr($svv['bit48'], 173, 12); 
				$svv['pdam_penalty_3'] = substr($svv['bit48'], 185, 8); 
				$svv['pdam_kubikasi_3'] = substr($svv['bit48'], 193, 17); 
				$svv['pdam_bill_date_4'] = substr($svv['bit48'], 210, 6); 
				$svv['pdam_bill_amount_4'] = substr($svv['bit48'], 216, 12); 
				$svv['pdam_penalty_4'] = substr($svv['bit48'], 228, 8); 
				$svv['pdam_kubikasi_4'] = substr($svv['bit48'], 236, 17); 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['pdam_idpel'] = $svv['pdam_idpel'];
				$json['pdam_customer_name'] = $svv['pdam_customer_name'];
				$json['pdam_rupiah_tagihan'] = $svv['pdam_rupiah_tagihan'];
				$json['pdam_biaya_admin'] = $svv['pdam_biaya_admin'];
				$bln_min = substr($svv['pdam_blth'], 0, 6);
				$bln_max = substr($svv['pdam_blth'], 6, 6);

				$bln_min_view = Esta::date_indo(substr($bln_min, 4, 2),'singkat').' '.substr($bln_min, 0,4);
				$bln_max_view = Esta::date_indo(substr($bln_max, 4, 2),'singkat').' '.substr($bln_max, 0,4);
				$json['pdam_blth'] = $bln_min_view.' - '.$bln_max_view;
				$json['jml_lembar_tagihan'] = $svv['pdam_bill_count'];

				/*$y = substr($svv['pdam_waktu_lunas'], 0,4);
				$m = substr($svv['pdam_waktu_lunas'], 4,2);
				$d = substr($svv['pdam_waktu_lunas'], 6,2);
				$h = substr($svv['pdam_waktu_lunas'], 8,2);
				$i = substr($svv['pdam_waktu_lunas'], 10,2);
				$json['pdam_waktu_lunas'] = $y.'-'.$m.'-'.$d.' '.$h.':'.$i;*/
				$json['id_log'] = $savev;
				break;
			case 'Payment':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '170000';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				/*log req*/
				$sv['type'] = 'PDAM Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);

				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0400';
						$lab['status'] = 'Reversal';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pdam($id_product,$no_hp,'0400','Reversal',$id_transaksi, $no_meter);
					}
				}

				curl_close($curl);

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				/*log res*/
				$svv['type'] = 'PDAM Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}


				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				/*log res*/
				$svv['pdam_idpel'] = substr($svv['bit48'], 0, 15); 
				$svv['pdam_blth'] = substr($svv['bit48'], 15, 12); 
				$svv['pdam_customer_name'] = substr($svv['bit48'], 27, 30); 
				$svv['pdam_bill_count'] = substr($svv['bit48'], 57, 2); 
				$svv['pdam_bill_repeat_count'] = substr($svv['bit48'], 59, 2); 
				$svv['pdam_no_ref_biller'] = substr($svv['bit48'], 61, 15); 
				$svv['pdam_no_ref_switching'] = substr($svv['bit48'], 76, 32); 
				$svv['pdam_rupiah_tagihan'] = substr($svv['bit48'], 108, 12); 
				$svv['pdam_biaya_admin'] = substr($svv['bit48'], 120, 8); 

				$repeat = $svv['pdam_bill_repeat_count'];
				if($repeat == '01') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 171, 14); 

					$svv['pdam_bill_date_2'] = 0; 
					$svv['pdam_bill_amount_2'] = 0;
					$svv['pdam_penalty_2'] = 0;
					$svv['pdam_kubikasi_2'] = 0;

					$svv['pdam_bill_date_3'] = 0; 
					$svv['pdam_bill_amount_3'] = 0;
					$svv['pdam_penalty_3'] = 0;
					$svv['pdam_kubikasi_3'] = 0;

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} elseif($repeat == '02') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 214, 14); 

					$svv['pdam_bill_date_3'] = 0; 
					$svv['pdam_bill_amount_3'] = 0;
					$svv['pdam_penalty_3'] = 0;
					$svv['pdam_kubikasi_3'] = 0;

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} elseif($repeat == '03') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_bill_date_3'] = substr($svv['bit48'], 214, 6); 
					$svv['pdam_bill_amount_3'] = substr($svv['bit48'], 220, 12); 
					$svv['pdam_penalty_3'] = substr($svv['bit48'], 232, 8); 
					$svv['pdam_kubikasi_3'] = substr($svv['bit48'], 240, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 257, 14); 

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} else {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_bill_date_3'] = substr($svv['bit48'], 214, 6); 
					$svv['pdam_bill_amount_3'] = substr($svv['bit48'], 220, 12); 
					$svv['pdam_penalty_3'] = substr($svv['bit48'], 232, 8); 
					$svv['pdam_kubikasi_3'] = substr($svv['bit48'], 240, 17); 
					$svv['pdam_bill_date_4'] = substr($svv['bit48'], 257, 6); 
					$svv['pdam_bill_amount_4'] = substr($svv['bit48'], 263, 12); 
					$svv['pdam_penalty_4'] = substr($svv['bit48'], 275, 8); 
					$svv['pdam_kubikasi_4'] = substr($svv['bit48'], 283, 17); 
					
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 300, 14); 

				}

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['pdam_idpel'] = $svv['pdam_idpel'];
				$json['pdam_blth'] = $svv['pdam_blth'];
				$json['pdam_no_ref_biller'] = $svv['pdam_no_ref_switching'];
				$json['pdam_customer_name'] = $svv['pdam_customer_name'];
				$json['pdam_rupiah_tagihan'] = $svv['pdam_rupiah_tagihan'];
				$json['pdam_biaya_admin'] = $svv['pdam_biaya_admin'];
				$bln_min = substr($svv['pdam_blth'], 0, 6);
				$bln_max = substr($svv['pdam_blth'], 6, 6);
				$bln_min_view = Esta::date_indo(substr($bln_min, 4, 2),'singkat').' '.substr($bln_min, 0,4);
				$bln_max_view = Esta::date_indo(substr($bln_max, 4, 2),'singkat').' '.substr($bln_max, 0,4);
				$json['pdam_blth'] = $bln_min_view.' - '.$bln_max_view;
				$json['jml_lembar_tagihan'] = $svv['pdam_bill_count'];

				$y = substr($svv['pdam_waktu_lunas'], 0,4);
				$m = substr($svv['pdam_waktu_lunas'], 4,2);
				$d = substr($svv['pdam_waktu_lunas'], 6,2);
				$h = substr($svv['pdam_waktu_lunas'], 8,2);
				$i = substr($svv['pdam_waktu_lunas'], 10,2);
				$json['pdam_waktu_lunas'] = $y.'-'.$m.'-'.$d.' '.$h.':'.$i;
				$json['status'] = 'Payment';
				$json['id_log'] = $savev;
				break;

			case 'Reversal':
				$detail_payment = DB::table('log_jatelindo_bit')
					->where('id_transaksi',$detail_inquiry->id)
					->where('type','PDAM Postpaid')
					->where('status','Payment')
					->where('jenis','res')
					->first();

				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '000200';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				/*log req*/
				$sv['type'] = 'PDAM Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0420';
						$lab['status'] = 'Reversal Repeat';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pdam($id_product,$no_hp,'0420','Reversal Repeat',$id_transaksi, $no_meter);
					}
				}

				curl_close($curl);

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				/*log res*/
				$svv['type'] = 'PDAM Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				/*echo $svv['bit48'];
				exit();*/
				/*log res*/
				$svv['pdam_idpel'] = substr($svv['bit48'], 0, 15); 
				$svv['pdam_blth'] = substr($svv['bit48'], 15, 12); 
				$svv['pdam_customer_name'] = substr($svv['bit48'], 27, 30); 
				$svv['pdam_bill_count'] = substr($svv['bit48'], 57, 2); 
				$svv['pdam_bill_repeat_count'] = substr($svv['bit48'], 59, 2); 
				$svv['pdam_no_ref_biller'] = substr($svv['bit48'], 61, 15); 
				$svv['pdam_no_ref_switching'] = substr($svv['bit48'], 76, 32); 
				$svv['pdam_rupiah_tagihan'] = substr($svv['bit48'], 108, 12); 
				$svv['pdam_biaya_admin'] = substr($svv['bit48'], 120, 8); 

				$repeat = $svv['pdam_bill_repeat_count'];
				if($repeat == '01') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 171, 14); 

					$svv['pdam_bill_date_2'] = 0; 
					$svv['pdam_bill_amount_2'] = 0;
					$svv['pdam_penalty_2'] = 0;
					$svv['pdam_kubikasi_2'] = 0;

					$svv['pdam_bill_date_3'] = 0; 
					$svv['pdam_bill_amount_3'] = 0;
					$svv['pdam_penalty_3'] = 0;
					$svv['pdam_kubikasi_3'] = 0;

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} elseif($repeat == '02') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 214, 14); 

					$svv['pdam_bill_date_3'] = 0; 
					$svv['pdam_bill_amount_3'] = 0;
					$svv['pdam_penalty_3'] = 0;
					$svv['pdam_kubikasi_3'] = 0;

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} elseif($repeat == '03') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_bill_date_3'] = substr($svv['bit48'], 214, 6); 
					$svv['pdam_bill_amount_3'] = substr($svv['bit48'], 220, 12); 
					$svv['pdam_penalty_3'] = substr($svv['bit48'], 232, 8); 
					$svv['pdam_kubikasi_3'] = substr($svv['bit48'], 240, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 257, 14); 

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} else {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_bill_date_3'] = substr($svv['bit48'], 214, 6); 
					$svv['pdam_bill_amount_3'] = substr($svv['bit48'], 220, 12); 
					$svv['pdam_penalty_3'] = substr($svv['bit48'], 232, 8); 
					$svv['pdam_kubikasi_3'] = substr($svv['bit48'], 240, 17); 
					$svv['pdam_bill_date_4'] = substr($svv['bit48'], 257, 6); 
					$svv['pdam_bill_amount_4'] = substr($svv['bit48'], 263, 12); 
					$svv['pdam_penalty_4'] = substr($svv['bit48'], 275, 8); 
					$svv['pdam_kubikasi_4'] = substr($svv['bit48'], 283, 17); 
					
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 300, 14); 

				} 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['pdam_idpel'] = $svv['pdam_idpel'];
				$json['pdam_blth'] = $svv['pdam_blth'];
				$json['pdam_no_ref_biller'] = $svv['pdam_no_ref_switching'];
				$json['pdam_customer_name'] = $svv['pdam_customer_name'];
				$json['pdam_rupiah_tagihan'] = $svv['pdam_rupiah_tagihan'];
				$json['pdam_biaya_admin'] = $svv['pdam_biaya_admin'];
				$bln_min = substr($svv['pdam_blth'], 0, 6);
				$bln_max = substr($svv['pdam_blth'], 6, 6);
				$bln_min_view = Esta::date_indo(substr($bln_min, 4, 2),'singkat').' '.substr($bln_min, 0,4);
				$bln_max_view = Esta::date_indo(substr($bln_max, 4, 2),'singkat').' '.substr($bln_max, 0,4);
				$json['pdam_blth'] = $bln_min_view.' - '.$bln_max_view;
				$json['jml_lembar_tagihan'] = $svv['pdam_bill_count'];

				$y = substr($svv['pdam_waktu_lunas'], 0,4);
				$m = substr($svv['pdam_waktu_lunas'], 4,2);
				$d = substr($svv['pdam_waktu_lunas'], 6,2);
				$h = substr($svv['pdam_waktu_lunas'], 8,2);
				$i = substr($svv['pdam_waktu_lunas'], 10,2);
				$json['pdam_waktu_lunas'] = $y.'-'.$m.'-'.$d.' '.$h.':'.$i;
				$json['status'] = 'Reversal';
				$json['id_log'] = $savev;
				break;

			case 'Reversal Repeat':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '000300';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				/*log req*/
				$sv['type'] = 'PDAM Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0420';
						$lab['status'] = 'Reversal Repeat 1';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pdam($id_product,$no_hp,'0420','Reversal Repeat 1',$id_transaksi, $no_meter);
					}
				}

				curl_close($curl);

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				/*log res*/
				$svv['type'] = 'PDAM Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				/*log res*/
				$svv['pdam_idpel'] = substr($svv['bit48'], 0, 15); 
				$svv['pdam_blth'] = substr($svv['bit48'], 15, 12); 
				$svv['pdam_customer_name'] = substr($svv['bit48'], 27, 30); 
				$svv['pdam_bill_count'] = substr($svv['bit48'], 57, 2); 
				$svv['pdam_bill_repeat_count'] = substr($svv['bit48'], 59, 2); 
				$svv['pdam_no_ref_biller'] = substr($svv['bit48'], 61, 15); 
				$svv['pdam_no_ref_switching'] = substr($svv['bit48'], 76, 32); 
				$svv['pdam_rupiah_tagihan'] = substr($svv['bit48'], 108, 12); 
				$svv['pdam_biaya_admin'] = substr($svv['bit48'], 120, 8); 

				$repeat = $svv['pdam_bill_repeat_count'];
				if($repeat == '01') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 171, 14); 

					$svv['pdam_bill_date_2'] = 0; 
					$svv['pdam_bill_amount_2'] = 0;
					$svv['pdam_penalty_2'] = 0;
					$svv['pdam_kubikasi_2'] = 0;

					$svv['pdam_bill_date_3'] = 0; 
					$svv['pdam_bill_amount_3'] = 0;
					$svv['pdam_penalty_3'] = 0;
					$svv['pdam_kubikasi_3'] = 0;

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} elseif($repeat == '02') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 214, 14); 

					$svv['pdam_bill_date_3'] = 0; 
					$svv['pdam_bill_amount_3'] = 0;
					$svv['pdam_penalty_3'] = 0;
					$svv['pdam_kubikasi_3'] = 0;

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} elseif($repeat == '03') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_bill_date_3'] = substr($svv['bit48'], 214, 6); 
					$svv['pdam_bill_amount_3'] = substr($svv['bit48'], 220, 12); 
					$svv['pdam_penalty_3'] = substr($svv['bit48'], 232, 8); 
					$svv['pdam_kubikasi_3'] = substr($svv['bit48'], 240, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 257, 14); 

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} else {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_bill_date_3'] = substr($svv['bit48'], 214, 6); 
					$svv['pdam_bill_amount_3'] = substr($svv['bit48'], 220, 12); 
					$svv['pdam_penalty_3'] = substr($svv['bit48'], 232, 8); 
					$svv['pdam_kubikasi_3'] = substr($svv['bit48'], 240, 17); 
					$svv['pdam_bill_date_4'] = substr($svv['bit48'], 257, 6); 
					$svv['pdam_bill_amount_4'] = substr($svv['bit48'], 263, 12); 
					$svv['pdam_penalty_4'] = substr($svv['bit48'], 275, 8); 
					$svv['pdam_kubikasi_4'] = substr($svv['bit48'], 283, 17); 
					
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 300, 14); 

				} 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['pdam_idpel'] = $svv['pdam_idpel'];
				$json['pdam_blth'] = $svv['pdam_blth'];
				$json['pdam_no_ref_biller'] = $svv['pdam_no_ref_switching'];
				$json['pdam_customer_name'] = $svv['pdam_customer_name'];
				$json['pdam_rupiah_tagihan'] = $svv['pdam_rupiah_tagihan'];
				$json['pdam_biaya_admin'] = $svv['pdam_biaya_admin'];
				$bln_min = substr($svv['pdam_blth'], 0, 6);
				$bln_max = substr($svv['pdam_blth'], 6, 6);
				$bln_min_view = Esta::date_indo(substr($bln_min, 4, 2),'singkat').' '.substr($bln_min, 0,4);
				$bln_max_view = Esta::date_indo(substr($bln_max, 4, 2),'singkat').' '.substr($bln_max, 0,4);
				$json['pdam_blth'] = $bln_min_view.' - '.$bln_max_view;
				$json['jml_lembar_tagihan'] = $svv['pdam_bill_count'];

				$y = substr($svv['pdam_waktu_lunas'], 0,4);
				$m = substr($svv['pdam_waktu_lunas'], 4,2);
				$d = substr($svv['pdam_waktu_lunas'], 6,2);
				$h = substr($svv['pdam_waktu_lunas'], 8,2);
				$i = substr($svv['pdam_waktu_lunas'], 10,2);
				$json['pdam_waktu_lunas'] = $y.'-'.$m.'-'.$d.' '.$h.':'.$i;
				$json['status'] = 'Reversal Repeat';
				$json['id_log'] = $savev;
				break;

			case 'Reversal Repeat 1':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '000300';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = $detail_inquiry->bit63;
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				/*log req*/
				$sv['type'] = 'PDAM Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0420';
						$lab['status'] = 'Reversal Repeat';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						$json['39'] = 'Pending';
						return $json;
						exit();
					}
				}

				curl_close($curl);

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				/*log res*/
				$svv['type'] = 'PDAM Postpaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"90\":\"".$bit90."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				/*log res*/
				$svv['pdam_idpel'] = substr($svv['bit48'], 0, 15); 
				$svv['pdam_blth'] = substr($svv['bit48'], 15, 12); 
				$svv['pdam_customer_name'] = substr($svv['bit48'], 27, 30); 
				$svv['pdam_bill_count'] = substr($svv['bit48'], 57, 2); 
				$svv['pdam_bill_repeat_count'] = substr($svv['bit48'], 59, 2); 
				$svv['pdam_no_ref_biller'] = substr($svv['bit48'], 61, 15); 
				$svv['pdam_no_ref_switching'] = substr($svv['bit48'], 76, 32); 
				$svv['pdam_rupiah_tagihan'] = substr($svv['bit48'], 108, 12); 
				$svv['pdam_biaya_admin'] = substr($svv['bit48'], 120, 8); 

				$repeat = $svv['pdam_bill_repeat_count'];
				if($repeat == '01') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 171, 14); 

					$svv['pdam_bill_date_2'] = 0; 
					$svv['pdam_bill_amount_2'] = 0;
					$svv['pdam_penalty_2'] = 0;
					$svv['pdam_kubikasi_2'] = 0;

					$svv['pdam_bill_date_3'] = 0; 
					$svv['pdam_bill_amount_3'] = 0;
					$svv['pdam_penalty_3'] = 0;
					$svv['pdam_kubikasi_3'] = 0;

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} elseif($repeat == '02') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 214, 14); 

					$svv['pdam_bill_date_3'] = 0; 
					$svv['pdam_bill_amount_3'] = 0;
					$svv['pdam_penalty_3'] = 0;
					$svv['pdam_kubikasi_3'] = 0;

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} elseif($repeat == '03') {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_bill_date_3'] = substr($svv['bit48'], 214, 6); 
					$svv['pdam_bill_amount_3'] = substr($svv['bit48'], 220, 12); 
					$svv['pdam_penalty_3'] = substr($svv['bit48'], 232, 8); 
					$svv['pdam_kubikasi_3'] = substr($svv['bit48'], 240, 17); 
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 257, 14); 

					$svv['pdam_bill_date_4'] = 0; 
					$svv['pdam_bill_amount_4'] = 0;
					$svv['pdam_penalty_4'] = 0;
					$svv['pdam_kubikasi_4'] = 0;

				} else {
					$svv['pdam_bill_date_1'] = substr($svv['bit48'], 128, 6); 
					$svv['pdam_bill_amount_1'] = substr($svv['bit48'], 134, 12); 
					$svv['pdam_penalty_1'] = substr($svv['bit48'], 146, 8);
					$svv['pdam_kubikasi_1'] = substr($svv['bit48'], 154, 17); 
					$svv['pdam_bill_date_2'] = substr($svv['bit48'], 171, 6); 
					$svv['pdam_bill_amount_2'] = substr($svv['bit48'], 177, 12); 
					$svv['pdam_penalty_2'] = substr($svv['bit48'], 189, 8); 
					$svv['pdam_kubikasi_2'] = substr($svv['bit48'], 197, 17); 
					$svv['pdam_bill_date_3'] = substr($svv['bit48'], 214, 6); 
					$svv['pdam_bill_amount_3'] = substr($svv['bit48'], 220, 12); 
					$svv['pdam_penalty_3'] = substr($svv['bit48'], 232, 8); 
					$svv['pdam_kubikasi_3'] = substr($svv['bit48'], 240, 17); 
					$svv['pdam_bill_date_4'] = substr($svv['bit48'], 257, 6); 
					$svv['pdam_bill_amount_4'] = substr($svv['bit48'], 263, 12); 
					$svv['pdam_penalty_4'] = substr($svv['bit48'], 275, 8); 
					$svv['pdam_kubikasi_4'] = substr($svv['bit48'], 283, 17); 
					
					$svv['pdam_waktu_lunas'] = substr($svv['bit48'], 300, 14); 

				} 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['pdam_idpel'] = $svv['pdam_idpel'];
				$json['pdam_blth'] = $svv['pdam_blth'];
				$json['pdam_no_ref_biller'] = $svv['pdam_no_ref_switching'];
				$json['pdam_customer_name'] = $svv['pdam_customer_name'];
				$json['pdam_rupiah_tagihan'] = $svv['pdam_rupiah_tagihan'];
				$json['pdam_biaya_admin'] = $svv['pdam_biaya_admin'];
				$bln_min = substr($svv['pdam_blth'], 0, 6);
				$bln_max = substr($svv['pdam_blth'], 6, 6);
				$bln_min_view = Esta::date_indo(substr($bln_min, 4, 2),'singkat').' '.substr($bln_min, 0,4);
				$bln_max_view = Esta::date_indo(substr($bln_max, 4, 2),'singkat').' '.substr($bln_max, 0,4);
				$json['pdam_blth'] = $bln_min_view.' - '.$bln_max_view;
				$json['jml_lembar_tagihan'] = $svv['pdam_bill_count'];

				$y = substr($svv['pdam_waktu_lunas'], 0,4);
				$m = substr($svv['pdam_waktu_lunas'], 4,2);
				$d = substr($svv['pdam_waktu_lunas'], 6,2);
				$h = substr($svv['pdam_waktu_lunas'], 8,2);
				$i = substr($svv['pdam_waktu_lunas'], 10,2);
				$json['pdam_waktu_lunas'] = $y.'-'.$m.'-'.$d.' '.$h.':'.$i;
				$json['status'] = 'Reversal Repeat 1';
				$json['id_log'] = $savev;
				break;
		}

		if ($err) {
		  	return $err;
		} else {

			if($json['39'] == '18' || $json['39'] == '06') {

				if($status == 'Payment') {
					$lab['created_at'] = date('Y-m-d H:i:s');
					$lab['mti'] = '0400';
					$lab['status'] = 'Reversal';
					$lab['id_log'] = $id_transaksi;
					$lab['id_product'] = $id_product;
					$lab['json'] = $status;

					$lab['created_user'] = Esta::user('0');
					$lab['updated_user'] = Esta::user('0');
					$sv_lab = DB::table('log_abnormal')
						->insert($lab);
					Esta::send_iso_pdam($id_product,$no_hp,'0400','Reversal',$id_transaksi, $no_meter);
				} else {
					if($status == 'Reversal') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0420';
						$lab['status'] = 'Reversal Repeat';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						Esta::send_iso_pdam($id_product,$no_hp,'0420','Reversal Repeat',$id_transaksi, $no_meter);
					} else {
						if($status == 'Reversal Repeat') {
							$lab['created_at'] = date('Y-m-d H:i:s');
							$lab['mti'] = '0420';
							$lab['status'] = 'Reversal Repeat 1';
							$lab['id_log'] = $id_transaksi;
							$lab['id_product'] = $id_product;
							$lab['json'] = $errr;

							$lab['created_user'] = Esta::user('0');
							$lab['updated_user'] = Esta::user('0');
							$sv_lab = DB::table('log_abnormal')
								->insert($lab);
							Esta::send_iso_pdam($id_product,$no_hp,'0420','Reversal Repeat 1',$id_transaksi, $no_meter);
						} else {
							if($status == 'Reversal Repeat 1') {
								$lab['created_at'] = date('Y-m-d H:i:s');
								$lab['mti'] = '0';
								$lab['status'] = 'Pending';
								$lab['id_log'] = $id_transaksi;
								$lab['id_product'] = $id_product;
								$lab['json'] = $errr;

								$lab['created_user'] = Esta::user('0');
								$lab['updated_user'] = Esta::user('0');
								$sv_lab = DB::table('log_abnormal')
									->insert($lab);
								$json['39'] = 'Pending';
								$return = $json;
							} else {
								$return = $json;
							}
						}
					}
				}
			} else {
				$return = $json;
			}

		}

		return $json;
		//print_r($json['39']);
		//exit();
	}

	public static function send_iso_bpjsks($id_product,$no_hp,$mti,$status,$id_transaksi,$id_pelanggan,$jml_bulan) {
		$product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$kode_biller = DB::table('pan_kode_biller')
			->where('id',$product->id_kode_biller)
			->first()->kode;

		$kode_layanan = DB::table('pan_kode_layanan')
			->where('id',$product->id_kode_layanan)
			->first()->kode;

		$tipe_layanan = DB::table('pan_tipe_layanan')
			->where('id',$product->id_tipe_layanan)
			->first()->kode;

		$detail_transaksi = DB::table('trans_bpjs')
			->where('id',$id_transaksi)
			->first();

		$detail_inquiry = DB::table('log_jatelindo_bit')
			->where('id',$id_transaksi)
			->where('type','BPJS Kesehatan')
			->where('status','Inquiry')
			->where('jenis','res')
			->first();

		//$kode_transaksi = Esta::nomor_transaksi('trans_bpjs',CRUDBooster::getsetting('transaksi_bpjs'));
		

		switch ($status) {
			case 'Inquiry':
				$bit2 = '134003';/*fix*/
				//echo $bit2;
				//exit();
				//$bit48 = "'".$product->product_id."'"; /*private data.*/
				$bit3 =  '380000'; /*Processing Code fix 380000 : inquiry, 170000/171000/172000 : payment/Purchase/advice, 000200/000300 : reversal/reversal repeat*/
				$bit4 = 0; /*amount trx*/
				$bit7 = date('mdhms'); /*Transmission date/time MMDDhhmmss*/
				$bit11 = date('sd').rand(11,99); /*STAN unik*/
				$bit12 = date('hms'); /*Local Time hhmmss*/
				$bit13 = date('md'); /*Local Date MMDD*/
				$bit15 = date('md'); /*Settlement Date kapan transaksi akan disettle. (MMDD)*/
				$bit18 = CRUDBooster::getsetting('multibiller_bit_18'); /*Merchant Type. klasifikasi tipe merchant untuk setiap bisnis produk layanan*/
				$bit32 = CRUDBooster::getsetting('multibiller_bit_32'); /*Acquiring Institution ID/Banking Code Nilai ini unik untuk setiap institusi. Untuk bangking nilai ini mengacu pada kode bank Indonesia.*/
				//$bit39 = '00'; /*Response Code*/
				$bit41 = CRUDBooster::getsetting('multibiller_bit_41'); /*Terminal ID. ID ini diserahkan pada setiap pihak acquirer sebagai keterangan untuk setiap pesan.*/
				$bit42 = CRUDBooster::getsetting('multibiller_bit_42'); /*Acceptor ID. diberikan oleh pihak jatelindo untuk setiap acquirer*/
				//$bit48 = 'JTL53L3149876543210000000000000';
				$bit48 = Esta::left_padding($id_pelanggan,'16').sprintf("%02s", $jml_bulan); /*Private Data. 4 Product ID. 4 kode operator. 10 Phone Number*/
				$bit49 = '360'; /*Transaction Currency Code*/
				$bit61 = '1'; /*Private Use*/
				//$mti = '0200'; 
				$bit62 = '';
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				/*log req*/
				$sv['type'] = 'BPJS Kesehatan';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$kode_transaksi = Esta::nomor_transaksi_ppobnew('log_jatelindo_bit',CRUDBooster::getsetting('transaksi_bpjs'),'BPJS Kesehatan');
				$bit37 = $kode_transaksi; /*Retrieval Reference Number. nomor referensi yang dipergunakan oleh system untuk mengacu pada transaksi tersebut.*/
				$sv['bit37'] = $bit37;
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-inquiry';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'BPJS Kesehatan';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['bpjsks_no_va_keluarga'] = substr($json['48'], 0, 16); 
				$svv['bpjsks_jml_bulan'] = substr($json['48'], 16, 2); 
				$svv['bpjsks_no_va_kepala_keluarga'] = substr($json['48'], 18, 16); 
				$svv['bpjsks_total_premi'] = substr($json['48'], 34, 12); 
				$svv['bpjsks_biaya_admin'] = substr($json['48'], 46, 8); 
				$svv['bpjsks_jml_anggota_keluarga'] = substr($json['48'], 54, 2); 

				$count_1 = 56;
				$jml_keluarga = ltrim(substr($json['48'], 54, 2),0);
				for ($i=0; $i < $jml_keluarga; $i++) { 
					$bpjsks_kode_premi_anggota .= substr($json['48'], $count_1, 16).','; 
					$count_1 = $count_1+16;
					$bpjsks_no_va_anggota_keluarga .= substr($json['48'], $count_1, 16).','; 
					$count_1 = $count_1+16;
					$bpjsks_nama .= substr($json['48'], $count_1, 30).','; 
					$count_1 = $count_1+30;
					$bpjsks_kd_cabang .= substr($json['48'], $count_1, 6).','; 
					$count_1 = $count_1+6;
					$bpjsks_nm_cabang .= substr($json['48'], $count_1, 20).','; 
					$count_1 = $count_1+20;
					$bpjsks_biaya_premi_dibayar .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;
					$bpjsks_biaya_premi_bln_ini .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;
					$bpjsks_premi_dimuka .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;

				}

				$svv['bpjsks_kode_premi_anggota'] = $bpjsks_kode_premi_anggota;
				$svv['bpjsks_no_va_anggota_keluarga'] = $bpjsks_no_va_anggota_keluarga;
				$svv['bpjsks_nama'] = $bpjsks_nama;
				$svv['bpjsks_kd_cabang'] = $bpjsks_kd_cabang;
				$svv['bpjsks_nm_cabang'] = $bpjsks_nm_cabang;
				$svv['bpjsks_biaya_premi_dibayar'] = $bpjsks_biaya_premi_dibayar;
				$svv['bpjsks_biaya_premi_bln_ini'] = $bpjsks_biaya_premi_bln_ini;
				$svv['bpjsks_premi_dimuka'] = $bpjsks_premi_dimuka;

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$nama_full = explode(" ,", $svv['bpjsks_nama']);
				$nama_1 = $nama_full[0];
				$json['bpjsks_nama'] = $nama_1;

				$json['bpjsks_no_va_keluarga'] = $svv['bpjsks_no_va_keluarga'];
				$json['bpjsks_no_va_kepala_keluarga'] = $svv['bpjsks_no_va_kepala_keluarga'];
				$json['bpjsks_jml_anggota_keluarga'] = $svv['bpjsks_jml_anggota_keluarga'];
				$json['periode'] = $svv['bpjsks_jml_bulan'];
				$nama_cabang_full = explode(",", $svv['bpjsks_nm_cabang']);
				$nama_cabang_1 = $nama_cabang_full[0];
				$json['bpjsks_nm_cabang'] = $nama_cabang_1;
				$json['bpjsks_total_premi'] = $svv['bpjsks_total_premi'];
				$json['bpjsks_biaya_admin'] = $svv['bpjsks_biaya_admin'];
				$json['id_log'] = $savev;
				$json['stan'] = $bit11;
				break;
			case 'Payment':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '170000';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				/*log req*/
				$sv['type'] = 'BPJS Kesehatan';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0200';
						$lab['status'] = 'Advice';
						$lab['id_log'] = $id_transaksi;
						$lab['no_meter'] = $no_meter;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_bpjsks($id_product,$no_hp,'0200','Advice',$id_transaksi,$id_pelanggan,$jml_bulan);
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'BPJS Kesehatan';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['bpjsks_no_va_keluarga'] = substr($json['48'], 0, 16); 
				$svv['bpjsks_jml_bulan'] = substr($json['48'], 16, 2); 
				$svv['bpjsks_no_va_kepala_keluarga'] = substr($json['48'], 18, 16); 
				$svv['bpjsks_total_premi'] = substr($json['48'], 34, 12); 
				$svv['bpjsks_biaya_admin'] = substr($json['48'], 46, 8); 
				$svv['bpjsks_jpa_refnum'] = substr($json['48'], 54, 32); 
				$svv['bpjsks_jml_anggota_keluarga'] = substr($json['48'], 86, 2); 

				$count_1 = 88;
				$jml_keluarga = ltrim(substr($json['48'], 86, 2),0);
				for ($i=0; $i < $jml_keluarga; $i++) { 
					$bpjsks_kode_premi_anggota .= substr($json['48'], $count_1, 16).','; 
					$count_1 = $count_1+16;
					$bpjsks_no_va_anggota_keluarga .= substr($json['48'], $count_1, 16).','; 
					$count_1 = $count_1+16;
					$bpjsks_nama .= substr($json['48'], $count_1, 30).','; 
					$count_1 = $count_1+30;
					$bpjsks_kd_cabang .= substr($json['48'], $count_1, 6).','; 
					$count_1 = $count_1+6;
					$bpjsks_nm_cabang .= substr($json['48'], $count_1, 20).','; 
					$count_1 = $count_1+20;
					$bpjsks_biaya_premi_dibayar .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;
					$bpjsks_biaya_premi_bln_ini .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;
					$bpjsks_premi_dimuka .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;

				}

				$svv['bpjsks_kode_premi_anggota'] = $bpjsks_kode_premi_anggota;
				$svv['bpjsks_no_va_anggota_keluarga'] = $bpjsks_no_va_anggota_keluarga;
				$svv['bpjsks_nama'] = $bpjsks_nama;
				$svv['bpjsks_kd_cabang'] = $bpjsks_kd_cabang;
				$svv['bpjsks_nm_cabang'] = $bpjsks_nm_cabang;
				$svv['bpjsks_biaya_premi_dibayar'] = $bpjsks_biaya_premi_dibayar;
				$svv['bpjsks_biaya_premi_bln_ini'] = $bpjsks_biaya_premi_bln_ini;
				$svv['bpjsks_premi_dimuka'] = $bpjsks_premi_dimuka;

				$svv['bpjsks_tgl_lunas'] = substr($json['48'], -14); 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['bpjsks_jpa_refnum'] = $svv['bpjsks_jpa_refnum'];
				$nama_full = explode(" ,", $svv['bpjsks_nama']);
				$nama_1 = $nama_full[0];
				$json['bpjsks_nama'] = $nama_1;

				$json['bpjsks_no_va_keluarga'] = $svv['bpjsks_no_va_keluarga'];
				$json['bpjsks_jml_anggota_keluarga'] = $svv['bpjsks_jml_anggota_keluarga'];
				$json['bpjsks_no_va_kepala_keluarga'] = $svv['bpjsks_no_va_kepala_keluarga'];
				$json['periode'] = $svv['bpjsks_jml_bulan'];
				$nama_cabang_full = explode(",", $svv['bpjsks_nm_cabang']);
				$nama_cabang_1 = $nama_cabang_full[0];
				$json['bpjsks_nm_cabang'] = $nama_cabang_1;
				$json['bpjsks_total_premi'] = $svv['bpjsks_total_premi'];
				$json['bpjsks_biaya_admin'] = $svv['bpjsks_biaya_admin'];
				$json['bpjsks_tgl_lunas'] = $svv['bpjsks_tgl_lunas'];
				$json['id_log'] = $savev;
				$json['stan'] = $bit11;
				break;

			case 'Advice':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '171000';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				/*log req*/
				$sv['type'] = 'BPJS Kesehatan';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0200';
						$lab['status'] = 'Advice 2';
						$lab['id_log'] = $id_transaksi;
						$lab['no_meter'] = $no_meter;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_bpjsks($id_product,$no_hp,'0200','Advice 2',$id_transaksi,$id_pelanggan,$jml_bulan);
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'BPJS Kesehatan';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';
				
				$svv['bpjsks_no_va_keluarga'] = substr($json['48'], 0, 16); 
				$svv['bpjsks_jml_bulan'] = substr($json['48'], 16, 2); 
				$svv['bpjsks_no_va_kepala_keluarga'] = substr($json['48'], 18, 16); 
				$svv['bpjsks_total_premi'] = substr($json['48'], 34, 12); 
				$svv['bpjsks_biaya_admin'] = substr($json['48'], 46, 8); 
				$svv['bpjsks_jpa_refnum'] = substr($json['48'], 54, 32); 
				$svv['bpjsks_jml_anggota_keluarga'] = substr($json['48'], 86, 2); 

				$count_1 = 88;
				$jml_keluarga = ltrim(substr($json['48'], 86, 2),0);
				for ($i=0; $i < $jml_keluarga; $i++) { 
					$bpjsks_kode_premi_anggota .= substr($json['48'], $count_1, 16).','; 
					$count_1 = $count_1+16;
					$bpjsks_no_va_anggota_keluarga .= substr($json['48'], $count_1, 16).','; 
					$count_1 = $count_1+16;
					$bpjsks_nama .= substr($json['48'], $count_1, 30).','; 
					$count_1 = $count_1+30;
					$bpjsks_kd_cabang .= substr($json['48'], $count_1, 6).','; 
					$count_1 = $count_1+6;
					$bpjsks_nm_cabang .= substr($json['48'], $count_1, 20).','; 
					$count_1 = $count_1+20;
					$bpjsks_biaya_premi_dibayar .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;
					$bpjsks_biaya_premi_bln_ini .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;
					$bpjsks_premi_dimuka .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;

				}

				$svv['bpjsks_kode_premi_anggota'] = $bpjsks_kode_premi_anggota;
				$svv['bpjsks_no_va_anggota_keluarga'] = $bpjsks_no_va_anggota_keluarga;
				$svv['bpjsks_nama'] = $bpjsks_nama;
				$svv['bpjsks_kd_cabang'] = $bpjsks_kd_cabang;
				$svv['bpjsks_nm_cabang'] = $bpjsks_nm_cabang;
				$svv['bpjsks_biaya_premi_dibayar'] = $bpjsks_biaya_premi_dibayar;
				$svv['bpjsks_biaya_premi_bln_ini'] = $bpjsks_biaya_premi_bln_ini;
				$svv['bpjsks_premi_dimuka'] = $bpjsks_premi_dimuka;

				$svv['bpjsks_tgl_lunas'] = substr($json['48'], -14); 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['bpjsks_jpa_refnum'] = $svv['bpjsks_jpa_refnum'];
				$nama_full = explode(" ,", $svv['bpjsks_nama']);
				$nama_1 = $nama_full[0];
				$json['bpjsks_nama'] = $nama_1;

				$json['bpjsks_no_va_keluarga'] = $svv['bpjsks_no_va_keluarga'];
				$json['bpjsks_no_va_kepala_keluarga'] = $svv['bpjsks_no_va_kepala_keluarga'];
				$json['bpjsks_jml_anggota_keluarga'] = $svv['bpjsks_jml_anggota_keluarga'];
				$json['periode'] = $svv['bpjsks_jml_bulan'];
				$nama_cabang_full = explode(",", $svv['bpjsks_nm_cabang']);
				$nama_cabang_1 = $nama_cabang_full[0];
				$json['bpjsks_nm_cabang'] = $nama_cabang_1;
				$json['bpjsks_total_premi'] = $svv['bpjsks_total_premi'];
				$json['bpjsks_biaya_admin'] = $svv['bpjsks_biaya_admin'];
				$json['bpjsks_tgl_lunas'] = $svv['bpjsks_tgl_lunas'];
				$json['id_log'] = $savev;
				$json['stan'] = $bit11;
				break;
			case 'Advice 2':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '171000';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				/*log req*/
				$sv['type'] = 'BPJS Kesehatan';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0200';
						$lab['status'] = 'Advice 3';
						$lab['id_log'] = $id_transaksi;
						$lab['no_meter'] = $no_meter;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_bpjsks($id_product,$no_hp,'0200','Advice 3',$id_transaksi,$id_pelanggan,$jml_bulan);
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'BPJS Kesehatan';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';
				
				$svv['bpjsks_no_va_keluarga'] = substr($json['48'], 0, 16); 
				$svv['bpjsks_jml_bulan'] = substr($json['48'], 16, 2); 
				$svv['bpjsks_no_va_kepala_keluarga'] = substr($json['48'], 18, 16); 
				$svv['bpjsks_total_premi'] = substr($json['48'], 34, 12); 
				$svv['bpjsks_biaya_admin'] = substr($json['48'], 46, 8); 
				$svv['bpjsks_jpa_refnum'] = substr($json['48'], 54, 32); 
				$svv['bpjsks_jml_anggota_keluarga'] = substr($json['48'], 86, 2); 

				$count_1 = 88;
				$jml_keluarga = ltrim(substr($json['48'], 86, 2),0);
				for ($i=0; $i < $jml_keluarga; $i++) { 
					$bpjsks_kode_premi_anggota .= substr($json['48'], $count_1, 16).','; 
					$count_1 = $count_1+16;
					$bpjsks_no_va_anggota_keluarga .= substr($json['48'], $count_1, 16).','; 
					$count_1 = $count_1+16;
					$bpjsks_nama .= substr($json['48'], $count_1, 30).','; 
					$count_1 = $count_1+30;
					$bpjsks_kd_cabang .= substr($json['48'], $count_1, 6).','; 
					$count_1 = $count_1+6;
					$bpjsks_nm_cabang .= substr($json['48'], $count_1, 20).','; 
					$count_1 = $count_1+20;
					$bpjsks_biaya_premi_dibayar .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;
					$bpjsks_biaya_premi_bln_ini .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;
					$bpjsks_premi_dimuka .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;

				}

				$svv['bpjsks_kode_premi_anggota'] = $bpjsks_kode_premi_anggota;
				$svv['bpjsks_no_va_anggota_keluarga'] = $bpjsks_no_va_anggota_keluarga;
				$svv['bpjsks_nama'] = $bpjsks_nama;
				$svv['bpjsks_kd_cabang'] = $bpjsks_kd_cabang;
				$svv['bpjsks_nm_cabang'] = $bpjsks_nm_cabang;
				$svv['bpjsks_biaya_premi_dibayar'] = $bpjsks_biaya_premi_dibayar;
				$svv['bpjsks_biaya_premi_bln_ini'] = $bpjsks_biaya_premi_bln_ini;
				$svv['bpjsks_premi_dimuka'] = $bpjsks_premi_dimuka;

				$svv['bpjsks_tgl_lunas'] = substr($json['48'], -14); 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['bpjsks_jpa_refnum'] = $svv['bpjsks_jpa_refnum'];
				$nama_full = explode(" ,", $svv['bpjsks_nama']);
				$nama_1 = $nama_full[0];
				$json['bpjsks_nama'] = $nama_1;

				$json['bpjsks_no_va_keluarga'] = $svv['bpjsks_no_va_keluarga'];
				$json['bpjsks_no_va_kepala_keluarga'] = $svv['bpjsks_no_va_kepala_keluarga'];
				$json['bpjsks_jml_anggota_keluarga'] = $svv['bpjsks_jml_anggota_keluarga'];
				$json['periode'] = $svv['bpjsks_jml_bulan'];
				$nama_cabang_full = explode(",", $svv['bpjsks_nm_cabang']);
				$nama_cabang_1 = $nama_cabang_full[0];
				$json['bpjsks_nm_cabang'] = $nama_cabang_1;
				$json['bpjsks_total_premi'] = $svv['bpjsks_total_premi'];
				$json['bpjsks_biaya_admin'] = $svv['bpjsks_biaya_admin'];
				$json['bpjsks_tgl_lunas'] = $svv['bpjsks_tgl_lunas'];
				$json['id_log'] = $savev;
				$json['stan'] = $bit11;
				break;

			case 'Advice 3':
				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '171000';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48;
				$bit49 = $detail_inquiry->bit49;
				//$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				/*log req*/
				$sv['type'] = 'BPJS Kesehatan';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0';
						$lab['status'] = 'Pending';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						//$json = array();
						$json['39'] = 'Pending';
						return $json;
						exit();
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'BPJS Kesehatan';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_multibiller')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_multibiller')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = $json['48']; 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';
				
				$svv['bpjsks_no_va_keluarga'] = substr($json['48'], 0, 16); 
				$svv['bpjsks_jml_bulan'] = substr($json['48'], 16, 2); 
				$svv['bpjsks_no_va_kepala_keluarga'] = substr($json['48'], 18, 16); 
				$svv['bpjsks_total_premi'] = substr($json['48'], 34, 12); 
				$svv['bpjsks_biaya_admin'] = substr($json['48'], 46, 8); 
				$svv['bpjsks_jpa_refnum'] = substr($json['48'], 54, 32); 
				$svv['bpjsks_jml_anggota_keluarga'] = substr($json['48'], 86, 2); 

				$count_1 = 88;
				$jml_keluarga = ltrim(substr($json['48'], 86, 2),0);
				for ($i=0; $i < $jml_keluarga; $i++) { 
					$bpjsks_kode_premi_anggota .= substr($json['48'], $count_1, 16).','; 
					$count_1 = $count_1+16;
					$bpjsks_no_va_anggota_keluarga .= substr($json['48'], $count_1, 16).','; 
					$count_1 = $count_1+16;
					$bpjsks_nama .= substr($json['48'], $count_1, 30).','; 
					$count_1 = $count_1+30;
					$bpjsks_kd_cabang .= substr($json['48'], $count_1, 6).','; 
					$count_1 = $count_1+6;
					$bpjsks_nm_cabang .= substr($json['48'], $count_1, 20).','; 
					$count_1 = $count_1+20;
					$bpjsks_biaya_premi_dibayar .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;
					$bpjsks_biaya_premi_bln_ini .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;
					$bpjsks_premi_dimuka .= substr($json['48'], $count_1, 12).',';
					$count_1 = $count_1+12;

				}

				$svv['bpjsks_kode_premi_anggota'] = $bpjsks_kode_premi_anggota;
				$svv['bpjsks_no_va_anggota_keluarga'] = $bpjsks_no_va_anggota_keluarga;
				$svv['bpjsks_nama'] = $bpjsks_nama;
				$svv['bpjsks_kd_cabang'] = $bpjsks_kd_cabang;
				$svv['bpjsks_nm_cabang'] = $bpjsks_nm_cabang;
				$svv['bpjsks_biaya_premi_dibayar'] = $bpjsks_biaya_premi_dibayar;
				$svv['bpjsks_biaya_premi_bln_ini'] = $bpjsks_biaya_premi_bln_ini;
				$svv['bpjsks_premi_dimuka'] = $bpjsks_premi_dimuka;

				$svv['bpjsks_tgl_lunas'] = substr($json['48'], -14); 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['bpjsks_jpa_refnum'] = $svv['bpjsks_jpa_refnum'];
				$nama_full = explode(" ,", $svv['bpjsks_nama']);
				$nama_1 = $nama_full[0];
				$json['bpjsks_nama'] = $nama_1;

				$json['bpjsks_no_va_keluarga'] = $svv['bpjsks_no_va_keluarga'];
				$json['bpjsks_no_va_kepala_keluarga'] = $svv['bpjsks_no_va_kepala_keluarga'];
				$json['bpjsks_jml_anggota_keluarga'] = $svv['bpjsks_jml_anggota_keluarga'];
				$json['periode'] = $svv['bpjsks_jml_bulan'];
				$nama_cabang_full = explode(",", $svv['bpjsks_nm_cabang']);
				$nama_cabang_1 = $nama_cabang_full[0];
				$json['bpjsks_nm_cabang'] = $nama_cabang_1;
				$json['bpjsks_total_premi'] = $svv['bpjsks_total_premi'];
				$json['bpjsks_biaya_admin'] = $svv['bpjsks_biaya_admin'];
				$json['bpjsks_tgl_lunas'] = $svv['bpjsks_tgl_lunas'];
				$json['id_log'] = $savev;
				$json['stan'] = $bit11;
				break;
		}

		
		if ($err) {
		  	return $err;
		} else {
			if($json['39'] == '18' || $json['39'] == '06') {

				if($status == 'Payment') {
					$lab['created_at'] = date('Y-m-d H:i:s');
					$lab['mti'] = '0200';
					$lab['status'] = 'Advice';
					$lab['id_log'] = $id_transaksi;
					$lab['id_product'] = $id_product;
					$lab['json'] = $status;

					$lab['created_user'] = Esta::user('0');
					$lab['updated_user'] = Esta::user('0');
					$sv_lab = DB::table('log_abnormal')
						->insert($lab);
					return Esta::send_iso_bpjsks($id_product,$no_hp,'0200','Advice',$id_transaksi,$id_pelanggan,$jml_bulan);
				} else {
					if($status == 'Advice') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0200';
						$lab['status'] = 'Advice 2';
						$lab['id_log'] = $id_transaksi;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_bpjsks($id_product,$no_hp,'0200','Advice 2',$id_transaksi,$id_pelanggan,$jml_bulan);
					} else {
						if($status == 'Advice 2') {
							$lab['created_at'] = date('Y-m-d H:i:s');
							$lab['mti'] = '0200';
							$lab['status'] = 'Advice 3';
							$lab['id_log'] = $id_transaksi;
							$lab['id_product'] = $id_product;
							$lab['json'] = $errr;

							$lab['created_user'] = Esta::user('0');
							$lab['updated_user'] = Esta::user('0');
							$sv_lab = DB::table('log_abnormal')
								->insert($lab);
							return Esta::send_iso_bpjsks($id_product,$no_hp,'0200','Advice 3',$id_transaksi,$id_pelanggan,$jml_bulan);
						} else {
							if($status == 'Advice 3') {
								$lab['created_at'] = date('Y-m-d H:i:s');
								$lab['mti'] = '0';
								$lab['status'] = 'Pending';
								$lab['id_log'] = $id_transaksi;
								$lab['id_product'] = $id_product;
								$lab['json'] = $errr;

								$lab['created_user'] = Esta::user('0');
								$lab['updated_user'] = Esta::user('0');
								$sv_lab = DB::table('log_abnormal')
									->insert($lab);
								$json['39'] = 'Pending';
								return $json;
							} else {
								return $json;
							}
						}
					}
				}
			} else {
				return $json;
			}
		}
	}

	public static function show_error($type,$kode,$nama) {
		switch ($kode) {
			case '00':
				return 'Transaksi Sukses';
				break;
			case '06':
				if($type == 'Pulsa') {
					return 'Transaksi Gagal';
				} else {
					return 'Transaksi Gagal';
				}
				break;
			case '07':
				return 'Transaksi gagal, silahkan coba lagi';
			case '09':
				if($type == 'Pulsa') {
					return 'Request in progress';
				} elseif($type == 'PDAM') {
					return 'Nomor rekening salah, mohon teliti kembali';
				} else {
					return 'Nomor meter/idpel yang anda masukkan salah, Mohon teliti kembali';
				}
				break;
			case '11':
				return 'Transaksi gagal';
				break;
			case '12':
				if($type == 'Pulsa') {
					return 'Invalid transaction';
				} else {
					return 'Transaksi gagal';
				}
				break;
			case '13':
				if($type == 'Pulsa') {
					return 'Terjadi gangguan pada koneksi/jaringan. Silahkan coba beberapa saat lagi';
				} else {
					return 'Transaksi gagal';
				}
				break;
			case '14':
				if($type == 'BPJSKS') {
					return 'Nomor virtual account salah, Mohon teliti kembali';
				} elseif($type == 'Pulsa') {
					return 'Nomor pelanggan salah, mohon teliti kembali';
				} elseif($type == 'PDAM') {
					return 'Nomor rekening salah, mohon teliti kembali';
				} elseif($type == 'PLN Prepaid') {
					return 'IDPEL yang anda masukkan salah, Mohon teliti kembali';
				} elseif($type == 'PLN Postpaid') {
					return 'IDPEL yang anda masukkan salah, Mohon teliti kembali';
				} else {
					return 'Invalid phone/ID number';
				}
				break;
			case '15':
				return 'Transaksi gagal';
				break;
			case '16':
				return 'Transaksi gagal';
				break;
			case '18':
				if($type == 'Pulsa') {
					return 'Request timeout';
				} else {
					return 'Transaksi gagal';
				}
				break;
			case '14':
				return 'Idpel yang anda masukkan salah, Mohon teliti kembali';
				break;
			case '16':
				return 'Konsumen '.$nama.' diblokir, Hubungi PLN';
				break;
			case '17':
				if($type == 'PLN Postpaid') {
					return 'Transaksi gagal';
				} elseif($type == 'PLN Prepaid') {
					return 'Nominal pembelian tidak terdaftar';
				} else {
					return '-';
				}
				break;
			case '21':
				return 'Unknown MTI / Jenis pesan tidak disupport oleh biller.';
				break;
			case '22':
				if($type == 'Pulsa') {
					return 'Unpaid transaction, jika pesan reversal dikirimkan tanpa pelunasan sebelumnya';
				} elseif($type == 'BPJSKS') {
					return 'Nomor virtual account salah, Mohon teliti kembali';
				} elseif($type == 'PDAM') {
					return 'Nomor rekening salah, mohon teliti kembali';
				} else {
					return 'Transaksi gagal';
				}
				break;
			case '23':
				return 'Transaksi gagal';
				break;
			case '24':
				return 'Data Advice tidak ditemukan';
				break;
			case '27':
				return 'Transaksi gagal';
				break;
			case '28':
				return 'Transaksi gagal';
				break;
			case '29':
				return 'Transaksi gagal';
				break;
			case '31':
				return 'Transaksi gagal';
				break;
			case '32':
				return 'Transaksi gagal';
				break;
			case '33':
				if($type == 'Pulsa') {
					return 'Kode biller/produk belum terdaftar';
				} else {
					return 'Transaksi gagal';
				}
				break;
			case '34':
				return 'Tagihan sudah terbayar';
				break;
			case '43':
				return 'blocked/expired phone number';
				break;
			case '44':
				return 'Voucher out of stock';
				break;
			case '45':
				return 'Nomor VA tidak sesuai';
				break;
			case '46':
				return 'Jumlah biaya tidak sesuai';
				break;
			case '47':
				if($type == 'BPJSKS') {
					return 'Format Tanggal Tidak Sesuai';
				} else {
					return 'Total kwh melebihi batas maksimum';
				}
				break;
			case '48':
				return 'Maksimal Pembayaran 12 bulan kedepan';
				break;
			case '49':
				return 'No Virtual Account tidak terdaftar';
				break;
			case '51':
				return 'Jumlah Tagihan yang dibayarkan tidak sesuai';
				break;
			case '58':
				return 'Transaksi gagal';
				break;
			case '61':
				return 'Transaksi tidak berhasil';
				break;
			case '63':
				return 'Konsumen '.$nama.' diblokir Hubungi PLN';
				break;
			case '73':
				return 'Transaksi gagal';
				break;
			case '66':
				return 'Mohon maaf transaksi prepaid sedang ada gangguan, Terima kasih';
				break;
			case '74':
				return 'Pembelian minimal Rp. 20 Ribu';
				break;
			case '75':
				return 'Pembelian minimal Rp. 20 Ribu';
				break;
			case '77':
				return 'Nomor meter yang anda masukkan salah, Mohon teliti kembali';
				break;
			case '82':
				return 'Tagihan bulan '.Esta::date_indo(date('m'),'lengkap').' belum tersedia';
				break;
			case '88':
				return 'Transaksi gagal';
				break;
			case '90':
				return 'Transaksi gagal';
				break;
			case '94':
				return 'Transaksi gagal';
				break;
			case '96':
				return 'Transaksi sedang diproses, Mohon hubungi customer service';
				break;
			case '97':
				return 'Transaksi sukses';
				break;
			case '98':
				return 'Transaksi gagal';
				break;
			case '99':
				return 'Transaksi gagal';
				break;
			case '31':
				return 'Transaksi gagal';
				break;
			case '73':
				return 'Transaksi gagal';
				break;
			default:
				return 'Transaksi gagal !';
				break;
		}
	}

	public static function nilai_minor($amount,$minor) {
		$nilai_minor = substr($amount, '-'.$minor);
		$nilai_amount = substr($amount, 0, '-'.$minor);
		if($amount <= 0) {
			return '0,00';
		} else {
			return number_format($nilai_amount, 0, ',', '.').','.$nilai_minor;
		}

	}

	public static function nilai_minor_val($amount,$minor) {
		$nilai_minor = substr($amount, '-'.$minor);
		$nilai_amount = substr($amount, 0, '-'.$minor);
		if($amount <= 0) {
			return '0.00';
		} else {
			return $nilai_amount.'.'.$nilai_minor;
		}

	}

	public static function view_bit62($string) {
		$rest['kode_distribusi'] = substr($string, 0, 2);
		$rest['unit_service'] = substr($string, 2, 5);
		$rest['telp_unit_service'] = substr($string, 7, 15);
		$rest['maksimum_unit_kwh'] = substr($string, 22, 5);
		$rest['total_repeat'] = substr($string, 27, 1);
		$rest['power_purchase_unsold'] = substr($string, 28, 22);

		return $rest;
	}

	public static function send_iso_pln_prabayar($id_product,$no_meter,$mti,$id_transaksi,$status,$repeat,$unsold,$unsold_nominal) {
		//set_time_limit(150);

		$product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$product_amount = ($unsold == 1 ? $unsold_nominal : $product->amount);

		$kode_biller = DB::table('pan_kode_biller')
			->where('id',$product->id_kode_biller)
			->first()->kode;

		$kode_layanan = DB::table('pan_kode_layanan')
			->where('id',$product->id_kode_layanan)
			->first()->kode;

		$tipe_layanan = DB::table('pan_tipe_layanan')
			->where('id',$product->id_tipe_layanan)
			->first()->kode;

		$detail_inquiry = DB::table('log_jatelindo_bit')
			->where('id',$id_transaksi)
			->where('type','PLN Prepaid')
			->where('status','Inquiry')
			->where('jenis','res')
			->first();

		//$kode_transaksi = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));;
		

		switch ($status) {
			case 'Inquiry':
				$bit2 = '053502';/*fix*/
				//echo $bit2;
				//exit();
				//$bit48 = "'".$product->product_id."'"; /*private data.*/
				$bit3 =  '380000'; /*Processing Code fix 380000 : inquiry, 170000/171000/172000 : payment/Purchase/advice, 000200/000300 : reversal/reversal repeat*/
				$bit4 = sprintf("%012s", $product_amount);  /*amount trx*/
				$bit7 = date('mdhms'); /*Transmission date/time MMDDhhmmss*/
				$bit11 = date('sd').rand(11,99); /*STAN unik*/
				$bit12 = date('hms'); /*Local Time hhmmss*/
				$bit13 = date('md'); /*Local Date MMDD*/
				$bit15 = date('md'); /*Settlement Date kapan transaksi akan disettle. (MMDD)*/
				$bit18 = CRUDBooster::getsetting('pln_bit_18'); /*Merchant Type. klasifikasi tipe merchant untuk setiap bisnis produk layanan*/
				$bit32 = CRUDBooster::getsetting('pln_bit_32'); /*Acquiring Institution ID/Banking Code Nilai ini unik untuk setiap institusi. Untuk bangking nilai ini mengacu pada kode bank Indonesia.*/
				//$bit39 = '00'; /*Response Code*/
				$bit41 = CRUDBooster::getsetting('pln_bit_41'); /*Terminal ID. ID ini diserahkan pada setiap pihak acquirer sebagai keterangan untuk setiap pesan.*/
				$bit42 = CRUDBooster::getsetting('pln_bit_42'); /*Acceptor ID. diberikan oleh pihak jatelindo untuk setiap acquirer*/
				//$bit48 = 'JTL53L3149876543210000000000000';
				$flag = strlen($no_meter);
				$bit48 = sprintf("%07s", CRUDBooster::getsetting('pln_switcher_id')).sprintf("%011s", ($flag == '12' ? '00000000000' : $no_meter)).sprintf("%012s", $no_meter).($flag == '12' ? '1' : '0'); /*Private Data. 4 Product ID. 4 kode operator. 10 Phone Number*/
				$bit49 = '360'; /*Transaction Currency Code*/
				$bit61 = ''; /*Private Use*/
				//$mti = '0200'; 
				$bit62 = '';
				$bit63 = 'Esta';//sprintf("%32s\n", CRUDBooster::getsetting('pln_kode_loket')).sprintf("%30s\n", CRUDBooster::getsetting('pln_nama_loket')).sprintf("%50s\n", CRUDBooster::getsetting('pln_alamat_loket')).sprintf("%18s\n", CRUDBooster::getsetting('pln_no_telp_loket'));
				/*Pesan ini otomatis dikirimkan jika dan hanya jika purchase tidak mendapat response atau mendapat response code 18.*/
				
				/*log req*/
				$sv['type'] = 'PLN Prepaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$kode_transaksi = Esta::nomor_transaksi_ppobnew('log_jatelindo_bit',CRUDBooster::getsetting('transaksi_pln'),'PLN Prepaid');
				$bit37 = $kode_transaksi; /*Retrieval Reference Number. nomor referensi yang dipergunakan oleh system untuk mengacu pada transaksi tersebut.*/
				$sv['bit37'] = $bit37;
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/
				
				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-inquiry';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				/*log res*/
				$svv['type'] = 'PLN Prepaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}
				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);
				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $product_amount;
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']); 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = 0;
				$svv['jenis'] = 'res';
				$svv['pln_pra_switcher_id'] = substr($svv['bit48'], 0, 7); 
				$svv['pln_pra_meter_id'] = substr($svv['bit48'], 7, 11);
				$svv['pln_pra_id_pel'] = substr($svv['bit48'], 18, 12);
				$svv['pln_pra_flag'] = substr($svv['bit48'], 30, 1);
				$svv['pln_pra_trx_id'] = substr($svv['bit48'], 31, 32);
				$svv['pln_pra_ref_id'] = substr($svv['bit48'], 63, 32);
				$svv['pln_pra_nama'] = str_replace('###', '"', substr($svv['bit48'], 95, 25));
				$svv['pln_pra_tarif'] = substr($svv['bit48'], 120, 4);
				$svv['pln_pra_kategori_daya'] = substr($svv['bit48'], 124, 9);
				$svv['pln_pra_pilihan_pembelian'] = substr($svv['bit48'], 133, 1);
				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				$json['pln_pra_meter_id'] = $svv['pln_pra_meter_id'];
				$json['pln_pra_nama'] = $svv['pln_pra_nama'];
				$json['pln_pra_tarif'] = $svv['pln_pra_tarif'];
				$json['pln_pra_kategori_daya'] = $svv['pln_pra_kategori_daya'];
				$json['id_log'] = $savev;
				$json['status_iso'] = 'Inquiry';
				/*log res*/
				break;
			case 'Purchase':
				$bit2 = '053502';
				$bit3 = '171000';
				$bit4 = sprintf("%012s", $product_amount); 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit62 = $detail_inquiry->bit62;
				$token_unsold = Esta::view_bit62($bit62);
				if($unsold != 1 && $token_unsold['total_repeat'] >= 1) {
					
					for ($i=0; $i < $token_unsold['total_repeat']; $i++) { 
						if($i == 1) {
							$rest_unsold_1 = substr($token_unsold['power_purchase_unsold'], 0, 11);
						} else {
							$rest_unsold_2 = substr($token_unsold['power_purchase_unsold'], 11, 11);
						}
					}
					if($product_amount == $rest_unsold_1 || $product_amount == $rest_unsold_2) {
						$unsold = 1;
					}
				}
				/*echo $unsold.'-'.$product_amount.'-'.$token_unsold['total_repeat'];
				exit();*/
				$bit48 = $detail_inquiry->bit48.($unsold == 1 ? '1' : '0');
				$bit48 = str_replace('"', ' ', $bit48);
				$bit49 = $detail_inquiry->bit49;
				$bit61 = $detail_inquiry->bit61; 
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				/*log req*/
				$sv['type'] = 'PLN Prepaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-purchase';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0220';
						$lab['status'] = 'Advice';
						$lab['id_log'] = $id_transaksi;
						$lab['no_meter'] = $no_meter;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pln_prabayar($id_product,$no_meter,'0220',$id_transaksi,'Advice',$repeat,$unsold,$unsold_nominal);
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				/*log res*/
				$svv['type'] = 'PLN Prepaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $product_amount;
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit39'] = $json['39'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']); 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';
				$svv['pln_pra_unsold_repeat'] = $repeat;
				$svv['pln_pra_unsold'] = $unsold;
				$svv['pln_pra_unsold_nominal'] = $unsold_nominal;

				$svv['pln_pra_switcher_id'] = substr($svv['bit48'], 0, 7); 
				$svv['pln_pra_meter_id'] = substr($svv['bit48'], 7, 11);
				$svv['pln_pra_id_pel'] = substr($svv['bit48'], 18, 12);
				$svv['pln_pra_flag'] = substr($svv['bit48'], 30, 1);
				$svv['pln_pra_trx_id'] = substr($svv['bit48'], 31, 32);
				$svv['pln_pra_ref_no'] = substr($svv['bit48'], 63, 32);
				$svv['pln_pra_vending_no'] = substr($svv['bit48'], 95, 8);
				$svv['pln_pra_nama'] = str_replace('###', '"', substr($svv['bit48'], 103, 25));
				$svv['pln_pra_tarif'] = substr($svv['bit48'], 128, 4);
				$svv['pln_pra_kategori_daya'] = substr($svv['bit48'], 132, 9);
				$svv['pln_pra_pilihan_pembelian'] = substr($svv['bit48'], 141, 1);
				$svv['pln_pra_nilai_minor_biaya_admin'] = substr($svv['bit48'], 142, 1);
				$svv['pln_pra_biaya_admin'] = substr($svv['bit48'], 143, 10);
				$svv['pln_pra_nilai_minor_materai'] = substr($svv['bit48'], 153, 1);
				$svv['pln_pra_biaya_materai'] = substr($svv['bit48'], 154, 10);
				$svv['pln_pra_nilai_minor_ppn'] = substr($svv['bit48'], 164, 1);
				$svv['pln_pra_ppn'] = substr($svv['bit48'], 165, 10);
				$svv['pln_pra_nilai_minor_ppju'] = substr($svv['bit48'], 175, 1);
				$svv['pln_pra_ppju'] = substr($svv['bit48'], 176, 10);
				$svv['pln_pra_nilai_minor_angsuran'] = substr($svv['bit48'], 186, 1);
				$svv['pln_pra_angsuran'] = substr($svv['bit48'], 187, 10);
				$svv['pln_pra_nilai_minor_pembelian_listrik'] = substr($svv['bit48'], 197, 1);
				$svv['pln_pra_pembelian_listrik'] = substr($svv['bit48'], 198, 12);
				$svv['pln_pra_nilai_minor_kwh'] = substr($svv['bit48'], 210, 1);
				$svv['pln_pra_jml_kwh'] = substr($svv['bit48'], 211, 10);
				$svv['pln_pra_token_number'] = substr($svv['bit48'], 221, 20);
				$svv['pln_pra_tgl_lunas'] = substr($svv['bit48'], 241, 14);

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				$json['pln_pra_meter_id'] = $svv['pln_pra_meter_id'];
				$json['pln_pra_id_pel'] = $svv['pln_pra_id_pel'];
				$json['pln_pra_nama'] = $svv['pln_pra_nama'];
				$json['pln_pra_tarif'] = $svv['pln_pra_tarif'];
				$json['pln_pra_kategori_daya'] = $svv['pln_pra_kategori_daya'];
				$json['pln_pra_ref_no'] = $svv['pln_pra_ref_no'];
				$json['pln_pra_pembelian_listrik'] = $svv['pln_pra_pembelian_listrik'];
				$json['pln_pra_nilai_minor_pembelian_listrik'] = $svv['pln_pra_nilai_minor_pembelian_listrik'];
				$json['pln_pra_biaya_materai'] = $svv['pln_pra_biaya_materai'];
				$json['pln_pra_nilai_minor_materai'] = $svv['pln_pra_nilai_minor_materai'];
				$json['pln_pra_ppn'] = $svv['pln_pra_ppn'];
				$json['pln_pra_nilai_minor_ppn'] = $svv['pln_pra_nilai_minor_ppn'];
				$json['pln_pra_ppju'] = $svv['pln_pra_ppju'];
				$json['pln_pra_nilai_minor_ppju'] = $svv['pln_pra_nilai_minor_ppju'];
				$json['pln_pra_angsuran'] = $svv['pln_pra_angsuran'];
				$json['pln_pra_nilai_minor_angsuran'] = $svv['pln_pra_nilai_minor_angsuran'];
				$json['pln_pra_rp_stroom'] = $svv['pln_pra_pembelian_listrik'];
				$json['pln_pra_jml_kwh'] = $svv['pln_pra_jml_kwh'];
				$json['pln_pra_nilai_minor_kwh'] = $svv['pln_pra_nilai_minor_kwh'];
				$json['pln_pra_token_number'] = $svv['pln_pra_token_number'];
				$json['pln_pra_biaya_admin'] = $svv['pln_pra_biaya_admin'];
				$json['pln_pra_nilai_minor_biaya_admin'] = $svv['pln_pra_nilai_minor_biaya_admin'];
				$json['pln_pra_tgl_lunas'] = $svv['pln_pra_tgl_lunas'];
				$json['id_log'] = $savev;
				$json['status_iso'] = 'Purchase';
				$json['stan'] = $bit11;
				/*log res*/
				break;
			case 'Advice':
				$bit2 = '053502';
				$bit3 = '172000';
				$bit4 = sprintf("%012s", $product_amount); 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48.($unsold == 1 ? '1' : '0');
				$bit48 = str_replace('"', ' ', $bit48);
				$bit49 = $detail_inquiry->bit49;
				$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				/*log req*/
				$sv['type'] = 'PLN Prepaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0220';
						$lab['status'] = 'Advice Repeat';
						$lab['id_log'] = $id_transaksi;
						$lab['no_meter'] = $no_meter;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						//$json = array();
						//$json['39'] = '100';
						//return $json['39'];
						return Esta::send_iso_pln_prabayar($id_product,$no_meter,'0221',$id_transaksi,'Advice Repeat',$repeat,$unsold,$unsold_nominal);
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'PLN Prepaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $product_amount;
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']); 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';
				$svv['pln_pra_unsold_repeat'] = $repeat;
				$svv['pln_pra_unsold'] = $unsold;
				$svv['pln_pra_unsold_nominal'] = $unsold_nominal;

				$svv['pln_pra_switcher_id'] = substr($svv['bit48'], 0, 7); 
				$svv['pln_pra_meter_id'] = substr($svv['bit48'], 7, 11);
				$svv['pln_pra_id_pel'] = substr($svv['bit48'], 18, 12);
				$svv['pln_pra_flag'] = substr($svv['bit48'], 30, 1);
				$svv['pln_pra_trx_id'] = substr($svv['bit48'], 31, 32);
				$svv['pln_pra_ref_no'] = substr($svv['bit48'], 63, 32);
				$svv['pln_pra_vending_no'] = substr($svv['bit48'], 95, 8);
				$svv['pln_pra_nama'] = str_replace('###', '"', substr($svv['bit48'], 103, 25));
				$svv['pln_pra_tarif'] = substr($svv['bit48'], 128, 4);
				$svv['pln_pra_kategori_daya'] = substr($svv['bit48'], 132, 9);
				$svv['pln_pra_pilihan_pembelian'] = substr($svv['bit48'], 141, 1);
				$svv['pln_pra_nilai_minor_biaya_admin'] = substr($svv['bit48'], 142, 1);
				$svv['pln_pra_biaya_admin'] = substr($svv['bit48'], 143, 10);
				$svv['pln_pra_nilai_minor_materai'] = substr($svv['bit48'], 153, 1);
				$svv['pln_pra_biaya_materai'] = substr($svv['bit48'], 154, 10);
				$svv['pln_pra_nilai_minor_ppn'] = substr($svv['bit48'], 164, 1);
				$svv['pln_pra_ppn'] = substr($svv['bit48'], 165, 10);
				$svv['pln_pra_nilai_minor_ppju'] = substr($svv['bit48'], 175, 1);
				$svv['pln_pra_ppju'] = substr($svv['bit48'], 176, 10);
				$svv['pln_pra_nilai_minor_angsuran'] = substr($svv['bit48'], 186, 1);
				$svv['pln_pra_angsuran'] = substr($svv['bit48'], 187, 10);
				$svv['pln_pra_nilai_minor_pembelian_listrik'] = substr($svv['bit48'], 197, 1);
				$svv['pln_pra_pembelian_listrik'] = substr($svv['bit48'], 198, 12);
				$svv['pln_pra_nilai_minor_kwh'] = substr($svv['bit48'], 210, 1);
				$svv['pln_pra_jml_kwh'] = substr($svv['bit48'], 211, 10);
				$svv['pln_pra_token_number'] = substr($svv['bit48'], 221, 20);
				$svv['pln_pra_tgl_lunas'] = substr($svv['bit48'], 241, 14);

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['pln_pra_meter_id'] = $svv['pln_pra_meter_id'];
				$json['pln_pra_id_pel'] = $svv['pln_pra_id_pel'];
				$json['pln_pra_nama'] = $svv['pln_pra_nama'];
				$json['pln_pra_tarif'] = $svv['pln_pra_tarif'];
				$json['pln_pra_kategori_daya'] = $svv['pln_pra_kategori_daya'];
				$json['pln_pra_ref_no'] = $svv['pln_pra_ref_no'];
				$json['pln_pra_pembelian_listrik'] = $svv['pln_pra_pembelian_listrik'];
				$json['pln_pra_nilai_minor_pembelian_listrik'] = $svv['pln_pra_nilai_minor_pembelian_listrik'];
				$json['pln_pra_biaya_materai'] = $svv['pln_pra_biaya_materai'];
				$json['pln_pra_nilai_minor_materai'] = $svv['pln_pra_nilai_minor_materai'];
				$json['pln_pra_ppn'] = $svv['pln_pra_ppn'];
				$json['pln_pra_nilai_minor_ppn'] = $svv['pln_pra_nilai_minor_ppn'];
				$json['pln_pra_ppju'] = $svv['pln_pra_ppju'];
				$json['pln_pra_nilai_minor_ppju'] = $svv['pln_pra_nilai_minor_ppju'];
				$json['pln_pra_angsuran'] = $svv['pln_pra_angsuran'];
				$json['pln_pra_nilai_minor_angsuran'] = $svv['pln_pra_nilai_minor_angsuran'];
				$json['pln_pra_rp_stroom'] = $svv['pln_pra_pembelian_listrik'];
				$json['pln_pra_jml_kwh'] = $svv['pln_pra_jml_kwh'];
				$json['pln_pra_nilai_minor_kwh'] = $svv['pln_pra_nilai_minor_kwh'];
				$json['pln_pra_token_number'] = $svv['pln_pra_token_number'];
				$json['pln_pra_biaya_admin'] = $svv['pln_pra_biaya_admin'];
				$json['pln_pra_nilai_minor_biaya_admin'] = $svv['pln_pra_nilai_minor_biaya_admin'];
				$json['pln_pra_tgl_lunas'] = $svv['pln_pra_tgl_lunas'];
				$json['id_log'] = $savev;
				$json['status_iso'] = 'Advice';
				$json['stan'] = $bit11;
				/*log res*/
				break;
			case 'Advice Repeat':
				$bit2 = '053502';
				$bit3 = '173000';
				$bit4 = sprintf("%012s", $product_amount); 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48.($unsold == 1 ? '1' : '0');
				$bit48 = str_replace('"', ' ', $bit48);
				$bit49 = $detail_inquiry->bit49;
				$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				/*log req*/
				$sv['type'] = 'PLN Prepaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice-repeat';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0220';
						$lab['status'] = 'Advice Repeat 2';
						$lab['id_log'] = $id_transaksi;
						$lab['no_meter'] = $no_meter;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pln_prabayar($id_product,$no_meter,'0221',$id_transaksi,'Advice Repeat 2',$repeat,$unsold,$unsold_nominal);
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				/*log res*/
				$svv['type'] = 'PLN Prepaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']); 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';
				$svv['pln_pra_unsold_repeat'] = $repeat;
				$svv['pln_pra_unsold'] = $unsold;
				$svv['pln_pra_unsold_nominal'] = $unsold_nominal;

				$svv['pln_pra_switcher_id'] = substr($svv['bit48'], 0, 7); 
				$svv['pln_pra_meter_id'] = substr($svv['bit48'], 7, 11);
				$svv['pln_pra_id_pel'] = substr($svv['bit48'], 18, 12);
				$svv['pln_pra_flag'] = substr($svv['bit48'], 30, 1);
				$svv['pln_pra_trx_id'] = substr($svv['bit48'], 31, 32);
				$svv['pln_pra_ref_no'] = substr($svv['bit48'], 63, 32);
				$svv['pln_pra_vending_no'] = substr($svv['bit48'], 95, 8);
				$svv['pln_pra_nama'] = str_replace('###', '"', substr($svv['bit48'], 103, 25));
				$svv['pln_pra_tarif'] = substr($svv['bit48'], 128, 4);
				$svv['pln_pra_kategori_daya'] = substr($svv['bit48'], 132, 9);
				$svv['pln_pra_pilihan_pembelian'] = substr($svv['bit48'], 141, 1);
				$svv['pln_pra_nilai_minor_biaya_admin'] = substr($svv['bit48'], 142, 1);
				$svv['pln_pra_biaya_admin'] = substr($svv['bit48'], 143, 10);
				$svv['pln_pra_nilai_minor_materai'] = substr($svv['bit48'], 153, 1);
				$svv['pln_pra_biaya_materai'] = substr($svv['bit48'], 154, 10);
				$svv['pln_pra_nilai_minor_ppn'] = substr($svv['bit48'], 164, 1);
				$svv['pln_pra_ppn'] = substr($svv['bit48'], 165, 10);
				$svv['pln_pra_nilai_minor_ppju'] = substr($svv['bit48'], 175, 1);
				$svv['pln_pra_ppju'] = substr($svv['bit48'], 176, 10);
				$svv['pln_pra_nilai_minor_angsuran'] = substr($svv['bit48'], 186, 1);
				$svv['pln_pra_angsuran'] = substr($svv['bit48'], 187, 10);
				$svv['pln_pra_nilai_minor_pembelian_listrik'] = substr($svv['bit48'], 197, 1);
				$svv['pln_pra_pembelian_listrik'] = substr($svv['bit48'], 198, 12);
				$svv['pln_pra_nilai_minor_kwh'] = substr($svv['bit48'], 210, 1);
				$svv['pln_pra_jml_kwh'] = substr($svv['bit48'], 211, 10);
				$svv['pln_pra_token_number'] = substr($svv['bit48'], 221, 20);
				$svv['pln_pra_tgl_lunas'] = substr($svv['bit48'], 241, 14);

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['pln_pra_meter_id'] = $svv['pln_pra_meter_id'];
				$json['pln_pra_id_pel'] = $svv['pln_pra_id_pel'];
				$json['pln_pra_nama'] = $svv['pln_pra_nama'];
				$json['pln_pra_tarif'] = $svv['pln_pra_tarif'];
				$json['pln_pra_kategori_daya'] = $svv['pln_pra_kategori_daya'];
				$json['pln_pra_ref_no'] = $svv['pln_pra_ref_no'];
				$json['pln_pra_pembelian_listrik'] = $svv['pln_pra_pembelian_listrik'];
				$json['pln_pra_nilai_minor_pembelian_listrik'] = $svv['pln_pra_nilai_minor_pembelian_listrik'];
				$json['pln_pra_biaya_materai'] = $svv['pln_pra_biaya_materai'];
				$json['pln_pra_nilai_minor_materai'] = $svv['pln_pra_nilai_minor_materai'];
				$json['pln_pra_ppn'] = $svv['pln_pra_ppn'];
				$json['pln_pra_nilai_minor_ppn'] = $svv['pln_pra_nilai_minor_ppn'];
				$json['pln_pra_ppju'] = $svv['pln_pra_ppju'];
				$json['pln_pra_nilai_minor_ppju'] = $svv['pln_pra_nilai_minor_ppju'];
				$json['pln_pra_angsuran'] = $svv['pln_pra_angsuran'];
				$json['pln_pra_nilai_minor_angsuran'] = $svv['pln_pra_nilai_minor_angsuran'];
				$json['pln_pra_rp_stroom'] = $svv['pln_pra_pembelian_listrik'];
				$json['pln_pra_jml_kwh'] = $svv['pln_pra_jml_kwh'];
				$json['pln_pra_nilai_minor_kwh'] = $svv['pln_pra_nilai_minor_kwh'];
				$json['pln_pra_token_number'] = $svv['pln_pra_token_number'];
				$json['pln_pra_biaya_admin'] = $svv['pln_pra_biaya_admin'];
				$json['pln_pra_nilai_minor_biaya_admin'] = $svv['pln_pra_nilai_minor_biaya_admin'];
				$json['pln_pra_tgl_lunas'] = $svv['pln_pra_tgl_lunas'];
				$json['id_log'] = $savev;
				$json['status_iso'] = 'Advice Repeat';
				$json['stan'] = $bit11;
				/*log res*/

				$repeat += 1;
				break;

			case 'Advice Repeat 2':
				$bit2 = '053502';
				$bit3 = '173000';
				$bit4 = sprintf("%012s", $product_amount); 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48.($unsold == 1 ? '1' : '0');
				$bit48 = str_replace('"', ' ', $bit48);
				$bit49 = $detail_inquiry->bit49;
				$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				/*log req*/
				$sv['type'] = 'PLN Prepaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice-repeat';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0220';
						$lab['status'] = 'Advice Repeat Manual';
						$lab['id_log'] = $id_transaksi;
						$lab['no_meter'] = $no_meter;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						$json['39'] = 'Advice Manual';
						return $json;
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				/*log res*/
				$svv['type'] = 'PLN Prepaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']); 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';
				$svv['pln_pra_unsold_repeat'] = $repeat;
				$svv['pln_pra_unsold'] = $unsold;
				$svv['pln_pra_unsold_nominal'] = $unsold_nominal;

				$svv['pln_pra_switcher_id'] = substr($svv['bit48'], 0, 7); 
				$svv['pln_pra_meter_id'] = substr($svv['bit48'], 7, 11);
				$svv['pln_pra_id_pel'] = substr($svv['bit48'], 18, 12);
				$svv['pln_pra_flag'] = substr($svv['bit48'], 30, 1);
				$svv['pln_pra_trx_id'] = substr($svv['bit48'], 31, 32);
				$svv['pln_pra_ref_no'] = substr($svv['bit48'], 63, 32);
				$svv['pln_pra_vending_no'] = substr($svv['bit48'], 95, 8);
				$svv['pln_pra_nama'] = str_replace('###', '"', substr($svv['bit48'], 103, 25));
				$svv['pln_pra_tarif'] = substr($svv['bit48'], 128, 4);
				$svv['pln_pra_kategori_daya'] = substr($svv['bit48'], 132, 9);
				$svv['pln_pra_pilihan_pembelian'] = substr($svv['bit48'], 141, 1);
				$svv['pln_pra_nilai_minor_biaya_admin'] = substr($svv['bit48'], 142, 1);
				$svv['pln_pra_biaya_admin'] = substr($svv['bit48'], 143, 10);
				$svv['pln_pra_nilai_minor_materai'] = substr($svv['bit48'], 153, 1);
				$svv['pln_pra_biaya_materai'] = substr($svv['bit48'], 154, 10);
				$svv['pln_pra_nilai_minor_ppn'] = substr($svv['bit48'], 164, 1);
				$svv['pln_pra_ppn'] = substr($svv['bit48'], 165, 10);
				$svv['pln_pra_nilai_minor_ppju'] = substr($svv['bit48'], 175, 1);
				$svv['pln_pra_ppju'] = substr($svv['bit48'], 176, 10);
				$svv['pln_pra_nilai_minor_angsuran'] = substr($svv['bit48'], 186, 1);
				$svv['pln_pra_angsuran'] = substr($svv['bit48'], 187, 10);
				$svv['pln_pra_nilai_minor_pembelian_listrik'] = substr($svv['bit48'], 197, 1);
				$svv['pln_pra_pembelian_listrik'] = substr($svv['bit48'], 198, 12);
				$svv['pln_pra_nilai_minor_kwh'] = substr($svv['bit48'], 210, 1);
				$svv['pln_pra_jml_kwh'] = substr($svv['bit48'], 211, 10);
				$svv['pln_pra_token_number'] = substr($svv['bit48'], 221, 20);
				$svv['pln_pra_tgl_lunas'] = substr($svv['bit48'], 241, 14);

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['pln_pra_meter_id'] = $svv['pln_pra_meter_id'];
				$json['pln_pra_id_pel'] = $svv['pln_pra_id_pel'];
				$json['pln_pra_nama'] = $svv['pln_pra_nama'];
				$json['pln_pra_tarif'] = $svv['pln_pra_tarif'];
				$json['pln_pra_kategori_daya'] = $svv['pln_pra_kategori_daya'];
				$json['pln_pra_ref_no'] = $svv['pln_pra_ref_no'];
				$json['pln_pra_pembelian_listrik'] = $svv['pln_pra_pembelian_listrik'];
				$json['pln_pra_nilai_minor_pembelian_listrik'] = $svv['pln_pra_nilai_minor_pembelian_listrik'];
				$json['pln_pra_biaya_materai'] = $svv['pln_pra_biaya_materai'];
				$json['pln_pra_nilai_minor_materai'] = $svv['pln_pra_nilai_minor_materai'];
				$json['pln_pra_ppn'] = $svv['pln_pra_ppn'];
				$json['pln_pra_nilai_minor_ppn'] = $svv['pln_pra_nilai_minor_ppn'];
				$json['pln_pra_ppju'] = $svv['pln_pra_ppju'];
				$json['pln_pra_nilai_minor_ppju'] = $svv['pln_pra_nilai_minor_ppju'];
				$json['pln_pra_angsuran'] = $svv['pln_pra_angsuran'];
				$json['pln_pra_nilai_minor_angsuran'] = $svv['pln_pra_nilai_minor_angsuran'];
				$json['pln_pra_rp_stroom'] = $svv['pln_pra_pembelian_listrik'];
				$json['pln_pra_jml_kwh'] = $svv['pln_pra_jml_kwh'];
				$json['pln_pra_nilai_minor_kwh'] = $svv['pln_pra_nilai_minor_kwh'];
				$json['pln_pra_token_number'] = $svv['pln_pra_token_number'];
				$json['pln_pra_biaya_admin'] = $svv['pln_pra_biaya_admin'];
				$json['pln_pra_nilai_minor_biaya_admin'] = $svv['pln_pra_nilai_minor_biaya_admin'];
				$json['pln_pra_tgl_lunas'] = $svv['pln_pra_tgl_lunas'];
				$json['id_log'] = $savev;
				$json['status_iso'] = 'Advice Repeat 2';
				$json['stan'] = $bit11;
				/*log res*/

				$repeat += 1;
				break;

			case 'Advice Manual':
				$bit2 = '053502';
				$bit3 = '173000';
				$bit4 = sprintf("%012s", $product_amount); 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$bit48 = $detail_inquiry->bit48.($unsold == 1 ? '1' : '0');
				$bit48 = str_replace('"', ' ', $bit48);
				$bit49 = $detail_inquiry->bit49;
				$bit61 = $detail_inquiry->bit61; 
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				/*log req*/
				$sv['type'] = 'PLN Prepaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice-repeat';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0220';
						$lab['status'] = 'Advice Repeat Manual';
						$lab['id_log'] = $id_transaksi;
						$lab['no_meter'] = $no_meter;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return $json['39'] = 'Advice Manual';
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				$response = str_replace(array('P"',' ,','",#','##'), array('P ','",',' #',''), $response);

				/*log res*/
				$svv['type'] = 'PLN Prepaid';
				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\"\n}";
				if ($err) {
					$svv['res'] = "cURL Error #:" . $err;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']); 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';
				$svv['pln_pra_unsold_repeat'] = $repeat;
				$svv['pln_pra_unsold'] = $unsold;
				$svv['pln_pra_unsold_nominal'] = $unsold_nominal;

				$svv['pln_pra_switcher_id'] = substr($svv['bit48'], 0, 7); 
				$svv['pln_pra_meter_id'] = substr($svv['bit48'], 7, 11);
				$svv['pln_pra_id_pel'] = substr($svv['bit48'], 18, 12);
				$svv['pln_pra_flag'] = substr($svv['bit48'], 30, 1);
				$svv['pln_pra_trx_id'] = substr($svv['bit48'], 31, 32);
				$svv['pln_pra_ref_no'] = substr($svv['bit48'], 63, 32);
				$svv['pln_pra_vending_no'] = substr($svv['bit48'], 95, 8);
				$svv['pln_pra_nama'] = str_replace('###', '"', substr($svv['bit48'], 103, 25));
				$svv['pln_pra_tarif'] = substr($svv['bit48'], 128, 4);
				$svv['pln_pra_kategori_daya'] = substr($svv['bit48'], 132, 9);
				$svv['pln_pra_pilihan_pembelian'] = substr($svv['bit48'], 141, 1);
				$svv['pln_pra_nilai_minor_biaya_admin'] = substr($svv['bit48'], 142, 1);
				$svv['pln_pra_biaya_admin'] = substr($svv['bit48'], 143, 10);
				$svv['pln_pra_nilai_minor_materai'] = substr($svv['bit48'], 153, 1);
				$svv['pln_pra_biaya_materai'] = substr($svv['bit48'], 154, 10);
				$svv['pln_pra_nilai_minor_ppn'] = substr($svv['bit48'], 164, 1);
				$svv['pln_pra_ppn'] = substr($svv['bit48'], 165, 10);
				$svv['pln_pra_nilai_minor_ppju'] = substr($svv['bit48'], 175, 1);
				$svv['pln_pra_ppju'] = substr($svv['bit48'], 176, 10);
				$svv['pln_pra_nilai_minor_angsuran'] = substr($svv['bit48'], 186, 1);
				$svv['pln_pra_angsuran'] = substr($svv['bit48'], 187, 10);
				$svv['pln_pra_nilai_minor_pembelian_listrik'] = substr($svv['bit48'], 197, 1);
				$svv['pln_pra_pembelian_listrik'] = substr($svv['bit48'], 198, 12);
				$svv['pln_pra_nilai_minor_kwh'] = substr($svv['bit48'], 210, 1);
				$svv['pln_pra_jml_kwh'] = substr($svv['bit48'], 211, 10);
				$svv['pln_pra_token_number'] = substr($svv['bit48'], 221, 20);
				$svv['pln_pra_tgl_lunas'] = substr($svv['bit48'], 241, 14);

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				$json['pln_pra_meter_id'] = $svv['pln_pra_meter_id'];
				$json['pln_pra_id_pel'] = $svv['pln_pra_id_pel'];
				$json['pln_pra_nama'] = $svv['pln_pra_nama'];
				$json['pln_pra_tarif'] = $svv['pln_pra_tarif'];
				$json['pln_pra_kategori_daya'] = $svv['pln_pra_kategori_daya'];
				$json['pln_pra_ref_no'] = $svv['pln_pra_ref_no'];
				$json['pln_pra_pembelian_listrik'] = $svv['pln_pra_pembelian_listrik'];
				$json['pln_pra_nilai_minor_pembelian_listrik'] = $svv['pln_pra_nilai_minor_pembelian_listrik'];
				$json['pln_pra_biaya_materai'] = $svv['pln_pra_biaya_materai'];
				$json['pln_pra_nilai_minor_materai'] = $svv['pln_pra_nilai_minor_materai'];
				$json['pln_pra_ppn'] = $svv['pln_pra_ppn'];
				$json['pln_pra_nilai_minor_ppn'] = $svv['pln_pra_nilai_minor_ppn'];
				$json['pln_pra_ppju'] = $svv['pln_pra_ppju'];
				$json['pln_pra_nilai_minor_ppju'] = $svv['pln_pra_nilai_minor_ppju'];
				$json['pln_pra_angsuran'] = $svv['pln_pra_angsuran'];
				$json['pln_pra_nilai_minor_angsuran'] = $svv['pln_pra_nilai_minor_angsuran'];
				$json['pln_pra_rp_stroom'] = $svv['pln_pra_pembelian_listrik'];
				$json['pln_pra_jml_kwh'] = $svv['pln_pra_jml_kwh'];
				$json['pln_pra_nilai_minor_kwh'] = $svv['pln_pra_nilai_minor_kwh'];
				$json['pln_pra_token_number'] = $svv['pln_pra_token_number'];
				$json['pln_pra_biaya_admin'] = $svv['pln_pra_biaya_admin'];
				$json['pln_pra_nilai_minor_biaya_admin'] = $svv['pln_pra_nilai_minor_biaya_admin'];
				$json['pln_pra_tgl_lunas'] = $svv['pln_pra_tgl_lunas'];
				$json['id_log'] = $savev;
				$json['status_iso'] = 'Advice Repeat 2';
				$json['stan'] = $bit11;
				/*log res*/

				$repeat += 1;
				break;
		}

		

		if($json['39'] == '18' && $status == 'Purchase') {
			$labb['created_at'] = date('Y-m-d H:i:s');
			$labb['mti'] = '0220';
			$labb['status'] = 'Advice';
			$labb['id_log'] = $id_transaksi;
			$labb['no_meter'] = $no_meter;
			$labb['id_product'] = $id_product;

			$labb['created_user'] = Esta::user('0');
			$labb['updated_user'] = Esta::user('0');
			$sv_labb = DB::table('log_abnormal')
				->insert($labb);
			return Esta::send_iso_pln_prabayar($id_product,$no_meter,'0220',$id_transaksi,'Advice','0',$unsold,$unsold_nominal);
		} else {
			if($json['39'] == '18' || $json['39'] == '96') {
				if($status == 'Advice') {
					$labb['created_at'] = date('Y-m-d H:i:s');
					$labb['mti'] = '0220';
					$labb['status'] = 'Advice Repeat'.$status.$json['39'];
					$labb['id_log'] = $id_transaksi;
					$labb['no_meter'] = $no_meter;
					$labb['id_product'] = $id_product;

					$labb['created_user'] = Esta::user('0');
					$labb['updated_user'] = Esta::user('0');
					$sv_labb = DB::table('log_abnormal')
						->insert($labb);
					return Esta::send_iso_pln_prabayar($id_product,$no_meter,'0221',$id_transaksi,'Advice Repeat','0',$unsold,$unsold_nominal);

				} elseif($status == 'Advice Repeat') {
					$labb['created_at'] = date('Y-m-d H:i:s');
					$labb['mti'] = '0220';
					$labb['status'] = 'Advice Repeat 2';
					$labb['id_log'] = $id_transaksi;
					$labb['no_meter'] = $no_meter;
					$labb['id_product'] = $id_product;

					$labbb['created_user'] = Esta::user('0');
					$labbb['updated_user'] = Esta::user('0');
					$sv_labb = DB::table('log_abnormal')
						->insert($labb);
					return Esta::send_iso_pln_prabayar($id_product,$no_meter,'0221',$id_transaksi,'Advice Repeat 2','0',$unsold,$unsold_nominal);
					
				} else {
					return $json;
					
				}
			} else {
				return $json;
			}
		}

	}

	public static function send_iso_pln_pascabayar($id_product,$no_meter,$mti,$id_transaksi,$status) {
		//set_time_limit(150);
		$product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$kode_biller = DB::table('pan_kode_biller')
			->where('id',$product->id_kode_biller)
			->first()->kode;

		$kode_layanan = DB::table('pan_kode_layanan')
			->where('id',$product->id_kode_layanan)
			->first()->kode;

		$tipe_layanan = DB::table('pan_tipe_layanan')
			->where('id',$product->id_tipe_layanan)
			->first()->kode;

		$detail_inquiry = DB::table('log_jatelindo_bit')
			->where('id',$id_transaksi)
			->where('type','PLN Postpaid')
			->where('status','Inquiry')
			->where('jenis','res')
			->first();

		//$kode_transaksi = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));
		

		switch ($status) {
			case 'Inquiry':
				$bit2 = '0'.substr($no_meter, 0, 2).'501';/*fix*/
				//echo $bit2;
				//exit();
				//$bit48 = "'".$product->product_id."'"; /*private data.*/
				$bit3 =  '380000'; /*Processing Code fix 380000 : inquiry, 170000/171000/172000 : payment/Purchase/advice, 000200/000300 : reversal/reversal repeat*/
				$bit4 = '000000000000'; /*amount trx*/
				$bit7 = date('mdhms'); /*Transmission date/time MMDDhhmmss*/
				$bit11 = date('sd').rand(11,99); /*STAN unik*/
				$bit12 = date('hms'); /*Local Time hhmmss*/
				$bit13 = date('md'); /*Local Date MMDD*/
				$bit15 = date('md', strtotime($bit13. ' + 1 days')); /*Settlement Date kapan transaksi akan disettle. (MMDD)*/
				$bit18 = CRUDBooster::getsetting('pln_bit_18'); /*Merchant Type. klasifikasi tipe merchant untuk setiap bisnis produk layanan*/
				$bit32 = CRUDBooster::getsetting('pln_bit_32'); /*Acquiring Institution ID/Banking Code Nilai ini unik untuk setiap institusi. Untuk bangking nilai ini mengacu pada kode bank Indonesia.*/
				
				//$bit39 = '00'; /*Response Code*/
				$bit41 = CRUDBooster::getsetting('pln_bit_41'); /*Terminal ID. ID ini diserahkan pada setiap pihak acquirer sebagai keterangan untuk setiap pesan.*/
				$bit42 = CRUDBooster::getsetting('pln_bit_42'); /*Acceptor ID. diberikan oleh pihak jatelindo untuk setiap acquirer*/
				//$bit48 = 'JTL53L3149876543210000000000000';
				$bit48 = $no_meter; /*Private Data. 4 Product ID. 4 kode operator. 10 Phone Number*/
				$bit49 = '360'; /*Transaction Currency Code*/
				$bit61 = ''; /*Private Use*/
				//$mti = '0200'; 
				$bit62 = '';
				$bit63 = 'Esta';//sprintf("%32s\n", CRUDBooster::getsetting('pln_kode_loket')).sprintf("%30s\n", CRUDBooster::getsetting('pln_nama_loket')).sprintf("%50s\n", CRUDBooster::getsetting('pln_alamat_loket')).sprintf("%18s\n", CRUDBooster::getsetting('pln_no_telp_loket'));
				/*Pesan ini otomatis dikirimkan jika dan hanya jika purchase tidak mendapat response atau mendapat response code 18.*/
				
				/*log req*/
				$sv['type'] = 'PLN Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
								
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				
				$kode_transaksi = Esta::nomor_transaksi_ppobnew('log_jatelindo_bit',CRUDBooster::getsetting('transaksi_pln'),'PLN Postpaid');
				$bit37 = $kode_transaksi; /*Retrieval Reference Number. nomor referensi yang dipergunakan oleh system untuk mengacu pada transaksi tersebut.*/				
				$sv['bit37'] = $bit37;
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);
				/*log req*/
				
				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-inquiry';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_error($curl);

				curl_close($curl);

				$response = str_replace(array('NAMA"','T"'), array('NAMA ','T '), $response);

				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
				if ($errr) {
					$svv['res'] = "cURL Error #:" . $errr;
				} else {
					$svv['res'] = $response;
				}
				

				$json =  json_decode(html_entity_decode($response), true);

				$svv['type'] = 'PLN Postpaid';
				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']); 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';


				$svv['pln_psc_id_pel'] = substr($svv['bit48'], 0, 12); 
				$svv['pln_psc_jml_tagihan'] = substr($svv['bit48'], 12, 1); 
				$svv['pln_psc_jml_tunggakan'] = substr($svv['bit48'], 13, 2); 
				$svv['pln_psc_trx_id'] = substr($svv['bit48'], 15, 32); 
				$svv['pln_psc_nama'] = substr($svv['bit48'], 47, 25); 
				$svv['pln_psc_unit_service'] = substr($svv['bit48'], 72, 5); 
				$svv['pln_psc_no_tlp_unit_service'] = substr($svv['bit48'], 77, 15); 
				$svv['pln_psc_tarif'] = substr($svv['bit48'], 92, 4); 
				$svv['pln_psc_daya'] = substr($svv['bit48'], 96, 9); 
				$svv['pln_psc_biaya_admin'] = substr($svv['bit48'], 105, 9); 

				$jml_tagihan = substr($svv['bit48'], 12, 1);
				$count_1 = 114;
				$sum_biayatagihan_pln = 0; 
				$sum_insentif = 0; 
				$sum_biayappn = 0; 
				$sum_biayaketerlambatan = 0; 
				$view_blth = '';

				for ($i=0; $i < $jml_tagihan; $i++) { 
					$blth .= substr($svv['bit48'], $count_1, 6).','; 

					$b = substr(substr($svv['bit48'], $count_1, 6),4,2);
					$date=date_create("2013-".$b."-15");
					$view_blth .= Esta::date_indo($b,'singkat').substr(substr($svv['bit48'], $count_1, 6),2,2).',';
					
					$count_1 = $count_1+6;

					$tgljthtempo .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$tglcatetmeter .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$biayatagihan_pln .= substr($svv['bit48'], $count_1, 12).','; 
					$sum_biayatagihan_pln += substr($svv['bit48'], $count_1, 12);
					$count_1 = $count_1+12;

					$insentif .= substr($svv['bit48'], $count_1, 11).','; 
					$sum_insentif += substr($svv['bit48'], $count_1, 11);
					$count_1 = $count_1+11;

					$biayappn .= substr($svv['bit48'], $count_1, 10).','; 
					$sum_biayappn += substr($svv['bit48'], $count_1, 10);
					$count_1 = $count_1+10;

					$biayaketerlambatan .= substr($svv['bit48'], $count_1, 12).','; 
					$sum_biayaketerlambatan += substr($svv['bit48'], $count_1, 12);
					$count_1 = $count_1+12;

					$lwbpsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$lwbpsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$wbpsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$wbpsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$kvarhsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$kvarhsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;
				}
				
				$svv['pln_psc_biaya_tagihan_pln'] = $biayatagihan_pln; 
				$svv['pln_psc_insentif'] = $insentif; 
				$svv['pln_psc_biaya_ppn'] = $biayappn; 
				$svv['pln_psc_biaya_keterlambatan'] = $biayaketerlambatan; 

				$svv['pln_psc_blth'] = substr($view_blth,0,-1); 
				$svv['pln_psc_tgl_jatuh_tempo'] = $tgljthtempo; 
				$svv['pln_psc_tgl_catat_meter'] = $tglcatetmeter; 
				$svv['pln_psc_lwbp_sebelum'] = $lwbpsebelum; 
				$svv['pln_psc_lwbp_sesudah'] = $lwbpsesudah; 
				$svv['pln_psc_wbp_sebelum'] = $wbpsebelum; 
				$svv['pln_psc_wbp_sesudah'] = $wbpsesudah; 
				$svv['pln_psc_kvarh_sebelum'] = $kvarhsebelum; 
				$svv['pln_psc_kvarh_sesudah'] = $kvarhsesudah; 

				if($svv['pln_psc_jml_tagihan'] > 1) {
					$json['pln_psc_jml_tagihan'] = $svv['pln_psc_jml_tagihan']+$svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_nama'] = $svv['pln_psc_nama'];
					$json['pln_psc_lwbp_sebelum'] = $svv['pln_psc_lwbp_sebelum'];
					$json['pln_psc_lwbp_sesudah'] = $svv['pln_psc_lwbp_sesudah'];
					$json['pln_psc_biaya_tagihan_pln'] = $sum_biayatagihan_pln;
					$json['pln_psc_id_pel'] = $svv['pln_psc_id_pel'];
					$json['pln_psc_blth'] = $view_blth;
					//$json['rp_transaksi'] = $json['pln_psc_biaya_tagihan_pln']+$sum_biayaketerlambatan;
					$json['rp_transaksi'] = $sum_biayatagihan_pln+$sum_biayaketerlambatan;
					$json['pln_psc_biaya_admin'] = $svv['pln_psc_biaya_admin'];
					$json['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];

					$svv['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];
				} else {
					$json['pln_psc_jml_tagihan'] = $svv['pln_psc_jml_tagihan']+$svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_nama'] = $svv['pln_psc_nama'];
					$json['pln_psc_lwbp_sebelum'] = $svv['pln_psc_lwbp_sebelum'];
					$json['pln_psc_lwbp_sesudah'] = $svv['pln_psc_lwbp_sesudah'];
					$json['pln_psc_biaya_tagihan_pln'] = $sum_biayatagihan_pln;
					$json['pln_psc_id_pel'] = $svv['pln_psc_id_pel'];
					$json['pln_psc_blth'] = $view_blth;
					//$json['rp_transaksi'] = $json['pln_psc_biaya_tagihan_pln']+$sum_biayaketerlambatan;
					$json['rp_transaksi'] = $sum_biayatagihan_pln+$sum_biayaketerlambatan;
					$json['pln_psc_biaya_admin'] = $svv['pln_psc_biaya_admin'];
					$json['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];

					$svv['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];
				}
				$json['header_msg'] = 'Yes';
				$json['jml_tagihan'] = $jml_tagihan;

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);
				$json['id_log'] = $savev;
				/*log res*/
				break;
			case 'Purchase':
				$amount = $detail_inquiry->pln_psc_biaya_tagihan_pln+$detail_inquiry->pln_psc_biaya_keterlambatan+$product->admin_edn+$product->admin_1+$product->admin_2+$product->admin_3+$product->margin-$product->potongan;

				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '170000';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;

				$tambah = substr($detail_inquiry->bit48, 0, 13).substr($detail_inquiry->bit48, 12, 1);
				$after = substr($detail_inquiry->bit48, 13);
				
				$bit48 = $tambah.$after;
				$bit48 = str_replace('"', ' ', $bit48);
				$bit49 = $detail_inquiry->bit49;
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				
				$sv['type'] = 'PLN Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
								
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-purchase';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"63\":\"".$bit63."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0400';
						$lab['status'] = 'Reversal';
						$lab['id_log'] = $id_transaksi;
						$lab['no_meter'] = $no_meter;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pln_pascabayar($id_product,$no_meter,'0400',$id_transaksi,'Reversal');
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				//$response = str_replace(array('NAMA"','T"'), array('NAMA ','T '), $response);

				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"63\":\"".$bit63."\"\n}";
				if ($errr) {
					$svv['res'] = "cURL Error #:" . $errr;
				} else {
					$svv['res'] = $response;
				}
				

				$json =  json_decode(html_entity_decode($response), true);

				$svv['type'] = 'PLN Postpaid';
				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']); 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				

				$svv['pln_psc_id_pel'] = substr($svv['bit48'], 0, 12); 
				$svv['pln_psc_jml_tagihan'] = substr($svv['bit48'], 12, 1); 
				$svv['pln_psc_jml_payment'] = substr($svv['bit48'], 13, 1);
				$svv['pln_psc_jml_tunggakan'] = substr($svv['bit48'], 14, 2); 
				$svv['pln_psc_trx_id'] = substr($svv['bit48'], 16, 32); 
				$svv['pln_psc_no_ref'] = substr($svv['bit48'], 48, 32); 
				$svv['pln_psc_nama'] = substr($svv['bit48'], 80, 25); 
				$svv['pln_psc_unit_service'] = substr($svv['bit48'], 105, 5); 
				$svv['pln_psc_no_tlp_unit_service'] = substr($svv['bit48'], 110, 15); 
				$svv['pln_psc_tarif'] = substr($svv['bit48'], 125, 4); 
				$svv['pln_psc_daya'] = substr($svv['bit48'], 129, 9); 
				$svv['pln_psc_biaya_admin'] = substr($svv['bit48'], 138, 9); 

				$jml_tagihan = substr($svv['bit48'], 12, 1);
				$count_1 = 147;
				$sum_biayatagihan_pln = 0; 
				$sum_insentif = 0; 
				$sum_biayappn = 0; 
				$sum_biayaketerlambatan = 0; 
				$view_blth = '';
				$lwp_awal = '';
				$lwp_akhir = '';

				for ($i=0; $i < $jml_tagihan; $i++) { 
					$blth .= substr($svv['bit48'], $count_1, 6).','; 
					$b = substr(substr($svv['bit48'], $count_1, 6),4,2);
					$date=date_create("2013-".$b."-15");
					$view_blth .= Esta::date_indo($b,'singkat').substr(substr($svv['bit48'], $count_1, 6),2,2).',';
					$count_1 = $count_1+6;

					$tgljthtempo .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$tglcatetmeter .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$biayatagihan_pln .= substr($svv['bit48'], $count_1, 12).','; 
					$sum_biayatagihan_pln += substr($svv['bit48'], $count_1, 12);
					$count_1 = $count_1+12;

					$insentif .= substr($svv['bit48'], $count_1, 11).','; 
					$sum_insentif += substr($svv['bit48'], $count_1, 11);
					$count_1 = $count_1+11;

					$biayappn .= substr($svv['bit48'], $count_1, 10).','; 
					$sum_biayappn += substr($svv['bit48'], $count_1, 10);
					$count_1 = $count_1+10;

					$biayaketerlambatan .= substr($svv['bit48'], $count_1, 12).','; 
					$sum_biayaketerlambatan += substr($svv['bit48'], $count_1, 12);
					$count_1 = $count_1+12;

					$lwbpsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					if($i == 1) {
						$lwp_awal = substr($svv['bit48'], $count_1, 8);
					} 
					$count_1 = $count_1+8;

					$lwbpsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					if($i == $jml_tagihan) {
						$lwp_akhir = substr($svv['bit48'], $count_1, 8);
					} 
					$count_1 = $count_1+8;

					$wbpsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$wbpsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$kvarhsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$kvarhsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

				}
				$tgllunas .= substr($svv['bit48'], $count_1, 8); 
				$count_1 = $count_1+8;

				$jamlunas .= substr($svv['bit48'], $count_1, 6); 
				$count_1 = $count_1+6;

				$svv['pln_psc_blth'] = $blth; 
				$svv['pln_psc_tgl_jatuh_tempo'] = $tgljthtempo;
				$svv['pln_psc_tgl_catat_meter'] = $tglcatetmeter; 
				$svv['pln_psc_biaya_tagihan_pln'] = $biayatagihan_pln; 
				$svv['pln_psc_insentif'] = $insentif; 
				$svv['pln_psc_biaya_ppn'] = $biayappn; 
				$svv['pln_psc_biaya_keterlambatan'] = $biayaketerlambatan; 
				$svv['pln_psc_lwbp_sebelum'] = $lwbpsebelum; 
				$svv['pln_psc_lwbp_sesudah'] = $lwbpsesudah; 
				$svv['pln_psc_wbp_sebelum'] = $wbpsebelum; 
				$svv['pln_psc_wbp_sesudah'] = $wbpsesudah; 
				$svv['pln_psc_kvarh_sebelum'] = $kvarhsebelum; 
				$svv['pln_psc_kvarh_sesudah'] = $kvarhsesudah; 
				$svv['pln_psc_tgl_lunas'] = $tgllunas; 
				$svv['pln_psc_jam_lunas'] = $jamlunas; 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				if($svv['pln_psc_jml_tagihan'] > 1) {

					$json['id_log'] = $savev;
					$json['pln_psc_tarif'] = $svv['pln_psc_tarif'];
					$json['pln_psc_daya'] = $svv['pln_psc_daya'];
					$json['pln_psc_jml_tunggakan'] = $svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_no_ref'] = $svv['pln_psc_no_ref'];
					$json['pln_psc_tgl_lunas'] = $tgllunas;
					$json['pln_psc_jam_lunas'] = $jamlunas;

					$json['pln_psc_jml_tagihan'] = $svv['pln_psc_jml_tagihan']+$svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_nama'] = $svv['pln_psc_nama'];
					$json['pln_psc_lwbp_sebelum'] = substr($svv['pln_psc_lwbp_sebelum'],0,8);
					$json['pln_psc_lwbp_sesudah'] = substr($svv['pln_psc_lwbp_sesudah'],-9,-1);
					$json['pln_psc_biaya_tagihan_pln'] = $sum_biayatagihan_pln;
					$json['pln_psc_id_pel'] = $svv['pln_psc_id_pel'];
					$json['pln_psc_blth'] = substr($view_blth,0,-1);

					$json['rp_transaksi'] = $sum_biayatagihan_pln+$sum_biayaketerlambatan;
					$json['pln_psc_biaya_admin'] = $svv['pln_psc_biaya_admin'];
					$json['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];
				} else {

					$json['id_log'] = $savev;
					$json['pln_psc_tarif'] = $svv['pln_psc_tarif'];
					$json['pln_psc_daya'] = $svv['pln_psc_daya'];
					$json['pln_psc_jml_tunggakan'] = $svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_no_ref'] = $svv['pln_psc_no_ref'];
					$json['pln_psc_tgl_lunas'] = $tgllunas;
					$json['pln_psc_jam_lunas'] = $jamlunas;

					$json['pln_psc_jml_tagihan'] = $svv['pln_psc_jml_tagihan']+$svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_nama'] = $svv['pln_psc_nama'];
					$json['pln_psc_lwbp_sebelum'] = $svv['pln_psc_lwbp_sebelum'];
					$json['pln_psc_lwbp_sesudah'] = $svv['pln_psc_lwbp_sesudah'];
					$json['pln_psc_biaya_tagihan_pln'] = $sum_biayatagihan_pln;
					$json['pln_psc_id_pel'] = $svv['pln_psc_id_pel'];
					$json['pln_psc_blth'] = str_replace(',', '', $view_blth);

					$json['rp_transaksi'] = $sum_biayatagihan_pln+$sum_biayaketerlambatan;
					$json['pln_psc_biaya_admin'] = $svv['pln_psc_biaya_admin'];
					$json['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];
				}
				$json['header_msg'] = 'Yes';
				$json['jml_tagihan'] = $jml_tagihan;
				$json['pln_psc_no_ref'] = $svv['pln_psc_no_ref'];
				/*log res*/
				break;
			case 'Reversal':
				$amount = $detail_inquiry->pln_psc_biaya_tagihan_pln+$detail_inquiry->pln_psc_biaya_keterlambatan+$product->admin_edn+$product->admin_1+$product->admin_2+$product->admin_3+$product->margin-$product->potongan;

				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '000200';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$tambah = substr($detail_inquiry->bit48, 0, 13).substr($detail_inquiry->bit48, 12, 1);
				$after = substr($detail_inquiry->bit48, 13);

				$bit48 = $tambah.$after;
				$bit48 = str_replace('"', ' ', $bit48);
				$bit49 = $detail_inquiry->bit49;
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				$sv['type'] = 'PLN Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
								
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"63\":\"".$bit63."\",\n\t\"90\":\"".$bit90."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab['created_at'] = date('Y-m-d H:i:s');
						$lab['mti'] = '0400';
						$lab['status'] = 'Reversal';
						$lab['id_log'] = $id_transaksi;
						$lab['no_meter'] = $no_meter;
						$lab['id_product'] = $id_product;
						$lab['json'] = $errr;

						$lab['created_user'] = Esta::user('0');
						$lab['updated_user'] = Esta::user('0');
						$sv_lab = DB::table('log_abnormal')
							->insert($lab);
						return Esta::send_iso_pln_pascabayar($id_product,$no_meter,'0420',$id_transaksi,'Reversal Repeat');
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				$response = str_replace(array('NAMA"','T"'), array('NAMA ','T '), $response);

				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"63\":\"".$bit63."\",\n\t\"90\":\"".$bit90."\"\n}";
				if ($errr) {
					$svv['res'] = "cURL Error #:" . $errr;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['type'] = 'PLN Postpaid';
				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']); 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pln_psc_id_pel'] = substr($svv['bit48'], 0, 12); 
				$svv['pln_psc_jml_tagihan'] = substr($svv['bit48'], 12, 1); 
				$svv['pln_psc_jml_payment'] = substr($svv['bit48'], 13, 1);
				$svv['pln_psc_jml_tunggakan'] = substr($svv['bit48'], 14, 2); 
				//$svv['pln_psc_trx_id'] = substr($svv['bit48'], 16, 32); 
				$svv['pln_psc_no_ref'] = substr($svv['bit48'], 16, 32); 
				$svv['pln_psc_nama'] = substr($svv['bit48'], 48, 25); 
				$svv['pln_psc_unit_service'] = substr($svv['bit48'], 73, 5); 
				$svv['pln_psc_no_tlp_unit_service'] = substr($svv['bit48'], 78, 15); 
				$svv['pln_psc_tarif'] = substr($svv['bit48'], 93, 4); 
				$svv['pln_psc_daya'] = substr($svv['bit48'], 97, 9); 
				$svv['pln_psc_biaya_admin'] = substr($svv['bit48'], 106, 9); 

				$jml_tagihan = substr($svv['bit48'], 12, 1);
				$count_1 = 115;
				$sum_biayatagihan_pln = 0; 
				$sum_insentif = 0; 
				$sum_biayappn = 0; 
				$sum_biayaketerlambatan = 0; 
				$view_blth = '';
				$lwp_awal = '';
				$lwp_akhir = '';

				for ($i=0; $i < $jml_tagihan; $i++) { 
					$blth .= substr($svv['bit48'], $count_1, 6).','; 
					$b = substr(substr($svv['bit48'], $count_1, 6),4,2);
					$date=date_create("2013-".$b."-15");
					$view_blth .= Esta::date_indo($b,'singkat').substr(substr($svv['bit48'], $count_1, 6),2,2).',';
					$count_1 = $count_1+6;

					$tgljthtempo .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$tglcatetmeter .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$biayatagihan_pln .= substr($svv['bit48'], $count_1, 12).','; 
					$sum_biayatagihan_pln += substr($svv['bit48'], $count_1, 12);
					$count_1 = $count_1+12;

					$insentif .= substr($svv['bit48'], $count_1, 11).','; 
					$sum_insentif += substr($svv['bit48'], $count_1, 11);
					$count_1 = $count_1+11;

					$biayappn .= substr($svv['bit48'], $count_1, 10).','; 
					$sum_biayappn += substr($svv['bit48'], $count_1, 10);
					$count_1 = $count_1+10;

					$biayaketerlambatan .= substr($svv['bit48'], $count_1, 12).','; 
					$sum_biayaketerlambatan += substr($svv['bit48'], $count_1, 12);
					$count_1 = $count_1+12;

					$lwbpsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					if($i == 1) {
						$lwp_awal = substr($svv['bit48'], $count_1, 8);
					} 
					$count_1 = $count_1+8;

					$lwbpsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					if($i == $jml_tagihan) {
						$lwp_akhir = substr($svv['bit48'], $count_1, 8);
					} 
					$count_1 = $count_1+8;

					$wbpsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$wbpsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$kvarhsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$kvarhsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

				}
				$tgllunas .= substr($svv['bit48'], $count_1, 8); 
				$count_1 = $count_1+8;

				$jamlunas .= substr($svv['bit48'], $count_1, 6); 
				$count_1 = $count_1+6;

				$svv['pln_psc_blth'] = $blth; 
				$svv['pln_psc_tgl_jatuh_tempo'] = $tgljthtempo;
				$svv['pln_psc_tgl_catat_meter'] = $tglcatetmeter; 
				$svv['pln_psc_biaya_tagihan_pln'] = $biayatagihan_pln; 
				$svv['pln_psc_insentif'] = $insentif; 
				$svv['pln_psc_biaya_ppn'] = $biayappn; 
				$svv['pln_psc_biaya_keterlambatan'] = $biayaketerlambatan; 
				$svv['pln_psc_lwbp_sebelum'] = $lwbpsebelum; 
				$svv['pln_psc_lwbp_sesudah'] = $lwbpsesudah; 
				$svv['pln_psc_wbp_sebelum'] = $wbpsebelum; 
				$svv['pln_psc_wbp_sesudah'] = $wbpsesudah; 
				$svv['pln_psc_kvarh_sebelum'] = $kvarhsebelum; 
				$svv['pln_psc_kvarh_sesudah'] = $kvarhsesudah; 
				$svv['pln_psc_tgl_lunas'] = $tgllunas; 
				$svv['pln_psc_jam_lunas'] = $jamlunas; 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				if($svv['pln_psc_jml_tagihan'] > 1) {

					$json['id_log'] = $savev;
					$json['pln_psc_tarif'] = $svv['pln_psc_tarif'];
					$json['pln_psc_daya'] = $svv['pln_psc_daya'];
					$json['pln_psc_jml_tunggakan'] = $svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_no_ref'] = $svv['pln_psc_no_ref'];
					$json['pln_psc_tgl_lunas'] = $tgllunas;
					$json['pln_psc_jam_lunas'] = $jamlunas;

					$json['pln_psc_jml_tagihan'] = $svv['pln_psc_jml_tagihan']+$svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_nama'] = $svv['pln_psc_nama'];
					$json['pln_psc_lwbp_sebelum'] = substr($svv['pln_psc_lwbp_sebelum'],0,8);
					$json['pln_psc_lwbp_sesudah'] = substr($svv['pln_psc_lwbp_sesudah'],-9,-1);
					$json['pln_psc_biaya_tagihan_pln'] = $sum_biayatagihan_pln;
					$json['pln_psc_id_pel'] = $svv['pln_psc_id_pel'];
					$json['pln_psc_blth'] = $view_blth;

					$json['rp_transaksi'] = $sum_biayatagihan_pln+$sum_biayaketerlambatan;
					$json['pln_psc_biaya_admin'] = $svv['pln_psc_biaya_admin'];
					$json['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];
				} else {

					$json['id_log'] = $savev;
					$json['pln_psc_tarif'] = $svv['pln_psc_tarif'];
					$json['pln_psc_daya'] = $svv['pln_psc_daya'];
					$json['pln_psc_jml_tunggakan'] = $svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_no_ref'] = $svv['pln_psc_no_ref'];
					$json['pln_psc_tgl_lunas'] = $tgllunas;
					$json['pln_psc_jam_lunas'] = $jamlunas;

					$json['pln_psc_jml_tagihan'] = $svv['pln_psc_jml_tagihan']+$svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_nama'] = $svv['pln_psc_nama'];
					$json['pln_psc_lwbp_sebelum'] = $svv['pln_psc_lwbp_sebelum'];
					$json['pln_psc_lwbp_sesudah'] = $svv['pln_psc_lwbp_sesudah'];
					$json['pln_psc_biaya_tagihan_pln'] = $sum_biayatagihan_pln;
					$json['pln_psc_id_pel'] = $svv['pln_psc_id_pel'];
					$json['pln_psc_blth'] = str_replace(',', '', $view_blth);;

					$json['rp_transaksi'] = $sum_biayatagihan_pln+$sum_biayaketerlambatan;
					$json['pln_psc_biaya_admin'] = $svv['pln_psc_biaya_admin'];
					$json['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];
				}
				$json['status'] = 'Reversal';
				$json['header_msg'] = 'No';
				$json['jml_tagihan'] = $jml_tagihan;
				/*log res*/
				break;
			case 'Reversal Repeat':
				$amount = $detail_inquiry->pln_psc_biaya_tagihan_pln+$detail_inquiry->pln_psc_biaya_keterlambatan+$product->admin_edn+$product->admin_1+$product->admin_2+$product->admin_3+$product->margin-$product->potongan;

				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '000300';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$tambah = substr($detail_inquiry->bit48, 0, 13).substr($detail_inquiry->bit48, 12, 1);
				$after = substr($detail_inquiry->bit48, 13);

				$bit48 = $tambah.$after;
				$bit48 = str_replace('"', ' ', $bit48);
				$bit49 = $detail_inquiry->bit49;
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				$sv['type'] = 'PLN Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
								
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice-repeat';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"63\":\"".$bit63."\",\n\t\"90\":\"".$bit90."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				$response = curl_exec($curl);
				$errr = curl_errno($curl);
				if($errr) {
					if($errr == '28') {
						$lab2['created_at'] = date('Y-m-d H:i:s');
						$lab2['mti'] = '0400';
						$lab2['status'] = 'Reversal Repeat';
						$lab2['id_log'] = $id_transaksi;
						$lab2['no_meter'] = $no_meter;
						$lab2['id_product'] = $id_product;
						$lab2['json'] = $errr;

						$lab2['created_user'] = Esta::user('0');
						$lab2['updated_user'] = Esta::user('0');
						$sv_lab2 = DB::table('log_abnormal')
							->insert($lab2);
						return Esta::send_iso_pln_pascabayar($id_product,$no_meter,'0420',$id_transaksi,'Reversal Repeat 2');
					}
				} else {
					$response = $response;
				}

				curl_close($curl);

				$response = str_replace(array('NAMA"','T"'), array('NAMA ','T '), $response);

				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"63\":\"".$bit63."\",\n\t\"90\":\"".$bit90."\"\n}";
				if ($errr) {
					$svv['res'] = "cURL Error #:" . $errr;
				} else {
					$svv['res'] = $response;
				}

				$json =  json_decode(html_entity_decode($response), true);

				$svv['type'] = 'PLN Postpaid';
				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']); 
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pln_psc_id_pel'] = substr($svv['bit48'], 0, 12); 
				$svv['pln_psc_jml_tagihan'] = substr($svv['bit48'], 12, 1); 
				$svv['pln_psc_jml_payment'] = substr($svv['bit48'], 13, 1);
				$svv['pln_psc_jml_tunggakan'] = substr($svv['bit48'], 14, 2); 
				//$svv['pln_psc_trx_id'] = substr($svv['bit48'], 16, 32); 
				$svv['pln_psc_no_ref'] = substr($svv['bit48'], 16, 32); 
				$svv['pln_psc_nama'] = substr($svv['bit48'], 48, 25); 
				$svv['pln_psc_unit_service'] = substr($svv['bit48'], 73, 5); 
				$svv['pln_psc_no_tlp_unit_service'] = substr($svv['bit48'], 78, 15); 
				$svv['pln_psc_tarif'] = substr($svv['bit48'], 93, 4); 
				$svv['pln_psc_daya'] = substr($svv['bit48'], 97, 9); 
				$svv['pln_psc_biaya_admin'] = substr($svv['bit48'], 106, 9); 

				$jml_tagihan = substr($svv['bit48'], 12, 1);
				$count_1 = 115;
				$sum_biayatagihan_pln = 0; 
				$sum_insentif = 0; 
				$sum_biayappn = 0; 
				$sum_biayaketerlambatan = 0; 
				$view_blth = '';
				$lwp_awal = '';
				$lwp_akhir = '';

				for ($i=0; $i < $jml_tagihan; $i++) { 
					$blth .= substr($svv['bit48'], $count_1, 6).','; 
					$b = substr(substr($svv['bit48'], $count_1, 6),4,2);
					$date=date_create("2013-".$b."-15");
					$view_blth .= Esta::date_indo($b,'singkat').substr(substr($svv['bit48'], $count_1, 6),2,2).',';
					$count_1 = $count_1+6;

					$tgljthtempo .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$tglcatetmeter .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$biayatagihan_pln .= substr($svv['bit48'], $count_1, 12).','; 
					$sum_biayatagihan_pln += substr($svv['bit48'], $count_1, 12);
					$count_1 = $count_1+12;

					$insentif .= substr($svv['bit48'], $count_1, 11).','; 
					$sum_insentif += substr($svv['bit48'], $count_1, 11);
					$count_1 = $count_1+11;

					$biayappn .= substr($svv['bit48'], $count_1, 10).','; 
					$sum_biayappn += substr($svv['bit48'], $count_1, 10);
					$count_1 = $count_1+10;

					$biayaketerlambatan .= substr($svv['bit48'], $count_1, 12).','; 
					$sum_biayaketerlambatan += substr($svv['bit48'], $count_1, 12);
					$count_1 = $count_1+12;

					$lwbpsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					if($i == 1) {
						$lwp_awal = substr($svv['bit48'], $count_1, 8);
					} 
					$count_1 = $count_1+8;

					$lwbpsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					if($i == $jml_tagihan) {
						$lwp_akhir = substr($svv['bit48'], $count_1, 8);
					} 
					$count_1 = $count_1+8;

					$wbpsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$wbpsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$kvarhsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$kvarhsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

				}
				$tgllunas .= substr($svv['bit48'], $count_1, 8); 
				$count_1 = $count_1+8;

				$jamlunas .= substr($svv['bit48'], $count_1, 6); 
				$count_1 = $count_1+6;

				$svv['pln_psc_blth'] = $blth; 
				$svv['pln_psc_tgl_jatuh_tempo'] = $tgljthtempo;
				$svv['pln_psc_tgl_catat_meter'] = $tglcatetmeter; 
				$svv['pln_psc_biaya_tagihan_pln'] = $biayatagihan_pln; 
				$svv['pln_psc_insentif'] = $insentif; 
				$svv['pln_psc_biaya_ppn'] = $biayappn; 
				$svv['pln_psc_biaya_keterlambatan'] = $biayaketerlambatan; 
				$svv['pln_psc_lwbp_sebelum'] = $lwbpsebelum; 
				$svv['pln_psc_lwbp_sesudah'] = $lwbpsesudah; 
				$svv['pln_psc_wbp_sebelum'] = $wbpsebelum; 
				$svv['pln_psc_wbp_sesudah'] = $wbpsesudah; 
				$svv['pln_psc_kvarh_sebelum'] = $kvarhsebelum; 
				$svv['pln_psc_kvarh_sesudah'] = $kvarhsesudah; 
				$svv['pln_psc_tgl_lunas'] = $tgllunas; 
				$svv['pln_psc_jam_lunas'] = $jamlunas; 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				if($svv['pln_psc_jml_tagihan'] > 1) {

					$json['id_log'] = $savev;
					$json['pln_psc_tarif'] = $svv['pln_psc_tarif'];
					$json['pln_psc_daya'] = $svv['pln_psc_daya'];
					$json['pln_psc_jml_tunggakan'] = $svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_no_ref'] = $svv['pln_psc_no_ref'];
					$json['pln_psc_tgl_lunas'] = $tgllunas;
					$json['pln_psc_jam_lunas'] = $jamlunas;

					$json['pln_psc_jml_tagihan'] = $svv['pln_psc_jml_tagihan']+$svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_nama'] = $svv['pln_psc_nama'];
					$json['pln_psc_lwbp_sebelum'] = substr($svv['pln_psc_lwbp_sebelum'],0,8);
					$json['pln_psc_lwbp_sesudah'] = substr($svv['pln_psc_lwbp_sesudah'],-9,-1);
					$json['pln_psc_biaya_tagihan_pln'] = $sum_biayatagihan_pln;
					$json['pln_psc_id_pel'] = $svv['pln_psc_id_pel'];
					$json['pln_psc_blth'] = $view_blth;

					$json['rp_transaksi'] = $sum_biayatagihan_pln+$sum_biayaketerlambatan;
					$json['pln_psc_biaya_admin'] = $svv['pln_psc_biaya_admin'];
					$json['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];
				} else {

					$json['id_log'] = $savev;
					$json['pln_psc_tarif'] = $svv['pln_psc_tarif'];
					$json['pln_psc_daya'] = $svv['pln_psc_daya'];
					$json['pln_psc_jml_tunggakan'] = $svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_no_ref'] = $svv['pln_psc_no_ref'];
					$json['pln_psc_tgl_lunas'] = $tgllunas;
					$json['pln_psc_jam_lunas'] = $jamlunas;

					$json['pln_psc_jml_tagihan'] = $svv['pln_psc_jml_tagihan']+$svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_nama'] = $svv['pln_psc_nama'];
					$json['pln_psc_lwbp_sebelum'] = $svv['pln_psc_lwbp_sebelum'];
					$json['pln_psc_lwbp_sesudah'] = $svv['pln_psc_lwbp_sesudah'];
					$json['pln_psc_biaya_tagihan_pln'] = $sum_biayatagihan_pln;
					$json['pln_psc_id_pel'] = $svv['pln_psc_id_pel'];
					$json['pln_psc_blth'] = str_replace(',', '', $view_blth);;

					$json['rp_transaksi'] = $sum_biayatagihan_pln+$sum_biayaketerlambatan;
					$json['pln_psc_biaya_admin'] = $svv['pln_psc_biaya_admin'];
					$json['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];
				}
				$json['status'] = 'Reversal';
				$json['header_msg'] = 'No';
				$json['jml_tagihan'] = $jml_tagihan;
				/*log res*/
				break;

			case 'Reversal Repeat 2':
				$amount = $detail_inquiry->pln_psc_biaya_tagihan_pln+$detail_inquiry->pln_psc_biaya_keterlambatan+$product->admin_edn+$product->admin_1+$product->admin_2+$product->admin_3+$product->margin-$product->potongan;

				$bit2 = $detail_inquiry->bit2; 
				$bit3 = '000300';
				$bit4 = $detail_inquiry->bit4; 
				$bit7 = $detail_inquiry->bit7; 
				$bit11 = $detail_inquiry->bit11;
				$bit12 = $detail_inquiry->bit12;
				$bit13 = $detail_inquiry->bit13; 
				$bit15 = $detail_inquiry->bit15;
				$bit18 = $detail_inquiry->bit18;
				$bit32 = $detail_inquiry->bit32;
				$bit37 = $detail_inquiry->bit37;
				$bit41 = $detail_inquiry->bit41; 
				$bit42 = $detail_inquiry->bit42;
				$tambah = substr($detail_inquiry->bit48, 0, 13).substr($detail_inquiry->bit48, 12, 1);
				$after = substr($detail_inquiry->bit48, 13);

				$bit48 = $tambah.$after;
				$bit48 = str_replace('"', ' ', $bit48);
				$bit49 = $detail_inquiry->bit49;
				$bit62 = $detail_inquiry->bit62;
				$bit63 = str_replace(',#','',Esta::view_bit63());
				$bit90 = '0200'.$bit11.$bit13.$bit12.'00000000008'.'00000000000';
				
				$sv['type'] = 'PLN Postpaid';
				$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
								
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['bit2 '] = $bit2;
				$sv['bit3 '] = $bit3;
				$sv['bit4 '] = $bit4;
				$sv['bit7 '] = $bit7;
				$sv['bit11'] = $bit11;
				$sv['bit12'] = $bit12;
				$sv['bit13'] = $bit13;
				$sv['bit15'] = $bit15;
				$sv['bit18'] = $bit18; 
				$sv['bit32'] = $bit32;
				$sv['bit37'] = $bit37;
				$sv['bit41'] = $bit41;
				$sv['bit42'] = $bit42;
				$sv['bit48'] = $bit48; 
				$sv['bit49'] = $bit49;
				$sv['bit61'] = $bit61;  
				$sv['bit62'] = $bit62;
				$sv['bit63'] = $bit63;
				$sv['mti'] = $mti;
				$sv['status'] = $status;
				$sv['id_transaksi'] = $id_transaksi;
				$sv['jenis'] = 'req';

				$sv['created_user'] = Esta::user('0');
				$sv['updated_user'] = Esta::user('0');
				$save = DB::table('log_jatelindo_bit')
					->insert($sv);

				$url_simulator_iso = 'http://crocodic.net/esta/simulatoriso.php?type=pln-advice-repeat';
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_PORT => "9988",
				  CURLOPT_URL => CRUDBooster::getsetting('path_java_adapter_iso8583'),
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"63\":\"".$bit63."\",\n\t\"90\":\"".$bit90."\"\n}",
				  CURLOPT_HTTPHEADER => array(
				    "cache-control: no-cache",
				    "content-type: application/json",
				    "postman-token: 565db80d-f593-1e12-d9d8-7eb84f769aec"
				  ),
				));

				//if ($err) {
					$errr = curl_errno($curl);
				//} else {
					$response = curl_exec($curl);
				//}

				curl_close($curl);

				$response = str_replace(array('NAMA"','T"'), array('NAMA ','T '), $response);

				$svv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"49\":\"".$bit49."\",\n\t\"63\":\"".$bit63."\",\n\t\"90\":\"".$bit90."\"\n}";
				if ($errr) {
					$svv['res'] = "cURL Error #:" . $errr;
				} else {
					$svv['res'] = $response;
				}
				
				

				$json =  json_decode(html_entity_decode($response), true);

				$svv['type'] = 'PLN Postpaid';
				$svv['created_at'] = date('Y-m-d H:i:s');
				$svv['bit2 '] = $json['2'];
				$svv['bit3 '] = $json['3'];
				$svv['bit4 '] = $json['4'];
				$svv['bit7 '] = $json['7'];
				$svv['bit11'] = $json['11'];
				$svv['bit12'] = $json['12'];
				$svv['bit13'] = $json['13'];
				$svv['bit15'] = $json['15'];
				$svv['bit18'] = $json['18']; 
				$svv['bit32'] = $json['32'];
				$svv['bit37'] = $json['37'];
				$svv['bit41'] = $json['41'];
				$svv['bit42'] = $json['42'];
				$svv['bit48'] = str_replace('###', '"', $json['48']);
				$svv['bit49'] = $json['49'];
				$svv['bit61'] = $json['61'];  
				$svv['bit62'] = $json['62'];
				$svv['bit63'] = $json['63'];
				$svv['bit39'] = $json['39'];
				$svv['mti'] = $json['MTI'];
				$svv['status'] = $status;
				$svv['id_transaksi'] = $id_transaksi;
				$svv['jenis'] = 'res';

				$svv['pln_psc_id_pel'] = substr($svv['bit48'], 0, 12); 
				$svv['pln_psc_jml_tagihan'] = substr($svv['bit48'], 12, 1); 
				$svv['pln_psc_jml_payment'] = substr($svv['bit48'], 13, 1);
				$svv['pln_psc_jml_tunggakan'] = substr($svv['bit48'], 14, 2); 
				//$svv['pln_psc_trx_id'] = substr($svv['bit48'], 16, 32); 
				$svv['pln_psc_no_ref'] = substr($svv['bit48'], 16, 32); 
				$svv['pln_psc_nama'] = substr($svv['bit48'], 48, 25); 
				$svv['pln_psc_unit_service'] = substr($svv['bit48'], 73, 5); 
				$svv['pln_psc_no_tlp_unit_service'] = substr($svv['bit48'], 78, 15); 
				$svv['pln_psc_tarif'] = substr($svv['bit48'], 93, 4); 
				$svv['pln_psc_daya'] = substr($svv['bit48'], 97, 9); 
				$svv['pln_psc_biaya_admin'] = substr($svv['bit48'], 106, 9); 

				$jml_tagihan = substr($svv['bit48'], 12, 1);
				$count_1 = 115;
				$sum_biayatagihan_pln = 0; 
				$sum_insentif = 0; 
				$sum_biayappn = 0; 
				$sum_biayaketerlambatan = 0; 
				$view_blth = '';
				$lwp_awal = '';
				$lwp_akhir = '';

				for ($i=0; $i < $jml_tagihan; $i++) { 
					$blth .= substr($svv['bit48'], $count_1, 6).','; 
					$b = substr(substr($svv['bit48'], $count_1, 6),4,2);
					$date=date_create("2013-".$b."-15");
					$view_blth .= Esta::date_indo($b,'singkat').substr(substr($svv['bit48'], $count_1, 6),2,2).',';
					$count_1 = $count_1+6;

					$tgljthtempo .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$tglcatetmeter .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$biayatagihan_pln .= substr($svv['bit48'], $count_1, 12).','; 
					$sum_biayatagihan_pln += substr($svv['bit48'], $count_1, 12);
					$count_1 = $count_1+12;

					$insentif .= substr($svv['bit48'], $count_1, 11).','; 
					$sum_insentif += substr($svv['bit48'], $count_1, 11);
					$count_1 = $count_1+11;

					$biayappn .= substr($svv['bit48'], $count_1, 10).','; 
					$sum_biayappn += substr($svv['bit48'], $count_1, 10);
					$count_1 = $count_1+10;

					$biayaketerlambatan .= substr($svv['bit48'], $count_1, 12).','; 
					$sum_biayaketerlambatan += substr($svv['bit48'], $count_1, 12);
					$count_1 = $count_1+12;

					$lwbpsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					if($i == 1) {
						$lwp_awal = substr($svv['bit48'], $count_1, 8);
					} 
					$count_1 = $count_1+8;

					$lwbpsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					if($i == $jml_tagihan) {
						$lwp_akhir = substr($svv['bit48'], $count_1, 8);
					} 
					$count_1 = $count_1+8;

					$wbpsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$wbpsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$kvarhsebelum .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

					$kvarhsesudah .= substr($svv['bit48'], $count_1, 8).','; 
					$count_1 = $count_1+8;

				}
				$tgllunas .= substr($svv['bit48'], $count_1, 8); 
				$count_1 = $count_1+8;

				$jamlunas .= substr($svv['bit48'], $count_1, 6); 
				$count_1 = $count_1+6;

				$svv['pln_psc_blth'] = $blth; 
				$svv['pln_psc_tgl_jatuh_tempo'] = $tgljthtempo;
				$svv['pln_psc_tgl_catat_meter'] = $tglcatetmeter; 
				$svv['pln_psc_biaya_tagihan_pln'] = $biayatagihan_pln; 
				$svv['pln_psc_insentif'] = $insentif; 
				$svv['pln_psc_biaya_ppn'] = $biayappn; 
				$svv['pln_psc_biaya_keterlambatan'] = $biayaketerlambatan; 
				$svv['pln_psc_lwbp_sebelum'] = $lwbpsebelum; 
				$svv['pln_psc_lwbp_sesudah'] = $lwbpsesudah; 
				$svv['pln_psc_wbp_sebelum'] = $wbpsebelum; 
				$svv['pln_psc_wbp_sesudah'] = $wbpsesudah; 
				$svv['pln_psc_kvarh_sebelum'] = $kvarhsebelum; 
				$svv['pln_psc_kvarh_sesudah'] = $kvarhsesudah; 
				$svv['pln_psc_tgl_lunas'] = $tgllunas; 
				$svv['pln_psc_jam_lunas'] = $jamlunas; 

				$svv['created_user'] = Esta::user('0');
				$svv['updated_user'] = Esta::user('0');
				$savev = DB::table('log_jatelindo_bit')
					->insertGetId($svv);

				if($svv['pln_psc_jml_tagihan'] > 1) {

					$json['id_log'] = $savev;
					$json['pln_psc_tarif'] = $svv['pln_psc_tarif'];
					$json['pln_psc_daya'] = $svv['pln_psc_daya'];
					$json['pln_psc_jml_tunggakan'] = $svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_no_ref'] = $svv['pln_psc_no_ref'];
					$json['pln_psc_tgl_lunas'] = $tgllunas;
					$json['pln_psc_jam_lunas'] = $jamlunas;

					$json['pln_psc_jml_tagihan'] = $svv['pln_psc_jml_tagihan']+$svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_nama'] = $svv['pln_psc_nama'];
					$json['pln_psc_lwbp_sebelum'] = substr($svv['pln_psc_lwbp_sebelum'],0,8);
					$json['pln_psc_lwbp_sesudah'] = substr($svv['pln_psc_lwbp_sesudah'],-9,-1);
					$json['pln_psc_biaya_tagihan_pln'] = $sum_biayatagihan_pln;
					$json['pln_psc_id_pel'] = $svv['pln_psc_id_pel'];
					$json['pln_psc_blth'] = $view_blth;

					$json['rp_transaksi'] = $sum_biayatagihan_pln+$sum_biayaketerlambatan;
					$json['pln_psc_biaya_admin'] = $svv['pln_psc_biaya_admin'];
					$json['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];
				} else {

					$json['id_log'] = $savev;
					$json['pln_psc_tarif'] = $svv['pln_psc_tarif'];
					$json['pln_psc_daya'] = $svv['pln_psc_daya'];
					$json['pln_psc_jml_tunggakan'] = $svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_no_ref'] = $svv['pln_psc_no_ref'];
					$json['pln_psc_tgl_lunas'] = $tgllunas;
					$json['pln_psc_jam_lunas'] = $jamlunas;

					$json['pln_psc_jml_tagihan'] = $svv['pln_psc_jml_tagihan']+$svv['pln_psc_jml_tunggakan'];
					$json['pln_psc_nama'] = $svv['pln_psc_nama'];
					$json['pln_psc_lwbp_sebelum'] = $svv['pln_psc_lwbp_sebelum'];
					$json['pln_psc_lwbp_sesudah'] = $svv['pln_psc_lwbp_sesudah'];
					$json['pln_psc_biaya_tagihan_pln'] = $sum_biayatagihan_pln;
					$json['pln_psc_id_pel'] = $svv['pln_psc_id_pel'];
					$json['pln_psc_blth'] = str_replace(',', '', $view_blth);;

					$json['rp_transaksi'] = $sum_biayatagihan_pln+$sum_biayaketerlambatan;
					$json['pln_psc_biaya_admin'] = $svv['pln_psc_biaya_admin'];
					$json['total_pembayaran'] = $json['rp_transaksi']+$svv['pln_psc_biaya_admin'];
				}
				$json['status'] = 'Reversal';
				$json['header_msg'] = 'No';
				$json['jml_tagihan'] = $jml_tagihan;
				/*log res*/
				break;
		}

		/*log req*/
		/* $sv['type'] = 'PLN Postpaid';
		$sv['req'] = "{\n\t\"ipaddress\":\"".CRUDBooster::getsetting('ip_pln')."\",\n\t\"port\":\"".CRUDBooster::getsetting('port_pln')."\",\n\t\"mti\":\"".$mti."\",\n\t\"2\":\"".$bit2."\",\n\t\"3\":\"".$bit3."\",\n\t\"4\":\"".$bit4."\",\n\t\"7\":\"".$bit7."\",\n\t\"11\":\"".$bit11."\",\n\t\"12\":\"".$bit12."\",\n\t\"13\":\"".$bit13."\",\n\t\"15\":\"".$bit15."\",\n\t\"18\":\"".$bit18."\",\n\t\"32\":\"".$bit32."\",\n\t\"37\":\"".$bit37."\",\n\t\"41\":\"".$bit41."\",\n\t\"42\":\"".$bit42."\",\n\t\"48\":\"".$bit48."\",\n\t\"61\":\"".$bit61."\",\n\t\"62\":\"".$bit62."\",\n\t\"63\":\"".$bit63."\",\n\t\"49\":\"".$bit49."\"\n}";
		if ($errr) {
			$sv['res'] = "cURL Error #:" . $errr;
		} else {
			$sv['res'] = $response;
		}
		
		$sv['created_at'] = date('Y-m-d H:i:s');
		$sv['bit2 '] = $bit2;
		$sv['bit3 '] = $bit3;
		$sv['bit4 '] = $bit4;
		$sv['bit7 '] = $bit7;
		$sv['bit11'] = $bit11;
		$sv['bit12'] = $bit12;
		$sv['bit13'] = $bit13;
		$sv['bit15'] = $bit15;
		$sv['bit18'] = $bit18; 
		$sv['bit32'] = $bit32;
		$sv['bit37'] = $bit37;
		$sv['bit41'] = $bit41;
		$sv['bit42'] = $bit42;
		$sv['bit48'] = $bit48; 
		$sv['bit49'] = $bit49;
		$sv['bit61'] = $bit61;  
		$sv['bit62'] = $bit62;
		$sv['bit63'] = $bit63;
		$sv['mti'] = $mti;
		$sv['status'] = $status;
		$sv['id_transaksi'] = $id_transaksi;
		$sv['jenis'] = 'req';

		$sv['created_user'] = Esta::user('0');
		$sv['updated_user'] = Esta::user('0');
		$save = DB::table('log_jatelindo_bit')
			->insert($sv); */
		/*log req*/
		//return $json;
		/*if ($err) {
		  	return $err;
		} else {*/

			if($json['39'] == '18' && $status == 'Purchase') {
				$lab['created_at'] = date('Y-m-d H:i:s');
				$lab['mti'] = '0400';
				$lab['status'] = 'Reversal';
				$lab['id_log'] = $id_transaksi;
				$lab['no_meter'] = $no_meter;
				$lab['id_product'] = $id_product;
				$lab['json'] = $json;

				$lab['created_user'] = Esta::user('0');
				$lab['updated_user'] = Esta::user('0');
				$sv_lab = DB::table('log_abnormal')
					->insert($lab);
				return Esta::send_iso_pln_pascabayar($id_product,$no_meter,'0400',$id_transaksi,'Reversal');
			} else {
				if($json['39'] == '18' || $json['39'] == '96' && $status == 'Reversal') {
					if($json['39'] != '97') {
						$labb['created_at'] = date('Y-m-d H:i:s');
						$labb['mti'] = '0420';
						$labb['status'] = 'Reversal Repeat';
						$labb['id_log'] = $id_transaksi;
						$labb['no_meter'] = $no_meter;
						$labb['id_product'] = $id_product;
						$labb['json'] = $json;

						$labb['created_user'] = Esta::user('0');
						$labb['updated_user'] = Esta::user('0');
						$sv_labb = DB::table('log_abnormal')
							->insert($labb);
						return Esta::send_iso_pln_pascabayar($id_product,$no_meter,'0420',$id_transaksi,'Reversal Repeat');
					} else {
						$labbb['created_at'] = date('Y-m-d H:i:s');
						$labbb['mti'] = '0000';
						$labbb['status'] = 'Gagal';
						$labbb['id_log'] = $id_transaksi;
						$labbb['no_meter'] = $no_meter;
						$labbb['id_product'] = $id_product;
						$labbb['json'] = $json;

						$labb['created_user'] = Esta::user('0');
						$labb['updated_user'] = Esta::user('0');
						$sv_labbb = DB::table('log_abnormal')
							->insert($labbb);
						return $json;
					}
				} else {
					if($json['39'] == '18' || $json['39'] == '96' && $status == 'Reversal Repeat') {
						$labb['created_at'] = date('Y-m-d H:i:s');
						$labb['mti'] = '0420';
						$labb['status'] = 'Reversal Repeat 2';
						$labb['id_log'] = $id_transaksi;
						$labb['no_meter'] = $no_meter;
						$labb['id_product'] = $id_product;
						$labb['json'] = $json;

						$labb['created_user'] = Esta::user('0');
						$labb['updated_user'] = Esta::user('0');
						$sv_labb = DB::table('log_abnormal')
							->insert($labb);
						return Esta::send_iso_pln_pascabayar($id_product,$no_meter,'0420',$id_transaksi,'Reversal Repeat 2');
					} else {
						$labbb['created_at'] = date('Y-m-d H:i:s');
						$labbb['mti'] = '0000';
						$labbb['status'] = 'Gagal';
						$labbb['id_log'] = $id_transaksi;
						$labbb['no_meter'] = $no_meter;
						$labbb['id_product'] = $id_product;
						$labbb['json'] = $json;

						$labbb['created_user'] = Esta::user('0');
						$labbb['updated_user'] = Esta::user('0');
						$sv_labbb = DB::table('log_abnormal')
							->insert($labbb);
						return $json;
					}
				}
			}

		//}
	}

	public static function cek_regid($id_agen,$regid) {
		$ag = DB::table('agen')->where('id',$id_agen)->first();
		if($regid != $ag->regid) {
			if($ag->saldo <= 0) {
		  		return 0;
			} else {
				return 0;
			}
		} else {
			if($ag->saldo <= 0) {
		  		return 0;
			} else {
				return 1;
			}
		}
	}

	public static function add_fraud($id_agen) {
		$count_fraud = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$up['fraud'] = $count_fraud->fraud+1;
		if($up['fraud'] >= 3) {
			$up['status_aktif'] = 'Tidak Aktif';
		}
		$update = DB::table('agen')
			->where('id',$id_agen)
			->update($up);
		return $up['fraud'];
	}

	public static function last_transaksi($id_agen,$table) {
		$cek = DB::table($table)
			->where('id_agen',$id_agen)
			->orderBy('trans_date','desc')
			->limit(1)
			->get();
		if(!empty($cek)) {
			if($cek[0]->trans_date == date('Y-m-d H:i:s')) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	public static function ip()
	{
	    // Get real visitor IP behind CloudFlare network
	    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
	              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
	              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
	    }
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }

	    return $ip;
	}

	public static function log_money_old($id_agen,$nominal,$datetime,$title,$description,$type,$kategori,$tbl_transaksi,$id_transaksi){
		$sv['created_at'] = $datetime;
		$sv['id_agen'] = $id_agen;
		$sv['title'] = $title;
		$sv['description'] = $description;
		$sv['nominal'] = $nominal;
		$sv['type'] = $type;
		$sv['kategori'] = $kategori;

		$agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$saldo_sekarang = $agen->saldo;
		$komisi_sekarang = $agen->komisi;
		if($type == 'In') {
			if($kategori == 'Transaksi') {
				$up['saldo'] = $saldo_sekarang+$nominal;
				$up2['saldo'] = $saldo_sekarang+$nominal;
			}
			if($kategori == 'Komisi') {
				$up['komisi'] = $komisi_sekarang+$nominal;
				$up2['saldo'] = $saldo_sekarang+$nominal;
			}
		} else {
			if($kategori == 'Transaksi') {
				$up['saldo'] = $saldo_sekarang-$nominal;
				$up2['saldo'] = $saldo_sekarang-$nominal;
			}
			if($kategori == 'Komisi') {
				$up['komisi'] = $komisi_sekarang-$nominal;
				$up2['saldo'] = $saldo_sekarang-$nominal;
			}
		}

		if($kategori == 'Transaksi' || $kategori == 'Komisi') {
			$up['updated_user'] = Esta::user('0');
			$update_saldo = DB::table('agen')
				->where('id',$id_agen)
				->update($up);
		}

		$sv['created_user'] = Esta::user('0');
		$sv['updated_user'] = Esta::user('0');
		$sv['saldo_sesudah'] = $up2['saldo'];
		$sv['saldo_sebelum'] = $saldo_sekarang;
		$sv['tbl_transaksi'] = $tbl_transaksi;
		$sv['id_transaksi'] = $id_transaksi;

		$sv['created_user'] = Esta::user('0');
		$sv['updated_user'] = Esta::user('0');
		if($sv['saldo_sebelum'] < 0) {
			return 0;
		} else {

			$save = DB::table('log_money')
			->insert($sv);
			return 1;
		}
	}

	public static function log_money($id_agen,$nominal,$datetime,$title,$description,$type,$kategori,$tbl_transaksi,$id_transaksi,$saldo){
		$sv['created_at'] = $datetime;
		$sv['id_agen'] = $id_agen;
		$sv['title'] = $title;
		$sv['description'] = $description;
		$sv['nominal'] = $nominal;
		$sv['type'] = $type;
		$sv['kategori'] = $kategori;

		$agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		//$saldo_sekarang = $agen->saldo;
		$saldo_sekarang = $saldo;
		$komisi_sekarang = $agen->komisi;
		if($type == 'In') {
			if($kategori == 'Transaksi') {
				//$up['saldo'] = $saldo_sekarang+$nominal;
				$up2['saldo'] = $saldo_sekarang+$nominal;
			}
			if($kategori == 'Komisi') {
				$up['komisi'] = $komisi_sekarang+$nominal;
				$up2['saldo'] = $saldo_sekarang+$nominal;
			}
		} else {
			if($kategori == 'Transaksi') {
				//$up['saldo'] = $saldo_sekarang-$nominal;
				$up2['saldo'] = $saldo_sekarang-$nominal;
			}
			if($kategori == 'Komisi') {
				$up['komisi'] = $komisi_sekarang-$nominal;
				$up2['saldo'] = $saldo_sekarang-$nominal;
			}
		}

		if($kategori == 'Komisi') {
			$up['updated_user'] = Esta::user('0');
			$update_saldo = DB::table('agen')
				->where('id',$id_agen)
				->update($up);
		}

		$sv['created_user'] = Esta::user('0');
		$sv['updated_user'] = Esta::user('0');
		$sv['saldo_sesudah'] = $up2['saldo'];
		$sv['saldo_sebelum'] = $saldo_sekarang;
		$sv['tbl_transaksi'] = $tbl_transaksi;
		$sv['id_transaksi'] = $id_transaksi;

		$sv['created_user'] = Esta::user('0');
		$sv['updated_user'] = Esta::user('0');
		if($sv['saldo_sebelum'] < 0) {
			return 0;
		} else {

			$save = DB::table('log_money')
			->insert($sv);
			return 1;
		}
	}

	public static function responseSobatku($responseCode,$responseDescription){
		$response_sobatku = CRUDBooster::getsetting('rc_'.strtolower($responseCode));
		//dd($response_sobatku);
		if($response_sobatku != ''){
			return $response_sobatku;
		}else{
			return $responseDescription;
		}
	}

		//21-10-2019 Update by IGLO
	public static function nomor_transaksi_new($table,$prefix) {
		$kode_before = DB::table($table)
			//->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'));
			if($table == 'txn_pengajuan_agen') {
				$kode_before = $kode_before->where('kode_pengajuan_agen','!=',NULL);
			} elseif($table == 'tu_res_sprint') {
				$kode_before = $kode_before->where('transactionNo','!=',NULL);
			}
			$kode_before = $kode_before
			->orderBy('id','desc')
			->limit(1)
			->first();

		if($table == 'txn_pengajuan_agen') {
			$kode_sebelum = $kode_before->kode_pengajuan_agen;
		} elseif($table == 'tu_res_sprint') {
			$kode_sebelum = $kode_before->transactionNo;
		} elseif($table == 'trans_topup_batch' || $table == 'trans_tarik_tunai_batch' || $table == 'trans_pulsa_batch' || $table == 'trans_pln_batch' || $table == 'trans_pdam_batch' || $table == 'trans_bpjs_batch') {
			$kode_sebelum = $kode_before->no_batch;
		} else {
			$kode_sebelum = $kode_before->trans_no;
		}
		$check_month = substr($kode_sebelum,4,2);

		if($check_month != date('m')) {
			$kode = $prefix.Date('ym').'000001';
		} else {
			$kode = $prefix.(!empty($kode_before) ? Date('ym').sprintf("%06s", substr($kode_sebelum, -6)+1) : Date('dm').'000001');
		}

		return $kode;
	}
	
	//generate trans_no ppob 03-12-2019
	public static function nomor_transaksi_ppobnew($table,$prefix,$type) {
		$kode_before = DB::table($table)
			//->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'));

		$kode_before = $kode_before
			->where('bit37','like',$prefix.'%')
			->orderBy('bit37','desc')
			->limit(1)
			->first();

		
		$kode_sebelum = $kode_before->bit37;
		
		$check_month = substr($kode_sebelum,4,2);

		if($check_month != date('m')) {
			$kode = $prefix.Date('ym').'000001';
		} else {
			$kode = $prefix.(!empty($kode_before) ? Date('ym').sprintf("%06s", substr($kode_sebelum, -6)+1) : Date('dm').'000001');
		}

		return $kode;
	}

}
?>















