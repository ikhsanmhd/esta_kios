<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Storage;
use Esta;
use Illuminate\Support\Facades\Auth;
use Hash;
use Carbon\Carbon;
use DateTime;

class ReferralEstaAPI extends ApiController
{
    public function getCoBranding(){
        //$coBrands = DB::select('exec getCoBrand');
        $coBrands = DB::table('cb_mst_cobrand')->select('id', 'nama', 'kode', 'kode_alias', 'kategori')
                    ->whereNull('deleted_at')
                    ->get();
        $data = [];
        foreach ($coBrands as $coBrand) {
            $data[] = [
                'id' => $coBrand->id,
                'nama' => $coBrand->id == 1 ? 'Tidak Ada Perusahaan' : $coBrand->nama,
                'kode' => $coBrand->kode,
                'kode_alias' => $coBrand->id == 1 ? 'Tidak Ada Perusahaan' : $coBrand->kode_alias,
                'kategori' => $coBrand->kategori
            ];
        }        
        return $this->respondWithDataAndMessage($data, "Success");
    }
	
	public function getCoBrandingByParam($query){
        $coBrands = DB::table('cb_mst_cobrand')->select('id', 'nama', 'kode', 'kode_alias', 'kategori')
                    ->whereNull('deleted_at')
                    ->where('kode_alias',trim($query))
                    ->first();
        $data = [];
        if(empty($coBrands)){
            $cobrand_default = DB::table('cb_mst_cobrand')->select('id', 'nama', 'kode', 'kode_alias', 'kategori')
                                ->whereNull('deleted_at')
                                ->where('id',1)
                                ->first();
            
			$data['id'] = $cobrand_default->id;
			$data['nama'] = 'Tidak Ada Perusahaan';
            $data['kode'] = $cobrand_default->kode;
			$data['kode_alias'] = 'Tidak Ada Perusahaan';
            $data['kategori'] = $cobrand_default->kategori;
        }else{
			$data['id'] = $coBrands->id;
			$data['nama'] = $coBrands->id == 1 ? 'Tidak Ada Perusahaan' : $coBrands->nama;
            $data['kode'] = $coBrands->kode;
			$data['kode_alias'] = $coBrands->id == 1 ? 'Tidak Ada Perusahaan' : $coBrands->kode_alias;
            $data['kategori'] = $coBrands->kategori;
        }
        
        return $this->respondWithDataAndMessage($data, "Success");

    }

    public function getCoBrandingData(Request $request, $cobrand_id){
        //$cbBranchs = DB::select('exec getCbBranch ?', [$cobrand_id]);
        $cbBranchs = DB::table('cb_mst_branch')
                     ->where('id_cobrand',$cobrand_id)
                     ->whereNull('deleted_at')
                     ->orderBy('nama','asc')
                     ->get();
        //$questions = DB::select('exec getCbQuestion ?', [$cobrand_id]);
        $questions = DB::table('cb_mst_question')
                     ->where('id_cobrand',$cobrand_id)
                     ->where('isActive','1')
                     ->whereNull('deleted_at')
                     ->orderBy('id','asc')
                     ->limit(3)
                     ->get();

        $cbBranch = [];
        foreach ($cbBranchs as $branch) {
            $cbBranch[] = [
                'id' => $branch->id,
                'nama' => $branch->nama,
                'no_tlp' => $branch->no_tlp
            ];
        }

        $cbQuestion = [];
        foreach ($questions as $questionz) {
            $cbQuestion[] = [
                'id' => $questionz->id,
                'question' => $questionz->question,
            ];
        }
        $data['cabang'] = $cbBranch;
        $data['pertanyaan'] = $cbQuestion;
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getCbAo(Request $request, $branch_id){
        //$cbAos = DB::select('exec getCbAo ?', [$branch_id]);
        $cbAos = DB::table('cb_mst_pj')
                    ->select('id', 'nama', 'employee_id', 'isActive')
                    ->whereNull('deleted_at')
                    ->where('id_branch',$branch_id)
                    ->get();

        $cbAo = [];
        foreach ($cbAos as $ao) {
            $cbAo[] = [
                'id' => $ao->id,
                'employee_id' => $ao->employee_id.' - '.$ao->nama,
				'nama' => $ao->nama
            ];
        }
        return $this->respondWithDataAndMessage($cbAo, "Success");
    }

    public function getProvinsi(){
        $data = DB::table('i_provinsi')->select('id', 'name')->orderBy('name', 'asc')->get();

        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getKabupatenByProv($id){
        $data = DB::table('i_kabupaten')
                ->select('id', 'name')
                ->where('id_i_provinsi', $id)
                ->orderBy('name', 'asc')
                ->get();

        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getKecamatanByKab($id){
        $kecamatans = DB::table('i_kecamatan')
                ->select('id', 'name')
                ->where('id_i_kabupaten', $id)
                ->orderBy('name', 'asc')
                ->get();
        $data = [];
        foreach ($kecamatans as $key) {
			$kodepos = DB::table('i_kodepos')
                ->select('id', 'kodepos')
                ->where('id_i_kecamatan', $key->id)
				->orderBy('kodepos', 'asc')
                ->first();
            $data[] = [
                'id' => trim($key->id),
                'name' => $key->name,
				'id_kodepos' => $kodepos->id == NULL ? '0' : $kodepos->id,
				'kodepos' => $kodepos->kodepos == NULL ? '0' : $kodepos->kodepos
            ];
        }

        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getKodeposByKec($id){
        $kodepos = DB::table('i_kodepos')
                ->select('id', 'kodepos')
                ->where('id_i_kecamatan', $id)
                ->orderBy('kodepos', 'asc')
                ->get();
        $data = [];
		if(empty($kodepos[0])){
			$data[] = [
                'id' => 0,
                'kodepos' => 0
            ];
		}else{
			foreach ($kodepos as $key) {
				$data[] = [
					'id' => $key->id == NULL ? '0' : $key->id,
					'kodepos' => $key->kodepos == NULL ? '0' : $key->kodepos
				];
			}
		}
        return $this->respondWithDataAndMessage($data, "Success");
    }
	
	public function getKelurahanByKec($id){
        $data = DB::table('i_kelurahan')
                ->select('id', 'name', 'full_name')
                ->where('id_i_kecamatan', $id)
                ->orderBy('name', 'asc')
                ->get();

        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getEstimasiPembayaran(){
        $agen = Auth::user();
        $id_agen = $agen->id;
        $type_referral = Request::get('type_referral');

        $agen = DB::table('agen')
            ->where('id',$id_agen)
            ->first();

        $getEstimasi = DB::table('cb_config_calculation')
                        ->where('id_cobrand',$agen->cobrand_id)
                        ->where('type_referral',$type_referral)
                        ->whereNull('deleted_at')
                        ->orderBy('id','desc')
                        ->first(); 

        $getDokumenPendukung = DB::table('cb_config_document')
                                ->select('slot1','slot2','slot3','slot4','slot5','isSlot1Mandatory','isSlot2Mandatory','isSlot3Mandatory','isSlot4Mandatory','isSlot5Mandatory','isSlot1Active','isSlot2Active','isSlot3Active','isSlot4Active','isSlot5Active')
                                ->where('id_cobrand',$agen->cobrand_id)
                                ->where('type_referral',$type_referral)
                                ->whereNull('deleted_at')
                                ->orderBy('id','desc')
                                ->first();

        $dokumen_pendukungs = [];
        if($getDokumenPendukung->isSlot1Active == 1){
            $dokumen_pendukungs[] = [
                'id' => '1',
                'dokumen_label' => $getDokumenPendukung->slot1,
                'isMandatory' => $getDokumenPendukung->isSlot1Mandatory,
                'isActive' => $getDokumenPendukung->isSlot1Active 
            ];
        }
        if($getDokumenPendukung->isSlot2Active == 1){
            $dokumen_pendukungs[] = [
                'id' => '2',
                'dokumen_label' => $getDokumenPendukung->slot2,
                'isMandatory' => $getDokumenPendukung->isSlot2Mandatory,
                'isActive' => $getDokumenPendukung->isSlot2Active
            ];
        }
        if($getDokumenPendukung->isSlot3Active == 1){
            $dokumen_pendukungs[] = [
                'id' => '3',
                'dokumen_label' => $getDokumenPendukung->slot3,
                'isMandatory' => $getDokumenPendukung->isSlot3Mandatory,
                'isActive' => $getDokumenPendukung->isSlot3Active
            ];
        }
        if($getDokumenPendukung->isSlot4Active == 1){
            $dokumen_pendukungs[] = [
                'id' => '4',
                'dokumen_label' => $getDokumenPendukung->slot4,
                'isMandatory' => $getDokumenPendukung->isSlot4Mandatory,
                'isActive' => $getDokumenPendukung->isSlot4Active
            ];
        }
        if($getDokumenPendukung->isSlot5Active == 1){
            $dokumen_pendukungs[] = [
                'id' => '5',
                'dokumen_label' => $getDokumenPendukung->slot5,
                'isMandatory' => $getDokumenPendukung->isSlot5Mandatory,
                'isActive' => $getDokumenPendukung->isSlot5Active
            ];
        }     
        
        $suku_bungas = explode(",", $getEstimasi->suku_bunga);
        $listSukuBunga = [];
        $i = 0;
        foreach ($suku_bungas as $suku_bunga) {
            $i++;
            $listSukuBunga[] = [
                'id' => $i,
                'suku_bunga' => $suku_bunga,
                'type' => '%',
                'suku_bunga_per_bulan' => $suku_bunga % 12
            ];
        }

        /*for($i = $getEstimasi->tenor_min; $i <= $getEstimasi->tenor_max; $i+=$getEstimasi->interval){
            $tenor .= $i.",";
            
        } */

		//perhitungan baru
		/* if(!empty($getEstimasi->tenor_min)){
			$i = $getEstimasi->tenor_min;
			while($i <= $getEstimasi->tenor_max){
				$tenor .= $i.",";
				$i += $getEstimasi->interval;
				
			}

			$tenors = explode(",", $tenor);

			$listTenor = [];
			$idTenor = 0;
			foreach ($tenors as $tenor) {
				if(!empty($tenor)){
					$tanggal_akhir = date('Y-m-d', strtotime('+'.$tenor.' months'));
					$jumlah_hari = Carbon::parse($tanggal_akhir)->diffInDays();
					$idTenor++;
					$listTenor[] = [
						'id' => $idTenor,
						'tenor' => $tenor,
						'type' => 'bulan',
						'tanggal_akhir' => date('Y-m-d', strtotime('+'.$tenor.' months')),
						'jumlah_hari' => $jumlah_hari
					];
				}            
			}
		} */
		
		//perhitungan lama
		$i = 0;
        while($i < $getEstimasi->tenor_max){
            
            $i += $getEstimasi->interval;
            $tenor .= $i.",";
        }

        $tenors = explode(",", $tenor);

        $listTenor = [];
        $idTenor = 0;
        foreach ($tenors as $tenor) {
            if(!empty($tenor)){
				$tanggal_akhir = date('Y-m-d', strtotime('+'.$tenor.' months'));
				$jumlah_hari = Carbon::parse($tanggal_akhir)->diffInDays();
                $idTenor++;
                $listTenor[] = [
                    'id' => $idTenor,
                    'tenor' => $tenor,
                    'type' => 'bulan',
                    'tanggal_akhir' => date('Y-m-d', strtotime('+'.$tenor.' months')),
					'jumlah_hari' => $jumlah_hari
                ];
            }            
        }
        

        $data['dokumen_pendukung'] = $dokumen_pendukungs;
        $data['list_suku_bunga'] = $listSukuBunga;
        $data['list_tenor'] = $listTenor;
        $data['alfa_jaminan'] = $getEstimasi->alfa_jaminan;

        if($type_referral == 'sertifikat'){
            $tipe_sertifikat = DB::table('cb_config_tipe_sertifikat')
                            ->whereNull('deleted_at')
							->where('id_cobrand','1')
                            ->orderBy('id','desc')
                            ->first();

            $tipe_sertifikats = [];
            if($tipe_sertifikat->isTipe1Active == 1){
                $tipe_sertifikats[] = [
                    'id' => '1',
                    'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat1
                ];
            }
            if($tipe_sertifikat->isTipe2Active == 1){
                $tipe_sertifikats[] = [
                    'id' => '2',
                    'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat2
                ];
            }
            if($tipe_sertifikat->isTipe3Active == 1){
                $tipe_sertifikats[] = [
                    'id' => '3',
                    'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat3
                ];
            }
            if($tipe_sertifikat->isTipe4Active == 1){
                $tipe_sertifikats[] = [
                    'id' => '4',
                    'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat4
                ];
            }
            if($tipe_sertifikat->isTipe5Active == 1){
                $tipe_sertifikats[] = [
                    'id' => '5',
                    'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat5
                ];
            }
            if($tipe_sertifikat->isTipe6Active == 1){
                $tipe_sertifikats[] = [
                    'id' => '6',
                    'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat6
                ];
            }
            if($tipe_sertifikat->isTipe7Active == 1){
                $tipe_sertifikats[] = [
                    'id' => '7',
                    'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat7
                ];
            }
            if($tipe_sertifikat->isTipe8Active == 1){
                $tipe_sertifikats[] = [
                    'id' => '8',
                    'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat8
                ];
            }
			

            $data['list_tipe_sertifikat'] = $tipe_sertifikats;

            $jenis_sertifikat = DB::table('cb_config_jenis_sertifikat')
                                ->whereNull('deleted_at')
								->where('id_cobrand','1')
                                ->orderBy('id','desc')
                                ->first();
            $jenis_sertifikats = [];
            if($jenis_sertifikat->isJenis1Active == 1){
                $jenis_sertifikats[] = [
                    'id' => '1',
                    'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat1
                ];
            }
            if($jenis_sertifikat->isJenis2Active == 1){
                $jenis_sertifikats[] = [
                    'id' => '2',
                    'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat2
                ];
            }
            if($jenis_sertifikat->isJenis3Active == 1){
                $jenis_sertifikats[] = [
                    'id' => '3',
                    'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat3
                ];
            }
            if($jenis_sertifikat->isJenis4Active == 1){
                $jenis_sertifikats[] = [
                    'id' => '4',
                    'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat4
                ];
            }
            if($jenis_sertifikat->isJenis5Active == 1){
                $jenis_sertifikats[] = [
                    'id' => '5',
                    'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat5
                ];
            }
            if($jenis_sertifikat->isJenis6Active == 1){
                $jenis_sertifikats[] = [
                    'id' => '6',
                    'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat6
                ];
            }
            if($jenis_sertifikat->isJenis7Active == 1){
                $jenis_sertifikats[] = [
                    'id' => '7',
                    'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat7
                ];
            }
            if($jenis_sertifikat->isJenis8Active == 1){
                $jenis_sertifikats[] = [
                    'id' => '8',
                    'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat8
                ];
            }

            $data['list_jenis_sertifikat'] = $jenis_sertifikats;
        }        

        return $this->respondWithDataAndMessage($data, "Success");
    }
	
	public function postDokumenPendukung(){
        $agen = Auth::user();
        $id_agen = $agen->id;
        $type_referral = Request::get('type_referral');

        $agen = DB::table('agen')
            ->where('id',$id_agen)
            ->first();
            
        $getDokumenPendukung = DB::table('cb_config_document')
                                ->select('slot1','slot2','slot3','slot4','slot5','isSlot1Mandatory','isSlot2Mandatory','isSlot3Mandatory','isSlot4Mandatory','isSlot5Mandatory','isSlot1Active','isSlot2Active','isSlot3Active','isSlot4Active','isSlot5Active')
                                ->where('id_cobrand','1')
                                ->where('type_referral',$type_referral)
                                ->whereNull('deleted_at')
                                ->orderBy('id','desc')
                                ->first();

        $dokumen_pendukungs = [];
        if($getDokumenPendukung->isSlot1Active == 1){
            $dokumen_pendukungs[] = [
                'id' => '1',
                'dokumen_label' => $getDokumenPendukung->slot1,
                'isMandatory' => $getDokumenPendukung->isSlot1Mandatory,
                'isActive' => $getDokumenPendukung->isSlot1Active 
            ];
        }
        if($getDokumenPendukung->isSlot2Active == 1){
            $dokumen_pendukungs[] = [
                'id' => '2',
                'dokumen_label' => $getDokumenPendukung->slot2,
                'isMandatory' => $getDokumenPendukung->isSlot2Mandatory,
                'isActive' => $getDokumenPendukung->isSlot2Active
            ];
        }
        if($getDokumenPendukung->isSlot3Active == 1){
            $dokumen_pendukungs[] = [
                'id' => '3',
                'dokumen_label' => $getDokumenPendukung->slot3,
                'isMandatory' => $getDokumenPendukung->isSlot3Mandatory,
                'isActive' => $getDokumenPendukung->isSlot3Active
            ];
        }
        if($getDokumenPendukung->isSlot4Active == 1){
            $dokumen_pendukungs[] = [
                'id' => '4',
                'dokumen_label' => $getDokumenPendukung->slot4,
                'isMandatory' => $getDokumenPendukung->isSlot4Mandatory,
                'isActive' => $getDokumenPendukung->isSlot4Active
            ];
        }
        if($getDokumenPendukung->isSlot5Active == 1){
            $dokumen_pendukungs[] = [
                'id' => '5',
                'dokumen_label' => $getDokumenPendukung->slot5,
                'isMandatory' => $getDokumenPendukung->isSlot5Mandatory,
                'isActive' => $getDokumenPendukung->isSlot5Active
            ];
        }  

        return $this->respondWithDataAndMessage($dokumen_pendukungs, "Success");
    }
	
	public function postListSukuBunga(){
        $agen = Auth::user();
        $id_agen = $agen->id;
        $type_referral = Request::get('type_referral');

        $agen = DB::table('agen')
            ->where('id',$id_agen)
            ->first();

        $getEstimasi = DB::table('cb_config_calculation')
                        ->where('id_cobrand',$agen->cobrand_id)
                        ->where('type_referral',$type_referral)
                        ->whereNull('deleted_at')
                        ->orderBy('id','desc')
                        ->first();

        $suku_bungas = explode(",", $getEstimasi->suku_bunga);
        $listSukuBunga = [];
        $i = 0;
        foreach ($suku_bungas as $suku_bunga) {
            $i++;
            $listSukuBunga[] = [
                'id' => $i,
                'suku_bunga' => $suku_bunga,
                'type' => '%',
                'suku_bunga_per_bulan' => $suku_bunga % 12
            ];
        }
        return $this->respondWithDataAndMessage($listSukuBunga, "Success");
    }

    public function postListTenor(){
        $agen = Auth::user();
        $id_agen = $agen->id;
        $type_referral = Request::get('type_referral');

        $agen = DB::table('agen')
            ->where('id',$id_agen)
            ->first();

        $getEstimasi = DB::table('cb_config_calculation')
                        ->where('id_cobrand',$agen->cobrand_id)
                        ->where('type_referral',$type_referral)
                        ->whereNull('deleted_at')
                        ->orderBy('id','desc')
                        ->first();

        $i = 0;
        while($i < $getEstimasi->tenor_max){
            $i += $getEstimasi->interval;
            $tenor .= $i.",";
        }

        $tenors = explode(",", $tenor);

        $listTenor = [];
        $idTenor = 0;
        foreach ($tenors as $tenor) {
            if(!empty($tenor)){
				//$effectiveDate = date('Y-m-d', strtotime("+3 months", strtotime($effectiveDate)));
                $idTenor++;
                $listTenor[] = [
                    'id' => $idTenor,
                    'tenor' => $tenor,
                    'type' => 'bulan'
                ];
            }            
        }

        $data['list_tenor'] = $listTenor;
        $data['alfa_jaminan'] = $getEstimasi->alfa_jaminan;
        return $this->respondWithDataAndMessage($data, "Success");
    }
	
	public function postTipeSertifikat(){
        $tipe_sertifikat = DB::table('cb_config_tipe_sertifikat')
                        ->whereNull('deleted_at')
                        ->where('id_cobrand','1')
                        ->orderBy('id','desc')
                        ->first();

        $tipe_sertifikats = [];
        if($tipe_sertifikat->isTipe1Active == 1){
            $tipe_sertifikats[0] = [
                'id' => '1',
                'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat1
            ];
        }
        if($tipe_sertifikat->isTipe2Active == 1){
            $tipe_sertifikats[1] = [
                'id' => '2',
                'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat2
            ];
        }
        if($tipe_sertifikat->isTipe3Active == 1){
            $tipe_sertifikats[2] = [
                'id' => '3',
                'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat3
            ];
        }
        if($tipe_sertifikat->isTipe4Active == 1){
            $tipe_sertifikats[3] = [
                'id' => '4',
                'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat4
            ];
        }
        if($tipe_sertifikat->isTipe5Active == 1){
            $tipe_sertifikats[4] = [
                'id' => '5',
                'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat5
            ];
        }
        if($tipe_sertifikat->isTipe6Active == 1){
            $tipe_sertifikats[5] = [
                'id' => '6',
                'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat6
            ];
        }
        if($tipe_sertifikat->isTipe7Active == 1){
            $tipe_sertifikats[6] = [
                'id' => '7',
                'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat7
            ];
        }
        if($tipe_sertifikat->isTipe8Active == 1){
            $tipe_sertifikats[7] = [
                'id' => '8',
                'tipe_sertifikat' => $tipe_sertifikat->tipe_sertifikat8
            ];
        }

        $data['list_tipe_sertifikat'] = $tipe_sertifikats;
        return $this->respondWithDataAndMessage($tipe_sertifikats, "Success");
    }

    public function postJenisSertifikat(){
        $jenis_sertifikat = DB::table('cb_config_jenis_sertifikat')
                            ->whereNull('deleted_at')
                            ->where('id_cobrand','1')
                            ->orderBy('id','desc')
                            ->first();
        $jenis_sertifikats = [];
        if($jenis_sertifikat->isJenis1Active == 1){
            $jenis_sertifikats[0] = [
                'id' => '1',
                'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat1
            ];
        }
        if($jenis_sertifikat->isJenis2Active == 1){
            $jenis_sertifikats[1] = [
                'id' => '2',
                'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat2
            ];
        }
        if($jenis_sertifikat->isJenis3Active == 1){
            $jenis_sertifikats[2] = [
                'id' => '3',
                'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat3
            ];
        }
        if($jenis_sertifikat->isJenis4Active == 1){
            $jenis_sertifikats[3] = [
                'id' => '4',
                'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat4
            ];
        }
        if($jenis_sertifikat->isJenis5Active == 1){
            $jenis_sertifikats[4] = [
                'id' => '5',
                'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat5
            ];
        }
        if($jenis_sertifikat->isJenis6Active == 1){
            $jenis_sertifikats[5] = [
                'id' => '6',
                'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat6
            ];
        }
        if($jenis_sertifikat->isJenis7Active == 1){
            $jenis_sertifikats[6] = [
                'id' => '7',
                'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat7
            ];
        }
        if($jenis_sertifikat->isJenis8Active == 1){
            $jenis_sertifikats[7] = [
                'id' => '8',
                'jenis_sertifikat' => $jenis_sertifikat->jenis_sertifikat8
            ];
        }

        $data['list_jenis_sertifikat'] = $jenis_sertifikats;
        return $this->respondWithDataAndMessage($jenis_sertifikats, "Success");
    }

    public function postHistoryReferral(){
        $agen = Auth::user();
        $agen_id = $agen->id;
        /* $respondAgenAktif = $this->validasiAgenAktif();
        if( $respondAgenAktif != NULL){
            return $respondAgenAktif;
            exit();
        } */		
        $status_pengajuan = Request::get('status_pengajuan');
        $no_pengajuan = Request::get('no_pengajuan');
        if(Request::get('start_date') == NULL && Request::get('end_date') == NULL){
            $start_date = Request::get('start_date');
            $end_date = Request::get('end_date');
        }else{
            $start_date = "'".Request::get('start_date')."'";
            $end_date = "'".Request::get('end_date')."'";
        }
        $is_motor = Request::get('is_motor');
        $is_mobil = Request::get('is_mobil');
        $is_sertifikat = Request::get('is_sertifikat');
        $is_deposito = Request::get('is_deposito');
        if($is_motor == '1' || $is_mobil == '1' || $is_sertifikat == '1' || $is_deposito == '1'){
			$motor = $is_motor != '1' ? '0' : 'motor';
			$mobil = $is_mobil != '1' ? '0' : 'mobil';
			$sertifikat = $is_sertifikat != '1' ? '0' : 'sertifikat';
			$deposito = $is_deposito != '1' ? '0' : 'deposito';
			$is_referral = "'".$motor."'".','."'".$mobil."'".','."'".$sertifikat."'".','."'".$deposito."'";
        }

        /* dd('exec getHistoryAllReferral ?,?,?,?,?,?', 
                                            array($agen_id,$no_pengajuan, $start_date, $end_date, $status_pengajuan, $is_referral)); */
        $transactionsReferrals = DB::select('exec getHistoryAllReferral ?,?,?,?,?,?', 
                                            array($agen_id,$no_pengajuan, $start_date, $end_date, $status_pengajuan, $is_referral));

        //dd($transactionsSP);
        $data=[];
        foreach ($transactionsReferrals as $transactionsReferral) {
			$tanggal_pengajuan = DB::table('cb_txn_pengajuan')
								 ->where('kode_pengajuan', $transactionsReferral->kode_pengajuan)
								 ->first();
            $data[] = [
				'id'=>$transactionsReferral->id,
                'nama_nasabah'=>$transactionsReferral->nama_nasabah,
                'kode_pengajuan'=>$transactionsReferral->kode_pengajuan,
                'type_referral'=>$transactionsReferral->type_referral,
                'nilai_pengajuan'=>$transactionsReferral->nilai_pengajuan == NULL ? '0' : $transactionsReferral->nilai_pengajuan,
                'nilai_pencairan'=>$transactionsReferral->nilai_pencairan == NULL ? '0' : $transactionsReferral->nilai_pencairan,
                'tenor'=>$transactionsReferral->tenor,
                'suku_bunga_per_tahun'=>$transactionsReferral->suku_bunga_per_tahun,
                'estimasi_saldo_akhir'=>$transactionsReferral->estimasi_saldo_akhir,
                'status_pengajuan'=>$transactionsReferral->status_pengajuan,
                'created_at'=>$tanggal_pengajuan->created_at,
				'insentif'=>$transactionsReferral->total_insentif == NULL ? '0' : $transactionsReferral->total_insentif
            ];
        }
        return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function getReferralById($id){
        $getReferral = DB::table('cb_txn_pengajuan AS txn')
                        ->leftJoin('cb_txn_insentif AS ti','ti.id_pengajuan', '=', 'txn.id')
                        ->select('txn.id','txn.kode_pengajuan','txn.id_i_provinsi','txn.id_i_kabupaten','txn.id_i_kecamatan','txn.id_i_kelurahan','txn.id_i_kodepos','txn.id_i_provinsi_jaminan','txn.id_i_kabupaten_jaminan','txn.id_i_kecamatan_jaminan','txn.id_i_kelurahan_jaminan','txn.id_i_kodepos_jaminan','txn.kode_pengajuan','txn.type_referral','txn.nilai_pengajuan','txn.tenor','txn.tenor_disetujui','txn.suku_bunga_per_tahun','txn.suku_bunga_disetujui','txn.estimasi_saldo_akhir','txn.nilai_pencairan','txn.insentif','txn.status_pengajuan','txn.remark_rejected','txn.brand_kendaraan','txn.tipe_kendaraan','txn.tahun_kendaraan','txn.masa_berlaku_pajak','txn.tipe_sertifikat','txn.jenis_jaminan','txn.nama_nasabah','txn.no_hp_nasabah','txn.no_ktp_nasabah','txn.alamat_nasabah','txn.jaminan_kendaraan','txn.estimasi_angsuran','txn.minimal_pendapatan','txn.dokumen1_name','txn.dokumen2_name','txn.dokumen3_name','txn.dokumen4_name','txn.dokumen5_name','txn.dokumen1_file','txn.dokumen2_file','txn.dokumen3_file','txn.dokumen4_file','txn.dokumen5_file','txn.alamat_jaminan','txn.created_at','txn.nama_cobrand','txn.foto_ktp','txn.foto_selfie','ti.total_insentif')
                        ->where('txn.id',$id)
                        ->first();
		
		$tanggal_pengajuan = DB::table('cb_txn_pengajuan')
								 ->where('kode_pengajuan', $getReferral->kode_pengajuan)
								 ->first();

        $provinsi_peminjam = DB::table('i_provinsi')->where('id',$getReferral->id_i_provinsi)->first()->name;
        $kabupaten_peminjam = DB::table('i_kabupaten')->where('id',$getReferral->id_i_kabupaten)->first()->name;
        $kecamatan_peminjam = DB::table('i_kecamatan')->where('id',$getReferral->id_i_kecamatan)->first()->name;
        $kelurahan_peminjam = DB::table('i_kelurahan')->where('id',$getReferral->id_i_kelurahan)->first()->name;
        $kodepos_peminjam = DB::table('i_kodepos')->where('id',$getReferral->id_i_kodepos)->first()->kodepos;

        $data['kode_pengajuan'] = $getReferral->kode_pengajuan;
        $data['type_referral'] = $getReferral->type_referral;
        $data['nilai_pengajuan'] = $getReferral->nilai_pengajuan == NULL ? '0' : $getReferral->nilai_pengajuan;
        $data['tenor'] = $getReferral->tenor;
        $data['tenor_disetujui'] = $getReferral->tenor_disetujui;
        $data['suku_bunga_per_tahun'] = $getReferral->suku_bunga_per_tahun;
        $data['suku_bunga_disetujui'] = $getReferral->suku_bunga_disetujui;
        $data['estimasi_saldo_akhir'] = $getReferral->estimasi_saldo_akhir;
        $data['nilai_pencairan'] = $getReferral->nilai_pencairan == NULL ? 0 : $getReferral->nilai_pencairan;
        $data['insentif'] = $getReferral->total_insentif == NULL ? 0 : $getReferral->total_insentif;
        $data['status_pengajuan'] = $getReferral->status_pengajuan;
        $data['remark_rejected'] = $getReferral->remark_rejected;
        $data['brand_kendaraan'] = $getReferral->brand_kendaraan;
        $data['tipe_kendaraan'] = $getReferral->tipe_kendaraan;
        $data['tahun_kendaraan'] = $getReferral->tahun_kendaraan;
        $data['masa_berlaku_pajak'] = $getReferral->masa_berlaku_pajak;        
        $data['tipe_sertifikat'] = $getReferral->tipe_sertifikat;
        $data['jenis_jaminan'] = $getReferral->jenis_jaminan;
        $data['nama_nasabah'] = $getReferral->nama_nasabah;
        $data['no_hp_nasabah'] = $getReferral->no_hp_nasabah;
        $data['no_ktp_nasabah'] = $getReferral->no_ktp_nasabah;
        //$data['alamat_nasabah'] = $getReferral->alamat_nasabah.' '.$kecamatan_peminjam.' '.$kabupaten_peminjam.' '.$provinsi_peminjam.' '.$kodepos_peminjam;
		$data['alamat_nasabah'] = $getReferral->alamat_nasabah;
        
        $data['jaminan_kendaraan'] = $getReferral->jaminan_kendaraan;
        $data['estimasi_angsuran'] = $getReferral->estimasi_angsuran;
        $data['minimal_pendapatan'] = $getReferral->minimal_pendapatan;

        $data['provinsi_peminjam'] = $provinsi_peminjam == NULL ? '' : $provinsi_peminjam;
        $data['kabupaten_peminjam'] = $kabupaten_peminjam == NULL ? '' : $kabupaten_peminjam;
        $data['kecamatan_peminjam'] = $kecamatan_peminjam == NULL ? '' : $kecamatan_peminjam;
        $data['kelurahan_peminjam'] = $kelurahan_peminjam == NULL ? '' : $kelurahan_peminjam;
        $data['kodepos_peminjam'] = $kodepos_peminjam == NULL ? '' : $kodepos_peminjam;

        $data['dokumen1_label'] = $getReferral->dokumen1_name == NULL ? '' : $getReferral->dokumen1_name;
        $data['dokumen2_label'] = $getReferral->dokumen2_name == NULL ? '' : $getReferral->dokumen2_name;
        $data['dokumen3_label'] = $getReferral->dokumen3_name == NULL ? '' : $getReferral->dokumen3_name;
        $data['dokumen4_label'] = $getReferral->dokumen4_name == NULL ? '' : $getReferral->dokumen4_name;
        $data['dokumen5_label'] = $getReferral->dokumen5_name == NULL ? '' : $getReferral->dokumen5_name;
        $data['dokumen1_file'] = config('app.esta_ftp').$getReferral->dokumen1_file;
        $data['dokumen2_file'] = config('app.esta_ftp').$getReferral->dokumen2_file;
        $data['dokumen3_file'] = config('app.esta_ftp').$getReferral->dokumen3_file;
        $data['dokumen4_file'] = config('app.esta_ftp').$getReferral->dokumen4_file;
        $data['dokumen5_file'] = config('app.esta_ftp').$getReferral->dokumen5_file;

        $provinsi_jaminan = DB::table('i_provinsi')->where('id',$getReferral->id_i_provinsi_jaminan)->first()->name;
        $kabupaten_jaminan = DB::table('i_kabupaten')->where('id',$getReferral->id_i_kabupaten_jaminan)->first()->name;
        $kecamatan_jaminan = DB::table('i_kecamatan')->where('id',$getReferral->id_i_kecamatan_jaminan)->first()->name;
        $kelurahan_jaminan = DB::table('i_kelurahan')->where('id',$getReferral->id_i_kelurahan_jaminan)->first()->name; 
        $kodepos_jaminan = DB::table('i_kodepos')->where('id',$getReferral->id_i_kodepos_jaminan)->first()->kodepos;
		
		//$data['alamat_jaminan'] = $getReferral->alamat_jaminan.' '.$kecamatan_jaminan.' '.$kabupaten_jaminan.' '.$provinsi_jaminan.' '.$kodepos_jaminan;
		$data['alamat_jaminan'] = $getReferral->alamat_jaminan == NULL ? '' : $getReferral->alamat_jaminan;
        $data['provinsi_jaminan'] = $provinsi_jaminan == NULL ? '' : $provinsi_jaminan;
        $data['kabupaten_jaminan'] = $kabupaten_jaminan == NULL ? '' : $kabupaten_jaminan;
        $data['kecamatan_jaminan'] = $kecamatan_jaminan == NULL ? '' : $kecamatan_jaminan;
        $data['kelurahan_jaminan'] = $kelurahan_jaminan == NULL ? '' : $kelurahan_jaminan;
        $data['kodepos_jaminan'] = $kodepos_jaminan == NULL ? '' : $kodepos_jaminan;
		
		//$logs_history = DB::table('cb_log_txn_pengajuan')->where('id_pengajuan',$getReferral->id)->orderby('updated_at','desc')->get();
		
		$getIdPengajuanLogs = DB::table('cb_txn_pengajuan')->where('kode_pengajuan',$getReferral->kode_pengajuan)->get();
        
        $id_pengajuanarr = [];
        foreach ($getIdPengajuanLogs as $value) {
            $id_pengajuan = $value->id;
            array_push($id_pengajuanarr,$id_pengajuan);
        }
        $logs_history = DB::table('cb_log_txn_pengajuan')
                            ->select('cb_log_txn_pengajuan.id_pengajuan as id_pengajuan'
                                    ,'cb_log_txn_pengajuan.status_pengajuan as status_pengajuan'
                                    ,'cb_log_txn_pengajuan.updated_user as updated_user'
                                    ,'cb_log_txn_pengajuan.updated_at as updated_at'
                                    ,'cb_txn_pengajuan.type_referral as type_referral'
                                    ,'cb_txn_pengajuan.nama_cobrand as nama_cobrand'
                                    ,'cb_txn_pengajuan.nama_branch as nama_branch'
                                    ,'cb_txn_pengajuan.created_at as created_at'
									,'cb_log_txn_pengajuan.nama_recobrand as nama_recobrand'
									,'cb_log_txn_pengajuan.nama_rebranch as nama_rebranch')
                            ->join('cb_txn_pengajuan', 'cb_txn_pengajuan.id', '=', 'cb_log_txn_pengajuan.id_pengajuan')
                            ->whereIn('cb_log_txn_pengajuan.id_pengajuan', $id_pengajuanarr)
                            ->orderBy('cb_log_txn_pengajuan.updated_at','desc')->get(); 
							
		$count_logs_history = $logs_history->count();
        $log_history = [];
		$i = 0;
        foreach ($logs_history as $value) {
			$i++;
            if($value->status_pengajuan == 1){
                $status_pengajuan = 'Pengajuan Baru';
            }elseif($value->status_pengajuan == 2){
                $status_pengajuan = 'Pengajuan telah di Assign';
            }elseif($value->status_pengajuan == 3){
                $status_pengajuan = 'Dalam Proses Survey';
            }elseif($value->status_pengajuan == 4){
                $status_pengajuan = 'Pengajuan Disetujui';
            }elseif($value->status_pengajuan == 5){
                $status_pengajuan = 'Pengajuan Sukses';
            }elseif($value->status_pengajuan == 6){
                $status_pengajuan = 'Pengajuan Selesai';
            }elseif($value->status_pengajuan == 7){
                $status_pengajuan = 'Pengajuan di Reject';
            }elseif($value->status_pengajuan == 9){
                $status_pengajuan = 'Pengajuan di Re-Assign';
            }
			if($value->nama_cobrand == NULL){
				$value->nama_cobrand = '';
			}
			if($value->nama_branch == NULL){
				$value->nama_branch = '';
			}
			
            $log_history[] = [
                'id' => $value->id,
                'status_pengajuan_id' => $getReferral->status_pengajuan,
                'status_pengajuan' => $status_pengajuan,
                'updated_user' => $value->updated_user == NULL ? $getReferral->type_referral : 'Di Update oleh '.$value->updated_user,
                'cobrand' => $value->status_pengajuan == 9 ? $value->nama_recobrand : $value->nama_cobrand,
                'branch' => $value->status_pengajuan == 9 ? 'Ke '.$value->nama_rebranch : $value->nama_branch,
                'updated_at' => $value->updated_at == NULL ? date_format( new DateTime($getReferral->created_at), 'Y-m-d H:i:s' ) : date_format( new DateTime($value->updated_at), 'Y-m-d H:i:s' ), 
				'limit_size' => $count_logs_history
            ];
        }
        $data['history'] = $log_history;
		$data['tanggal_submit'] =date_format( new DateTime($tanggal_pengajuan->created_at), 'Y-m-d H:i:s' );
		$data['perusahaan_perdana'] = $getReferral->nama_cobrand;
		$data['foto_ktp'] = $getReferral->foto_ktp == NULL ? '' : config('app.esta_ftp').$getReferral->foto_ktp;
		$data['selfie_ktp'] = $getReferral->foto_selfie == NULL ? '' : config('app.esta_ftp').$getReferral->foto_selfie;		

        return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function getFAQ(){
        $faqs = DB::table('cb_config_faq')
                ->whereNull('deleted_at')
                ->get();
        $datas = [];
        foreach ($faqs as $faq) {
            $datas[] = [
                'id' => $faq->id,
                'kode_faq' => $faq->kode_faq,
                'nama_faq' => $faq->nama_faq,
                'desc' => $faq->desc
            ];
        }
        return $this->respondWithDataAndMessage($datas, 'Success');
    }
	
	public function postRiwayat(){
        $agen = Auth::user();
        $id_agen = $agen->id;
        $periode = Request::get('periode');

        $nilai_pencairan = DB::select('exec ReferralNilaiPencairan ?,?', array($id_agen, $periode));

        $grafik_komposisi = DB::select('exec ReferralGrafikKomposisi ?,?', array($id_agen, $periode));


        $data['nilai_pencairan'] = ($nilai_pencairan[0]->nilai_pencairan == NULL ? '0' : $nilai_pencairan[0]->nilai_pencairan);
        $data['kontrak_nilai_pencairan'] = $nilai_pencairan[0]->kontrak_nilai_pencairan;
        $data['pending_pencairan'] = ($nilai_pencairan[0]->pending_pencairan == NULL ? '0' : $nilai_pencairan[0]->pending_pencairan);
        $data['kontrak_pending_pencairan'] = $nilai_pencairan[0]->kontrak_pending_pencairan;

        $grafik_komposisi_data = [];
        foreach ($grafik_komposisi as $value) {
            $grafik_komposisi_data[] = [
                'inew' => $value->New,
                'assign' => $value->Assign,
                'survey' => $value->Survey,
                'disetujui' => $value->Disetujui,
                'sukses' => $value->Sukses,
                'pencairan' => $value->Pencairan,
                'ditolak' => $value->Ditolak
            ];
        }

        $data['grafik_komposisi_referral'] = $grafik_komposisi_data;

        //---------------------------------------------------------------------------------------------------------------------

        $grafik_produk_motor_sp = DB::select('exec ReferralGrafikPerProduk ?,?,?', array($id_agen, $periode, 'motor'));
        $grafik_produk_mobil_sp = DB::select('exec ReferralGrafikPerProduk ?,?,?', array($id_agen, $periode, 'mobil'));
        $grafik_produk_sertifikat_sp = DB::select('exec ReferralGrafikPerProduk ?,?,?', array($id_agen, $periode, 'sertifikat'));
        $grafik_produk_deposito_sp = DB::select('exec ReferralGrafikPerProduk ?,?,?', array($id_agen, $periode, 'deposito'));

        $grafik_produk_motor['inew'] = $grafik_produk_motor_sp[0]->New;
        $grafik_produk_motor['assign'] = $grafik_produk_motor_sp[0]->Assign;
        $grafik_produk_motor['survey'] = $grafik_produk_motor_sp[0]->Survey;
        $grafik_produk_motor['disetujui'] = $grafik_produk_motor_sp[0]->Disetujui;
        $grafik_produk_motor['sukses'] = $grafik_produk_motor_sp[0]->Sukses;
        $grafik_produk_motor['pencairan'] = $grafik_produk_motor_sp[0]->Pencairan;
        $grafik_produk_motor['ditolak'] = $grafik_produk_motor_sp[0]->Ditolak;

        $grafik_pengajuan_per_produk[] = [
            'id' => 1,
            'nama' => 'motor',
            //'data' => $grafik_produk_motor,
			'inew' => $grafik_produk_motor_sp[0]->New,
			'assign' => $grafik_produk_motor_sp[0]->Assign,
			'survey' => $grafik_produk_motor_sp[0]->Survey,
			'disetujui' => $grafik_produk_motor_sp[0]->Disetujui,
            'sukses' => $grafik_produk_motor_sp[0]->Sukses,
			'pencairan' => $grafik_produk_motor_sp[0]->Pencairan,
            'ditolak' => $grafik_produk_motor_sp[0]->Ditolak,
        ];

        $grafik_produk_mobil['inew'] = $grafik_produk_mobil_sp[0]->New;
        $grafik_produk_mobil['assign'] = $grafik_produk_mobil_sp[0]->Assign;
        $grafik_produk_mobil['survey'] = $grafik_produk_mobil_sp[0]->Survey;
        $grafik_produk_mobil['disejutui'] = $grafik_produk_mobil_sp[0]->Disetujui;
        $grafik_produk_mobil['sukses'] = $grafik_produk_mobil_sp[0]->Sukses;
        $grafik_produk_mobil['pencairan'] = $grafik_produk_mobil_sp[0]->Pencairan;
        $grafik_produk_mobil['ditolak'] = $grafik_produk_mobil_sp[0]->Ditolak;

        $grafik_pengajuan_per_produk[] = [
            'id' => 2,
            'nama' => 'mobil',
            //'data' => $grafik_produk_mobil,
			'inew' => $grafik_produk_mobil_sp[0]->New,
			'assign' => $grafik_produk_mobil_sp[0]->Assign,
			'survey' => $grafik_produk_mobil_sp[0]->Survey,
			'disetujui' => $grafik_produk_mobil_sp[0]->Disetujui,
            'sukses' => $grafik_produk_mobil_sp[0]->Sukses,
			'pencairan' => $grafik_produk_mobil_sp[0]->Pencairan,
            'ditolak' => $grafik_produk_mobil_sp[0]->Ditolak
        ];

        $grafik_produk_sertifikat['inew'] = $grafik_produk_sertifikat_sp[0]->New;
        $grafik_produk_sertifikat['assign'] = $grafik_produk_sertifikat_sp[0]->Assign;
        $grafik_produk_sertifikat['survey'] = $grafik_produk_sertifikat_sp[0]->Survey;
        $grafik_produk_sertifikat['disetujui'] = $grafik_produk_sertifikat_sp[0]->Disetujui;
        $grafik_produk_sertifikat['sukses'] = $grafik_produk_sertifikat_sp[0]->Sukses;
        $grafik_produk_sertifikat['pencairan'] = $grafik_produk_sertifikat_sp[0]->Pencairan;
        $grafik_produk_sertifikat['ditolak'] = $grafik_produk_sertifikat_sp[0]->Ditolak;

        $grafik_pengajuan_per_produk[] = [
            'id' => 3,
            'nama' => 'sertifikat',
            //'data' => $grafik_produk_sertifikat,
			'inew' => $grafik_produk_sertifikat_sp[0]->New,
			'assign' => $grafik_produk_sertifikat_sp[0]->Assign,
			'survey' => $grafik_produk_sertifikat_sp[0]->Survey,
			'disetujui' => $grafik_produk_sertifikat_sp[0]->Disetujui,
            'sukses' => $grafik_produk_sertifikat_sp[0]->Sukses,
			'pencairan' => $grafik_produk_sertifikat_sp[0]->Pencairan,
            'ditolak' => $grafik_produk_sertifikat_sp[0]->Ditolak
        ];

        $grafik_produk_deposito['inew'] = $grafik_produk_deposito_sp[0]->New;
        $grafik_produk_deposito['assign'] = $grafik_produk_deposito_sp[0]->Assign;
        $grafik_produk_deposito['survey'] = $grafik_produk_deposito_sp[0]->Survey;
        $grafik_produk_deposito['disetujui'] = $grafik_produk_deposito_sp[0]->Disetujui;
        $grafik_produk_deposito['sukses'] = $grafik_produk_deposito_sp[0]->Sukses;
        $grafik_produk_deposito['pencairan'] = $grafik_produk_deposito_sp[0]->Pencairan;
        $grafik_produk_deposito['ditolak'] = $grafik_produk_deposito_sp[0]->Ditolak;

        $grafik_pengajuan_per_produk[] = [
            'id' => 4,
            'nama' => 'deposito',
            //'data' => $grafik_produk_deposito,
			'inew' => $grafik_produk_deposito_sp[0]->New,
			'assign' => $grafik_produk_deposito_sp[0]->Assign,
			'survey' => $grafik_produk_deposito_sp[0]->Survey,
			'disetujui' => $grafik_produk_deposito_sp[0]->Disetujui,
            'sukses' => $grafik_produk_deposito_sp[0]->Sukses,
			'pencairan' => $grafik_produk_deposito_sp[0]->Pencairan,
            'ditolak' => $grafik_produk_deposito_sp[0]->Ditolak,
        ];

        $data['grafik_pengajuan_per_produk'] = $grafik_pengajuan_per_produk;

        //---------------------------------------------------------------------------------------------------------
        
        $grafik_produk_disetujui_sp = DB::select('exec ReferralGrafikDisetujui ?', array($id_agen));     

        if($periode == 'jan-mar'){
            $grafik_produk_disetujui[] = [
                'id' => 1,
                'nama' => 'januari',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_januari,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_januari
            ];
            $grafik_produk_disetujui[] = [
                'id' => 2,
                'nama' => 'februari',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_februari,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_februari
            ];
            $grafik_produk_disetujui[] = [
                'id' => 3,
                'nama' => 'maret',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_maret,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_maret
            ];
        }elseif ($periode == 'apr-jun') {
            $grafik_produk_disetujui[] = [
                'id' => 1,
                'nama' => 'april',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_april,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_april
            ];
            $grafik_produk_disetujui[] = [
                'id' => 2,
                'nama' => 'mei',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_mei,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_mei
            ];
            $grafik_produk_disetujui[] = [
                'id' => 3,
                'nama' => 'juni',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_juni,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_juni
            ];
        }elseif($periode == 'jul-sep'){
            $grafik_produk_disetujui[] = [
                'id' => 1,
                'nama' => 'juli',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_juli,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_juli
            ];
            $grafik_produk_disetujui[] = [
                'id' => 2,
                'nama' => 'agustus',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_agustus,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_agustus
            ];
            $grafik_produk_disetujui[] = [
                'id' => 3,
                'nama' => 'september',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_september,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_september
            ];
        }elseif($periode == 'okt-des'){
            $grafik_produk_disetujui[] = [
                'id' => 1,
                'nama' => 'oktober',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_oktober,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_oktober
            ];
            $grafik_produk_disetujui[] = [
                'id' => 2,
                'nama' => 'november',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_november,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_november
            ];
            $grafik_produk_disetujui[] = [
                'id' => 3,
                'nama' => 'desember',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_desember,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_desember
            ];
        }elseif($periode == 'current_month'){
			$current_month = Carbon::now()->month;
            if($current_month == 1){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'januari',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_januari,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_januari
                ];
            }elseif($current_month == 2){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'februari',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_februari,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_februari
                ];
            }elseif($current_month == 3){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'maret',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_maret,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_maret
                ];
            }elseif($current_month == 4){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'april',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_april,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_april
                ];
            }elseif($current_month == 5){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'mei',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_mei,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_mei
                ];
            }elseif($current_month == 6){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'juni',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_juni,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_juni
                ];
            }elseif($current_month == 7){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'juli',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_juli,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_juli
                ];
            }elseif($current_month == 8){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'agustus',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_agustus,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_agustus
                ];
            }elseif($current_month == 9){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'september',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_september,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_september
                ];
            }elseif($current_month == 10){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'oktober',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_oktober,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_oktober
                ];
            }elseif($current_month == 11){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'november',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_november,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_november
                ];
            }elseif($current_month == 12){
                $grafik_produk_disetujui[] = [
                    'id' => 1,
                    'nama' => 'desember',
                    'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_desember,
                    'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_desember
                ];
            }
        }else{
            $grafik_produk_disetujui[] = [
                'id' => 1,
                'nama' => 'januari',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_januari,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_januari
            ];
            $grafik_produk_disetujui[] = [
                'id' => 2,
                'nama' => 'februari',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_februari,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_februari
            ];
            $grafik_produk_disetujui[] = [
                'id' => 3,
                'nama' => 'maret',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_maret,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_maret
            ];
            $grafik_produk_disetujui[] = [
                'id' => 4,
                'nama' => 'april',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_april,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_april
            ];
            $grafik_produk_disetujui[] = [
                'id' => 5,
                'nama' => 'mei',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_mei,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_mei
            ];
            $grafik_produk_disetujui[] = [
                'id' => 6,
                'nama' => 'juni',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_juni,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_juni
            ];
            $grafik_produk_disetujui[] = [
                'id' => 7,
                'nama' => 'juli',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_juli,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_juli
            ];
            $grafik_produk_disetujui[] = [
                'id' => 8,
                'nama' => 'agustus',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_agustus,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_agustus
            ];
            $grafik_produk_disetujui[] = [
                'id' => 9,
                'nama' => 'september',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_september,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_september
            ];
            $grafik_produk_disetujui[] = [
                'id' => 10,
                'nama' => 'oktober',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_oktober,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_oktober
            ];
            $grafik_produk_disetujui[] = [
                'id' => 11,
                'nama' => 'november',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_november,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_november
            ];
            $grafik_produk_disetujui[] = [
                'id' => 12,
                'nama' => 'desember',
                'total_aplikasi' => $grafik_produk_disetujui_sp[0]->total_aplikasi_desember,
                'aplikasi_disetujui' => $grafik_produk_disetujui_sp[0]->aplikasi_disetujui_desember
            ];
        }

        $data['grafik_produk_disetujui'] = $grafik_produk_disetujui;

        return $this->respondWithDataAndMessage($data, 'Success');
        
    }
	
	public function getSyaratKetentuanReferral(){
        $type_referral = Request::get('type_referral');

        $sk = DB::table('cb_config_sk')->where('type_referral', $type_referral)->first();

        $response['api_status']  = 1;
        $response['api_message'] = 'Sukses';
        $response['type_dialog']  = 'Informasi';
        $response['content'] = $sk->desc;

        return response()->json($response);
    }
	
	/* public function postOCR(){
        $response = Request::get('response');
        $img_decode = base64_decode(Request::get('image'));
        $file_name_ocr = 'OCR/ocr-'.uniqid().".jpg";
        Storage::disk('ftp')->put($file_name_ocr, $img_decode);

		$response_jsonEncode = json_decode( $response,true );
        //dd($response_jsonEncode['message']);
        $sv_ocr['req'] = $file_name_ocr;
        $sv_ocr['res'] = $response;
        $sv_ocr['created_at'] = date('Y-m-d H:i:s');
        $sv_ocr['updated_at'] = date('Y-m-d H:i:s');
        $save_to_db = DB::table('log_ocr')->insert($sv_ocr);        
        if($response_jsonEncode['type'] == 'ktp'){
            return $this->respondWithSuccess("Success");
        }else{
            return $this->respondWithError($response_jsonEncode['message']);
        }       
    
    } */
	
	public function postOCR(){
		$agen = Auth::user();
        $id_agen = $agen->id;
		
		$agen = DB::table('agen')->where('id',$id_agen)->first();

        $serviceURLOCR = 'http://ai.inergi.id:8001/document_classifier';

        $request = [
          'key' => Request::get('key'),
          'image' => Request::get('image'),
          'return_segmented' => Request::get('return_segmented'),
          'return_person_photo' => Request::get('return_person_photo'),
          'return_signature_photo' => Request::get('return_signature_photo')
        ];

        $request_toJson = json_encode( $request );
        $ch = curl_init( $serviceURLOCR );
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $request_toJson );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
          )
        );
        $response = curl_exec( $ch );
        $responseBody = json_decode( $response,true );
        
        $img_decode = base64_decode(Request::get('image'));
        $file_name_ocr = 'OCR/ocr-'.uniqid().".jpg";
        Storage::disk('ftp')->put($file_name_ocr, $img_decode);

        $sv_ocr['req'] = $file_name_ocr;
        $sv_ocr['res'] = $response;
        $sv_ocr['created_at'] = date('Y-m-d H:i:s');
        $sv_ocr['updated_at'] = date('Y-m-d H:i:s');
		$sv_ocr['id_agen'] = $id_agen;
		$sv_ocr['kode_agen'] = $agen->kode;
		$sv_ocr['nama_agen'] = $agen->nama;
		$sv_ocr['no_hp_agen'] = $agen->no_hp;
        $save_to_db = DB::table('log_ocr')->insert($sv_ocr);

        $id_provinsi = DB::table('i_provinsi')->where('name','like','%'.$responseBody['data']['Provinsi']['value'].'%' )->first()->id;
        if($id_provinsi && $responseBody['data']['Kabupaten/Kota']['value'] != NULL){
            $id_kabupaten = DB::table('i_kabupaten')
                            ->where('name','like','%'.$responseBody['data']['Kabupaten/Kota']['value'].'%' )
                            ->where('id_i_provinsi',$id_provinsi)
                            ->first()->id;
            if($id_kabupaten && $responseBody['data']['Kecamatan']['value'] != NULL){
                $id_kecamatan = DB::table('i_kecamatan')
                                ->where('name','like','%'.$responseBody['data']['Kecamatan']['value'].'%' )
                                ->where('id_i_kabupaten',$id_kabupaten)
                                ->first()->id;
				if(!empty($id_kecamatan)){
					$id_kecamatan = trim($id_kecamatan);
				} 
                if($id_kecamatan && $responseBody['data']['Kel/Desa']['value'] != NULL){
                    $id_kelurahan = DB::table('i_kelurahan')
                                    ->where('name','like','%'.$responseBody['data']['Kel/Desa']['value'].'%' )
                                    ->where('id_i_kecamatan',$id_kecamatan)
                                    ->first()->id;
                    $kodepos = DB::table('i_kodepos')
                                ->select('id', 'kodepos')
                                ->where('id_i_kecamatan', $id_kecamatan)
                                ->first();
                }
            }
        }
        $responseBody['data']['Provinsi']['id'] = $id_provinsi == null ? '' : $id_provinsi;
        $responseBody['data']['Kabupaten/Kota']['id'] = $id_kabupaten == null ? '' : $id_kabupaten;
        $responseBody['data']['Kecamatan']['id'] = $id_kecamatan == null ? '' : $id_kecamatan;
        $responseBody['data']['Kel/Desa']['id'] = $id_kelurahan == null ? '' : $id_kelurahan;
        $responseBody['data']['Kodepos'] = [
            'id' => $kodepos->id == null ? '' : $kodepos->id,
            'value' => $kodepos->kodepos == null ? '' : $kodepos->kodepos
        ];

        return $responseBody;
    }

    function riwayatPendingInsentif(){
        $agen = Auth::user();
        $id_agen = $agen->id;        
        $agen = DB::table('agen')->where('id',$id_agen)->first();
        $periode = Request::get('periode');

		$monthJan = Carbon::now()->startOfMonth();		
		$monthApr = Carbon::now()->startOfMonth()->addMonth(+3);
		$monthJul = Carbon::now()->startOfMonth()->addMonth(+6);
		$monthOkt = Carbon::now()->startOfMonth()->addMonth(+9);
		$monthDes = Carbon::now()->startOfMonth()->addMonth(+12);
		//dd(Carbon::now()->month);

		if($periode == 'jan-mar'){
			$pending_insentif = DB::table('cb_txn_insentif')                
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereBetween('created_at',[$monthJan,$monthApr])
                ->where(function ($query) {
                    $query->where('status', 0)
                          ->orWhere('status', 1);
                })
                ->orderBy('created_at','desc')
                ->get();
        }elseif($periode == 'apr-jun'){
			$pending_insentif = DB::table('cb_txn_insentif')                
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereBetween('created_at',[$monthApr,$monthJul])
                ->where(function ($query) {
                    $query->where('status', 0)
                          ->orWhere('status', 1);
                })
                ->orderBy('created_at','desc')
                ->get();
		}elseif($periode == 'jul-sep'){
			$pending_insentif = DB::table('cb_txn_insentif')                
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereBetween('created_at',[$monthJul,$monthOkt])
                ->where(function ($query) {
                    $query->where('status', 0)
                          ->orWhere('status', 1);
                })
                ->orderBy('created_at','desc')
                ->get();
        }elseif($periode == 'okt-des'){
			$pending_insentif = DB::table('cb_txn_insentif')                
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereBetween('created_at',[$monthOkt,$monthDes])
                ->where(function ($query) {
                    $query->where('status', 0)
                          ->orWhere('status', 1);
                })
                ->orderBy('created_at','desc')
                ->get();
		}elseif($periode == 'current_year'){
			$pending_insentif = DB::table('cb_txn_insentif')                
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereYear('created_at', Carbon::now()->year)
                ->where(function ($query) {
                    $query->where('status', 0)
                          ->orWhere('status', 1);
                })
                ->orderBy('created_at','desc')
                ->get();
        }elseif($periode == 'current_month'){
			$pending_insentif = DB::table('cb_txn_insentif')                
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')                
                ->whereYear('created_at', Carbon::now()->year)
                ->whereMonth('created_at', Carbon::now()->month)
                ->where(function ($query) {
                    $query->where('status', 0)
                          ->orWhere('status', 1);
                })
                ->orderBy('created_at','desc')
                ->get();        
		}else{
			$pending_insentif = DB::table('cb_txn_insentif')                
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->where(function ($query) {
                    $query->where('status', 0)
                          ->orWhere('status', 1);
                })
                ->orderBy('created_at','desc')
                ->get();
        }        

        $data = [];
        foreach ($pending_insentif as $value) {
            $data[] = [
                'id' => $value->id,
                'jenis_insentif' => $value->jenis_insentif,
                'nama_program' => $value->nama_program,
                'nama_agen' => $value->nama_agen,
                'level_agen' => $value->level_agen,
                'soa' => $value->soa,
                'nominal_insentif' => $value->nominal_insentif,
                'pajak' => $value->pajak,
                'total_insentif' => $value->total_insentif,
                'status' => $value->status,
                'total_kontrak' => $value->total_kontrak,
                'total_downline' => $value->total_downline,
                'created_at' => $value->created_at,
                'updated_at' => $value->updated_at
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }

    function riwayatInsentif(){
        $agen = Auth::user();
        $id_agen = $agen->id;        
        $agen = DB::table('agen')->where('id',$id_agen)->first();

        $periode = Request::get('periode');

		$monthJan = Carbon::now()->startOfMonth();		
		$monthApr = Carbon::now()->startOfMonth()->addMonth(+3);
		$monthJul = Carbon::now()->startOfMonth()->addMonth(+6);
		$monthOkt = Carbon::now()->startOfMonth()->addMonth(+9);
		$monthDes = Carbon::now()->startOfMonth()->addMonth(+12);
		//dd(Carbon::now()->month);

		if($periode == 'jan-mar'){
            $pending_insentif = DB::table('cb_txn_insentif')
                ->where('status',2)
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereBetween('created_at',[$monthJan,$monthApr])
                ->orderBy('created_at','desc')
                ->get();
        }elseif($periode == 'apr-jun'){
			$pending_insentif = DB::table('cb_txn_insentif')
                ->where('status',2)
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereBetween('created_at',[$monthApr,$monthJul])
                ->orderBy('created_at','desc')
                ->get();
		}elseif($periode == 'jul-sep'){
			$pending_insentif = DB::table('cb_txn_insentif')
                ->where('status',2)
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereBetween('created_at',[$monthJul,$monthOkt])
                ->orderBy('created_at','desc')
                ->get();
        }elseif($periode == 'okt-des'){
			$pending_insentif = DB::table('cb_txn_insentif')
                ->where('status',2)
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereBetween('created_at',[$monthOkt,$monthDes])
                ->orderBy('created_at','desc')
                ->get();
		}elseif($periode == 'current_year'){
            $pending_insentif = DB::table('cb_txn_insentif')
                ->where('status',2)
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereYear('created_at', Carbon::now()->year)
                ->orderBy('created_at','desc')
                ->get();
        }elseif($periode == 'current_month'){
            $pending_insentif = DB::table('cb_txn_insentif')
                ->where('status',2)
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->whereYear('created_at', Carbon::now()->year)
                ->whereMonth('created_at', Carbon::now()->month)
                ->orderBy('created_at','desc')
                ->get();      
		}else{
			$pending_insentif = DB::table('cb_txn_insentif')  
                ->where('status',2)              
                ->where('id_agen',$id_agen)
                ->whereNull('deleted_at')
                ->orderBy('created_at','desc')
                ->get();
        }

        $data = [];
        foreach ($pending_insentif as $value) {
            $data[] = [
                'id' => $value->id,
                'jenis_insentif' => $value->jenis_insentif,
                'nama_program' => $value->nama_program,
                'nama_agen' => $value->nama_agen,
                'level_agen' => $value->level_agen,
                'soa' => $value->soa,
                'nominal_insentif' => $value->nominal_insentif,
                'pajak' => $value->pajak,
                'total_insentif' => $value->total_insentif,
                'status' => $value->status,
                'total_kontrak' => $value->total_kontrak,
                'total_downline' => $value->total_downline,
                'created_at' => $value->created_at,
                'updated_at' => $value->updated_at,
                'tgl_transfer' => $value->tgl_transfer_insentif == null ? '' : $value->tgl_transfer_insentif 
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getDompetDigital(){
        $data = DB::table('cb_mst_ewallet')->select('id', 'nama')->where('id','!=',1)->whereNull('deleted_at')->orderBy('nama', 'asc')->get();

        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getDompetNotes(){
        $data = DB::table('cb_config_notes_ewallet')->select('id', 'notes')->get();

        return $this->respondWithDataAndMessage($data, "Success");
    }
}
