<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Request;
use Route;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;
use Storage;
use Esta;
use App;
use Illuminate\Support\Facades\Auth;

class BpjsAPI extends Controller
{
    
	public function postVoucherBpjs(){
		$agen = Auth::user();
        $id_agen = $agen->id;

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen));

		$arrays = DB::select('exec getVoucherWithParams ?,?', array($id_agen,'BPJS'));

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->voucher_nama;
			$rest['amount'] = $array->voucher_amount;
			$rest['masa_berlaku'] = date("Y-m-d H:i:s", strtotime($array->voucher_expired));
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postCheckTagihanBpjs(){
		$agen = Auth::user();
        $id_agen = $agen->id;
		$type = Request::get('type');
		$no_hp = Request::get('no_hp');
		//$id_agen = Request::get('id_agen');
		$id_pelanggan = Request::get('id_pelanggan');
		$bayar_hingga = Request::get('bayar_hingga');

		/*if($type == 'Kesehatan') {
			$id_product = 1020;
		} else {
			$id_product = 1020;
		}*/

		$tipe_layanan = DB::table('pan_tipe_layanan')->where('kode','13')->first()->id;
		$kode_layanan = DB::table('pan_kode_layanan')->where('kode','4')->first()->id;
		$id_product = DB::table('pan_product')->where('id_tipe_layanan',$tipe_layanan)->where('id_kode_layanan',$kode_layanan)->first()->id;

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen))[0];

		$d1 = $bayar_hingga;
		$d2 = date('Y-m-d');
		$jml_bulan = (int)abs((strtotime($d1) - strtotime($d2))/(60*60*24*30))+1;

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$kode = Esta::nomor_transaksi('trans_bpjs',CRUDBooster::getsetting('transaksi_bpjs'));

		$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);

		/*send iso*/
		$send_iso = Esta::send_iso_bpjsks($id_product,$no_hp,'0200','Inquiry',0,$id_pelanggan,$jml_bulan);
		//dd($send_iso);

		if($send_iso['39'] == '00') {
			$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$rest['id_transaksi'] = $send_iso['id_log'];

			$rest['bpjsks_nama'] = $send_iso['bpjsks_nama'];
			$rest['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
			$rest['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
			$rest['bpjsks_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0)).' Orang';
			$rest['periode'] = ltrim($send_iso['periode'],0).' BLN';
			$rest['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
			$rest['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
			$rest['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
			$rest['val_total_bayar'] = $rest['val_bpjsks_total_premi']+$rest['val_bpjsks_biaya_admin'];
			$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');

			$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
			$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');

			$rest['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
			$rest['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
			$rest['komisi'] = 'Rp '.number_format($rest['val_komisi'],0,',','.');

			$response['api_status']  = 1;
	    	$response['api_message'] = 'Berhasil';
	    	$response['type_dialog']  = 'Informasi';
	    	$response['item']  = $rest;
		} else {
			$rest['jml_tagihan'] = 0;
			$rest['komisi'] = 0;

			$response['api_status']  = 0;
	    	$response['api_message'] = Esta::show_error('BPJSKS',$send_iso['39'],'0');
	    	$response['type_dialog']  = 'Error';
	    	$response['item']  = $rest;
		}
		return response()->json($response);
	}

	public function postBulanBpjs() {
		$stop_date = date('Y-m-d', strtotime(date('Y-m-d') . ' +11 month'));

		$first = date('Y-m-d');
		$last = $stop_date;
		$step = '+1 month';
		$format = 'Y-m-d';
		$dates = array();
		$current = strtotime( $first );
		$last = strtotime( $last );

		$rest_json = array();
		while( $current <= $last ) {

			$dates[] = date( $format, $current );
			/*echo date( 'Y-m-d', $current );
			exit();*/
			$bulan = date( 'm', $current );
			$nama_bulan = Esta::date_indo($bulan,'singkat');
			$rest['date_value'] = $nama_bulan.' '.date( 'Y', $current );
			$rest['date'] = date( 'Y-m-d', $current );
			$current = strtotime( $step, $current );
			array_push($rest_json, $rest);
		}

		$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public static function postBpjsSubmit() {
		/*$agen = Auth::user();
        $id_agen = $agen->id;*/
        $id_agen = Request::get('id_agen');
        $request_transaksi = Request::get('request_transaksi');
        $transactionId = Request::get('transactionId');
        $request_transaksi = json_decode( $request_transaksi,true );
		//dd($request_transaksi['transactionId']);
		/*$agen = Auth::user();
        $id_agen = $agen->id;*/
        $oldRefNum = $request_transaksi['oldRefNum'];
        $saldo = $request_transaksi['saldo'];
		$regid = $request_transaksi['regid'];
		//$token = Request::get('token');
		//$id_agen = Request::get('id_agen');
		$cek_regid = Esta::cek_regid($id_agen,$regid);

        $getlog_transaction = DB::table('log_sobatku_payment')
            ->where('transactionId',$transactionId)
            ->where('keterangan','Request Payment')
            ->first();
        $oldRefNum = $getlog_transaction->ref_number;
        $saldo_sebelum = $getlog_transaction->saldo_sebelum;
		
		$ut['is_used'] = 1;
        DB::table('temp_trans_payment')
					->where('transactionId',$transactionId)
					->update($ut);

		/*if($cek_regid == 0) {
			$response['api_status']  = 2;
		    $response['api_message'] = 'Terjadi gangguan pada koneksi internet atau Server. Silahkan coba beberapa saat lagi';//'Akun Anda terdeteksi login di HP lain, Silahkan logout dan login kembali untuk melanjutkan transaksi.';
		    $response['type_dialog']  = 'Error';
		    //Esta::add_fraud($id_agen);
		    return response()->json($response);
		}*/

		$type = $request_transaksi['type'];
		$no_hp = $request_transaksi['no_hp'];
		$id_pelanggan = $request_transaksi['id_pelanggan'];
		$pin = $request_transaksi['pin'];
		$amount = $request_transaksi['amount'];
		//$id_agen = Request::get('id_agen');
		//$id_voucher = Request::get('id_voucher');
		$id_transaksi = $request_transaksi['id_transaksi'];

		$id_voucher_child = $request_transaksi['id_voucher'];
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$tipe_layanan = DB::table('pan_tipe_layanan')->where('kode','13')->first()->id;
		$kode_layanan = DB::table('pan_kode_layanan')->where('kode','4')->first()->id;
		$id_product = DB::table('pan_product')->where('id_tipe_layanan',$tipe_layanan)->where('id_kode_layanan',$kode_layanan)->first()->id;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		/*if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}*/

			$detail_voucher = DB::table('voucher')
				->where('id',$id_voucher)
				->first();

			$detail_product = DB::table('pan_product')
				->where('id',$id_product)
				->first();

			$kode = Esta::nomor_transaksi('trans_bpjs',CRUDBooster::getsetting('transaksi_bpjs'));

			/*$detail_inquiry = DB::table('log_jatelindo_bit')
				->where('id',$id_transaksi)
				->where('type','BPJS Kesehatan')
				->where('status','Inquiry')
				->where('jenis','res')
				->first();*/

			$detail_inquiry = DB::select('exec getLogJatelindoBitByParams ?,?,?,?', array($id_transaksi,'BPJS Kesehatan','Inquiry','res'))[0];

			$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);
			$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			
			$biaya_admin = $detail_inquiry->bpjsks_biaya_admin;
			
			if($trans_amount == 0) {
				$trans_amount = 0;
			} else {
				$trans_amount = $trans_amount+$biaya_admin;
			}
			$trans_amount = $trans_amount-$komisi;
			/*echo $trans_amount.'-'.($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

			Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi BPJS','Transaksi BPJS Kesehatan','Out','Transaksi');
			Esta::log_money($id_agen,($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),date('Y-m-d H:i:s'),'Komisi Transaksi BPJS','Komisi Transaksi BPJS Kesehatan','In','Komisi');
			exit();*/

			$jml_bulan = 1;
			$status_match_sobatku = 'Waiting Rekon';
			//if($saldo >= $trans_amount) {
				try{
					$created_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'BPJS Kesehatan';
					$currency = 'IDR';
					$trans_amount = $trans_amount;
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Pending';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_pelanggan = $id_pelanggan;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$id_voucher = $detail_voucher->id;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_bpjs');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					/*$save = DB::table('trans_bpjs')
						->insertGetId($sv);*/
					/*$saveTransBPJS = DB::select('exec postTransBPJS ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
						array($created_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$trans_amount,$komisi,$id_agen,$status,$rekon_amount,$status_match,$no_hp,$id_pelanggan,$id_product,$agen_nama,$agen_email,$agen_kode,$agen_no_hp,$agen_level,$agen_referall,$agen_referall_relation ,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan ,$agen_prov,$agen_kab,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller,$product_komisi_basic,$product_komisi_premium,$product_amount ,$product_margin,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan,$voucher_nama,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description,$id_voucher,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
					$save = $saveTransBPJS[0]->id;*/
				} catch(\Exception $e) {
					//sleep(1);
					$kode = Esta::nomor_transaksi('trans_bpjs',CRUDBooster::getsetting('transaksi_bpjs'));
					$created_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'BPJS Kesehatan';
					$currency = 'IDR';
					$trans_amount = $trans_amount;
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Pending';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_pelanggan = $id_pelanggan;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$id_voucher = $detail_voucher->id;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_bpjs');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					/*$save = DB::table('trans_bpjs')
						->insertGetId($sv);*/
					/*$saveTransBPJS = DB::select('exec postTransBPJS ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
						array($created_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$trans_amount,$komisi,$id_agen,$status,$rekon_amount,$status_match,$no_hp,$id_pelanggan,$id_product,$agen_nama,$agen_email,$agen_kode,$agen_no_hp,$agen_level,$agen_referall,$agen_referall_relation ,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan ,$agen_prov,$agen_kab,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller,$product_komisi_basic,$product_komisi_premium,$product_amount ,$product_margin,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan,$voucher_nama,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description,$id_voucher,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
					$save = $saveTransBPJS[0]->id;*/
				}

				/*Get Id Table Trans*/
				$save = DB::table('trans_bpjs')
							->where('transactionId_sobatku',$transactionId)
							->first()->id;

				//if($save) {
					/*potong saldo*/
					//sleep(1);
					$tr_out = Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi BPJS','Transaksi BPJS Kesehatan','Out','Transaksi','trans_bpjs',$save,$saldo_sebelum);
					$cb_in = Esta::log_money($id_agen,$komisi,date('Y-m-d H:i:s'),'Komisi Transaksi BPJS','Komisi Transaksi BPJS Kesehatan','In','Komisi','trans_bpjs',$save,$saldo_sebelum-$trans_amount);

					/*send iso*/ 
					$send_iso = Esta::send_iso_bpjsks($id_product,$no_hp,'0200','Payment',$id_transaksi,$id_pelanggan,$jml_bulan);
					
					
					if($send_iso['39'] == 'Pending' || $send_iso['39'] == '06' || $send_iso['39'] == '09' || $send_iso['39'] == '18' || $send_iso['39'] == '22') {
						$status = 'Pending';
						$stan = $send_iso['stan'];
						$no_va = $send_iso['bpjsks_no_va_keluarga'];
						$jpa_ref = $send_iso['bpjsks_jpa_refnum'];

						/*$update = DB::table('trans_bpjs')
							->where('id',$save)
							->update($up3);*/

						$update = DB::select('exec updateTransBPJSIso ?,?,?,?,?', array($save,$status,$stan,$no_va,$jpa_ref));

						$response['api_status']  = 3;
				    	$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
				    	$response['type_dialog']  = 'Informasi';
				    	$response['id_transaksi']  = 0;

				    	$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

						$response['bpjsks_jpa_refnum'] = $send_iso['bpjsks_jpa_refnum'];
						$response['bpjsks_nama'] = $send_iso['bpjsks_nama'];
						$response['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
						$response['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
						$response['bpjsks_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0)).' Orang';
						$response['periode'] = ltrim($send_iso['periode'],0).' Bulan';
						$response['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
						$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
						$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);

						$response['bpjsks_tgl_lunas'] = substr($send_iso['bpjsks_tgl_lunas'],6,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],4,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],0,4).' '.substr($send_iso['bpjsks_tgl_lunas'],8,2).':'.substr($send_iso['bpjsks_tgl_lunas'],10,2);

						$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
						$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
						$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
						
						$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
						$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
						$response['val_total_bayar'] = $response['val_bpjsks_total_premi']+$response['val_bpjsks_biaya_admin'];
						$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');

						$val_total_pembayaran = $response['val_total_bayar']-$response['val_komisi'];
						$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
						$response['total_pembayaran'] = 'Rp '.number_format($val_total_pembayaran,0,',','.');

						$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
						$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
						$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
						$response['info'] = $send_iso['62'];

						//OLD
						/*$up_struk['struk_tgl_lunas'] = $response['bpjsks_tgl_lunas'];
						$up_struk['struk_no_ref'] = $response['bpjsks_jpa_refnum'];
						$up_struk['struk_no_va_keluarga'] = $response['bpjsks_no_va_keluarga'];
						$up_struk['struk_no_va_kepala_keluarga'] = $response['bpjsks_no_va_kepala_keluarga'];
						$up_struk['struk_nama_peserta'] = $response['bpjsks_nama'];
						$up_struk['struk_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
						$up_struk['struk_periode'] = ltrim($send_iso['periode'],0);
						$up_struk['struk_jml_tagihan'] = $response['val_bpjsks_total_premi'];
						$up_struk['struk_admin_bank'] = $response['val_bpjsks_biaya_admin'];
						$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
						$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran']-$sv['voucher_amount'];
						$up_struk['jpa_ref'] = $response['bpjsks_jpa_refnum'];
						$up_struk['struk_info'] = $response['info'];

						$update = DB::table('trans_bpjs')
							->where('id',$save)
							->update($up_struk);*/

						//NEW
						$struk_tgl_lunas = $response['bpjsks_tgl_lunas'];
						$struk_no_ref = $response['bpjsks_jpa_refnum'];
						$struk_no_va_keluarga = $response['bpjsks_no_va_keluarga'];
						$struk_no_va_kepala_keluarga = $response['bpjsks_no_va_kepala_keluarga'];
						$struk_nama_peserta = $response['bpjsks_nama'];
						$struk_jml_anggota_keluarga = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
						$struk_periode = ltrim($send_iso['periode'],0);
						$struk_jml_tagihan = $response['val_bpjsks_total_premi'];
						$struk_admin_bank = $response['val_bpjsks_biaya_admin'];
						$struk_total_bayar = $response['val_total_bayar'];
						$struk_total_pembayaran = $response['val_total_pembayaran']-$voucher_amount;
						$jpa_ref = $response['bpjsks_jpa_refnum'];
						$struk_info = $response['info'];

						$update = DB::select('exec updateTransBPJSStruk ?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_no_ref,$struk_no_va_keluarga,$struk_no_va_kepala_keluarga,$struk_nama_peserta,$struk_jml_anggota_keluarga,$struk_periode,$struk_jml_tagihan,$struk_admin_bank,$struk_total_bayar,$struk_total_pembayaran,$jpa_ref,$struk_info));

						return $response;
						exit();
					}

					if($send_iso['39'] != '00') { /*jika respon error*/
						/*kembalikan saldo*/
						

						if($send_iso['39'] != '18' || $send_iso['39'] != '06' || $send_iso['39'] != '09') {
							/*reversal sobatku*/
							$start_time = time();
							//$refrn = 0;
							while(true){
								
								//$refrn++;
								$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
								//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
								$user = CRUDBooster::getsetting('user_sobatku_api');
								$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

								$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
								$keterangan = 'Reversal Payment';
								$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
								$referenceNumber = $referenceNumber[0]->Prefix;
								$request = [
									'oldRefNum' => $oldRefNum,
									'referenceNumber' => $referenceNumber,
									'user' => $user,
									'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
								];

								$data_toJson = json_encode( $request );
								$ch = curl_init( $serviceURL );
								curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
								curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
								curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
								curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
										'Content-Type: application/json',
										'Accept:application/json'
									)
								);
								curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
								curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
								$responsereversal = curl_exec( $ch );
								$responseerror = curl_errno($ch);
								if ($responseerror == 28){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
									break;
								}else {
									$responseBody = json_decode( $responsereversal, true);													

									if($responseBody['responseCode'] == '00' ){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										/*kembalikan saldo*/
										Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi BPJS','Return Transaksi BPJS Kesehatan','In','Transaksi','trans_bpjs',$save,$saldo_sebelum-$trans_amount);
										Esta::log_money($id_agen,$komisi,date('Y-m-d H:i:s'),'Komisi Transaksi BPJS','Return Komisi Transaksi BPJS Kesehatan','Out','Komisi','trans_bpjs',$save,$saldo_sebelum);
										break;
									}elseif($responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3' || $responseBody['responseCode'] == 'P1'){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										break;
									}elseif((time() - $start_time) > 60){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										break;
									}else{
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
									}
								}
								
								curl_close($ch);
								sleep(10);
							}

							//OLD
							/*$up['return_saldo'] = 'Yes';
							$up['flag_reversal'] = 'Reversal Sukses';
							$up['status'] = 'Error';
							//$up['status_match'] = 'Error';
							$up['error_code'] = $send_iso['39'];
							$up['stan'] = $send_iso['stan'];
							$up['no_va'] = $send_iso['bpjsks_no_va_keluarga'];
							$up['jpa_ref'] = $send_iso['bpjsks_jpa_refnum'];

							$update = DB::table('trans_bpjs')
								->where('id',$save)
								->update($up);*/

							//NEW
							$return_saldo = 'Yes';
							$flag_reversal = 'Reversal Sukses';
							$status = 'Error';
							//$status_match = 'Error';
							$error_code = $send_iso['39'];
							$stan = $send_iso['stan'];
							$no_va = $send_iso['bpjsks_no_va_keluarga'];
							$jpa_ref = $send_iso['bpjsks_jpa_refnum'];

							$update = DB::statement('exec updateTransBPJSIsoError ?,?,?,?,?,?,?,?', array($save,$return_saldo,$flag_reversal,$status,$error_code,$stan,$no_va,$jpa_ref));
							
							if(!empty($id_voucher)) {
								$uv['used'] = 'No';

								$up_voucher = DB::table('trans_voucher_child')
									->where('id',$id_voucher_child)
									->where('id_agen',$id_agen)
									->update($uv);
							}

							$response['api_status']  = 2;
					    	$response['api_message'] = Esta::show_error('BPJSTK',$send_iso['39'],'0');
					    	$response['type_dialog']  = 'Error';
					    	$response['id_transaksi']  = 0;

					    	$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

							$response['bpjsks_jpa_refnum'] = $send_iso['bpjsks_jpa_refnum'];
							$response['bpjsks_nama'] = $send_iso['bpjsks_nama'];
							$response['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
							$response['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
							$response['bpjsks_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0)).' Orang';
							$response['periode'] = ltrim($send_iso['periode'],0).' Bulan';
							$response['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
							$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
							$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);

							$response['bpjsks_tgl_lunas'] = substr($send_iso['bpjsks_tgl_lunas'],6,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],4,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],0,4).' '.substr($send_iso['bpjsks_tgl_lunas'],8,2).':'.substr($send_iso['bpjsks_tgl_lunas'],10,2);

							$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
							$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
							$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
							
							$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
							$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
							$response['val_total_bayar'] = $response['val_bpjsks_total_premi']+$response['val_bpjsks_biaya_admin'];
							$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');

							$val_total_pembayaran = $response['val_total_bayar']-$response['val_komisi'];
							$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
							$response['total_pembayaran'] = 'Rp '.number_format($val_total_pembayaran,0,',','.');

							$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
							$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
							$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
							$response['info'] = $send_iso['62'];

							//OLD
							/*$up_struk['struk_tgl_lunas'] = $response['bpjsks_tgl_lunas'];
							$up_struk['struk_no_ref'] = $response['bpjsks_jpa_refnum'];
							$up_struk['struk_no_va_keluarga'] = $response['bpjsks_no_va_keluarga'];
							$up_struk['struk_no_va_kepala_keluarga'] = $response['bpjsks_no_va_kepala_keluarga'];
							$up_struk['struk_nama_peserta'] = $response['bpjsks_nama'];
							$up_struk['struk_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
							$up_struk['struk_periode'] = ltrim($send_iso['periode'],0);
							$up_struk['struk_jml_tagihan'] = $response['val_bpjsks_total_premi'];
							$up_struk['struk_admin_bank'] = $response['val_bpjsks_biaya_admin'];
							$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
							$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran']-$sv['voucher_amount'];
							$up_struk['jpa_ref'] = $response['bpjsks_jpa_refnum'];
							$up_struk['struk_info'] = $response['info'];

							$update = DB::table('trans_bpjs')
								->where('id',$save)
								->update($up_struk);*/

							//NEW
							$struk_tgl_lunas = $response['bpjsks_tgl_lunas'];
							$struk_no_ref = $response['bpjsks_jpa_refnum'];
							$struk_no_va_keluarga = $response['bpjsks_no_va_keluarga'];
							$struk_no_va_kepala_keluarga = $response['bpjsks_no_va_kepala_keluarga'];
							$struk_nama_peserta = $response['bpjsks_nama'];
							$struk_jml_anggota_keluarga = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
							$struk_periode = ltrim($send_iso['periode'],0);
							$struk_jml_tagihan = $response['val_bpjsks_total_premi'];
							$struk_admin_bank = $response['val_bpjsks_biaya_admin'];
							$struk_total_bayar = $response['val_total_bayar'];
							$struk_total_pembayaran = $response['val_total_pembayaran']-$voucher_amount;
							$jpa_ref = $response['bpjsks_jpa_refnum'];
							$struk_info = $response['info'];

							$update = DB::statement('exec updateTransBPJSStruk ?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_no_ref,$struk_no_va_keluarga,$struk_no_va_kepala_keluarga,$struk_nama_peserta,$struk_jml_anggota_keluarga,$struk_periode,$struk_jml_tagihan,$struk_admin_bank,$struk_total_bayar,$struk_total_pembayaran,$jpa_ref,$struk_info));

					    	return $response;
						} else {
							//OLD
							/*$up['status'] = 'Error';
							//$up['status_match'] = 'Error';
							$up['error_code'] = $send_iso['39'];
							$up['stan'] = $send_iso['stan'];
							$up['no_va'] = $send_iso['bpjsks_no_va_keluarga'];
							$up['jpa_ref'] = $send_iso['bpjsks_jpa_refnum'];

							$update = DB::table('trans_bpjs')
								->where('id',$save)
								->update($up);*/
								
							/*reversal sobatku*/
							$start_time = time();
							//$refrn = 0;
							while(true){
								
								//$refrn++;
								$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
								//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
								$user = CRUDBooster::getsetting('user_sobatku_api');
								$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

								$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
								$keterangan = 'Reversal Payment';
								$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
								$referenceNumber = $referenceNumber[0]->Prefix;
								$request = [
									'oldRefNum' => $oldRefNum,
									'referenceNumber' => $referenceNumber,
									'user' => $user,
									'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
								];

								$data_toJson = json_encode( $request );
								$ch = curl_init( $serviceURL );
								curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
								curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
								curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
								curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
										'Content-Type: application/json',
										'Accept:application/json'
									)
								);
								curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
								curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
								$responsereversal = curl_exec( $ch );
								$responseerror = curl_errno($ch);
								if ($responseerror == 28){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
									break;
								}else {
									$responseBody = json_decode( $responsereversal, true);													

									if($responseBody['responseCode'] == '00' ){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										/*kembalikan saldo*/
										Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi BPJS','Return Transaksi BPJS Kesehatan','In','Transaksi','trans_bpjs',$save,$saldo_sebelum-$trans_amount);
										Esta::log_money($id_agen,$komisi,date('Y-m-d H:i:s'),'Komisi Transaksi BPJS','Return Komisi Transaksi BPJS Kesehatan','Out','Komisi','trans_bpjs',$save,$saldo_sebelum);
										break;
									}elseif($responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3' || $responseBody['responseCode'] == 'P1'){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										break;
									}elseif((time() - $start_time) > 60){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										break;
									}else{
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
									}
								}
								
								curl_close($ch);
								sleep(10);
							}
							//NEW
							$status = 'Error';
							//$up['status_match'] = 'Error';
							$error_code = $send_iso['39'];
							$stan = $send_iso['stan'];
							$no_va = $send_iso['bpjsks_no_va_keluarga'];
							$jpa_ref = $send_iso['bpjsks_jpa_refnum'];
							$update = DB::statement('exec updateTransBPJSIsoError ?,?,?,?,?,?,?,?', array($save,'','',$status,$error_code,$stan,$no_va,$jpa_ref));

							$response['api_status']  = 2;
					    	$response['api_message'] = Esta::show_error('BPJSTK',$send_iso['39'],'0');
					    	$response['type_dialog']  = 'Error';
					    	$response['id_transaksi']  = 0;

					    	$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

							$response['bpjsks_jpa_refnum'] = $send_iso['bpjsks_jpa_refnum'];
							$response['bpjsks_nama'] = $send_iso['bpjsks_nama'];
							$response['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
							$response['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
							$response['bpjsks_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0)).' Orang';
							$response['periode'] = ltrim($send_iso['periode'],0).' Bulan';
							$response['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
							$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
							$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);

							$response['bpjsks_tgl_lunas'] = substr($send_iso['bpjsks_tgl_lunas'],6,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],4,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],0,4).' '.substr($send_iso['bpjsks_tgl_lunas'],8,2).':'.substr($send_iso['bpjsks_tgl_lunas'],10,2);

							$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
							$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
							$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
							
							$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
							$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
							$response['val_total_bayar'] = $response['val_bpjsks_total_premi']+$response['val_bpjsks_biaya_admin'];
							$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');

							$val_total_pembayaran = $response['val_total_bayar']-$response['val_komisi'];
							$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
							$response['total_pembayaran'] = 'Rp '.number_format($val_total_pembayaran,0,',','.');

							$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
							$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
							$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
							$response['info'] = $send_iso['62'];

							//OLD
							/*$up_struk['struk_tgl_lunas'] = $response['bpjsks_tgl_lunas'];
							$up_struk['struk_no_ref'] = $response['bpjsks_jpa_refnum'];
							$up_struk['struk_no_va_keluarga'] = $response['bpjsks_no_va_keluarga'];
							$up_struk['struk_no_va_kepala_keluarga'] = $response['bpjsks_no_va_kepala_keluarga'];
							$up_struk['struk_nama_peserta'] = $response['bpjsks_nama'];
							$up_struk['struk_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
							$up_struk['struk_periode'] = ltrim($send_iso['periode'],0);
							$up_struk['struk_jml_tagihan'] = $response['val_bpjsks_total_premi'];
							$up_struk['struk_admin_bank'] = $response['val_bpjsks_biaya_admin'];
							$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
							$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran']-$sv['voucher_amount'];
							$up_struk['jpa_ref'] = $response['bpjsks_jpa_refnum'];
							$up_struk['struk_info'] = $response['info'];

							$update = DB::table('trans_bpjs')
								->where('id',$save)
								->update($up_struk);*/
								
							if(!empty($id_voucher)) {
								$uv['used'] = 'No';

								$up_voucher = DB::table('trans_voucher_child')
									->where('id',$id_voucher_child)
									->where('id_agen',$id_agen)
									->update($uv);
							}

							//NEW
							$struk_tgl_lunas = $response['bpjsks_tgl_lunas'];
							$struk_no_ref = $response['bpjsks_jpa_refnum'];
							$struk_no_va_keluarga = $response['bpjsks_no_va_keluarga'];
							$struk_no_va_kepala_keluarga = $response['bpjsks_no_va_kepala_keluarga'];
							$struk_nama_peserta = $response['bpjsks_nama'];
							$struk_jml_anggota_keluarga = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
							$struk_periode = ltrim($send_iso['periode'],0);
							$struk_jml_tagihan = $response['val_bpjsks_total_premi'];
							$struk_admin_bank = $response['val_bpjsks_biaya_admin'];
							$struk_total_bayar = $response['val_total_bayar'];
							$struk_total_pembayaran = $response['val_total_pembayaran']-$voucher_amount;
							$jpa_ref = $response['bpjsks_jpa_refnum'];
							$struk_info = $response['info'];

							$update = DB::statement('exec updateTransBPJSStruk ?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_no_ref,$struk_no_va_keluarga,$struk_no_va_kepala_keluarga,$struk_nama_peserta,$struk_jml_anggota_keluarga,$struk_periode,$struk_jml_tagihan,$struk_admin_bank,$struk_total_bayar,$struk_total_pembayaran,$jpa_ref,$struk_info));

					    	return $response;
					    	exit();
					    }
					}

					$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_bpjs');
					if($send_sms_transaksi == 'Yes') {
						$msg = CRUDBooster::getsetting('sukses_transaksi_bpjs');
						$msg = str_replace('[no_hp]', $no_hp, $msg);
						$msg = str_replace('[nama]', $detail_agen->nama, $msg);
						$msg = str_replace('[komisi]', $komisi, $msg);
						Esta::send_sms($no_hp, $msg);
					}

					if(!empty($id_voucher)) {
						$uv['used'] = 'Yes';

						$up_voucher = DB::table('trans_voucher_child')
							->where('id',$id_voucher_child)
							->where('id_agen',$id_agen)
							->update($uv);
					}

					$up4['no_va'] = $send_iso['bpjsks_no_va_keluarga'];
					$up4['status'] = 'Clear';

					$update = DB::table('trans_bpjs')
						->where('id',$save)
						->update($up4);

					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi BPJS berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['id_transaksi']  = $send_iso['id_log'];

					$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

					$response['bpjsks_jpa_refnum'] = $send_iso['bpjsks_jpa_refnum'];
					$response['bpjsks_nama'] = $send_iso['bpjsks_nama'];
					$response['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
					$response['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
					$response['bpjsks_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0)).' Orang';
					$response['periode'] = ltrim($send_iso['periode'],0).' Bulan';
					$response['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
					$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
					$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);

					$response['bpjsks_tgl_lunas'] = substr($send_iso['bpjsks_tgl_lunas'],6,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],4,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],0,4).' '.substr($send_iso['bpjsks_tgl_lunas'],8,2).':'.substr($send_iso['bpjsks_tgl_lunas'],10,2);

					$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
					$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
					$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
					
					$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
					$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
					$response['val_total_bayar'] = $response['val_bpjsks_total_premi']+$response['val_bpjsks_biaya_admin'];
					$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');

					$val_total_pembayaran = $response['val_total_bayar']-$response['val_komisi'];
					$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
					$response['total_pembayaran'] = 'Rp '.number_format($val_total_pembayaran,0,',','.');

					$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
					$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
					$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
					$response['info'] = $send_iso['62'];

					//OLD
					/*$up_struk['struk_tgl_lunas'] = $response['bpjsks_tgl_lunas'];
					$up_struk['struk_no_ref'] = $response['bpjsks_jpa_refnum'];
					$up_struk['struk_no_va_keluarga'] = $response['bpjsks_no_va_keluarga'];
					$up_struk['struk_no_va_kepala_keluarga'] = $response['bpjsks_no_va_kepala_keluarga'];
					$up_struk['struk_nama_peserta'] = $response['bpjsks_nama'];
					$up_struk['struk_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
					$up_struk['struk_periode'] = ltrim($send_iso['periode'],0);
					$up_struk['struk_jml_tagihan'] = $response['val_bpjsks_total_premi'];
					$up_struk['struk_admin_bank'] = $response['val_bpjsks_biaya_admin'];
					$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
					$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran']-$sv['voucher_amount'];
					$up_struk['jpa_ref'] = $response['bpjsks_jpa_refnum'];
					$up_struk['struk_info'] = $response['info'];
					$up_struk['status'] = 'Clear';

					$update = DB::table('trans_bpjs')
						->where('id',$save)
						->update($up_struk);*/

					//NEW
					$struk_tgl_lunas = $response['bpjsks_tgl_lunas'];
					$struk_no_ref = $response['bpjsks_jpa_refnum'];
					$struk_no_va_keluarga = $response['bpjsks_no_va_keluarga'];
					$struk_no_va_kepala_keluarga = $response['bpjsks_no_va_kepala_keluarga'];
					$struk_nama_peserta = $response['bpjsks_nama'];
					$struk_jml_anggota_keluarga = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
					$struk_periode = ltrim($send_iso['periode'],0);
					$struk_jml_tagihan = $response['val_bpjsks_total_premi'];
					$struk_admin_bank = $response['val_bpjsks_biaya_admin'];
					$struk_total_bayar = $response['val_total_bayar'];
					$struk_total_pembayaran = $response['val_total_pembayaran']-$voucher_amount;
					$jpa_ref = $response['bpjsks_jpa_refnum'];
					$struk_info = $response['info'];

					$update = DB::statement('exec updateTransBPJSStruk ?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_no_ref,$struk_no_va_keluarga,$struk_no_va_kepala_keluarga,$struk_nama_peserta,$struk_jml_anggota_keluarga,$struk_periode,$struk_jml_tagihan,$struk_admin_bank,$struk_total_bayar,$struk_total_pembayaran,$jpa_ref,$struk_info));

					/*email*/
					$view     = view('struk/struk_bpjs_kesehatan',$response)->render();
					$filename = "Struk-BPJS-Kesehatan-".$no_hp;
					$pdf      = App::make('dompdf.wrapper');

					$path = storage_path('app/uploads/'.$filename.'.pdf');

					$pdf->loadHTML($view);
					$pdf->setPaper('A4','landscape');
					$output = $pdf->output();

					file_put_contents($path, $output);

					$attachments = [$path];
					$email = $detail_agen->email;
					if(!empty($email) && $detail_agen->notif_email != 'No') {					    
						try{
						   Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_bpjs_kesehatan','attachments'=>$attachments]);
					    }catch(\Exception $e){               	
					    }
					}
			/*} else {
				$response['api_status']  = 3;
		    	$response['api_message'] = 'Saldo anda tidak mencukupi';
		    	$response['type_dialog']  = 'Error';
		    	$response['id_transaksi']  = 0;
			}*/

		return $response;
	}
	

	public function postBpjsTransaksiPending() {
		$agen = Auth::user();
        $id_agen = $agen->id;

		$arrays = DB::table('trans_bpjs')
			->where('id_agen',$id_agen)
			//->where('deleted_at','!=',NULL)
			->where('status','Pending')
			->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'))
			->orderBy('id','DESC')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$date_tr = Esta::change_date_format($array->created_at,'d M Y');
			$rest['id'] = $array->id;
			$rest['datetime'] = Esta::change_date_format($array->created_at,'d M Y H:i');
			$rest['trans_no'] = $array->trans_no;
			$rest['id_pelanggan'] = $array->id_pelanggan;
			$rest['stan'] = $array->stan;
			$rest['amount'] = 'Rp '.number_format($array->trans_amount,0,',','.');
			$rest['jenis_layanan'] = 'BPJS Kesehatan';
			$rest['status'] = ($date_tr == date('d M Y') ? 'Pending' : 'Expired');
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postBpjsAdviceManual() {
		$stan = Request::get('stan');
		$trans_no = Request::get('trans_no');

		$inquiry = DB::table('log_jatelindo_bit');
			if(!empty($stan)) {
				$inquiry = $inquiry->where('bit11',$stan);
			}
			$inquiry = $inquiry
			->where('bit37',$trans_no)
			->where('type','BPJS Kesehatan')
			->where('status','Inquiry')
			->where('jenis','res')
			->first();
		$purchase = DB::table('log_jatelindo_bit');
			if(!empty($stan)) {
				$purchase = $purchase->where('bit11',$stan);
			}
			$purchase = $purchase
			->where('bit37',$trans_no)
			->where('type','BPJS Kesehatan')
			//->where('status','Purchase')
			->where('jenis','res')
			->orderBy('id','desc')
			->first();

		$detail_transaksi = DB::table('trans_bpjs')
			->where('trans_no',$trans_no)
			->first();

		$detail_product = DB::table('pan_product')
			->where('id',$detail_transaksi->id_product)
			->first();

		$detail_voucher= DB::table('voucher')
			->where('id',$detail_transaksi->id_product)
			->first();

		/*send iso*/
		$send_iso = Esta::send_iso_bpjsks($detail_transaksi->id_product,$detail_transaksi->no_hp,'0200','Advice 3',$inquiry->id,$detail_transaksi->id_pelanggan,$inquiry->bpjsks_jml_bulan);

		if($send_iso['39'] == '00') {

			$response['api_status']  = 1;
	    	$response['api_message'] = 'Transaksi BPJS berhasil';
	    	$response['type_dialog']  = 'Informasi';
	    	$response['id_transaksi']  = $send_iso['id_log'];
	    	$response['id_log']  = $send_iso['id_log'];

			$response['val_komisi'] = $detail_transaksi->komisi;

			$response['bpjsks_jpa_refnum'] = $send_iso['bpjsks_jpa_refnum'];
			$response['bpjsks_nama'] = $send_iso['bpjsks_nama'];
			$response['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
			$response['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
			$response['bpjsks_jml_anggota_keluarga'] = ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0).' Orang';
			$response['periode'] = ltrim($send_iso['periode'],0).' Bulan';
			$response['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
			$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
			$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);

			$response['bpjsks_tgl_lunas'] = substr($send_iso['bpjsks_tgl_lunas'],6,2).'/'.substr($send_iso['bpjsks_tgl_lunas'],4,2).'/'.substr($send_iso['bpjsks_tgl_lunas'],0,4).' '.substr($send_iso['bpjsks_tgl_lunas'],8,2).':'.substr($send_iso['bpjsks_tgl_lunas'],10,2);

			$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
			$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
			$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
			
			$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
			$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
			$response['val_total_bayar'] = $response['val_bpjsks_total_premi']+$response['val_bpjsks_biaya_admin'];
			$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');

			$val_total_pembayaran = $response['val_total_bayar']-$response['val_komisi'];
			$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
			$response['total_pembayaran'] = 'Rp '.number_format($val_total_pembayaran,0,',','.');

			$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
			$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
			$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
			$response['info'] = $send_iso['62'];
			$response['voucher_amount'] = ($detail_voucher->amount >= 1 ? $detail_voucher->amount : 0);

			/*$up['struk_tgl_lunas'] = $response['bpjsks_tgl_lunas'];
			$up['struk_no_ref'] = $response['bpjsks_jpa_refnum'];
			$up['struk_no_va_keluarga'] = $response['bpjsks_no_va_keluarga'];
			$up['struk_no_va_kepala_keluarga'] = $response['bpjsks_no_va_kepala_keluarga'];
			$up['struk_nama_peserta'] = $response['bpjsks_nama'];
			$up['struk_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
			$up['struk_periode'] = ltrim($send_iso['periode'],0);
			$up['struk_jml_tagihan'] = $response['val_bpjsks_total_premi'];
			$up['struk_admin_bank'] = $response['val_bpjsks_biaya_admin'];
			$up['struk_total_bayar'] = $response['val_total_bayar'];
			$up['struk_total_pembayaran'] = $response['val_total_pembayaran'];
			$up['jpa_ref'] = $response['bpjsks_jpa_refnum'];
			$up['struk_info'] = $response['info'];

			$up['status'] = 'Clear';

			$update = DB::table('trans_bpjs')
				->where('id',$detail_transaksi->id)
				->update($up);*/

			$struk_tgl_lunas = $response['bpjsks_tgl_lunas'];
			$struk_no_ref = $response['bpjsks_jpa_refnum'];
			$struk_no_va_keluarga = $response['bpjsks_no_va_keluarga'];
			$struk_no_va_kepala_keluarga = $response['bpjsks_no_va_kepala_keluarga'];
			$struk_nama_peserta = $response['bpjsks_nama'];
			$struk_jml_anggota_keluarga = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
			$struk_periode = ltrim($send_iso['periode'],0);
			$struk_jml_tagihan = $response['val_bpjsks_total_premi'];
			$struk_admin_bank = $response['val_bpjsks_biaya_admin'];
			$struk_total_bayar = $response['val_total_bayar'];
			$struk_total_pembayaran = $response['val_total_pembayaran'];
			$jpa_ref = $response['bpjsks_jpa_refnum'];
			$struk_info = $response['info'];
			$status = 'Clear';

			$update = DB::statement('exec postBpjsAdviceManual ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($detail_transaksi->id,$struk_tgl_lunas,$struk_no_ref,$struk_no_va_keluarga,$struk_no_va_kepala_keluarga,$struk_nama_peserta,$struk_jml_anggota_keluarga,$struk_periode,$struk_jml_tagihan,$struk_admin_bank,$struk_total_bayar,$struk_total_pembayaran,$jpa_ref,$struk_info,$status));

			/*email*/
			$view     = view('struk/struk_bpjs_kesehatan',$response)->render();
			$filename = "Struk-BPJS-Kesehatan-".$detail_transaksi->no_hp;
			$pdf      = App::make('dompdf.wrapper');

			$path = storage_path('app/uploads/'.$filename.'.pdf');

			$pdf->loadHTML($view);
			$pdf->setPaper('A4','landscape');
			$output = $pdf->output();

			file_put_contents($path, $output);

			$attachments = [$path];
			$email = $detail_transaksi->agen_email;
			if(!empty($email) && $detail_agen->notif_email != 'No') {
			    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_bpjs_kesehatan','attachments'=>$attachments]);
			}
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'Transaksi gagal, Mohon coba kembali';
	    	$response['id_transaksi']  = 0;
	    	$response['type_dialog']  = 'Error';
		}

		return response()->json($response);
	}

}