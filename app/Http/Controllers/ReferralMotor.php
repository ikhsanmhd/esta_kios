<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Storage;
use Esta;
use Illuminate\Support\Facades\Auth;
use Hash;

class ReferralMotor extends ApiController
{
	public function postSubmitMotor(){
		$agen = Auth::user();
        $id_agen = $agen->id;
		
		$respondAgenAktif = $this->validasiAgenAktif();
        if( $respondAgenAktif != NULL){
            return $respondAgenAktif;
            exit();
        }
		
        $nama = Request::get('nama');
        $no_hp = Request::get('no_hp');
        $ktp = Request::get('ktp');
        $alamat = Request::get('alamat');
        $id_provinsi = Request::get('id_provinsi');
        $id_kabupaten = Request::get('id_kabupaten');
        $id_kecamatan = Request::get('id_kecamatan');
        $id_kodepos = Request::get('id_kodepos');
		$id_kelurahan = Request::get('id_kelurahan');

        //DATA JAMINAN
        $brand = Request::get('brand');
        $tipe = Request::get('tipe');
        $tahun = Request::get('tahun');
        $masa_berlaku_pajak = Request::get('masa_berlaku_pajak');

        $nilai_pengajuan = Request::get('nilai_pengajuan');
        $tenor = Request::get('tenor');

        //Save Image to FTP
	    $fotoKtp = Request::file('fotoKtp');
	    if(!empty($fotoKtp)){
	    	$file_nameKtp = 'Referral/Motor/'.$nama.'/fotoKtp-'.time().'-'.$nama.".jpg";
	    	Storage::disk('ftp')->put($file_nameKtp, file_get_contents($fotoKtp));
	    }
	    $selfieKtp = Request::file('selfieKtp');
		if(!empty($selfieKtp)){
			$file_nameSelfieKtp = 'Referral/Motor/'.$nama.'/selfieKtp-'.time().'-'.$nama.".jpg";
			Storage::disk('ftp')->put($file_nameSelfieKtp, file_get_contents($selfieKtp));
		}

	    $agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$cobrand = DB::table('cb_mst_cobrand')->where('id',$agen->cobrand_id)->first();
		$branch = DB::table('cb_mst_branch')->where('id',$agen->cobrand_cabang_id)->first();
		$ao = DB::table('cb_mst_pj')->where('id',$agen->cobrand_ao_id)->first();

	    //CREATE No PENGAJUAN
	    $prefixReferral = CRUDBooster::getsetting('prefix_referral_motor').substr($cobrand->kode_alias,0,3);
	    $no_pengajuan = DB::select('exec CreateNoPengajuanReferral ?', array($prefixReferral));
	    //dd($no_pengajuan[0]->Prefix);
	    $sv['kode_pengajuan'] = $no_pengajuan[0]->Prefix;

	    $sv['id_agen'] = $id_agen;
	    $sv['type_referral'] = 'motor';
	    $sv['nilai_pengajuan'] = $nilai_pengajuan;
	    $sv['tenor'] = $tenor;
	    //$sv['kode_referral'] = $agen->kode_referall_agen;
	    $sv['status_pengajuan'] = '1';
	    $sv['created_at'] = date('Y-m-d H:i:s');
	    $sv['updated_at'] = date('Y-m-d H:i:s');
	    $sv['foto_ktp'] = $file_nameKtp;
	    $sv['foto_selfie'] = $file_nameSelfieKtp;
	    $sv['id_cobrand'] = $agen->cobrand_id;
	    $sv['id_branch'] = $agen->cobrand_cabang_id;
	    $sv['nama_cobrand'] = $cobrand->nama;
	    $sv['nama_branch'] = $branch->nama;
	    $sv['soa'] = $cobrand->nama;
	    $sv['nama_nasabah'] = $nama;
	    $sv['no_hp_nasabah'] = $no_hp;
	    $sv['no_ktp_nasabah'] = $ktp;
	    $sv['alamat_nasabah'] = $alamat;
	    $sv['id_i_provinsi'] = $id_provinsi;
	    $sv['id_i_kabupaten'] = $id_kabupaten;
	    $sv['id_i_kecamatan'] = $id_kecamatan;
	    $sv['id_i_kodepos'] = $id_kodepos;
		$sv['id_i_kelurahan'] = $id_kelurahan;
		
	    $sv['brand_kendaraan'] = $brand;
	    $sv['tipe_kendaraan'] = $tipe;
	    $sv['tahun_kendaraan'] = $tahun;
	    $sv['masa_berlaku_pajak'] = $masa_berlaku_pajak;

	    if(!empty(Request::file('dokumen1_file'))){
	    	$dokumen1_label = Request::get('dokumen1_label');
	    	$dokumen1_file = Request::file('dokumen1_file');
		    $file_nameDokumen1 = 'Referral/Motor/'.$nama.'/dokumen1-'.time().'-'.$nama.".jpg";
		    Storage::disk('ftp')->put($file_nameDokumen1, file_get_contents($dokumen1_file));
		    $sv['dokumen1_name'] = Request::get('dokumen1_label');
		    $sv['dokumen1_file'] = $file_nameDokumen1;
	    }
	    if(!empty(Request::file('dokumen2_file'))){
	    	$dokumen2_label = Request::get('dokumen2_label');
	    	$dokumen2_file = Request::file('dokumen2_file');
		    $file_nameDokumen2 = 'Referral/Motor/'.$nama.'/dokument2-'.time().'-'.$nama.".jpg";
		    Storage::disk('ftp')->put($file_nameDokumen2, file_get_contents($dokumen2_file));
		    $sv['dokumen2_name'] = Request::get('dokumen2_label');
		    $sv['dokumen2_file'] = $file_nameDokumen2;
	    }
	    if(!empty(Request::file('dokumen3_file'))){
	    	$dokumen3_label = Request::get('dokumen3_label');
	    	$dokumen3_file = Request::file('dokumen3_file');
		    $file_nameDokumen3 = 'Referral/Motor/'.$nama.'/dokumen3-'.time().'-'.$nama.".jpg";
		    Storage::disk('ftp')->put($file_nameDokumen3, file_get_contents($dokumen3_file));
		    $sv['dokumen3_name'] = Request::get('dokumen3_label');
		    $sv['dokumen3_file'] = $file_nameDokumen3;
	    }
	    if(!empty(Request::file('dokumen4_file'))){
	    	$dokumen4_label = Request::get('dokumen4_label');
	    	$dokumen4_file = Request::file('dokumen4_file');
		    $file_nameDokumen4 = 'Referral/Motor/'.$nama.'/dokumen4-'.time().'-'.$nama.".jpg";
		    Storage::disk('ftp')->put($file_nameDokumen4, file_get_contents($dokumen4_file));
		    $sv['dokumen4_name'] = Request::get('dokumen4_label');
		    $sv['dokumen4_file'] = $file_nameDokumen4;
	    }
	    if(!empty(Request::file('dokumen5_file'))){
	    	$dokumen5_label = Request::get('dokumen5_label');
	    	$dokumen5_file = Request::file('dokumen5_file');
		    $file_nameDokumen5 = 'Referral/Motor/'.$nama.'/dokumen5-'.time().'-'.$nama.".jpg";
		    Storage::disk('ftp')->put($file_nameDokumen5, file_get_contents($dokumen5_file));
		    $sv['dokumen5_name'] = Request::get('dokumen5_label');
		    $sv['dokumen5_file'] = $file_nameDokumen5;
	    }

	    //$sv['created_user'] = '0';
	    $sv['created_at'] = date('Y-m-d H:i:s');
	    //$sv['updated_user'] = '0';
	    $sv['updated_at'] = date('Y-m-d H:i:s');	    

	    $save = DB::table('cb_txn_pengajuan')
				->insertGetId($sv);

		$log['id_pengajuan'] = $save;
		$log['status_pengajuan'] = '1';
		$log['updated_at'] = date('Y-m-d H:i:s');
		$save_log = DB::table('cb_log_txn_pengajuan')
					->insert($log);
					
		try{						
			$notifData =  DB::table('mst_notification')->where('notification_slug', 'referral_pengajuan_baru')->first();
			$notifDataTitle = $notifData->notification_title;
			$notifDataShortDescription = $notifData->notification_short_description;
			$notifDataDescription = $notifData->notification_description;
			$notifDataImage = $notifData->notification_image;
			$notifDataEnabled = $notifData->enabled;
			
			if($notifDataEnabled == '1'){
				//$transDetail = DB::select('EXEC getTransactionExchangeDetail '.$idList[$i].'')[0];

				$title = $notifDataTitle;
				$title = str_replace("[no_pengajuan]", $no_pengajuan[0]->Prefix, $title);
				$title = str_replace("[nama_nasabah]", $nama, $title);
				$title = str_replace("[jenis_referral]", $sv['type_referral'], $title);
				$title = str_replace("[tenor]", $sv['tenor'], $title);
				$title = str_replace("[cobrand]", $sv['nama_cobrand'], $title);
				$title = str_replace("[klik_disini]", '<a href="'.CRUDBooster::getsetting('url_notifikasi_referral').$save.'">Klik disini</a>', $title);

				$description = $notifDataDescription;
				$description = str_replace("[no_pengajuan]", $no_pengajuan[0]->Prefix, $description);
				$description = str_replace("[nama_nasabah]", $nama, $description);
				$description = str_replace("[jenis_referral]", $sv['type_referral'], $description);
				$description = str_replace("[tenor]", $sv['tenor'], $description);
				$description = str_replace("[cobrand]", $sv['nama_cobrand'], $description);
				$description = str_replace("[klik_disini]", '<a href="'.CRUDBooster::getsetting('url_notifikasi_referral').$save.'">Klik disini</a>', $description);
				
				$shortDesc = $notifDataShortDescription;
				$shortDesc = str_replace("[no_pengajuan]", $no_pengajuan[0]->Prefix, $shortDesc);
				$shortDesc = str_replace("[nama_nasabah]", $nama, $shortDesc);
				$shortDesc = str_replace("[jenis_referral]", $sv['type_referral'], $shortDesc);
				$shortDesc = str_replace("[tenor]", $sv['tenor'], $shortDesc);
				$shortDesc = str_replace("[cobrand]", $sv['nama_cobrand'], $shortDesc);
				$shortDesc = str_replace("[klik_disini]", '<a href="'.CRUDBooster::getsetting('url_notifikasi_referral').$save.'">Klik disini</a>', $shortDesc);
				
				//send notif
				$save_notif['created_at'] = date('Y-m-d H:i:s');
				$save_notif['title'] = $title;			
				$save_notif['description'] = $description;
				$save_notif['description_short'] = $shortDesc;
				$save_notif['image'] = $notifDataImage;
				$save_notif['flag'] = 'Notifikasi';
				$save_notif['id_agen'] = $id_agen;
				$save_notif['read'] = 'No';
				$save_notif['id_header'] = $id;

				$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
				if($detail_agen->regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
				}
				
				DB::table('notification')->insert($save_notif);
			}
		}
		catch(Exception $e){

		}
		
		$data['id_pengajuan'] = $save;
		$data['kode_pengajuan'] = $sv['kode_pengajuan'];
		return $this->respondWithDataAndMessage($data, "Success");
	}
	
	public function getDataMotor(){
		/*$agenid = Auth::user();
		$agen_id = $agenid->id;
		$agen = DB::table('agen')
			->where('id',$agen_id)
			->first();*/

		$getConfig = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->whereNull('deleted_at')->get();
		//dd($getConfig);
		/*$config = [];
		foreach ($getConfig as $value) {
			$config[] = [
				'field_name' => $value->field_name,
				'isMandatory' => $value->isMandatory
			];
		}
		return $this->respondWithDataAndMessage($config, "Success");*/
		$data['foto_ktp'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','foto_ktp')->whereNull('deleted_at')->first()->isMandatory;
		$data['foto_selfie'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','selfie_ktp')->whereNull('deleted_at')->first()->isMandatory;
		$data['nama_nasabah'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','nama')->whereNull('deleted_at')->first()->isMandatory;
		$data['no_hp_nasabah'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','no_hp')->whereNull('deleted_at')->first()->isMandatory;
		$data['no_ktp_nasabah'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','no_ktp')->whereNull('deleted_at')->first()->isMandatory;
		$data['alamat_nasabah'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','alamat')->whereNull('deleted_at')->first()->isMandatory;
		$data['provinsi'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','provinsi')->whereNull('deleted_at')->first()->isMandatory;
		$data['kota/kabupaten'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','kota_kabupaten')->whereNull('deleted_at')->first()->isMandatory;
		$data['kecamatan'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','kecamatan')->whereNull('deleted_at')->first()->isMandatory;
		$data['kelurahan'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','kelurahan')->whereNull('deleted_at')->first()->isMandatory;
		$data['kode_pos'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','kodepos')->whereNull('deleted_at')->first()->isMandatory;
		//$data['kode_referral'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','kode_referral')->whereNull('deleted_at')->first()->isMandatory;
		
		$data['brand'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','merk')->whereNull('deleted_at')->first()->isMandatory;
		$data['tipe'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','tipe')->whereNull('deleted_at')->first()->isMandatory;
		$data['tahun'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','tahun')->whereNull('deleted_at')->first()->isMandatory;
		$data['masa_berlaku_pajak'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','masa_berlaku_pajak')->whereNull('deleted_at')->first()->isMandatory;
		$data['nilai_pengajuan'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','nilai_pengajuan')->whereNull('deleted_at')->first()->isMandatory;
		$data['tenor'] = DB::table('cb_config_field_mobile')->where('type_referral', 'motor')->where('field_name','tenor')->whereNull('deleted_at')->first()->isMandatory;	

		return $this->respondWithDataAndMessage($data, "Success");
	}
}