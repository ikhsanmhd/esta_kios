<?php 
namespace App\Http\Controllers;

use DB;
use Session;
use Request;
use Route;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;
use Storage;
use Esta;
use App;

class ApisController extends Controller {

	public function __construct() 
	{
		
		$id_agen = Request::get('id_agen');

		$a = Request::url();
		if (strpos($a, 'apis/setting') !== false) {

  		} else {
  			if (strpos($a, 'apis/va-inquiry') !== false) {

  			} else {
  				if (strpos($a, 'apis/va-flag') !== false) {

  				} else {
  					if (strpos($a, 'apis/upload-foto') !== false) {

	  				} else {
	  					if (strpos($a, 'apis/doku-notify') !== false) {

		  				} else {
		  					if (strpos($a, 'apis/doku-inquiry') !== false) {

			  				} else {
								$this->token();
								if(!empty($id_agen)) {
									$ag = DB::table('agen')->where('id',$id_agen)->first()->status_aktif;
									if($ag == 'Tidak Aktif') {
										exit();
									} else {
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public function getCekEmail(){
		//$rest['aa'] = '';
		//Esta::kirimemail(['to'=>Request::get('email'),'data'=>$rest,'template'=>'email_transaksi_pulsa_postpaid']);

		/*\Config::set('mail.driver',CRUDBooster::getSetting('smtp_driver'));
	    \Config::set('mail.host',CRUDBooster::getSetting('smtp_host'));
	    \Config::set('mail.port',CRUDBooster::getSetting('smtp_port'));
	    \Config::set('mail.username',CRUDBooster::getSetting('smtp_username'));
	    \Config::set('mail.password',CRUDBooster::getSetting('smtp_password'));
	    \Config::set('mail.encryption', 'ssl');

	    echo CRUDBooster::getSetting('smtp_driver').' -- ';
	    echo CRUDBooster::getSetting('smtp_host').' -- ';
	    echo CRUDBooster::getSetting('smtp_port').' -- ';
	    echo CRUDBooster::getSetting('smtp_username').' -- ';
	    echo CRUDBooster::getSetting('smtp_password').' -- ';
	
		exit();*/

	    /*\Config::set('mail.driver', 'smtp');
	    \Config::set('mail.host', 'smtp.mailgun.org');
	    \Config::set('mail.port', 25);
	    \Config::set('mail.username','no-reply@crocodic.net');
	    \Config::set('mail.password', '123456');
	    \Config::set('mail.encryption', 'tls');*/

	   /* $a = \Mail::send("crudbooster::emails.blank", ['content' => 'Ini hanya test email saja'], function ($message) {
	        $message->priority(1); 
	        $message->to(Request::get('email'));
	        //$message->from('notifikasi@estakios.co.id', 'Notifikasi');
	        $message->from('notifikasi@estakios.co.id', 'Notifikasi');
	        $message->subject('Test Email Saja Notifikasi');
	    });

	    print_r($a);print_r(1);*/
	    $rest['data'] = 'ss';
		return Esta::kirimemail(['to'=>Request::get('email'),'data'=>$rest,'template'=>'forgot_password_backend']);
	}

	public function postUploadFoto() {
		$storage = storage_path("app/uploads/esta/");
		$image = Request::get('photo');
		$filename = Request::get('filename');
		$image = base64_decode($image);
		$file  = $filename;
        if(file_put_contents(($storage.$file), $image)) {
    		$response['api_status']  = 1;
        	$response['api_message'] = 'Update photo berhasil';

        	return response()->json($response);
		}
	}

	public function getTesFcm() {
		$regid[] = Request::input('regid');
		$datafcm['title'] = 'EstaKios';
		$datafcm['content'] = 'Testing';
		return Esta::sendFCM($regid,$datafcm);
	}

	public function postRiwayatKomisi() {
		$id_agen = Request::get('id_agen');
		$tgl_awal = Request::get('tgl_awal');
		$tgl_akhir = Request::get('tgl_akhir');
		$limit = Request::get('limit');
		$offset = Request::get('offset');
		$bulan_default = date('Y-m-d', strtotime(date('Y-m-d'). ' - 3 months'));

		$arrays = DB::table('log_money')
			->where('id_agen',$id_agen)
			->where('kategori','Komisi');
			if(!empty($tgl_awal)) {
				$arrays = $arrays->whereBetween('created_at',[$tgl_awal.' 01:00:00',$tgl_akhir.' 23:59:59']);
			} else {
				$arrays = $arrays->whereBetween('created_at',[$bulan_default.' 01:00:00',date('Y-m-d H:i:s')]);
			}
			$arrays = $arrays
			->orderBy('id','DESC')
			->offset($offset)
            ->limit($limit)
			->get();
		
		$rest_json = array();
		$total_komisi = 0;
	  	foreach($arrays as $array) {
	  		$tr = DB::table($array->tbl_transaksi)->where('id',$array->id_transaksi)->first();
	  		$total_komisi += $tr->komisi;

	  		$st = ($tr->trans_desc == 'TARIK TUNAI' || $tr->trans_desc == NULL || $tr->trans_desc == 'Top up VA' || $tr->status == 'Delete' || $tr->status == 'Deleted' ? 'Clear' : $tr->status);
	  		if($tr->trans_desc != NULL) {
	  			if($st == 'Clear' || $st == 'Received' || $st == 'Approved') {

	  		//if($array->type == 'In') {
					$rest['id'] = $array->id;
					$rest['datetime'] = $array->created_at;
					$return = (strpos($array->description, 'Return') !== false ? 'Return ' : '');
					$rest['description'] = $return.($tr->trans_desc == 'BPJS Ketenagakerjaan' ? 'Cashback Pembayaran' : (strpos($tr->trans_desc, 'PRABAYAR') !== false ? 'Cashback Pembelian' : 'Cashback Pembayaran'));
					
					if($return == 'Return ') {
						$rest['nominal'] = '(Rp '.($tr->komisi <= 0 ? 0 : $tr->komisi).')';
					} else {
						$rest['nominal'] = 'Rp '.($tr->komisi <= 0 ? 0 : $tr->komisi);
					}
					$rest['type'] = $array->type;


					if($tr->trans_desc == 'PLN PASCABAYAR') {
						$rest['nama_product'] = 'Tagihan Listrik '.$tr->struk_blth;
					} elseif($tr->trans_desc == 'PULSA PASCABAYAR') {
						$rest['nama_product'] = 'Tagihan '.$tr->product_nama;
					} elseif($tr->trans_desc == 'PDAM') {
						$rest['nama_product'] = 'Tagihan PDAM '.$tr->struk_blth;
					} elseif($tr->trans_desc == 'BPJS Kesehatan') {
						$rest['nama_product'] = 'Tagihan BPJS Kesehatan '.$tr->struk_periode.' BLN';
					} else {
						$rest['nama_product'] = $tr->product_nama;
					}

					array_push($rest_json, $rest);
			//}
				}
			}
	  	}
	  	
	  	$total = DB::table('log_money')
			->where('id_agen',$id_agen)
			->where('kategori','Komisi');
			if(!empty($tgl_awal)) {
				$total = $total->whereBetween('created_at',[$tgl_awal.' 01:00:00',$tgl_akhir.' 23:59:59']);
			}
			$total = $total
			->orderBy('id','DESC')
			->get();
		$totall = 0;
		foreach($total as $tl) {
			$trr = DB::table($tl->tbl_transaksi)->where('id',$tl->id_transaksi)->first();
			$return = (strpos($tl->description, 'Return') !== false ? 'Return ' : '');
			if($return == 'Return ') {
				$totall -= $trr->komisi;
			} else {
		  		$totall += $trr->komisi;
		  	}
		}

	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;
	    $response['total_komisi'] = ($totall <= 0 ? 0 : $totall);

	  	return response()->json($response);
	}

	public function postRiwayatTransaksi() {
		$id_agen = Request::get('id_agen');
		$tgl_awal = Request::get('tgl_awal');
		$tgl_akhir = Request::get('tgl_akhir');
		$limit = Request::get('limit');
		$offset = Request::get('offset');
		$bulan_default = date('Y-m-d', strtotime(date('Y-m-d'). ' - 3 months'));

		$arrays = DB::table('log_money')
			->where('id_agen',$id_agen)
			->whereIN('kategori',['Transaksi']);
			if(!empty($tgl_awal)) {
				$arrays = $arrays->whereBetween('created_at',[$tgl_awal.' 01:00:00',$tgl_akhir.' 23:59:59']);
			} else {
				$arrays = $arrays->whereBetween('created_at',[$bulan_default.' 01:00:00',date('Y-m-d H:i:s')]);
			}
			$arrays = $arrays
			->orderBy('id','DESC')
			->offset($offset)
            ->limit($limit)
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$detail = DB::table($array->tbl_transaksi)
	  			->where('id',$array->id_transaksi)
	  			->first();
	  		$return = (strpos($array->description, 'Return') !== false ? 'RETURN ' : '');

	  		$st = ($detail->trans_desc == 'TARIK TUNAI' || $detail->trans_desc == NULL || $detail->trans_desc == 'Top up VA' || $detail->status == 'Delete' || $detail->status == 'Deleted' ? 'Clear' : $detail->status);
	  		if($detail->trans_desc != NULL) {
	  			if($st == 'Clear' || $st == 'Received' || $st == 'Approved') {
				$rest['id'] = $array->id;
				$rest['datetime'] = Esta::change_date_format($array->created_at,'d M Y H:i');

				$rest['trans_desc'] = $detail->trans_desc;
				

				if($rest['trans_desc'] == 'PLN PRABAYAR') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_rp_bayar,0,',','.');
					$rest['token'] = substr($detail->struk_token,0,-1);
					$rest['nomor'] = $detail->no_meter;
					$rest['type'] = 'PLN';
				} elseif($rest['trans_desc'] == 'PLN PASCABAYAR') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->no_meter;
					$rest['type'] = 'PLN';
				} elseif($rest['trans_desc'] == 'PULSA PRABAYAR') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->no_hp;
					$rest['type'] = 'Pulsa';
				} elseif($rest['trans_desc'] == 'PULSA PASCABAYAR') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->no_hp;
					$rest['type'] = 'Pulsa';
				} elseif($rest['trans_desc'] == 'PDAM PASCABAYAR') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->struk_norek;
					$rest['type'] = 'PDAM';
				} elseif($rest['trans_desc'] == 'BPJS Kesehatan') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->struk_no_va_keluarga;
					$rest['type'] = 'BPJS';
				} elseif($rest['trans_desc'] == 'Top up VA') {
					$rest['amount'] = 'Rp '.number_format($detail->trans_amount-$detail->biaya_admin_bank-$detail->biaya_admin,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->agen_no_hp;
					$rest['type'] = 'Topup';
				} elseif($rest['trans_desc'] == 'TARIK TUNAI') {
					$rest['amount'] = 'Rp '.number_format($detail->trans_amount+$detail->biaya_admin,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->agen_no_hp;
					$rest['type'] = 'Tarik Tunai';
				} else {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = '';
					$rest['type'] = 'Pulsa';
				}

				if($rest['trans_desc'] == 'Top up VA') {
					$rest['cashback'] = '';
					$rest['voucher_nama'] = '';
					$rest['voucher_amount'] = '';
					$rest['total_pembayaran'] = '';
					$rest['product_nama'] = 'Top Up';
					$rest['trans_desc'] = 'TOP UP ESTAWALLET';
				} elseif($rest['trans_desc'] == 'TARIK TUNAI') {
					$rest['cashback'] = '';
					$rest['voucher_nama'] = '';
					$rest['voucher_amount'] = '';
					$rest['total_pembayaran'] = '';
					$rest['product_nama'] = 'Tarik Tunai';
					$rest['trans_desc'] = 'TARIK TUNAI';
				} else {
					$rest['cashback'] = 'Rp '.number_format($detail->komisi,0,',','.');
					$rest['voucher_nama'] = ($detail->voucher_nama != '' ? $detail->voucher_nama : '');
					$rest['voucher_amount'] = ($detail->voucher_amount >= 1 ? 'Rp '.number_format($detail->voucher_amount,0,',','.') : '');
					$rest['total_pembayaran'] = 'Rp '.number_format($detail->struk_total_pembayaran,0,',','.');
					$rest['trans_desc'] = $return.$detail->trans_desc;
					if($rest['trans_desc'] == 'PLN PASCABAYAR') {
						$rest['product_nama'] = 'Tagihan '.$detail->struk_blth;
					} elseif($rest['trans_desc'] == 'BPJS Kesehatan') {
						$rest['product_nama'] = 'Tagihan '.$detail->struk_periode.' BLN';
					} elseif($rest['trans_desc'] == 'PDAM') {
						$rest['product_nama'] = 'Tagihan '.$detail->struk_blth;
					} else {
						$rest['product_nama'] = $detail->product_nama;
					}
				}
				$rest['trans_desc'] = $return.$detail->trans_desc;
				array_push($rest_json, $rest);
		}
		}
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postAllVoucher() {
		$id_agen = Request::get('id_agen');

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$arrays = DB::table('trans_voucher_child')
			->where('id_agen',$id_agen)
			->where('used','No')
			->where('voucher_expired','>=',date('Y-m-d H:i:s'))
			->orderBy('id','desc')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$detail_voucher = DB::table('voucher')
				->where('id',$array->id_voucher)
				->first();

			$rest['id'] = $array->id;
			$rest['nama'] = $detail_voucher->nama;
			$rest['expired_date'] = $detail_voucher->expired_date;
			$rest['amount'] = $detail_voucher->amount;
			$rest['description'] = $detail_voucher->description;
			$rest['tagline'] = $detail_voucher->tagline;
			$rest['syarat_ketentuan'] = $detail_voucher->syarat_ketentuan;
			$rest['image'] = asset('').$detail_voucher->image;
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postVoucherBpjs() {
		$id_agen = Request::get('id_agen');

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$arrays = DB::table('trans_voucher_child')
			->whereIN('product',['BPJS','All'])
			->where('id_agen',$id_agen)
			->where('used','No')
			->where('voucher_expired','>=',date('Y-m-d H:i:s'))
			->orderBy('id','desc')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->voucher_nama;
			$rest['amount'] = $array->voucher_amount;
			$rest['masa_berlaku'] = date("Y-m-d H:i:s", strtotime($array->voucher_expired));
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postBpjsSubmit() {
		$type = Request::get('type');
		$no_hp = Request::get('no_hp');
		$id_pelanggan = Request::get('id_pelanggan');
		$pin = Request::get('pin');
		$amount = Request::get('amount');
		$id_agen = Request::get('id_agen');
		//$id_voucher = Request::get('id_voucher');
		$id_transaksi = Request::get('id_transaksi');

		$id_voucher_child = Request::get('id_voucher');
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$tipe_layanan = DB::table('pan_tipe_layanan')->where('kode','13')->first()->id;
		$kode_layanan = DB::table('pan_kode_layanan')->where('kode','4')->first()->id;
		$id_product = DB::table('pan_product')->where('id_tipe_layanan',$tipe_layanan)->where('id_kode_layanan',$kode_layanan)->first()->id;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}

			$detail_voucher = DB::table('voucher')
				->where('id',$id_voucher)
				->first();

			$detail_product = DB::table('pan_product')
				->where('id',$id_product)
				->first();

			$kode = Esta::nomor_transaksi('trans_bpjs',CRUDBooster::getsetting('transaksi_bpjs'));

			$detail_inquiry = DB::table('log_jatelindo_bit')
				->where('id',$id_transaksi)
				->where('type','BPJS Kesehatan')
				->where('status','Inquiry')
				->where('jenis','res')
				->first();

			$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);
			$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

			$biaya_admin = $detail_inquiry->bpjsks_biaya_admin;
			if($trans_amount == 0) {
				$trans_amount = 0;
			} else {
				$trans_amount = $trans_amount+$biaya_admin;
			}
			$trans_amount = $trans_amount-$komisi;

			/*echo $trans_amount.'-'.($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

			Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi BPJS','Transaksi BPJS Kesehatan','Out','Transaksi');
			Esta::log_money($id_agen,($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),date('Y-m-d H:i:s'),'Komisi Transaksi BPJS','Komisi Transaksi BPJS Kesehatan','In','Komisi');
			exit();*/

			$jml_bulan = 1;

			//if($detail_agen->saldo >= $trans_amount) {
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['created_user'] = Esta::user($id_agen);
				$sv['updated_user'] = Esta::user($id_agen);
				$sv['trans_no'] = $kode;
				$sv['ref_trans_no'] = $kode;
				$sv['trans_date'] = date('Y-m-d H:i:s');
				$sv['trans_desc'] = 'BPJS Kesehatan';
				$sv['currency'] = 'IDR';
				$sv['trans_amount'] = $trans_amount;
				$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
				//$sv['create_user'] = $detail_agen->nama;
				$sv['id_agen'] = $id_agen;
				$sv['status'] = 'Pending';
				$sv['rekon_amount'] = 0;
				$sv['status_match'] = 'Waiting Rekon';
				$sv['no_hp'] = $no_hp;
				$sv['id_pelanggan'] = $id_pelanggan;
				$sv['id_product'] = $id_product;
				$sv['agen_nama'] = $detail_agen->nama;
				$sv['agen_email'] = $detail_agen->email;
				$sv['agen_kode'] = $detail_agen->kode;
				$sv['agen_no_hp'] = $detail_agen->no_hp;
				$sv['agen_level'] = $detail_agen->status_agen;
				$sv['agen_referall'] = $detail_agen->kode_referall_agen;
				$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
				$sv['agen_status_aktif'] = $detail_agen->status_aktif;
				$sv['agen_nik'] = $detail_agen->nik;
				$sv['agen_tgl_register'] = $detail_agen->tgl_register;
				$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
				$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
				$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
				$sv['agen_agama'] = $detail_agen->agama;
				$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
				$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
				$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
				$sv['agen_prov'] = $detail_agen->prov;
				$sv['agen_kab'] = $detail_agen->kab;
				$sv['agen_kec'] = $detail_agen->kec;
				$sv['agen_kel'] = $detail_agen->kel;
				$sv['product_nama'] = $detail_product->product_name;
				$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
				$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
				$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
				$sv['product_komisi_basic'] = $detail_product->komisi_basic;
				$sv['product_komisi_premium'] = $detail_product->komisi_premium;
				$sv['product_amount'] = $amount;
				$sv['product_margin'] = $detail_product->margin;
				$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
				$sv['product_biaya_admin_1'] = $detail_product->admin_1;
				$sv['product_biaya_admin_2'] = $detail_product->admin_2;
				$sv['product_biaya_Admin_3'] = $detail_product->admin_3;
				$sv['product_potongan'] = $detail_product->potongan;
				$sv['voucher_nama'] = $detail_voucher->nama;
				$sv['voucher_expired_date'] = $detail_voucher->expired_date;
				$sv['voucher_amount'] = $detail_voucher->amount;
				$sv['voucher_tagline'] = $detail_voucher->tagline;
				$sv['voucher_description'] = $detail_voucher->description;
				$sv['id_voucher'] = $detail_voucher->id;

				$save = DB::table('trans_bpjs')
					->insertGetId($sv);

				//if($save) {
					/*potong saldo*/
					Esta::log_money($id_agen,$sv['trans_amount'],date('Y-m-d H:i:s'),'Transaksi BPJS','Transaksi BPJS Kesehatan','Out','Transaksi','trans_bpjs',$save);
					Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi BPJS','Komisi Transaksi BPJS Kesehatan','In','Komisi','trans_bpjs',$save);

					/*send iso*/
					$send_iso = Esta::send_iso_bpjsks($id_product,$no_hp,'0200','Payment',$id_transaksi,$id_pelanggan,$jml_bulan);
					
					if($send_iso['39'] == 'Pending' || $send_iso['39'] == '06' || $send_iso['39'] == '09' || $send_iso['39'] == '18' || $send_iso['39'] == '22') {
						$up3['status'] = 'Pending';
						$up3['stan'] = $send_iso['stan'];
						$up3['no_va'] = $send_iso['bpjsks_no_va_keluarga'];
						$up3['jpa_ref'] = $send_iso['bpjsks_jpa_refnum'];

						$update = DB::table('trans_bpjs')
							->where('id',$save)
							->update($up3);

						$response['api_status']  = 3;
				    	$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
				    	$response['type_dialog']  = 'Informasi';
				    	$response['id_transaksi']  = 0;

				    	$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

						$response['bpjsks_jpa_refnum'] = $send_iso['bpjsks_jpa_refnum'];
						$response['bpjsks_nama'] = $send_iso['bpjsks_nama'];
						$response['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
						$response['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
						$response['bpjsks_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0)).' Orang';
						$response['periode'] = ltrim($send_iso['periode'],0).' Bulan';
						$response['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
						$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
						$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);

						$response['bpjsks_tgl_lunas'] = substr($send_iso['bpjsks_tgl_lunas'],6,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],4,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],0,4).' '.substr($send_iso['bpjsks_tgl_lunas'],8,2).':'.substr($send_iso['bpjsks_tgl_lunas'],10,2);

						$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
						$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
						$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
						
						$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
						$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
						$response['val_total_bayar'] = $response['val_bpjsks_total_premi']+$response['val_bpjsks_biaya_admin'];
						$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');

						$val_total_pembayaran = $response['val_total_bayar']-$response['val_komisi'];
						$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
						$response['total_pembayaran'] = 'Rp '.number_format($val_total_pembayaran,0,',','.');

						$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
						$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
						$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
						$response['info'] = $send_iso['62'];


						$up_struk['struk_tgl_lunas'] = $response['bpjsks_tgl_lunas'];
						$up_struk['struk_no_ref'] = $response['bpjsks_jpa_refnum'];
						$up_struk['struk_no_va_keluarga'] = $response['bpjsks_no_va_keluarga'];
						$up_struk['struk_no_va_kepala_keluarga'] = $response['bpjsks_no_va_kepala_keluarga'];
						$up_struk['struk_nama_peserta'] = $response['bpjsks_nama'];
						$up_struk['struk_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
						$up_struk['struk_periode'] = ltrim($send_iso['periode'],0);
						$up_struk['struk_jml_tagihan'] = $response['val_bpjsks_total_premi'];
						$up_struk['struk_admin_bank'] = $response['val_bpjsks_biaya_admin'];
						$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
						$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran']-$sv['voucher_amount'];
						$up_struk['jpa_ref'] = $response['bpjsks_jpa_refnum'];
						$up_struk['struk_info'] = $response['info'];

						$update = DB::table('trans_bpjs')
							->where('id',$save)
							->update($up_struk);

						return response()->json($response);
						exit();
					}

					if($send_iso['39'] != '00') { /*jika respon error*/
						/*kembalikan saldo*/
						if($send_iso['39'] != '18' || $send_iso['39'] != '06' || $send_iso['39'] != '09') {
							Esta::log_money($id_agen,$sv['trans_amount'],date('Y-m-d H:i:s'),'Transaksi BPJS','Return Transaksi BPJS Kesehatan','In','Transaksi','trans_bpjs',$save);
							Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi BPJS','Return Komisi Transaksi BPJS Kesehatan','Out','Komisi','trans_bpjs',$save);

							$up['return_saldo'] = 'Yes';
							$up['flag_reversal'] = 'Reversal Sukses';
							$up['status'] = 'Error';
							//$up['status_match'] = 'Error';
							$up['error_code'] = $send_iso['39'];
							$up['stan'] = $send_iso['stan'];
							$up['no_va'] = $send_iso['bpjsks_no_va_keluarga'];
							$up['jpa_ref'] = $send_iso['bpjsks_jpa_refnum'];

							$update = DB::table('trans_bpjs')
								->where('id',$save)
								->update($up);

							$response['api_status']  = 2;
					    	$response['api_message'] = Esta::show_error('BPJSTK',$send_iso['39'],'0');
					    	$response['type_dialog']  = 'Error';
					    	$response['id_transaksi']  = 0;

					    	$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

							$response['bpjsks_jpa_refnum'] = $send_iso['bpjsks_jpa_refnum'];
							$response['bpjsks_nama'] = $send_iso['bpjsks_nama'];
							$response['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
							$response['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
							$response['bpjsks_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0)).' Orang';
							$response['periode'] = ltrim($send_iso['periode'],0).' Bulan';
							$response['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
							$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
							$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);

							$response['bpjsks_tgl_lunas'] = substr($send_iso['bpjsks_tgl_lunas'],6,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],4,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],0,4).' '.substr($send_iso['bpjsks_tgl_lunas'],8,2).':'.substr($send_iso['bpjsks_tgl_lunas'],10,2);

							$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
							$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
							$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
							
							$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
							$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
							$response['val_total_bayar'] = $response['val_bpjsks_total_premi']+$response['val_bpjsks_biaya_admin'];
							$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');

							$val_total_pembayaran = $response['val_total_bayar']-$response['val_komisi'];
							$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
							$response['total_pembayaran'] = 'Rp '.number_format($val_total_pembayaran,0,',','.');

							$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
							$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
							$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
							$response['info'] = $send_iso['62'];


							$up_struk['struk_tgl_lunas'] = $response['bpjsks_tgl_lunas'];
							$up_struk['struk_no_ref'] = $response['bpjsks_jpa_refnum'];
							$up_struk['struk_no_va_keluarga'] = $response['bpjsks_no_va_keluarga'];
							$up_struk['struk_no_va_kepala_keluarga'] = $response['bpjsks_no_va_kepala_keluarga'];
							$up_struk['struk_nama_peserta'] = $response['bpjsks_nama'];
							$up_struk['struk_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
							$up_struk['struk_periode'] = ltrim($send_iso['periode'],0);
							$up_struk['struk_jml_tagihan'] = $response['val_bpjsks_total_premi'];
							$up_struk['struk_admin_bank'] = $response['val_bpjsks_biaya_admin'];
							$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
							$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran']-$sv['voucher_amount'];
							$up_struk['jpa_ref'] = $response['bpjsks_jpa_refnum'];
							$up_struk['struk_info'] = $response['info'];

							$update = DB::table('trans_bpjs')
								->where('id',$save)
								->update($up_struk);

					    	return response()->json($response);
						} else {
							$up['status'] = 'Error';
							//$up['status_match'] = 'Error';
							$up['error_code'] = $send_iso['39'];
							$up['stan'] = $send_iso['stan'];
							$up['no_va'] = $send_iso['bpjsks_no_va_keluarga'];
							$up['jpa_ref'] = $send_iso['bpjsks_jpa_refnum'];

							$update = DB::table('trans_bpjs')
								->where('id',$save)
								->update($up);

							$response['api_status']  = 2;
					    	$response['api_message'] = Esta::show_error('BPJSTK',$send_iso['39'],'0');
					    	$response['type_dialog']  = 'Error';
					    	$response['id_transaksi']  = 0;

					    	$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

							$response['bpjsks_jpa_refnum'] = $send_iso['bpjsks_jpa_refnum'];
							$response['bpjsks_nama'] = $send_iso['bpjsks_nama'];
							$response['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
							$response['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
							$response['bpjsks_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0)).' Orang';
							$response['periode'] = ltrim($send_iso['periode'],0).' Bulan';
							$response['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
							$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
							$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);

							$response['bpjsks_tgl_lunas'] = substr($send_iso['bpjsks_tgl_lunas'],6,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],4,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],0,4).' '.substr($send_iso['bpjsks_tgl_lunas'],8,2).':'.substr($send_iso['bpjsks_tgl_lunas'],10,2);

							$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
							$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
							$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
							
							$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
							$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
							$response['val_total_bayar'] = $response['val_bpjsks_total_premi']+$response['val_bpjsks_biaya_admin'];
							$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');

							$val_total_pembayaran = $response['val_total_bayar']-$response['val_komisi'];
							$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
							$response['total_pembayaran'] = 'Rp '.number_format($val_total_pembayaran,0,',','.');

							$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
							$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
							$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
							$response['info'] = $send_iso['62'];


							$up_struk['struk_tgl_lunas'] = $response['bpjsks_tgl_lunas'];
							$up_struk['struk_no_ref'] = $response['bpjsks_jpa_refnum'];
							$up_struk['struk_no_va_keluarga'] = $response['bpjsks_no_va_keluarga'];
							$up_struk['struk_no_va_kepala_keluarga'] = $response['bpjsks_no_va_kepala_keluarga'];
							$up_struk['struk_nama_peserta'] = $response['bpjsks_nama'];
							$up_struk['struk_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
							$up_struk['struk_periode'] = ltrim($send_iso['periode'],0);
							$up_struk['struk_jml_tagihan'] = $response['val_bpjsks_total_premi'];
							$up_struk['struk_admin_bank'] = $response['val_bpjsks_biaya_admin'];
							$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
							$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran']-$sv['voucher_amount'];
							$up_struk['jpa_ref'] = $response['bpjsks_jpa_refnum'];
							$up_struk['struk_info'] = $response['info'];

							$update = DB::table('trans_bpjs')
								->where('id',$save)
								->update($up_struk);

					    	return response()->json($response);
					    	exit();
					    }
					}

					$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_bpjs');
					if($send_sms_transaksi == 'Yes') {
						$msg = CRUDBooster::getsetting('sukses_transaksi_bpjs');
						$msg = str_replace('[no_hp]', $no_hp, $msg);
						$msg = str_replace('[nama]', $detail_agen->nama, $msg);
						$msg = str_replace('[komisi]', $sv['komisi'], $msg);
						Esta::send_sms($no_hp, $msg);
					}

					if(!empty($id_voucher)) {
						$uv['used'] = 'Yes';

						$up_voucher = DB::table('trans_voucher_child')
							->where('id',$id_voucher_child)
							->where('id_agen',$id_agen)
							->update($uv);
					}

					$up4['no_va'] = $send_iso['bpjsks_no_va_keluarga'];

					$update = DB::table('trans_bpjs')
						->where('id',$save)
						->update($up4);

					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi BPJS berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['id_transaksi']  = $send_iso['id_log'];

					$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

					$response['bpjsks_jpa_refnum'] = $send_iso['bpjsks_jpa_refnum'];
					$response['bpjsks_nama'] = $send_iso['bpjsks_nama'];
					$response['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
					$response['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
					$response['bpjsks_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0)).' Orang';
					$response['periode'] = ltrim($send_iso['periode'],0).' Bulan';
					$response['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
					$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
					$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);

					$response['bpjsks_tgl_lunas'] = substr($send_iso['bpjsks_tgl_lunas'],6,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],4,2).'-'.substr($send_iso['bpjsks_tgl_lunas'],0,4).' '.substr($send_iso['bpjsks_tgl_lunas'],8,2).':'.substr($send_iso['bpjsks_tgl_lunas'],10,2);

					$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
					$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
					$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
					
					$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
					$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
					$response['val_total_bayar'] = $response['val_bpjsks_total_premi']+$response['val_bpjsks_biaya_admin'];
					$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');

					$val_total_pembayaran = $response['val_total_bayar']-$response['val_komisi'];
					$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
					$response['total_pembayaran'] = 'Rp '.number_format($val_total_pembayaran,0,',','.');

					$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
					$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
					$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
					$response['info'] = $send_iso['62'];


					$up_struk['struk_tgl_lunas'] = $response['bpjsks_tgl_lunas'];
					$up_struk['struk_no_ref'] = $response['bpjsks_jpa_refnum'];
					$up_struk['struk_no_va_keluarga'] = $response['bpjsks_no_va_keluarga'];
					$up_struk['struk_no_va_kepala_keluarga'] = $response['bpjsks_no_va_kepala_keluarga'];
					$up_struk['struk_nama_peserta'] = $response['bpjsks_nama'];
					$up_struk['struk_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
					$up_struk['struk_periode'] = ltrim($send_iso['periode'],0);
					$up_struk['struk_jml_tagihan'] = $response['val_bpjsks_total_premi'];
					$up_struk['struk_admin_bank'] = $response['val_bpjsks_biaya_admin'];
					$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
					$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran']-$sv['voucher_amount'];
					$up_struk['jpa_ref'] = $response['bpjsks_jpa_refnum'];
					$up_struk['struk_info'] = $response['info'];
					$up_struk['status'] = 'Clear';

					$update = DB::table('trans_bpjs')
						->where('id',$save)
						->update($up_struk);

					/*email*/
					$view     = view('struk/struk_bpjs_kesehatan',$response)->render();
					$filename = "Struk-BPJS-Kesehatan-".$no_hp;
					$pdf      = App::make('dompdf.wrapper');

					$path = storage_path('app/uploads/'.$filename.'.pdf');

					$pdf->loadHTML($view);
					$pdf->setPaper('A4','landscape');
					$output = $pdf->output();

					file_put_contents($path, $output);

					$attachments = [$path];
					$email = $detail_agen->email;
					if(!empty($email) || $detail_agen->notif_email != 'No') {
					    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_bpjs_kesehatan','attachments'=>$attachments]);
					}
			/*} else {
				$response['api_status']  = 3;
		    	$response['api_message'] = 'Saldo anda tidak mencukupi';
		    	$response['type_dialog']  = 'Error';
		    	$response['id_transaksi']  = 0;
			}*/

		return response()->json($response);
	}

	public function postCheckTagihanBpjs() {
		$type = Request::get('type');
		$no_hp = Request::get('no_hp');
		$id_agen = Request::get('id_agen');
		$id_pelanggan = Request::get('id_pelanggan');
		$bayar_hingga = Request::get('bayar_hingga');

		/*if($type == 'Kesehatan') {
			$id_product = 1020;
		} else {
			$id_product = 1020;
		}*/

		$tipe_layanan = DB::table('pan_tipe_layanan')->where('kode','13')->first()->id;
		$kode_layanan = DB::table('pan_kode_layanan')->where('kode','4')->first()->id;
		$id_product = DB::table('pan_product')->where('id_tipe_layanan',$tipe_layanan)->where('id_kode_layanan',$kode_layanan)->first()->id;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$d1 = $bayar_hingga;
		$d2 = date('Y-m-d');
		$jml_bulan = (int)abs((strtotime($d1) - strtotime($d2))/(60*60*24*30))+1;

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$kode = Esta::nomor_transaksi('trans_bpjs',CRUDBooster::getsetting('transaksi_bpjs'));

		$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);

		/*send iso*/
		$send_iso = Esta::send_iso_bpjsks($id_product,$no_hp,'0200','Inquiry',0,$id_pelanggan,$jml_bulan);

		if($send_iso['39'] == '00') {
			$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$rest['id_transaksi'] = $send_iso['id_log'];

			$rest['bpjsks_nama'] = $send_iso['bpjsks_nama'];
			$rest['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
			$rest['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
			$rest['bpjsks_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0)).' Orang';
			$rest['periode'] = ltrim($send_iso['periode'],0).' BLN';
			$rest['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
			$rest['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
			$rest['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
			$rest['val_total_bayar'] = $rest['val_bpjsks_total_premi']+$rest['val_bpjsks_biaya_admin'];
			$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');

			$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
			$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');

			$rest['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
			$rest['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
			$rest['komisi'] = 'Rp '.number_format($rest['val_komisi'],0,',','.');

			$response['api_status']  = 1;
	    	$response['api_message'] = 'Berhasil';
	    	$response['type_dialog']  = 'Informasi';
	    	$response['item']  = $rest;
		} else {
			$rest['jml_tagihan'] = 0;
			$rest['komisi'] = 0;

			$response['api_status']  = 0;
	    	$response['api_message'] = Esta::show_error('BPJSKS',$send_iso['39'],$send_iso['bpjsks_nama']);
	    	$response['type_dialog']  = 'Error';
	    	$response['item']  = $rest;
		}
		return response()->json($response);
	}

	public function postBulanBpjs() {
		$stop_date = date('Y-m-d', strtotime(date('Y-m-d') . ' +11 month'));

		$first = date('Y-m-d');
		$last = $stop_date;
		$step = '+1 month';
		$format = 'Y-m-d';
		$dates = array();
		$current = strtotime( $first );
		$last = strtotime( $last );

		$rest_json = array();
		while( $current <= $last ) {

			$dates[] = date( $format, $current );
			/*echo date( 'Y-m-d', $current );
			exit();*/
			$bulan = date( 'm', $current );
			$nama_bulan = Esta::date_indo($bulan,'singkat');
			$rest['date_value'] = $nama_bulan.' '.date( 'Y', $current );
			$rest['date'] = date( 'Y-m-d', $current );
			$current = strtotime( $step, $current );
			array_push($rest_json, $rest);
		}

		$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postVoucherPdam() {
		$id_agen = Request::get('id_agen');

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$arrays = DB::table('trans_voucher_child')
			->whereIN('product',['PDAM','All'])
			->where('id_agen',$id_agen)
			->where('used','No')
			->where('voucher_expired','>=',date('Y-m-d H:i:s'))
			->orderBy('id','desc')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->voucher_nama;
			$rest['amount'] = $array->voucher_amount;
			$rest['masa_berlaku'] = date("Y-m-d H:i:s", strtotime($array->voucher_expired));
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postPdamPascabayarSubmit() {
		$no_hp = Request::get('no_hp');
		$id_product = Request::get('id_area');
		$id_pelanggan = Request::get('id_pelanggan');
		//$id_voucher = Request::get('id_voucher');
		$pin = Request::get('pin');
		$amount = Request::get('amount');
		$id_agen = Request::get('id_agen');
		$id_transaksi = Request::get('id_transaksi');

		$id_voucher_child = Request::get('id_voucher');
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}

			$detail_product = DB::table('pan_product')
				->where('id',$id_product)
				->first();

			$detail_voucher = DB::table('voucher')
				->where('id',$id_voucher)
				->first();

			$kode = Esta::nomor_transaksi('trans_pdam',CRUDBooster::getsetting('transaksi_pdam'));

			$detail_inquiry = DB::table('log_jatelindo_bit')
				->where('id',$id_transaksi)
				->where('type','PDAM Postpaid')
				->where('status','Inquiry')
				->where('jenis','res')
				->first();

			$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);
			$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$biaya_admin = $detail_inquiry->pdam_biaya_admin;
			if($trans_amount == 0) {
				$trans_amount = 0;
			} else {
				$trans_amount = $trans_amount+$biaya_admin;
			}
			$trans_amount = $trans_amount-$komisi;

			//if($detail_agen->saldo >= $amount) {
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['created_user'] = Esta::user($id_agen);
				$sv['updated_user'] = Esta::user($id_agen);
				$sv['trans_no'] = $kode;
				$sv['ref_trans_no'] = $kode;
				$sv['trans_date'] = date('Y-m-d H:i:s');
				$sv['trans_desc'] = 'PDAM PASCABAYAR';
				$sv['currency'] = 'IDR';
				$sv['trans_amount'] = $trans_amount;
				$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
				//$sv['create_user'] = $detail_agen->nama;
				$sv['id_agen'] = $id_agen;
				$sv['status'] = 'Pending';
				$sv['rekon_amount'] = 0;
				$sv['status_match'] = 'Waiting Rekon';
				$sv['no_hp'] = $no_hp;
				$sv['id_pelanggan'] = $id_pelanggan;
				$sv['id_product'] = $id_product;
				$sv['agen_nama'] = $detail_agen->nama;
				$sv['agen_email'] = $detail_agen->email;
				$sv['agen_no_hp'] = $detail_agen->no_hp;
				$sv['agen_kode'] = $detail_agen->kode;
				$sv['agen_level'] = $detail_agen->status_agen;
				$sv['agen_referall'] = $detail_agen->kode_referall_agen;
				$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
				$sv['agen_status_aktif'] = $detail_agen->status_aktif;
				$sv['agen_nik'] = $detail_agen->nik;
				$sv['agen_tgl_register'] = $detail_agen->tgl_register;
				$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
				$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
				$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
				$sv['agen_agama'] = $detail_agen->agama;
				$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
				$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
				$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
				$sv['agen_prov'] = $detail_agen->prov;
				$sv['agen_kab'] = $detail_agen->kab;
				$sv['agen_kec'] = $detail_agen->kec;
				$sv['agen_kel'] = $detail_agen->kel;
				$sv['product_nama'] = $detail_product->product_name;
				$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
				$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
				$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
				$sv['product_komisi_basic'] = $detail_product->komisi_basic;
				$sv['product_komisi_premium'] = $detail_product->komisi_premium;
				$sv['product_amount'] = $amount;
				$sv['product_margin'] = $detail_product->margin;
				$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
				$sv['product_biaya_admin_1'] = $detail_product->admin_1;
				$sv['product_biaya_admin_2'] = $detail_product->admin_2;
				$sv['product_biaya_Admin_3'] = $detail_product->admin_3;
				$sv['product_potongan'] = $detail_product->potongan;
				$sv['voucher_nama'] = $detail_voucher->nama;
				$sv['voucher_expired_date'] = $detail_voucher->expired_date;
				$sv['voucher_amount'] = $detail_voucher->amount;
				$sv['voucher_tagline'] = $detail_voucher->tagline;
				$sv['voucher_description'] = $detail_voucher->description;
				$sv['id_voucher'] = $detail_voucher->id;

				$save = DB::table('trans_pdam')
					->insertGetId($sv);

				if($save) {
					/*potong saldo*/
					Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PDAM','Transaksi PDAM','Out','Transaksi','trans_pdam',$save);
					Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi PDAM','Komisi Transaksi PDAM','In','Komisi','trans_pdam',$save);

					/*send iso*/
					$send_iso = Esta::send_iso_pdam($id_product,$no_hp,'0200','Payment',$id_transaksi, $no_meter);
					//dd($send_iso);
					/*print_r($send_iso);
					exit();*/

					if($send_iso['status'] == 'Reversal' || $send_iso['status'] == 'Reversal Repeat' || $send_iso['status'] == 'Reversal Repeat 1'/* || $send_iso['status'] == 'Payment'*/) {

						if($send_iso['39'] == '00' || $send_iso['39'] == '94') {
							Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PDAM','Return Transaksi PDAM','In','Transaksi','trans_pdam',$save);
							Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi PDAM','Return Komisi Transaksi PDAM','Out','Komisi','trans_pdam',$save);

							$up['return_saldo'] = 'Yes';
							$up['flag_reversal'] = 'Reversal Sukses';
							$up['status'] = 'Pending';
							//$up['status_match'] = 'Pending';
							$up['error_code'] = $send_iso['39'];
							$up['stan'] = $send_iso['11'];
							$up['jpa_ref'] = $send_iso['pdam_no_ref_biller'];

							$update = DB::table('trans_pdam')
								->where('id',$save)
								->update($up);

							$response['nama_pelanggan']  = $send_iso['pdam_customer_name'];
							$response['pdam_no_ref_biller']  = $send_iso['pdam_no_ref_biller'];
					    	//$response['pdam_blth']  = $send_iso['pdam_blth'];
					    	$response['tagihan']  = 'Rp '.number_format(str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0)),0,',','.');
					    	$response['biaya_admin']  = 'Rp '.number_format(ltrim($send_iso['pdam_biaya_admin'],0),0,',','.');

							$response['val_tagihan']  = str_replace('-', '', ltrim($send_iso['pdam_rupiah_tagihan'],0));
					    	$response['val_biaya_admin']  = ltrim($send_iso['pdam_biaya_admin'],0);
					    	$response['pdam_blth'] = $send_iso['pdam_blth'];
							$response['jml_lembar_tagihan'] = $send_iso['jml_lembar_tagihan'];
							$response['pdam_waktu_lunas'] = $send_iso['pdam_waktu_lunas'];
							$response['pdam_idpel'] = $send_iso['pdam_idpel'];

							$response['val_total_bayar'] = $response['val_tagihan']+$response['val_biaya_admin'];
							$val_total_pembayaran = $response['val_total_bayar']-$sv['komisi']-$sv['voucher_amount'];
							$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

							$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
							$response['total_pembayaran'] = 'Rp '.number_format($response['val_total_pembayaran'],0,',','.');
							$response['tgl_lunas'] = $send_iso['pdam_waktu_lunas'];


							$up_struk['struk_tgl_lunas'] = $response['tgl_lunas'];
							$up_struk['struk_norek'] = $response['pdam_idpel'];
							$up_struk['struk_blth'] = $response['pdam_blth'];
							$up_struk['struk_nama_pelanggan'] = $response['nama_pelanggan'];
							$up_struk['struk_jpa_ref'] = $response['pdam_no_ref_biller'];
							$up_struk['struk_rp_tag'] = str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0));
							$up_struk['struk_biaya_admin'] = $response['val_biaya_admin'];
							$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
							$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran'];
							$up_struk['jpa_ref'] = $send_iso['pdam_no_ref_switching'];

							$update = DB::table('trans_pdam')
								->where('id',$save)
								->update($up_struk);

							$response['api_status']  = 2;
					    	$response['api_message'] = 'Transaksi gagal, Mohon coba kembali';
					    	//$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
					    	$response['type_dialog']  = 'Error';
					    	$response['id_transaksi']  = 0;

					    	return response()->json($response);
						} else {
							/*selain 00 dan 94 masukin ongoing*/
							$up['status'] = 'Pending';
							//$up['status_match'] = 'Pending';
							$up['error_code'] = $send_iso['39'];
							$up['stan'] = $send_iso['11'];
							$up['jpa_ref'] = $send_iso['pdam_no_ref_biller'];

							$update = DB::table('trans_pdam')
								->where('id',$save)
								->update($up);

							$response['api_status']  = 2;
					    	//$response['api_message'] = 'Transaksi gagal, Mohon coba kembali';
					    	$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
					    	$response['type_dialog']  = 'Error';
					    	$response['id_transaksi']  = 0;

					    	$response['nama_pelanggan']  = $send_iso['pdam_customer_name'];
							$response['pdam_no_ref_biller']  = $send_iso['pdam_no_ref_biller'];
					    	//$response['pdam_blth']  = $send_iso['pdam_blth'];
					    	$response['tagihan']  = 'Rp '.number_format(str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0)),0,',','.');
					    	$response['biaya_admin']  = 'Rp '.number_format(ltrim($send_iso['pdam_biaya_admin'],0),0,',','.');

							$response['val_tagihan']  = str_replace('-', '', ltrim($send_iso['pdam_rupiah_tagihan'],0));
					    	$response['val_biaya_admin']  = ltrim($send_iso['pdam_biaya_admin'],0);
					    	$response['pdam_blth'] = $send_iso['pdam_blth'];
							$response['jml_lembar_tagihan'] = $send_iso['jml_lembar_tagihan'];
							$response['pdam_waktu_lunas'] = $send_iso['pdam_waktu_lunas'];
							$response['pdam_idpel'] = $send_iso['pdam_idpel'];

							$response['val_total_bayar'] = $response['val_tagihan']+$response['val_biaya_admin'];
							$val_total_pembayaran = $response['val_total_bayar']-$sv['komisi']-$sv['voucher_amount'];
							$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

							$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
							$response['total_pembayaran'] = 'Rp '.number_format($response['val_total_pembayaran'],0,',','.');
							$response['tgl_lunas'] = $send_iso['pdam_waktu_lunas'];


							$up_struk['struk_tgl_lunas'] = $response['tgl_lunas'];
							$up_struk['struk_norek'] = $response['pdam_idpel'];
							$up_struk['struk_blth'] = $response['pdam_blth'];
							$up_struk['struk_nama_pelanggan'] = $response['nama_pelanggan'];
							$up_struk['struk_jpa_ref'] = $response['pdam_no_ref_biller'];
							$up_struk['struk_rp_tag'] = str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0));
							$up_struk['struk_biaya_admin'] = $response['val_biaya_admin'];
							$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
							$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran'];
							$up_struk['jpa_ref'] = $send_iso['pdam_no_ref_switching'];

							$update = DB::table('trans_pdam')
								->where('id',$save)
								->update($up_struk);

					    	return response()->json($response);
					    	exit();
					    }
					} else {
					
						if($send_iso['39'] == 'Pending' || $send_iso['39'] == '06' || $send_iso['39'] == '09' || $send_iso['39'] == '18' || $send_iso['39'] == '22') {
							$up3['status'] = 'Error';
							$up3['stan'] = $send_iso['11'];
							$up3['jpa_ref'] = $send_iso['pdam_no_ref_biller'];

							$update = DB::table('trans_pdam')
								->where('id',$save)
								->update($up3);

							$response['api_status']  = 3;
					    	//$response['api_message'] = 'Transaksi sedang di proses';
					    	$response['api_message'] = 'Transaksi gagal, Mohon coba kembali';
					    	$response['type_dialog']  = 'Informasi';
					    	$response['id_transaksi']  = 0;

					    	$response['nama_pelanggan']  = $send_iso['pdam_customer_name'];
							$response['pdam_no_ref_biller']  = $send_iso['pdam_no_ref_biller'];
					    	//$response['pdam_blth']  = $send_iso['pdam_blth'];
					    	$response['tagihan']  = 'Rp '.number_format(str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0)),0,',','.');
					    	$response['biaya_admin']  = 'Rp '.number_format(ltrim($send_iso['pdam_biaya_admin'],0),0,',','.');

							$response['val_tagihan']  = str_replace('-', '', ltrim($send_iso['pdam_rupiah_tagihan'],0));
					    	$response['val_biaya_admin']  = ltrim($send_iso['pdam_biaya_admin'],0);
					    	$response['pdam_blth'] = $send_iso['pdam_blth'];
							$response['jml_lembar_tagihan'] = $send_iso['jml_lembar_tagihan'];
							$response['pdam_waktu_lunas'] = $send_iso['pdam_waktu_lunas'];
							$response['pdam_idpel'] = $send_iso['pdam_idpel'];

							$response['val_total_bayar'] = $response['val_tagihan']+$response['val_biaya_admin'];
							$val_total_pembayaran = $response['val_total_bayar']-$sv['komisi']-$sv['voucher_amount'];
							$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

							$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
							$response['total_pembayaran'] = 'Rp '.number_format($response['val_total_pembayaran'],0,',','.');
							$response['tgl_lunas'] = $send_iso['pdam_waktu_lunas'];


							$up_struk['struk_tgl_lunas'] = $response['tgl_lunas'];
							$up_struk['struk_norek'] = $response['pdam_idpel'];
							$up_struk['struk_blth'] = $response['pdam_blth'];
							$up_struk['struk_nama_pelanggan'] = $response['nama_pelanggan'];
							$up_struk['struk_jpa_ref'] = $response['pdam_no_ref_biller'];
							$up_struk['struk_rp_tag'] = str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0));
							$up_struk['struk_biaya_admin'] = $response['val_biaya_admin'];
							$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
							$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran'];
							$up_struk['jpa_ref'] = $send_iso['pdam_no_ref_switching'];

							$update = DB::table('trans_pdam')
								->where('id',$save)
								->update($up_struk);

							return response()->json($response);
							exit();
						}
					}

					$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_pdam');
					if($send_sms_transaksi == 'Yes') {
						$msg = CRUDBooster::getsetting('sukses_transaksi_pdam');
						$msg = str_replace('[no_hp]', $no_hp, $msg);
						$msg = str_replace('[nama]', $detail_agen->nama, $msg);
						$msg = str_replace('[komisi]', $sv['komisi'], $msg);
						Esta::send_sms($no_hp, $msg);
					}

					if(!empty($id_voucher)) {
						$uv['used'] = 'Yes';

						$up_voucher = DB::table('trans_voucher_child')
							->where('id',$id_voucher_child)
							->where('id_agen',$id_agen)
							->update($uv);
					}

					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi PDAM berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['id_transaksi']  = $send_iso['id_log'];
			    	$response['nama_pelanggan']  = $send_iso['pdam_customer_name'];
					$response['pdam_no_ref_biller']  = $send_iso['pdam_no_ref_biller'];
			    	//$response['pdam_blth']  = $send_iso['pdam_blth'];
			    	$response['tagihan']  = 'Rp '.number_format(str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0)),0,',','.');
			    	$response['biaya_admin']  = 'Rp '.number_format(ltrim($send_iso['pdam_biaya_admin'],0),0,',','.');

					$response['val_tagihan']  = str_replace('-', '', ltrim($send_iso['pdam_rupiah_tagihan'],0));
			    	$response['val_biaya_admin']  = ltrim($send_iso['pdam_biaya_admin'],0);
			    	$response['pdam_blth'] = $send_iso['pdam_blth'];
					$response['jml_lembar_tagihan'] = $send_iso['jml_lembar_tagihan'];
					$response['pdam_waktu_lunas'] = $send_iso['pdam_waktu_lunas'];
					$response['pdam_idpel'] = $send_iso['pdam_idpel'];

					$response['val_total_bayar'] = $response['val_tagihan']+$response['val_biaya_admin'];
					$val_total_pembayaran = $response['val_total_bayar']-$sv['komisi']-$sv['voucher_amount'];
					$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

					$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
					$response['total_pembayaran'] = 'Rp '.number_format($response['val_total_pembayaran'],0,',','.');
					$response['tgl_lunas'] = $send_iso['pdam_waktu_lunas'];


					$up_struk['struk_tgl_lunas'] = $response['tgl_lunas'];
					$up_struk['struk_norek'] = $response['pdam_idpel'];
					$up_struk['struk_blth'] = $response['pdam_blth'];
					$up_struk['struk_nama_pelanggan'] = $response['nama_pelanggan'];
					$up_struk['struk_jpa_ref'] = $response['pdam_no_ref_biller'];
					$up_struk['struk_rp_tag'] = str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0));
					$up_struk['struk_biaya_admin'] = $response['val_biaya_admin'];
					$up_struk['struk_total_bayar'] = $response['val_total_bayar'];
					$up_struk['struk_total_pembayaran'] = $response['val_total_pembayaran'];
					$up_struk['jpa_ref'] = $send_iso['pdam_no_ref_switching'];
					$up_struk['status'] = 'Clear';

					$update = DB::table('trans_pdam')
						->where('id',$save)
						->update($up_struk);

					/*email*/
					$view     = view('struk/struk_pdam',$response)->render();
					$filename = "Struk-PDAM-".$no_hp;
					$pdf      = App::make('dompdf.wrapper');

					$path = storage_path('app/uploads/'.$filename.'.pdf');

					$pdf->loadHTML($view);
					$pdf->setPaper('A4','landscape');
					$output = $pdf->output();

					file_put_contents($path, $output);

					$attachments = [$path];
					$email = $detail_agen->email;
					if(!empty($email) || $detail_agen->notif_email != 'No') {
					    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pdam','attachments'=>$attachments]);
					}
				} else {
					$response['api_status']  = 0;
			    	$response['api_message'] = 'Transaksi PDAM gagal';
			    	$response['type_dialog']  = 'Error';
			    	$response['id_transaksi']  = 0;
			    	$response['pdam_no_ref_biller']  = $send_iso['pdam_no_ref_biller'];
				}
			/*} else {
				$response['api_status']  = 3;
		    	$response['api_message'] = 'Saldo anda tidak mencukupi';
		    	$response['type_dialog']  = 'Error';
		    	$response['id_transaksi']  = 0;
			}*/

		return response()->json($response);
	}

	public function postCheckTagihanPascabayarPdam() {
		$no_hp = Request::get('no_hp');
		$id_agen = Request::get('id_agen');
		$id_area = Request::get('id_area');
		$no_meter = Request::get('no_meter');

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$detail_product = DB::table('pan_product')
			->where('id',$id_area)
			->first();

		/*send iso*/
		$send_iso = Esta::send_iso_pdam($id_area,$no_hp,'0200','Inquiry',0, $no_meter);

		if($send_iso['39'] == '00') {
			$rest['jml_tagihan'] = 'Rp '.number_format(ltrim($send_iso['pdam_rupiah_tagihan'],0),0,',','.');
			$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
			$rest['nama_pelanggan'] = $send_iso['pdam_customer_name'];
			$rest['id_pelanggan'] = $send_iso['pdam_idpel'];
			$rest['id_transaksi'] = $send_iso['id_log'];

			$rest['val_jml_tagihan'] = ltrim($send_iso['pdam_rupiah_tagihan'],0);
			$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$rest['val_biaya_admin'] = ($send_iso['pdam_biaya_admin'] >= 1 ? ltrim($send_iso['pdam_biaya_admin'],0) : 0);
			$rest['biaya_admin'] = 'Rp '.number_format($rest['val_biaya_admin'],0,',','.');
			$rest['pdam_blth'] = $send_iso['pdam_blth'];
			$rest['jml_lembar_tagihan'] = ltrim($send_iso['jml_lembar_tagihan'],0).' BLN';
			$rest['val_total_bayar'] = $rest['val_jml_tagihan']+$rest['val_biaya_admin'];
			$val_total_pembayaran = $rest['val_total_bayar']-$rest['val_komisi'];
			$rest['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

			$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
			$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');

			$response['api_status']  = 1;
	    	$response['api_message'] = 'Berhasil';
	    	$response['type_dialog']  = 'Informasi';
	    	$response['item']  = $rest;
		} else {
			$rest['jml_tagihan'] = 0;
			$rest['komisi'] = 0;

			$response['api_status']  = 0;
	    	$response['api_message'] = Esta::show_error('PDAM',$send_iso['39'],'rino');
	    	$response['type_dialog']  = 'Error';
	    	$response['item']  = $rest;
		}
		return response()->json($response);
	}

	public function postPdamArea() {
		$id_agen = Request::get('id_agen');
		$id_kode_biller = 1003;//Request::get('id_kode_biller');
		$id_kode_layanan = 2;//Request::get('id_kode_layanan'); /*1 prabayar, 2 pasca bayar*/

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$arrays = DB::table('pan_product')
			->where('product_name','LIKE','%PDAM%')
			->where('id_kode_layanan',$id_kode_layanan)
			->whereNull('deleted_at')
			->orderBy('product_name','ASC')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->product_name;
			//$rest['amount'] = $array->amount;
			$rest['komisi'] = ($detail_agen->status_agen == 'Basic' ? $array->komisi_basic : $array->komisi_premium);
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog'] = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postPlnPascabayarSubmit() {
		$no_hp = Request::get('no_hp');
		$no_meter = Request::get('no_meter');
		//$id_voucher = Request::get('id_voucher');
		$pin = Request::get('pin');
		$amount = Request::get('amount');
		$id_agen = Request::get('id_agen');
		$id_transaksi = Request::get('id_transaksi');

		$tipe_layanan = DB::table('pan_tipe_layanan')->where('kode','00')->first()->id;
		$kode_layanan = DB::table('pan_kode_layanan')->where('kode','4')->first()->id;
		$id_product = DB::table('pan_product')->where('id_tipe_layanan',$tipe_layanan)->where('id_kode_layanan',$kode_layanan)->first()->id;

		$id_voucher_child = Request::get('id_voucher');
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}

			$detail_product = DB::table('pan_product')
				->where('id',$id_product)
				->first();

			$detail_voucher = DB::table('voucher')
				->where('id',$id_voucher)
				->first();

			$kode = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));

			$trans_amount = Esta::amount_product_pln_pascabayar($id_product,$id_agen,$id_voucher,$amount);
			
			/*echo $trans_amount.'-'.($trans_amount <= 0 ? 0 : ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium));

			Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN','Transaksi PLN '.$detail_product->product_name,'Out','Transaksi');
			Esta::log_money($id_agen,($trans_amount <= 0 ? 0 : ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium)),date('Y-m-d H:i:s'),'Komisi Transaksi PLN','Komisi Transaksi PLN '.$detail_product->product_name,'In','Komisi');
			exit();*/
			//if($detail_agen->saldo >= $amount) {
				
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['updated_at'] = date('Y-m-d H:i:s');
				$sv['created_user'] = Esta::user($id_agen);
				$sv['updated_user'] = Esta::user($id_agen);
				$sv['trans_no'] = $kode;
				$sv['ref_trans_no'] = $kode;
				$sv['trans_date'] = date('Y-m-d H:i:s');
				$sv['trans_desc'] = 'PLN PASCABAYAR';
				$sv['currency'] = 'IDR';
				$sv['trans_amount'] = $trans_amount;
				$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
				//$sv['create_user'] = $detail_agen->nama;
				$sv['id_agen'] = $id_agen;
				$sv['status'] = 'Pending';
				$sv['rekon_amount'] = 0;
				$sv['status_match'] = 'Waiting Rekon';
				$sv['no_hp'] = $no_hp;
				$sv['no_meter'] = $no_meter;
				$sv['id_product'] = $id_product;
				$sv['agen_nama'] = $detail_agen->nama;
				$sv['batch_last_user'] = $detail_agen->nama;
				$sv['agen_email'] = $detail_agen->email;
				$sv['agen_kode'] = $detail_agen->kode;
				$sv['agen_no_hp'] = $detail_agen->no_hp;
				$sv['agen_level'] = $detail_agen->status_agen;
				$sv['agen_referall'] = $detail_agen->kode_referall_agen;
				$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
				$sv['agen_status_aktif'] = $detail_agen->status_aktif;
				$sv['agen_nik'] = $detail_agen->nik;
				$sv['agen_tgl_register'] = $detail_agen->tgl_register;
				$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
				$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
				$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
				$sv['agen_agama'] = $detail_agen->agama;
				$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
				$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
				$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
				$sv['agen_prov'] = $detail_agen->prov;
				$sv['agen_kab'] = $detail_agen->kab;
				$sv['agen_kec'] = $detail_agen->kec;
				$sv['agen_kel'] = $detail_agen->kel;
				$sv['product_nama'] = $detail_product->product_name;
				$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
				$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
				$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
				$sv['product_komisi_basic'] = $detail_product->komisi_basic;
				$sv['product_komisi_premium'] = $detail_product->komisi_premium;
				$sv['product_amount'] = $trans_amount;
				$sv['product_margin'] = $detail_product->margin;
				$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
				$sv['product_biaya_admin_1'] = $detail_product->admin_1;
				$sv['product_biaya_admin_2'] = $detail_product->admin_2;
				$sv['product_biaya_admin_3'] = $detail_product->admin_3;
				$sv['product_potongan'] = $detail_product->potongan;
				$sv['voucher_nama'] = $detail_voucher->nama;
				$sv['voucher_expired_date'] = $detail_voucher->expired_date;
				$sv['voucher_amount'] = $detail_voucher->amount;
				$sv['voucher_tagline'] = $detail_voucher->tagline;
				$sv['id_voucher'] = $detail_voucher->id;
				$sv['voucher_description'] = $detail_voucher->description;


				$save = DB::table('trans_pln')
					->insertGetId($sv);
				/*potong saldo*/
				Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN','Transaksi PLN '.$detail_product->product_name,'Out','Transaksi','trans_pln',$save);
				Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi PLN','Komisi Transaksi PLN '.$detail_product->product_name,'In','Komisi','trans_pln',$save);

				/*send iso*/
				$send_iso = Esta::send_iso_pln_pascabayar($id_product,$no_meter,'0200',$id_transaksi,'Purchase');

				/*print_r($send_iso);
				exit();*/
				if($send_iso['39'] != '97') {
					if($send_iso['status'] == 'Reversal' || $send_iso['status'] == 'Reversal Repeat' || $send_iso['status'] == 'Reversal Repeat 2') { /*jika respon error*/
						/*kembalikan saldo*/
						if($send_iso['39'] == '00' || $send_iso['39'] == '94') {
							Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN','Return Transaksi PLN '.$detail_product->product_name,'In','Transaksi','trans_pln',$save);
							Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi PLN','Return Komisi Transaksi PLN '.$detail_product->product_name,'Out','Komisi','trans_pln',$save);
							$up['return_saldo'] = 'Yes';
						}

						$up['status'] = 'Error';
						$up['error_code'] = $send_iso['39'];
						$up['jpa_ref'] = $send_iso['pln_psc_no_ref'];

						$update = DB::table('trans_pln')
							->where('id',$save)
							->update($up);

						$header_msg = $send_iso['header_msg'];

						$response['api_status']  = ($header_msg == 'Yes' ? '2' : '4');
				    	$response['api_message'] = ($header_msg == 'Yes' ?Esta::show_error('PLN Postpaid',$send_iso['39'],$no_meter) : ($send_iso['39'] == '96' ? 'Transaksi sedang diproses, Mohon hubungi customer service' : 'Transaksi Gagal'));
				    	$response['id_transaksi']  = 0;
				    	$response['type_dialog']  = 'Informasi';

				    	$response['pln_psc_jml_tagihan'] = $send_iso['pln_psc_jml_tagihan'].' Bulan';
						$response['pln_psc_tarif'] = str_replace(' ', '', $send_iso['pln_psc_tarif']);
						$response['pln_psc_daya'] = $send_iso['pln_psc_daya'];
						$response['pln_psc_no_ref'] = $send_iso['pln_psc_no_ref'];
						$response['pln_psc_jml_tunggakan'] = ($send_iso['pln_psc_jml_tunggakan'] >= 1 ? ltrim($send_iso['pln_psc_jml_tunggakan'],0) : 0);
						$response['stand_meter'] = str_replace(',', '', $send_iso['pln_psc_lwbp_sebelum'].' - '.$send_iso['pln_psc_lwbp_sesudah']);

						$response['pln_psc_blth'] = $send_iso['pln_psc_blth'];
						$response['info'] = str_replace('###', '"', $send_iso['62']);
						$tgl_lunas = $send_iso['pln_psc_tgl_lunas'].$send_iso['pln_psc_jam_lunas'];
						$response['pln_psc_tgl_lunas'] = substr($tgl_lunas,6,2).'/'.substr($tgl_lunas,4,2).'/'.substr($tgl_lunas,2,2).' '.substr($tgl_lunas,8,2).':'.substr($tgl_lunas,10,1).substr($tgl_lunas,11,1);
						$response['pln_psc_jam_lunas'] = $send_iso['pln_psc_jam_lunas'];
						$response['nominal_voucher'] = 'Rp '.number_format($detail_voucher->amount,0,',','.');

						$response['val_rp_transaksi'] = $send_iso['rp_transaksi'];
						$response['val_pln_psc_biaya_tagihan_pln'] = $send_iso['pln_psc_biaya_tagihan_pln'];
						$response['val_pln_psc_biaya_admin'] = $sv['product_biaya_admin_edn']+$sv['product_biaya_admin_1']+$sv['product_biaya_admin_2']+$sv['product_biaya_admin_3']+$sv['product_margin']-$sv['product_potongan'];
						$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
						$response['val_total_bayar'] = $response['val_rp_transaksi']+$response['val_pln_psc_biaya_admin'];
						//$response['val_total_pembayaran'] = $response['val_total_bayar']-$response['val_komisi'];

						$val_total_pembayaran = $send_iso['rp_transaksi']+$response['val_pln_psc_biaya_admin']-$response['val_komisi']-$detail_voucher->amount;
						$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

						$response['pln_psc_id_pel'] = $send_iso['pln_psc_id_pel'];
						$response['pln_psc_nama'] = $send_iso['pln_psc_nama'];
						$response['rp_transaksi'] = 'Rp '.number_format($send_iso['rp_transaksi'],0,',','.');
						$response['pln_psc_biaya_tagihan_pln'] = 'Rp '.number_format($send_iso['pln_psc_biaya_tagihan_pln'],0,',','.');
						$response['pln_psc_biaya_admin'] = 'Rp '.number_format($response['val_pln_psc_biaya_admin'],0,',','.');
						//$total_pembayaran = 'Rp '.number_format($response['val_total_bayar']-$response['val_komisi']-$detail_voucher->amount,0,',','.');
						$response['total_pembayaran'] = 'Rp '.number_format(($response['val_total_pembayaran'] <= 0 ? 0 : $response['val_total_pembayaran']),0,',','.');
						$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
						$response['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');

						$up2['jpa_ref'] = $send_iso['pln_psc_no_ref'];
						$up2['struk_info'] = $response['info'];
						$up2['struk_jml_tunggakan'] = $response['pln_psc_jml_tunggakan'];
						

						$up2['struk_nama_pelanggan'] = $response['pln_psc_nama'];
						$up2['struk_tarif'] = $response['pln_psc_tarif'];
						$up2['struk_daya'] = $response['pln_psc_daya'];
						$up2['struk_admin_bank'] = $response['val_pln_psc_biaya_admin'];
						$up2['struk_total_pembayaran'] = $response['val_total_pembayaran'];
						$up2['struk_tgl_lunas'] = $response['pln_psc_tgl_lunas'];
						$up2['struk_id_pelanggan'] = $response['pln_psc_id_pel'];
						$up2['struk_blth'] = $response['pln_psc_blth'];
						$up2['struk_stand_meter'] = $response['stand_meter'];
						$up2['struk_rp_tag_pln'] = $response['val_rp_transaksi'];
						$up2['struk_total_bayar'] = $response['val_total_bayar'];
						$up2['struk_total_lbr_tagihan'] = $send_iso['pln_psc_jml_tagihan'];

						$update2 = DB::table('trans_pln')
							->where('id',$save)
							->update($up2);

				    	return response()->json($response);
				    	exit();
					}
				}

				if($send_iso['39'] != '97') {
					if($send_iso['39'] != '00' || $send_iso['status'] == 'Reversal' || $send_iso['status'] == 'Reversal Repeat' || $send_iso['status'] == 'Reversal Repeat 2') { /*jika respon error*/
						/*kembalikan saldo*/
						if($send_iso['39'] == '00' || $send_iso['39'] == '94') {
							Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN','Return Transaksi PLN '.$detail_product->product_name,'In','Transaksi','trans_pln',$save);
							Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi PLN','Return Komisi Transaksi PLN '.$detail_product->product_name,'Out','Komisi','trans_pln',$save);
							$up['return_saldo'] = 'Yes';
						}

						$up['status'] = 'Error';
						$up['error_code'] = $send_iso['39'];
						$up['jpa_ref'] = $send_iso['pln_psc_no_ref'];

						$update = DB::table('trans_pln')
							->where('id',$save)
							->update($up);

						$header_msg = $send_iso['header_msg'];

						$response['api_status']  = ($header_msg == 'Yes' ? '2' : '4');
				    	$response['api_message'] = ($header_msg == 'Yes' ?Esta::show_error('PLN Postpaid',$send_iso['39'],$no_meter) : ($send_iso['39'] == '96' ? 'Transaksi sedang diproses, Mohon hubungi customer service' : 'Transaksi Gagal'));
				    	$response['id_transaksi']  = 0;
				    	$response['type_dialog']  = 'Informasi';

				    	$response['pln_psc_jml_tagihan'] = $send_iso['pln_psc_jml_tagihan'].' Bulan';
						$response['pln_psc_tarif'] = str_replace(' ', '', $send_iso['pln_psc_tarif']);
						$response['pln_psc_daya'] = $send_iso['pln_psc_daya'];
						$response['pln_psc_no_ref'] = $send_iso['pln_psc_no_ref'];
						$response['pln_psc_jml_tunggakan'] = ($send_iso['pln_psc_jml_tunggakan'] >= 1 ? ltrim($send_iso['pln_psc_jml_tunggakan'],0) : 0);
						$response['stand_meter'] = str_replace(',', '', $send_iso['pln_psc_lwbp_sebelum'].' - '.$send_iso['pln_psc_lwbp_sesudah']);

						$response['pln_psc_blth'] = $send_iso['pln_psc_blth'];
						$response['info'] = str_replace('###', '"', $send_iso['62']);
						$tgl_lunas = $send_iso['pln_psc_tgl_lunas'].$send_iso['pln_psc_jam_lunas'];
						$response['pln_psc_tgl_lunas'] = substr($tgl_lunas,6,2).'/'.substr($tgl_lunas,4,2).'/'.substr($tgl_lunas,2,2).' '.substr($tgl_lunas,8,2).':'.substr($tgl_lunas,10,1).substr($tgl_lunas,11,1);
						$response['pln_psc_jam_lunas'] = $send_iso['pln_psc_jam_lunas'];
						$response['nominal_voucher'] = 'Rp '.number_format($detail_voucher->amount,0,',','.');

						$response['val_rp_transaksi'] = $send_iso['rp_transaksi'];
						$response['val_pln_psc_biaya_tagihan_pln'] = $send_iso['pln_psc_biaya_tagihan_pln'];
						$response['val_pln_psc_biaya_admin'] = $sv['product_biaya_admin_edn']+$sv['product_biaya_admin_1']+$sv['product_biaya_admin_2']+$sv['product_biaya_admin_3']+$sv['product_margin']-$sv['product_potongan'];
						$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
						$response['val_total_bayar'] = $response['val_rp_transaksi']+$response['val_pln_psc_biaya_admin'];
						//$response['val_total_pembayaran'] = $response['val_total_bayar']-$response['val_komisi'];

						$val_total_pembayaran = $send_iso['rp_transaksi']+$response['val_pln_psc_biaya_admin']-$response['val_komisi']-$detail_voucher->amount;
						$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

						$response['pln_psc_id_pel'] = $send_iso['pln_psc_id_pel'];
						$response['pln_psc_nama'] = $send_iso['pln_psc_nama'];
						$response['rp_transaksi'] = 'Rp '.number_format($send_iso['rp_transaksi'],0,',','.');
						$response['pln_psc_biaya_tagihan_pln'] = 'Rp '.number_format($send_iso['pln_psc_biaya_tagihan_pln'],0,',','.');
						$response['pln_psc_biaya_admin'] = 'Rp '.number_format($response['val_pln_psc_biaya_admin'],0,',','.');
						//$total_pembayaran = 'Rp '.number_format($response['val_total_bayar']-$response['val_komisi']-$detail_voucher->amount,0,',','.');
						$response['total_pembayaran'] = 'Rp '.number_format(($response['val_total_pembayaran'] <= 0 ? 0 : $response['val_total_pembayaran']),0,',','.');
						$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
						$response['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');

						$up2['jpa_ref'] = $send_iso['pln_psc_no_ref'];
						$up2['struk_info'] = $response['info'];
						$up2['struk_jml_tunggakan'] = $response['pln_psc_jml_tunggakan'];
						

						$up2['struk_nama_pelanggan'] = $response['pln_psc_nama'];
						$up2['struk_tarif'] = $response['pln_psc_tarif'];
						$up2['struk_daya'] = $response['pln_psc_daya'];
						$up2['struk_admin_bank'] = $response['val_pln_psc_biaya_admin'];
						$up2['struk_total_pembayaran'] = $response['val_total_pembayaran'];
						$up2['struk_tgl_lunas'] = $response['pln_psc_tgl_lunas'];
						$up2['struk_id_pelanggan'] = $response['pln_psc_id_pel'];
						$up2['struk_blth'] = $response['pln_psc_blth'];
						$up2['struk_stand_meter'] = $response['stand_meter'];
						$up2['struk_rp_tag_pln'] = $response['val_rp_transaksi'];
						$up2['struk_total_bayar'] = $response['val_total_bayar'];
						$up2['struk_total_lbr_tagihan'] = $send_iso['pln_psc_jml_tagihan'];

						$update2 = DB::table('trans_pln')
							->where('id',$save)
							->update($up2);

				    	return response()->json($response);
				    	exit();
					}
				}

				$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_pln');
				if($send_sms_transaksi == 'Yes') {
					$msg = CRUDBooster::getsetting('sukses_transaksi_pln');
					$msg = str_replace('[no_hp]', $no_hp, $msg);
					$msg = str_replace('[nama]', $detail_agen->nama, $msg);
					$msg = str_replace('[komisi]', $sv['komisi'], $msg);
					Esta::send_sms($no_hp, $msg);
				}

				if(!empty($id_voucher)) {
					$uv['used'] = 'Yes';

					$up_voucher = DB::table('trans_voucher_child')
						->where('id',$id_voucher_child)
						->where('id_agen',$id_agen)
						->update($uv);
				}

				/*if($send_iso['39'] == '97') {
					$id_log = $send_iso['id_log'];
					$detail_log = DB::table('log_jatelindo_bit')
						->where('id',$id_log)
						->first();
					$detail_inq = DB::table('log_jatelindo_bit')
						->where('id',$detail_log->id_transaksi)
						->where('status','Inquiry')
						->where('jenis','res')
						->first();

					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi PLN berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['id_transaksi']  = $save;
			    	$response['pln_psc_jml_tagihan'] = $detail_inq->pln_psc_jml_tagihan.' Bulan';
					$response['pln_psc_tarif'] = str_replace(' ', '', $detail_inq->pln_psc_tarif);
					$response['pln_psc_daya'] = $detail_inq->pln_psc_daya;
					$response['pln_psc_no_ref'] = $detail_log->pln_psc_trx_id;
					$response['pln_psc_jml_tunggakan'] = ($detail_inq->pln_psc_jml_tunggakan >= 1 ? ltrim($detail_inq->pln_psc_jml_tunggakan,0) : 0);
					$response['stand_meter'] = str_replace(',', '', $detail_inq->pln_psc_lwbp_sebelum.' - '.$detail_inq->pln_psc_lwbp_sesudah);

					$response['pln_psc_blth'] = $detail_inq->pln_psc_blth;
					$response['info'] = str_replace('###', '"', $detail_inq->bit62);
					$tgl_lunas = '-';
					$response['pln_psc_tgl_lunas'] = date( "d/m/y H:i", strtotime($detail_log->created_at));
					$response['nominal_voucher'] = 'Rp '.number_format($detail_voucher->amount,0,',','.');

					$response['val_rp_transaksi'] = $detail_inq->rp_transaksi;
					$response['val_pln_psc_biaya_tagihan_pln'] = $detail_inq->pln_psc_biaya_tagihan_pln;
					$response['val_pln_psc_biaya_admin'] = $sv['product_biaya_admin_edn']+$sv['product_biaya_admin_1']+$sv['product_biaya_admin_2']+$sv['product_biaya_admin_3']+$sv['product_margin']-$sv['product_potongan'];
					$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					$val_total_pembayaran = $detail_inq->rp_transaksi+$response['val_pln_psc_biaya_admin']-$response['val_komisi']-$detail_voucher->amount;
					$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
					$response['val_total_bayar'] = $response['val_rp_transaksi']+$response['val_pln_psc_biaya_admin'];

					$response['pln_psc_id_pel'] = $detail_inq->pln_psc_id_pel;
					$response['pln_psc_nama'] = $detail_inq->pln_psc_nama;
					$response['rp_transaksi'] = 'Rp '.number_format($detail_inq->rp_transaksi,0,',','.');
					$response['pln_psc_biaya_tagihan_pln'] = 'Rp '.number_format($detail_inq->pln_psc_biaya_tagihan_pln,0,',','.');
					$response['pln_psc_biaya_admin'] = 'Rp '.number_format($sv['product_biaya_admin_edn'],0,',','.');
					$response['total_pembayaran'] = 'Rp '.number_format($response['val_total_pembayaran'],0,',','.');
					$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
					$response['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');

					$up2['jpa_ref'] = $detail_log->pln_psc_trx_id;
					$up2['struk_info'] = $response['info'];
					$up2['struk_jml_tunggakan'] = $response['pln_psc_jml_tunggakan'];

				} else {*/
					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi PLN berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['id_transaksi']  = $save;
			    	$response['pln_psc_jml_tagihan'] = $send_iso['pln_psc_jml_tagihan'].' Bulan';
					$response['pln_psc_tarif'] = str_replace(' ', '', $send_iso['pln_psc_tarif']);
					$response['pln_psc_daya'] = $send_iso['pln_psc_daya'];
					$response['pln_psc_no_ref'] = $send_iso['pln_psc_no_ref'];
					$response['pln_psc_jml_tunggakan'] = ($send_iso['pln_psc_jml_tunggakan'] >= 1 ? ltrim($send_iso['pln_psc_jml_tunggakan'],0) : 0);
					$response['stand_meter'] = str_replace(',', '', $send_iso['pln_psc_lwbp_sebelum'].' - '.$send_iso['pln_psc_lwbp_sesudah']);

					$response['pln_psc_blth'] = $send_iso['pln_psc_blth'];
					$response['info'] = str_replace('###', '"', $send_iso['62']);
					$tgl_lunas = $send_iso['pln_psc_tgl_lunas'].$send_iso['pln_psc_jam_lunas'];
					$response['pln_psc_tgl_lunas'] = substr($tgl_lunas,6,2).'/'.substr($tgl_lunas,4,2).'/'.substr($tgl_lunas,2,2).' '.substr($tgl_lunas,8,2).':'.substr($tgl_lunas,10,1).substr($tgl_lunas,11,1);
					$response['pln_psc_jam_lunas'] = $send_iso['pln_psc_jam_lunas'];
					$response['nominal_voucher'] = 'Rp '.number_format($detail_voucher->amount,0,',','.');

					$response['val_rp_transaksi'] = $send_iso['rp_transaksi'];
					$response['val_pln_psc_biaya_tagihan_pln'] = $send_iso['pln_psc_biaya_tagihan_pln'];
					$response['val_pln_psc_biaya_admin'] = $sv['product_biaya_admin_edn']+$sv['product_biaya_admin_1']+$sv['product_biaya_admin_2']+$sv['product_biaya_admin_3']+$sv['product_margin']-$sv['product_potongan'];
					$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					$response['val_total_bayar'] = $response['val_rp_transaksi']+$response['val_pln_psc_biaya_admin'];
					//$response['val_total_pembayaran'] = $response['val_total_bayar']-$response['val_komisi'];

					$val_total_pembayaran = $send_iso['rp_transaksi']+$response['val_pln_psc_biaya_admin']-$response['val_komisi']-$detail_voucher->amount;
					$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

					$response['pln_psc_id_pel'] = $send_iso['pln_psc_id_pel'];
					$response['pln_psc_nama'] = $send_iso['pln_psc_nama'];
					$response['rp_transaksi'] = 'Rp '.number_format($send_iso['rp_transaksi'],0,',','.');
					$response['pln_psc_biaya_tagihan_pln'] = 'Rp '.number_format($send_iso['pln_psc_biaya_tagihan_pln'],0,',','.');
					$response['pln_psc_biaya_admin'] = 'Rp '.number_format($response['val_pln_psc_biaya_admin'],0,',','.');
					//$total_pembayaran = 'Rp '.number_format($response['val_total_bayar']-$response['val_komisi']-$detail_voucher->amount,0,',','.');
					$response['total_pembayaran'] = 'Rp '.number_format(($response['val_total_pembayaran'] <= 0 ? 0 : $response['val_total_pembayaran']),0,',','.');
					$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
					$response['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');

					$up2['jpa_ref'] = $send_iso['pln_psc_no_ref'];
					$up2['struk_info'] = $response['info'];
					$up2['status'] = 'Clear';
					$up2['struk_jml_tunggakan'] = $response['pln_psc_jml_tunggakan'];
				//}

				$up2['struk_nama_pelanggan'] = $response['pln_psc_nama'];
				$up2['struk_tarif'] = $response['pln_psc_tarif'];
				$up2['struk_daya'] = $response['pln_psc_daya'];
				$up2['struk_admin_bank'] = $response['val_pln_psc_biaya_admin'];
				$up2['struk_total_pembayaran'] = $response['val_total_pembayaran'];
				$up2['struk_tgl_lunas'] = $response['pln_psc_tgl_lunas'];
				$up2['struk_id_pelanggan'] = $response['pln_psc_id_pel'];
				$up2['struk_blth'] = $response['pln_psc_blth'];
				$up2['struk_stand_meter'] = $response['stand_meter'];
				$up2['struk_rp_tag_pln'] = $response['val_rp_transaksi'];
				$up2['struk_total_bayar'] = $response['val_total_bayar'];
				$up2['struk_total_lbr_tagihan'] = $send_iso['pln_psc_jml_tagihan'];

				$update2 = DB::table('trans_pln')
					->where('id',$save)
					->update($up2);

				/*email*/

				$view     = view('struk/struk_pln_postpaid',$response)->render();
				$filename = "Struk-PLN-Postpaid-".$response['pln_psc_id_pel'];
				$pdf      = App::make('dompdf.wrapper');

				$path = storage_path('app/uploads/'.$filename.'.pdf');

				$pdf->loadHTML($view);
				$pdf->setPaper('A4','landscape');
				$output = $pdf->output();

				file_put_contents($path, $output);

				$attachments = [$path];
				$email = $detail_agen->email;
				if(!empty($email) || $detail_agen->notif_email != 'No') {
				    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pln_postpaid','attachments'=>$attachments]);
				}

				/*email*/
			/*} else {
				$response['api_status']  = 3;
		    	$response['api_message'] = 'Saldo anda tidak mencukupi';
		    	$response['id_transaksi']  = 0;
		    	$response['type_dialog']  = 'Error';
			}*/

		return response()->json($response);
	}

	public function postCheckTagihanPascabayarPln() {
		$no_hp = Request::get('no_hp');
		$id_agen = Request::get('id_agen');
		$no_meter = Request::get('no_meter');

		$tipe_layanan = DB::table('pan_tipe_layanan')->where('kode','00')->first()->id;
		$kode_layanan = DB::table('pan_kode_layanan')->where('kode','4')->first()->id;
		$id_product = DB::table('pan_product')->where('id_tipe_layanan',$tipe_layanan)->where('id_kode_layanan',$kode_layanan)->first()->id;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		/*send iso*/
		$send_iso = Esta::send_iso_pln_pascabayar('0',$no_meter,'0200',0,'Inquiry');

		if($send_iso['39'] == '00') {
			$rest['val_rp_transaksi'] = $send_iso['rp_transaksi'];
			$rest['val_pln_psc_biaya_tagihan_pln'] = $send_iso['pln_psc_biaya_tagihan_pln'];
			$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$rest['val_pln_psc_biaya_admin'] = $detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan;
			$rest['val_rp_bayar'] = $rest['val_rp_transaksi']+$rest['val_pln_psc_biaya_admin'];
			$rest['val_total_pembayaran'] = $rest['val_rp_bayar']-$rest['val_komisi'];

			$rest['id_transaksi'] = $send_iso['id_log'];
			$rest['pln_psc_id_pel'] = $send_iso['pln_psc_id_pel'];
			$rest['pln_psc_nama'] = $send_iso['pln_psc_nama'];

			/*$b = substr($send_iso['pln_psc_blth'],4,2);
			$date=date_create("2013-".$b."-15");

			$rest['pln_psc_blth'] = strtoupper(date_format($date,"M")).substr($send_iso['pln_psc_blth'],2,2);*/
			$rest['pln_psc_blth'] = substr($send_iso['pln_psc_blth'],0,-1);
			$rest['pln_psc_jml_tagihan'] = $send_iso['pln_psc_jml_tagihan'];
			$rest['rp_transaksi'] = 'Rp '.number_format($send_iso['rp_transaksi'],0,',','.');
			$rest['pln_psc_biaya_tagihan_pln'] = 'Rp '.number_format($send_iso['pln_psc_biaya_tagihan_pln'],0,',','.');
			$rest['pln_psc_biaya_admin'] = 'Rp '.number_format($rest['val_pln_psc_biaya_admin'],0,',','.');
			$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
			$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
			$rest['rp_bayar'] = 'Rp '.number_format($rest['val_rp_bayar'],0,',','.');


			$response['api_status']  = 1;
	    	$response['api_message'] = 'Berhasil';
	    	$response['type_dialog']  = 'Informasi';
	    	$response['item']  = $rest;
		} else {
			$rest['jml_tagihan'] = 0;
			$rest['komisi'] = 0;

			$response['api_status']  = 0;
	    	$response['api_message'] = Esta::show_error('PLN Postpaid',$send_iso['39'],$no_meter);
	    	$response['type_dialog']  = 'Error';
	    	$response['item']  = $rest;
		}
		return response()->json($response);
	}

	public function getPecah() {
		/*$token_unsold = Esta::view_bit62('5151106123            060000');
		print_r($token_unsold);
		exit();*/
		//$string = '2018-08-01;TID;NAMA_PP;NOTELP  ;JTL66719FCBF536601C24AFBXXXX;5800;0;XL Voucher 5K;XL;457144';
		/*$string = '|20200710|114755|ECBSM001|6011|000000000708175|3451140ZB43917378724000000000000|521072113208 |201807201807|000000708175|000000000|201000145100000|000001';*/
		/*$string = 'aaaa|bbbb|cccc
		dddd|eeee|ffff
		gggg|hhhh|iiii
		jjjj|kkkk|llll';*/

		/*$string = '|20180712|050117|ECBSM001|6017|000000000100000|0451170ZAEAC59AD16559AFDA0876258|110100086661|56120241270|000000100000|000000000|201000145100000|21597171527088907074';*/
		/*echo strlen($string);
		echo substr($string, 1,8).'<br>';
		echo substr($string, 10,6).'<br>';
		echo substr($string, 17,8).'<br>';
		echo substr($string, 26,4).'<br>';
		echo substr($string, 31,15).'<br>';
		echo substr($string, 47,32).'<br>';
		echo substr($string, 80,12).'<br><br>';
		echo substr($string, 94,12).'<br>';
		echo substr($string, 107,12).'<br>';
		echo substr($string, 120,9).'<br>';
		echo substr($string, 130,15).'<br>';
		echo substr($string, 146,6).'<br>';*/
		//echo strlen($string);
		/*$raw = (explode(";",$string));

		$sv["tgl_transaksi"]              = $raw[0];
		$sv["terminal_id"]              = $raw[1];
		$sv["nama_pp"]            = $raw[2];
		$sv["notelp"]               = $raw[3];
		$sv["refnum_jpa"] = $raw[4];
		$sv["tran_amount"]                = $raw[5];
		$sv["admin_bank"]           = $raw[6];
		$sv["info"]            = $raw[7];
		$sv["operator"]                  = $raw[8];
		$sv["stan"]             = $raw[9];

		print_r($sv);

		exit();*/

		/*$local_file = public_path('rekon_file/multibiller/final/LIST-TELCOPREPAID-20180909-000.txt');
		$string = file_get_contents($local_file);
		//$string = str_replace(array("\n","\t"), array('|','|'), $string);
		//echo $string;
		$strings = explode("\n", $string);

		foreach($strings as $string) {
			$length = strlen($string);
			if($length >= 20) {
				$raw = (explode(";",$string));

				$sv["tgl_transaksi"]              = $raw[0];
				$sv["terminal_id"]              = $raw[1];
				$sv["nama_pp"]            = $raw[2];
				$sv["notelp"]               = $raw[3];
				$sv["refnum_jpa"] = $raw[4];
				$sv["tran_amount"]                = $raw[5];
				$sv["admin_bank"]           = $raw[6];
				$sv["info"]            = $raw[7];
				$sv["operator"]                  = $raw[8];
				$sv["stan"]             = $raw[9];
				$sv['created_at']             = date('Y-m-d H:i:s');

				$sv['raw_rekon'] = $string;
				$ins = DB::table('rekon_pulsa_prepaid')
					->insert($sv);
			}
		}*/

		/*$string = str_replace(array("\n","\t"), array('|','|'), $string);
		$string = str_replace('|||', '|', $string);
		//echo $string;
		//exit();
		$tags = explode('|',$string);
		$num = 1;
		foreach($tags as $key) {    
		    echo $key.($num == 3 ? '<br/>' : ',');  
		    if($num == 3) {
		    	$num = 1;
		    } else {
		    	$num++;
		    }
		}*/

		//$month1 = '2018-09-01';
		//$current = strtotime( $first );

		//$month1 = date( 'm', $month1 );
		/*$current = date( 'm' );
		$jml_bulan = $month1-$current+1;
		$jml_bulan = ($jml_bulan <= 0 ? 1 : $jml_bulan);
		//echo $jml_bulan;

		$d1 = $month1;
		$d2 = date('Y-m-d');
		echo (int)abs((strtotime($d1) - strtotime($d2))/(60*60*24*30));*/

		/*$string = '085287882816';
		$a = substr($string, 4);
		$b = substr($a, 0,10);
		echo sprintf("%04s", 1234).substr($string, 0, 4).sprintf("%-10d", $b);*/
		/*$string = 'JTL53L314987654321149999999911069F5EBE6AA8A507175698DA9773E20690MUP210Z7C2D959CD872B8786D74A89800112233HAMDANIE LESTALUHUANI    R1  000000900020000000000200000000002000002040820000019996200000197942000001939802200000016219875231541498765432120180717175903';
		echo ' <br>#switcher '.substr($string, 0, 7); 
		echo ' <br>#meter id '.substr($string, 7, 11);
		echo ' <br>#id pel '.substr($string, 18, 12);
		echo ' <br>#flag '.substr($string, 30, 1);
		echo ' <br>#trx id '.substr($string, 31, 32);
		echo ' <br>#ref no '.substr($string, 63, 32);
		echo ' <br>#vending no '.substr($string, 95, 8);
		echo ' <br>#nama '.substr($string, 103, 25);
		echo ' <br>#tarif '.substr($string, 128, 4);
		echo ' <br>#kategori daya '.substr($string, 132, 9);

		echo ' <br>#pilihan pembelian '.substr($string, 141, 1);
		echo ' <br>#minor biaya admin '.substr($string, 142, 1);
		echo ' <br>#biaya admin '.substr($string, 143, 10);
		echo ' <br>#minor materai '.substr($string, 153, 1);
		echo ' <br>#biaya materai '.substr($string, 154, 10);
		echo ' <br>#minor ppn '.substr($string, 164, 1);
		echo ' <br>#ppn '.substr($string, 165, 10);
		echo ' <br>#minor ppju '.substr($string, 175, 1);
		echo ' <br>#ppju '.substr($string, 176, 10);
		echo ' <br>#minor angsuran '.substr($string, 186, 1);
		echo ' <br>#angsuran '.substr($string, 187, 10);
		echo ' <br>#minor pembelian listrik '.substr($string, 197, 1);
		echo ' <br>#pembelian listrik '.substr($string, 198, 12);

		echo ' <br>#minor kwh '.substr($string, 210, 1);
		echo ' <br>#jml kwh '.substr($string, 211, 10);
		echo ' <br>#token '.substr($string, 221, 20);
		echo ' <br>#tgl lunas '.substr($string, 241, 14);*/


		/*$string = '5434003657021000097326BEB970F5987C0511AB78F417DSALINO DANIEL            54380123              1R0000001000000000002016032004201600000000000000098623D00000000000000000000000000003000018092000182620000000000000000000000000000000000';

		echo ' <br>#idpel '.substr($string, 0, 12); 
		echo ' <br>#jml tagihan '.substr($string, 12, 1); 
		echo ' <br>#jml tunggakan '.substr($string, 13, 2); 
		echo ' <br>#trxid '.substr($string, 15, 32); 
		echo ' <br>#nama '.substr($string, 47, 25); 
		echo ' <br>#unitservice '.substr($string, 72, 5); 
		echo ' <br>#notelpunitservice '.substr($string, 77, 15); 
		echo ' <br>#tarif '.substr($string, 92, 4); 
		echo ' <br>#daya '.substr($string, 96, 9); 
		echo ' <br>#biayaadmin '.substr($string, 105, 9); 
		echo ' <br>#blth '.substr($string, 114, 6); 
		echo ' <br>#tgljthtempo '.substr($string, 120, 8); 
		echo ' <br>#tglcatetmeter '.substr($string, 128, 8); 
		echo ' <br>#biayatagihan pln '.substr($string, 136, 11); 
		echo ' <br>#insentif '.substr($string, 147, 11); 
		echo ' <br>#biayappn '.substr($string, 158, 10); 
		echo ' <br>#biayaketerlambatan '.substr($string, 168, 9); 
		echo ' <br>#lwbpsebelum '.substr($string, 177, 8); 
		echo ' <br>#lwbpsesudah '.substr($string, 185, 8); 
		echo ' <br>#wbpsebelum '.substr($string, 193, 8); 
		echo ' <br>#wbpsesudah '.substr($string, 201, 8); 
		echo ' <br>#kvarhsebelum '.substr($string, 209, 8); 
		echo ' <br>#kvarhsesudah '.substr($string, 217, 8); 
		$tambah = substr($string, 0, 13).substr($string, 12, 1);
		$after = substr($string, 13);
		echo '<br>tambah '.$tambah.$after;*/

		/*$string = '543400365702110002A27F2E46190CE48A537C4C1118ED0F0MUP210ZFDF229E84BDB855957F38E93SALINO DANIEL            54380123              1R0000001000000000002016032004201600000000000000098623D0000000000000000000000000000300001809200018262000000000000000000000000000000000020180717205117';

		echo ' <br>#idpel '.substr($string, 0, 12); 
		echo ' <br>#jml tagihan '.substr($string, 12, 1); 
		echo ' <br>#jml payment '.substr($string, 13, 1); 
		echo ' <br>#jml tunggakan '.substr($string, 14, 2); 
		echo ' <br>#trxid '.substr($string, 16, 32); 
		echo ' <br>#noref '.substr($string, 48, 32); 
		echo ' <br>#nama '.substr($string, 80, 25); 
		echo ' <br>#unitservice '.substr($string, 105, 5); 
		echo ' <br>#notelpunitservice '.substr($string, 110, 15); 
		echo ' <br>#tarif '.substr($string, 125, 4); 
		echo ' <br>#daya '.substr($string, 129, 9); 
		echo ' <br>#biayaadmin '.substr($string, 138, 9); 
		echo ' <br>#blth '.substr($string, 147, 6); 
		echo ' <br>#tgljthtempo '.substr($string, 153, 8); 
		echo ' <br>#tglcatetmeter '.substr($string, 161, 8); 
		echo ' <br>#biayatagihan pln '.substr($string, 169, 11); 
		echo ' <br>#insentif '.substr($string, 180, 11); 
		echo ' <br>#biayappn '.substr($string, 191, 10); 
		echo ' <br>#biayaketerlambatan '.substr($string, 201, 9); 
		echo ' <br>#lwbpsebelum '.substr($string, 210, 8); 
		echo ' <br>#lwbpsesudah '.substr($string, 218, 8); 
		echo ' <br>#wbpsebelum '.substr($string, 226, 8); 
		echo ' <br>#wbpsesudah '.substr($string, 234, 8); 
		echo ' <br>#kvarhsebelum '.substr($string, 242, 8); 
		echo ' <br>#kvarhsesudah '.substr($string, 250, 8); 
		echo ' <br>#tgllunas '.substr($string, 258, 8); 
		echo ' <br>#jamlunas '.substr($string, 266, 6); */

		/*$string = '21100019430111000MUP210Z5376E41E7D1537E8F3C4EBF1F.ANI SYAHLENDRA         21100                 R10000013000000000002016042005201600000000000000445605D0000000000000000000000000000500005395000054464000000000000000000000000000000000020180912161229';

		echo ' <br>#idpel '.substr($string, 0, 12); 
		echo ' <br>#jml tagihan '.substr($string, 12, 1); 
		echo ' <br>#jml payment '.substr($string, 13, 1); 
		echo ' <br>#jml tunggakan '.substr($string, 14, 2); 
		echo ' <br>#noref '.substr($string, 16, 32); 
		echo ' <br>#nama '.substr($string, 48, 25); 

		echo ' <br>#unitservice '.substr($string, 73, 5); 
		echo ' <br>#notelpunitservice '.substr($string, 78, 15); 

		echo ' <br>#tarif '.substr($string, 93, 4); 
		echo ' <br>#daya '.substr($string, 97, 9); 
		echo ' <br>#biayaadmin '.substr($string, 106, 9); 

		echo ' <br>#blth '.substr($string, 115, 6); 

		echo ' <br>#tgljthtempo '.substr($string, 121, 8); 
		echo ' <br>#tglcatetmeter '.substr($string, 129, 8); 
		echo ' <br>#biayatagihan pln '.substr($string, 137, 12); 
		echo ' <br>#insentif '.substr($string, 149, 11); 

		echo ' <br>#biayappn '.substr($string, 160, 10); 
		echo ' <br>#biayaketerlambatan '.substr($string, 170, 12); 
		echo ' <br>#lwbpsebelum '.substr($string, 182, 8); 
		echo ' <br>#lwbpsesudah '.substr($string, 190, 8); 

		echo ' <br>#wbpsebelum '.substr($string, 198, 8); 

		echo ' <br>#wbpsesudah '.substr($string, 206, 8); 
		echo ' <br>#kvarhsebelum '.substr($string, 214, 8); 

		echo ' <br>#kvarhsesudah '.substr($string, 222, 8); 
		echo ' <br>#tglbatal '.substr($string, 230, 8); 
		echo ' <br>#jambatal '.substr($string, 238, 6); */

		/*$string = '000108528788281600000004970000000100000000008828160718510000814145518DEC78E1EA0930470770AB0C20180718123953';

		echo ' <br>#product_id '.substr($string, 0, 4); 
		echo ' <br>#code_area '.substr($string, 4, 4); 
		echo ' <br>#phone_no '.substr($string, 8, 10); 
		echo ' <br>#voucher_nominal '.substr($string, 18, 12); 
		echo ' <br>#biaya_admin '.substr($string, 30, 8); 
		echo ' <br>#expire '.substr($string, 38, 8); 
		echo ' <br>#serial_no '.substr($string, 46, 16); 
		echo ' <br>#no_ref_switching '.substr($string, 62, 32); 
		echo ' <br>#waktu_lunas '.substr($string, 94, 14); */

		/*$string = '0001081212331101  Tes /hallo....                4ACA80D5CC85C9D3F5F09B0AFAE1C45E10000100000000000704A       000000021000           000000000000           000000000000';

		echo ' <br>#product_id '.substr($string, 0, 4); 
		echo ' <br>#kode_area '.substr($string, 4, 4); 
		echo ' <br>#phone_no '.substr($string, 8, 10); 
		echo ' <br>#customer_name '.substr($string, 18, 30); 
		echo ' <br>#no_ref_switching '.substr($string, 48, 32); 
		echo ' <br>#total_bill '.substr($string, 80, 1); 
		echo ' <br>#biaya_admin '.substr($string, 81, 8); 
		echo ' <br>#expire '.substr($string, 89, 8); 
		echo ' <br>#bill_ref_1 '.substr($string, 97, 11); 
		echo ' <br>#bill_amount_1 '.substr($string, 108, 12); 
		echo ' <br>#bill_ref_2 '.substr($string, 120, 11); 
		echo ' <br>#bill_amount_2 '.substr($string, 131, 12); 
		echo ' <br>#bill_ref_3 '.substr($string, 143, 11); 
		echo ' <br>#bill_amount_3 '.substr($string, 154, 12); 
		echo ' <br>#waktu_lunas '.substr($string, 166, 14); */

		$string = '00000141       201201201201JUNAIDI XX014                 0101000000011110000010002012010000000111100000000000000402-00000458';

		echo ' '.$string; 
		echo ' <br><br>#idpel '.substr($string, 0, 15); 
		echo ' <br>#blth '.substr($string, 15, 12); 
		echo ' <br>#customer_name '.substr($string, 27, 30); 
		echo ' <br>#bill_count '.substr($string, 57, 2); 
		echo ' <br>#bill_repeat_count '.substr($string, 59, 2); 
		echo ' <br>#rupiah_tagihan '.substr($string, 61, 12); 
		echo ' <br>#biaya_admin '.substr($string, 73, 8); 
		echo ' <br>#bill_date_1 '.substr($string, 81, 6); 
		echo ' <br>#bill_amount_1 '.substr($string, 87, 12); 
		echo ' <br>#penalty_1 '.substr($string, 99, 8); 
		echo ' <br>#kubikasi_1 '.substr($string, 107, 17); 
		echo ' <br>#bill_date_2 '.substr($string, 124, 6); 
		echo ' <br>#bill_amount_2 '.substr($string, 130, 12); 
		echo ' <br>#penalty_2 '.substr($string, 142, 8); 
		echo ' <br>#kubikasi_2 '.substr($string, 150, 17); 
		echo ' <br>#bill_date_3 '.substr($string, 167, 6); 
		echo ' <br>#bill_amount_3 '.substr($string, 173, 12); 
		echo ' <br>#penalty_3 '.substr($string, 185, 8); 
		echo ' <br>#kubikasi_3 '.substr($string, 193, 17); 
		echo ' <br>#bill_date_4 '.substr($string, 210, 6); 
		echo ' <br>#bill_amount_4 '.substr($string, 216, 12); 
		echo ' <br>#penalty_4 '.substr($string, 228, 8); 
		echo ' <br>#kubikasi_4 '.substr($string, 236, 17); 

		/*$string = '00000161       201201201201JUNAIDI XX016                 0101A2345678901234519F35ECC297AB03535BC3C94D0DAB306000000011110000010002012010000000111100000000000000402-0000045820180831081752';

		echo ' <br>#idpel '.substr($string, 0, 15); 
		echo ' <br>#blth '.substr($string, 15, 12); 
		echo ' <br>#customer_name '.substr($string, 27, 30); 
		echo ' <br>#bill_count '.substr($string, 57, 2); 
		echo ' <br>#bill_repeat_count '.substr($string, 59, 2); 
		echo ' <br>#no_ref_biller '.substr($string, 61, 15); 
		echo ' <br>#no_ref_switching '.substr($string, 76, 32); 
		echo ' <br>#rupiah_tagihan '.substr($string, 108, 12); 
		echo ' <br>#biaya_admin '.substr($string, 120, 8); 





		$repeat = substr($string, 59, 2);
		if($repeat == '01') {
			echo ' <br>#bill_date_1 '.substr($string, 128, 6); 
			echo ' <br>#bill_amount_1 '.substr($string, 134, 12); 
			echo ' <br>#penalty_1 '.substr($string, 146, 8);
			echo ' <br>#kubikasi_1 '.substr($string, 154, 17); 
			echo ' <br>#waktu_lunas '.substr($string, 171, 14); 
		} elseif($repeat == '02') {
			echo ' <br>#bill_date_2 '.substr($string, 171, 6); 
			echo ' <br>#bill_amount_2 '.substr($string, 177, 12); 
			echo ' <br>#penalty_2 '.substr($string, 189, 8); 
			echo ' <br>#kubikasi_2 '.substr($string, 197, 17); 
			echo ' <br>#waktu_lunas '.substr($string, 214, 14); 
		} elseif($repeat == '03') {
			echo ' <br>#bill_date_3 '.substr($string, 214, 6); 
			echo ' <br>#bill_amount_3 '.substr($string, 220, 12); 
			echo ' <br>#penalty_3 '.substr($string, 232, 8); 
			echo ' <br>#kubikasi_3 '.substr($string, 240, 17); 
			echo ' <br>#waktu_lunas '.substr($string, 257, 14); 
		} else {
			echo ' <br>#waktu_lunas '.substr($string, 300, 14); 
			echo ' <br>#bill_date_4 '.substr($string, 257, 6); 
			echo ' <br>#bill_amount_4 '.substr($string, 263, 12); 
			echo ' <br>#penalty_4 '.substr($string, 275, 8); 
			echo ' <br>#kubikasi_4 '.substr($string, 283, 17); 
		}*/


		/*$string = '000000125679828301000000125679828300000015300000002500011891251         8988801256798283JULIEN WUISANG                1011  BEKASI              000000153000000000153000000000000000';

		echo ' <br>#bpjsks_no_va_keluarga '.substr($string, 0, 16); 
		echo ' <br>#bpjsks_jml_bulan '.substr($string, 0, 2); 
		echo ' <br>#bpjsks_no_va_kepala_keluarga '.substr($string, 0, 16); 
		echo ' <br>#bpjsks_total_premi '.substr($string, 0, 12); 
		echo ' <br>#bpjsks_biaya_admin '.substr($string, 0, 8); 
		echo ' <br>#bpjsks_jml_anggota_keluarga '.substr($string, 0, 2); 
		echo ' <br>#bpjsks_kode_premi_anggota '.substr($string, 0, 16); 
		echo ' <br>#bpjsks_no_va_anggota_keluarga '.substr($string, 0, 16); 
		echo ' <br>#bpjsks_nama '.substr($string, 0, 30); 
		echo ' <br>#bpjsks_kd_cabang '.substr($string, 0, 6); 
		echo ' <br>#bpjsks_nm_cabang '.substr($string, 0, 20); 
		echo ' <br>#bpjsks_biaya_premi_dibayar '.substr($string, 0, 12);
		echo ' <br>#bpjsks_biaya_premi_bln_ini '.substr($string, 0, 12);
		echo ' <br>#bpjsks_premi_dimuka '.substr($string, 0, 12); */

		/*$string = '0000000125406718010000000125406718000000051000000025000225945863        8988800127676744AULIYANTI,SH                  1801  MAKASSAR            00000002550000000002550000000000000025945923        8988800127676755ULFAIZAH,SN                   1801  MAKASSAR            000000025500000000025500000000000000';

		echo ' <br>#bpjsks_no_va_keluarga '.substr($string, 0, 16); 
		echo ' <br>#bpjsks_jml_bulan '.substr($string, 16, 2); 
		echo ' <br>#bpjsks_no_va_kepala_keluarga '.substr($string, 18, 16); 
		echo ' <br>#bpjsks_total_premi '.substr($string, 34, 12); 
		echo ' <br>#bpjsks_biaya_admin '.substr($string, 46, 8); 
		echo ' <br>#bpjsks_jml_anggota_keluarga '.substr($string, 54, 2); 

		$count_1 = 56;
		$jml_keluarga = ltrim(substr($string, 54, 2),0);
		for ($i=0; $i < $jml_keluarga; $i++) { 
			$bpjsks_kode_premi_anggota .= substr($string, $count_1, 16).','; 
			$count_1 = $count_1+16;
			$bpjsks_no_va_anggota_keluarga .= substr($string, $count_1, 16).','; 
			$count_1 = $count_1+16;
			$bpjsks_nama .= substr($string, $count_1, 30).','; 
			$count_1 = $count_1+30;
			$bpjsks_kd_cabang .= substr($string, $count_1, 6).','; 
			$count_1 = $count_1+6;
			$bpjsks_nm_cabang .= substr($string, $count_1, 20).','; 
			$count_1 = $count_1+20;
			$bpjsks_biaya_premi_dibayar .= substr($string, $count_1, 12).',';
			$count_1 = $count_1+12;
			$bpjsks_biaya_premi_bln_ini .= substr($string, $count_1, 12).',';
			$count_1 = $count_1+12;
			$bpjsks_premi_dimuka .= substr($string, $count_1, 12).',';
			$count_1 = $count_1+12;

		}

		echo '<br>#bpjsks_kode_premi_anggota '.$bpjsks_kode_premi_anggota;
		echo ' <br>#bpjsks_no_va_anggota_keluarga '.$bpjsks_no_va_anggota_keluarga;
		echo ' <br>#bpjsks_nama '.$bpjsks_nama;
		echo ' <br>#bpjsks_kd_cabang '.$bpjsks_kd_cabang;
		echo ' <br>#bpjsks_nm_cabang '.$bpjsks_nm_cabang;
		echo ' <br>#bpjsks_biaya_premi_dibayar '.$bpjsks_biaya_premi_dibayar;
		echo ' <br>#bpjsks_biaya_premi_bln_ini '.$bpjsks_biaya_premi_bln_ini;
		echo ' <br>#bpjsks_premi_dimuka '.$bpjsks_premi_dimuka;*/

		/*$string = '0000001293641649010000001293641649000000160000000025009161060E7C345F20                021891250         8988801293641649SAMANA                        1101  SEMARANG            0000000800000000000800000000000000001891255         8988801293641807NUR HARYANTI MARSINAH         1101  SEMARANG            00000008000000000008000000000000000020180722232730';

		echo ' <br>#bpjsks_no_va_keluarga '.substr($string, 0, 16); 
		echo ' <br>#bpjsks_jml_bulan '.substr($string, 16, 2); 
		echo ' <br>#bpjsks_no_va_kepala_keluarga '.substr($string, 18, 16); 
		echo ' <br>#bpjsks_total_premi '.substr($string, 34, 12); 
		echo ' <br>#bpjsks_biaya_admin '.substr($string, 46, 8); 
		echo ' <br>#jpa_refnum '.substr($string, 54, 32); 
		echo ' <br>#bpjsks_jml_anggota_keluarga '.substr($string, 86, 2); 

		$count_1 = 88;
		$jml_keluarga = ltrim(substr($string, 86, 2),0);
		for ($i=0; $i < $jml_keluarga; $i++) { 
			$bpjsks_kode_premi_anggota .= substr($string, $count_1, 16).','; 
			$count_1 = $count_1+16;
			$bpjsks_no_va_anggota_keluarga .= substr($string, $count_1, 16).','; 
			$count_1 = $count_1+16;
			$bpjsks_nama .= substr($string, $count_1, 30).','; 
			$count_1 = $count_1+30;
			$bpjsks_kd_cabang .= substr($string, $count_1, 6).','; 
			$count_1 = $count_1+6;
			$bpjsks_nm_cabang .= substr($string, $count_1, 20).','; 
			$count_1 = $count_1+20;
			$bpjsks_biaya_premi_dibayar .= substr($string, $count_1, 12).',';
			$count_1 = $count_1+12;
			$bpjsks_biaya_premi_bln_ini .= substr($string, $count_1, 12).',';
			$count_1 = $count_1+12;
			$bpjsks_premi_dimuka .= substr($string, $count_1, 12).',';
			$count_1 = $count_1+12;

		}

		echo '<br>#bpjsks_kode_premi_anggota '.$bpjsks_kode_premi_anggota;
		echo ' <br>#bpjsks_no_va_anggota_keluarga '.$bpjsks_no_va_anggota_keluarga;
		echo ' <br>#bpjsks_nama '.$bpjsks_nama;
		echo ' <br>#bpjsks_kd_cabang '.$bpjsks_kd_cabang;
		echo ' <br>#bpjsks_nm_cabang '.$bpjsks_nm_cabang;
		echo ' <br>#bpjsks_biaya_premi_dibayar '.$bpjsks_biaya_premi_dibayar;
		echo ' <br>#bpjsks_biaya_premi_bln_ini '.$bpjsks_biaya_premi_bln_ini;
		echo ' <br>#bpjsks_premi_dimuka '.$bpjsks_premi_dimuka;

		echo ' <br>#bpjsks_tgl_lunas '.substr($string, -14); */

		/*$string = '20170121140001';
		echo substr($string,0,4);
		echo substr($string,4,2);
		echo substr($string,6,2);
		echo substr($string,8,2);
		echo substr($string,10,1);
		echo substr($string,11,1);
		echo substr($string,12,2);

		echo substr($string,6,2).'/'.substr($string,4,2).'/'.substr($string,0,4).' '.substr($string,8,2).':'.substr($string,10,1).substr($string,11,1).':'.substr($string,12,2);*/

		/*$string = '1710001119012200B2B7EC8E8D3D3B0B3EF28024BE5E975EROHANI                   17100123              R1000000900000001200201509200920150000000000000054685D00000000000000000000000003000029885000299620000000000000000000000000000000000';

		$before = substr($string, 0,106);
		$after = substr($string, -111);
		$admin = sprintf("%06s",1200);

		echo $before*//*.$admin.$after*/

		/*$month1 = strtotime( '2018-09-01' );
		$current = strtotime( '2018-08-01' );

		$month1 = date( 'm', $month1 );
		$current = date( 'm', $current );
		echo $month1-$current+1;*/
	}

	public function postInquiryPrabayarPln() {
		$no_hp = Request::get('no_hp');
		$id_product = Request::get('id_product');
		$no_meter = Request::get('no_meter');
		$pin = Request::get('pin');
		$id_agen = Request::get('id_agen');
		$id_voucher = Request::get('id_voucher');

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		/*send iso inquiry*/
		$send_iso_inquiry = Esta::send_iso_pln_prabayar($id_product,$no_meter,'0200','0','Inquiry','0','0','0');

		if($send_iso_inquiry['39'] == '00') {
			$token_unsold = Esta::view_bit62($send_iso_inquiry['62']);
			if($token_unsold['total_repeat'] >= 1) {
				for ($i=0; $i < $token_unsold['total_repeat']; $i++) { 
					if($i == 1) {
						$rest_unsold_1 = substr($token_unsold['power_purchase_unsold'], 0, 11);
					} else {
						$rest_unsold_2 = substr($token_unsold['power_purchase_unsold'], 11, 11);
					}
				}
			}

	    	$rest['id_transaksi']  = $send_iso_inquiry['id_log'];
	    	$rest['pln_pra_meter_id'] = $send_iso_inquiry['pln_pra_meter_id'];
			$rest['pln_pra_nama'] = $send_iso_inquiry['pln_pra_nama'];
			$rest['pln_pra_tarif'] = str_replace(' ', '', $send_iso_inquiry['pln_pra_tarif']);
			$rest['pln_pra_kategori_daya'] = $send_iso_inquiry['pln_pra_kategori_daya'];

			$response['api_status']  = 1;
	    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_inquiry['39'],$no_meter);
	    	$response['type_dialog']  = 'Informasi';
	    	$response['item']  = $rest;

	    	$detail_product_unsold_1 = DB::table('pan_product')->where('product_name','Token Unsold 1')->first();
	    	$detail_product_unsold_2 = DB::table('pan_product')->where('product_name','Token Unsold 2')->first();

	    	$response['unsold_1_nama'] = 'Token unsold '.number_format($rest_unsold_1,0,',','.');
	    	$response['unsold_1']  = ($rest_unsold_1 <= 0 ? '' : ltrim($rest_unsold_1+$detail_product_unsold_1->admin_edn+$detail_product_unsold_1->admin_1+$detail_product_unsold_1->admin_2+$detail_product_unsold_1->admin_3+$detail_product_unsold_1->margin,0));
	    	$response['unsold_1_val']  = ($rest_unsold_1 <= 0 ? 0 : ltrim($rest_unsold_1,0));
	    	$response['komisi_unsold_1']  = ($detail_agen->status_agen == 'Basic' ? DB::table('pan_product')->where('product_name','Token Unsold 1')->first()->komisi_basic : DB::table('pan_product')->where('product_name','Token Unsold 1')->first()->komisi_premium);

	    	$response['unsold_2_nama'] = 'Token unsold '.number_format($rest_unsold_2,0,',','.');
	    	$response['unsold_2']  = ($rest_unsold_2 <= 0 ? '' : ltrim($rest_unsold_2+$detail_product_unsold_2->admin_edn+$detail_product_unsold_2->admin_1+$detail_product_unsold_2->admin_2+$detail_product_unsold_2->admin_3+$detail_product_unsold_2->margin,0));
	    	$response['unsold_2_val']  = ($rest_unsold_2 <= 0 ? 0 : ltrim($rest_unsold_2,0));
	    	$response['komisi_unsold_2']  = ($detail_agen->status_agen == 'Basic' ? DB::table('pan_product')->where('product_name','Token Unsold 2')->first()->komisi_basic : DB::table('pan_product')->where('product_name','Token Unsold 2')->first()->komisi_premium);
		} else {
			$response['api_status']  = 0;
	    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_inquiry['39'],$no_meter);
	    	$response['type_dialog']  = 'Error';
		}
		return response()->json($response);
	}

	public function postPlnPrepaidAdviceManual() {
		$stan = Request::get('stan');
		$trans_no = Request::get('trans_no');

		$inquiry = DB::table('log_jatelindo_bit');
			if(!empty($stan)) {
				$inquiry = $inquiry->where('bit11',$stan);
			}
			$inquiry = $inquiry
			->where('bit37',$trans_no)
			->where('type','PLN Prepaid')
			->where('status','Inquiry')
			->where('jenis','res')
			->first();
		$purchase = DB::table('log_jatelindo_bit');
			if(!empty($stan)) {
				$purchase = $purchase->where('bit11',$stan);
			}
			$purchase = $purchase
			->where('bit37',$trans_no)
			->where('type','PLN Prepaid')
			->where('status','Purchase')
			->where('jenis','res')
			->first();

		$detail_transaksi = DB::table('trans_pln')
			->where('trans_no',$trans_no)
			->first();

		$detail_product = DB::table('pan_product')
			->where('id',$detail_transaksi->id_product)
			->first();

		$detail_voucher= DB::table('voucher')
			->where('id',$detail_transaksi->id_product)
			->first();
			/*print_r($detail_product);
			exit();*/
		if(!empty($purchase)) {
			$send_iso_purchase = Esta::send_iso_pln_prabayar($detail_transaksi->id_product,$inquiry->pln_pra_meter_id,'0221',$inquiry->id,'Advice Manual',$purchase->pln_pra_unsold_repeat,$purchase->pln_pra_unsold,$purchase->pln_pra_unsold_nominal);

			$no_meter = $inquiry->pln_pra_meter_id;
			$product_amount = ($purchase->pln_pra_unsold == 1 ? $purchase->pln_pra_unsold_nominal : $detail_product->amount);
		} else {
			$send_iso_purchase = Esta::send_iso_pln_prabayar($detail_transaksi->id_product,$inquiry->pln_pra_meter_id,'0221',$inquiry->id,'Advice Manual',0,0,0);

			$no_meter = $inquiry->pln_pra_meter_id;
			$product_amount = $detail_product->amount;
		}

		if($send_iso_purchase['39'] == '00') {
			$response['api_status']  = 1;
	    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_purchase['39'],$no_meter);
	    	$response['type_dialog']  = 'Error';
	    	$response['id_transaksi']  = $detail_transaksi->id;
	    	$response['id_log']  = $send_iso_purchase['id_log'];

	    	$response['pln_pra_meter_id'] = $send_iso_purchase['pln_pra_meter_id'];
	    	$response['pln_pra_id_pel'] = $send_iso_purchase['pln_pra_id_pel'];
			$response['pln_pra_nama'] = $send_iso_purchase['pln_pra_nama'];
			$response['pln_pra_tarif'] = str_replace(' ', '', $send_iso_purchase['pln_pra_tarif']);
			$response['pln_pra_kategori_daya'] = $send_iso_purchase['pln_pra_kategori_daya'];
			$response['pln_pra_ref_no'] = $send_iso_purchase['pln_pra_ref_no'];
			$response['pln_pra_biaya_materai'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['pln_pra_ppn'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['pln_pra_ppju'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['pln_pra_angsuran'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['pln_pra_rp_stroom'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
			$response['pln_pra_jml_kwh'] = substr(Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_jml_kwh'],0),$send_iso_purchase['pln_pra_nilai_minor_kwh']),0,-1);
			$response['pln_pra_token_number'] = chunk_split($send_iso_purchase['pln_pra_token_number'],4,' ');
			$response['pln_pra_biaya_admin'] = 'Rp '.number_format($detail_transaksi->product_biaya_admin_edn+$detail_transaksi->product_biaya_admin_1+$detail_transaksi->product_biaya_admin_2+$detail_transaksi->product_biaya_admin_3+$detail_transaksi->product_margin-$detail_transaksi->product_potongan,0,',','.');

			$response['val_pln_pra_rp_stroom'] = ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0);
			$response['val_pln_pra_biaya_materai'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['val_pln_pra_ppn'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['val_pln_pra_ppju'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['val_pln_pra_angsuran'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['val_pln_pra_biaya_admin'] = $detail_transaksi->product_biaya_admin_edn+$detail_transaksi->product_biaya_admin_1+$detail_transaksi->product_biaya_admin_2+$detail_transaksi->product_biaya_admin_3+$detail_transaksi->product_margin-$detail_transaksi->product_potongan;

			$response['pln_pra_pembelian_listrik'] = 'Rp '.Esta::nilai_minor($response['val_pln_pra_rp_stroom']+$response['val_pln_pra_biaya_admin'],$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
			$response['info'] = str_replace('TMP', '"TMP"', $send_iso_purchase['62']);
			$response['val_komisi'] = $detail_transaksi->komisi;
			$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
			$response['pln_pra_tgl_lunas'] = substr($send_iso_purchase['pln_pra_tgl_lunas'],6,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],4,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],0,4).' '.substr($send_iso_purchase['pln_pra_tgl_lunas'],8,2).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],10,1).substr($send_iso_purchase['pln_pra_tgl_lunas'],11,1).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],12,2);
			//$response['nominal_total_pembayaran'] = 'Rp '.number_format($trans_amount, 0, ',', '.');
			$response['nominal_voucher'] = $detail_transaksi->voucher_amount;
			$response['tipe_pembayaran'] = 'PLN Prepaid';

			$response['val_2_pln_pra_ppn'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['val_2_pln_pra_ppju'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['val_2_pln_pra_angsuran'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['val_2_pln_pra_biaya_materai'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['val_2_pln_pra_rp_stroom'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);

			$response['val_rp_bayar'] = $response['val_pln_pra_biaya_admin']+$product_amount;
			$response['rp_bayar'] = 'Rp '.number_format($response['val_rp_bayar'],0,',','.');
			$total_pembayaran = $response['val_rp_bayar']-$response['val_komisi']-$detail_voucher->amount;
			$response['total_pembayaran'] = 'Rp '.number_format(($total_pembayaran <= 0 ? 0 : $total_pembayaran),0,',','.');

			$up['struk_id_pelanggan'] = $response['pln_pra_id_pel'];
			$up['no_meter'] = $response['pln_pra_meter_id'];
			$up['struk_info'] = $response['info'];
			$up['struk_nama_pelanggan'] = $response['pln_pra_nama'];
			$up['struk_tarif'] = $response['pln_pra_tarif'];
			$up['struk_daya'] = $response['pln_pra_kategori_daya'];
			$up['struk_rp_bayar'] = $response['val_rp_bayar'];
			$up['struk_materai'] = $response['val_2_pln_pra_biaya_materai'];
			$up['struk_ppn'] = $response['val_2_pln_pra_ppn'];
			$up['struk_ppj'] = $response['val_2_pln_pra_ppju'];
			$up['struk_angsuran'] = $response['val_2_pln_pra_angsuran'];
			$up['struk_rp_stroom'] = $response['val_2_pln_pra_rp_stroom'];
			$up['struk_jml_kwh'] = $response['pln_pra_jml_kwh'];
			$up['struk_token'] = $response['pln_pra_token_number'];
			$up['struk_admin_bank'] = $response['val_pln_pra_biaya_admin'];
			$up['struk_total_pembayaran'] = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
			$up['struk_tgl_lunas'] = $response['pln_pra_tgl_lunas'];
			$up['jpa_ref'] = $response['pln_pra_ref_no'];
			$up['status'] = 'Clear';
			$update = DB::table('trans_pln')
				->where('id',$detail_transaksi->id)
				->update($up);

			/*struk*/

			/*email*/

			$view     = view('struk/struk_pln_prepaid',$response)->render();
			$filename = "Struk-PLN-Prepaid-".$response['pln_pra_id_pel'];
			$pdf      = App::make('dompdf.wrapper');

			$path = storage_path('app/uploads/'.$filename.'.pdf');

			$pdf->loadHTML($view);
			$pdf->setPaper('A4','landscape');
			$output = $pdf->output();

			file_put_contents($path, $output);

			$attachments = [$path];
			$email = $detail_agen->email;
			if(!empty($email) || $detail_agen->notif_email != 'No') {
			    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pln_prepaid','attachments'=>$attachments]);
			}
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_purchase['39'],$no_meter);
	    	$response['id_transaksi']  = 0;
	    	$response['type_dialog']  = 'Error';
		}

		return response()->json($response);
	}

	public function postBpjsAdviceManual() {
		$stan = Request::get('stan');
		$trans_no = Request::get('trans_no');

		$inquiry = DB::table('log_jatelindo_bit');
			if(!empty($stan)) {
				$inquiry = $inquiry->where('bit11',$stan);
			}
			$inquiry = $inquiry
			->where('bit37',$trans_no)
			->where('type','BPJS Kesehatan')
			->where('status','Inquiry')
			->where('jenis','res')
			->first();
		$purchase = DB::table('log_jatelindo_bit');
			if(!empty($stan)) {
				$purchase = $purchase->where('bit11',$stan);
			}
			$purchase = $purchase
			->where('bit37',$trans_no)
			->where('type','BPJS Kesehatan')
			//->where('status','Purchase')
			->where('jenis','res')
			->orderBy('id','desc')
			->first();

		$detail_transaksi = DB::table('trans_bpjs')
			->where('trans_no',$trans_no)
			->first();

		$detail_product = DB::table('pan_product')
			->where('id',$detail_transaksi->id_product)
			->first();

		$detail_voucher= DB::table('voucher')
			->where('id',$detail_transaksi->id_product)
			->first();

		/*send iso*/
		$send_iso = Esta::send_iso_bpjsks($detail_transaksi->id_product,$detail_transaksi->no_hp,'0200','Advice 3',$inquiry->id,$detail_transaksi->id_pelanggan,$inquiry->bpjsks_jml_bulan);

		if($send_iso['39'] == '00') {

			$response['api_status']  = 1;
	    	$response['api_message'] = 'Transaksi BPJS berhasil';
	    	$response['type_dialog']  = 'Informasi';
	    	$response['id_transaksi']  = $send_iso['id_log'];
	    	$response['id_log']  = $send_iso['id_log'];

			$response['val_komisi'] = $detail_transaksi->komisi;

			$response['bpjsks_jpa_refnum'] = $send_iso['bpjsks_jpa_refnum'];
			$response['bpjsks_nama'] = $send_iso['bpjsks_nama'];
			$response['bpjsks_no_va_keluarga'] = $send_iso['bpjsks_no_va_keluarga'];
			$response['bpjsks_no_va_kepala_keluarga'] = $send_iso['bpjsks_no_va_kepala_keluarga'];
			$response['bpjsks_jml_anggota_keluarga'] = ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0).' Orang';
			$response['periode'] = ltrim($send_iso['periode'],0).' Bulan';
			$response['bpjsks_nm_cabang'] = $send_iso['bpjsks_nm_cabang'];
			$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
			$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);

			$response['bpjsks_tgl_lunas'] = substr($send_iso['bpjsks_tgl_lunas'],6,2).'/'.substr($send_iso['bpjsks_tgl_lunas'],4,2).'/'.substr($send_iso['bpjsks_tgl_lunas'],0,4).' '.substr($send_iso['bpjsks_tgl_lunas'],8,2).':'.substr($send_iso['bpjsks_tgl_lunas'],10,2);

			$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
			$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
			$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
			
			$response['val_bpjsks_total_premi'] = ltrim($send_iso['bpjsks_total_premi'],0);
			$response['val_bpjsks_biaya_admin'] = ltrim($send_iso['bpjsks_biaya_admin'],0);
			$response['val_total_bayar'] = $response['val_bpjsks_total_premi']+$response['val_bpjsks_biaya_admin'];
			$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');

			$val_total_pembayaran = $response['val_total_bayar']-$response['val_komisi'];
			$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
			$response['total_pembayaran'] = 'Rp '.number_format($val_total_pembayaran,0,',','.');

			$response['bpjsks_total_premi'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_total_premi'],0),0,',','.');
			$response['bpjsks_biaya_admin'] = 'Rp '.number_format(ltrim($send_iso['bpjsks_biaya_admin'],0),0,',','.');
			$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
			$response['info'] = $send_iso['62'];
			$response['voucher_amount'] = ($detail_voucher->amount >= 1 ? $detail_voucher->amount : 0);

			$up['struk_tgl_lunas'] = $response['bpjsks_tgl_lunas'];
			$up['struk_no_ref'] = $response['bpjsks_jpa_refnum'];
			$up['struk_no_va_keluarga'] = $response['bpjsks_no_va_keluarga'];
			$up['struk_no_va_kepala_keluarga'] = $response['bpjsks_no_va_kepala_keluarga'];
			$up['struk_nama_peserta'] = $response['bpjsks_nama'];
			$up['struk_jml_anggota_keluarga'] = str_replace(' ','',ltrim($send_iso['bpjsks_jml_anggota_keluarga'],0));
			$up['struk_periode'] = ltrim($send_iso['periode'],0);
			$up['struk_jml_tagihan'] = $response['val_bpjsks_total_premi'];
			$up['struk_admin_bank'] = $response['val_bpjsks_biaya_admin'];
			$up['struk_total_bayar'] = $response['val_total_bayar'];
			$up['struk_total_pembayaran'] = $response['val_total_pembayaran'];
			$up['jpa_ref'] = $response['bpjsks_jpa_refnum'];
			$up['struk_info'] = $response['info'];

			$up['status'] = 'Clear';
			$update = DB::table('trans_bpjs')
				->where('id',$detail_transaksi->id)
				->update($up);

			/*email*/
			$view     = view('struk/struk_bpjs_kesehatan',$response)->render();
			$filename = "Struk-BPJS-Kesehatan-".$detail_transaksi->no_hp;
			$pdf      = App::make('dompdf.wrapper');

			$path = storage_path('app/uploads/'.$filename.'.pdf');

			$pdf->loadHTML($view);
			$pdf->setPaper('A4','landscape');
			$output = $pdf->output();

			file_put_contents($path, $output);

			$attachments = [$path];
			$email = $detail_transaksi->agen_email;
			if(!empty($email) || $detail_agen->notif_email != 'No') {
			    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_bpjs_kesehatan','attachments'=>$attachments]);
			}
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'Transaksi gagal, Mohon coba kembali';
	    	$response['id_transaksi']  = 0;
	    	$response['type_dialog']  = 'Error';
		}

		return response()->json($response);
	}

	public function postPlnTransaksiAdviceManual() {
		$id_agen = Request::get('id_agen');

		$arrays = DB::table('trans_pln')
			->where('id_agen',$id_agen)
			//->where('deleted_at','!=',NULL)
			->where('status','Advice Manual')
			->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'))
			->orderBy('id','DESC')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$date_tr = Esta::change_date_format($array->created_at,'d M Y');
			$rest['id'] = $array->id;
			$rest['datetime'] = Esta::change_date_format($array->created_at,'d M Y H:i');
			$rest['trans_no'] = $array->trans_no;
			$rest['amount'] = 'Rp '.number_format($array->trans_amount,0,',','.');
			$rest['stan'] = $array->stan;
			$rest['id_pelanggan'] = $array->no_meter;
			$rest['status'] = ($date_tr == date('d M Y') ? 'Pending' : 'Expired');
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postPulsaTransaksiPending() {
		$id_agen = Request::get('id_agen');

		$arrays = DB::table('trans_pulsa')
			->where('id_agen',$id_agen)
			//->where('deleted_at','!=',NULL)
			->where('status','Pending')
			->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'))
			->orderBy('id','DESC')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$date_tr = Esta::change_date_format($array->created_at,'d M Y');
			$rest['id'] = $array->id;
			$rest['datetime'] = $array->created_at;
			$rest['trans_no'] = $array->trans_no;
			$rest['no_hp'] = $array->no_hp;
			$rest['amount'] = 'Rp '.number_format($array->trans_amount,0,',','.');
			$rest['nama_operator'] = $array->product_kode_biller;
			$rest['nama_product'] = $array->product_nama;
			$rest['id_kode_biller'] = DB::table('pan_kode_biller')->where('nama',$array->product_kode_biller)->first()->kode;
			$rest['status'] = ($date_tr == date('d M Y') ? 'Pending' : 'Expired');
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postBpjsTransaksiPending() {
		$id_agen = Request::get('id_agen');

		$arrays = DB::table('trans_bpjs')
			->where('id_agen',$id_agen)
			//->where('deleted_at','!=',NULL)
			->where('status','Pending')
			->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'))
			->orderBy('id','DESC')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$date_tr = Esta::change_date_format($array->created_at,'d M Y');
			$rest['id'] = $array->id;
			$rest['datetime'] = Esta::change_date_format($array->created_at,'d M Y H:i');
			$rest['trans_no'] = $array->trans_no;
			$rest['id_pelanggan'] = $array->id_pelanggan;
			$rest['stan'] = $array->stan;
			$rest['amount'] = 'Rp '.number_format($array->trans_amount,0,',','.');
			$rest['jenis_layanan'] = 'BPJS Kesehatan';
			$rest['status'] = ($date_tr == date('d M Y') ? 'Pending' : 'Expired');
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postPdamTransaksiPending() {
		$id_agen = Request::get('id_agen');

		$arrays = DB::table('trans_pdam')
			->where('id_agen',$id_agen)
			//->where('deleted_at','!=',NULL)
			->where('status','Pending')
			->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'))
			->orderBy('id','DESC')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$date_tr = Esta::change_date_format($array->created_at,'d M Y');
			$rest['id'] = $array->id;
			$rest['datetime'] = $array->created_at;
			$rest['trans_no'] = $array->trans_no;
			$rest['stan'] = $array->stan;
			$rest['amount'] = 'Rp '.number_format($array->trans_amount,0,',','.');
			$rest['jenis_layanan'] = 'PDAM';
			$rest['nomor'] = $array->id_pelanggan;
			$rest['status'] = ($date_tr == date('d M Y') ? 'Pending' : 'Expired');
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postPlnPrabayarSubmit() {
		$no_hp = Request::get('no_hp');
		$id_product = Request::get('id_product');
		$id_transaksi = Request::get('id_transaksi');
		$no_meter = Request::get('no_meter');
		$pin = Request::get('pin');
		$id_agen = Request::get('id_agen');
		//$id_voucher = Request::get('id_voucher');
		$amount = Request::get('amount');
		$unsold = Request::get('unsold');
		$unsold_nominal = Request::get('unsold_nominal');

		$id_voucher_child = Request::get('id_voucher');
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$product_amount = ($unsold == 1 ? $unsold_nominal : $detail_product->amount);

		$detail_voucher = DB::table('voucher')
			->where('id',$id_voucher)
			->first();

		$kode = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));

		if($unsold == 1) {
			$detail_product_unsold_1 = DB::table('pan_product')->where('product_name','Token Unsold 1')->first();
			$voucher = DB::table('voucher')->where('id',$id_voucher)->first();
			$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product_unsold_1->komisi_basic : $detail_product_unsold_1->komisi_premium);

			$trans_amount = $product_amount+$detail_product_unsold_1->admin_edn+$detail_product_unsold_1->admin_1+$detail_product_unsold_1->admin_2+$detail_product_unsold_1->admin_3+$detail_product_unsold_1->margin-$komisi-$voucher->amount;
		} else {
			$trans_amount = Esta::amount_product_trans_pln_prabayar($id_product,$id_agen,$id_voucher,0);
		}

		/*echo $trans_amount.'-'.($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

		Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN Prabayar','Transaksi PLN Prabayar '.$detail_product->product_name,'Out','Transaksi');
		Esta::log_money($id_agen,($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),date('Y-m-d H:i:s'),'Komisi Transaksi PLN Prabayar','Komisi Transaksi PLN Prabayar '.$detail_product->product_name,'In','Komisi');
		exit();*/

		//if($detail_agen->saldo >= $product_amount) {	
			$sv['created_at'] = date('Y-m-d H:i:s');
			$sv['updated_at'] = date('Y-m-d H:i:s');
			$sv['created_user'] = Esta::user($id_agen);
			$sv['updated_user'] = Esta::user($id_agen);
			$sv['trans_no'] = $kode;
			$sv['ref_trans_no'] = $kode;
			$sv['trans_date'] = date('Y-m-d H:i:s');
			$sv['trans_desc'] = 'PLN PRABAYAR';
			$sv['currency'] = 'IDR';
			$sv['trans_amount'] = $product_amount;
			if($unsold == 1) {
				$sv['komisi'] = $komisi;
			} else {
				$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			}
			//$sv['create_user'] = $detail_agen->nama;
			$sv['id_agen'] = $id_agen;
			$sv['status'] = 'Pending';
			$sv['rekon_amount'] = 0;
			$sv['status_match'] = 'Waiting Rekon';
			$sv['no_hp'] = $no_hp;
			$sv['no_meter'] = $no_meter;
			$sv['id_product'] = $id_product;
			$sv['agen_nama'] = $detail_agen->nama;
			$sv['agen_email'] = $detail_agen->email;
			$sv['agen_kode'] = $detail_agen->kode;
			$sv['agen_no_hp'] = $detail_agen->no_hp;
			$sv['agen_level'] = $detail_agen->status_agen;
			$sv['agen_referall'] = $detail_agen->kode_referall_agen;
			$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
			$sv['agen_status_aktif'] = $detail_agen->status_aktif;
			$sv['agen_nik'] = $detail_agen->nik;
			$sv['agen_tgl_register'] = $detail_agen->tgl_register;
			$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
			$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
			$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
			$sv['agen_agama'] = $detail_agen->agama;
			$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
			$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
			$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
			$sv['agen_prov'] = $detail_agen->prov;
			$sv['agen_kab'] = $detail_agen->kab;
			$sv['agen_kec'] = $detail_agen->kec;
			$sv['agen_kel'] = $detail_agen->kel;
			$sv['product_nama'] = $detail_product->product_name;
			$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
			$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
			$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
			$sv['product_komisi_basic'] = $detail_product->komisi_basic;
			$sv['product_komisi_premium'] = $detail_product->komisi_premium;
			$sv['product_amount'] = $product_amount;
			$sv['product_margin'] = $detail_product->margin;
			$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
			$sv['product_biaya_admin_1'] = $detail_product->admin_1;
			$sv['product_biaya_admin_2'] = $detail_product->admin_2;
			$sv['product_biaya_Admin_3'] = $detail_product->admin_3;
			$sv['product_potongan'] = $detail_product->potongan;
			$sv['voucher_nama'] = $detail_voucher->nama;
			$sv['id_voucher'] = $detail_voucher->id;
			$sv['voucher_expired_date'] = $detail_voucher->expired_date;
			$sv['voucher_amount'] = $detail_voucher->amount;
			$sv['voucher_tagline'] = $detail_voucher->tagline;
			$sv['voucher_description'] = $detail_voucher->description;
			$sv['status_jatelindo'] = 'Pending';

			$save = DB::table('trans_pln')
				->insertGetId($sv);

			Esta::log_money($id_agen,($trans_amount == 0 ? $trans_amount : $trans_amount),date('Y-m-d H:i:s'),'Transaksi PLN Prabayar','Transaksi PLN Prabayar '.$detail_product->product_name,'Out','Transaksi','trans_pln',$save);
			Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Cashback Transaksi PLN Prabayar','Cashback Transaksi PLN Prabayar '.$detail_product->product_name,'In','Komisi','trans_pln',$save);

			/*send purchase*/
			$send_iso_purchase = Esta::send_iso_pln_prabayar($id_product,$no_meter,'0200',$id_transaksi,'Purchase','0',$unsold,$unsold_nominal);
			/*print_r($send_iso_purchase);
			exit();*/
			if($send_iso_purchase['39'] == 'Advice Manual' || $send_iso_purchase['39'] == '18' || $send_iso_purchase['39'] == '96') {

				$up['status'] = 'Advice Manual';
				$up['stan'] = $send_iso_purchase['stan'];

				$update = DB::table('trans_pln')
					->where('id',$save)
					->update($up);

				$response['api_status']  = 3;
		    	$response['api_message'] = 'Transaksi sedang diproses, klik menu ongoing untuk manual advice';
		    	$response['id_transaksi']  = 0;
		    	$response['type_dialog']  = 'Informasi';

		    	$response['pln_pra_meter_id'] = $send_iso_purchase['pln_pra_meter_id'];
		    	$response['pln_pra_id_pel'] = $send_iso_purchase['pln_pra_id_pel'];
				$response['pln_pra_nama'] = $send_iso_purchase['pln_pra_nama'];
				$response['pln_pra_tarif'] = str_replace(' ', '', $send_iso_purchase['pln_pra_tarif']);
				$response['pln_pra_kategori_daya'] = $send_iso_purchase['pln_pra_kategori_daya'];
				$response['pln_pra_ref_no'] = $send_iso_purchase['pln_pra_ref_no'];
				$response['pln_pra_biaya_materai'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['pln_pra_ppn'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['pln_pra_ppju'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['pln_pra_angsuran'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['pln_pra_rp_stroom'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
				$response['pln_pra_jml_kwh'] = substr(Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_jml_kwh'],0),$send_iso_purchase['pln_pra_nilai_minor_kwh']),0,-1);
				$response['pln_pra_token_number'] = chunk_split($send_iso_purchase['pln_pra_token_number'],4,' ');
				$response['pln_pra_biaya_admin'] = 'Rp '.number_format($detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan,0,',','.');

				$response['val_pln_pra_rp_stroom'] = ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0);
				$response['val_pln_pra_biaya_materai'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['val_pln_pra_ppn'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['val_pln_pra_ppju'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['val_pln_pra_angsuran'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['val_pln_pra_biaya_admin'] = $detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan;

				$response['pln_pra_pembelian_listrik'] = 'Rp '.Esta::nilai_minor($response['val_pln_pra_rp_stroom']+$response['val_pln_pra_biaya_admin'],$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
				$response['info'] = str_replace(array('TMP','#"TMP"#'), array('"TMP"','"TMP"'), $send_iso_purchase['62']);
				$response['val_komisi'] = $sv['komisi'];
				$response['komisi'] = 'Rp '.number_format($sv['komisi'],0,',','.');
				$response['pln_pra_tgl_lunas'] = substr($send_iso_purchase['pln_pra_tgl_lunas'],6,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],4,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],0,4).' '.substr($send_iso_purchase['pln_pra_tgl_lunas'],8,2).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],10,1).substr($send_iso_purchase['pln_pra_tgl_lunas'],11,1).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],12,2);
				//$response['nominal_total_pembayaran'] = 'Rp '.number_format($trans_amount, 0, ',', '.');
				$response['nominal_voucher'] = ($detail_voucher->amount <= 0 ? "" : 'Rp '.number_format($detail_voucher->amount,0,',','.'));
				$response['tipe_pembayaran'] = 'PLN Prepaid';

				$response['val_2_pln_pra_ppn'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['val_2_pln_pra_ppju'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['val_2_pln_pra_angsuran'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['val_2_pln_pra_biaya_materai'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['val_2_pln_pra_rp_stroom'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);

				$response['val_rp_bayar'] = $response['val_pln_pra_biaya_admin']+$product_amount;
				$response['rp_bayar'] = 'Rp '.number_format($response['val_rp_bayar'],0,',','.');
				$total_pembayaran = $response['val_rp_bayar']-$response['val_komisi']-$detail_voucher->amount;
				$response['total_pembayaran'] = 'Rp '.number_format(($total_pembayaran <= 0 ? 0 : $total_pembayaran),0,',','.');
				$response['pln_pra_biaya_admin'] = 'Rp '.number_format($response['val_pln_pra_biaya_admin'],0,',','.');

				/*struk*/
				$st['struk_id_pelanggan'] = $response['pln_pra_id_pel'];
				$st['no_meter'] = $response['pln_pra_meter_id'];
				$st['struk_info'] = $response['info'];
				$st['struk_nama_pelanggan'] = $response['pln_pra_nama'];
				$st['struk_tarif'] = $response['pln_pra_tarif'];
				$st['struk_daya'] = $response['pln_pra_kategori_daya'];
				$st['struk_rp_bayar'] = $response['val_rp_bayar'];
				$st['struk_materai'] = $response['val_2_pln_pra_biaya_materai'];
				$st['struk_ppn'] = $response['val_2_pln_pra_ppn'];
				$st['struk_ppj'] = $response['val_2_pln_pra_ppju'];
				$st['struk_angsuran'] = $response['val_2_pln_pra_angsuran'];
				$st['struk_rp_stroom'] = $response['val_2_pln_pra_rp_stroom'];
				$st['struk_jml_kwh'] = $response['pln_pra_jml_kwh'];
				$st['struk_token'] = $response['pln_pra_token_number'];
				$st['struk_admin_bank'] = $response['val_pln_pra_biaya_admin'];
				$st['struk_total_pembayaran'] = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
				$st['struk_tgl_lunas'] = $response['pln_pra_tgl_lunas'];
				$st['jpa_ref'] = $response['pln_pra_ref_no'];

				$up_struk = DB::table('trans_pln')
					->where('id',$save)
					->update($st);

		    	return response()->json($response);
		    	exit();
			}

			if($send_iso_purchase['39'] != '00') { /*jika respon error*/
				/*kembalikan saldo*/
				Esta::log_money($id_agen,($trans_amount == 0 ? $trans_amount : $trans_amount-$sv['komisi']),date('Y-m-d H:i:s'),'Transaksi PLN Prabayar','Return Transaksi PLN Prabayar '.$detail_product->product_name,'In','Transaksi','trans_pln',$save);
				Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Cashback Transaksi PLN Prabayar','Return Cashback Transaksi PLN Prabayar '.$detail_product->product_name,'In','Komisi','trans_pln',$save);

				$up['return_saldo'] = 'Yes';
				$up['status'] = 'Error';
				$up['error_code'] = $send_iso['39'];
				$up['stan'] = $send_iso_purchase['stan'];

				$update = DB::table('trans_pln')
					->where('id',$save)
					->update($up);


				$response['api_status']  = 2;
		    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_purchase['39'],$no_meter);
		    	$response['id_transaksi']  = 0;
		    	$response['type_dialog']  = 'Informasi';

		    	$response['pln_pra_id_pel'] = $send_iso_purchase['pln_pra_id_pel'];
				$response['pln_pra_nama'] = $send_iso_purchase['pln_pra_nama'];
				$response['pln_pra_tarif'] = str_replace(' ', '', $send_iso_purchase['pln_pra_tarif']);
				$response['pln_pra_kategori_daya'] = $send_iso_purchase['pln_pra_kategori_daya'];
				$response['pln_pra_ref_no'] = $send_iso_purchase['pln_pra_ref_no'];
				$response['pln_pra_biaya_materai'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['pln_pra_ppn'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['pln_pra_ppju'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['pln_pra_angsuran'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['pln_pra_rp_stroom'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
				$response['pln_pra_jml_kwh'] = substr(Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_jml_kwh'],0),$send_iso_purchase['pln_pra_nilai_minor_kwh']),0,-1);
				$response['pln_pra_token_number'] = chunk_split($send_iso_purchase['pln_pra_token_number'],4,' ');
				$response['pln_pra_biaya_admin'] = 'Rp '.number_format($detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan,0,',','.');

				$response['val_pln_pra_rp_stroom'] = ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0);
				$response['val_pln_pra_biaya_materai'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['val_pln_pra_ppn'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['val_pln_pra_ppju'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['val_pln_pra_angsuran'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['val_pln_pra_biaya_admin'] = $detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan;

				$response['pln_pra_pembelian_listrik'] = 'Rp '.Esta::nilai_minor($response['val_pln_pra_rp_stroom']+$response['val_pln_pra_biaya_admin'],$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
				$response['info'] = str_replace(array('TMP','#"TMP"#'), array('"TMP"','"TMP"'), $send_iso_purchase['62']);
				$response['val_komisi'] = $sv['komisi'];
				$response['komisi'] = 'Rp '.number_format($sv['komisi'],0,',','.');
				$response['pln_pra_tgl_lunas'] = substr($send_iso_purchase['pln_pra_tgl_lunas'],6,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],4,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],0,4).' '.substr($send_iso_purchase['pln_pra_tgl_lunas'],8,2).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],10,1).substr($send_iso_purchase['pln_pra_tgl_lunas'],11,1).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],12,2);
				//$response['nominal_total_pembayaran'] = 'Rp '.number_format($trans_amount, 0, ',', '.');
				$response['nominal_voucher'] = ($detail_voucher->amount <= 0 ? "" : 'Rp '.number_format($detail_voucher->amount,0,',','.'));
				$response['tipe_pembayaran'] = 'PLN Prepaid';

				$response['val_2_pln_pra_ppn'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['val_2_pln_pra_ppju'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['val_2_pln_pra_angsuran'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['val_2_pln_pra_biaya_materai'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['val_2_pln_pra_rp_stroom'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);

				$response['val_rp_bayar'] = $response['val_pln_pra_biaya_admin']+$product_amount;
				$response['rp_bayar'] = 'Rp '.number_format($response['val_rp_bayar'],0,',','.');
				$total_pembayaran = $response['val_rp_bayar']-$response['val_komisi']-$detail_voucher->amount;
				$response['total_pembayaran'] = 'Rp '.number_format(($total_pembayaran <= 0 ? 0 : $total_pembayaran),0,',','.');
				$response['pln_pra_biaya_admin'] = 'Rp '.number_format($response['val_pln_pra_biaya_admin'],0,',','.');

				/*struk*/
				$st['struk_id_pelanggan'] = $response['pln_pra_id_pel'];
				$st['no_meter'] = $response['pln_pra_meter_id'];
				$st['struk_info'] = $response['info'];
				$st['struk_nama_pelanggan'] = $response['pln_pra_nama'];
				$st['struk_tarif'] = $response['pln_pra_tarif'];
				$st['struk_daya'] = $response['pln_pra_kategori_daya'];
				$st['struk_rp_bayar'] = $response['val_rp_bayar'];
				$st['struk_materai'] = $response['val_2_pln_pra_biaya_materai'];
				$st['struk_ppn'] = $response['val_2_pln_pra_ppn'];
				$st['struk_ppj'] = $response['val_2_pln_pra_ppju'];
				$st['struk_angsuran'] = $response['val_2_pln_pra_angsuran'];
				$st['struk_rp_stroom'] = $response['val_2_pln_pra_rp_stroom'];
				$st['struk_jml_kwh'] = $response['pln_pra_jml_kwh'];
				$st['struk_token'] = $response['pln_pra_token_number'];
				$st['struk_admin_bank'] = $response['val_pln_pra_biaya_admin'];
				$st['struk_total_pembayaran'] = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
				$st['struk_tgl_lunas'] = $response['pln_pra_tgl_lunas'];
				$st['jpa_ref'] = $response['pln_pra_ref_no'];

				$up_struk = DB::table('trans_pln')
					->where('id',$save)
					->update($st);

		    	return response()->json($response);
		    	exit();
			}

			$up['status'] = 'Clear';
			$up['error_code'] = $send_iso['39'];
			$up['stan'] = $send_iso_purchase['stan'];

			$update = DB::table('trans_pln')
				->where('id',$save)
				->update($up);

			$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_pln');
			if($send_sms_transaksi == 'Yes') {
				$msg = CRUDBooster::getsetting('sukses_transaksi_pln');
				$msg = str_replace('[no_hp]', $no_hp, $msg);
				$msg = str_replace('[nama]', $detail_agen->nama, $msg);
				$msg = str_replace('[komisi]', $sv['komisi'], $msg);
				Esta::send_sms($no_hp, $msg);
			}

			if(!empty($id_voucher)) {
				$uv['used'] = 'Yes';

				$up_voucher = DB::table('trans_voucher_child')
					->where('id',$id_voucher_child)
					->where('id_agen',$id_agen)
					->update($uv);
			}

			$response['api_status']  = 1;
	    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_purchase['39'],$no_meter);
	    	$response['type_dialog']  = 'Error';
	    	$response['id_transaksi']  = $save;
	    	$response['id_log']  = $send_iso_purchase['id_log'];

	    	$response['pln_pra_meter_id'] = $send_iso_purchase['pln_pra_meter_id'];
	    	$response['pln_pra_id_pel'] = $send_iso_purchase['pln_pra_id_pel'];
			$response['pln_pra_nama'] = $send_iso_purchase['pln_pra_nama'];
			$response['pln_pra_tarif'] = str_replace(' ', '', $send_iso_purchase['pln_pra_tarif']);
			$response['pln_pra_kategori_daya'] = $send_iso_purchase['pln_pra_kategori_daya'];
			$response['pln_pra_ref_no'] = $send_iso_purchase['pln_pra_ref_no'];
			$response['pln_pra_biaya_materai'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['pln_pra_ppn'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['pln_pra_ppju'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['pln_pra_angsuran'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['pln_pra_rp_stroom'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
			$response['pln_pra_jml_kwh'] = substr(Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_jml_kwh'],0),$send_iso_purchase['pln_pra_nilai_minor_kwh']),0,-1);
			$response['pln_pra_token_number'] = chunk_split($send_iso_purchase['pln_pra_token_number'],4,' ');
			$response['pln_pra_biaya_admin'] = 'Rp '.number_format($detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan,0,',','.');

			$response['val_pln_pra_rp_stroom'] = ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0);
			$response['val_pln_pra_biaya_materai'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['val_pln_pra_ppn'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['val_pln_pra_ppju'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['val_pln_pra_angsuran'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['val_pln_pra_biaya_admin'] = $detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan;

			$response['pln_pra_pembelian_listrik'] = 'Rp '.Esta::nilai_minor($response['val_pln_pra_rp_stroom']+$response['val_pln_pra_biaya_admin'],$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
			$response['info'] = str_replace(array('TMP','#"TMP"#'), array('"TMP"','"TMP"'), $send_iso_purchase['62']);
			$response['val_komisi'] = $sv['komisi'];
			$response['komisi'] = 'Rp '.number_format($sv['komisi'],0,',','.');
			$response['pln_pra_tgl_lunas'] = substr($send_iso_purchase['pln_pra_tgl_lunas'],6,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],4,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],0,4).' '.substr($send_iso_purchase['pln_pra_tgl_lunas'],8,2).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],10,1).substr($send_iso_purchase['pln_pra_tgl_lunas'],11,1).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],12,2);
			//$response['nominal_total_pembayaran'] = 'Rp '.number_format($trans_amount, 0, ',', '.');
			$response['nominal_voucher'] = ($detail_voucher->amount <= 0 ? "" : 'Rp '.number_format($detail_voucher->amount,0,',','.'));
			$response['tipe_pembayaran'] = 'PLN Prepaid';

			$response['val_2_pln_pra_ppn'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['val_2_pln_pra_ppju'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['val_2_pln_pra_angsuran'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['val_2_pln_pra_biaya_materai'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['val_2_pln_pra_rp_stroom'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);

			$response['val_rp_bayar'] = $response['val_pln_pra_biaya_admin']+$product_amount;
			$response['rp_bayar'] = 'Rp '.number_format($response['val_rp_bayar'],0,',','.');
			$total_pembayaran = $response['val_rp_bayar']-$response['val_komisi']-$detail_voucher->amount;
			$response['total_pembayaran'] = 'Rp '.number_format(($total_pembayaran <= 0 ? 0 : $total_pembayaran),0,',','.');
			$response['pln_pra_biaya_admin'] = 'Rp '.number_format($response['val_pln_pra_biaya_admin'],0,',','.');

			/*struk*/
			$st['struk_id_pelanggan'] = $response['pln_pra_id_pel'];
			$st['no_meter'] = $response['pln_pra_meter_id'];
			$st['struk_info'] = $response['info'];
			$st['struk_nama_pelanggan'] = $response['pln_pra_nama'];
			$st['struk_tarif'] = $response['pln_pra_tarif'];
			$st['struk_daya'] = $response['pln_pra_kategori_daya'];
			$st['struk_rp_bayar'] = $response['val_rp_bayar'];
			$st['struk_materai'] = $response['val_2_pln_pra_biaya_materai'];
			$st['struk_ppn'] = $response['val_2_pln_pra_ppn'];
			$st['struk_ppj'] = $response['val_2_pln_pra_ppju'];
			$st['struk_angsuran'] = $response['val_2_pln_pra_angsuran'];
			$st['struk_rp_stroom'] = $response['val_2_pln_pra_rp_stroom'];
			$st['struk_jml_kwh'] = $response['pln_pra_jml_kwh'];
			$st['struk_token'] = $response['pln_pra_token_number'];
			$st['struk_admin_bank'] = $response['val_pln_pra_biaya_admin'];
			$st['struk_total_pembayaran'] = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
			$st['struk_tgl_lunas'] = $response['pln_pra_tgl_lunas'];
			$st['jpa_ref'] = $response['pln_pra_ref_no'];

			$up_struk = DB::table('trans_pln')
				->where('id',$save)
				->update($st);
			/*email*/

			$view     = view('struk/struk_pln_prepaid',$response)->render();
			$filename = "Struk-PLN-Prepaid-".$response['pln_pra_id_pel'];
			$pdf      = App::make('dompdf.wrapper');

			$path = storage_path('app/uploads/'.$filename.'.pdf');

			$pdf->loadHTML($view);
			$pdf->setPaper('A4','landscape');
			$output = $pdf->output();

			file_put_contents($path, $output);

			$attachments = [$path];
			$email = $detail_agen->email;
			if(!empty($email) || $detail_agen->notif_email != 'No') {
			    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pln_prepaid','attachments'=>$attachments]);
			}

			/*email*/
		
		/*} else {
			$response['api_status']  = 3;
	    	$response['api_message'] = 'Saldo anda tidak mencukupi';
	    	$response['id_transaksi']  = 0;
	    	$response['no_token']  = 0;
	    	$response['type_dialog']  = 'Informasi';
		}*/

		return response()->json($response);
	}

	public function postPlnProduk() {
		$id_agen = Request::get('id_agen');
		$agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$arrays = DB::table('pan_product')
			->where('id_tipe_layanan','4')
			->where('id_kode_layanan','1')
			->where('product_name','!=','Token Unsold 1')
			->where('product_name','!=','Token Unsold 2')
			->where('deleted_at',NULL)
			->orderBy('amount','asc')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->product_name;
			$rest['amount'] = Esta::amount_product($array->id);;
			if($agen->status_agen == 'Basic') {
				$rest['komisi'] = $array->komisi_basic;
			} else {
				$rest['komisi'] = $array->komisi_premium;
			}
			array_push($rest_json, $rest);
		
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postSms() {
		$no_hp = Request::get('no_hp');
		$kode = '123';
		$msg = 'Esta';

		echo Esta::send_sms($no_hp, $msg);
	}

	public function postPaketDataSubmit() {
		$no_hp = Request::get('no_hp');
		$id_product = Request::get('id_product');
		$id_voucher = Request::get('id_voucher');
		$pin = Request::get('pin');
		$id_agen = Request::get('id_agen');

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$sv['created_at'] = date('Y-m-d H:i:s');
		$sv['trans_no'] = 1;
		$sv['ref_trans_no'] = 1;
		$sv['trans_date'] = date('Y-m-d H:i:s');
		$sv['trans_desc'] = 'PULSA PRABAYAR';
		$sv['currency'] = 'IDR';
		$sv['trans_amount'] = $detail_product->amount;
		$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
		$sv['create_user'] = $detail_agen->nama;
		$sv['id_agen'] = $id_agen;
		$sv['status'] = '';
		$sv['no_hp'] = $no_hp;
		$sv['id_product'] = $id_product;

		$save = DB::table('trans_pulsa')
			->insertGetId($sv);

		if($save) {
			Esta::log_money($id_agen,$detail_product->amount,date('Y-m-d H:i:s'),'Transaksi Pulsa Prabayar','Transaksi Pulsa Prabayar '.$detail_product->product_name,'Out','Transaksi');
			Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi Pulsa Prabayar','Komisi Transaksi Pulsa Prabayar '.$detail_product->product_name,'Out','Komisi');
			$response['api_status']  = 1;
	    	$response['api_message'] = 'Transaksi pulsa berhasil';
	    	$response['id_transaksi']  = $save;
	    	$response['type_dialog']  = 'Informasi';
		} else {
			$response['api_status']  = 0;
	    	$response['api_message'] = 'Transaksi pulsa gagal';
	    	$response['id_transaksi']  = 0;
	    	$response['type_dialog']  = 'Error';
		}
		return response()->json($response);
	}

	public function postCheckTagihanPascabayarPulsa() {
		$no_hp = Request::get('no_hp');
		$id_agen = Request::get('id_agen');
		$id_product = Request::get('id_product');

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		/*send iso*/
		$send_iso = Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0200','Inquiry',0);

		if($send_iso['39'] == '00') {
			//'Rp '.number_format(ltrim($send_iso['jml_tagihan'],0));
			$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
			$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
			$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
			$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
			$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
			$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
			$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
			$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
			$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
			$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
			$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
			$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
			$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
			$rest['val_total_bayar'] = $send_iso['total_bayar'];
			$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
			$rest['id_transaksi'] = $send_iso['id_log'];

			$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
			$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
			$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
			$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
			$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
			$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');


			/*$rest['val_jml_tagihan'] = str_replace(' ', '',ltrim($send_iso['jml_tagihan'],0));
			$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$rest['val_biaya_admin'] = str_replace(' ', '',ltrim($send_iso['pls_psc_biaya_admin']+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan,0));
			$rest['biaya_admin'] = 'Rp '.number_format(ltrim($rest['val_biaya_admin'],0));
			$rest['jml_tagihan'] = $rest['val_jml_tagihan'];
			$rest['val_total_bayar'] = $rest['val_jml_tagihan']+$rest['val_biaya_admin'];
			$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
			$rest['blth'] = $send_iso['blth'];
			$rest['jml_bulan'] = $send_iso['pls_psc_total_bill'].' BLN';
			$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
			$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');*/

			$response['api_status']  = 1;
	    	$response['api_message'] = 'Berhasil';
	    	$response['type_dialog'] = 'Informasi';
	    	$response['item']  = $rest;
		} else {
			$rest['jml_tagihan'] = 0;
			$rest['komisi'] = 0;

			$response['api_status']  = 0;
	    	$response['api_message'] = Esta::show_error('Pulsa',$send_iso['39'],'0');;
	    	$response['type_dialog']  = 'Error';
	    	$response['item']  = $rest;
		}
		return response()->json($response);
	}

	public function postPulsaPascabayarSubmit() {
		$no_hp = Request::get('no_hp');
		$id_product = Request::get('id_product');
		//$id_voucher = Request::get('id_voucher');
		$pin = Request::get('pin');
		$id_agen = Request::get('id_agen');
		$amount = Request::get('amount');
		$id_transaksi = Request::get('id_transaksi');

		$id_voucher_child = Request::get('id_voucher');
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}

			$detail_product = DB::table('pan_product')
				->where('id',$id_product)
				->first();

			$detail_voucher = DB::table('voucher')
				->where('id',$id_voucher)
				->first();

			$kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));

			$detail_inquiry = DB::table('log_jatelindo_bit')
				->where('id',$id_transaksi)
				->where('type','Pulsa Postpaid')
				->where('status','Inquiry')
				->where('jenis','res')
				->first();

			$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);

			$biaya_admin = $detail_inquiry->pls_psc_biaya_admin;
			if($trans_amount == 0) {
				$trans_amount = 0;
			} else {
				$trans_amount = $trans_amount+$biaya_admin;
			}

			/*echo $trans_amount;
			exit();*/

			//if($detail_agen->saldo >= $trans_amount) {
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['updated_at'] = date('Y-m-d H:i:s');
				$sv['created_user'] = Esta::user($id_agen);
				$sv['updated_user'] = Esta::user($id_agen);
				$sv['trans_no'] = $kode;
				$sv['ref_trans_no'] = $kode;
				$sv['trans_date'] = date('Y-m-d H:i:s');
				$sv['trans_desc'] = 'PULSA PASCABAYAR';
				$sv['currency'] = 'IDR';
				$sv['trans_amount'] = $trans_amount;
				$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
				//$sv['create_user'] = $detail_agen->nama;
				$sv['id_agen'] = $id_agen;
				$sv['status'] = 'Pending';
				$sv['rekon_amount'] = 0;
				$sv['status_match'] = 'Waiting Rekon';
				$sv['no_hp'] = $no_hp;
				$sv['id_product'] = $id_product;
				$sv['agen_nama'] = $detail_agen->nama;
				$sv['agen_no_hp'] = $detail_agen->no_hp;
				$sv['agen_email'] = $detail_agen->email;
				$sv['agen_kode'] = $detail_agen->kode;
				$sv['agen_level'] = $detail_agen->status_agen;
				$sv['agen_referall'] = $detail_agen->kode_referall_agen;
				$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
				$sv['agen_status_aktif'] = $detail_agen->status_aktif;
				$sv['agen_nik'] = $detail_agen->nik;
				$sv['agen_tgl_register'] = $detail_agen->tgl_register;
				$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
				$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
				$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
				$sv['agen_agama'] = $detail_agen->agama;
				$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
				$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
				$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
				$sv['agen_prov'] = $detail_agen->prov;
				$sv['agen_kab'] = $detail_agen->kab;
				$sv['agen_kec'] = $detail_agen->kec;
				$sv['agen_kel'] = $detail_agen->kel;
				$sv['product_nama'] = $detail_product->product_name;
				$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
				$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
				$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
				$sv['product_komisi_basic'] = $detail_product->komisi_basic;
				$sv['product_komisi_premium'] = $detail_product->komisi_premium;
				$sv['product_amount'] = $amount;
				$sv['product_margin'] = $detail_product->margin;
				$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
				$sv['product_biaya_admin_1'] = $detail_product->admin_1;
				$sv['product_biaya_admin_2'] = $detail_product->admin_2;
				$sv['product_biaya_Admin_3'] = $detail_product->admin_3;
				$sv['product_potongan'] = $detail_product->potongan;
				$sv['voucher_nama'] = $detail_voucher->nama;
				$sv['voucher_expired_date'] = $detail_voucher->expired_date;
				$sv['voucher_amount'] = $detail_voucher->amount;
				$sv['voucher_tagline'] = $detail_voucher->tagline;
				$sv['voucher_description'] = $detail_voucher->description;
				$sv['id_voucher'] = $detail_voucher->id;

				$save = DB::table('trans_pulsa')
					->insertGetId($sv);

				if($save) {
					/*potong saldo*/
					Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi Pulsa Pascabayar','Transaksi Pulsa Pascabayar '.$detail_product->product_name,'Out','Transaksi','trans_pulsa',$save);
					Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi Pulsa Pascabayar','Komisi Transaksi Pulsa Pascabayar '.$detail_product->product_name,'In','Komisi','trans_pulsa',$save);

					/*send iso*/
					$send_iso = Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0200','Payment',$id_transaksi);

					if($send_iso['39'] != '97') {
						if($send_iso['status'] == 'Reversal' || $send_iso['status'] == 'Reversal Repeat' || $send_iso['status'] == 'Reversal Repeat 2' || $send_iso['status'] == 'Reversal Repeat 1') {


							if($send_iso['39'] == '00' || $send_iso['39'] == '94') { /*jika respon error*/
								/*kembalikan saldo*/
								Esta::log_money($id_agen,$sv['trans_amount'],date('Y-m-d H:i:s'),'Transaksi Pulsa Pascabayar','Return Transaksi Pulsa Pascabayar '.$detail_product->product_name,'In','Transaksi','trans_pulsa',$save);
								Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi Pulsa Pascabayar','Return Komisi Transaksi Pulsa Pascabayar '.$detail_product->product_name,'Out','Komisi','trans_pulsa',$save);

								$up['return_saldo'] = 'Yes';
								$up['status'] = 'Error';
								//$up['status_match'] = 'Error';
								$up['flag_reversal'] = 'Reversal Sukses';
								$up['error_code'] = $send_iso['39'];
								$up['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

								$update = DB::table('trans_pulsa')
									->where('id',$save)
									->update($up);

								$header_msg = $send_iso['header_msg'];

								$response['api_status']  = ($header_msg == 'Yes' ? '2' : '4');
						    	$response['api_message'] = ($header_msg == 'Yes' ?Esta::show_error('Pulsa Postpaid',$send_iso['39'],$no_meter) : ($send_iso['39'] == '96' ? 'Transaksi sedang diproses, Mohon hubungi customer service' : 'Transaksi Gagal, Mohon coba kembali'));
						    	$response['id_transaksi']  = 0;
						    	$response['type_dialog']  = ($header_msg == 'Yes' ? 'Informasi' : 'Error');

						    	$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
								$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
								$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
								$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
								$rest['pls_psc_no_hp'] = $no_hp;
								$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
								$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
								$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
								$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
								$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
								$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
								$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
								$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
								$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
								$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
								$rest['val_total_bayar'] = $send_iso['total_bayar'];
								$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
								$rest['id_transaksi'] = $send_iso['id_log'];

								$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
								$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
								$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
								$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
								$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
								$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
								$rest['biaya_admin'] = 'Rp '.number_format($rest['val_pls_psc_biaya_admin'],0,',','.');


								/*$response['api_status']  = 1;
						    	$response['api_message'] = 'Transaksi pulsa berhasil';
						    	$response['type_dialog']  = 'Informasi';
						    	$response['id_transaksi']  = $id_transaksi;*/
						    	$d = substr($send_iso['pls_psc_waktu_lunas'], 6,2);
						    	$m = substr($send_iso['pls_psc_waktu_lunas'], 4,2);
						    	$y = substr($send_iso['pls_psc_waktu_lunas'], 0,4);
						    	$h = substr($send_iso['pls_psc_waktu_lunas'], 8,2);
						    	$i = substr($send_iso['pls_psc_waktu_lunas'], 10,2);
						    	//$response['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;
						    	$rest['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;

						    	$up_struk['struk_tgl_lunas'] = $rest['pls_psc_waktu_lunas'];
						    	$up_struk['struk_nama_pelanggan'] = $rest['nama_pelanggan'];
						    	$up_struk['struk_tag_bln_1'] = $rest['pls_psc_blth_1'];
						    	$up_struk['struk_tag_bln_2'] = $rest['pls_psc_blth_2'];
						    	$up_struk['struk_tag_bln_3'] = $rest['pls_psc_blth_3'];
						    	$up_struk['struk_tag_amount_1'] = $rest['val_pls_psc_jml_tagihan_1'];
						    	$up_struk['struk_tag_amount_2'] = $rest['val_pls_psc_jml_tagihan_2'];
						    	$up_struk['struk_tag_amount_3'] = $rest['val_pls_psc_jml_tagihan_3'];
						    	$up_struk['struk_total_tagihan'] = $rest['val_total_tagihan'];
						    	$up_struk['struk_biaya_admin'] = $rest['val_pls_psc_biaya_admin'];
						    	$up_struk['struk_total_bayar'] = $rest['val_total_bayar'];
						    	$up_struk['struk_total_pembayaran'] = $rest['val_total_pembayaran'];
						    	$up_struk['struk_total_bill_tagihan'] = $rest['pls_psc_total_bill'];
						    	$up_struk['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

								$update = DB::table('trans_pulsa')
									->where('id',$save)
									->update($up_struk);

						    	return response()->json($response);
						    	exit();
							} else {
								$up3['status'] = 'Pending';
								$up3['stan'] = $send_iso['11'];
								$up3['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

								$update = DB::table('trans_pulsa')
									->where('id',$save)
									->update($up3);

								$response['api_status']  = '2';
						    	$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
						    	$response['id_transaksi']  = 0;
						    	$response['type_dialog']  = ($header_msg == 'Yes' ? 'Informasi' : 'Error');

						    	$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
								$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
								$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
								$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
								$rest['pls_psc_no_hp'] = $no_hp;
								$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
								$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
								$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
								$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
								$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
								$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
								$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
								$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
								$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
								$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
								$rest['val_total_bayar'] = $send_iso['total_bayar'];
								$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
								$rest['id_transaksi'] = $send_iso['id_log'];

								$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
								$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
								$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
								$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
								$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
								$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
								$rest['biaya_admin'] = 'Rp '.number_format($rest['val_pls_psc_biaya_admin'],0,',','.');


								/*$response['api_status']  = 1;
						    	$response['api_message'] = 'Transaksi pulsa berhasil';
						    	$response['type_dialog']  = 'Informasi';
						    	$response['id_transaksi']  = $id_transaksi;*/
						    	$d = substr($send_iso['pls_psc_waktu_lunas'], 6,2);
						    	$m = substr($send_iso['pls_psc_waktu_lunas'], 4,2);
						    	$y = substr($send_iso['pls_psc_waktu_lunas'], 0,4);
						    	$h = substr($send_iso['pls_psc_waktu_lunas'], 8,2);
						    	$i = substr($send_iso['pls_psc_waktu_lunas'], 10,2);
						    	//$response['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;
						    	$rest['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;

						    	$up_struk['struk_tgl_lunas'] = $rest['pls_psc_waktu_lunas'];
						    	$up_struk['struk_nama_pelanggan'] = $rest['nama_pelanggan'];
						    	$up_struk['struk_tag_bln_1'] = $rest['pls_psc_blth_1'];
						    	$up_struk['struk_tag_bln_2'] = $rest['pls_psc_blth_2'];
						    	$up_struk['struk_tag_bln_3'] = $rest['pls_psc_blth_3'];
						    	$up_struk['struk_tag_amount_1'] = $rest['val_pls_psc_jml_tagihan_1'];
						    	$up_struk['struk_tag_amount_2'] = $rest['val_pls_psc_jml_tagihan_2'];
						    	$up_struk['struk_tag_amount_3'] = $rest['val_pls_psc_jml_tagihan_3'];
						    	$up_struk['struk_total_tagihan'] = $rest['val_total_tagihan'];
						    	$up_struk['struk_biaya_admin'] = $rest['val_pls_psc_biaya_admin'];
						    	$up_struk['struk_total_bayar'] = $rest['val_total_bayar'];
						    	$up_struk['struk_total_pembayaran'] = $rest['val_total_pembayaran'];
						    	$up_struk['struk_total_bill_tagihan'] = $rest['pls_psc_total_bill'];
						    	$up_struk['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

								$update = DB::table('trans_pulsa')
									->where('id',$save)
									->update($up_struk);

						    	return response()->json($response);
							}
						}
					}


					if($send_iso['39'] != '97') {
						if($send_iso['39'] != '00' || $send_iso['status'] == 'Reversal' || $send_iso['status'] == 'Reversal Repeat' || $send_iso['status'] == 'Reversal Repeat 2' || $send_iso['status'] == 'Reversal Repeat 1') {


							if($send_iso['39'] == '00' || $send_iso['39'] == '94') { /*jika respon error*/
								$header_msg = $send_iso['header_msg'];
								/*kembalikan saldo*/
								Esta::log_money($id_agen,$sv['trans_amount'],date('Y-m-d H:i:s'),'Transaksi Pulsa Pascabayar','Return Transaksi Pulsa Pascabayar '.$detail_product->product_name,'In','Transaksi','trans_pulsa',$save);
								Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi Pulsa Pascabayar','Return Komisi Transaksi Pulsa Pascabayar '.$detail_product->product_name,'Out','Komisi','trans_pulsa',$save);

								$up['return_saldo'] = 'Yes';
								$up['status'] = 'Error';
								//$up['status_match'] = 'Error';
								$up['flag_reversal'] = 'Reversal Sukses';
								$up['error_code'] = $send_iso['39'];
								$up['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

								$update = DB::table('trans_pulsa')
									->where('id',$save)
									->update($up);


								$response['api_status']  = ($header_msg == 'Yes' ? '2' : '4');
						    	$response['api_message'] = ($header_msg == 'Yes' ?Esta::show_error('Pulsa Postpaid',$send_iso['39'],$no_meter) : ($send_iso['39'] == '96' ? 'Transaksi sedang diproses, Mohon hubungi customer service' : 'Transaksi Gagal'));
						    	$response['id_transaksi']  = 0;
						    	$response['type_dialog']  = ($header_msg == 'Yes' ? 'Informasi' : 'Error');

						    	$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
								$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
								$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
								$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
								$rest['pls_psc_no_hp'] = $no_hp;
								$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
								$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
								$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
								$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
								$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
								$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
								$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
								$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
								$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
								$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
								$rest['val_total_bayar'] = $send_iso['total_bayar'];
								$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
								$rest['id_transaksi'] = $send_iso['id_log'];

								$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
								$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
								$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
								$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
								$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
								$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
								$rest['biaya_admin'] = 'Rp '.number_format($rest['val_pls_psc_biaya_admin'],0,',','.');


								/*$response['api_status']  = 1;
						    	$response['api_message'] = 'Transaksi pulsa berhasil';
						    	$response['type_dialog']  = 'Informasi';
						    	$response['id_transaksi']  = $id_transaksi;*/
						    	$d = substr($send_iso['pls_psc_waktu_lunas'], 6,2);
						    	$m = substr($send_iso['pls_psc_waktu_lunas'], 4,2);
						    	$y = substr($send_iso['pls_psc_waktu_lunas'], 0,4);
						    	$h = substr($send_iso['pls_psc_waktu_lunas'], 8,2);
						    	$i = substr($send_iso['pls_psc_waktu_lunas'], 10,2);
						    	//$response['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;
						    	$rest['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;

						    	$up_struk['struk_tgl_lunas'] = $rest['pls_psc_waktu_lunas'];
						    	$up_struk['struk_nama_pelanggan'] = $rest['nama_pelanggan'];
						    	$up_struk['struk_tag_bln_1'] = $rest['pls_psc_blth_1'];
						    	$up_struk['struk_tag_bln_2'] = $rest['pls_psc_blth_2'];
						    	$up_struk['struk_tag_bln_3'] = $rest['pls_psc_blth_3'];
						    	$up_struk['struk_tag_amount_1'] = $rest['val_pls_psc_jml_tagihan_1'];
						    	$up_struk['struk_tag_amount_2'] = $rest['val_pls_psc_jml_tagihan_2'];
						    	$up_struk['struk_tag_amount_3'] = $rest['val_pls_psc_jml_tagihan_3'];
						    	$up_struk['struk_total_tagihan'] = $rest['val_total_tagihan'];
						    	$up_struk['struk_biaya_admin'] = $rest['val_pls_psc_biaya_admin'];
						    	$up_struk['struk_total_bayar'] = $rest['val_total_bayar'];
						    	$up_struk['struk_total_pembayaran'] = $rest['val_total_pembayaran'];
						    	$up_struk['struk_total_bill_tagihan'] = $rest['pls_psc_total_bill'];
						    	$up_struk['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

								$update = DB::table('trans_pulsa')
									->where('id',$save)
									->update($up_struk);

						    	return response()->json($response);
						    	exit();
							}
						}
					}

					if($send_iso['39'] == 'Pending' || $send_iso['39'] == '06' || $send_iso['39'] == '09' || $send_iso['39'] == '18' || $send_iso['39'] == '22') {
						$up3['status'] = 'Pending';
						$up3['stan'] = $send_iso['11'];
						$up3['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

						$update = DB::table('trans_pulsa')
							->where('id',$save)
							->update($up3);

						$response['api_status']  = 3;
				    	$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
				    	$response['type_dialog']  = 'Informasi';
				    	$response['id_transaksi']  = 0;

				    	$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
						$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
						$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
						$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
						$rest['pls_psc_no_hp'] = $no_hp;
						$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
						$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
						$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
						$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
						$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
						$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
						$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
						$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
						$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
						$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
						$rest['val_total_bayar'] = $send_iso['total_bayar'];
						$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
						$rest['id_transaksi'] = $send_iso['id_log'];

						$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
						$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
						$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
						$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
						$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
						$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
						$rest['biaya_admin'] = 'Rp '.number_format($rest['val_pls_psc_biaya_admin'],0,',','.');


						/*$response['api_status']  = 1;
				    	$response['api_message'] = 'Transaksi pulsa berhasil';
				    	$response['type_dialog']  = 'Informasi';
				    	$response['id_transaksi']  = $id_transaksi;*/
				    	$d = substr($send_iso['pls_psc_waktu_lunas'], 6,2);
				    	$m = substr($send_iso['pls_psc_waktu_lunas'], 4,2);
				    	$y = substr($send_iso['pls_psc_waktu_lunas'], 0,4);
				    	$h = substr($send_iso['pls_psc_waktu_lunas'], 8,2);
				    	$i = substr($send_iso['pls_psc_waktu_lunas'], 10,2);
				    	//$response['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;
				    	$rest['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;

				    	$up_struk['struk_tgl_lunas'] = $rest['pls_psc_waktu_lunas'];
				    	$up_struk['struk_nama_pelanggan'] = $rest['nama_pelanggan'];
				    	$up_struk['struk_tag_bln_1'] = $rest['pls_psc_blth_1'];
				    	$up_struk['struk_tag_bln_2'] = $rest['pls_psc_blth_2'];
				    	$up_struk['struk_tag_bln_3'] = $rest['pls_psc_blth_3'];
				    	$up_struk['struk_tag_amount_1'] = $rest['val_pls_psc_jml_tagihan_1'];
				    	$up_struk['struk_tag_amount_2'] = $rest['val_pls_psc_jml_tagihan_2'];
				    	$up_struk['struk_tag_amount_3'] = $rest['val_pls_psc_jml_tagihan_3'];
				    	$up_struk['struk_total_tagihan'] = $rest['val_total_tagihan'];
				    	$up_struk['struk_biaya_admin'] = $rest['val_pls_psc_biaya_admin'];
				    	$up_struk['struk_total_bayar'] = $rest['val_total_bayar'];
				    	$up_struk['struk_total_pembayaran'] = $rest['val_total_pembayaran'];
				    	$up_struk['struk_total_bill_tagihan'] = $rest['pls_psc_total_bill'];
				    	$up_struk['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

						$update = DB::table('trans_pulsa')
							->where('id',$save)
							->update($up_struk);

						return response()->json($response);
						exit();
					}

					$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_pulsa');
					if($send_sms_transaksi == 'Yes') {
						$msg = CRUDBooster::getsetting('sukses_transaksi_pulsa');
						$msg = str_replace('[no_hp]', $no_hp, $msg);
						$msg = str_replace('[nama]', $detail_agen->nama, $msg);
						$msg = str_replace('[komisi]', $sv['komisi'], $msg);
						Esta::send_sms($no_hp, $msg);
					}

					if(!empty($id_voucher)) {
						$uv['used'] = 'Yes';

						$up_voucher = DB::table('trans_voucher_child')
							->where('id',$id_voucher_child)
							->where('id_agen',$id_agen)
							->update($uv);
					}

					/*email*/
					$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
					$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
					$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
					$rest['pls_psc_no_hp'] = $no_hp;
					$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
					$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
					$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
					$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
					$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
					$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
					$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
					$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
					$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
					$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
					$rest['val_total_bayar'] = $send_iso['total_bayar'];
					$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
					$rest['id_transaksi'] = $send_iso['id_log'];

					$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
					$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
					$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
					$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
					$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
					$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
					$rest['biaya_admin'] = 'Rp '.number_format($rest['val_pls_psc_biaya_admin'],0,',','.');


					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi pulsa berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['id_transaksi']  = $id_transaksi;
			    	$d = substr($send_iso['pls_psc_waktu_lunas'], 6,2);
			    	$m = substr($send_iso['pls_psc_waktu_lunas'], 4,2);
			    	$y = substr($send_iso['pls_psc_waktu_lunas'], 0,4);
			    	$h = substr($send_iso['pls_psc_waktu_lunas'], 8,2);
			    	$i = substr($send_iso['pls_psc_waktu_lunas'], 10,2);
			    	$response['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;
			    	$rest['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;

			    	$up_struk['struk_tgl_lunas'] = $rest['pls_psc_waktu_lunas'];
			    	$up_struk['struk_nama_pelanggan'] = $rest['nama_pelanggan'];
			    	$up_struk['struk_tag_bln_1'] = $rest['pls_psc_blth_1'];
			    	$up_struk['struk_tag_bln_2'] = $rest['pls_psc_blth_2'];
			    	$up_struk['struk_tag_bln_3'] = $rest['pls_psc_blth_3'];
			    	$up_struk['struk_tag_amount_1'] = $rest['val_pls_psc_jml_tagihan_1'];
			    	$up_struk['struk_tag_amount_2'] = $rest['val_pls_psc_jml_tagihan_2'];
			    	$up_struk['struk_tag_amount_3'] = $rest['val_pls_psc_jml_tagihan_3'];
			    	$up_struk['struk_total_tagihan'] = $rest['val_total_tagihan'];
			    	$up_struk['struk_biaya_admin'] = $rest['val_pls_psc_biaya_admin'];
			    	$up_struk['struk_total_bayar'] = $rest['val_total_bayar'];
			    	$up_struk['struk_total_pembayaran'] = $rest['val_total_pembayaran'];
			    	$up_struk['struk_total_bill_tagihan'] = $rest['pls_psc_total_bill'];
			    	$up_struk['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];
			    	$up_struk['status'] = 'Clear';

					$update = DB::table('trans_pulsa')
						->where('id',$save)
						->update($up_struk);

					$view     = view('struk/struk_pulsa_postpaid',$rest)->render();
					$filename = "Struk-Pulsa-Postpaid-".$no_hp;
					$pdf      = App::make('dompdf.wrapper');

					$path = storage_path('app/uploads/'.$filename.'.pdf');

					$pdf->loadHTML($view);
					$pdf->setPaper('A4','landscape');
					$output = $pdf->output();

					file_put_contents($path, $output);

					$attachments = [$path];
					$email = $detail_agen->email;
					if(!empty($email)) {
					    Esta::kirimemail(['to'=>$email,'data'=>$rest,'template'=>'email_transaksi_pulsa_postpaid','attachments'=>$attachments]);
					}

					/*email*/
				} else {
					$response['api_status']  = 0;
			    	$response['api_message'] = 'Transaksi pulsa gagal';
			    	$response['id_transaksi']  = 0;
				}
			/*} else {
				$response['api_status']  = 3;
		    	$response['api_message'] = 'Saldo anda tidak mencukupi';
		    	$response['type_dialog']  = 'Error';
		    	$response['id_transaksi']  = 0;
			}*/

		return response()->json($response);
	}

	public function postKirimEmail() {
		$id_transaksi = Request::get('id_transaksi');
		$email = Request::get('email');
		$id_agen = Request::get('id_agen');
		$type = Request::get('type');
		$kategori = Request::get('kategori');

		switch ($type) {
			case 'pulsa_postpaid':
				$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
				$detail_jatelindo = DB::table('log_jatelindo_bit')
					->where('id_transaksi',$id_transaksi->id_transaksi)
					->where('type','Pulsa Postpaid')
					//->where('status','Purchase')
					->where('jenis','res')
					//->orWhere('status','Advice Manual')
					->orderBy('id','desc')
					->first();

				$detail_transaksi = DB::table('trans_pulsa')
					->where('trans_no',$detail_jatelindo->bit37)
					->first();

				$rest['pls_psc_waktu_lunas'] = $detail_transaksi->struk_tgl_lunas;
				$rest['pls_psc_no_hp'] = $detail_transaksi->no_hp;
				$rest['pls_psc_total_bill'] = $detail_transaksi->struk_total_bill_tagihan;
				$rest['nama_pelanggan'] = $detail_transaksi->struk_nama_pelanggan;
				$rest['pls_psc_blth_1'] = $detail_transaksi->struk_tag_bln_1;
				$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($detail_transaksi->struk_tag_amount_1,0,',','.');
				$rest['pls_psc_blth_2'] = $detail_transaksi->struk_tag_bln_2;
				$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($detail_transaksi->struk_tag_amount_2,0,',','.');
				$rest['pls_psc_blth_3'] = $detail_transaksi->struk_tag_bln_3;
				$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($detail_transaksi->struk_tag_amount_3,0,',','.');
				$rest['total_tagihan'] = 'Rp '.number_format($detail_transaksi->struk_total_tagihan,0,',','.');
				$rest['biaya_admin'] = 'Rp '.number_format($detail_transaksi->struk_biaya_admin,0,',','.');
				$rest['total_bayar'] = 'Rp '.number_format($detail_transaksi->struk_total_bayar,0,',','.');
				$rest['pls_psc_no_ref_switching'] = $detail_transaksi->jpa_ref;

				$view     = view('struk/struk_pulsa_postpaid',$rest)->render();
				$filename = "Struk-Pulsa-Postpaid-".$no_hp;
				$pdf      = App::make('dompdf.wrapper');

				$path = storage_path('app/uploads/'.$filename.'.pdf');

				$pdf->loadHTML($view);
				$pdf->setPaper('A4','landscape');
				$output = $pdf->output();

				file_put_contents($path, $output);

				$attachments = [$path];

				if(!empty($email)) {
				    Esta::kirimemail(['to'=>$email,'data'=>$rest,'template'=>'email_transaksi_pulsa_postpaid','attachments'=>$attachments]);
				}
				break;
			case 'pulsa_prepaid':
				/*$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
				$detail_jatelindo = DB::table('log_jatelindo_bit')
					->where('id_transaksi',$id_transaksi->id_transaksi)
					->where('type','Pulsa Prepaid')
					->where('jenis','res')
					->orderBy('id','desc')
					->first();*/
				/*print_r($id_transaksi);
				exit();*/

				$detail_transaksi = DB::table('trans_pulsa')
					->where('id',$id_transaksi)
					->first();

		    	$response['sn']  = $detail_transaksi->struk_sn;
		    	$response['tanggal']  = $detail_transaksi->struk_tgl_lunas;
		    	$response['product_nama']  = $detail_transaksi->product_nama;
		    	$response['product_amount_rp'] = 'Rp '.number_format($detail_transaksi->struk_total_bayar,0,',','.');
		    	$response['no_hp'] = $detail_transaksi->no_hp;


				$view     = view('struk/struk_pulsa_prepaid',$response)->render();
				$filename = "Struk-Pulsa-Prepaid-".$no_hp;
				$pdf      = App::make('dompdf.wrapper');

				$path = storage_path('app/uploads/'.$filename.'.pdf');

				$pdf->loadHTML($view);
				$pdf->setPaper('A4','landscape');
				$output = $pdf->output();

				file_put_contents($path, $output);

				$attachments = [$path];

				if(!empty($email) || $detail_agen->notif_email != 'No') {
				    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pulsa_prepaid','attachments'=>$attachments]);
				}
				break;
			case 'bpjs':
				$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
				$detail_jatelindo = DB::table('log_jatelindo_bit')
					->where('id_transaksi',$id_transaksi->id_transaksi)
					->where('type','BPJS Kesehatan')
					//->where('status','Purchase')
					->where('jenis','res')
					//->orWhere('status','Advice Manual')
					->orderBy('id','desc')
					->first();

				$detail_transaksi = DB::table('trans_bpjs')
					->where('trans_no',$detail_jatelindo->bit37)
					->first();

				$response['bpjsks_tgl_lunas'] = $detail_transaksi->struk_tgl_lunas;
				$response['bpjsks_jpa_refnum'] = $detail_transaksi->jpa_ref;
				$response['bpjsks_no_va_keluarga'] = $detail_transaksi->struk_no_va_keluarga;
				$response['bpjsks_no_va_kepala_keluarga'] = $detail_transaksi->struk_no_va_kepala_keluarga;
				$response['bpjsks_nama'] = $detail_transaksi->struk_nama_peserta;
				$response['bpjsks_jml_anggota_keluarga'] = $detail_transaksi->struk_jml_anggota_keluarga;
				$response['periode'] = $detail_transaksi->struk_periode;
				$response['bpjsks_total_premi'] = 'Rp '.number_format($detail_transaksi->struk_jml_tagihan,0,',','.');
				$response['bpjsks_biaya_admin'] = 'Rp '.number_format($detail_transaksi->struk_admin_bank,0,',','.');
				$response['total_bayar'] = 'Rp '.number_format($detail_transaksi->struk_total_bayar,0,',','.');
				$response['info'] = $detail_transaksi->struk_info;

				/*email*/
				$view     = view('struk/struk_bpjs_kesehatan',$response)->render();
				$filename = "Struk-BPJS-Kesehatan-".$no_hp;
				$pdf      = App::make('dompdf.wrapper');

				$path = storage_path('app/uploads/'.$filename.'.pdf');

				$pdf->loadHTML($view);
				$pdf->setPaper('A4','landscape');
				$output = $pdf->output();

				file_put_contents($path, $output);

				$attachments = [$path];

				if(!empty($email) || $detail_agen->notif_email != 'No') {
				    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_bpjs_kesehatan','attachments'=>$attachments]);
				}
				break;
			case 'pln':
			    switch ($kategori) {
			    	case 'Prepaid':
						//print_r($id_transaksi);
						//exit();
			    		$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
						$detail_jatelindo = DB::table('log_jatelindo_bit')
							->where('id_transaksi',$id_transaksi->id_transaksi)
							->where('type','PLN Prepaid')
							//->where('status','Purchase')
							->where('jenis','res')
							//->orWhere('status','Advice Manual')
							->orderBy('id','desc')
							->first();

						$detail_transaksi = DB::table('trans_pln')
							->where('trans_no',$detail_jatelindo->bit37)
							->first();


						$response['pln_pra_tgl_lunas'] = $detail_transaksi->struk_tgl_lunas;
						$response['pln_pra_meter_id'] = $detail_transaksi->no_meter;
						$response['pln_pra_id_pel'] = $detail_transaksi->struk_id_pelanggan;
						$response['pln_pra_nama'] = $detail_transaksi->struk_nama_pelanggan;
						$response['info'] = $detail_transaksi->struk_info;
						$response['pln_pra_tarif'] = $detail_transaksi->struk_tarif;
						$response['pln_pra_kategori_daya'] = $detail_transaksi->struk_daya;
						$response['pln_pra_ref_no'] = $detail_transaksi->jpa_ref;
						$response['rp_bayar'] = 'Rp '.number_format($detail_transaksi->struk_rp_bayar,0,',','.');
						$response['pln_pra_biaya_materai'] = 'Rp '.number_format($detail_transaksi->struk_materai,2,',','.');
						$response['pln_pra_ppn'] = 'Rp '.number_format($detail_transaksi->struk_ppn,2,',','.');
						$response['pln_pra_ppju'] = 'Rp '.number_format($detail_transaksi->struk_ppj,2,',','.');
						$response['pln_pra_angsuran'] = 'Rp '.number_format($detail_transaksi->struk_angsuran,2,',','.');
						$response['pln_pra_rp_stroom'] = 'Rp '.number_format($detail_transaksi->struk_rp_stroom,2,',','.');
						$response['pln_pra_jml_kwh'] = $detail_transaksi->struk_jml_kwh;
						$response['pln_pra_token_number'] = $detail_transaksi->struk_token;
						$response['pln_pra_biaya_admin'] = 'Rp '.number_format($detail_transaksi->struk_admin_bank,0,',','.');

						/*kirim email*/
			    		$view     = view('struk/struk_pln_prepaid',$response)->render();
						$filename = "Struk-PLN-Prepaid-".$response['pln_pra_id_pel'];
						$pdf      = App::make('dompdf.wrapper');

						$path = storage_path('app/uploads/'.$filename.'.pdf');

						$pdf->loadHTML($view);
						$pdf->setPaper('A4','landscape');
						$output = $pdf->output();

						file_put_contents($path, $output);

						$attachments = [$path];
						Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pln_prepaid','attachments'=>$attachments]);
			    		break;
			    	case 'Postpaid':
			    		/*$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
						$detail_jatelindo = DB::table('log_jatelindo_bit')
							->where('id_transaksi',$id_transaksi->id_transaksi)
							->where('type','PLN Postpaid')
							->where('status','Purchase')
							->where('jenis','res')
							->first();

						print_r($detail_jatelindo);
						exit();*/

						$detail_transaksi = DB::table('trans_pln')
							->where('id',$id_transaksi)
							->first();

						$response['pln_psc_tgl_lunas'] = $detail_transaksi->struk_tgl_lunas;
						$response['pln_psc_id_pel'] = $detail_transaksi->struk_id_pelanggan;
						$response['pln_psc_blth'] = $detail_transaksi->struk_blth;
						$response['pln_psc_nama'] = $detail_transaksi->struk_nama_pelanggan;
						$response['stand_meter'] = $detail_transaksi->struk_stand_meter;
						$response['pln_psc_tarif'] = $detail_transaksi->struk_tarif;
						$response['pln_psc_daya'] = $detail_transaksi->struk_daya;
						$response['rp_transaksi'] = 'Rp '.number_format($detail_transaksi->struk_rp_tag_pln,0,',','.');
						$response['pln_psc_no_ref'] = $detail_transaksi->jpa_ref;
						$response['pln_psc_biaya_admin'] = 'Rp '.number_format($detail_transaksi->struk_admin_bank,0,',','.');
						$response['total_bayar'] = 'Rp '.number_format($detail_transaksi->struk_total_bayar,0,',','.');
						$response['info'] = $detail_transaksi->struk_info;
						$response['pln_psc_jml_tunggakan'] = $detail_transaksi->struk_jml_tunggakan;

						/*kirim email*/
			    		$view     = view('struk/struk_pln_postpaid',$response)->render();
						$filename = "Struk-PLN-Postpaid-".$response['pln_psc_id_pel'];
						$pdf      = App::make('dompdf.wrapper');

						$path = storage_path('app/uploads/'.$filename.'.pdf');

						$pdf->loadHTML($view);
						$pdf->setPaper('A4','landscape');
						$output = $pdf->output();

						file_put_contents($path, $output);

						$attachments = [$path];
						Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pln_postpaid','attachments'=>$attachments]);
			    		break;
			    }

			    
				break;
			case 'pdam':
				$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
				$detail_jatelindo = DB::table('log_jatelindo_bit')
					->where('id_transaksi',$id_transaksi->id_transaksi)
					->where('type','PDAM Postpaid')
					//->where('status','Purchase')
					->where('jenis','res')
					//->orWhere('status','Advice Manual')
					->orderBy('id','desc')
					->first();

				$detail_transaksi = DB::table('trans_pdam')
					->where('trans_no',$detail_jatelindo->bit37)
					->first();

				$response['tgl_lunas'] = $detail_transaksi->struk_tgl_lunas;
				$response['pdam_idpel'] = $detail_transaksi->id_pelanggan;
				$response['pdam_blth'] = $detail_transaksi->struk_blth;
				$response['nama_pelanggan'] = $detail_transaksi->struk_nama_pelanggan;
				$response['pdam_no_ref_biller'] = $detail_transaksi->struk_jpa_ref;
				$response['tagihan'] = 'Rp '.number_format($detail_transaksi->struk_rp_tag,0,',','.');
				$response['biaya_admin'] = 'Rp '.number_format($detail_transaksi->struk_biaya_admin,0,',','.');
				$response['total_bayar'] = 'Rp '.number_format($detail_transaksi->struk_total_bayar,0,',','.');
				//$response['pdam_no_ref_biller'] = $detail_transaksi->jpa_ref;

				/*email*/
				$view     = view('struk/struk_pdam',$response)->render();
				$filename = "Struk-PDAM-".$no_hp;
				$pdf      = App::make('dompdf.wrapper');

				$path = storage_path('app/uploads/'.$filename.'.pdf');

				$pdf->loadHTML($view);
				$pdf->setPaper('A4','landscape');
				$output = $pdf->output();

				file_put_contents($path, $output);

				$attachments = [$path];
				//$email = $detail_agen->email;
				if(!empty($email)) {
				    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pdam','attachments'=>$attachments]);
				}
				break;
		}
		$response['api_status']  = 1;
	    $response['api_message'] = 'Email sukses terkirim';
	    $response['type_dialog']  = 'Informasi';

	  	return response()->json($response);
	}

	public function postVoucherPln() {
		$id_agen = Request::get('id_agen');

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$arrays = DB::table('trans_voucher_child')
			->whereIN('product',['PLN','All'])
			->where('id_agen',$id_agen)
			->where('used','No')
			->where('voucher_expired','>=',date('Y-m-d H:i:s'))
			->orderBy('id','desc')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->voucher_nama;
			$rest['amount'] = $array->voucher_amount;
			$rest['masa_berlaku'] = date("Y-m-d H:i:s", strtotime($array->voucher_expired));
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postVoucherPulsa() {
		$id_agen = Request::get('id_agen');

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$arrays = DB::table('trans_voucher_child')
			->whereIN('product',['Pulsa','All'])
			->where('id_agen',$id_agen)
			->where('used','No')
			->where('voucher_expired','>=',date('Y-m-d H:i:s'))
			->orderBy('id','desc')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->voucher_nama;
			$rest['amount'] = $array->voucher_amount;
			$rest['masa_berlaku'] = date("Y-m-d H:i:s", strtotime($array->voucher_expired));
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postProductPulsa() {
		$id_agen = Request::get('id_agen');
		$id_kode_biller = Request::get('id_kode_biller');
		$id_nama_operator = Request::get('id_nama_operator');
		$status = Request::get('status');
		$id_kode_layanan = Request::get('id_kode_layanan'); /*1 prabayar, 2 pasca bayar*/

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$arrays = DB::table('pan_product')
			->where('id_kode_biller',$id_kode_biller)
			->where('id_kode_layanan',$id_kode_layanan)
			->where('id_prefix_nama_operator',$id_nama_operator)
			->orderBy('amount','asc')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->product_name;
			$rest['amount'] = Esta::amount_product($array->id);
			$rest['komisi'] = ($detail_agen->status_agen == 'Basic' ? $array->komisi_basic : $array->komisi_premium);
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postPrefixOperator() {
		$status = Request::input('status');

		$prefixs = DB::table('prefix_operator')
			->join('pan_kode_biller', 'prefix_operator.id_kode_biller', '=', 'pan_kode_biller.id');
			if($status == 'Prabayar') {
				$prefixs = $prefixs->where('pan_kode_biller.nama','LIKE','%Prepaid%');
			} elseif($status == 'Pascabayar') {
				$prefixs = $prefixs->where('pan_kode_biller.nama','LIKE','%Postpaid%');
			}
			$prefixs = $prefixs
			->whereNull('prefix_operator.deleted_at')
			->get();

		$rest_prefix = array();
	  	foreach($prefixs as $prefix) {
			$rest['id'] = $prefix->id;
			$rest['id_kode_biller'] = $prefix->id_kode_biller;
			$rest['operator'] = DB::table('pan_kode_biller')->where('id',$prefix->id_kode_biller)->first()->nama;
			$rest['prefix'] = $prefix->prefix;
			$rest['id_nama_operator'] = ($prefix->id_nama_operator >= 1 ? $prefix->id_nama_operator : 0);
			array_push($rest_prefix, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_prefix;

	  	return response()->json($response);
	}

	public function postIsoPulsa() {
		$id_product = Request::input('id_product');
		$no_hp = Request::get('no_hp');

		$iso = Esta::send_iso_pulsa($id_product,$no_hp);
		if($iso == '06') {
			$response = 'sukses';
		} else {
			$response = 'gagal';
		}
		return $response;
	}

	public function postPulsaPrabayarSubmit() {
		$no_hp = Request::get('no_hp');
		$id_product = Request::get('id_product');
		$pin = Request::get('pin');
		$id_agen = Request::get('id_agen');
		$id_voucher_child = Request::get('id_voucher');
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}

			$detail_product = DB::table('pan_product')
				->where('id',$id_product)
				->first();

			$detail_voucher = DB::table('voucher')
				->where('id',$id_voucher)
				->first();

			$kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));

			$trans_amount = Esta::amount_product_trans($id_product,$id_agen,$id_voucher)-($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			if($trans_amount <= 0) {
				$trans_amount = 0;
			} else {
				$trans_amount = $trans_amount;
			}

			//if($detail_agen->saldo >= $trans_amount) {
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['updated_at'] = date('Y-m-d H:i:s');
				$sv['created_user'] = Esta::user($id_agen);
				$sv['updated_user'] = Esta::user($id_agen);
				$sv['trans_no'] = $kode;
				$sv['ref_trans_no'] = $kode;
				$sv['trans_date'] = date('Y-m-d H:i:s');
				$sv['trans_desc'] = 'PULSA PRABAYAR';
				$sv['currency'] = 'IDR';
				$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
				$sv['trans_amount'] = $trans_amount;
				//$sv['create_user'] = $detail_agen->nama;
				$sv['id_agen'] = $id_agen;
				$sv['status'] = 'Pending';
				$sv['rekon_amount'] = 0;
				$sv['status_match'] = 'Waiting Rekon';
				$sv['no_hp'] = $no_hp;
				$sv['id_product'] = $id_product;
				$sv['agen_nama'] = $detail_agen->nama;
				$sv['agen_no_hp'] = $detail_agen->no_hp;
				$sv['agen_email'] = $detail_agen->email;
				$sv['agen_kode'] = $detail_agen->kode;
				$sv['agen_level'] = $detail_agen->status_agen;
				$sv['agen_referall'] = $detail_agen->kode_referall_agen;
				$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
				$sv['agen_status_aktif'] = $detail_agen->status_aktif;
				$sv['agen_nik'] = $detail_agen->nik;
				$sv['agen_tgl_register'] = $detail_agen->tgl_register;
				$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
				$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
				$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
				$sv['agen_agama'] = $detail_agen->agama;
				$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
				$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
				$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
				$sv['agen_prov'] = $detail_agen->prov;
				$sv['agen_kab'] = $detail_agen->kab;
				$sv['agen_kec'] = $detail_agen->kec;
				$sv['agen_kel'] = $detail_agen->kel;
				$sv['product_nama'] = $detail_product->product_name;
				$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
				$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
				$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
				$sv['product_komisi_basic'] = $detail_product->komisi_basic;
				$sv['product_komisi_premium'] = $detail_product->komisi_premium;
				$sv['product_amount'] = $detail_product->amount;
				$sv['product_margin'] = $detail_product->margin;
				$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
				$sv['product_biaya_admin_1'] = $detail_product->admin_1;
				$sv['product_biaya_admin_2'] = $detail_product->admin_2;
				$sv['product_biaya_Admin_3'] = $detail_product->admin_3;
				$sv['product_potongan'] = $detail_product->potongan;
				$sv['voucher_nama'] = $detail_voucher->nama;
				$sv['voucher_expired_date'] = $detail_voucher->expired_date;
				$sv['voucher_amount'] = $detail_voucher->amount;
				$sv['id_voucher'] = $detail_voucher->id;
				$sv['voucher_tagline'] = $detail_voucher->tagline;
				$sv['voucher_description'] = $detail_voucher->description;

				$save = DB::table('trans_pulsa')
					->insertGetId($sv);

				if($save) {
					/*potong saldo*/
					Esta::log_money($id_agen,$sv['trans_amount'],date('Y-m-d H:i:s'),'Transaksi Pulsa Prabayar','Transaksi Pulsa Prabayar '.$detail_product->product_name,'Out','Transaksi','trans_pulsa',$save);
					Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Cashback Transaksi Pulsa Prabayar','Cashback Transaksi Pulsa Prabayar '.$detail_product->product_name,'In','Komisi','trans_pulsa',$save);

					/*send iso*/
					$send_iso = Esta::send_iso_pulsa_prabayar($id_product,$no_hp,'0200','Purchase',$save);

					if($send_iso['39'] == 'Pending' || $send_iso['39'] == '06' || $send_iso['39'] == '09' || $send_iso['39'] == '18' || $send_iso['39'] == '22') {
						$up3['status'] = 'Pending';
						$up['jpa_ref'] = $send_iso['no_ref'];

						$update = DB::table('trans_pulsa')
							->where('id',$save)
							->update($up3);

						$response['api_status']  = 3;
				    	$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
				    	$response['type_dialog']  = 'Informasi';
				    	$response['id_transaksi']  = 0;

				    	$response['sn']  = $send_iso['sn'];
				    	$response['pls_psc_no_ref_switching']  = $send_iso['no_ref'];
				    	$response['tanggal']  = $send_iso['tanggal'];
				    	$response['id_transaksi']  = $save;
				    	$response['product_amount']  = Esta::amount_product($id_product);
				    	$response['product_amount_rp']  = 'Rp '.number_format(Esta::amount_product($id_product),0,',','.');
				    	$response['komisi'] = $sv['komisi'];
				    	$response['product_nama'] = $sv['product_nama'];
				    	$response['no_hp'] = $no_hp;
				    	$response['voucher_amount'] = $detail_voucher->amount;
				    	$total_pembayaran = $response['product_amount']-$response['komisi']-$response['voucher_amount'];
				    	$response['total_pembayaran'] = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);

				    	$up_struk['struk_tgl_lunas'] = $response['tanggal'];
				    	$up_struk['struk_sn'] = $response['sn'];
				    	$up_struk['jpa_ref'] = $send_iso['no_ref'];
				    	$up_struk['struk_total_bayar'] = $response['product_amount'];
				    	$up_struk['struk_total_pembayaran'] = $response['total_pembayaran'];

						$update = DB::table('trans_pulsa')
							->where('id',$save)
							->update($up_struk);

						return response()->json($response);
						exit();
					}
					
					if($send_iso['39'] != '00') { /*jika respon error*/
						/*kembalikan saldo*/
						if($send_iso['39'] != '18' || $send_iso['39'] != '06' || $send_iso['39'] != '09') {
							Esta::log_money($id_agen,$sv['trans_amount'],date('Y-m-d H:i:s'),'Transaksi Pulsa Prabayar','Return Transaksi Pulsa Prabayar '.$detail_product->product_name,'In','Transaksi','trans_pulsa',$save);
							Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Cashback Transaksi Pulsa Prabayar','Return Cashback Transaksi Pulsa Prabayar '.$detail_product->product_name,'Out','Komisi','trans_pulsa',$save);
							$up['return_saldo'] = 'Yes';
						}
						$up['status'] = 'Error';
						//$up['status_match'] = 'Error';
						$up['error_code'] = $send_iso['39'];
						$up['jpa_ref'] = $send_iso['no_ref'];

						$update = DB::table('trans_pulsa')
							->where('id',$save)
							->update($up);

						$response['api_status']  = 2;
				    	$response['api_message'] = Esta::show_error('Pulsa',$send_iso['39'],'0');
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;

				    	$response['sn']  = $send_iso['sn'];
				    	$response['pls_psc_no_ref_switching']  = $send_iso['no_ref'];
				    	$response['tanggal']  = $send_iso['tanggal'];
				    	$response['id_transaksi']  = $save;
				    	$response['product_amount']  = Esta::amount_product($id_product);
				    	$response['product_amount_rp']  = 'Rp '.number_format(Esta::amount_product($id_product),0,',','.');
				    	$response['komisi'] = $sv['komisi'];
				    	$response['product_nama'] = $sv['product_nama'];
				    	$response['no_hp'] = $no_hp;
				    	$response['voucher_amount'] = $detail_voucher->amount;
				    	$total_pembayaran = $response['product_amount']-$response['komisi']-$response['voucher_amount'];
				    	$response['total_pembayaran'] = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);

				    	$up_struk['struk_tgl_lunas'] = $response['tanggal'];
				    	$up_struk['struk_sn'] = $response['sn'];
				    	$up_struk['jpa_ref'] = $send_iso['no_ref'];
				    	$up_struk['struk_total_bayar'] = $response['product_amount'];
				    	$up_struk['struk_total_pembayaran'] = $response['total_pembayaran'];

						$update = DB::table('trans_pulsa')
							->where('id',$save)
							->update($up_struk);

				    	return response()->json($response);
				    	exit();
					}

					$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_pulsa');
					if($send_sms_transaksi == 'Yes') {
						$msg = CRUDBooster::getsetting('sukses_transaksi_pulsa');
						$msg = str_replace('[no_hp]', $no_hp, $msg);
						$msg = str_replace('[nama]', $detail_agen->nama, $msg);
						$msg = str_replace('[komisi]', $sv['komisi'], $msg);
						Esta::send_sms($no_hp, $msg);
					}

					if(!empty($id_voucher)) {
						$uv['used'] = 'Yes';

						$up_voucher = DB::table('trans_voucher_child')
							->where('id',$id_voucher_child)
							->where('id_agen',$id_agen)
							->update($uv);
					}

					$up2['status'] = 'Clear';

					$update = DB::table('trans_pulsa')
						->where('id',$save)
						->update($up2);

					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi pulsa berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['sn']  = $send_iso['sn'];
			    	$response['pls_psc_no_ref_switching']  = $send_iso['no_ref'];
			    	$response['tanggal']  = $send_iso['tanggal'];
			    	$response['id_transaksi']  = $save;
			    	$response['product_amount']  = Esta::amount_product($id_product);
			    	$response['product_amount_rp']  = 'Rp '.number_format(Esta::amount_product($id_product),0,',','.');
			    	$response['komisi'] = $sv['komisi'];
			    	$response['product_nama'] = $sv['product_nama'];
			    	$response['no_hp'] = $no_hp;
			    	$response['voucher_amount'] = $detail_voucher->amount;
			    	$total_pembayaran = $response['product_amount']-$response['komisi']-$response['voucher_amount'];
			    	$response['total_pembayaran'] = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);

			    	$up_struk['struk_tgl_lunas'] = $response['tanggal'];
			    	$up_struk['struk_sn'] = $response['sn'];
			    	$up_struk['jpa_ref'] = $send_iso['no_ref'];
			    	$up_struk['struk_total_bayar'] = $response['product_amount'];
			    	$up_struk['struk_total_pembayaran'] = $response['total_pembayaran'];

					$update = DB::table('trans_pulsa')
						->where('id',$save)
						->update($up_struk);

					$view     = view('struk/struk_pulsa_prepaid',$response)->render();
					$filename = "Struk-Pulsa-Prepaid-".$no_hp;
					$pdf      = App::make('dompdf.wrapper');

					$path = storage_path('app/uploads/'.$filename.'.pdf');

					$pdf->loadHTML($view);
					$pdf->setPaper('A4','landscape');
					$output = $pdf->output();

					file_put_contents($path, $output);

					$attachments = [$path];
					$email = $detail_agen->email;
					if(!empty($email) || $detail_agen->notif_email != 'No') {
					    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pulsa_prepaid','attachments'=>$attachments]);
					}
				} else {
					$response['api_status']  = 0;
			    	$response['api_message'] = 'Transaksi pulsa gagal';
			    	$response['type_dialog']  = 'Error';
			    	$response['id_transaksi']  = 0;
				}
			/*} else {
				$response['api_status']  = 3;
		    	$response['api_message'] = 'Saldo anda tidak mencukupi';
		    	$response['type_dialog']  = 'Error';
		    	$response['id_transaksi']  = 0;
			}*/

		return response()->json($response);
	}

	public function postPrivacyPolicy() {
		$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['content'] = CRUDBooster::getsetting('syarat_dan_ketentuan');

	    return response()->json($response);
	}

	public function postResendOtp() {
		$id_agen = Request::get('id_agen');
		$no_hp_baru = Request::get('no_hp_baru');
		$type = Request::get('type');
		$kode_otp = rand(11,99).date('s');

		$detail = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$msg = str_replace('[kode]', $kode_otp, CRUDBooster::getsetting('otp_registrasi'));
		if($type = 'update') {
			//echo Esta::send_sms($no_hp_baru,'EstaKios - '.$detail->kode_otp);
			Esta::send_sms($detail->no_hp,$msg);
		} else {
			Esta::send_sms($detail->no_hp,$msg);
		}
		//$up['tgl_otp_terkirim'] = date('Y-m-d H:i:s');
		$up['kode_otp'] = $kode_otp;
		$up['updated_user'] = Esta::user($id_agen);
		$update = DB::table('agen')
			->where('id',$id_agen)
			->update($up);

		$response['api_status']  = 1;
	    $response['api_message'] = 'Kode verifikasi berhasil dikirim ulang';
	    $response['kode_otp']  = $kode_otp;
	    $response['type_dialog']  = 'Informasi';
		
	    return response()->json($response);
	}

	public function postTarikTunai() {
		$id_agen = Request::get('id_agen');
		$tanpa_biaya_admin = CRUDBooster::getsetting('tarik_tunai_tanpa_biaya_admin');
		$biaya_admin = CRUDBooster::getsetting('biaya_admin_tarik_tunai');
		$minimal_tarik_tunai = CRUDBooster::getsetting('minimal_tarik_tunai');

		$detail = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$detail_bank = DB::table('bank')
			->where('id',$detail->rek_id_bank)
			->first();

		$sv['id_agen'] = $id_agen;
		$sv['id_bank'] = $detail->id_bank;
		$sv['no_rek'] = $detail->rek_no;
		$sv['bank_nama'] = $detail_bank->nama;
		$sv['bank_kode'] = $detail_bank->kode_bank;
		$sv['nama_akun_bank'] = $detail->rek_nama;
		$sv['jml_tarik_tunai'] = Request::get('nominal');
		$biaya_admin = ($sv['jml_tarik_tunai'] < $tanpa_biaya_admin ? $biaya_admin : 0);
		$sv['biaya_admin'] = $biaya_admin;
		$sv['trans_amount'] = $sv['jml_tarik_tunai'];

		$kode = Esta::nomor_transaksi('trans_tarik_tunai',CRUDBooster::getsetting('transaksi_tarik_tunai'));

		//if($sv['jml_tarik_tunai'] > $minimal_tarik_tunai) {
			if($detail->saldo >= $sv['trans_amount']+$sv['biaya_admin']) {
				$sv['trans_no'] = $kode;
				$sv['created_user'] = Esta::user($id_agen);
				$sv['updated_user'] = Esta::user($id_agen);
				$sv['ref_trans_no'] = $kode;
				$sv['trans_desc'] = 'TARIK TUNAI';
				//$sv['status'] = 'Pending';
				//$sv['create_user'] = $detail->nama;
				$sv['created_at'] = date('Y-m-d H:i:s');
				$sv['updated_at'] = date('Y-m-d H:i:s');
				$sv['trans_date'] = date('Y-m-d H:i:s');
				$sv['agen_nama'] = $detail->nama;
				$sv['agen_email'] = $detail->email;
				$sv['agen_kode'] = $detail->kode;
				$sv['agen_level'] = $detail->status_agen;
				$sv['agen_referall'] = $detail->kode_referall_agen;
				$sv['agen_referall_relation'] = $detail->kode_relation_referall;
				$sv['agen_status_aktif'] = $detail->status_aktif;
				$sv['agen_nik'] = $detail->nik;
				$sv['agen_tgl_register'] = $detail->tgl_register;
				$sv['agen_tempat_lahir'] = $detail->tempat_lahir;
				$sv['agen_tgl_lahir'] = $detail->tgl_lahir;
				$sv['agen_jenis_kelamin'] = $detail->jenis_kelamin;
				$sv['agen_agama'] = $detail->agama;
				$sv['agen_no_hp'] = $detail->no_hp;
				$sv['agen_status_perkawinan'] = $detail->status_perkawinan;
				$sv['agen_pekerjaan'] = $detail->pekerjaan;
				$sv['agen_kewarganegaraan'] = $detail->kewarganegaraan;
				$sv['agen_prov'] = $detail->prov;
				$sv['agen_kab'] = $detail->kab;
				$sv['agen_kec'] = $detail->kec;
				$sv['agen_kel'] = $detail->kel;

				$save = DB::table('trans_tarik_tunai')
					->insertGetId($sv);
				if($save){
					Esta::log_money($id_agen,$sv['jml_tarik_tunai']+$sv['biaya_admin'],date('Y-m-d H:i:s'),'Tarik Tunai','Request tarik tunai','Out','Transaksi','trans_tarik_tunai',$save);
					Esta::log_money($id_agen,0,date('Y-m-d H:i:s'),'Agen Tarik Tunai','Agen Tarik Tunai '.$save_trans['trans_no'],'','Riwayat Agen','trans_tarik_tunai',$save);
		    		$response['api_status']  = 1;
		        	$response['api_message'] = 'Permintaan Tarik Tunai Anda akan diproses dalam 1x24 Jam';
		        	$response['type_dialog']  = 'Informasi';
		        	$response['saldo']  = DB::table('agen')->where('id',$id_agen)->first()->saldo;
				} else {
					$response['api_status']  = 0;
			        $response['api_message'] = 'Gagal request tarik dana';
			        $response['type_dialog']  = 'Error';
				}
			} else {
				$response['api_status']  = 2;
			    $response['api_message'] = 'Saldo anda tidak mencukupi';
			    $response['type_dialog']  = 'Error';
			}
		/*} else {
			$response['api_status']  = 3;
		    $response['api_message'] = 'Minimal tarik tunai Rp '.number_format($minimal_tarik_tunai,0,',','.');
		    $response['type_dialog']  = 'Error';
		}*/
	    return response()->json($response);

	}

	public function postShowPremium() {
		$id_agen = Request::get('id_agen');
		$detail = DB::table('agen_verify_premium')
	        ->where('id_agen',$id_agen)
	        ->first();
	    if(!empty($detail)) {
	    	$item['foto_ktp'] = asset('').$detail->foto_ktp;
	    	$item['foto_ktp_status'] = (!empty($detail->foto_ktp_status) ? $detail->foto_ktp_status : 'Pastikan keseluruhan KTP terlihat jelas');
	    	$item['foto_ktp_note'] = ($detail->foto_ktp_status == 'Approved' ? '' : (!empty($detail->foto_ktp_note) ? $detail->foto_ktp_note : 'Pastikan keseluruhan KTP terlihat jelas'));
	    	$item['foto_rekening'] = asset('').$detail->foto_rekening;
	    	$item['foto_rekening_status'] = (!empty($detail->foto_rekening_status) ? $detail->foto_rekening_status : 'Pastikan keseluruhan buku rekening terlihat jelas');
	    	$item['foto_rekening_note'] = ($detail->foto_rekening_status == 'Approved' ? '' : (!empty($detail->foto_rekening_note) ? $detail->foto_rekening_note : 'Pastikan keseluruhan buku rekening terlihat jelas'));
	    	$item['foto_selfie'] = asset('').$detail->foto_selfie;
	    	$item['foto_selfie_status'] = (!empty($detail->foto_selfie_status) ? $detail->foto_selfie_status : 'Perlihatkan wajah dan KTP saat foto');
	    	$item['foto_selfie_note'] = ($detail->foto_selfie_status == 'Approved' ? '' : (!empty($detail->foto_selfie_note) ? $detail->foto_selfie_note : 'Perlihatkan wajah dan KTP saat foto'));

	    	$response['api_status']  = 1;
	        $response['api_message'] = 'Sudah ada request premium';
	        $response['type_dialog']  = 'Informasi';
	        $response['item'] = $item;
	    } else {
	    	$item['foto_ktp'] = '';
	    	$item['foto_ktp_status'] = '';
	    	$item['foto_ktp_note'] = 'Pastikan keseluruhan KTP terlihat jelas';
	    	$item['foto_rekening'] = '';
	    	$item['foto_rekening_status'] = '';
	    	$item['foto_rekening_note'] = 'Pastikan keseluruhan buku rekening terlihat jelas';
	    	$item['foto_selfie'] = '';
	    	$item['foto_selfie_status'] = '';
	    	$item['foto_selfie_note'] = 'Perlihatkan wajah dan KTP saat foto';

	    	$response['api_status']  = 0;
	        $response['api_message'] = 'Belum ada request premium';
	        $response['type_dialog']  = 'Informasi';
	        $response['item'] = $item;
		}
	    return response()->json($response);

	}

	public function postSubmitPremium() {
		$id_agen = Request::get('id_agen');
		$ktp = Request::get('ktp');
		$buku_rekening = Request::get('buku_rekening');
		$selfie = Request::get('selfie');

		$storage = storage_path("app/uploads/verifikasi_premium/");
		
		if(!empty($ktp)) {
			$ktp = base64_decode($ktp);
			$filename_ktp  = 'ktp-'.date('ymdhis').'-'.$id_agen.".jpg";
	        if(file_put_contents(($storage.$filename_ktp), $ktp)) {
	        	$up['foto_ktp'] = 'uploads/verifikasi_premium/'.$filename_ktp;
	        	$up['foto_ktp_status'] = 'Submitted';
	        	$up['foto_ktp_note'] = 'Menunggu verifikasi';
	        	$up['foto_ktp_last_submit'] = date('Y-m-d H:i:s');
			}
		}

		if(!empty($buku_rekening)) {
			$buku_rekening = base64_decode($buku_rekening);
			$filename_buku_rekening  = 'buku_rekening-'.date('ymdhis').'-'.$id_agen.".jpg";
	        if(file_put_contents(($storage.$filename_buku_rekening), $buku_rekening)) {
	        	$up['foto_rekening'] = 'uploads/verifikasi_premium/'.$filename_buku_rekening;
	        	$up['foto_rekening_status'] = 'Submitted';
	        	$up['foto_rekening_note'] = 'Menunggu verifikasi';
	        	$up['foto_rekening_last_submit'] = date('Y-m-d H:i:s');
			}
		}

		if(!empty($selfie)) {
			$selfie = base64_decode($selfie);
			$filename_selfie  = 'selfie-'.date('ymdhis').'-'.$id_agen.".jpg";
	        if(file_put_contents(($storage.$filename_selfie), $selfie)) {
	        	$up['foto_selfie'] = 'uploads/verifikasi_premium/'.$filename_selfie;
	        	$up['foto_selfie_status'] = 'Submitted';
	        	$up['foto_selfie_note'] = 'Menunggu verifikasi';
	        	$up['foto_selfie_last_submit'] = date('Y-m-d H:i:s');
			}
		}

		$check = DB::table('agen_verify_premium')
	        ->where('id_agen',$id_agen)
	        ->first();

	    $up['id_agen'] = $id_agen;
	    $up['created_user'] = Esta::user($id_agen);
	    $up['updated_user'] = Esta::user($id_agen);
	    $up_agn['updated_at'] = date('Y-m-d H:i:s');
	    if(!empty($check)) {
			$update = DB::table('agen_verify_premium')
				->where('id',$check->id)
				->update($up);	    	
	    } else {
	    	$up['created_at'] = date('Y-m-d H:i:s');
	    	$update = DB::table('agen_verify_premium')
				->insert($up);	
	    }

		/*$up_agn['updated_user'] = Esta::user($id_agen);
	    $up_agen = DB::table('agen')
	    	->where('id',$id_agen)
	    	->update($up_agn);*/


		if($update){
			Esta::log_money($id_agen,0,date('Y-m-d H:i:s'),'Agen Update Request Premium','Agen Update Request Premium ','','Riwayat Agen','','');
    		$response['api_status']  = 1;
        	$response['api_message'] = 'Kami akan melakukan verifikasi akun anda terlebih dahulu dalam  1x24 jam';
        	$response['type_dialog']  = 'Informasi';
		} else {
			$response['api_status']  = 0;
	        $response['api_message'] = 'Gagal request premium';
	        $response['type_dialog']  = 'Error';
		}
	    return response()->json($response);
	}

	public function postTopupTutorial() {
		$id_agen = Request::get('id_agen');
		$banks = DB::table('bank')
			->where('deleted_at',NULL)
			->get();

		$rest_topup = array();
	  	foreach($banks as $bank) {
	  		$layanans = DB::table('tu_tutorial')
	  			->where('id_bank',$bank->id)
				->get();

			$va = DB::table('agen_va')
	  			->where('id_bank',$bank->id)
	  			->where('id_agen',$id_agen)
				->first();

			$rest_layanan = array();
		  	foreach($layanans as $layanan) {
		  		$rest2['id'] = $layanan->id;
		  		$rest2['layanan'] = $layanan->layanan;
		  		$rest2['description'] = str_replace('[va_number]', $va->no_va, $layanan->description);
		  		array_push($rest_layanan, $rest2);
		  	}

			$rest['id'] = $bank->id;
			$rest['image'] = asset('').$bank->image;
			$rest['layanan'] = $rest_layanan;
			array_push($rest_topup, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_topup;

	  	return response()->json($response);
	}

	public function postUpdatePhotoProfile() {
		$id_agen = Request::get('id_agen');

		$storage = storage_path("app/uploads/profile_agen/");
		$image = Request::input('photo');
		$image = base64_decode($image);
		$file  = 'profile-'.date('ymdhis').'-'.$id_agen.".jpg";
        if(file_put_contents(($storage.$file), $image)) {
        	$up['photo'] = 'uploads/profile_agen/'.$file;
            $up['updated_user'] = Esta::user($id_agen);
			$update = DB::table('agen')
				->where('id',$id_agen)
				->update($up);

			if($update){
	    		$response['api_status']  = 1;
	        	$response['api_message'] = 'Update photo berhasil';
	        	$response['type_dialog']  = 'Informasi';
	        	$response['photo'] = asset('').$up['photo'];
			} else {
				$response['api_status']  = 0;
		        $response['api_message'] = 'Update photo gagal';
		        $response['type_dialog']  = 'Error';
			}
		}

        return response()->json($response);
	}

	public function postChangePassword() {
		$id_agen = Request::get('id_agen');
		$old_pass = Request::get('old_pass');
		$new_pass = Request::get('new_pass');

		$detail = DB::table('agen')
			->where('id',$id_agen)
			->where('kode','!=',NULL)
			->first();

		if(hash::check($old_pass, $detail->password)){
			$up['password'] = Hash::make($new_pass);
			$up['updated_at'] = date('Y-m-d H:i:s');
			$up['updated_user'] = Esta::user($id_agen);
			$update = DB::table('agen')
				->where('id',$id_agen)
				->update($up);
	    	if($update) {
	    		$response['api_status']  = 1;
	        	$response['api_message'] = 'Kata sandi Anda berhasil diupdate';
	        	$response['type_dialog']  = 'Informasi';
	    	} else {
	    		$response['api_status']  = 2;
	        	$response['api_message'] = 'Kata sandi Anda gagal diupdate';
	        	$response['type_dialog']  = 'Error';
	    	}
		} else {
			$response['api_status']  = 0;
	        $response['api_message'] = 'Kata sandi lama Anda salah';
	        $response['type_dialog']  = 'Error';
		}

        return response()->json($response);
	}

	public function postEditProfile() {
		$id_agen = Request::get('id_agen');
		$sv['nama'] = Request::get('nama');
		$no_hp_new = Request::get('no_hp');
		$sv['email'] = Request::get('email');
        $sv['updated_user'] = Esta::user($id_agen);

		$cek_hp = DB::table('agen')
			->where('no_hp',$no_hp_new)
			->where('id','!=',$id_agen)
			->where('kode','!=',NULL)
			->first();

		if(!empty($cek_hp)) {
			$response['api_status']  = 2;
	        $response['api_message'] = 'No HP sudah terdaftar';
	        $response['type_dialog']  = 'Error';
        	return response()->json($response);
		exit();
		}

		$update = DB::table('agen')
			->where('id',$id_agen)
			->update($sv);
    	if($update) {
    		$detail = DB::table('agen')->where('id',$id_agen)->first();
    		if($no_hp_new != $detail->no_hp) {
    			$kode_otp = rand(11,99).date('s');
    			$msg = str_replace('[kode]', $kode_otp, CRUDBooster::getsetting('otp_registrasi'));
    			Esta::send_sms($no_hp_new,$msg);
    			$up['updated_at'] = date('Y-m-d H:i:s');
				//$up['tgl_otp_terkirim'] = date('Y-m-d H:i:s');
				$up['kode_otp'] = $kode_otp;

				$update = DB::table('agen')
					->where('id',$id_agen)
					->update($up);

    			$item['verifikasi_otp'] = 'Yes';
    			$item['kode_otp'] = $kode_otp;
    			$item['no_hp_baru'] = $no_hp_new;
    		} else {
    			$item['verifikasi_otp'] = 'No';
    			$item['kode_otp'] = 0;
    			$item['no_hp_baru'] = 0;
		    }
	    		$item['nama'] = Request::get('nama');
	    		$item['email'] = Request::get('email');
	    		$item['no_hp'] = Request::get('no_hp');
	    		$item['kode_referall_agen'] = $detail->kode_referall_agen;
	    		$item['status_agen'] = $detail->status_agen;
	    		$item['photo'] = asset('').$detail->photo;;

	    		$response['api_status']  = 1;
		        $response['api_message'] = 'Edit profile berhasil';
		        $response['type_dialog']  = 'Informasi';
		        $response['item'] = $item;
    	} else {
	    	$response['api_status']  = 0;
	        $response['api_message'] = 'Gagal';
	        $response['type_dialog']  = 'Error';
	    }

        return response()->json($response);
	}

	public function postRegistrasi() {
		$id_agen = Request::get('id_agen');
		$sv['nama'] = Request::get('nama');
		$sv['no_hp'] = Request::get('no_hp');
		$sv['email'] = Request::get('email');
		$sv['photo'] = 'uploads/profile_agen/avatar.jpg';
		$sv['kode_relation_referall'] = Request::get('kode_relation_referall');
		$sv['password'] = Hash::make(Request::get('password'));

		$kode = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
		/*echo $kode;
		exit();*/
		$sv['kode'] = $kode;
		$sv['kode_referall_agen'] = Esta::kode_referall();
		$sv['status_agen'] = 'Basic';
		$sv['status_aktif'] = 'Aktif';
		$sv['status_verifikasi'] = 'Pending';
		$sv['notif_email'] = 'Yes';
		$sv['created_user'] = Esta::user($id_agen);
        $sv['updated_user'] = Esta::user($id_agen);

		if(!empty($sv['kode_relation_referall'])) {
			$check_referall = DB::table('agen')->where('kode_referall_agen',$sv['kode_relation_referall'])->first();
			$setting_ref = DB::table('voucher_referall')->whereNull('deleted_at')->where('aktif','Yes')->first();

			if($check_referall->id >= 1) {
				
			} else {
				$response['api_status']  = 2;
			    $response['api_message'] = 'Kode referal yang anda pakai tidak terdaftar';
			    $response['type_dialog']  = 'Error';
			    return response()->json($response);
			    exit();
			}
		}

		$update = DB::table('agen')
			->where('id',$id_agen)
			->update($sv);
    	if($update) {
    		$banks = DB::table('bank')
    			->where('prefix_va','!=',NULL)
    			->whereNull('deleted_at')
    			->get();
    		foreach($banks as $bank) {
    			$flag_genva = $bank->flag_genva;

    			$c['created_at'] = date('Y-m-d H:i:s');
    			$c['updated_at'] = date('Y-m-d H:i:s');
    			$c['created_user'] = Esta::user($id_agen);
    			$c['updated_user'] = Esta::user($id_agen);
    			$c['id_agen'] = $id_agen;
    			$c['id_bank'] = $bank->id;
    			if($flag_genva == 0) {
	    			$c['no_va'] = $bank->prefix_va.$sv['no_hp'];
	    		} else {
	    			$c['no_va'] = $bank->prefix_va.str_replace(CRUDBooster::getsetting('kode_agen'), '', $kode);
	    		}

    			$in = DB::table('agen_va')
    				->insert($c);
    		}

    		Esta::log_money($id_agen,0,date('Y-m-d H:i:s'),'Agen Registrasi','Agen Registrasi','','Riwayat Agen','','');

    		if($check_referall->id >= 1) {
	    		$detail_voucher_referall = DB::table('voucher_referall')->whereNull('deleted_at')->first();
	    		$voucher_referall_aktif = $detail_voucher_referall->aktif;
	    		if($voucher_referall_aktif == 'Yes') {
		    		$voucher_pemakai = $detail_voucher_referall->referall_pemakai;
		    		$voucher_dipakai = $detail_voucher_referall->referall_dipakai;

					$detail_voucher_pemakai = DB::table('voucher')->where('id',$voucher_pemakai)->first();
					$detail_voucher_dipakai = DB::table('voucher')->where('id',$voucher_dipakai)->first();

					$save_child['created_at']       = date('Y-m-d H:i:s');
					$save_child['created_user'] = Esta::user($id_agen);
					$save_child['updated_user'] = Esta::user($id_agen);
					$save_child['id_agen']          = $check_referall->id;
					$save_child['id_voucher']       = $voucher_dipakai;
					$save_child['product']          = $detail_voucher_dipakai->product;
					$save_child['voucher_nama']          = $detail_voucher_dipakai->nama;
					$save_child['voucher_expired']          = $detail_voucher_dipakai->expired_date;
					$save_child['voucher_amount']          = $detail_voucher_dipakai->amount;
					$save_child['voucher_image']          = $detail_voucher_dipakai->image;
					$save_child['voucher_product']          = $detail_voucher_dipakai->product;
					$save_child['voucher_description']          = $detail_voucher_dipakai->description;
					$save_child['id_trans_voucher'] = '';
					$save_child['used']             = 'No';
					DB::table('trans_voucher_child')->insert($save_child);

					$save_notif['created_at'] = $detail_voucher_dipakai->expired_date;
					$save_notif['created_user'] = Esta::user($id_agen);
					$save_notif['updated_user'] = Esta::user($id_agen);
					$save_notif['title'] = $detail_voucher_dipakai->nama;
					$save_notif['description'] = $detail_voucher_dipakai->description;
					$save_notif['description_short'] = $detail_voucher_dipakai->description;
					$save_notif['image'] = $detail_voucher_dipakai->image;
					$save_notif['id_agen'] = $check_referall->id;
					$save_notif['read'] = 'No';
					$save_notif['flag'] = 'Voucher';
					$save_notif['syarat_ketentuan'] = $detail_voucher_dipakai->syarat_ketentuan;
					$save_notif['id_voucher'] = $voucher_dipakai;
					DB::table('notification')->insert($save_notif);

					$save_child2['created_at']       = date('Y-m-d H:i:s');
					$save_child2['created_user'] = Esta::user($id_agen);
					$save_child2['updated_user'] = Esta::user($id_agen);
					$save_child2['id_agen']          = $id_agen;
					$save_child2['id_voucher']       = $voucher_pemakai;
					$save_child2['product']          = $detail_voucher_pemakai->product;
					$save_child2['voucher_nama']          = $detail_voucher_pemakai->nama;
					$save_child2['voucher_expired']          = $detail_voucher_pemakai->expired_date;
					$save_child2['voucher_amount']          = $detail_voucher_pemakai->amount;
					$save_child2['voucher_image']          = $detail_voucher_pemakai->image;
					$save_child2['voucher_product']          = $detail_voucher_pemakai->product;
					$save_child2['voucher_description']          = $detail_voucher_pemakai->description;
					$save_child2['id_trans_voucher'] = '';
					$save_child2['used']             = 'No';
					DB::table('trans_voucher_child')->insert($save_child2);

					$save_notif2['created_at'] = $detail_voucher_pemakai->expired_date;
					$save_notif2['created_user'] = Esta::user($id_agen);
					$save_notif2['updated_user'] = Esta::user($id_agen);
					$save_notif2['title'] = $detail_voucher_pemakai->nama;
					$save_notif2['description'] = $detail_voucher_pemakai->description;
					$save_notif2['description_short'] = $detail_voucher_pemakai->description;
					$save_notif2['image'] = $detail_voucher_pemakai->image;
					$save_notif2['id_agen'] = $id_agen;
					$save_notif2['read'] = 'No';
					$save_notif2['flag'] = 'Voucher';
					$save_notif2['syarat_ketentuan'] = $detail_voucher_pemakai->syarat_ketentuan;
					$save_notif2['id_voucher'] = $voucher_pemakai;
					DB::table('notification')->insert($save_notif2);

					if($check_referall->regid != NULL) {
						$datafcm['title'] = CRUDBooster::getsetting('notification_voucher_header');
						$datafcm['content'] = CRUDBooster::getsetting('notification_voucher_referall');
						$regid[] = $check_referall->regid;
						Esta::sendFCM($regid,$datafcm);
					}
				}
			}

    		$item['nama'] = Request::get('nama');
    		$item['no_hp'] = Request::get('no_hp');
    		$item['kode_referall_agen'] = $sv['kode_referall_agen'];
    		$item['status_agen'] = $sv['status_agen'];
    		$item['photo'] = asset('').'uploads/profile_agen/avatar.jpg';

    		$response['api_status']  = 1;
	        $response['api_message'] = 'Registrasi berhasil';
	        $response['type_dialog']  = 'Informasi';
	        $response['item'] = $item;
    	} else {
	    	$response['api_status']  = 0;
	        $response['api_message'] = 'Gagal';
	        $response['type_dialog']  = 'Error';
	    }

        return response()->json($response);
	}

	public function postRegistrasiOtp() {
		$no_hp = Request::get('no_hp');
		$kode_otp = rand(11,99).date('s');


		$check = DB::table('agen')
			->where('no_hp',$no_hp)
			->where('kode','!=',NULL)
			->where('deleted_at',NULL)
			->first();

		if(!empty($check)) {
			$response['api_status']  = 0;
        	$response['api_message'] = 'Nomor HP sudah terdaftar';
        	$response['type_dialog']  = 'Error';
		} else {
			$sv['no_hp'] = $no_hp;
			$sv['created_at'] = date('Y-m-d H:i:s');
			$sv['created_user'] = '';
			$sv['updated_user'] = '';
			$sv['tgl_register'] = date('Y-m-d H:i:s');
			$sv['tgl_otp_terkirim'] = date('Y-m-d H:i:s');
			$sv['kode_otp'] = $kode_otp;

			$save = DB::table('agen')
				->insertGetId($sv);

			if($save) {
				$kode = $kode_otp;
				//$msg = 'EstaKios - '.$kode;
				$msg = str_replace('[kode]', $kode, CRUDBooster::getsetting('otp_registrasi'));
				 Esta::send_sms($no_hp, $msg);

				$response['api_status']  = 1;
	        	$response['api_message'] = 'Registrasi no HP berhasil';
	        	$response['type_dialog']  = 'Informasi';
	        	$response['kode_otp'] = $kode_otp;
	        	$response['id_agen'] = $save;
	        	$response['timer'] = CRUDBooster::getsetting('timer_resend_otp_detik');
	        	$response['resend'] = CRUDBooster::getsetting('maksimal_resend_otp');
			} else {
				$response['api_status']  = 2;
	        	$response['api_message'] = 'Gagal simpan no HP';
	        	$response['type_dialog']  = 'Error';
	        }
		}

        return response()->json($response);
	}

	public function postForgotPassOtp() {
		$no_hp = Request::get('no_hp');
		$kode_otp = rand(11,99).date('s');

		$check = DB::table('agen')
			->where('no_hp',$no_hp)
			->where('kode','!=',NULL)
			->first();

		if(empty($check)) {
			$response['api_status']  = 0;
        	$response['api_message'] = 'Nomor HP tidak terdaftar';
        	$response['type_dialog']  = 'Error';
		} else {
			$sv['updated_at'] = date('Y-m-d H:i:s');
			//$sv['tgl_otp_terkirim'] = date('Y-m-d H:i:s');
			$sv['kode_otp'] = $kode_otp;
			$sv['created_user'] = Esta::user($check->id);
			$sv['updated_user'] = Esta::user($check->id);

			$update = DB::table('agen')
				->where('id',$check->id)
				->update($sv);

			if($update) {
				$kode = $kode_otp;
				//$msg = 'EstaKios - '.$kode;
				$msg = str_replace('[kode]', $kode, CRUDBooster::getsetting('otp_registrasi'));
				Esta::send_sms($no_hp, $msg);

				$response['api_status']  = 1;
	        	$response['api_message'] = 'OTP berhasil dikirim';
	        	$response['type_dialog']  = 'Informasi';
	        	$response['kode_otp'] = $kode_otp;
	        	$response['id_agen'] = $check->id;
	        	$response['timer'] = CRUDBooster::getsetting('timer_resend_otp_detik');
	        	$response['resend'] = CRUDBooster::getsetting('maksimal_resend_otp');
			} else {
				$response['api_status']  = 2;
	        	$response['api_message'] = 'OTP gagal';
	        	$response['type_dialog']  = 'Error';
	        }
		}

        return response()->json($response);
	}

	public function postChangePassOtp() {
		$id = Request::get('id_agen');
		$kode_otp = rand(11,99).date('s');

		$check = DB::table('agen')
			->where('id',$id)
			//->where('kode','!=',NULL)
			->first();

		if(empty($check)) {
			$response['api_status']  = 0;
        	$response['api_message'] = 'Nomor HP tidak terdaftar';
        	$response['type_dialog']  = 'Error';
		} else {
			$sv['updated_at'] = date('Y-m-d H:i:s');
			//$sv['tgl_otp_terkirim'] = date('Y-m-d H:i:s');
			$sv['kode_otp'] = $kode_otp;
			$sv['created_user'] = Esta::user($check->id);
			$sv['updated_user'] = Esta::user($check->id);

			$update = DB::table('agen')
				->where('id',$check->id)
				->update($sv);

			if($update) {
				$kode = $kode_otp;
				//$msg = 'EstaKios - '.$kode;
				$msg = str_replace('[kode]', $kode, CRUDBooster::getsetting('otp_registrasi'));
				Esta::send_sms($check->no_hp, $msg);

				$response['api_status']  = 1;
	        	$response['api_message'] = 'OTP berhasil dikirim';
	        	$response['type_dialog']  = 'Informasi';
	        	$response['kode_otp'] = $kode_otp;
	        	$response['id_agen'] = $check->id;
	        	$response['timer'] = CRUDBooster::getsetting('timer_resend_otp_detik');
	        	$response['resend'] = CRUDBooster::getsetting('maksimal_resend_otp');
			} else {
				$response['api_status']  = 2;
	        	$response['api_message'] = 'OTP gagal';
	        	$response['type_dialog']  = 'Error';
	        }
		}

        return response()->json($response);
	}

	public function postForgotChangePassword() {
		$id_agen = Request::get('id_agen');
		$new_pass = Request::get('new_pass');

		$detail = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$up['password'] = Hash::make($new_pass);
		$up['updated_at'] = date('Y-m-d H:i:s');
		$up['created_user'] = Esta::user($id_agen);
		$up['updated_user'] = Esta::user($id_agen);
		$update = DB::table('agen')
			->where('id',$id_agen)
			->update($up);
    	if($update) {
    		$item['id_agen'] = $detail->id;
			$item['nama'] = $detail->nama;
    		$item['no_hp'] = $detail->no_hp;
    		$item['email'] = ($detail->email == '' ? '-' : $detail->email);
    		$item['kode_referall_agen'] = $detail->kode_referall_agen;
    		$item['status_agen'] = $detail->status_agen;
    		$item['saldo'] = ($detail->saldo <= 0 ? 0 : $detail->saldo);
    		$item['photo'] = asset('').$detail->photo;

    		$response['api_status']  = 1;
        	$response['api_message'] = 'Password updated';
        	$response['type_dialog']  = 'Informasi';
        	$response['item'] = $item;
    	} else {
    		$response['api_status']  = 2;
        	$response['api_message'] = 'Password update gagal';
        	$response['type_dialog']  = 'Error';
    	}

        return response()->json($response);
	}

	public function postLogin() {
		$no_hp = Request::get('no_hp');
		$password = Request::get('password');
		$regid = Request::get('regid');

		$check_hp = DB::table('agen')
			->where('no_hp',$no_hp)
			->where('kode','!=',NULL)
			->whereNull('deleted_at')
			->first();
		if(!empty($check_hp)) {
			if($check_hp->status_aktif == 'Tidak Aktif') {
				$response['api_status']  = 3;
		        $response['api_message'] = CRUDBooster::getsetting('pesan_paksa_logout');
		        $response['type_dialog']  = 'Error';
			} else {
				$check_pass = DB::table('agen')
					->where('no_hp',$no_hp)
					->where('kode','!=',NULL)
					->whereNull('deleted_at')
					->first();
				if(hash::check($password, $check_pass->password)){
					$up['regid'] = $regid;
					$update = DB::table('agen')
						->where('id',$check_pass->id)
						->update($up);

					$item['id_agen'] = $check_pass->id;
					$item['nama'] = $check_pass->nama;
		    		$item['no_hp'] = $check_pass->no_hp;
		    		$item['email'] = ($check_pass->email == '' ? '-' : $check_pass->email);
		    		$item['kode_referall_agen'] = $check_pass->kode_referall_agen;
		    		$item['status_agen'] = $check_pass->status_agen;
		    		$item['saldo'] = ($check_pass->saldo <= 0 ? 0 : $check_pass->saldo);
		    		$item['photo'] = asset('').$check_pass->photo;

		    		$response['api_status']  = 1;
			        $response['api_message'] = 'Login berhasil';
			        $response['type_dialog']  = 'Informasi';
			        $response['item'] = $item;
				} else {
					$response['api_status']  = 2;
			        $response['api_message'] = 'Password salah';
			        $response['type_dialog']  = 'Error';
				}
			}
		} else {
			$response['api_status']  = 0;
		    $response['api_message'] = 'No HP tidak ditemukan';
		    $response['type_dialog']  = 'Error';
		}

		return response()->json($response);
	}

	public function postLogout() {
		$id_agen = Request::get('id_agen');

		$up['regid'] = NULL;

		$update = DB::table('agen')
			->where('id',$id_agen)
			->update($up);

		$response['api_status']  = 1;
		$response['api_message'] = 'Logout berhasil';
		$response['type_dialog']  = 'Informasi';

		return response()->json($response);
	}

	public function postVerifikasiOtp() {
		$type = Request::get('type');
		$no_hp_baru = Request::get('no_hp_baru');
		$kode_otp = Request::get('kode_otp');
		$id_agen = Request::get('id_agen');

		if($type == 'update') {
			$sv['no_hp'] = $no_hp_baru;
		}
		$sv['tgl_verifikasi_otp'] = date('Y-m-d H:i:s');
		$sv['updated_at'] = date('Y-m-d H:i:s');
		$sv['created_user'] = Esta::user($id_agen);
		$sv['updated_user'] = Esta::user($id_agen);

		$update = DB::table('agen')
			->where('id',$id_agen)
			->where('kode_otp',$kode_otp)
			->update($sv);
    	if($update) {
    		$response['api_status']  = 1;
	        $response['api_message'] = 'Sukses update tgl verifikasi';
	        $response['type_dialog']  = 'Informasi';
	        $response['kode_otp'] = $kode_otp;
	        $response['id_agen'] = $id_agen;
    	} else {
	    	$response['api_status']  = 0;
	        $response['api_message'] = 'Verifikasi OTP gagal. Kode OTP yang Anda gunakan sudah tidak valid';
	        $response['type_dialog']  = 'Error';
	    }

        return response()->json($response);
	}

	public function postChangeNotifEmail() {
		$status = Request::get('status');
		$id_agen = Request::get('id_agen');

		$sv['notif_email'] = $status;
		$sv['updated_at'] = date('Y-m-d H:i:s');
		$sv['updated_user'] = Esta::user($id_agen);

		$update = DB::table('agen')
			->where('id',$id_agen)
			->update($sv);
    	if($update) {
    		$response['api_status']  = 1;
	        $response['api_message'] = 'Update notifikasi berhasil';
	        $response['type_dialog']  = 'Informasi';
    	} else {
	    	$response['api_status']  = 0;
	        $response['api_message'] = 'Update notifikasi gagal';
	        $response['type_dialog']  = 'Error';
	    }

        return response()->json($response);
	}

	public function postSetting() {
		$id_agen = Request::get('id_agen');
		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$item['notif_email'] = ($detail_agen->notif_email == 'No' ? 'No' : 'Yes');
		$item['saldo'] = ($detail_agen->saldo <= 0 ? 0 : $detail_agen->saldo);
		if($detail_agen->is_agen_kios == true){
			$item['status_agen'] = 'Agen Kios';
		}else{
			$item['status_agen'] = $detail_agen->status_agen;
		}
		$item['versi_aplikasi'] = CRUDBooster::getsetting('versi_aplikasi');
		$item['share_referal'] = str_replace('[kode]', $detail_agen->kode_referall_agen, CRUDBooster::getsetting('share_referal'));
		$item['link_play_store'] = CRUDBooster::getsetting('link_play_store');
		$item['share_struk'] = CRUDBooster::getsetting('share_struk');
		$item['paksa_update'] = CRUDBooster::getsetting('paksa_update');
		$item['maintenance_pulsa'] = CRUDBooster::getsetting('maintenance_pulsa');
		$item['maintenance_pln'] = CRUDBooster::getsetting('maintenance_pln');
		$item['maintenance_pdam'] = CRUDBooster::getsetting('maintenance_pdam');
		$item['maintenance_bpjs'] = CRUDBooster::getsetting('maintenance_bpjs');
		$item['minimal_tarik_tunai'] = CRUDBooster::getsetting('minimal_tarik_tunai');
		$item['tarik_tunai_tanpa_biaya_admin'] = CRUDBooster::getsetting('tarik_tunai_tanpa_biaya_admin');
		$item['biaya_admin_tarik_tunai'] = CRUDBooster::getsetting('biaya_admin_tarik_tunai');
		$item['bantuan_cs'] = CRUDBooster::getsetting('bantuan_cs');
		$item['logout'] = ($detail_agen->status_aktif == 'Tidak Aktif' ? 'Yes' : 'No');
		$item['logout_desc'] = CRUDBooster::getsetting('pesan_paksa_logout');
		$item['info_pulsa'] = (CRUDBooster::getsetting('info_pulsa') != '' ? CRUDBooster::getsetting('info_pulsa') : '');
		$item['info_pln'] = (CRUDBooster::getsetting('info_pln') != '' ? CRUDBooster::getsetting('info_pln') : '');
		$item['info_pdam'] = (CRUDBooster::getsetting('info_pdam') != '' ? CRUDBooster::getsetting('info_pdam') : '');
		$item['info_bpjs'] = (CRUDBooster::getsetting('info_bpjs') != '' ? CRUDBooster::getsetting('info_bpjs') : '');
		$item['info_tarik_tunai'] = (CRUDBooster::getsetting('info_tarik_tunai') != '' ? str_replace(array('[param1]','[param2]'), array(number_format(CRUDBooster::getsetting('tarik_tunai_tanpa_biaya_admin'),0,',','.'),number_format(CRUDBooster::getsetting('biaya_admin_tarik_tunai'),0,',','.')),CRUDBooster::getsetting('info_tarik_tunai')) : '');
		if($item['status_agen'] == 'Premium') {
			$item['tt_nama_bank'] = DB::table('bank')->where('id',$detail_agen->rek_id_bank)->first()->nama;
			$item['tt_nama_akun'] = $detail_agen->rek_nama;
			$item['tt_no_rek'] = $detail_agen->rek_no;
		} else {
			$item['tt_nama_bank'] = '';
			$item['tt_nama_akun'] = '';
			$item['tt_no_rek'] = '';
		}

		$response['api_status']  = 1;
	    $response['api_message'] = 'Api setting';
	    $response['type_dialog']  = 'Informasi';
	    $response['item'] = $item;

        return response()->json($response);
	}

	public function postSlider() {
		$sliders = DB::table('slider')
			//->where('approve','Yes')
			->where('deleted_at',NULL)
			->orderBy('id','desc')
			->get();

		$rest_slider = array();
	  	foreach($sliders as $slider) {
			$rest['id']         = $slider->id;
			$rest['datetime']         = $slider->created_at;
			$rest['redirect']         = ($slider->redirect != '' ? $slider->redirect : 'Tidak');
			$rest['tagline'] = '';//$slider->title;
			$rest['url']      = $slider->url;
			$rest['description']      = $slider->description;
			$rest['title']   = '';//$slider->tagline;
			$rest['image']      = asset('').$slider->image;
			array_push($rest_slider, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Slider';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_slider;

	  	return response()->json($response);
	}

	public function postNotification() {
		$id_agen = Request::get('id_agen');
		$notifs = DB::table('notification')
			->where('id_agen',$id_agen)
			->orderBy('id','desc')
			->get();

		$rest_slider = array();
	  	foreach($notifs as $notif) {
			$rest['id']         = $notif->id;
			$rest['datetime']         = $notif->created_at;
			$rest['description']      = $notif->description;
			$rest['description_short']      = strip_tags(($notif->description_short != '' ? $notif->description_short : ''));
			$rest['title']   = $notif->title;
			$rest['id_voucher']   = $notif->id_voucher;
			$rest['expired_date']   = $notif->expired_date;
			$rest['flag']   = $notif->flag;
			$rest['syarat_ketentuan']   = $notif->syarat_ketentuan;
			$rest['read']   = ($notif->read == 'Yes' ? 'Yes' : 'No');
			$rest['image']      = asset('').$notif->image;
			array_push($rest_slider, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Notif';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_slider;

	  	return response()->json($response);
	}

	public function postReadNotification() {
		$id_notif = Request::get('id_notif');

		$up['read'] = 'Yes';
		$update = DB::table('notification')
			->where('id',$id_notif)
			->update($up);
		if($update) {
			$response['api_status']  = 1;
	   	 	$response['api_message'] = 'Sukses';
	   	 	$response['type_dialog']  = 'Informasi';
		} else {
			$response['api_status']  = 0;
	   	 	$response['api_message'] = 'Gagal';
	   	 	$response['type_dialog']  = 'Error';
		}
		return response()->json($response);
	}

	public function postVaInquiry() {
		$bank = Request::get('bank');
		switch ($bank) {
			case 'bca':
				$id_bank = 1;
				$channelId  = CRUDBooster::getsetting('channel_id_bca'); // Your Channel ID
				$secretKey  = CRUDBooster::getsetting('secret_key_bca'); // Your secretKey
				$biaya_admin = DB::table('bank')->where('id','1')->first()->biaya_admin+DB::table('bank')->where('id','1')->first()->biaya_admin_bank;
				break;
			case 'bri':
				$id_bank = 3;
				$channelId  = CRUDBooster::getsetting('channel_id_bri'); // Your Channel ID
				$secretKey  = CRUDBooster::getsetting('secret_key_bri'); // Your secretKey
				$biaya_admin = DB::table('bank')->where('id','3')->first()->biaya_admin+DB::table('bank')->where('id','1')->first()->biaya_admin_bank;
				break;
		}
		$ExpReqToup = DB::table('cms_settings')->where('name','expired_topup')->first()->content;
		$kode = Esta::nomor_transaksi('tu_res_sprint',CRUDBooster::getsetting('inquiry_top_up'));

		$transactionNo = $kode;//uniqid();

		$customerAccount = Request::get('customerAccount');
		$currency = Request::get('currency');
		$channelType = Request::get('channelType');
		$transactionDate = Request::get('transactionDate');
		$inquiryReffId = Request::get('inquiryReffId');
		$channelId2 = Request::get('channelId');
		$authCode = Request::get('authCode');
		$transactionAmount = "0";
		$additionalData = null;

		/*
		    Get Customer Info from database
		    1. customerAccount
		    2. customerName
		*/
		$get_va = DB::table('agen_va')
			->where('no_va',$customerAccount)
			->where('deleted_at',NULL)
			->first();
		$customerName = DB::table('agen')->where('id',$get_va->id_agen)->where('nama','!=',NULL)->first()->nama;

		$data_sprint = array(
		    "currency"            => $currency,
		    "transactionDate"     => $transactionDate,
		    "channelType"         => $channelType,
		    "customerAccount"     => $customerAccount,
		    "inquiryReffId"       => $inquiryReffId,
		    "authCode"            => $authCode,
		);

		$resp   = array(
		    "channelId"           => $channelId2,                       // Channel ID sent by Sprint
		    "currency"            => $currency,                        // Currency sent by Sprint (IDR)
		    "transactionNo"       => $transactionNo,                               // Unique transaction number
		    "transactionAmount"   => "0",                                            // Transaction amount set to 0
		    "transactionDate"     => date("Y-m-d H:i:s"),                          // Transaction Date
		    "transactionExpire"   => date("Y-m-d H:i:s", strtotime("+".$ExpReqToup." hours")),     // Transaction will be expired after 1 day
		    "description"         => "Top up saldo",                               // Transaction Description
		    "customerAccount"     => $customerAccount,                 // Customer Account sent by Sprint
		    "customerName"        => $customerName,                                // Customer Name From Merchant Data
		    "inquiryReffId"       => $inquiryReffId,                   // Inquiry Referrence ID sent by Sprint
		    "additionalData"      => $additionalData,                            // Additional data information (Optional)
		);

		// Generate authCode
		$authCode2 = hash("sha256", $customerAccount.$inquiryReffId.$secretKey);

		//response & insert ke sprint
		$authCodeInsert = hash("sha256", $transactionNo.$transactionAmount.$channelId.$secretKey);
		//$freeTexts = '{"indonesian":"'.CRUDBooster::getsetting('freetext_sprint_indonesia').'","english":"'.CRUDBooster::getsetting('freetext_sprint_english').'","section":"1"}';
		$data = array(
		    "channelId"         => $channelId,
		    "currency"          => $currency,
		    "transactionNo"     => $transactionNo,
		    "transactionAmount" => $transactionAmount,
		    "transactionDate"   => $transactionDate,
		    "transactionExpire" => $resp['transactionExpire'],
		    "description"       => "Order".$transactionNo,
		    //"itemDetail"        => '',
		    //"authCode"          => $authCodeInsert,
		    "additionalData"    => $additionalData,
		    "customerAccount"   => $customerAccount,
		    "customerName"      => $customerName,
		    "inquiryReffId"       => $inquiryReffId,
		    "freeTexts"       => array(array(
		    	"indonesian" => str_replace('[amount]',number_format($biaya_admin,0,',','.'),CRUDBooster::getsetting('freetext_sprint_indonesia')),
		    	"english" => str_replace('[amount]',number_format($biaya_admin,0,',','.'),CRUDBooster::getsetting('freetext_sprint_english')),
		    	"section" => '1'),
		    )//str_replace('[amount]', number_format($biaya_admin,0,',','.'), $freeTexts),
		);

		/*
		Validation process
		*/
		$va_length = strlen($customerAccount);

		// Set header response
		header('Content-Type: application/json');

		// Validate authcode
		/*if( $authCode != $authCode2 ){
		    $data                   = array();
		    $data['channelId']      = $channelId;
		    $data['inquiryError']   = "Invalid Authcode";
		    $data['currency']          = NULL;
		    $data['transactionNo']     = NULL;
		    $data['transactionAmount'] = NULL;
		    $data['transactionDate']   = NULL;
		    $data['transactionExpire'] = NULL;
		    $data['description']       = NULL;
		    $data['additionalData']    = NULL;
		    $data['customerAccount']   = NULL;
		    $data['customerName']      = NULL;
		    $data['inquiryReffId']     = NULL;
		}*/

		// Validate Currency
		/*else*/if( $currency != "IDR" ){
		    $data                   = array();
		    $data['channelId']      = $channelId;
		    $data['inquiryError']   = "Invalid Currency";
		    $data['currency']          = NULL;
		    $data['transactionNo']     = NULL;
		    $data['transactionAmount'] = NULL;
		    $data['transactionDate']   = NULL;
		    $data['transactionExpire'] = NULL;
		    $data['description']       = NULL;
		    $data['additionalData']    = NULL;
		    $data['customerAccount']   = NULL;
		    $data['customerName']      = NULL;
		    $data['inquiryReffId']     = NULL;
		}

		// Validate customerAccount
		elseif(!$get_va){
		    $data                   = array();
		    $data['channelId']      = $channelId;
		    $data['inquiryError']   = "Invalid Customer Account";
		    $data['currency']          = NULL;
		    $data['transactionNo']     = NULL;
		    $data['transactionAmount'] = NULL;
		    $data['transactionDate']   = NULL;
		    $data['transactionExpire'] = NULL;
		    $data['description']       = NULL;
		    $data['additionalData']    = NULL;
		    $data['customerAccount']   = NULL;
		    $data['customerName']      = NULL;
		    $data['inquiryReffId']     = NULL;
		} else {

			if($channelId<>NULL){
				$req_sv['created_at'] = date('Y-m-d H:i:s');
				$req_sv['transactionNo'] = $transactionNo;
				$req_sv['currency'] = $currency;
				$req_sv['transactionAmount'] = $transactionAmount;
				$req_sv['transactionDate'] = $transactionDate;
				$req_sv['transactionExpire'] = $resp['transactionExpire'];
				$req_sv['description'] = $resp['$description'];
				$req_sv['customerAccount'] = $customerAccount;
				$req_sv['customerName'] = $customerName;
				$req_sv['inquiryReffId'] = $inquiryReffId;
				$req_sv['additionalData'] = $additionalData;
				//$req_sv['create_user'] = 'SPRINT';
				$req_sv['created_user'] = 'SPRINT';
				$req_sv['updated_user'] = 'SPRINT';
				$req_sv['id_agen'] = $get_va->id_agen;
				$req_sv['id_bank'] = $id_bank;

				$save_req = DB::table('tu_req_sprint')
					->insert($req_sv);

				$res_sv['created_at'] = date('Y-m-d H:i:s');
				$res_sv['created_user'] = 'SPRINT';
				$res_sv['updated_user'] = 'SPRINT';
				$res_sv['transactionNo'] = $transactionNo;
				$res_sv['currency'] = $currency;
				$res_sv['transactionAmount'] = $transactionAmount;
				$res_sv['transactionDate'] = $transactionDate;
				$res_sv['transactionExpire'] = $resp['transactionExpire'];
				$res_sv['description'] = $resp['$description'];
				$res_sv['customerAccount'] = $customerAccount;
				$res_sv['customerName'] = $customerName;
				$res_sv['inquiryReffId'] = $inquiryReffId;
				$res_sv['additionalData'] = $additionalData;
				$res_sv['channelId'] = $channelId2;
				$res_sv['channelType'] = $channelType;
				$res_sv['id_agen'] = $get_va->id_agen;
				$res_sv['id_bank'] = $id_bank;

				$save_res = DB::table('tu_res_sprint')
					->insert($res_sv);
			}
		}

		return response()->json($data);
	}

	public function postVaFlag() {
		$lg1['res'] = 'start'.date('Y-m-d H:i:s');

		$lgg = DB::table('log_sprint')
			->insert($lg1);

		$bank = Request::get('bank');
		switch ($bank) {
			case 'bca':
				$id_bank = 1;
				$channelId  = CRUDBooster::getsetting('channel_id_bca'); // Your Channel ID
				$secretKey  = CRUDBooster::getsetting('secret_key_bca'); // Your secretKey
				$biaya_admin = DB::table('bank')->where('id','1')->first()->biaya_admin+DB::table('bank')->where('id','1')->first()->biaya_admin_bank;
				break;
			case 'bri':
				$id_bank = 3;
				$channelId  = CRUDBooster::getsetting('channel_id_bri'); // Your Channel ID
				$secretKey  = CRUDBooster::getsetting('secret_key_bri'); // Your secretKey
				$biaya_admin = DB::table('bank')->where('id','3')->first()->biaya_admin+DB::table('bank')->where('id','1')->first()->biaya_admin_bank;
				break;
		}

		/*
		Get Transaction from database
		1. transactionNo
		2. currency           => IDR
		3. transactionAmount
		4. transactionDate
		5. transactionExpire
		6. insertId           => insertId from Sprint in Insert Transaction Response
		7. status             => payment order status
		*/

		$transaction_data = DB::table('tu_res_sprint')
			->where('transactionNo',Request::get('transactionNo'))
			->where('customerAccount',Request::get('customerAccount'))
			->first();

		/*print_r($transaction_data);
		exit();*/

		// Parsing request data from Sprint
		$data_sprint = array(
		    "channelId"           => Request::get('channelId'),
		    "currency"            => Request::get('currency'),
		    "transactionNo"       => Request::get('transactionNo'),
		    "transactionAmount"   => Request::get('transactionAmount'),
		    "transactionDate"     => Request::get('transactionDate'),
		    "transactionExpired"  => Request::get('transactionExpired'),
		    "transactionStatus"   => Request::get('transactionStatus'),
		    "transactionMessage"  => Request::get('transactionMessage'),
		    "flagType"            => Request::get('flagType'),
		    "insertId"            => Request::get('insertId'),
		    "paymentReffId"       => Request::get('paymentReffId'),
		    "authCode"            => Request::get('authCode'),
		    "additionalData"      => Request::get('additionalData'),
		    "customerAccount"      => Request::get('customerAccount'),
		);

		// Prepare response data
		//$freeTexts = '{"indonesian":"'.CRUDBooster::getsetting('freetext_sprint_indonesia').'","english":"'.CRUDBooster::getsetting('freetext_sprint_english').'","section":"1"}';
		$resp   = array(
		    "channelId"           => Request::get('channelId'),      // Channel ID sent by Sprint
		    "currency"            => Request::get('currency'),       // Currency sent by Sprint (IDR)
		    "paymentStatus"       => "",                          // Payment Status ( 00 => Success , 01,03 => Failed , 02 => isPaid , 04 => isExpired , 05 => isCancelled )
		    "paymentMessage"      => "",                          // Payment Message
		    "flagType"            => Request::get('flagType'),       // Flag Type sent by Sprint
		    "paymentReffId"       => Request::get('paymentReffId'),  // Payment Referrence ID sent by Sprint
		    "freeTexts"       => array(
		    	"indonesian" => str_replace('[amount]',number_format($biaya_admin,0,',','.'),CRUDBooster::getsetting('freetext_sprint_indonesia')),
		    	"english" => str_replace('[amount]',number_format($biaya_admin,0,',','.'),CRUDBooster::getsetting('freetext_sprint_english')),
		    	"section" => '2',
		    )
		);

		//$lg['req'] = ;
		$lg['res'] = 'channelId='.$data_sprint['channelId'].'##'.'currency='.$data_sprint['currency'].'##'.'transactionAmount='.$data_sprint['transactionAmount'].'##'.'transactionNo='.$data_sprint['transactionNo'].'##'.'transactionDate='.$data_sprint['transactionDate'].'##'.'transactionExpired='.$data_sprint['transactionExpired'].'##'.'transactionStatus='.$data_sprint['transactionStatus'].'##'.'transactionMessage='.$data_sprint['transactionMessage'].'##'.'flagType='.$data_sprint['flagType'].'##'.'insertId='.$data_sprint['insertId'].'##'.'paymentReffId='.$data_sprint['paymentReffId'].'##'.'authCode='.$data_sprint['authCode'].'##'.'additionalData='.$data_sprint['additionalData'].'##'.'customerAccount='.$data_sprint['customerAccount'];

		$lgg = DB::table('log_sprint')
			->insert($lg);

		// Generate authCode
		$authCode = hash("sha256", $transaction_data->transactionNo.$data_sprint['transactionAmount'].$channelId.$data_sprint['transactionStatus'].$data_sprint['insertId'].$secretKey);

		/*return $data_sprint['insertId'];
		exit();*/
		// Validate Transaction

		if( !$transaction_data ){
		    $resp['paymentStatus']  = "01";
		    $resp['paymentMessage'] = "Invalid Transaction";

		    /*$save_flag1['created_at'] = date('Y-m-d H:i:s');
		    $save_flag1['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag1['currency'] = $data_sprint['currency'];
		    $save_flag1['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag1['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag1['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag1['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag1['customerName'] = $transaction_data->customerName;
		    $save_flag1['create_user'] = 'SPRINT';
		    $save_flag1['id_bank'] = $id_bank;
		    $save_flag1['status'] = $resp['paymentStatus'];
		    $save_flag1['statusmessage'] = $resp['paymentMessage'];
		    $save_flag1['insertId'] = $data_sprint['insertId'];
		    $save_flag1['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag1 = DB::table('tu_flag_sprint')
			    	->insert($save_flag1);*/
		}

		// Validate Channel Id
		elseif( $data_sprint['channelId'] != $channelId ){
		    $resp['paymentStatus']  = "01";
		    $resp['paymentMessage'] = "Invalid Channel ID";
		}

		elseif($data_sprint['transactionAmount'] < CRUDBooster::getsetting('minimal_topup')) {
		    $resp['paymentStatus']  = "01";
		    $resp['paymentMessage'] = "Invalid Transaction";
		}

		// Validate Currency
		elseif( $data_sprint['currency'] != "IDR" ){
		    $resp['paymentStatus']  = "01";
		    $resp['paymentMessage'] = "Invalid Currency";

		    /*$save_flag3['created_at'] = date('Y-m-d H:i:s');
		    $save_flag3['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag3['currency'] = $data_sprint['currency'];
		    $save_flag3['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag3['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag3['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag3['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag3['customerName'] = $transaction_data->customerName;
		    $save_flag3['create_user'] = 'SPRINT';
		    $save_flag3['id_bank'] = $id_bank;
		    $save_flag3['status'] = $resp['paymentStatus'];
		    $save_flag3['statusmessage'] = $resp['paymentMessage'];
		    $save_flag3['insertId'] = $data_sprint['insertId'];
		    $save_flag3['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag3 = DB::table('tu_flag_sprint')
			    	->insert($save_flag3);*/
		}

		// Validate Transaction No
		elseif( $data_sprint['transactionNo'] != $transaction_data->transactionNo ){
		    $resp['paymentStatus']  = "01";
		    $resp['paymentMessage'] = "Invalid Currency";

		    /*$save_flag4['created_at'] = date('Y-m-d H:i:s');
		    $save_flag4['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag4['currency'] = $data_sprint['currency'];
		    $save_flag4['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag4['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag4['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag4['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag4['customerName'] = $transaction_data->customerName;
		    $save_flag4['create_user'] = 'SPRINT';
		    $save_flag4['id_bank'] = $id_bank;
		    $save_flag4['status'] = $resp['paymentStatus'];
		    $save_flag4['statusmessage'] = $resp['paymentMessage'];
		    $save_flag4['insertId'] = $data_sprint['insertId'];
		    $save_flag4['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag4 = DB::table('tu_flag_sprint')
			    	->insert($save_flag4);*/
		}

		// 4.  Validate Transaction Amount
		/*elseif( $data_sprint['transactionAmount'] != $transaction_data->transactionAmount ){
		   $resp['paymentStatus']  = "01";
		   $resp['paymentMessage'] = "Invalid Transaction Amount";

		   $save_flag11['created_at'] = date('Y-m-d H:i:s');
		    $save_flag11['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag11['currency'] = $data_sprint['currency'];
		    $save_flag11['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag11['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag11['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag11['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag11['customerName'] = $transaction_data->customerName;
		    $save_flag11['create_user'] = 'SPRINT';
		    $save_flag11['id_bank'] = $id_bank;
		    $save_flag11['status'] = $resp['paymentStatus'];
		    $save_flag11['statusmessage'] = $resp['paymentMessage'];
		    $save_flag11['insertId'] = $data_sprint['insertId'];
		    $save_flag11['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag4 = DB::table('tu_flag_sprint')
			    	->insert($save_flag11);
		}*/

		// Validate Transaction Status
		elseif( $data_sprint['transactionStatus'] != "00" ){
		    $resp['paymentStatus']  = "01";
		    $resp['paymentMessage'] = "Invalid Transaction Status ".$data_sprint['transactionStatus'];

		    /*$save_flag5['created_at'] = date('Y-m-d H:i:s');
		    $save_flag5['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag5['currency'] = $data_sprint['currency'];
		    $save_flag5['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag5['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag5['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag5['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag5['customerName'] = $transaction_data->customerName;
		    $save_flag5['create_user'] = 'SPRINT';
		    $save_flag5['id_bank'] = $id_bank;
		    $save_flag5['status'] = $resp['paymentStatus'];
		    $save_flag5['statusmessage'] = $resp['paymentMessage'];
		    $save_flag5['insertId'] = $data_sprint['insertId'];
		    $save_flag5['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag5 = DB::table('tu_flag_sprint')
			    	->insert($save_flag5);*/
		}

		// Validate FlagType
		elseif( Request::get('flagType') != "11" && Request::get('flagType') != "12" && Request::get('flagType') != "13" ){
		    $resp['paymentStatus']  = "01";
		    $resp['paymentMessage'] = "Invalid Flagtype";

		    /*$save_flag6['created_at'] = date('Y-m-d H:i:s');
		    $save_flag6['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag6['currency'] = $data_sprint['currency'];
		    $save_flag6['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag6['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag6['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag6['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag6['customerName'] = $transaction_data->customerName;
		    $save_flag6['create_user'] = 'SPRINT';
		    $save_flag6['id_bank'] = $id_bank;
		    $save_flag6['status'] = $resp['paymentStatus'];
		    $save_flag6['statusmessage'] = $resp['paymentMessage'];
		    $save_flag6['insertId'] = $data_sprint['insertId'];
		    $save_flag6['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag6 = DB::table('tu_flag_sprint')
			    	->insert($save_flag6);*/
		}

		// Validate Insert ID
		/*elseif( $data_sprint['insertId'] != $transaction_data->insertId ){
		    $resp['paymentStatus']  = "01";
		    $resp['paymentMessage'] = "Invalid Transaction Status ".$data_sprint['insertId'];

		    $save_flag12['created_at'] = date('Y-m-d H:i:s');
		    $save_flag12['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag12['currency'] = $data_sprint['currency'];
		    $save_flag12['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag12['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag12['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag12['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag12['customerName'] = $transaction_data->customerName;
		    $save_flag12['create_user'] = 'SPRINT';
		    $save_flag12['id_bank'] = $id_bank;
		    $save_flag12['status'] = $resp['paymentStatus'];
		    $save_flag12['statusmessage'] = $resp['paymentMessage'];
		    $save_flag12['insertId'] = $data_sprint['insertId'];
		    $save_flag12['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag4 = DB::table('tu_flag_sprint')
			    	->insert($save_flag12);
		}*/

		// Validate authCode
		
		elseif( $data_sprint['authCode'] != $authCode ) {
		   $resp['paymentStatus'] = "01";
		   $resp['paymentMessage'] = "Invalid authCode";

		   /*$save_flag7['created_at'] = date('Y-m-d H:i:s');
		   $save_flag7['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag7['currency'] = $data_sprint['currency'];
		    $save_flag7['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag7['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag7['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag7['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag7['customerName'] = $transaction_data->customerName;
		    $save_flag7['create_user'] = 'SPRINT';
		    $save_flag7['id_bank'] = $id_bank;
		    $save_flag7['status'] = $resp['paymentStatus'];
		    $save_flag7['statusmessage'] = $resp['paymentMessage'];
		    $save_flag7['insertId'] = $data_sprint['insertId'];
		    $save_flag7['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag7 = DB::table('tu_flag_sprint')
			    	->insert($save_flag7);*/
		   //$resp['kode'] = $authCode;
		}
		
		// Validate Transaction Status => CANCELLED
		elseif( $transaction_data->status == "CANCELLED" ){
		    $resp['paymentStatus']  = "05";
		    $resp['paymentMessage'] = "Transaction has been cancelled";

		    /*$save_flag8['created_at'] = date('Y-m-d H:i:s');
		    $save_flag8['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag8['currency'] = $data_sprint['currency'];
		    $save_flag8['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag8['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag8['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag8['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag8['customerName'] = $transaction_data->customerName;
		    $save_flag8['create_user'] = 'SPRINT';
		    $save_flag8['id_bank'] = $id_bank;
		    $save_flag8['status'] = $resp['paymentStatus'];
		    $save_flag8['statusmessage'] = $resp['paymentMessage'];
		    $save_flag8['insertId'] = $data_sprint['insertId'];
		    $save_flag8['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag8 = DB::table('tu_flag_sprint')
			    	->insert($save_flag8);*/
		}

		// Validate Transaction Status => EXPIRED
		elseif( $transaction_data->status == "EXPIRED" ){
		    $resp['paymentStatus']  = "04";
		    $resp['paymentMessage'] = "Transaction has been expired";

		    /*$save_flag9['created_at'] = date('Y-m-d H:i:s');
		    $save_flag9['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag9['currency'] = $data_sprint['currency'];
		    $save_flag9['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag9['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag9['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag9['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag9['customerName'] = $transaction_data->customerName;
		    $save_flag9['create_user'] = 'SPRINT';
		    $save_flag9['id_bank'] = $id_bank;
		    $save_flag9['status'] = $resp['paymentStatus'];
		    $save_flag9['statusmessage'] = $resp['paymentMessage'];
		    $save_flag9['insertId'] = $data_sprint['insertId'];
		    $save_flag9['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag1 = DB::table('tu_flag_sprint')
			    	->insert($save_flag9);*/
		}

		// Validate Transaction Status => PAID
		elseif( $transaction_data->status == "PAID" ){
		    $resp['paymentStatus']  = "02";
		    $resp['paymentMessage'] = "Transaction has been paid";

		    /*$save_flag10['created_at'] = date('Y-m-d H:i:s');
		    $save_flag10['transactionNo'] = $data_sprint['transactionNo'];
		    $save_flag10['currency'] = $data_sprint['currency'];
		    $save_flag10['transactionAmount'] = $data_sprint['transactionAmount'];
		    $save_flag10['transactionDate'] = $data_sprint['transactionDate'];
		    $save_flag10['transactionExpire'] = $data_sprint['transactionExpired'];
		    $save_flag10['customerAccount'] = $transaction_data->customerAccount;
		    $save_flag10['customerName'] = $transaction_data->customerName;
		    $save_flag10['create_user'] = 'SPRINT';
		    $save_flag10['id_bank'] = $id_bank;
		    $save_flag10['status'] = $resp['paymentStatus'];
		    $save_flag10['statusmessage'] = $resp['paymentMessage'];
		    $save_flag10['insertId'] = $data_sprint['insertId'];
		    $save_flag10['paymentReffId'] = $data_sprint['paymentReffId'];

		    $sv_flag1 = DB::table('tu_flag_sprint')
			    	->insert($save_flag10);*/
		}

		// Success
		else{
		    $resp['paymentStatus']  = "00";
		    $resp['paymentMessage'] = "Success";

		    $check = DB::table('trans_topup')
		    	->where('ref_trans_no',$data_sprint['transactionNo'])
		    	->first();
		    if($check->id <= 0) {
			    $detail_agen = DB::table('agen')->where('id',$transaction_data->id_agen)->first();
			    $detail_bank = DB::table('bank')->where('id',$id_bank)->first();
			    $kode = Esta::nomor_transaksi('trans_topup',CRUDBooster::getsetting('transaksi_top_up'));

			    $save_trans['trans_no'] = $kode;//
			    $save_trans['created_at'] = date('Y-m-d H:i:s');
			    $save_trans['updated_at'] = date('Y-m-d H:i:s');
			    $save_trans['created_user'] = 'SPRINT';
				$save_trans['updated_user'] = 'SPRINT';
			    $save_trans['ref_trans_no'] = $data_sprint['transactionNo'];
			    $save_trans['trans_date'] = $data_sprint['transactionDate'];
			    $save_trans['trans_desc'] = 'Top up VA';
			    $save_trans['currency'] = $data_sprint['currency'];
			    $save_trans['flg_plus_min'] = '1';
			    $save_trans['trans_amount'] = $data_sprint['transactionAmount'];
			    $save_trans['trans_type_code'] = 'VA';
			    $save_trans['va_no'] = $transaction_data->customerAccount;
			    //$save_trans['create_user'] = 'SPRINT';
			    $save_trans['id_bank'] = $id_bank;
			    $save_trans['id_agen'] = $transaction_data->id_agen;
			    $save_trans['biaya_admin_bank'] = DB::table('bank')->where('id',DB::table('agen_va')->where('no_va',$transaction_data->customerAccount)->first()->id_bank)->first()->biaya_admin_bank;
			    $save_trans['biaya_admin'] = DB::table('bank')->where('id',DB::table('agen_va')->where('no_va',$transaction_data->customerAccount)->first()->id_bank)->first()->biaya_admin;
			    $save_trans['batch_last_user'] = 'SPRINT';
			    $save_trans['agen_referall'] = $detail_agen->kode_referall_agen;
			    $save_trans['agen_referall_relation'] = $detail_agen->kode_relation_referall;
			    $save_trans['agen_status_aktif'] = $detail_agen->status_aktif;
			    $save_trans['agen_nik'] = $detail_agen->nik;
			    $save_trans['agen_tgl_register'] = $detail_agen->tgl_register;
			    $save_trans['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
			    $save_trans['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
			    $save_trans['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
			    $save_trans['agen_agama'] = $detail_agen->agama;
			    $save_trans['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
			    $save_trans['agen_pekerjaan'] = $detail_agen->pekerjaan;
			    $save_trans['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
			    $save_trans['agen_prov'] = $detail_agen->prov;
			    $save_trans['agen_kab'] = $detail_agen->kab;
			    $save_trans['agen_kec'] = $detail_agen->kec;
			    $save_trans['agen_kel'] = $detail_agen->kel;
			    $save_trans['agen_no_hp'] = $detail_agen->no_hp;
			    $save_trans['agen_nama'] = $detail_agen->nama;
			    $save_trans['agen_email'] = $detail_agen->email;
			    $save_trans['agen_level'] = $detail_agen->status_agen;
			    $save_trans['agen_kode'] = $detail_agen->kode;
			    $save_trans['bank_nama'] = $detail_bank->nama;
			    $save_trans['bank_kode'] = $detail_bank->kode;

			    $save_trans_topup = DB::table('trans_topup')
			    	->insertGetId($save_trans);

			    if($save_trans_topup) {
			    	Esta::log_money($transaction_data->id_agen,$data_sprint['transactionAmount']-$save_trans['biaya_admin_bank']-$save_trans['biaya_admin'],date('Y-m-d H:i:s'),'Top Up','Top Up '.$transaction_data->customerAccount,'In','Transaksi','trans_topup',$save_trans_topup);
			    	Esta::log_money($transaction_data->id_agen,0,date('Y-m-d H:i:s'),'Agen Top Up','Agen Top Up '.$save_trans['trans_no'],'','Riwayat Agen','trans_topup',$save_trans_topup);
			    }

			    /*$save_flag['created_at'] = date('Y-m-d H:i:s');
				$save_flag['transactionNo'] = $data_sprint['transactionNo'];
			    $save_flag['currency'] = $data_sprint['currency'];
			    $save_flag['transactionAmount'] = $data_sprint['transactionAmount'];
			    $save_flag['transactionDate'] = $data_sprint['transactionDate'];
			    $save_flag['transactionExpire'] = $data_sprint['transactionExpired'];
			    $save_flag['customerAccount'] = $transaction_data->customerAccount;
			    $save_flag['customerName'] = $transaction_data->customerName;
			    $save_flag['create_user'] = 'SPRINT';
			    $save_flag['id_bank'] = $id_bank;
			    $save_flag['status'] = $data_sprint['transactionStatus'];
			    $save_flag['statusmessage'] = $data_sprint['transactionMessage'];
			    $save_flag['insertId'] = $data_sprint['insertId'];
			    $save_flag['paymentReffId'] = $data_sprint['paymentReffId'];

			    $save_flag = DB::table('tu_flag_sprint')
				    	->insert($save_flag);*/
			} else {
				$resp['paymentStatus']  = "01";
		   		$resp['paymentMessage'] = "Invalid Transaction";

				$save_flag1['created_at'] = date('Y-m-d H:i:s');
				$save_flag1['created_user'] = 'SPRINT';
				$save_flag1['updated_user'] = 'SPRINT';
			    $save_flag1['transactionNo'] = $data_sprint['transactionNo'];
			    $save_flag1['currency'] = $data_sprint['currency'];
			    $save_flag1['transactionAmount'] = $data_sprint['transactionAmount'];
			    $save_flag1['transactionDate'] = $data_sprint['transactionDate'];
			    $save_flag1['transactionExpire'] = $data_sprint['transactionExpired'];
			    $save_flag1['customerAccount'] = $transaction_data->customerAccount;
			    $save_flag1['customerName'] = $transaction_data->customerName;
			    //$save_flag1['create_user'] = 'SPRINT';
			    $save_flag1['id_bank'] = $id_bank;
			    $save_flag1['status'] = $resp['paymentStatus'];
			    $save_flag1['statusmessage'] = $resp['paymentMessage'];
			    $save_flag1['insertId'] = $data_sprint['insertId'];
			    $save_flag1['paymentReffId'] = $data_sprint['paymentReffId'];

			    $sv_flag2 = DB::table('tu_flag_sprint')
				    	->insert($save_flag1);
			}

		}

		$save_flag2['created_at'] = date('Y-m-d H:i:s');
		$save_flag2['created_user'] = 'SPRINT';
		$save_flag2['updated_user'] = 'SPRINT';
	    $save_flag2['transactionNo'] = $data_sprint['transactionNo'];
	    $save_flag2['currency'] = $data_sprint['currency'];
	    $save_flag2['transactionAmount'] = $data_sprint['transactionAmount'];
	    $save_flag2['transactionDate'] = $data_sprint['transactionDate'];
	    $save_flag2['transactionExpire'] = $data_sprint['transactionExpired'];
	    $save_flag2['customerAccount'] = $transaction_data->customerAccount;
	    $save_flag2['customerName'] = $transaction_data->customerName;
	    //$save_flag2['create_user'] = 'SPRINT';
	    $save_flag2['id_bank'] = $id_bank;
	    $save_flag2['status'] = $resp['paymentStatus'];
	    $save_flag2['statusmessage'] = $resp['paymentMessage'];
	    $save_flag2['insertId'] = $data_sprint['insertId'];
	    $save_flag2['paymentReffId'] = $data_sprint['paymentReffId'];

	    $sv_flag2 = DB::table('tu_flag_sprint')
		    	->insert($save_flag2);


		return response()->json($resp);
	}

	public function postDokuInquiry() {
		date_default_timezone_set("Asia/Jakarta");
		$MALL_ID	= CRUDBooster::getsetting('mall_id');
		$SHARED_KEY	= CRUDBooster::getsetting('shared_key');
		$CHAINMERCHANT = CRUDBooster::getsetting('chainmerchant');

		// Parsing Data Request From DOKU
		$data_doku = array(
		    "MALLID"			=> $_REQUEST['MALLID'],
		    "CHAINMERCHANT"		=> $_REQUEST['CHAINMERCHANT'],
		    "PAYMENTCHANNEL"	=> $_REQUEST['PAYMENTCHANNEL'],
		    "PAYMENTCODE"		=> $_REQUEST['PAYMENTCODE'],
		    "WORDS"				=> $_REQUEST['WORDS']
		);

		$get_va = DB::table('agen_va')
			->where('no_va',$_REQUEST['PAYMENTCODE'])
			->where('deleted_at',NULL)
			->first();
		$customerName = DB::table('agen')->where('id',$get_va->id_agen)->first();

		$words   = sha1(trim($MALL_ID).trim($SHARED_KEY).trim($_REQUEST['PAYMENTCODE']));
		$req_dtm = date('YmdHis');

		if(empty($get_va)){
			$response_customerName = "";
			$response_code = "3006";
		}else if($_REQUEST['WORDS']!=$words){
			$response_customerName = "";
			$response_code = "3001";
		}else{
			$response_customerName = $customerName->nama;
			$response_code = "0000";
		}

		$uniqid=uniqid();
		// Save the inquiry data as a new transaction on your database
		$request['payment_channel'] = $data_doku['PAYMENTCHANNEL'];
		$request['payment_code'] = $data_doku['PAYMENTCODE'];
		$request['words'] = $data_doku['WORDS'];
		$request['status'] = 'NEW';
		//$request['create_user'] = 'DOKU';
		$request['create_dtm'] = NOW();
		$request['created_at'] = NOW();
		$request['updated_user'] = NOW();
		$request['created_user'] = 'DOKU';
		$request['updated_user'] = 'DOKU';

		$sv_req = DB::table('tu_req_doku')
			->insert($request);

		// Save the response inquiry data as a new transaction on your database
		$request['payment_code'] = $get_va->no_va;
		$request['trans_id_merchant'] = $req_dtm;
		$request['words'] = $words;
		$request['request_dtm'] = $req_dtm;
		$request['currency'] = '360';
		$request['session_id'] = $uniqid;
		$request['name'] = $customerName->nama;
		$request['response_code'] = $response_code;
		//$request['create_user'] = 'SYSTEM';
		$request['create_dtm'] = NOW();
		$request['created_at'] = NOW();
		$request['updated_user'] = NOW();
		$request['created_user'] = 'SYSTEM';
		$request['updated_user'] = 'SYSTEM';

		$sv_req = DB::table('tu_res_doku')
			->insert($request);

		header("Content-type: text/xml");
		echo "<?xml version='1.0'?>";
		echo "<INQUIRY_RESPONSE>";
		echo "<PAYMENTCODE>".$_REQUEST['PAYMENTCODE']."</PAYMENTCODE>";
		echo "<AMOUNT>0.00</AMOUNT>";
		//echo "<MINAMOUNT>".CRUDBooster::getsetting('minimal_topup').".00</MINAMOUNT>";
		echo "<MINAMOUNT>".CRUDBooster::getsetting('minimal_topup').".00</MINAMOUNT>";
		echo "<MAXAMOUNT>10000000.00</MAXAMOUNT>";
		echo "<PURCHASEAMOUNT>0.00</PURCHASEAMOUNT>";
		echo "<TRANSIDMERCHANT>".$req_dtm."</TRANSIDMERCHANT>";
		echo "<WORDS>".$words."</WORDS>";
		echo "<REQUESTDATETIME>".$req_dtm."</REQUESTDATETIME>";
		echo "<CURRENCY>360</CURRENCY>";
		echo "<PURCHASECURRENCY>360</PURCHASECURRENCY>";
		echo "<SESSIONID>".$uniqid."</SESSIONID>";
		echo "<NAME>".$customerName->nama."</NAME>";
		echo "<EMAIL>".$customerName->email."</EMAIL>";
		echo "<BASKET>Top Up Saldo,0.00,0,0.00</BASKET>";
		echo "<ADDITIONALDATA>TOPUP</ADDITIONALDATA>";
		echo "<RESPONSECODE>".$response_code."</RESPONSECODE>";
		echo "</INQUIRY_RESPONSE>";
	}

	public function postDokuNotify() {
		date_default_timezone_set("Asia/Jakarta");
		$MALL_ID	= CRUDBooster::getsetting('mall_id');
		$SHARED_KEY	= CRUDBooster::getsetting('shared_key');
		$CHAINMERCHANT = CRUDBooster::getsetting('chainmerchant');

		// Parsing request data from DOKU
		$data_doku = array(
		    "AMOUNT"			=> isset($_REQUEST['AMOUNT']) ? $_REQUEST['AMOUNT'] : '',
		    "TRANSIDMERCHANT"	=> isset($_REQUEST['TRANSIDMERCHANT']) ? $_REQUEST['TRANSIDMERCHANT'] : '',
		    "WORDS"				=> isset($_REQUEST['WORDS']) ? $_REQUEST['WORDS'] : '',
		    "STATUSTYPE"		=> isset($_REQUEST['STATUSTYPE']) ? $_REQUEST['STATUSTYPE'] : '',
		    "RESPONSECODE"		=> isset($_REQUEST['RESPONSECODE']) ? $_REQUEST['RESPONSECODE'] : '',
			"APPROVALCODE"		=> isset($_REQUEST['APPROVALCODE']) ? $_REQUEST['APPROVALCODE'] : '',
		    "RESULTMSG"			=> isset($_REQUEST['RESULTMSG']) ? $_REQUEST['RESULTMSG'] : '',
		    "PAYMENTCHANNEL"	=> isset($_REQUEST['PAYMENTCHANNEL']) ? $_REQUEST['PAYMENTCHANNEL'] : '',
		    "PAYMENTCODE"		=> isset($_REQUEST['PAYMENTCODE']) ? $_REQUEST['PAYMENTCODE'] : '',
		    "SESSIONID"			=> isset($_REQUEST['SESSIONID']) ? $_REQUEST['SESSIONID'] : '',
			"BANK"				=> isset($_REQUEST['BANK']) ? $_REQUEST['BANK'] : '',
		    "MCN"				=> isset($_REQUEST['MCN']) ? $_REQUEST['MCN'] : '',
		    "PAYMENTDATETIME"	=> isset($_REQUEST['PAYMENTDATETIME']) ? $_REQUEST['PAYMENTDATETIME'] : '',
		    "VERIFYID"			=> isset($_REQUEST['VERIFYID']) ? $_REQUEST['VERIFYID'] : '',
		    "VERIFYSCORE"		=> isset($_REQUEST['VERIFYSCORE']) ? $_REQUEST['VERIFYSCORE'] : '',
			"VERIFYSTATUS"		=> isset($_REQUEST['VERIFYSTATUS']) ? $_REQUEST['VERIFYSTATUS'] : '',
		    "CURRENCY"			=> isset($_REQUEST['CURRENCY']) ? $_REQUEST['CURRENCY'] : '',
		    "PURCHASECURRENCY"	=> isset($_REQUEST['PURCHASECURRENCY']) ? $_REQUEST['PURCHASECURRENCY'] : '',
		    "BRAND"				=> isset($_REQUEST['BRAND']) ? $_REQUEST['BRAND'] : '',
		    "CHNAME"			=> isset($_REQUEST['CHNAME']) ? $_REQUEST['CHNAME'] : '',
			"THREEDSECURESTATUS"=> isset($_REQUEST['THREEDSECURESTATUS']) ? $_REQUEST['THREEDSECURESTATUS'] : '',
		    "LIABILITY"			=> isset($_REQUEST['LIABILITY']) ? $_REQUEST['LIABILITY'] : '',
		    "EDUSTATUS"			=> isset($_REQUEST['EDUSTATUS']) ? $_REQUEST['EDUSTATUS'] : '',
		    "CUSTOMERID"		=> isset($_REQUEST['CUSTOMERID']) ? $_REQUEST['CUSTOMERID'] : '',
		    "TOKENID"			=> isset($_REQUEST['TOKENID']) ? $_REQUEST['TOKENID'] : '',
		);

		$sv['amount'] = $data_doku['AMOUNT'];
		$sv['trans_id_merchant'] = $data_doku['TRANSIDMERCHANT'];
		$sv['words'] = $data_doku['WORDS'];
		$sv['status_type'] = $data_doku['STATUSTYPE'];
		$sv['response_code'] = $data_doku['RESPONSECODE'];
		$sv['approval_code'] = $data_doku['APPROVALCODE'];
		$sv['result_msg'] = $data_doku['RESULTMSG'];
		$sv['payment_channel'] = $data_doku['PAYMENTCHANNEL'];
		$sv['payment_code'] = $data_doku['PAYMENTCODE'];
		$sv['session_id'] = $data_doku['SESSIONID'];
		$sv['bank'] = $data_doku['BANK'];
		$sv['mcn'] = $data_doku['MCN'];
		$sv['payment_dtm'] = $data_doku['PAYMENTDATETIME'];
		$sv['verify_id'] = $data_doku['VERIFYID'];
		$sv['verify_score'] = $data_doku['VERIFYSCORE'];
		$sv['verify_status'] = $data_doku['VERIFYSTATUS'];
		$sv['currency'] = $data_doku['CURRENCY'];
		$sv['purchase_currency'] = $data_doku['PURCHASECURRENCY'];
		$sv['brand'] = $data_doku['BRAND'];
		$sv['ch_name'] = $data_doku['CHNAME'];
		$sv['threed_secure_status'] = $data_doku['THREEDSECURESTATUS'];
		$sv['liability'] = $data_doku['LIABILITY'];
		$sv['edu_status'] = $data_doku['EDUSTATUS'];
		$sv['customer_id'] = $data_doku['CUSTOMERID'];
		$sv['token_id'] = $data_doku['TOKENID'];
		//$sv['create_user'] = 'DOKU';
		$sv['create_dtm'] = NOW();
		$sv['created_at'] = NOW();
		$sv['updated_user'] = NOW();

		$save = DB::table('tu_flag_doku')
			->insert($sv);

		/*transaksi*/
		$get_va = DB::table('agen_va')
			->where('no_va',$sv['payment_code'])
			->where('deleted_at',NULL)
			->first();

		$detail_agen = DB::table('agen')->where('id',$get_va->id_agen)->first();
	    $detail_bank = DB::table('bank')->where('id',4)->first();
	    $kode = Esta::nomor_transaksi('trans_topup',CRUDBooster::getsetting('transaksi_top_up'));

	    $save_trans['trans_no'] = $kode;//
	    $save_trans['created_at'] = date('Y-m-d H:i:s');
	    $save_trans['updated_at'] = date('Y-m-d H:i:s');
	    $save_trans['created_user'] = 'DOKU';
		$save_trans['updated_user'] = 'DOKU';
	    $save_trans['ref_trans_no'] = $kode;
	    $save_trans['trans_date'] = date('Y-m-d H:i:s');
	    $save_trans['trans_desc'] = 'Top up VA';
	    $save_trans['currency'] = $sv['currency'];
	    //$save_trans['flg_plus_min'] = '1';
	    $save_trans['trans_amount'] = str_replace('.00', '', $sv['amount']);
	    $save_trans['trans_type_code'] = 'VA';
	    $save_trans['va_no'] = $sv['payment_code'];
	    //$save_trans['create_user'] = 'DOKU';
	    $save_trans['batch_last_user'] = 'DOKU';
	    $save_trans['id_bank'] = 4;
	    $save_trans['id_agen'] = $detail_agen->id;
	    $save_trans['biaya_admin_bank'] = DB::table('bank')->where('id',4)->first()->biaya_admin_bank;
	    $save_trans['biaya_admin'] = DB::table('bank')->where('id',4)->biaya_admin;
	    $save_trans['agen_referall'] = $detail_agen->kode_referall_agen;
	    $save_trans['agen_referall_relation'] = $detail_agen->kode_relation_referall;
	    $save_trans['agen_status_aktif'] = $detail_agen->status_aktif;
	    $save_trans['agen_nik'] = $detail_agen->nik;
	    $save_trans['agen_tgl_register'] = $detail_agen->tgl_register;
	    $save_trans['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
	    $save_trans['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
	    $save_trans['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
	    $save_trans['agen_agama'] = $detail_agen->agama;
	    $save_trans['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
	    $save_trans['agen_pekerjaan'] = $detail_agen->pekerjaan;
	    $save_trans['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
	    $save_trans['agen_prov'] = $detail_agen->prov;
	    $save_trans['agen_kab'] = $detail_agen->kab;
	    $save_trans['agen_kec'] = $detail_agen->kec;
	    $save_trans['agen_kel'] = $detail_agen->kel;
	    $save_trans['agen_no_hp'] = $detail_agen->no_hp;
	    $save_trans['agen_nama'] = $detail_agen->nama;
	    $save_trans['agen_email'] = $detail_agen->email;
	    $save_trans['agen_level'] = $detail_agen->status_agen;
	    $save_trans['agen_kode'] = $detail_agen->kode;
	    $save_trans['bank_nama'] = $detail_bank->nama;
	    $save_trans['bank_kode'] = $detail_bank->kode;

	    $save_trans_topup = DB::table('trans_topup')
	    	->insertGetId($save_trans);

	    if($save_trans_topup) {
	    	Esta::log_money($detail_agen->id,$save_trans['trans_amount']-$save_trans['biaya_admin_bank']-$save_trans['biaya_admin'],date('Y-m-d H:i:s'),'Top Up','Top Up '.$detail_agen->nama,'In','Transaksi','trans_topup',$save_trans_topup);
	    	Esta::log_money($detail_agen->id,0,date('Y-m-d H:i:s'),'Agen Top Up','Agen Top Up '.$save_trans['trans_no'],'','Riwayat Agen','trans_topup',$save_trans_topup);
	    }

		echo "CONTINUE";
	}








}












