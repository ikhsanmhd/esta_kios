<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;

use Illuminate\Support\Facades\Auth;

class RegistrasiAgenPremiumAPI extends ApiController
{

    public function checkAgenPremium(){
    	//$agen_id=Request::get('id_agen');
        $agen = Auth::user();
        $agen_id = $agen->id;
    	$checkagen = DB::select('exec CheckAgenKios ?',array($agen_id));  
        $getAgen = DB::select('exec getAgenById ?', array($agen_id))[0];
        $getVerify = DB::table('agen_verify_premium')->where('id_agen',$agen_id);
		
        $getAgenVerify = DB::table('agen_verify_premium')->where('id_agen',$agen_id)->first();
        if($getAgen->status_agen == 'Premium'){
            $is_premium = 1;
        }elseif($getVerify->exists()){
            if($getAgenVerify->foto_ktp_status == 'Rejected' && $getAgenVerify->foto_rekening_status == 'Rejected' && $getAgenVerify->foto_selfie_status == 'Rejected'){
                $is_premium = 3;
            }else{
                $is_premium = 2;
            }
        }else{
            $is_premium = 0;
        }
        $getLimitCart = DB::table('mst_duedate')->where('parameter','=','limit_keep_data_on_cart')->first();
		$shareProduct = CRUDBooster::getsetting('share_product');
		$shareTransactionKios = CRUDBooster::getsetting('Share_Transaction_Kios');
    	if($checkagen[0]->status == 1){
            $data['is_agen'] = 1;
            $data['nama_kios'] = $checkagen[0]->nama_kios;
            $data['kode_kios'] = $checkagen[0]->agenkios_code;
			$data['is_premium'] = $is_premium;
            $data['cart_autoremove_day'] = $getLimitCart->nilai_limit;
			$data['share_product_description'] = $shareProduct;
			$data['share_transaction_pos_description'] = $shareTransactionKios;
            return $this->respondWithDataAndMessage($data, "Agen Kios");
    	}elseif($checkagen[0]->status == 2) {
            $data['is_agen'] = 2;
            $data['nama_kios'] = $checkagen[0]->nama_kios;
            $data['kode_kios'] = $checkagen[0]->agenkios_code;
			$data['is_premium'] = $is_premium;
            $data['cart_autoremove_day'] = $getLimitCart->nilai_limit;
			$data['share_product_description'] = $shareProduct;
			$data['share_transaction_pos_description'] = $shareTransactionKios;
            return $this->respondWithDataAndMessage($data, "Agen belum menjadi Agen Kios, Mohon menunggu verifikasi");
        }else {
            $data['is_agen'] = 0;
            $data['nama_kios'] = '';
            $data['kode_kios'] = '';
			$data['is_premium'] = $is_premium;
            $data['cart_autoremove_day'] = $getLimitCart->nilai_limit;
			$data['share_product_description'] = $shareProduct;
			$data['share_transaction_pos_description'] = $shareTransactionKios;
            return $this->respondWithDataAndMessage($data, "Agen Belum terdaftar sebagai Agen Kios");
        }
    }

    public function getQuiz(){
        $getQuizs = DB::select('exec getQuizList');
        $quiz = [];
        foreach ($getQuizs as $getQuiz) {        
            $getJawabans = DB::select('exec getDetailJawabanQuiz ?', array($getQuiz->id));
            if($getQuiz->type == 'Pilihan Ganda' && $getJawabans == NULL){

            }else{
                $options = [];
                foreach ($getJawabans as $jawaban) {
                    $options[] = [
                        'answer' => $jawaban->answer,
                        'score' => $jawaban->score
                    ]; 
                }
                
                $quiz [] = [
                    'id' => $getQuiz->id,
                    'type' => $getQuiz->type,
                    'question' => $getQuiz->question,
                    'options' => $options            
                ];
            }
        }
        
    	return $this->respondWithDataAndMessage($quiz, "Success");
    }

    public function postRegistrasiForm(){
    	$agen_id = Request::get('agen_id');
        $nama_kios = Request::get('nama_kios');
    	$latitude = Request::get('latitude');
    	$longitude = Request::get('longitude');
    	$provinsi_id = Request::get('provinsi_id');
    	$kabupaten_id = Request::get('kabupaten_id');
    	$kecamatan_id = Request::get('kecamatan_id');
    	$kelurahan_id = Request::get('kelurahan_id');
    	$rt = Request::get('rt');
    	$rw = Request::get('rw');
    	$store_address = Request::get('store_address');
    	$answers = Request::get('answers');
        $status = "New";
        $created_at = date('Y-m-d H:i:sa');
        $is_agen_kios = false;

        $prefix_pengajuan_agen = CRUDBooster::getsetting('kode_agen_kios');
        $prefix_agen_kios_code = CRUDBooster::getsetting('agen_kios_code');
        /* $last_id = \App\Premiumagen::limit(1)->orderBy('id','DESC')->first();
        $lastkode = $last_id->kode_pengajuan_agen;
        $kode_pengajuan_agen = $prefix.date('y').date('m').sprintf("%06s", substr($lastkode, -6)+1); */
		if($agen_id == NULL || $agen_id == 0){
			return $this->respondWithSuccess("Terjadi gangguan pada koneksi Internet atau Server. Silahkan coba beberapa saat lagi");
		}
        $checkagen = DB::table('txn_agenkios')->where('agen_id', $agen_id)->where('status', 'NOT LIKE','Rejected')->orderBy('id', 'desc');
        if(!$checkagen->exists()){
			$flag_registrasi = 5;
            $check_agen = DB::table('agen')
                ->where('id',$agen_id)
                ->where('kode','!=',NULL)
                ->whereNull('deleted_at')
                ->first(); 
            $check_pengajuan = DB::table('txn_pengajuan_agen')
                ->where('agen_no_hp',$check_agen->no_hp)
                ->where('kode_pengajuan_agen','!=',NULL)
                ->whereNull('deleted_at')
                ->orderBy('id', 'DESC')
                ->first();   
			
			//INSERT TO DB
            try{
                DB::beginTransaction();
                $txn_premiumagen_id = DB::select('exec PostRegistrasiAK ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?',array($agen_id,$nama_kios,$latitude,$longitude,$store_address,$provinsi_id,$kelurahan_id,$kecamatan_id,$kabupaten_id,$rt,$rw,$status,$created_at,$is_agen_kios,$prefix_pengajuan_agen,$prefix_agen_kios_code,$flag_registrasi));

            	foreach($answers as $answer){
                    $premiumagen_id = $txn_premiumagen_id[0]->id;
            		$quiz_id = $answer['quiz_id'];
            		$question = $answer['question'];
            		$answerr = $answer['answer'];
            		$score = $answer['score'];
            		$type = $answer['type'];

                    $answer_save = DB::select('exec PostRegistrasiQuizAK '.$premiumagen_id.','.$quiz_id.',\''.$question.'\',\''.$answerr.'\','.$score.',\''.$type.'\'');
            	}					

                if(!empty($answer_save[0]->id_quiz)){
					if(!empty($check_pengajuan)){
						$updateagen['flag_registrasi'] = $flag_registrasi;
						$updateagen['is_existing'] = 0;
						$update_flag = DB::table('txn_pengajuan_agen')
								  ->where('id',$check_pengajuan->id)
								  ->update($updateagen);
					}
					if(!empty($check_agen)){
						$updateagen['flag_registrasi'] = $flag_registrasi;
						$updateagen['is_existing'] = 0;
						$update_flag = DB::table('agen')
								  ->where('id',$agen_id)
								  ->update($updateagen);
					}
                    DB::commit();					
                    return $this->respondWithSuccess("Data Berhasil Di Submit");
                }else{
					DB::rollBack();
                    return $this->respondWithError("Terjadi gangguan pada koneksi Internet atau Server. Silahkan coba beberapa saat lagi");
                }
            }catch(\Exception $e) {
                DB::rollBack();
                return $this->respondWithError("Terjadi gangguan pada koneksi Internet atau Server. Silahkan coba beberapa saat lagi");
            }    	
        }else{
            return $this->respondWithError("Gagal. Agen sudah memiliki kios.");
        }
    }

    public function getRegistrasiForm(){
        $agen = Auth::user();
        $agen_id = $agen->id;
        //$agen_id = Request::get('agen_id');
        $agen = DB::table('agen')->where('id', $agen_id)->first();
        /* $premium_agen = \App\Premiumagen::where('agen_id', $agen_id)
                        ->where('status', 'NOT LIKE','Rejected')->orderBy('id', 'desc')->first(); */
        $premium_agen = DB::select('exec getAgenKiosRegistrasiById ?', array($agen_id))[0];
       
            $answers = [];
            $quizResults = DB::select('exec getQuizResult ?', array($premium_agen->id));
            foreach ($quizResults as $dtlQuizresult) {
                $answers[] = [
                    'quiz_id' => $dtlQuizresult->quiz_id,
                    'question' => $dtlQuizresult->question,
                    'answer' => $dtlQuizresult->answer,
                    'score' => $dtlQuizresult->score,
                    'type' => $dtlQuizresult->type
                ];
            }
            $location_name = $premium_agen->provinsi->name.', '.$premium_agen->kabupaten->name.', '.$premium_agen->kecamatan->name.', '.$premium_agen->kelurahan->name;
                
            $form = [
                'agen_id' => $premium_agen->agen_id,
                'nama_kios' => $premium_agen->nama_kios,
                'provinsi_id' => $premium_agen->provinsi_id,
                'kabupaten_id' => $premium_agen->kabupaten_id,
                'kecamatan_id' => $premium_agen->kecamatan_id,
                'kelurahan_id' => $premium_agen->kelurahan_id,
                'location' => $premium_agen->location,
                'latitude' => $premium_agen->lat,
                'longitude' => $premium_agen->lng,
                'rt' => $premium_agen->RT,
                'rw' => $premium_agen->RW,
                'store_address' => $premium_agen->store_address,
                'answers' => $answers
            ];
        return $this->respondWithDataAndMessage($form, "Success");

    }

    public function getProvinsi(){
        $provinsis = DB::select('exec getProvinsiList');
        $data = [];
        foreach ($provinsis as $provinsi) {
            $data[] = [
                'id' => $provinsi->id,
                'name' => $provinsi->name
            ];
        }        
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getKabupaten(Request $request, $provinsi_id){
        $kabupatens = DB::select('exec getKabupatenList ?', array($provinsi_id));
        $data = [];
        foreach ($kabupatens as $kabupaten) {
            $data[] = [
                'id' => $kabupaten->id,
                'name' => $kabupaten->name
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getKecamatan(Request $request, $kabupaten_id){
        $kecamatans = DB::select('exec getKecamatanList ?', array($kabupaten_id));
        $data = [];
        foreach ($kecamatans as $kecamatan) {
            $data[] = [
                'id' => $kecamatan->id,
                'name' => $kecamatan->name
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getKelurahan(Request $request, $kecamatan_id){
        $kelurahans = DB::select('exec getKelurahanList ?', array($kecamatan_id));
        $data = [];
        foreach ($kelurahans as $kelurahan) {
            $data[] = [
                'id' => $kelurahan->id,
                'name' => $kelurahan->name,
                'full_name' => $kelurahan->full_name 
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getLocation(){
        $query = Request::get('query');
        $provinsis = DB::select('exec getLocation ?', array($query));
        
        $data = [];
        foreach ($provinsis as $provinsi) {
            $names = $provinsi->provinsiname.', '.$provinsi->kabupatenname.', '.$provinsi->kecamatanname.', '.$provinsi->kelurahanname;
            $data[] = [
                'name' => $names,
                'provinsi_id' => $provinsi->id_i_provinsi,
                'kabupaten_id' => $provinsi->id_i_kabupaten,
                'kecamatan_id' => $provinsi->id_i_kecamatan,
                'kelurahan_id' => $provinsi->kelurahan_id
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }
}