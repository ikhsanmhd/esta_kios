<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;

use Illuminate\Support\Facades\Auth;

class RegistrasiAgenPremiumAPI extends ApiController
{
    //

    public function checkAgenPremium(){
    	//$agen_id=Request::get('id_agen');
        $agen = Auth::user();
        $agen_id = $agen->id;
    	$checkagen = DB::select('exec CheckAgenKios ?',array($agen_id));    	
        

    	if($checkagen[0]->status == 1){
            $data = 1;
            return $this->respondWithDataAndMessage($data, "Agen Kios");
    	}elseif($checkagen[0]->status == 2) {
            $data = 2;
            return $this->respondWithDataAndMessage($data, "Agen belum menjadi Agen Kios, Mohon menunggu verifikasi");
        }else {
            $data = 0;
            return $this->respondWithDataAndMessage($data, "Agen Belum terdaftar sebagai Agen Kios");
        }
    }

    public function getQuiz(){
    	$getQuizs = \App\MstQuiz::get();
        $quiz = [];
        foreach ($getQuizs as $getQuiz) {
            $options = [];
                /*$options[] = [
                    'answer1' => $getQuiz->answer1,
                    'score1' => $getQuiz->score1,
                    'answer2' => $getQuiz->answer2,
                    'score2' => $getQuiz->score2,
                    'answer3' => $getQuiz->answer3,
                    'score3' => $getQuiz->score3,
                    'answer4' => $getQuiz->answer4,
                    'score4' => $getQuiz->score4,
                    'answer5' => $getQuiz->answer5,
                    'score5' => $getQuiz->score5
                ];*/

            if(isset($getQuiz->answer1)) {
                $options[] = [                    
                    'answer' => $getQuiz->answer1,
                    'score' => $getQuiz->score1,
                ];
            }
            if(isset($getQuiz->answer2)) {
                $options[] = [
                    'answer' => $getQuiz->answer2,
                    'score' => $getQuiz->score2,
                ];
            }
            if(isset($getQuiz->answer3)) {
                $options[] = [
                    'answer' => $getQuiz->answer3,
                    'score' => $getQuiz->score3,
                ];
            }
            if(isset($getQuiz->answer4)) {
                $options[] = [
                
                    'answer' => $getQuiz->answer4,
                    'score' => $getQuiz->score4,
                ];
            }
            if(isset($getQuiz->answer5)) {
                $options[] = [
                    'answer' => $getQuiz->answer5,
                    'score' => $getQuiz->score5,                
                ];
            }
            
            $quiz [] = [
                'id' => $getQuiz->id,
                'type' => $getQuiz->type,
                'question' => $getQuiz->question,
                'options' => $options            
            ];
        }
        
    	return $this->respondWithDataAndMessage($quiz, "Success");
    }

    public function postRegistrasiForm(){
    	$id_agen = Request::get('agen_id');
        $nama_kios = Request::get('nama_kios');
    	$latitude = Request::get('latitude');
    	$longitude = Request::get('longitude');
    	$provinsi_id = Request::get('provinsi_id');
    	$kabupaten_id = Request::get('kabupaten_id');
    	$kecamatan_id = Request::get('kecamatan_id');
    	$kelurahan_id = Request::get('kelurahan_id');
    	$rt = Request::get('rt');
    	$rw = Request::get('rw');
    	$store_address = Request::get('store_address');
    	$answers = Request::get('answers');
		
		//prefix(2)+thn(2)+bln(2)+6digit autoincrement
			//$lastdigit = '000000';
        $prefix = CRUDBooster::getsetting('kode_agen_kios');
        $last_id = \App\Premiumagen::limit(1)->orderBy('id','DESC')->first();
        $lastkode = $last_id->kode_pengajuan_agen;
        $kode_pengajuan_agen = $prefix.date('y').date('m').sprintf("%06s", substr($lastkode, -6)+1);
        //dd($kode_pengajuan_agen);

        $checkagen = \App\Premiumagen::where('agen_id', $id_agen)->where('status', 'NOT LIKE','Rejected')->orderBy('id', 'desc');
        //$restjson = array();
        if(!$checkagen->exists()){
        	$txn_premiumagen['agen_id'] = $id_agen;
            $txn_premiumagen['nama_kios'] = $nama_kios;
        	$txn_premiumagen['lat'] = $latitude;
        	$txn_premiumagen['lng'] = $longitude;
        	$txn_premiumagen['store_address'] = $store_address;
        	$txn_premiumagen['provinsi_id'] = $provinsi_id;
        	$txn_premiumagen['kelurahan_id'] = $kelurahan_id;
        	$txn_premiumagen['kecamatan_id'] = $kecamatan_id;
        	$txn_premiumagen['kabupaten_id'] = $kabupaten_id;
        	$txn_premiumagen['rt'] = $rt;
        	$txn_premiumagen['rw'] = $rw;
            $txn_premiumagen['status'] = "New";
            $txn_premiumagen['is_agen_kios'] = false;
            $txn_premiumagen['created_at'] = date('Y-m-d H:i:sa');
			$txn_premiumagen['kode_pengajuan_agen'] = $kode_pengajuan_agen;
        	$txn_premiumagen_id = \App\Premiumagen::insertGetId($txn_premiumagen);

        	foreach($answers as $answer){
        		$quiz_id = $answer['quiz_id'];
        		$question = $answer['question'];
        		$answerr = $answer['answer'];
        		$score = $answer['score'];
        		$type = $answer['type'];

        		$answer['premiumagen_id'] = $txn_premiumagen_id;
        		$answer['quiz_id'] = $quiz_id;
        		$answer['question'] = $question;
        		$answer['answer'] = $answerr;
        		$answer['score'] = $score;
        		$answer['type'] = $type;
        		$answer_save = \App\DtlQuizresult::insert($answer);
        	}   

            if($answer_save){
                return $this->respondWithSuccess("Data Berhasil Di Submit");
        	    //return $this->respondWithDataAndMessage($restjson, "Data Berhasil Di Submit");
            }    	
        }else{
            //$premium_agenDB = \App\Premiumagen::where('agen_id', $id_agen)->where('status', 'NOT LIKE','Rejected')
            /*$txn_premiumagen['lat'] = $latitude;
            $txn_premiumagen['lng'] = $longitude;
            $txn_premiumagen['store_address'] = $store_address;
            $txn_premiumagen['provinsi_id'] = $provinsi_id;
            $txn_premiumagen['kelurahan_id'] = $kelurahan_id;
            $txn_premiumagen['kecamatan_id'] = $kecamatan_id;
            $txn_premiumagen['kabupaten_id'] = $kabupaten_id;
            $txn_premiumagen['rt'] = $rt;
            $txn_premiumagen['rw'] = $rw;
            $txn_premiumagen['status'] = "New Agen Kios";
            $txn_premiumagen['is_agenpremium'] = false;
            $txn_premiumagen['created_at'] = date('Y-m-d H:i:sa');
            $txn_premiumagen_update = \App\Premiumagen::where('agen_id', $id_agen)->where('status', 'NOT LIKE','Rejected')->update($txn_premiumagen);*/

            /*foreach($answers as $answer){
                $quiz_id = $answer['quiz_id'];
                $question = $answer['question'];
                $answerr = $answer['answer'];
                $score = $answer['score'];
                $type = $answer['type'];

                $answer['premiumagen_id'] = $checkagen->id;
                $answer['quiz_id'] = $quiz_id;
                $answer['question'] = $question;
                $answer['answer'] = $answerr;
                $answer['score'] = $score;
                $answer['type'] = $type;
                $answer_update = \App\DtlQuizresult::where('premiumagen_id', $checkagen->id)->update($answer);
            }*/
            return $this->respondWithError("Data Sudah Ada");
        }
    }

    public function getRegistrasiForm(){
        $agen = Auth::user();
        $agen_id = $agen->id;
        //$agen_id = Request::get('agen_id');
        $agen = \App\Agen::where('id', $agen_id)->first();
        $premium_agen = \App\Premiumagen::where('agen_id', $agen_id)
                        ->where('status', 'NOT LIKE','Rejected')->orderBy('id', 'desc')->first();
        //dd($premium_agen);
       
            $answers = [];
            foreach ($premium_agen->dtlQuizresult as $dtlQuizresult) {
                $answers[] = [
                    'quiz_id' => $dtlQuizresult->quiz_id,
                    'question' => $dtlQuizresult->question,
                    'answer' => $dtlQuizresult->answer,
                    'score' => $dtlQuizresult->score,
                    'type' => $dtlQuizresult->type
                ];
            }
            $location_name = $premium_agen->provinsi->name.', '.$premium_agen->kabupaten->name.', '.$premium_agen->kecamatan->name.', '.$premium_agen->kelurahan->name;
                
            $form = [
                'agen_id' => $premium_agen->agen_id,
                'nama_kios' => $premium_agen->nama_kios,
                'provinsi_id' => $premium_agen->provinsi_id,
                'kabupaten_id' => $premium_agen->kabupaten_id,
                'kecamatan_id' => $premium_agen->kecamatan_id,
                'kelurahan_id' => $premium_agen->kelurahan_id,
                'location' => $location_name,
                'latitude' => $premium_agen->lat,
                'longitude' => $premium_agen->lng,
                'rt' => $premium_agen->RT,
                'rw' => $premium_agen->RW,
                'store_address' => $premium_agen->store_address,
                'answers' => $answers
            ];
        return $this->respondWithDataAndMessage($form, "Success");

    }

    public function getProvinsi(){
        $provinsis = \App\Provinsi::get();
        $data = [];
        foreach ($provinsis as $provinsi) {
            $data[] = [
                'id' => $provinsi->id,
                'name' => $provinsi->name
            ];
        }        
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getKabupaten(Request $request, $provinsi_id){
        $kabupatens = \App\Kabupaten::where('id_i_provinsi',$provinsi_id)->get();
        //dd($provinsi_id);
        $data = [];
        foreach ($kabupatens as $kabupaten) {
            $data[] = [
                'id' => $kabupaten->id,
                'name' => $kabupaten->name
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getKecamatan(Request $request, $kabupaten_id){
        $kecamatans = \App\Kecamatan::where('id_i_kabupaten', $kabupaten_id)->get();
        $data = [];
        foreach ($kecamatans as $kecamatan) {
            $data[] = [
                'id' => $kecamatan->id,
                'name' => $kecamatan->name
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getKelurahan(Request $request, $kecamatan_id){
        $kelurahans = \App\Kelurahan::where('id_i_kecamatan', $kecamatan_id)->get();
        $data = [];
        foreach ($kelurahans as $kelurahan) {
            $data[] = [
                'id' => $kelurahan->id,
                'name' => $kelurahan->name,
                'full_name' => $kelurahan->full_name 
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getLocation(){
        $query = Request::get('query');
        $provinsis = \App\Provinsi::select('i_provinsi.name as provinsiname', 'i_kabupaten.name as kabupatenname', 'i_kecamatan.name as kecamatanname', 'i_kelurahan.name as kelurahanname', 'i_kelurahan.id as kelurahan_id','*')
                    ->join('i_kabupaten', 'i_provinsi.id' ,'=', 'i_kabupaten.id_i_provinsi')
                    ->join('i_kecamatan', 'i_kabupaten.id', '=', 'i_kecamatan.id_i_kabupaten')
                    ->join('i_kelurahan', 'i_kecamatan.id', '=', 'i_kelurahan.id_i_kecamatan')
                    ->where('i_provinsi.name', 'like', '%'.$query.'%')
                    ->orWhere('i_kabupaten.name', 'like', '%'.$query.'%')
                    ->orWhere('i_kecamatan.name', 'like', '%'.$query.'%')
                    ->orWhere('i_kelurahan.name', 'like', '%'.$query.'%')
                    ->get();
        
        $data = [];
        foreach ($provinsis as $provinsi) {
            //dd($provinsi->kabupatenname);
            $names = $provinsi->provinsiname.', '.$provinsi->kabupatenname.', '.$provinsi->kecamatanname.', '.$provinsi->kelurahanname;
            $data[] = [
                'name' => $names,
                'provinsi_id' => $provinsi->id_i_provinsi,
                'kabupaten_id' => $provinsi->id_i_kabupaten,
                'kecamatan_id' => $provinsi->id_i_kecamatan,
                'kelurahan_id' => $provinsi->kelurahan_id
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }
}
