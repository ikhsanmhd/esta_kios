<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Storage;
use Esta;
use Illuminate\Support\Facades\Auth;
use Hash;

class RegistrasiEstaAPIv2 extends ApiController
{
  public function inquiryActivation(){
    $serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/inquiryActivation';
    //$serviceURL = CRUDBooster::getsetting('sobatku_url').'api/inquiryActivation';
    $referenceNumber = Request::get('referenceNumber');
    $idCardNumber = Request::get('idCardNumber');
    $phoneNumber = Request::get('phoneNumber');
    $birthDate = Request::get('birthDate');
    $birthDate = str_replace(' ','-',$birthDate);
    $birthDate = $this->clearMonth($birthDate);
    $birthDate = date('Y-m-d' , strtotime($birthDate));
    $user = CRUDBooster::getsetting('user_sobatku_api');
    $hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
    $prefixActivation = CRUDBooster::getsetting('prefix_reference_activation');
    $keterangan = 'Inquiry Activation';
    $referenceNumber = DB::select('exec CreateReferenceActivationSobatku ?,?', array($prefixActivation, $keterangan));
    $request = [
      'referenceNumber' => $referenceNumber[0]->Prefix,
      'idCardNumber' => $idCardNumber,
      'phoneNumber' => $phoneNumber,
      'birthDate' => $birthDate,
      'user' => $user,
      'hashCode' => hash('sha256', $referenceNumber[0]->Prefix.$idCardNumber.$phoneNumber.$birthDate.$user.$hashCodeKey)
    ];

    $data_toJson = json_encode( $request );
    //dd($data_toJson);

    $saveLog = DB::select('exec CreateLogSobatkuActivation ?,?,?,?,?', array($phoneNumber,$referenceNumber[0]->Prefix,$data_toJson,'',$keterangan));
    $save_id = $saveLog[0]->id;
    $ch = curl_init( $serviceURL );
    curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept:application/json'
      )
    );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
    $response = curl_exec( $ch );
    $responseBody = json_decode( $response,true );
    if($responseBody['responseCode'] == 'RA'){
      $responseBody['urlActivate'] = CRUDBooster::getsetting('webview_url_sobatku_api').'/walletPage/activate?phoneNumber='.$phoneNumber.'&sessionId='.$responseBody['sessionId'].'&user='.$user;
      $responseBody['is_active'] = 1;
    }else{
	  //$responseBody['responseCode'] = 'RD';
	  $responseBody['is_active'] = 0;
      //$responseBody['is_active'] = 1; 
	  //$responseBody['urlActivate'] = CRUDBooster::getsetting('parent_url_sobatku_api').'/walletPage/activate?phoneNumber='.$phoneNumber.'&sessionId='.$responseBody['sessionId'].'&user='.$user;
      $responseBody['responseDescription'] = Esta::responseSobatku($responseBody['responseCode'], $responseBody['responseDescription']);
    }
    //dd($responseBody);

	$uv['response'] = $response;
	$update_response = DB::table('log_sobatku_activation')
						->where('id',$save_id)
						->update($uv);
    curl_close($ch);
    
    return $responseBody;   
  }

  //-------
  //updated by mike
  //28 aug 2019
  //-------
  public function clearMonth($birthDate) {
	  $birthDate = str_replace('Okt','Oct',$birthDate);
	  $birthDate = str_replace('Mei','May',$birthDate);
	  $birthDate = str_replace('Agt','Aug',$birthDate);
	  $birthDate = str_replace('Des','Dec',$birthDate);
	  return $birthDate;
  }
  public function postRegistrasiNew() {

    //todo:
    //simpan data agen ke table txn_pengajuan_agen (done)
    //simpan data va ke table txn_pengajuan_agen (done)
    //final checking - data added to table (done)
	
	$signatureFile = image(string $name, int $width = 10, int $height = 10) ;
	dd($signatureFile);

    $serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/register';
    //$serviceURL = CRUDBooster::getsetting('sobatku_url').'api/register';
    $user = CRUDBooster::getsetting('user_sobatku_api');
    $hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
    $idCardFile = $_FILES['idCardFile']['tmp_name'];
    $pasPhotoFile = $_FILES['pasPhotoFile']['tmp_name'];
    $name = Request::get('name');
    $email = Request::get('email');
    $phoneNumber = Request::get('mobilePhone');
    $birthDate = Request::get('birthDate');
    $birthDate = str_replace(' ','-',$birthDate);
	$birthDate = $this->clearMonth($birthDate);
    $birthDate = date('Y-m-d' , strtotime($birthDate));
    $idCardNumber = Request::get('idCardNumber');
    $motherName = Request::get('motherName');
    $is_active = Request::get('is_active');
    $regid = Request::get('regid');
    $buku_rekening = Request::get('buku_rekening');
    $storage = storage_path("app/uploads/verify_agen/");
	$statusSobatku = Request::get('statusSobatku');
    //$id_agen = Request::get('id_agen');

    /*$temp = DB::table('agen_temp')
      ->where('id',$id_agen)
      ->first();*/

    $cek_no_hp = DB::table('agen')
      ->where('no_hp',$phoneNumber)
      ->whereNull('deleted_at')
      ->first();
      //dd($cek_no_hp);
    /* if(!empty($cek_no_hp->id) && $cek_no_hp->is_existing != 1 && $cek_no_hp->flag_registrasi == 5) {
      $response['api_status']  = 0;
      $response['api_message'] = 'No HP sudah terdaftar';
      $response['type_dialog']  = 'Error';
      return response()->json($response);
    } */

    //check pengajuan agen ada ayng pending atau Tidak
    $cek_pengajuan_agen = DB::table('txn_pengajuan_agen')
      ->where('agen_no_hp',$phoneNumber)
      ->whereNull('deleted_at')
      ->whereNotNull('agen_response_sobatku')
      ->where('flag_registrasi',5)
      ->first();

      if(!empty($cek_pengajuan_agen->id)) {
        $response['api_status']  = 0;
        $response['api_message'] = 'Pengajuan anda sedang dalam proses';
        $response['type_dialog']  = 'Error';
        return response()->json($response);
      }

	$sv['agen_is_agen_kios'] = $cek_no_hp->is_agen_kios == null ? '0' : $cek_no_hp->is_agen_kios;
    $sv['agen_is_existing'] = $cek_no_hp->is_existing;
    $sv['agen_tgl_register'] = date('Y-m-d H:i:s');
    //$sv['agen_tgl_otp_terkirim'] = $temp->tgl_otp_terkirim;
    //$sv['agen_tgl_verifikasi_otp'] = $temp->tgl_verifikasi_otp;
    //$sv['agen_kode_otp'] = $temp->kode_otp;
    $sv['agen_no_hp'] = $phoneNumber;
    $sv['created_at'] = date('Y-m-d H:i:s');
    $sv['updated_at'] = date('Y-m-d H:i:s');

    $sv['agen_nama'] = $name;
    $sv['agen_no_hp'] = $phoneNumber;
    $sv['agen_email'] = $email;
    $sv['agen_photo'] = 'uploads/profile_agen/avatar.jpg';
    $sv['agen_kode_relation_referall'] = Request::get('kode_relation_referall');
    $sv['agen_password'] = Hash::make(Request::get('password'));

    $kode = Esta::nomor_transaksi_new('txn_pengajuan_agen',CRUDBooster::getsetting('kode_pengajuan_agen'));
    /*echo $kode;
    exit();*/
    $sv['kode_pengajuan_agen'] = $kode;
    $sv['agen_kode_referall_agen'] = Esta::kode_referall();
    $sv['agen_status_agen'] = 'Basic';
    $sv['agen_status_aktif'] = 'Tidak Aktif';
    $sv['agen_status_verifikasi'] = 'Pending';
    $sv['agen_notif_email'] = 'Yes';
    $sv['created_user'] = $sv['agen_nama'];
    $sv['updated_user'] = $sv['agen_nama'];
    $sv['agen_regid'] = $regid;

    if(!empty($sv['agen_kode_relation_referall'])) {
      $check_referall = DB::table('agen')->where('kode_referall_agen',$sv['agen_kode_relation_referall'])->first();
      //$setting_ref = DB::table('voucher_referall')->whereNull('deleted_at')->where('aktif','Yes')->first();

      if($check_referall->id >= 1) {

      } else {
        $response['api_status']  = 2;
          $response['api_message'] = 'Kode referal yang anda pakai tidak terdaftar';
          $response['type_dialog']  = 'Error';
          return response()->json($response);
          exit();
      }
    }

    //Save Image to FTP
    $idCardFileFoto = Request::file('idCardFile');
	//dd($idCardFileFoto);
    $file_nameidCardFile = 'Sobatku/idCardFoto-'.time().'-'.$name.".jpg";
    Storage::disk('ftp')->put($file_nameidCardFile, file_get_contents($idCardFileFoto));
    $pasPhotoFileFoto = Request::file('pasPhotoFile');
    $file_namepasPhotoFile = 'Sobatku/pasPhotoFile-'.time().'-'.$name.".jpg";
    Storage::disk('ftp')->put($file_namepasPhotoFile, file_get_contents($pasPhotoFileFoto));
    //dd(file_put_contents($pasPhotoFileFoto, file_get_contents($pasPhotoFileFoto)));

    if($statusSobatku != 'RD'){
	    if($is_active == 0){
	      //PROCCESS TO SOBATKU
	      $prefixActivation = CRUDBooster::getsetting('prefix_reference_registrasi');
	      $keterangan = 'Registration';
	      $referenceNumber = DB::select('exec CreateReferenceRegistrasiSobatku ?,?', array($prefixActivation, $keterangan));

	      $jsonRegistration = [
	        'name' => $name,
	        'email' => $email,
	        'mobilePhone' => $phoneNumber,
	        'birthDate' => $birthDate,
	        'idCardNumber' => $idCardNumber,
	        'motherName' => $motherName,
	        'referenceNumber' => $referenceNumber[0]->Prefix,
	        'user' => $user,
	        'hashCode' => hash('sha256', $phoneNumber.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
	      ];

	      $request = [
	        'idCardFile' => curl_file_create($idCardFile, 'image/jpg', $file_nameidCardFile),
	        'pasPhotoFile' => curl_file_create($pasPhotoFile, 'image/jpg', $file_namepasPhotoFile),
	        'jsonRegistration' => json_encode($jsonRegistration)
	      ];

	      $saveLog = DB::select('exec CreateLogSobatkuRegistration ?,?,?,?,?', array($phoneNumber,$referenceNumber[0]->Prefix,json_encode( $request ),'',$keterangan));
	      $save_id = $saveLog[0]->id;

	      $ch = curl_init($serviceURL);
	      curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
	      curl_setopt( $ch, CURLOPT_POSTFIELDS, $request );
	      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	      curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
	          'Accept:application/json',
	          'Content-Type:multipart/form-data'
	        )
	      );
		  curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		  curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
	      $responsecurl = curl_exec($ch);
	      $responseBody = json_decode( $responsecurl,true );
	      curl_close($ch);
			//dd($responseBody);
		  $uv['response'] = $responsecurl;
		  $update_response = DB::table('log_sobatku_registration')
							->where('id',$save_id)
							->update($uv);
	      /*if($responseBody['responseCode'] == '02'){
	      	$responseBody['responseDescription'] = 'Maaf proses pengajuan registrasi anda belum dapat dilanjutkan, silahkan ajukan registrasi pada aplikasi sobatku';
	      	$item['responseSobatku'] = $responseBody;
	        $response['api_status']  = 0;
	        $response['api_message'] = 'Maaf proses pengajuan registrasi anda belum dapat dilanjutkan, silahkan ajukan registrasi pada aplikasi sobatku';
	        $response['type_dialog']  = 'Informasi';
	        $response['item'] = $item;
	        return response()->json($response);
	      }else*/
	      if($responseBody['responseCode'] == '00'){
	      	/* $responseBody['responseDescription'] = Esta::responseSobatku($responseBody['responseCode'], $responseBody['responseDescription']);
	        $item['responseSobatku'] = $responseBody;
	        $response['api_status']  = 0;
	        $response['api_message'] = '';
	        $response['type_dialog']  = 'Informasi';
	        $response['item'] = $item;
	        return response()->json($response); */
			$responseBody['urlCompleteRegister'] = CRUDBooster::getsetting('webview_url_sobatku_api').'/walletPage/register?phoneNumber='.$phoneNumber.'&sessionId='.$responseBody['sessionId'].'&user='.$user;
	    
	      }

	     }
	}

    $sv['agen_idCardNumber'] = $idCardNumber;
    $sv['agen_motherName'] = $motherName;
    $sv['agen_pasPhotoFile'] = $file_namepasPhotoFile;
    $sv['agen_idCardFile'] = $file_nameidCardFile;
    $sv['agen_tglpengajuansobatku'] = date('Y-m-d H:i:s');
    $sv['agen_tgl_lahir'] = $birthDate;
	
	//UPDATE 241019 -- UPDATE DATA EXISTING TO TABLE AGEN
	if(!empty($cek_no_hp)){
		$svagen['updated_at'] = $sv['updated_at'];
		$svagen['nama'] = $sv['agen_nama'];
		$svagen['no_hp'] = $sv['agen_no_hp'];
		$svagen['email'] = $sv['agen_email'];
		$svagen['password'] = $sv['agen_password']; 
		$svagen['updated_user'] = $sv['updated_user']; 
		$svagen['idCardNumber'] = $sv['agen_idCardNumber']; 
		$svagen['motherName'] = $sv['agen_motherName']; 
		$svagen['pasPhotoFile'] = $sv['agen_pasPhotoFile']; 
		$svagen['idCardFile'] = $sv['agen_idCardFile']; 
		$svagen['tglpengajuansobatku'] = $sv['agen_tglpengajuansobatku']; 
		$svagen['tgl_lahir'] = $sv['agen_tgl_lahir']; 
		$svagen['kode_relation_referall'] = Request::get('kode_relation_referall');
		$update_agen = DB::table('agen')
					->where('id',$cek_no_hp->id)
					->update($svagen);
	}

    if(!empty($idCardFile)) {
      $sv['va_foto_ktp'] = $file_nameidCardFile;
    }

    if(!empty($pasPhotoFile)) {
      $sv['va_foto_selfie'] = $file_namepasPhotoFile;
    }

	$sv['agen_status_verifikasi_sobatku'] = 'Menunggu Verifikasi';
    $sv['flag_registrasi'] = 1;

    $check_pengajuan = DB::table('txn_pengajuan_agen')
			->where('agen_no_hp',$phoneNumber)
			->where('kode_pengajuan_agen','!=',NULL)
			->whereNull('deleted_at')
			->orderBy('id', 'DESC')
			->first();

	if(!empty($check_pengajuan)){
		$sv['agen_alasan_reject'] = '';
		$updateExisting = DB::table('txn_pengajuan_agen')
    				->where('id',$check_pengajuan->id)
			    	->update($sv);
		$update = $check_pengajuan->id;
	}else{
		$update = DB::table('txn_pengajuan_agen')
    	->insertGetId($sv);
	}    

    if($is_active == 1){
      $update = $this->movePengajuanAgenToAgenByID($update,1);
    }

    if($update) {
      //get ID dari table txn_pengajuan_agen
      $id_agen = $update;
	  
	  if($statusSobatku == 'RD') {
		  $responseBody['responseCode'] = $statusSobatku;
		  $responseBody['responseDescription'] = Esta::responseSobatku($responseBody['responseCode'], $responseBody['responseDescription']);
		  $item['responseSobatku'] = $responseBody;
		  $response['item'] = $item;
		  $response['api_status']  = 0;
	      $response['api_message'] = 'Gagal';
	      $response['type_dialog']  = 'Error';
	      return response()->json($response);
      }elseif($responseBody['responseCode'] != '00' && $is_active != 1) {
		  $responseBody['responseDescription'] = Esta::responseSobatku($responseBody['responseCode'], $responseBody['responseDescription']);
		  $item['responseSobatku'] = $responseBody;
		  $response['item'] = $item;
		  $response['api_status']  = 0;
	      $response['api_message'] = 'Gagal';
	      $response['type_dialog']  = 'Error';
	      return response()->json($response);
      }

      $item['nama'] = $name;
      $item['no_hp'] = $phoneNumber;
      $item['kode_referall_agen'] = $sv['agen_kode_referall_agen'];
      $item['status_agen'] = $sv['agen_status_agen'];
      $item['photo'] = env('BACKEND_URL').'uploads/profile_agen/avatar.jpg';
      $item['responseSobatku'] = $responseBody;
      $item['id_agen'] = $id_agen;

      $response['api_status']  = 1;
      $response['api_message'] = 'Registrasi berhasil. Menunggu proses aktivasi dari sobatku.';
      $response['type_dialog']  = 'Informasi';
      $response['item'] = $item;
    } else {
      $response['api_status']  = 0;
      $response['api_message'] = 'Gagal';
      $response['type_dialog']  = 'Error';
    }

    return response()->json($response);

  }

  public function movePengajuanAgenToAgen(){

    $no_hp_pengajuan = Request::get('no_hp');
	
    $getAgen = DB::select('EXEC getPengajuanAgenByNohp ?',array($no_hp_pengajuan))[0];
    $getAgenBasic = DB::table('agen')
			->where('no_hp',$no_hp_pengajuan)
			->where('kode','!=',NULL)
			->whereNull('deleted_at')
			->first();	
	if(!empty($getAgenBasic)){
		$existingBasic['flag_registrasi'] = 4;
		$update_existing = DB::table('agen')
				          ->where('id',$getAgenBasic->id)
				          ->update($existingBasic);
		$update_existing_pengajuan = DB::table('txn_pengajuan_agen')
				          ->where('id',$getAgen->id)
				          ->update($existingBasic);
		$response['id_agen'] = $getAgenBasic->id;
		//return $this->respondWithDataAndMessage($response, "Success");
	}elseif(empty($getAgen)){
    	$response['api_status']  = 0;
	    $response['api_message'] = 'Agen Tidak Ditemukan';
		$response['type_dialog']  = 'Error';
	    return response()->json($response);
    }
    $sv['tgl_register'] = $getAgen->agen_tgl_register;
    $sv['tgl_otp_terkirim'] = $getAgen->agen_tgl_otp_terkirim;
    $sv['tgl_verifikasi_otp'] = $getAgen->agen_tgl_verifikasi_otp;
    $sv['kode_otp'] = $getAgen->agen_kode_otp;
    $sv['no_hp'] = $getAgen->agen_no_hp;
    $sv['created_at'] = date('Y-m-d H:i:s');

    $sv['nama'] = $getAgen->agen_nama;
    $sv['no_hp'] = $getAgen->agen_no_hp;
    $sv['email'] = $getAgen->agen_email;
    $sv['photo'] = $getAgen->agen_photo;
    $sv['kode_relation_referall'] = $getAgen->agen_kode_relation_referall;
    $sv['password'] = $getAgen->agen_password;

    $kode = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
    /*echo $kode;
    exit();*/
    $sv['kode'] = $kode;
    $sv['kode_referall_agen'] = $getAgen->agen_kode_referall_agen;
    $sv['status_agen'] = 'Basic';
    $sv['status_aktif'] = 'Aktif';
    $sv['status_verifikasi'] = 'Pending';
    $sv['notif_email'] = 'Yes';
    $sv['created_user'] = $sv['nama'];
        $sv['updated_user'] = '';
        $sv['regid'] = $getAgen->agen_regid;

    if(!empty($sv['kode_relation_referall'])) {
      $check_referall = DB::table('agen')->where('kode_referall_agen',$sv['kode_relation_referall'])->first();
      $setting_ref = DB::table('voucher_referall')->whereNull('deleted_at')->where('aktif','Yes')->first();
    }

    //---
    $sv['idCardNumber'] = $getAgen->agen_idCardNumber;
    $sv['motherName'] = $getAgen->agen_motherName;
    $sv['pasPhotoFile'] = $getAgen->agen_pasPhotoFile;
    $sv['idCardFile'] = $getAgen->agen_idCardFile;
    $sv['tglpengajuansobatku'] = $getAgen->agen_tglpengajuansobatku;
    $sv['tgl_lahir'] = $getAgen->agen_tgl_lahir;

    $sv['flag_registrasi'] = 4;

    $check_pass = DB::table('agen')
					->where('no_hp',$no_hp_pengajuan)
					->where('kode','!=',NULL)
					->whereNull('deleted_at')
					->first();
	if($check_pass->is_existing == 1){
		$existing['nama'] = $getAgen->agen_nama;
		$existing['email'] = $getAgen->agen_email;
		$existing['password'] = $getAgen->agen_password;
		$existing['idCardNumber'] = $getAgen->agen_idCardNumber;
	    $existing['motherName'] = $getAgen->agen_motherName;
	    $existing['pasPhotoFile'] = $getAgen->agen_pasPhotoFile;
	    $existing['idCardFile'] = $getAgen->agen_idCardFile;
	    $existing['tglpengajuansobatku'] = $getAgen->agen_tglpengajuansobatku;
	    $existing['tgl_lahir'] = $getAgen->agen_tgl_lahir;
	    //$existing['is_existing'] = 0;
	    $existing['flag_registrasi'] = 4;
		$update_existing = DB::table('agen')
				          ->where('id',$check_pass->id)
				          ->update($existing);
						  
		$up['foto_ktp'] = $getAgen->va_foto_ktp;
        $up['foto_ktp_status'] = 'Submitted';
        $up['foto_ktp_note'] = 'Menunggu verifikasi';
        $up['foto_ktp_last_submit'] = date('Y-m-d H:i:s');

        $up['foto_selfie'] = $getAgen->va_foto_selfie;
        $up['foto_selfie_status'] = 'Submitted';
        $up['foto_selfie_note'] = 'Menunggu verifikasi';
        $up['foto_selfie_last_submit'] = date('Y-m-d H:i:s');

      	$check = DB::table('verify_agen')
            ->where('id_agen',$check_pass->id)
            ->first();

        $up['id_agen'] = $check_pass->id;
        $up['created_user'] = Esta::user($check_pass->id);
        $up['updated_user'] = Esta::user($check_pass->id);
        $up_agn['updated_at'] = date('Y-m-d H:i:s');
        if(!empty($check)) {
        $update_verify = DB::table('verify_agen')
          ->where('id',$check->id)
          ->update($up);
        } else {
          $up['created_at'] = date('Y-m-d H:i:s');
          $update_verify = DB::table('verify_agen')
          ->insert($up);
        }
						  
		DB::table('txn_pengajuan_agen')
        	->where('agen_no_hp', $getAgen->agen_no_hp)->delete();
        $response['id_agen'] = $check_pass->id;
	}elseif($check_pass->is_existing == 0 && empty($getAgen)){
		$response['id_agen'] = $check_pass->id;
	}else{
		if(!empty($check_pass->id)){
			$existing['nama'] = $getAgen->agen_nama;
			$existing['email'] = $getAgen->agen_email;
			$existing['password'] = $getAgen->agen_password;
			$existing['idCardNumber'] = $getAgen->agen_idCardNumber;
			$existing['motherName'] = $getAgen->agen_motherName;
			$existing['pasPhotoFile'] = $getAgen->agen_pasPhotoFile;
			$existing['idCardFile'] = $getAgen->agen_idCardFile;
			$existing['tglpengajuansobatku'] = $getAgen->agen_tglpengajuansobatku;
			$existing['tgl_lahir'] = $getAgen->agen_tgl_lahir;
			//$existing['is_existing'] = 0;
			$existing['flag_registrasi'] = 4;
			$update_existing = DB::table('agen')
							  ->where('id',$check_pass->id)
							  ->update($existing);
							  
			$up['foto_ktp'] = $getAgen->va_foto_ktp;
			$up['foto_ktp_status'] = 'Submitted';
			$up['foto_ktp_note'] = 'Menunggu verifikasi';
			$up['foto_ktp_last_submit'] = date('Y-m-d H:i:s');

			$up['foto_selfie'] = $getAgen->va_foto_selfie;
			$up['foto_selfie_status'] = 'Submitted';
			$up['foto_selfie_note'] = 'Menunggu verifikasi';
			$up['foto_selfie_last_submit'] = date('Y-m-d H:i:s');

			$check = DB::table('verify_agen')
				->where('id_agen',$check_pass->id)
				->first();

			$up['id_agen'] = $check_pass->id;
			$up['created_user'] = Esta::user($check_pass->id);
			$up['updated_user'] = Esta::user($check_pass->id);
			$up_agn['updated_at'] = date('Y-m-d H:i:s');
			
			$update_verify = DB::table('verify_agen')
			  ->where('id',$check->id)
			  ->update($up);
			DB::table('txn_pengajuan_agen')
				->where('agen_no_hp', $getAgen->agen_no_hp)->delete();
			$response['id_agen'] = $check_pass->id;
		}else{
			$update = DB::table('agen')
			  //->where('id',$id_agen)
			  ->insertGetId($sv);
		}
	    $response['id_agen'] = $update;
	}
    if($update_existing || $update){
      if($update) {

        DB::table('txn_pengajuan_agen')
        ->where('agen_no_hp', $getAgen->agen_no_hp)->delete();

        $id_agen = $update;

        $up['foto_ktp'] = $getAgen->va_foto_ktp;
        $up['foto_ktp_status'] = 'Submitted';
        $up['foto_ktp_note'] = 'Menunggu verifikasi';
        $up['foto_ktp_last_submit'] = date('Y-m-d H:i:s');

        $up['foto_selfie'] = $getAgen->va_foto_selfie;
        $up['foto_selfie_status'] = 'Submitted';
        $up['foto_selfie_note'] = 'Menunggu verifikasi';
        $up['foto_selfie_last_submit'] = date('Y-m-d H:i:s');

      $check = DB::table('verify_agen')
            ->where('id_agen',$id_agen)
            ->first();

        $up['id_agen'] = $id_agen;
        $up['created_user'] = Esta::user($id_agen);
        $up['updated_user'] = Esta::user($id_agen);
        $up_agn['updated_at'] = date('Y-m-d H:i:s');
        if(!empty($check)) {
        $update = DB::table('verify_agen')
          ->where('id',$check->id)
          ->update($up);
        } else {
          $up['created_at'] = date('Y-m-d H:i:s');
          $update = DB::table('verify_agen')
          ->insert($up);
        }

        $banks = DB::table('bank')
          ->where('prefix_va','!=',NULL)
          ->whereNull('deleted_at')
          ->get();
        foreach($banks as $bank) {
          $flag_genva = $bank->flag_genva;

          $c['created_at'] = date('Y-m-d H:i:s');
          $c['updated_at'] = date('Y-m-d H:i:s');
          $c['created_user'] = Esta::user($id_agen);
          $c['updated_user'] = Esta::user($id_agen);
          $c['id_agen'] = $id_agen;
          $c['id_bank'] = $bank->id;
          if($flag_genva == 0) {
            $c['no_va'] = $bank->prefix_va.$sv['no_hp'];
          } else {
            $c['no_va'] = $bank->prefix_va.str_replace(CRUDBooster::getsetting('kode_agen'), '', $kode);
          }

          $in = DB::table('agen_va')
            ->insert($c);
        }

      $merchants = DB::table('merchant')
          ->whereNull('deleted_at')
          //->groupby('group')
          ->where('kode','Alfamart')
          ->get();
        foreach($merchants as $merchant) {
          $cm['created_at'] = date('Y-m-d H:i:s');
          $cm['updated_at'] = date('Y-m-d H:i:s');
          $cm['created_user'] = Esta::user($id_agen);
          $cm['updated_user'] = Esta::user($id_agen);
          $cm['id_agen'] = $id_agen;
          $cm['merchant'] = $merchant->group;
          $cm['no_va'] = $merchant->prefix_va.$sv['no_hp'];

          $in = DB::table('agen_va')
            ->insert($cm);
        }

        Esta::log_money_old($id_agen,0,date('Y-m-d H:i:s'),'Agen Registrasi','Agen Registrasi','','Riwayat Agen','','');

        if($check_referall->id >= 1) {
          $detail_voucher_referall = DB::table('voucher_referall')->whereNull('deleted_at')->first();
          $voucher_referall_aktif = $detail_voucher_referall->aktif;
          if($voucher_referall_aktif == 'Yes') {
            $voucher_pemakai = $detail_voucher_referall->referall_pemakai;
            $voucher_dipakai = $detail_voucher_referall->referall_dipakai;

          $detail_voucher_pemakai = DB::table('voucher')->where('id',$voucher_pemakai)->first();
          $detail_voucher_dipakai = DB::table('voucher')->where('id',$voucher_dipakai)->first();

          $save_child['created_at']       = date('Y-m-d H:i:s');
          $save_child['created_user'] = Esta::user($id_agen);
          $save_child['updated_user'] = Esta::user($id_agen);
          $save_child['id_agen']          = $check_referall->id;
          $save_child['id_voucher']       = $voucher_dipakai;
          $save_child['product']          = $detail_voucher_dipakai->product;
          $save_child['voucher_nama']          = $detail_voucher_dipakai->nama;
          $save_child['voucher_expired']          = $detail_voucher_dipakai->expired_date;
          $save_child['voucher_amount']          = $detail_voucher_dipakai->amount;
          $save_child['voucher_image']          = $detail_voucher_dipakai->image;
          $save_child['voucher_product']          = $detail_voucher_dipakai->product;
          $save_child['voucher_description']          = $detail_voucher_dipakai->description;
          $save_child['id_trans_voucher'] = '';
          $save_child['used']             = 'No';
          DB::table('trans_voucher_child')->insert($save_child);

          $save_notif['created_at'] = $detail_voucher_dipakai->expired_date;
          $save_notif['created_user'] = Esta::user($id_agen);
          $save_notif['updated_user'] = Esta::user($id_agen);
          $save_notif['title'] = $detail_voucher_dipakai->nama;
          $save_notif['description'] = $detail_voucher_dipakai->description;
          $save_notif['description_short'] = $detail_voucher_dipakai->description;
          $save_notif['image'] = $detail_voucher_dipakai->image;
          $save_notif['id_agen'] = $check_referall->id;
          $save_notif['read'] = 'No';
          $save_notif['flag'] = 'Voucher';
          $save_notif['syarat_ketentuan'] = $detail_voucher_dipakai->syarat_ketentuan;
          $save_notif['id_voucher'] = $voucher_dipakai;
          DB::table('notification')->insert($save_notif);

          $save_child2['created_at']       = date('Y-m-d H:i:s');
          $save_child2['created_user'] = Esta::user($id_agen);
          $save_child2['updated_user'] = Esta::user($id_agen);
          $save_child2['id_agen']          = $id_agen;
          $save_child2['id_voucher']       = $voucher_pemakai;
          $save_child2['product']          = $detail_voucher_pemakai->product;
          $save_child2['voucher_nama']          = $detail_voucher_pemakai->nama;
          $save_child2['voucher_expired']          = $detail_voucher_pemakai->expired_date;
          $save_child2['voucher_amount']          = $detail_voucher_pemakai->amount;
          $save_child2['voucher_image']          = $detail_voucher_pemakai->image;
          $save_child2['voucher_product']          = $detail_voucher_pemakai->product;
          $save_child2['voucher_description']          = $detail_voucher_pemakai->description;
          $save_child2['id_trans_voucher'] = '';
          $save_child2['used']             = 'No';
          DB::table('trans_voucher_child')->insert($save_child2);

          $save_notif2['created_at'] = $detail_voucher_pemakai->expired_date;
          $save_notif2['created_user'] = Esta::user($id_agen);
          $save_notif2['updated_user'] = Esta::user($id_agen);
          $save_notif2['title'] = $detail_voucher_pemakai->nama;
          $save_notif2['description'] = $detail_voucher_pemakai->description;
          $save_notif2['description_short'] = $detail_voucher_pemakai->description;
          $save_notif2['image'] = $detail_voucher_pemakai->image;
          $save_notif2['id_agen'] = $id_agen;
          $save_notif2['read'] = 'No';
          $save_notif2['flag'] = 'Voucher';
          $save_notif2['syarat_ketentuan'] = $detail_voucher_pemakai->syarat_ketentuan;
          $save_notif2['id_voucher'] = $voucher_pemakai;
          DB::table('notification')->insert($save_notif2);

          if($check_referall->regid != NULL) {
            $datafcm['title'] = CRUDBooster::getsetting('notification_voucher_header');
            $datafcm['content'] = CRUDBooster::getsetting('notification_voucher_referall');
            $regid[] = $check_referall->regid;
            Esta::sendFCM($regid,$datafcm);
          }
        }
      }

      /*//send notif
      $save_notif['created_at'] = date('Y-m-d H:i:s');
      $save_notif['title'] = 'Verification';
      $save_notif['description'] = 'Verification Result';
      $save_notif['description_short'] = 'Akun sukses di verifikasi';
      $save_notif['image'] = '';
      $save_notif['flag'] = 'Notifikasi';
      $save_notif['id_agen'] = $getAgen->id;
      $save_notif['read'] = 'No';
      //$save_notif['id_header'] = $id;

      $detail_agen = DB::table('txn_pengajuan_agen')->where('id',$save_notif['id_agen'])->first();
      if($detail_agen->agen_regid != NULL) {
        $datafcm['title'] = $save_notif['title'];
        $datafcm['content'] = $save_notif['description'];
        $datafcm['type'] = 'Sobatku';
        $save_notif['regid'] = Esta::sendFCM([$detail_agen->agen_regid],$datafcm);
      }
      $save_ver['tglverifikasisobatku'] = date('Y-m-d H:i:s');
      $save_ver['is_existing'] = 0;
      DB::table('agen')
        ->where('id',$getAgen->id)
        ->update($save_ver);

      DB::table('notification')->insert($save_notif);*/


	  }
	}else{

        return $this->respondWithError("Gagal membuat data agen");

    }

    return $this->respondWithDataAndMessage($response, "Success");

  }

  public function movePengajuanAgenToAgenByID($id,$flag_registrasi){

    $getAgen = DB::table('txn_pengajuan_agen')
    ->where('id',$id)
    ->first();

    $sv['tgl_register'] = $getAgen->agen_tgl_register;
    $sv['tgl_otp_terkirim'] = $getAgen->agen_tgl_otp_terkirim;
    $sv['tgl_verifikasi_otp'] = $getAgen->agen_tgl_verifikasi_otp;
    $sv['kode_otp'] = $getAgen->agen_kode_otp;
    $sv['no_hp'] = $getAgen->agen_no_hp;
    $sv['created_at'] = date('Y-m-d H:i:s');

    $sv['nama'] = $getAgen->agen_nama;
    $sv['no_hp'] = $getAgen->agen_no_hp;
    $sv['email'] = $getAgen->agen_email;
    $sv['photo'] = $getAgen->agen_photo;
    $sv['kode_relation_referall'] = $getAgen->agen_kode_relation_referall;
    $sv['password'] = $getAgen->agen_password;

    $kode = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
    /*echo $kode;
    exit();*/
    $sv['kode'] = $kode;
    $sv['kode_referall_agen'] = $getAgen->agen_kode_referall_agen;
    $sv['status_agen'] = 'Basic';
    $sv['status_aktif'] = 'Aktif';
    $sv['status_verifikasi'] = 'Pending';
    $sv['notif_email'] = 'Yes';
    $sv['created_user'] = $sv['nama'];
        $sv['updated_user'] = '';
        $sv['regid'] = $getAgen->agen_regid;

    if(!empty($sv['kode_relation_referall'])) {
      $check_referall = DB::table('agen')->where('kode_referall_agen',$sv['kode_relation_referall'])->first();
      $setting_ref = DB::table('voucher_referall')->whereNull('deleted_at')->where('aktif','Yes')->first();
    }

    //---
    $sv['idCardNumber'] = $getAgen->agen_idCardNumber;
    $sv['motherName'] = $getAgen->agen_motherName;
    $sv['pasPhotoFile'] = $getAgen->agen_pasPhotoFile;
    $sv['idCardFile'] = $getAgen->agen_idCardFile;
    $sv['tglpengajuansobatku'] = $getAgen->agen_tglpengajuansobatku;
    $sv['tgl_lahir'] = $getAgen->agen_tgl_lahir;
    $sv['flag_registrasi'] = $flag_registrasi;

    $check_pass = DB::table('agen')
					->where('no_hp',$getAgen->agen_no_hp)
					->where('kode','!=',NULL)
					->whereNull('deleted_at')
					->first();
	if($check_pass->is_existing == 1 || $check_pass->flag_registrasi == 1){
		$existing['nama'] = $getAgen->agen_nama;
		$existing['email'] = $getAgen->agen_email;
		$existing['password'] = $getAgen->agen_password;
		$existing['idCardNumber'] = $getAgen->agen_idCardNumber;
	    $existing['motherName'] = $getAgen->agen_motherName;
	    $existing['pasPhotoFile'] = $getAgen->agen_pasPhotoFile;
	    $existing['idCardFile'] = $getAgen->agen_idCardFile;
	    $existing['tglpengajuansobatku'] = $getAgen->agen_tglpengajuansobatku;
	    $existing['tgl_lahir'] = $getAgen->agen_tgl_lahir;
	    //$existing['is_existing'] = 0;
	    $existing['flag_registrasi'] = $flag_registrasi;
		$existing['regid'] = $getAgen->agen_regid;
		$update_existing = DB::table('agen')
				          ->where('id',$check_pass->id)
				          ->update($existing);
						  
		$up['foto_ktp'] = $getAgen->va_foto_ktp;
        $up['foto_ktp_status'] = 'Submitted';
        $up['foto_ktp_note'] = 'Menunggu verifikasi';
        $up['foto_ktp_last_submit'] = date('Y-m-d H:i:s');

        $up['foto_selfie'] = $getAgen->va_foto_selfie;
        $up['foto_selfie_status'] = 'Submitted';
        $up['foto_selfie_note'] = 'Menunggu verifikasi';
        $up['foto_selfie_last_submit'] = date('Y-m-d H:i:s');

      	$check = DB::table('verify_agen')
            ->where('id_agen',$check_pass->id)
            ->first();

        $up['id_agen'] = $check_pass->id;
        $up['created_user'] = Esta::user($check_pass->id);
        $up['updated_user'] = Esta::user($check_pass->id);
        $up_agn['updated_at'] = date('Y-m-d H:i:s');
        if(!empty($check)) {
        $update_verify = DB::table('verify_agen')
          ->where('id',$check->id)
          ->update($up);
        } else {
          $up['created_at'] = date('Y-m-d H:i:s');
          $update_verify = DB::table('verify_agen')
          ->insert($up);
        }
		
		DB::table('txn_pengajuan_agen')
        	->where('agen_no_hp', $getAgen->agen_no_hp)->delete();
        $response['id_agen'] = $check_pass->id;
	}else{
		if(!empty($check_pass->id)){
			$existing['nama'] = $getAgen->agen_nama;
			$existing['email'] = $getAgen->agen_email;
			$existing['password'] = $getAgen->agen_password;
			$existing['idCardNumber'] = $getAgen->agen_idCardNumber;
			$existing['motherName'] = $getAgen->agen_motherName;
			$existing['pasPhotoFile'] = $getAgen->agen_pasPhotoFile;
			$existing['idCardFile'] = $getAgen->agen_idCardFile;
			$existing['tglpengajuansobatku'] = $getAgen->agen_tglpengajuansobatku;
			$existing['tgl_lahir'] = $getAgen->agen_tgl_lahir;
			//$existing['is_existing'] = 0;
			$existing['flag_registrasi'] = $flag_registrasi;
			$update_existing = DB::table('agen')
							  ->where('id',$check_pass->id)
							  ->update($existing);
							  
			$up['foto_ktp'] = $getAgen->va_foto_ktp;
			$up['foto_ktp_status'] = 'Submitted';
			$up['foto_ktp_note'] = 'Menunggu verifikasi';
			$up['foto_ktp_last_submit'] = date('Y-m-d H:i:s');

			$up['foto_selfie'] = $getAgen->va_foto_selfie;
			$up['foto_selfie_status'] = 'Submitted';
			$up['foto_selfie_note'] = 'Menunggu verifikasi';
			$up['foto_selfie_last_submit'] = date('Y-m-d H:i:s');

			$check = DB::table('verify_agen')
				->where('id_agen',$check_pass->id)
				->first();

			$up['id_agen'] = $check_pass->id;
			$up['created_user'] = Esta::user($check_pass->id);
			$up['updated_user'] = Esta::user($check_pass->id);
			$up_agn['updated_at'] = date('Y-m-d H:i:s');
			
			$update_verify = DB::table('verify_agen')
			  ->where('id',$check->id)
			  ->update($up);
			DB::table('txn_pengajuan_agen')
				->where('agen_no_hp', $getAgen->agen_no_hp)->delete();
			$response['id_agen'] = $check_pass->id;
		}else{
			$update = DB::table('agen')
			  //->where('id',$id_agen)
			  ->insertGetId($sv);
			$response['id_agen'] = $update;
		}
	}
	if($update_existing || $update){
      if($update) {

        DB::table('txn_pengajuan_agen')
        ->where('agen_no_hp', $getAgen->agen_no_hp)->delete();

        $id_agen = $update;

        $up['foto_ktp'] = $getAgen->va_foto_ktp;
        $up['foto_ktp_status'] = 'Submitted';
        $up['foto_ktp_note'] = 'Menunggu verifikasi';
        $up['foto_ktp_last_submit'] = date('Y-m-d H:i:s');

        $up['foto_selfie'] = $getAgen->va_foto_selfie;
        $up['foto_selfie_status'] = 'Submitted';
        $up['foto_selfie_note'] = 'Menunggu verifikasi';
        $up['foto_selfie_last_submit'] = date('Y-m-d H:i:s');

      $check = DB::table('verify_agen')
            ->where('id_agen',$id_agen)
            ->first();

        $up['id_agen'] = $id_agen;
        $up['created_user'] = Esta::user($id_agen);
        $up['updated_user'] = Esta::user($id_agen);
        $up_agn['updated_at'] = date('Y-m-d H:i:s');
        if(!empty($check)) {
        $update = DB::table('verify_agen')
          ->where('id',$check->id)
          ->update($up);
        } else {
          $up['created_at'] = date('Y-m-d H:i:s');
          $update = DB::table('verify_agen')
          ->insert($up);
        }

        $banks = DB::table('bank')
          ->where('prefix_va','!=',NULL)
          ->whereNull('deleted_at')
          ->get();
        foreach($banks as $bank) {
          $flag_genva = $bank->flag_genva;

          $c['created_at'] = date('Y-m-d H:i:s');
          $c['updated_at'] = date('Y-m-d H:i:s');
          $c['created_user'] = Esta::user($id_agen);
          $c['updated_user'] = Esta::user($id_agen);
          $c['id_agen'] = $id_agen;
          $c['id_bank'] = $bank->id;
          if($flag_genva == 0) {
            $c['no_va'] = $bank->prefix_va.$sv['no_hp'];
          } else {
            $c['no_va'] = $bank->prefix_va.str_replace(CRUDBooster::getsetting('kode_agen'), '', $kode);
          }

          $in = DB::table('agen_va')
            ->insert($c);
        }

      $merchants = DB::table('merchant')
          ->whereNull('deleted_at')
          //->groupby('group')
          ->where('kode','Alfamart')
          ->get();
        foreach($merchants as $merchant) {
          $cm['created_at'] = date('Y-m-d H:i:s');
          $cm['updated_at'] = date('Y-m-d H:i:s');
          $cm['created_user'] = Esta::user($id_agen);
          $cm['updated_user'] = Esta::user($id_agen);
          $cm['id_agen'] = $id_agen;
          $cm['merchant'] = $merchant->group;
          $cm['no_va'] = $merchant->prefix_va.$sv['no_hp'];

          $in = DB::table('agen_va')
            ->insert($cm);
        }

        Esta::log_money_old($id_agen,0,date('Y-m-d H:i:s'),'Agen Registrasi','Agen Registrasi','','Riwayat Agen','','');

        if($check_referall->id >= 1) {
          $detail_voucher_referall = DB::table('voucher_referall')->whereNull('deleted_at')->first();
          $voucher_referall_aktif = $detail_voucher_referall->aktif;
          if($voucher_referall_aktif == 'Yes') {
            $voucher_pemakai = $detail_voucher_referall->referall_pemakai;
            $voucher_dipakai = $detail_voucher_referall->referall_dipakai;

          $detail_voucher_pemakai = DB::table('voucher')->where('id',$voucher_pemakai)->first();
          $detail_voucher_dipakai = DB::table('voucher')->where('id',$voucher_dipakai)->first();

          $save_child['created_at']       = date('Y-m-d H:i:s');
          $save_child['created_user'] = Esta::user($id_agen);
          $save_child['updated_user'] = Esta::user($id_agen);
          $save_child['id_agen']          = $check_referall->id;
          $save_child['id_voucher']       = $voucher_dipakai;
          $save_child['product']          = $detail_voucher_dipakai->product;
          $save_child['voucher_nama']          = $detail_voucher_dipakai->nama;
          $save_child['voucher_expired']          = $detail_voucher_dipakai->expired_date;
          $save_child['voucher_amount']          = $detail_voucher_dipakai->amount;
          $save_child['voucher_image']          = $detail_voucher_dipakai->image;
          $save_child['voucher_product']          = $detail_voucher_dipakai->product;
          $save_child['voucher_description']          = $detail_voucher_dipakai->description;
          $save_child['id_trans_voucher'] = '';
          $save_child['used']             = 'No';
          DB::table('trans_voucher_child')->insert($save_child);

          $save_notif['created_at'] = $detail_voucher_dipakai->expired_date;
          $save_notif['created_user'] = Esta::user($id_agen);
          $save_notif['updated_user'] = Esta::user($id_agen);
          $save_notif['title'] = $detail_voucher_dipakai->nama;
          $save_notif['description'] = $detail_voucher_dipakai->description;
          $save_notif['description_short'] = $detail_voucher_dipakai->description;
          $save_notif['image'] = $detail_voucher_dipakai->image;
          $save_notif['id_agen'] = $check_referall->id;
          $save_notif['read'] = 'No';
          $save_notif['flag'] = 'Voucher';
          $save_notif['syarat_ketentuan'] = $detail_voucher_dipakai->syarat_ketentuan;
          $save_notif['id_voucher'] = $voucher_dipakai;
          DB::table('notification')->insert($save_notif);

          $save_child2['created_at']       = date('Y-m-d H:i:s');
          $save_child2['created_user'] = Esta::user($id_agen);
          $save_child2['updated_user'] = Esta::user($id_agen);
          $save_child2['id_agen']          = $id_agen;
          $save_child2['id_voucher']       = $voucher_pemakai;
          $save_child2['product']          = $detail_voucher_pemakai->product;
          $save_child2['voucher_nama']          = $detail_voucher_pemakai->nama;
          $save_child2['voucher_expired']          = $detail_voucher_pemakai->expired_date;
          $save_child2['voucher_amount']          = $detail_voucher_pemakai->amount;
          $save_child2['voucher_image']          = $detail_voucher_pemakai->image;
          $save_child2['voucher_product']          = $detail_voucher_pemakai->product;
          $save_child2['voucher_description']          = $detail_voucher_pemakai->description;
          $save_child2['id_trans_voucher'] = '';
          $save_child2['used']             = 'No';
          DB::table('trans_voucher_child')->insert($save_child2);

          $save_notif2['created_at'] = $detail_voucher_pemakai->expired_date;
          $save_notif2['created_user'] = Esta::user($id_agen);
          $save_notif2['updated_user'] = Esta::user($id_agen);
          $save_notif2['title'] = $detail_voucher_pemakai->nama;
          $save_notif2['description'] = $detail_voucher_pemakai->description;
          $save_notif2['description_short'] = $detail_voucher_pemakai->description;
          $save_notif2['image'] = $detail_voucher_pemakai->image;
          $save_notif2['id_agen'] = $id_agen;
          $save_notif2['read'] = 'No';
          $save_notif2['flag'] = 'Voucher';
          $save_notif2['syarat_ketentuan'] = $detail_voucher_pemakai->syarat_ketentuan;
          $save_notif2['id_voucher'] = $voucher_pemakai;
          DB::table('notification')->insert($save_notif2);

          if($check_referall->regid != NULL) {
            $datafcm['title'] = CRUDBooster::getsetting('notification_voucher_header');
            $datafcm['content'] = CRUDBooster::getsetting('notification_voucher_referall');
            $regid[] = $check_referall->regid;
            Esta::sendFCM($regid,$datafcm);
          }
        }
      }

     /* //send notif
      $save_notif['created_at'] = date('Y-m-d H:i:s');
      $save_notif['title'] = 'Verification';
      $save_notif['description'] = 'Verification Result';
      $save_notif['description_short'] = 'Akun sukses di verifikasi';
      $save_notif['image'] = '';
      $save_notif['flag'] = 'Notifikasi';
      $save_notif['id_agen'] = $getAgen->id;
      $save_notif['read'] = 'No';
      //$save_notif['id_header'] = $id;

      $detail_agen = DB::table('txn_pengajuan_agen')->where('id',$save_notif['id_agen'])->first();
      if($detail_agen->agen_regid != NULL) {
        $datafcm['title'] = $save_notif['title'];
        $datafcm['content'] = $save_notif['description'];
        $datafcm['type'] = 'Sobatku';
        $save_notif['regid'] = Esta::sendFCM([$detail_agen->agen_regid],$datafcm);
      }
      $save_ver['tglverifikasisobatku'] = date('Y-m-d H:i:s');
      $save_ver['is_existing'] = 0;
      $save_ver['regid'] = NULL;
      DB::table('agen')
        ->where('id',$getAgen->id)
        ->update($save_ver);

      DB::table('notification')->insert($save_notif);*/


      }
	}else{

        $response['api_status']  = 0;
        $response['api_message'] = 'Gagal membuat data agen';
        $response['type_dialog']  = 'Error';

    }
    return $getAgen->id;
  }

	public function verificationManualResult(){
		$request = '';
		$phoneNumber = Request::get('phoneNumber');
		$sessionId = Request::get('sessionId');
		$responseCode = Request::get('responseCode');
		$responseDescription = Request::get('responseDescription');
		$hashCode = Request::get('hashCode');
		$response = [
			'phoneNumber' => $phoneNumber,
			'sessionId' => $sessionId,
			'responseCode' => $responseCode,
			'responseDescription' => $responseDescription,
			'hashCode' => $hashCode
		];

		$responseJson = json_encode($response);

		$saveResponse = DB::statement('exec CreateLogSobatkuVerification ?,?,?,?,?', array($phoneNumber,'',$request,$responseJson,'Verification Manual Result'));
		if($response['responseCode'] == '00'|| $response['responseCode'] == 'R1' ){

	      $getPengajuanAgen = DB::select('EXEC getPengajuanAgenByNohp ?',array($phoneNumber))[0];
	      if(!empty($getPengajuanAgen->id) ){
	        $this->movePengajuanAgenToAgenByID($getPengajuanAgen->id,5);
	      }

	      $getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];


			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Verification';
			$save_notif['description'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);
			$save_notif['description_short'] = 'Akun sukses di verifikasi';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku Verification';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
			}
			$save_ver['tglverifikasisobatku'] = date('Y-m-d H:i:s');
			$save_ver['tglaktivasisobatku'] = date('Y-m-d H:i:s');
			$save_ver['is_existing'] = 0;
			$save_ver['regid'] = NULL;
			$save_ver['response_sobatku'] = $response['responseCode'];
			$save_ver['alasan_reject_sobatku'] = NULL;
			DB::table('agen')
				->where('id',$getAgen->id)
				->update($save_ver);

			DB::table('log_notification_sobatku')->insert($save_notif);
		}else{

			 $getPengajuanAgen = DB::select('EXEC getPengajuanAgenByNohp ?',array($phoneNumber))[0];
			 //dd($getPengajuanAgen);
		      if(!empty($getPengajuanAgen->id) ){
				//send notif
				$save_notif['created_at'] = date('Y-m-d H:i:s');
				$save_notif['title'] = 'Verification';
				$save_notif['description'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);
				$save_notif['description_short'] = 'Gagal Verification';
				$save_notif['image'] = '';
				$save_notif['flag'] = 'Notifikasi';
				$save_notif['id_agen'] = $getPengajuanAgen->id;
				$save_notif['read'] = 'No';
				//$save_notif['id_header'] = $id;

				//$detail_agen = DB::table('txn_pengajuan_agen')->where('id',$save_notif['id_agen'])->first();
				if($getPengajuanAgen->agen_regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$datafcm['type'] = 'Sobatku Verification';
					$save_notif['regid'] = Esta::sendFCM([$getPengajuanAgen->agen_regid],$datafcm);
				}
				//$save_ver['deleted_at'] = date('Y-m-d H:i:s');
				$save_ver['agen_response_sobatku'] = $response['responseCode'];
				$save_ver['agen_alasan_reject'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);	
				$save_ver['agen_status_verifikasi_sobatku'] = 'Rejected';	
				$save_ver['flag_registrasi'] = 1;
				DB::table('txn_pengajuan_agen')
					->where('id',$getPengajuanAgen->id)
					->update($save_ver);
				$getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];
				if(!empty($getAgen->id)){
					$save_ver_agen['regid'] = NULL;
					$save_ver_agen['flag_registrasi'] = 1;
					$save_ver_agen['response_sobatku'] = $response['responseCode'];
					$save_ver_agen['alasan_reject_sobatku'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);
					DB::table('agen')
						->where('id',$getAgen->id)
						->update($save_ver_agen);
				}
				DB::table('log_notification_sobatku')->insert($save_notif);
		      }else{
		      	$getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];

				//send notif
				$save_notif['created_at'] = date('Y-m-d H:i:s');
				$save_notif['title'] = 'Verification';
				$save_notif['description'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);
				$save_notif['description_short'] = 'Gagal Verification';
				$save_notif['image'] = '';
				$save_notif['flag'] = 'Notifikasi';
				$save_notif['id_agen'] = $getAgen->id;
				$save_notif['read'] = 'No';
				//$save_notif['id_header'] = $id;

				$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
				if($detail_agen->regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$datafcm['type'] = 'Sobatku Verification';
					$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
				}
				//$save_ver['deleted_at'] = date('Y-m-d H:i:s');
				//$save_ver['is_existing'] = 0;
				$save_ver['regid'] = NULL;
				$save_ver['flag_registrasi'] = 1;
				$save_ver['response_sobatku'] = $response['responseCode'];
				$save_ver['alasan_reject_sobatku'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);
				DB::table('agen')
					->where('id',$getAgen->id)
					->update($save_ver);

				DB::table('log_notification_sobatku')->insert($save_notif);
		      }
			
		}

		return $this->respondWithDataAndMessage($response, "Success");
	}	

	public function infoTarikTunai(){
		$agen = Auth::user();
        $id_agen = $agen->id;
		$item['informasi_tarik_tunai'] = CRUDBooster::getsetting('informasi_tarik_tunai');
		// $item['info_bpjs'] = (CRUDBooster::getsetting('info_bpjs') != '' ? CRUDBooster::getsetting('info_bpjs') : '');
		// $item['info_tarik_tunai'] = (CRUDBooster::getsetting('info_tarik_tunai') != '' ? str_replace(array('[param1]','[param2]'), array(number_format(CRUDBooster::getsetting('tarik_tunai_tanpa_biaya_admin'),0,',','.'),number_format(CRUDBooster::getsetting('biaya_admin_tarik_tunai'),0,',','.')),CRUDBooster::getsetting('info_tarik_tunai')) : '');

		$response['api_status']  = 1;
	    $response['api_message'] = 'Success';
	    $response['type_dialog']  = 'Informasi';
	 //    $response['item'] = $item;
		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$item['saldo'] = ($detail_agen->saldo <= 0 ? 0 : $detail_agen->saldo);
	 	$response['item'] = $item;

	    return response()->json($response);
	}

	public function getDataRegistrasi(){

	    $no_hp = Request::get('no_hp');
	    $getDataRegistrasiAgen = DB::table('agen')
								->where('no_hp',$no_hp)
								->whereNull('deleted_at')
								->where('is_existing',1)
								->first();
		$getDataRegistrasiAgenFlag = DB::table('agen')
								->where('no_hp',$no_hp)
								->whereNull('deleted_at')
								->where('flag_registrasi',1)
								->first();
		//$getDataRegistrasiAgen = DB::select('exec getAgenRegistrasiByNohp ?', array($no_hp))[0];
	    $getDataRegistrasi = DB::table('txn_pengajuan_agen')
								->where('agen_no_hp',$no_hp)
								->whereNull('deleted_at')
								->where('flag_registrasi',1)
								->first();
		
		$item['is_rejected'] = false;
		if(!empty($getDataRegistrasiAgenFlag)){
			$item['name'] = ($getDataRegistrasiAgenFlag->nama == null ? '' : $getDataRegistrasiAgenFlag->nama);
		    $item['email'] = ($getDataRegistrasiAgenFlag->email == null ? '' : $getDataRegistrasiAgenFlag->email);
		    $item['mobilePhone'] = ($getDataRegistrasiAgenFlag->no_hp == null ? '' : $getDataRegistrasiAgenFlag->no_hp);
		    $item['birthDate'] = ($getDataRegistrasiAgenFlag->tgl_lahir == null ? '' : $getDataRegistrasiAgenFlag->tgl_lahir);
		    $item['idCardNumber'] = ($getDataRegistrasiAgenFlag->idCardNumber == null ? '' : $getDataRegistrasiAgenFlag->idCardNumber);
		    $item['motherName'] = ($getDataRegistrasiAgenFlag->motherName == null ? '' : $getDataRegistrasiAgenFlag->motherName);
			$item['pasPhotoFile'] = ($getDataRegistrasiAgenFlag->pasPhotoFile == null ? '' : config('app.esta_ftp').$getDataRegistrasiAgenFlag->pasPhotoFile);
			$item['idCardFile'] = ($getDataRegistrasiAgenFlag->idCardFile == null ? '' : config('app.esta_ftp').$getDataRegistrasiAgenFlag->idCardFile);
			$item['kode_relation_referall'] = ($getDataRegistrasiAgenFlag->kode_relation_referall == null ? '' : $getDataRegistrasiAgenFlag->kode_relation_referall);
			$item['is_rejected'] = ($getDataRegistrasiAgenFlag->alasan_reject_sobatku == null ? false : true);
			$item['responseCode'] = ($getDataRegistrasiAgenFlag->response_sobatku == null ? '' : $getDataRegistrasiAgenFlag->response_sobatku);
			$item['responseDescription'] = Esta::responseSobatku($getDataRegistrasiAgenFlag->response_sobatku, $getDataRegistrasiAgenFlag->alasan_reject_sobatku);
		}elseif(!empty($getDataRegistrasi)){
		    $item['name'] = ($getDataRegistrasi->agen_nama == null ? '' : $getDataRegistrasi->agen_nama);
		    $item['email'] = ($getDataRegistrasi->agen_email == null ? '' : $getDataRegistrasi->agen_email);
		    $item['mobilePhone'] = ($getDataRegistrasi->agen_no_hp == null ? '' : $getDataRegistrasi->agen_no_hp);
		    $item['birthDate'] = ($getDataRegistrasi->agen_tgl_lahir == null ? '' : $getDataRegistrasi->agen_tgl_lahir);
		    $item['idCardNumber'] = ($getDataRegistrasi->agen_idCardNumber == null ? '' : $getDataRegistrasi->agen_idCardNumber);
		    $item['motherName'] = ($getDataRegistrasi->agen_motherName == null ? '' : $getDataRegistrasi->agen_motherName);
			$item['pasPhotoFile'] = ($getDataRegistrasi->agen_pasPhotoFile == null ? '' : config('app.esta_ftp').$getDataRegistrasi->agen_pasPhotoFile);
			$item['idCardFile'] = ($getDataRegistrasi->agen_idCardFile == null ? '' : config('app.esta_ftp').$getDataRegistrasi->agen_idCardFile);
			$item['kode_relation_referall'] = ($getDataRegistrasi->agen_kode_relation_referall == null ? '' : $getDataRegistrasi->agen_kode_relation_referall);
			$item['is_rejected'] = ($getDataRegistrasi->agen_alasan_reject == null ? false : true);
			$item['responseCode'] = ($getDataRegistrasi->agen_response_sobatku == null ? '' : $getDataRegistrasi->agen_response_sobatku);
			$item['responseDescription'] = Esta::responseSobatku($getDataRegistrasi->agen_response_sobatku, $getDataRegistrasi->agen_alasan_reject);
		}else{
			//dd($getDataRegistrasiAgen);
			$item['name'] = ($getDataRegistrasiAgen->nama == null ? '' : $getDataRegistrasiAgen->nama);
		    $item['email'] = ($getDataRegistrasiAgen->email == null ? '' : $getDataRegistrasiAgen->email);
		    $item['mobilePhone'] = ($getDataRegistrasiAgen->no_hp == null ? '' : $getDataRegistrasiAgen->no_hp);
		    $item['birthDate'] = ($getDataRegistrasiAgen->tgl_lahir == null ? '' : $getDataRegistrasiAgen->tgl_lahir);
		    $item['idCardNumber'] = ($getDataRegistrasiAgen->idCardNumber == null ? '' : $getDataRegistrasiAgen->idCardNumber);
		    $item['motherName'] = ($getDataRegistrasiAgen->motherName == null ? '' : $getDataRegistrasiAgen->motherName);
			$item['pasPhotoFile'] = ($getDataRegistrasiAgen->pasPhotoFile == null ? '' : config('app.esta_ftp').$getDataRegistrasiAgen->pasPhotoFile);
			$item['idCardFile'] = ($getDataRegistrasiAgen->idCardFile == null ? '' : config('app.esta_ftp').$getDataRegistrasiAgen->idCardFile);
			$item['kode_relation_referall'] = ($getDataRegistrasiAgen->kode_relation_referall == null ? '' : $getDataRegistrasiAgen->kode_relation_referall);
			$item['is_rejected'] = ($getDataRegistrasiAgen->alasan_reject_sobatku == null ? false : true);
			$item['responseCode'] = ($getDataRegistrasiAgen->response_sobatku == null ? '' : $getDataRegistrasiAgen->response_sobatku);
			$item['responseDescription'] = Esta::responseSobatku($getDataRegistrasiAgen->response_sobatku, $getDataRegistrasiAgen->alasan_reject_sobatku);
		}
		$response['api_status']  = 1;
	    $response['api_message'] = 'Success';
		$response['type_dialog']  = 'Informasi';
		$response['item'] = $item;
	    return response()->json($response);
	}

	public function saveFlagRegistrasi(){
		$no_hp = Request::get('no_hp');
		$flag_registrasi = Request::get('flag_registrasi');
		$up['flag_registrasi'] = $flag_registrasi;
		$check_pengajuan = DB::table('txn_pengajuan_agen')
			->where('agen_no_hp',$no_hp)
			->where('kode_pengajuan_agen','!=',NULL)
			->whereNull('deleted_at')
			->first();
		
		$check_hp = DB::table('agen')
			->where('no_hp',$no_hp)
			->where('kode','!=',NULL)
			->whereNull('deleted_at')
			->first();
		if(!empty($check_pengajuan)){
			$update = DB::table('txn_pengajuan_agen')
			    	->where('id',$check_pengajuan->id)
			    	->update($up);
			$updateagen = DB::table('agen')
			    	->where('id',$check_hp->id)
			    	->update($up);
		}else{
			if($flag_registrasi == 5){
				$up['is_existing'] = 0;
			}
			$update = DB::table('agen')
			    	->where('id',$check_hp->id)
			    	->update($up);
		}
		if($update){
			return $this->respondWithSuccess("Success");	
		}else{
			return $this->respondWithError("Data Gagal Di Submit. Silahkan Mencoba kembali");
		}	
	}


//-========================
}
