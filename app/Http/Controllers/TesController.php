<?php

namespace App\Http\Controllers;
use DB;
use Request;

class TesController extends Controller
{
    public function tes()
    {
    	$q = DB::table('cb_config_insentif')->get();
    	return response()->json(['message' => 'OK', 'data' => $q], 200);
    }

    public function getAgenReferral(){
    	$status_pengajuan = Request::get('status_pengajuan');
    	$start_date = Request::get('start_date');
    	$end_date = Request::get('end_date');
    	$status = 'Pengajuan';
    	//$cobrand_id = Auth::user();
    	$cobrand_id = '1';
    	$pengajuan_agen_data = DB::table('txn_pengajuan_agen AS pa')
    						->leftJoin('cb_mst_branch AS cabang','pa.agen_cobrand_cabang_id', '=', 'cabang.id')
    						->leftJoin('cb_mst_pj AS pj','pa.agen_cobrand_ao_id', '=', 'pj.id')
    						->leftJoin('cb_mst_ewallet AS ewallet','pa.agen_ewallet_id', '=', 'ewallet.id')
    						->select('pa.created_at', 'pa.created_user', 'pa.updated_at', 'pa.updated_user','pa.kode_pengajuan_agen AS kode', 'pa.agen_no_hp AS no_hp', 'pa.agen_nama AS nama', 'pa.agen_email AS email', 'pa.agen_tgl_lahir AS tgl_lahir', 'pa.agen_idCardNumber AS idCardNumber', 'pa.agen_kode_referall_agen AS kode_referall_agen' , 'pa.agen_tgl_register AS tgl_register' , 'pa.agen_status_agen AS level_agen','cabang.nama AS cabang', 'pj.nama AS penanggung_jawab','ewallet.nama AS metode_pembayaran', 'pa.agen_status_aktif AS status_aktif'
    							,DB::raw('\'Pengajuan\' AS status_agen') ,'pa.updated_at', 'pa.updated_user')
    						->where('pa.agen_cobrand_id',$cobrand_id)->whereNull('pa.deleted_at')
                            ->whereBetween('pa.agen_tgl_register',[$start_date, $end_date]);
    	$agen_data = DB::table('agen AS a')
    			->leftJoin('cb_mst_branch AS cabang','a.cobrand_cabang_id', '=', 'cabang.id')
    			->leftJoin('cb_mst_pj AS pj','a.cobrand_ao_id', '=', 'pj.id')
    			->leftJoin('cb_mst_ewallet AS ewallet','a.ewallet_id', '=', 'ewallet.id')
    			->select('a.created_at', 'a.created_user', 'a.updated_at', 'a.updated_user','a.kode','a.no_hp', 'a.nama', 'a.email', 'a.tgl_lahir', 'a.idCardNumber', 'a.kode_referall_agen', 'a.tgl_register', 'a.status_agen AS level_agen', 'cabang.nama AS cabang', 'pj.nama AS penanggung_jawab', 'ewallet.nama AS metode_pembayaran', 'a.status_aktif'
                ,DB::raw('(CASE WHEN a.tglaktivasimembershipsobatku is null THEN \'Aktivasi\'
                        WHEN a.tglaktivasimembershipsobatku is not null THEN \'Aktif\'
                        END) AS status_agen')
                ,'a.updated_at', 'a.updated_user')
    			->where('a.cobrand_id',$cobrand_id)->whereNull('a.deleted_at')->union($pengajuan_agen_data)
                ->whereBetween('a.tgl_register',[$start_date, $end_date])->get();

    	return response()->json(['message' => 'OK', 'data' => $agen_data], 200);
    }
}
