<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Request;
use Route;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;
use Storage;
use Esta;
use App;
use Illuminate\Support\Facades\Auth;

class PlnAPI extends Controller
{
	public function postPlnProduk() {
		$agen = Auth::user();
        $id_agen = $agen->id;

        $agen = DB::select('exec getAgenById ?', array($id_agen))[0];

		/*$agen = DB::table('agen')
			->where('id',$id_agen)
			->first();*/

		$arrays = DB::table('pan_product')
			->where('id_tipe_layanan','4')
			->where('id_kode_layanan','1')
			->where('product_name','!=','Token Unsold 1')
			->where('product_name','!=','Token Unsold 2')
			->where('deleted_at',NULL)
			->orderBy('amount','asc')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->product_name;
			$rest['amount'] = Esta::amount_product($array->id);;
			if($agen->status_agen == 'Basic') {
				$rest['komisi'] = $array->komisi_basic;
			} else {
				$rest['komisi'] = $array->komisi_premium;
			}
			array_push($rest_json, $rest);
		
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postVoucherPln() {
		$agen = Auth::user();
        $id_agen = $agen->id;

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen));

		/*$arrays = DB::table('trans_voucher_child')
			->whereIN('product',['PLN','All'])
			->where('id_agen',$id_agen)
			->where('used','No')
			->where('voucher_expired','>=',date('Y-m-d H:i:s'))
			->orderBy('id','desc')
			->get();*/

		$arrays = DB::select('exec getVoucherWithParams ?,?', array($id_agen,'PLN'));

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->voucher_nama;
			$rest['amount'] = $array->voucher_amount;
			$rest['masa_berlaku'] = date("Y-m-d H:i:s", strtotime($array->voucher_expired));
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postCheckTagihanPascabayarPln() {
		$no_hp = Request::get('no_hp');
		$agen = Auth::user();
        $id_agen = $agen->id;
		$no_meter = Request::get('no_meter');

		$tipe_layanan = DB::table('pan_tipe_layanan')->where('kode','00')->first()->id;
		$kode_layanan = DB::table('pan_kode_layanan')->where('kode','4')->first()->id;
		$id_product = DB::table('pan_product')->where('id_tipe_layanan',$tipe_layanan)->where('id_kode_layanan',$kode_layanan)->first()->id;

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen))[0];

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		/*send iso*/
		$send_iso = Esta::send_iso_pln_pascabayar('0',$no_meter,'0200',0,'Inquiry');
		//dd($send_iso);

		if($send_iso['39'] == '00') {
			$jml_tagihan = $send_iso['jml_tagihan'];
			$rest['val_rp_transaksi'] = $send_iso['rp_transaksi'];
			$rest['val_pln_psc_biaya_tagihan_pln'] = $send_iso['pln_psc_biaya_tagihan_pln'];
			$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$rest['val_pln_psc_biaya_admin'] = ($detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan)*$jml_tagihan;
			$rest['val_rp_bayar'] = $rest['val_rp_transaksi']+$rest['val_pln_psc_biaya_admin'];
			$rest['val_total_pembayaran'] = $rest['val_rp_bayar']-$rest['val_komisi'];

			$rest['id_transaksi'] = $send_iso['id_log'];
			$rest['pln_psc_id_pel'] = $send_iso['pln_psc_id_pel'];
			$rest['pln_psc_nama'] = $send_iso['pln_psc_nama'];

			/*$b = substr($send_iso['pln_psc_blth'],4,2);
			$date=date_create("2013-".$b."-15");

			$rest['pln_psc_blth'] = strtoupper(date_format($date,"M")).substr($send_iso['pln_psc_blth'],2,2);*/
			$rest['pln_psc_blth'] = substr($send_iso['pln_psc_blth'],0,-1);
			$rest['pln_psc_jml_tagihan'] = $send_iso['pln_psc_jml_tagihan'];
			$rest['rp_transaksi'] = 'Rp '.number_format($send_iso['rp_transaksi'],0,',','.');
			$rest['pln_psc_biaya_tagihan_pln'] = 'Rp '.number_format($send_iso['pln_psc_biaya_tagihan_pln'],0,',','.');
			$rest['pln_psc_biaya_admin'] = 'Rp '.number_format($rest['val_pln_psc_biaya_admin'],0,',','.');
			$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
			$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
			$rest['rp_bayar'] = 'Rp '.number_format($rest['val_rp_bayar'],0,',','.');


			$response['api_status']  = 1;
	    	$response['api_message'] = 'Berhasil';
	    	$response['type_dialog']  = 'Informasi';
	    	$response['item']  = $rest;
		} else {
			$rest['jml_tagihan'] = 0;
			$rest['komisi'] = 0;

			$response['api_status']  = 0;
	    	$response['api_message'] = Esta::show_error('PLN Postpaid',$send_iso['39'],$no_meter);
	    	$response['type_dialog']  = 'Error';
	    	$response['item']  = $rest;
		}
		return response()->json($response);
	}

	public function postInquiryPrabayarPln() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$no_hp = Request::get('no_hp');
		$id_product = Request::get('id_product');
		$no_meter = Request::get('no_meter');
		$pin = Request::get('pin');
		$id_voucher = Request::get('id_voucher');

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen))[0];

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		/*send iso inquiry*/
		$send_iso_inquiry = Esta::send_iso_pln_prabayar($id_product,$no_meter,'0200','0','Inquiry','0','0','0');

		if($send_iso_inquiry['39'] == '00') {
			$token_unsold = Esta::view_bit62($send_iso_inquiry['62']);
			if($token_unsold['total_repeat'] >= 1) {
				for ($i=0; $i < $token_unsold['total_repeat']; $i++) { 
					if($i == 1) {
						$rest_unsold_1 = substr($token_unsold['power_purchase_unsold'], 0, 11);
					} else {
						$rest_unsold_2 = substr($token_unsold['power_purchase_unsold'], 11, 11);
					}
				}
			}

	    	$rest['id_transaksi']  = $send_iso_inquiry['id_log'];
	    	$rest['rc']  = $send_iso_inquiry['39'];
	    	$rest['pln_pra_meter_id'] = $send_iso_inquiry['pln_pra_meter_id'];
			$rest['pln_pra_nama'] = $send_iso_inquiry['pln_pra_nama'];
			$rest['pln_pra_tarif'] = str_replace(' ', '', $send_iso_inquiry['pln_pra_tarif']);
			$rest['pln_pra_kategori_daya'] = $send_iso_inquiry['pln_pra_kategori_daya'];
            $rest['rc'] = $send_iso_inquiry['39'];

			$response['api_status']  = 1;
	    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_inquiry['39'],$no_meter);
	    	$response['type_dialog']  = 'Informasi';
	    	$response['item']  = $rest;

	    	$detail_product_unsold_1 = DB::table('pan_product')->where('product_name','Token Unsold 1')->first();
	    	$detail_product_unsold_2 = DB::table('pan_product')->where('product_name','Token Unsold 2')->first();

	    	$response['unsold_1_nama'] = 'Token unsold '.number_format($rest_unsold_1,0,',','.');
	    	$response['unsold_1']  = ($rest_unsold_1 <= 0 ? '' : ltrim($rest_unsold_1+$detail_product_unsold_1->admin_edn+$detail_product_unsold_1->admin_1+$detail_product_unsold_1->admin_2+$detail_product_unsold_1->admin_3+$detail_product_unsold_1->margin,0));
	    	$response['unsold_1_val']  = ($rest_unsold_1 <= 0 ? 0 : ltrim($rest_unsold_1,0));
	    	$response['komisi_unsold_1']  = ($detail_agen->status_agen == 'Basic' ? DB::table('pan_product')->where('product_name','Token Unsold 1')->first()->komisi_basic : DB::table('pan_product')->where('product_name','Token Unsold 1')->first()->komisi_premium);

	    	$response['unsold_2_nama'] = 'Token unsold '.number_format($rest_unsold_2,0,',','.');
	    	$response['unsold_2']  = ($rest_unsold_2 <= 0 ? '' : ltrim($rest_unsold_2+$detail_product_unsold_2->admin_edn+$detail_product_unsold_2->admin_1+$detail_product_unsold_2->admin_2+$detail_product_unsold_2->admin_3+$detail_product_unsold_2->margin,0));
	    	$response['unsold_2_val']  = ($rest_unsold_2 <= 0 ? 0 : ltrim($rest_unsold_2,0));
	    	$response['komisi_unsold_2']  = ($detail_agen->status_agen == 'Basic' ? DB::table('pan_product')->where('product_name','Token Unsold 2')->first()->komisi_basic : DB::table('pan_product')->where('product_name','Token Unsold 2')->first()->komisi_premium);
		} else {
			$response['api_status']  = 0;
	    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_inquiry['39'],$no_meter);
	    	$response['type_dialog']  = 'Error';
            $rest['rc']  = $send_iso_inquiry['39'];
            $response['item']  = $rest;
		}
		return response()->json($response);
	}

	public function postPlnPrepaidAdviceManual() {
		$stan = Request::get('stan');
		$trans_no = Request::get('trans_no');

		$inquiry = DB::table('log_jatelindo_bit');
			if(!empty($stan)) {
				$inquiry = $inquiry->where('bit11',$stan);
			}
			$inquiry = $inquiry
			->where('bit37',$trans_no)
			->where('type','PLN Prepaid')
			->where('status','Inquiry')
			->where('jenis','res')
			->first();
		$purchase = DB::table('log_jatelindo_bit');
			if(!empty($stan)) {
				$purchase = $purchase->where('bit11',$stan);
			}
			$purchase = $purchase
			->where('bit37',$trans_no)
			->where('type','PLN Prepaid')
			->where('status','Purchase')
			->where('jenis','res')
			->first();

		$detail_transaksi = DB::table('trans_pln')
			->where('trans_no',$trans_no)
			->first();

		$detail_product = DB::table('pan_product')
			->where('id',$detail_transaksi->id_product)
			->first();

		$detail_voucher= DB::table('voucher')
			->where('id',$detail_transaksi->id_product)
			->first();
			/*print_r($detail_product);
			exit();*/
		if(!empty($purchase)) {
			$send_iso_purchase = Esta::send_iso_pln_prabayar($detail_transaksi->id_product,$inquiry->pln_pra_meter_id,'0221',$inquiry->id,'Advice Manual',$purchase->pln_pra_unsold_repeat,$purchase->pln_pra_unsold,$purchase->pln_pra_unsold_nominal);

			$no_meter = $inquiry->pln_pra_meter_id;
			$product_amount = ($purchase->pln_pra_unsold == 1 ? $purchase->pln_pra_unsold_nominal : $detail_product->amount);
		} else {
			$send_iso_purchase = Esta::send_iso_pln_prabayar($detail_transaksi->id_product,$inquiry->pln_pra_meter_id,'0221',$inquiry->id,'Advice Manual',0,0,0);

			$no_meter = $inquiry->pln_pra_meter_id;
			$product_amount = $detail_product->amount;
		}

		if($send_iso_purchase['39'] == '00') {
			$response['api_status']  = 1;
	    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_purchase['39'],$no_meter);
	    	$response['type_dialog']  = 'Error';
	    	$response['id_transaksi']  = $detail_transaksi->id;
	    	$response['id_log']  = $send_iso_purchase['id_log'];

	    	$response['pln_pra_meter_id'] = $send_iso_purchase['pln_pra_meter_id'];
	    	$response['pln_pra_id_pel'] = $send_iso_purchase['pln_pra_id_pel'];
			$response['pln_pra_nama'] = $send_iso_purchase['pln_pra_nama'];
			$response['pln_pra_tarif'] = str_replace(' ', '', $send_iso_purchase['pln_pra_tarif']);
			$response['pln_pra_kategori_daya'] = $send_iso_purchase['pln_pra_kategori_daya'];
			$response['pln_pra_ref_no'] = $send_iso_purchase['pln_pra_ref_no'];
			$response['pln_pra_biaya_materai'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['pln_pra_ppn'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['pln_pra_ppju'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['pln_pra_angsuran'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['pln_pra_rp_stroom'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
			$response['pln_pra_jml_kwh'] = substr(Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_jml_kwh'],0),$send_iso_purchase['pln_pra_nilai_minor_kwh']),0,-1);
			$response['pln_pra_token_number'] = chunk_split($send_iso_purchase['pln_pra_token_number'],4,' ');
			$response['pln_pra_biaya_admin'] = 'Rp '.number_format($detail_transaksi->product_biaya_admin_edn+$detail_transaksi->product_biaya_admin_1+$detail_transaksi->product_biaya_admin_2+$detail_transaksi->product_biaya_admin_3+$detail_transaksi->product_margin-$detail_transaksi->product_potongan,0,',','.');

			$response['val_pln_pra_rp_stroom'] = ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0);
			$response['val_pln_pra_biaya_materai'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['val_pln_pra_ppn'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['val_pln_pra_ppju'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['val_pln_pra_angsuran'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['val_pln_pra_biaya_admin'] = $detail_transaksi->product_biaya_admin_edn+$detail_transaksi->product_biaya_admin_1+$detail_transaksi->product_biaya_admin_2+$detail_transaksi->product_biaya_admin_3+$detail_transaksi->product_margin-$detail_transaksi->product_potongan;

			$response['pln_pra_pembelian_listrik'] = 'Rp '.Esta::nilai_minor($response['val_pln_pra_rp_stroom']+$response['val_pln_pra_biaya_admin'],$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
			$response['info'] = str_replace('TMP', '"TMP"', $send_iso_purchase['62']);
			$response['val_komisi'] = $detail_transaksi->komisi;
			$response['komisi'] = 'Rp '.number_format($response['val_komisi'],0,',','.');
			$response['pln_pra_tgl_lunas'] = substr($send_iso_purchase['pln_pra_tgl_lunas'],6,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],4,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],0,4).' '.substr($send_iso_purchase['pln_pra_tgl_lunas'],8,2).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],10,1).substr($send_iso_purchase['pln_pra_tgl_lunas'],11,1).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],12,2);
			//$response['nominal_total_pembayaran'] = 'Rp '.number_format($trans_amount, 0, ',', '.');
			$response['nominal_voucher'] = $detail_transaksi->voucher_amount;
			$response['tipe_pembayaran'] = 'PLN Prepaid';

			$response['val_2_pln_pra_ppn'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['val_2_pln_pra_ppju'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['val_2_pln_pra_angsuran'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['val_2_pln_pra_biaya_materai'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['val_2_pln_pra_rp_stroom'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);

			$response['val_rp_bayar'] = $response['val_pln_pra_biaya_admin']+$product_amount;
			$response['rp_bayar'] = 'Rp '.number_format($response['val_rp_bayar'],0,',','.');
			$total_pembayaran = $response['val_rp_bayar']-$response['val_komisi']-$detail_voucher->amount;
			$response['total_pembayaran'] = 'Rp '.number_format(($total_pembayaran <= 0 ? 0 : $total_pembayaran),0,',','.');

			$struk_id_pelanggan = $response['pln_pra_id_pel'];
			$no_meter = $response['pln_pra_meter_id'];
			$struk_info = $response['info'];
			$struk_nama_pelanggan = $response['pln_pra_nama'];
			$struk_tarif = $response['pln_pra_tarif'];
			$struk_daya = $response['pln_pra_kategori_daya'];
			$struk_rp_bayar = $response['val_rp_bayar'];
			$struk_materai = $response['val_2_pln_pra_biaya_materai'];
			$struk_ppn = $response['val_2_pln_pra_ppn'];
			$struk_ppj = $response['val_2_pln_pra_ppju'];
			$struk_angsuran = $response['val_2_pln_pra_angsuran'];
			$struk_rp_stroom = $response['val_2_pln_pra_rp_stroom'];
			$struk_jml_kwh = $response['pln_pra_jml_kwh'];
			$struk_token = $response['pln_pra_token_number'];
			$struk_admin_bank = $response['val_pln_pra_biaya_admin'];
			$struk_total_pembayaran = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
			$struk_tgl_lunas = $response['pln_pra_tgl_lunas'];
			$jpa_ref = $response['pln_pra_ref_no'];
			$status = 'Clear';

			/*$update = DB::table('trans_pln')
				->where('id',$detail_transaksi->id)
				->update($up);*/

			$update = DB::statement('exec postPlnPrepaidAdviceManual ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($detail_transaksi->id,$struk_id_pelanggan,$no_meter,$struk_info,$struk_nama_pelanggan,$struk_tarif,$struk_daya,$struk_rp_bayar,$struk_materai,$struk_ppn,$struk_ppj,$struk_angsuran,$struk_rp_stroom,$struk_jml_kwh,$struk_token,$struk_admin_bank,$struk_total_pembayaran,$struk_tgl_lunas,$jpa_ref,$status));

			/*struk*/

			/*email*/

			$view     = view('struk/struk_pln_prepaid',$response)->render();
			$filename = "Struk-PLN-Prepaid-".$response['pln_pra_id_pel'];
			$pdf      = App::make('dompdf.wrapper');

			$path = storage_path('app/uploads/'.$filename.'.pdf');

			$pdf->loadHTML($view);
			$pdf->setPaper('A4','landscape');
			$output = $pdf->output();

			file_put_contents($path, $output);

			$attachments = [$path];
			$email = $detail_agen->email;
			if(!empty($email) && $detail_agen->notif_email != 'No') {
			    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pln_prepaid','attachments'=>$attachments]);
			}
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_purchase['39'],$no_meter);
	    	$response['id_transaksi']  = 0;
	    	$response['type_dialog']  = 'Error';
		}

		return response()->json($response);
	}

	public function postPlnTransaksiAdviceManual() {
		$agen = Auth::user();
        $id_agen = $agen->id;

		$arrays = DB::table('trans_pln')
			->where('id_agen',$id_agen)
			//->where('deleted_at','!=',NULL)
			->where('status','Advice Manual')
			->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'))
			->orderBy('id','DESC')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$date_tr = Esta::change_date_format($array->created_at,'d M Y');
			$rest['id'] = $array->id;
			$rest['datetime'] = Esta::change_date_format($array->created_at,'d M Y H:i');
			$rest['trans_no'] = $array->trans_no;
			$rest['amount'] = 'Rp '.number_format($array->trans_amount,0,',','.');
			$rest['stan'] = $array->stan;
			$rest['id_pelanggan'] = $array->no_meter;
			$rest['status'] = ($date_tr == date('d M Y') ? 'Pending' : 'Expired');
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postPlnPrabayarSubmit() {
		$id_agen = Request::get('id_agen');
        $request_transaksi = Request::get('request_transaksi');
        $transactionId = Request::get('transactionId');
        $request_transaksi = json_decode( $request_transaksi,true );
		/*$agen = Auth::user();
        $id_agen = $agen->id;*/
		$regid = $request_transaksi['regid'];
		$token = $request_transaksi['token'];
		$kode_paket = $request_transaksi['kode_paket'];
		$cek_regid = Esta::cek_regid($id_agen,$regid);

        $getlog_transaction = DB::table('log_sobatku_payment')
            ->where('transactionId',$transactionId)
            ->where('keterangan','Request Payment')
            ->first();
        $oldRefNum = $getlog_transaction->ref_number;
        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

        $ut['is_used'] = 1;
        DB::table('temp_trans_payment')
					->where('transactionId',$transactionId)
					->update($ut);

		/*if($cek_regid == 0) {
			$response['api_status']  = 2;
		    $response['api_message'] = 'Terjadi gangguan pada koneksi internet atau Server. Silahkan coba beberapa saat lagi';//'Akun Anda terdeteksi login di HP lain, Silahkan logout dan login kembali untuk melanjutkan transaksi.';
		    $response['type_dialog']  = 'Error';
		    //Esta::add_fraud($id_agen);
		    return response()->json($response);
		}*/

		$no_hp = $request_transaksi['no_hp'];
		$id_product = $request_transaksi['id_product'];
		$id_transaksi = $request_transaksi['id_transaksi'];
		$no_meter = $request_transaksi['no_meter'];
		$pin = $request_transaksi['pin'];
		//$id_voucher = Request::get('id_voucher');
		$amount = $request_transaksi['amount'];
		$unsold = $request_transaksi['unsold'];
		$unsold_nominal = $request_transaksi['unsold_nominal'];

		$id_voucher_child = $request_transaksi['id_voucher'];
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		/*if($pin != 'estakiosprosespaket'){

			if(hash::check($pin, $detail_agen->password)){
				
			} else {
				$response['api_status']  = 2;
		    	$response['api_message'] = 'PIN anda salah';
		    	$response['type_dialog']  = 'Error';
		    	return response()->json($response);
		    	exit();
			}
		}*/

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		$product_amount = ($unsold == 1 ? $unsold_nominal : $detail_product->amount);

		$detail_voucher = DB::table('voucher')
			->where('id',$id_voucher)
			->first();

		$kode = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));

		if($unsold == 1) {
			$detail_product_unsold_1 = DB::table('pan_product')->where('product_name','Token Unsold 1')->first();
			$voucher = DB::table('voucher')->where('id',$id_voucher)->first();
			$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product_unsold_1->komisi_basic : $detail_product_unsold_1->komisi_premium);

			$trans_amount = $product_amount+$detail_product_unsold_1->admin_edn+$detail_product_unsold_1->admin_1+$detail_product_unsold_1->admin_2+$detail_product_unsold_1->admin_3+$detail_product_unsold_1->margin-$komisi-$voucher->amount;
		} else {
			$trans_amount = Esta::amount_product_trans_pln_prabayar($id_product,$id_agen,$id_voucher,0);
		}

		/*echo $trans_amount.'-'.($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

		Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN Prabayar','Transaksi PLN Prabayar '.$detail_product->product_name,'Out','Transaksi');
		Esta::log_money($id_agen,($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),date('Y-m-d H:i:s'),'Komisi Transaksi PLN Prabayar','Komisi Transaksi PLN Prabayar '.$detail_product->product_name,'In','Komisi');
		exit();*/
		$status_match_sobatku = 'Waiting Rekon';
		//if($saldo >= $trans_amount) {	
			try{
				$created_at = date('Y-m-d H:i:s');
				$updated_at = date('Y-m-d H:i:s');
				$created_user = Esta::user($id_agen);
				$updated_user = Esta::user($id_agen);
				$trans_no = $kode;
				$ref_trans_no = $kode;
				$trans_date = date('Y-m-d H:i:s');
				$trans_desc = 'PLN PRABAYAR';
				$currency = 'IDR';
				//$trans_amount = $product_amount;
				if($unsold == 1) {
					$komisi = $komisi;
				} else {
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
				}
				//$create_user = $detail_agen->nama;
				$id_agen = $id_agen;
				$status = 'Pending';
				$rekon_amount = 0;
				$status_match = 'Waiting Rekon';
				$no_hp = $no_hp;
				$no_meter = $no_meter;
				$id_product = $id_product;
				$agen_nama = $detail_agen->nama;
				$agen_email = $detail_agen->email;
				$agen_kode = $detail_agen->kode;
				$agen_no_hp = $detail_agen->no_hp;
				$agen_level = $detail_agen->status_agen;
				$agen_referall = $detail_agen->kode_referall_agen;
				$agen_referall_relation = $detail_agen->kode_relation_referall;
				$agen_status_aktif = $detail_agen->status_aktif;
				$agen_nik = $detail_agen->nik;
				$agen_tgl_register = $detail_agen->tgl_register;
				$agen_tempat_lahir = $detail_agen->tempat_lahir;
				$agen_tgl_lahir = $detail_agen->tgl_lahir;
				$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
				$agen_agama = $detail_agen->agama;
				$agen_status_perkawinan = $detail_agen->status_perkawinan;
				$agen_pekerjaan = $detail_agen->pekerjaan;
				$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
				$agen_prov = $detail_agen->prov;
				$agen_kab = $detail_agen->kab;
				$agen_kec = $detail_agen->kec;
				$agen_kel = $detail_agen->kel;
				$product_nama = $detail_product->product_name;
				$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
				$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
				$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
				$product_komisi_basic = $detail_product->komisi_basic;
				$product_komisi_premium = $detail_product->komisi_premium;
				$product_amount = $product_amount;
				$product_margin = $detail_product->margin;
				$product_biaya_admin_edn = $detail_product->admin_edn;
				$product_biaya_admin_1 = $detail_product->admin_1;
				$product_biaya_admin_2 = $detail_product->admin_2;
				$product_biaya_Admin_3 = $detail_product->admin_3;
				$product_potongan = $detail_product->potongan;
				$voucher_nama = $detail_voucher->nama;
				$id_voucher = $detail_voucher->id;
				$voucher_expired_date = $detail_voucher->expired_date;
				$voucher_amount = $detail_voucher->amount;
				$voucher_tagline = $detail_voucher->tagline;
				$voucher_description = $detail_voucher->description;
				$status_jatelindo = 'Pending';
				//$kode_paket = $kode_paket;
				$no_ref_sobatku = $oldRefNum;
				$transactionId_sobatku = $transactionId;

				
				$last_transaksi = Esta::last_transaksi($id_agen,'trans_pln');
				
				//dd($last_transaksi);
				if($last_transaksi == 1) {
					Esta::add_fraud($id_agen);
					$response['api_status']  = 2;
			    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
			    	$response['type_dialog']  = 'Error';
			    	$response['id_transaksi']  = 0;
			    	return $response;
					exit();
				}

				/*$save = DB::table('trans_pln')
					->insertGetId($sv);*/
				/*$saveTransPLN = DB::select('exec postTransPlnPrabayar 
					?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
					,array($created_at,$updated_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$trans_amount,$komisi,$id_agen
							,$status,$rekon_amount,$status_match,$no_hp,$no_meter,$id_product,$agen_nama,$agen_email,$agen_kode,$agen_no_hp,$agen_level
							,$agen_referall,$agen_referall_relation,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin
							,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov,$agen_kab,$agen_kec,$agen_kel
							,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller,$product_komisi_basic,$product_komisi_premium
							,$product_amount,$product_margin,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan
							,$voucher_nama,$id_voucher,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description
							,$status_jatelindo,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
				$save = $saveTransPLN[0]->id;*/
			} catch(\Exception $e) {
				
				$kode = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));
				$created_at = date('Y-m-d H:i:s');
				$updated_at = date('Y-m-d H:i:s');
				$created_user = Esta::user($id_agen);
				$updated_user = Esta::user($id_agen);
				$trans_no = $kode;
				$ref_trans_no = $kode;
				$trans_date = date('Y-m-d H:i:s');
				$trans_desc = 'PLN PRABAYAR';
				$currency = 'IDR';
				//$trans_amount = $product_amount;
				if($unsold == 1) {
					$komisi = $komisi;
				} else {
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
				}
				//$create_user = $detail_agen->nama;
				$id_agen = $id_agen;
				$status = 'Pending';
				$rekon_amount = 0;
				$status_match = 'Waiting Rekon';
				$no_hp = $no_hp;
				$no_meter = $no_meter;
				$id_product = $id_product;
				$agen_nama = $detail_agen->nama;
				$agen_email = $detail_agen->email;
				$agen_kode = $detail_agen->kode;
				$agen_no_hp = $detail_agen->no_hp;
				$agen_level = $detail_agen->status_agen;
				$agen_referall = $detail_agen->kode_referall_agen;
				$agen_referall_relation = $detail_agen->kode_relation_referall;
				$agen_status_aktif = $detail_agen->status_aktif;
				$agen_nik = $detail_agen->nik;
				$agen_tgl_register = $detail_agen->tgl_register;
				$agen_tempat_lahir = $detail_agen->tempat_lahir;
				$agen_tgl_lahir = $detail_agen->tgl_lahir;
				$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
				$agen_agama = $detail_agen->agama;
				$agen_status_perkawinan = $detail_agen->status_perkawinan;
				$agen_pekerjaan = $detail_agen->pekerjaan;
				$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
				$agen_prov = $detail_agen->prov;
				$agen_kab = $detail_agen->kab;
				$agen_kec = $detail_agen->kec;
				$agen_kel = $detail_agen->kel;
				$product_nama = $detail_product->product_name;
				$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
				$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
				$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
				$product_komisi_basic = $detail_product->komisi_basic;
				$product_komisi_premium = $detail_product->komisi_premium;
				$product_amount = $product_amount;
				$product_margin = $detail_product->margin;
				$product_biaya_admin_edn = $detail_product->admin_edn;
				$product_biaya_admin_1 = $detail_product->admin_1;
				$product_biaya_admin_2 = $detail_product->admin_2;
				$product_biaya_Admin_3 = $detail_product->admin_3;
				$product_potongan = $detail_product->potongan;
				$voucher_nama = $detail_voucher->nama;
				$id_voucher = $detail_voucher->id;
				$voucher_expired_date = $detail_voucher->expired_date;
				$voucher_amount = $detail_voucher->amount;
				$voucher_tagline = $detail_voucher->tagline;
				$voucher_description = $detail_voucher->description;
				$status_jatelindo = 'Pending';
				//$kode_paket = $kode_paket;
				$no_ref_sobatku = $oldRefNum;
				$transactionId_sobatku = $transactionId;

				$last_transaksi = Esta::last_transaksi($id_agen,'trans_pln');
				
				//dd($last_transaksi);
				if($last_transaksi == 1) {
					/*reversal sobatku*/
					/*$start_time = time();

					while(true){
						sleep(5);
						$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
						$user = CRUDBooster::getsetting('user_sobatku_api');
						$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

						$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
						$keterangan = 'Reversal Payment';
						$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));

						$request = [
							'oldRefNum' => $oldRefNum,
							'referenceNumber' => $referenceNumber[0]->Prefix,
							'user' => $user,
							'hashCode' => hash('sha256', $oldRefNum.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
						];

						$data_toJson = json_encode( $request );
						$ch = curl_init( $serviceURL );
						curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
						curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
						curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
								'Content-Type: application/json',
								'Accept:application/json'
							)
						);
						$responsereversal = curl_exec( $ch );
						$responseerror = curl_errno($ch);
						if ($responseerror == 28){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',1));
							break;
						}else {
							$responseBody = json_decode( $responsereversal, true);													

							if($responseBody['responseCode'] == '00' ){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',0));
								break;
							}else if((time() - $start_time) > 300){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',0));
								break;
							}
						}
						
						curl_close($ch);	
					}*/
					Esta::add_fraud($id_agen);
					$response['api_status']  = 2;
			    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
			    	$response['type_dialog']  = 'Error';
			    	$response['id_transaksi']  = 0;
			    	return $response;
					exit();
				}

				/*$save = DB::table('trans_pln')
					->insertGetId($sv);*/
				/*$saveTransPLN = DB::select('exec postTransPlnPrabayar 
					?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
					,array($created_at,$updated_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$trans_amount,$komisi,$id_agen,$status,$rekon_amount,$status_match,$no_hp,$no_meter,$id_product,$agen_nama,$agen_email,$agen_kode,$agen_no_hp,$agen_level,$agen_referall,$agen_referall_relation,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov,$agen_kab,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller,$product_komisi_basic,$product_komisi_premium,$product_amount,$product_margin,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan,$voucher_nama,$id_voucher,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description,$status_jatelindo,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
				$save = $saveTransPLN[0]->id;*/
			}	

			$last_transaksi2 = Esta::last_transaksi($id_agen,'trans_pln');
			if($last_transaksi2 == 1) {
				/*reversal sobatku*/
				/*$start_time = time();

				while(true){
					sleep(5);
					$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
					$user = CRUDBooster::getsetting('user_sobatku_api');
					$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

					$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
					$keterangan = 'Reversal Payment';
					$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));

					$request = [
						'oldRefNum' => $oldRefNum,
						'referenceNumber' => $referenceNumber[0]->Prefix,
						'user' => $user,
						'hashCode' => hash('sha256', $oldRefNum.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
					];

					$data_toJson = json_encode( $request );
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$responsereversal = curl_exec( $ch );
					$responseerror = curl_errno($ch);
					if ($responseerror == 28){
						$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',1));
						break;
					}else {
						$responseBody = json_decode( $responsereversal, true);													

						if($responseBody['responseCode'] == '00' ){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',0));
							break;
						}else if((time() - $start_time) > 300){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',0));
							break;
						}
					}
					
					curl_close($ch);	
				}*/
				Esta::add_fraud($id_agen);
				$response['api_status']  = 2;
		    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
		    	$response['type_dialog']  = 'Error';
		    	$response['id_transaksi']  = 0;
		    	return $response;
				exit();
			}

			/*Get Id Table Trans*/
			$save = DB::table('trans_pln')
						->where('transactionId_sobatku',$transactionId)
						->first()->id;

			$tr_out = Esta::log_money($id_agen,($trans_amount == 0 ? $trans_amount : $trans_amount),date('Y-m-d H:i:s'),'Transaksi PLN Prabayar','Transaksi PLN Prabayar '.$detail_product->product_name,'Out','Transaksi','trans_pln',$save,$saldo_sebelum);
			$cb_in = Esta::log_money($id_agen,$komisi,date('Y-m-d H:i:s'),'Cashback Transaksi PLN Prabayar','Cashback Transaksi PLN Prabayar '.$detail_product->product_name,'In','Komisi','trans_pln',$save,$saldo_sebelum-$trans_amount);

			$last_transaksi2 = Esta::last_transaksi($id_agen,'trans_pln');
			if($last_transaksi2 == 1) {
				$del_tr = DB::table('trans_pln')
					->where('id',$save)
					->delete();
				$del_log = DB::table('log_money')
					->where('tbl_transaksi','trans_pln')
					->where('id_transaksi',$save)
					->delete();

				$agen = DB::table('agen')
					->where('id',$id_agen)
					->first();

				$saldo_sekarang = $saldo;
				$up_s['saldo'] = $saldo_sekarang+$trans_amount;
				/*$update_saldo = DB::table('agen')
					->where('id',$id_agen)
					->update($up_s);*/

				/*reversal sobatku*/
				/*$start_time = time();
				while(true){
					sleep(5);
					$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
					$user = CRUDBooster::getsetting('user_sobatku_api');
					$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

					$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
					$keterangan = 'Reversal Payment';
					$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));

					$request = [
						'oldRefNum' => $oldRefNum,
						'referenceNumber' => $referenceNumber[0]->Prefix,
						'user' => $user,
						'hashCode' => hash('sha256', $oldRefNum.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
					];

					$data_toJson = json_encode( $request );
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$responsereversal = curl_exec( $ch );
					$responseerror = curl_errno($ch);
					if ($responseerror == 28){
						$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',1));
						break;
					}else {
						$responseBody = json_decode( $responsereversal, true);													

						if($responseBody['responseCode'] == '00' ){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',0));
							break;
						}else if((time() - $start_time) > 300){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',0));
							break;
						}
					}
					
					curl_close($ch);	
				}*/

				Esta::add_fraud($id_agen);
				$response['api_status']  = 2;
		    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
		    	$response['type_dialog']  = 'Error';
		    	$response['id_transaksi']  = 0;
		    	return $response;
				exit();
			}

			/*send purchase*/
			$send_iso_purchase = Esta::send_iso_pln_prabayar($id_product,$no_meter,'0200',$id_transaksi,'Purchase','0',$unsold,$unsold_nominal);
			
			/*print_r($send_iso_purchase);
			exit();*/
			if($send_iso_purchase['39'] == 'Advice Manual' || $send_iso_purchase['39'] == '18' || $send_iso_purchase['39'] == '96') {

				$up['status'] = 'Advice Manual';
				$up['stan'] = $send_iso_purchase['stan'];

				$update = DB::table('trans_pln')
					->where('id',$save)
					->update($up);

				$response['api_status']  = 3;
		    	$response['api_message'] = 'Transaksi sedang diproses, klik menu ongoing untuk manual advice';
		    	$response['id_transaksi']  = 0;
		    	$response['type_dialog']  = 'Informasi';

		    	$response['rc'] = $send_iso_purchase['39'];
		    	$response['pln_pra_meter_id'] = $send_iso_purchase['pln_pra_meter_id'];
		    	$response['pln_pra_id_pel'] = $send_iso_purchase['pln_pra_id_pel'];
				$response['pln_pra_nama'] = $send_iso_purchase['pln_pra_nama'];
				$response['pln_pra_tarif'] = str_replace(' ', '', $send_iso_purchase['pln_pra_tarif']);
				$response['pln_pra_kategori_daya'] = $send_iso_purchase['pln_pra_kategori_daya'];
				$response['pln_pra_ref_no'] = $send_iso_purchase['pln_pra_ref_no'];
				$response['pln_pra_biaya_materai'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['pln_pra_ppn'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['pln_pra_ppju'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['pln_pra_angsuran'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['pln_pra_rp_stroom'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
				$response['pln_pra_jml_kwh'] = substr(Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_jml_kwh'],0),$send_iso_purchase['pln_pra_nilai_minor_kwh']),0,-1);
				$response['pln_pra_token_number'] = chunk_split($send_iso_purchase['pln_pra_token_number'],4,' ');
				$response['pln_pra_biaya_admin'] = 'Rp '.number_format($detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan,0,',','.');

				$response['val_pln_pra_rp_stroom'] = ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0);
				$response['val_pln_pra_biaya_materai'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['val_pln_pra_ppn'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['val_pln_pra_ppju'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['val_pln_pra_angsuran'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['val_pln_pra_biaya_admin'] = $detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan;

				$response['pln_pra_pembelian_listrik'] = 'Rp '.Esta::nilai_minor($response['val_pln_pra_rp_stroom']+$response['val_pln_pra_biaya_admin'],$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
				$response['info'] = str_replace(array('TMP','#"TMP"#'), array('"TMP"','"TMP"'), $send_iso_purchase['62']);
				$response['val_komisi'] = $komisi;
				$response['komisi'] = 'Rp '.number_format($komisi,0,',','.');
				$response['pln_pra_tgl_lunas'] = substr($send_iso_purchase['pln_pra_tgl_lunas'],6,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],4,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],0,4).' '.substr($send_iso_purchase['pln_pra_tgl_lunas'],8,2).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],10,1).substr($send_iso_purchase['pln_pra_tgl_lunas'],11,1).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],12,2);
				//$response['nominal_total_pembayaran'] = 'Rp '.number_format($trans_amount, 0, ',', '.');
				$response['nominal_voucher'] = ($detail_voucher->amount <= 0 ? "" : 'Rp '.number_format($detail_voucher->amount,0,',','.'));
				$response['tipe_pembayaran'] = 'PLN Prepaid';

				$response['val_2_pln_pra_ppn'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['val_2_pln_pra_ppju'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['val_2_pln_pra_angsuran'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['val_2_pln_pra_biaya_materai'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['val_2_pln_pra_rp_stroom'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);

				$response['val_rp_bayar'] = $response['val_pln_pra_biaya_admin']+$product_amount;
				$response['rp_bayar'] = 'Rp '.number_format($response['val_rp_bayar'],0,',','.');
				$total_pembayaran = $response['val_rp_bayar']-$response['val_komisi']-$detail_voucher->amount;
				$response['total_pembayaran'] = 'Rp '.number_format(($total_pembayaran <= 0 ? 0 : $total_pembayaran),0,',','.');
				$response['pln_pra_biaya_admin'] = 'Rp '.number_format($response['val_pln_pra_biaya_admin'],0,',','.');

				/*struk*/
				$struk_id_pelanggan = $response['pln_pra_id_pel'];
				$no_meter = $response['pln_pra_meter_id'];
				$struk_info = $response['info'];
				$struk_nama_pelanggan = $response['pln_pra_nama'];
				$struk_tarif = $response['pln_pra_tarif'];
				$struk_daya = $response['pln_pra_kategori_daya'];
				$struk_rp_bayar = $response['val_rp_bayar'];
				$struk_materai = $response['val_2_pln_pra_biaya_materai'];
				$struk_ppn = $response['val_2_pln_pra_ppn'];
				$struk_ppj = $response['val_2_pln_pra_ppju'];
				$struk_angsuran = $response['val_2_pln_pra_angsuran'];
				$struk_rp_stroom = $response['val_2_pln_pra_rp_stroom'];
				$struk_jml_kwh = $response['pln_pra_jml_kwh'];
				$struk_token = $response['pln_pra_token_number'];
				$struk_admin_bank = $response['val_pln_pra_biaya_admin'];
				$struk_total_pembayaran = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
				$struk_tgl_lunas = $response['pln_pra_tgl_lunas'];
				$jpa_ref = $response['pln_pra_ref_no'];

				/*$up_struk = DB::table('trans_pln')
					->where('id',$save)
					->update($st);*/
				$up_struk = DB::statement('exec updateTransPlnPrabayarAdviceManual ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
							,array($save,$struk_id_pelanggan,$no_meter,$struk_info ,$struk_nama_pelanggan,$struk_tarif,$struk_daya,$struk_rp_bayar,$struk_materai,$struk_ppn,$struk_ppj,$struk_angsuran,$struk_rp_stroom,$struk_jml_kwh,$struk_token,$struk_admin_bank,$struk_total_pembayaran,$struk_tgl_lunas,$jpa_ref));

		    	return $response;
		    	exit();
			}

			if($send_iso_purchase['39'] != '00') { /*jika respon error*/
				/*reversal sobatku*/
				$start_time = time();
				//$refrn = 0;
				while(true){
					
					//$refrn++;
					$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
					//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
					$user = CRUDBooster::getsetting('user_sobatku_api');
					$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

					$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
					$keterangan = 'Reversal Payment';
					$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
					$referenceNumber = $referenceNumber[0]->Prefix;
					$request = [
						'oldRefNum' => $oldRefNum,
						'referenceNumber' => $referenceNumber,
						'user' => $user,
						'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
					];

					$data_toJson = json_encode( $request );
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
					curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
					$responsereversal = curl_exec( $ch );
					$responseerror = curl_errno($ch);
					if ($responseerror == 28){
						$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
						break;
					}else {
						$responseBody = json_decode( $responsereversal, true);													

						if($responseBody['responseCode'] == '00' ){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
							/*kembalikan saldo*/
							Esta::log_money($id_agen,($trans_amount == 0 ? $trans_amount : $trans_amount),date('Y-m-d H:i:s'),'Transaksi PLN Prabayar','Return Transaksi PLN Prabayar '.$detail_product->product_name,'In','Transaksi','trans_pln',$save,$saldo_sebelum-$trans_amount);
							Esta::log_money($id_agen,$komisi,date('Y-m-d H:i:s'),'Cashback Transaksi PLN Prabayar','Return Cashback Transaksi PLN Prabayar '.$detail_product->product_name,'Out','Komisi','trans_pln',$save,$saldo_sebelum);
							break;
						}elseif($responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3' || $responseBody['responseCode'] == 'P1'){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
							break;
						}elseif((time() - $start_time) > 60){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
							break;
						}else{
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
						}
					}
					
					curl_close($ch);	
					sleep(10);
				}
				
				$up['return_saldo'] = 'Yes';
				$up['status'] = 'Error';
				$up['error_code'] = $send_iso['39'];
				$up['stan'] = $send_iso_purchase['stan'];

				$update = DB::table('trans_pln')
					->where('id',$save)
					->update($up);
					
				if(!empty($id_voucher)) {
					$uv['used'] = 'No';

					$up_voucher = DB::table('trans_voucher_child')
						->where('id',$id_voucher_child)
						->where('id_agen',$id_agen)
						->update($uv);
				}


				$response['api_status']  = 2;
		    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_purchase['39'],$no_meter);
		    	$response['id_transaksi']  = 0;
		    	$response['type_dialog']  = 'Informasi';

		    	$response['rc'] = $send_iso_purchase['39'];
		    	$response['pln_pra_id_pel'] = $send_iso_purchase['pln_pra_id_pel'];
				$response['pln_pra_nama'] = $send_iso_purchase['pln_pra_nama'];
				$response['pln_pra_tarif'] = str_replace(' ', '', $send_iso_purchase['pln_pra_tarif']);
				$response['pln_pra_kategori_daya'] = $send_iso_purchase['pln_pra_kategori_daya'];
				$response['pln_pra_ref_no'] = $send_iso_purchase['pln_pra_ref_no'];
				$response['pln_pra_biaya_materai'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['pln_pra_ppn'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['pln_pra_ppju'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['pln_pra_angsuran'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['pln_pra_rp_stroom'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
				$response['pln_pra_jml_kwh'] = substr(Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_jml_kwh'],0),$send_iso_purchase['pln_pra_nilai_minor_kwh']),0,-1);
				$response['pln_pra_token_number'] = chunk_split($send_iso_purchase['pln_pra_token_number'],4,' ');
				$response['pln_pra_biaya_admin'] = 'Rp '.number_format($detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan,0,',','.');

				$response['val_pln_pra_rp_stroom'] = ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0);
				$response['val_pln_pra_biaya_materai'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['val_pln_pra_ppn'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['val_pln_pra_ppju'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['val_pln_pra_angsuran'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['val_pln_pra_biaya_admin'] = $detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan;

				$response['pln_pra_pembelian_listrik'] = 'Rp '.Esta::nilai_minor($response['val_pln_pra_rp_stroom']+$response['val_pln_pra_biaya_admin'],$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
				$response['info'] = str_replace(array('TMP','#"TMP"#'), array('"TMP"','"TMP"'), $send_iso_purchase['62']);
				$response['val_komisi'] = $komisi;
				$response['komisi'] = 'Rp '.number_format($komisi,0,',','.');
				$response['pln_pra_tgl_lunas'] = substr($send_iso_purchase['pln_pra_tgl_lunas'],6,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],4,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],0,4).' '.substr($send_iso_purchase['pln_pra_tgl_lunas'],8,2).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],10,1).substr($send_iso_purchase['pln_pra_tgl_lunas'],11,1).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],12,2);
				//$response['nominal_total_pembayaran'] = 'Rp '.number_format($trans_amount, 0, ',', '.');
				$response['nominal_voucher'] = ($detail_voucher->amount <= 0 ? "" : 'Rp '.number_format($detail_voucher->amount,0,',','.'));
				$response['tipe_pembayaran'] = 'PLN Prepaid';

				$response['val_2_pln_pra_ppn'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
				$response['val_2_pln_pra_ppju'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
				$response['val_2_pln_pra_angsuran'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
				$response['val_2_pln_pra_biaya_materai'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
				$response['val_2_pln_pra_rp_stroom'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);

				$response['val_rp_bayar'] = $response['val_pln_pra_biaya_admin']+$product_amount;
				$response['rp_bayar'] = 'Rp '.number_format($response['val_rp_bayar'],0,',','.');
				$total_pembayaran = $response['val_rp_bayar']-$response['val_komisi']-$detail_voucher->amount;
				$response['total_pembayaran'] = 'Rp '.number_format(($total_pembayaran <= 0 ? 0 : $total_pembayaran),0,',','.');
				$response['pln_pra_biaya_admin'] = 'Rp '.number_format($response['val_pln_pra_biaya_admin'],0,',','.');

				/*struk*/
				$struk_id_pelanggan = $response['pln_pra_id_pel'];
				$no_meter = $response['pln_pra_meter_id'];
				$struk_info = $response['info'];
				$struk_nama_pelanggan = $response['pln_pra_nama'];
				$struk_tarif = $response['pln_pra_tarif'];
				$struk_daya = $response['pln_pra_kategori_daya'];
				$struk_rp_bayar = $response['val_rp_bayar'];
				$struk_materai = $response['val_2_pln_pra_biaya_materai'];
				$struk_ppn = $response['val_2_pln_pra_ppn'];
				$struk_ppj = $response['val_2_pln_pra_ppju'];
				$struk_angsuran = $response['val_2_pln_pra_angsuran'];
				$struk_rp_stroom = $response['val_2_pln_pra_rp_stroom'];
				$struk_jml_kwh = $response['pln_pra_jml_kwh'];
				$struk_token = $response['pln_pra_token_number'];
				$struk_admin_bank = $response['val_pln_pra_biaya_admin'];
				$struk_total_pembayaran = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
				$struk_tgl_lunas = $response['pln_pra_tgl_lunas'];
				$jpa_ref = $response['pln_pra_ref_no'];

				/*$up_struk = DB::table('trans_pln')
					->where('id',$save)
					->update($st);*/
				$up_struk = DB::statement('exec updateTransPlnPrabayarAdviceManual ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
							,array($save,$struk_id_pelanggan,$no_meter,$struk_info,$struk_nama_pelanggan,$struk_tarif,$struk_daya,$struk_rp_bayar,$struk_materai,$struk_ppn,$struk_ppj,$struk_angsuran,$struk_rp_stroom,$struk_jml_kwh,$struk_token,$struk_admin_bank,$struk_total_pembayaran,$struk_tgl_lunas,$jpa_ref));

		    	return $response;
		    	exit();
			}

			$up['status'] = 'Clear';
			$up['error_code'] = $send_iso['39'];
			$up['stan'] = $send_iso_purchase['stan'];

			$update = DB::table('trans_pln')
				->where('id',$save)
				->update($up);

			$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_pln');
			if($send_sms_transaksi == 'Yes') {
				$msg = CRUDBooster::getsetting('sukses_transaksi_pln');
				$msg = str_replace('[no_hp]', $no_hp, $msg);
				$msg = str_replace('[nama]', $detail_agen->nama, $msg);
				$msg = str_replace('[komisi]', $komisi, $msg);
				Esta::send_sms($no_hp, $msg);
			}

			if(!empty($id_voucher)) {
				$uv['used'] = 'Yes';

				$up_voucher = DB::table('trans_voucher_child')
					->where('id',$id_voucher_child)
					->where('id_agen',$id_agen)
					->update($uv);
			}

			$response['api_status']  = 1;
	    	$response['api_message'] = Esta::show_error('PLN Prepaid',$send_iso_purchase['39'],$no_meter);
	    	$response['type_dialog']  = 'Informasi';
	    	$response['id_transaksi']  = $save;
	    	$response['id_log']  = $send_iso_purchase['id_log'];

	    	$response['rc'] = $send_iso_purchase['39'];
	    	$response['pln_pra_meter_id'] = $send_iso_purchase['pln_pra_meter_id'];
	    	$response['pln_pra_id_pel'] = $send_iso_purchase['pln_pra_id_pel'];
			$response['pln_pra_nama'] = $send_iso_purchase['pln_pra_nama'];
			$response['pln_pra_tarif'] = str_replace(' ', '', $send_iso_purchase['pln_pra_tarif']);
			$response['pln_pra_kategori_daya'] = $send_iso_purchase['pln_pra_kategori_daya'];
			$response['pln_pra_ref_no'] = $send_iso_purchase['pln_pra_ref_no'];
			$response['pln_pra_biaya_materai'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['pln_pra_ppn'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['pln_pra_ppju'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['pln_pra_angsuran'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['pln_pra_rp_stroom'] = 'Rp '.Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
			$response['pln_pra_jml_kwh'] = substr(Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_jml_kwh'],0),$send_iso_purchase['pln_pra_nilai_minor_kwh']),0,-1);
			$response['pln_pra_token_number'] = chunk_split($send_iso_purchase['pln_pra_token_number'],4,' ');
			$response['pln_pra_biaya_admin'] = 'Rp '.number_format($detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan,0,',','.');

			$response['val_pln_pra_rp_stroom'] = ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0);
			$response['val_pln_pra_biaya_materai'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['val_pln_pra_ppn'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['val_pln_pra_ppju'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['val_pln_pra_angsuran'] = Esta::nilai_minor(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['val_pln_pra_biaya_admin'] = $detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan;

			$response['pln_pra_pembelian_listrik'] = 'Rp '.Esta::nilai_minor($response['val_pln_pra_rp_stroom']+$response['val_pln_pra_biaya_admin'],$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);
			$response['info'] = str_replace(array('TMP','#"TMP"#'), array('"TMP"','"TMP"'), $send_iso_purchase['62']);
			$response['val_komisi'] = $komisi;
			$response['komisi'] = 'Rp '.number_format($komisi,0,',','.');
			$response['pln_pra_tgl_lunas'] = substr($send_iso_purchase['pln_pra_tgl_lunas'],6,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],4,2).'/'.substr($send_iso_purchase['pln_pra_tgl_lunas'],0,4).' '.substr($send_iso_purchase['pln_pra_tgl_lunas'],8,2).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],10,1).substr($send_iso_purchase['pln_pra_tgl_lunas'],11,1).':'.substr($send_iso_purchase['pln_pra_tgl_lunas'],12,2);
			//$response['nominal_total_pembayaran'] = 'Rp '.number_format($trans_amount, 0, ',', '.');
			$response['nominal_voucher'] = ($detail_voucher->amount <= 0 ? "" : 'Rp '.number_format($detail_voucher->amount,0,',','.'));
			$response['tipe_pembayaran'] = 'PLN Prepaid';

			$response['val_2_pln_pra_ppn'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppn'],0),$send_iso_purchase['pln_pra_nilai_minor_ppn']);
			$response['val_2_pln_pra_ppju'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_ppju'],0),$send_iso_purchase['pln_pra_nilai_minor_ppju']);
			$response['val_2_pln_pra_angsuran'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_angsuran'],0),$send_iso_purchase['pln_pra_nilai_minor_angsuran']);
			$response['val_2_pln_pra_biaya_materai'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_biaya_materai'],0),$send_iso_purchase['pln_pra_nilai_minor_materai']);
			$response['val_2_pln_pra_rp_stroom'] = Esta::nilai_minor_val(ltrim($send_iso_purchase['pln_pra_pembelian_listrik'],0),$send_iso_purchase['pln_pra_nilai_minor_pembelian_listrik']);

			$response['val_rp_bayar'] = $response['val_pln_pra_biaya_admin']+$product_amount;
			$response['rp_bayar'] = 'Rp '.number_format($response['val_rp_bayar'],0,',','.');
			$total_pembayaran = $response['val_rp_bayar']-$response['val_komisi']-$detail_voucher->amount;
			$response['total_pembayaran'] = 'Rp '.number_format(($total_pembayaran <= 0 ? 0 : $total_pembayaran),0,',','.');
			$response['pln_pra_biaya_admin'] = 'Rp '.number_format($response['val_pln_pra_biaya_admin'],0,',','.');

			/*struk*/
			$struk_id_pelanggan = $response['pln_pra_id_pel'];
			$no_meter = $response['pln_pra_meter_id'];
			$struk_info = $response['info'];
			$struk_nama_pelanggan = $response['pln_pra_nama'];
			$struk_tarif = $response['pln_pra_tarif'];
			$struk_daya = $response['pln_pra_kategori_daya'];
			$struk_rp_bayar = $response['val_rp_bayar'];
			$struk_materai = $response['val_2_pln_pra_biaya_materai'];
			$struk_ppn = $response['val_2_pln_pra_ppn'];
			$struk_ppj = $response['val_2_pln_pra_ppju'];
			$struk_angsuran = $response['val_2_pln_pra_angsuran'];
			$struk_rp_stroom = $response['val_2_pln_pra_rp_stroom'];
			$struk_jml_kwh = $response['pln_pra_jml_kwh'];
			$struk_token = $response['pln_pra_token_number'];
			$struk_admin_bank = $response['val_pln_pra_biaya_admin'];
			$struk_total_pembayaran = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
			$struk_tgl_lunas = $response['pln_pra_tgl_lunas'];
			$jpa_ref = $response['pln_pra_ref_no'];			

			/*$up_struk = DB::table('trans_pln')
				->where('id',$save)
				->update($st);*/
			$up_struk = DB::statement('exec updateTransPlnPrabayarAdviceManual ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
						,array($save,$struk_id_pelanggan,$no_meter,$struk_info ,$struk_nama_pelanggan,$struk_tarif,$struk_daya,$struk_rp_bayar,$struk_materai,$struk_ppn,$struk_ppj,$struk_angsuran,$struk_rp_stroom,$struk_jml_kwh,$struk_token,$struk_admin_bank,$struk_total_pembayaran,$struk_tgl_lunas,$jpa_ref));

			$sterr['error_code'] = $response['rc'];

			$update = DB::table('trans_pln')
				->where('id',$save)
				->update($sterr);
			/*email*/

			$view     = view('struk/struk_pln_prepaid',$response)->render();
			$filename = "Struk-PLN-Prepaid-".$response['pln_pra_id_pel'];
			$pdf      = App::make('dompdf.wrapper');

			$path = storage_path('app/uploads/'.$filename.'.pdf');

			$pdf->loadHTML($view);
			$pdf->setPaper('A4','landscape');
			$output = $pdf->output();

			file_put_contents($path, $output);

			$attachments = [$path];
			$email = $detail_agen->email;
			if(!empty($email) && $detail_agen->notif_email != 'No') {
                //if($pin != 'estakiosprosespaket') {
                   try{
					   Esta::kirimemail(['to' => $email, 'data' => $response, 'template' => 'email_transaksi_pln_prepaid', 'attachments' => $attachments]);
				   }catch(\Exception $e){               	
				   }
                //}
			}

		return $response;
	}

	public function postPlnPascabayarSubmit() {
		$id_agen = Request::get('id_agen');
        $request_transaksi = Request::get('request_transaksi');
        $transactionId = Request::get('transactionId');
        $request_transaksi = json_decode( $request_transaksi,true );
		/*$agen = Auth::user();
        $id_agen = $agen->id;*/
		$regid = $request_transaksi['regid'];
		$token = $request_transaksi['token'];
		$cek_regid = Esta::cek_regid($id_agen,$regid);

        $getlog_transaction = DB::table('log_sobatku_payment')
            ->where('transactionId',$transactionId)
            ->where('keterangan','Request Payment')
            ->first();
        $oldRefNum = $getlog_transaction->ref_number;
        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

        $ut['is_used'] = 1;
        DB::table('temp_trans_payment')
					->where('transactionId',$transactionId)
					->update($ut);

		/*if($cek_regid == 0) {
			$response['api_status']  = 2;
		    $response['api_message'] = 'Terjadi gangguan pada koneksi internet atau Server. Silahkan coba beberapa saat lagi';//'Akun Anda terdeteksi login di HP lain, Silahkan logout dan login kembali untuk melanjutkan transaksi.';
		    $response['type_dialog']  = 'Error';
		    //Esta::add_fraud($id_agen);
		    return response()->json($response);
		}*/

		$no_hp = $request_transaksi['no_hp'];
		$no_meter = $request_transaksi['no_meter'];
		//$id_voucher = Request::get('id_voucher');
		$pin = $request_transaksi['pin'];
		$amount = $request_transaksi['amount'];
		$id_transaksi = $request_transaksi['id_transaksi'];

		$tipe_layanan = DB::table('pan_tipe_layanan')->where('kode','00')->first()->id;
		$kode_layanan = DB::table('pan_kode_layanan')->where('kode','4')->first()->id;
		$id_product = DB::table('pan_product')->where('id_tipe_layanan',$tipe_layanan)->where('id_kode_layanan',$kode_layanan)->first()->id;

		$id_voucher_child = $request_transaksi['id_voucher'];
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		/*if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}*/

			$detail_product = DB::table('pan_product')
				->where('id',$id_product)
				->first();

			$detail_voucher = DB::table('voucher')
				->where('id',$id_voucher)
				->first();

			$kode = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));

			$trans_amount = Esta::amount_product_pln_pascabayar($id_product,$id_agen,$id_voucher,$amount);
			
			/*echo $trans_amount.'-'.($trans_amount <= 0 ? 0 : ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium));

			Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN','Transaksi PLN '.$detail_product->product_name,'Out','Transaksi');
			Esta::log_money($id_agen,($trans_amount <= 0 ? 0 : ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium)),date('Y-m-d H:i:s'),'Komisi Transaksi PLN','Komisi Transaksi PLN '.$detail_product->product_name,'In','Komisi');
			exit();*/
			$status_match_sobatku = 'Waiting Rekon';
			//if($saldo >= $trans_amount) {
				try{
					$sv['created_at'] = date('Y-m-d H:i:s');
					$sv['updated_at'] = date('Y-m-d H:i:s');
					$sv['created_user'] = Esta::user($id_agen);
					$sv['updated_user'] = Esta::user($id_agen);
					$sv['trans_no'] = $kode;
					$sv['ref_trans_no'] = $kode;
					$sv['trans_date'] = date('Y-m-d H:i:s');
					$sv['trans_desc'] = 'PLN PASCABAYAR';
					$sv['currency'] = 'IDR';
					$sv['trans_amount'] = $amount+$detail_voucher->amount-($detail_product->margin+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3-$detail_product->potongan);
					$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$sv['create_user'] = $detail_agen->nama;
					$sv['id_agen'] = $id_agen;
					$sv['status'] = 'Pending';
					$sv['rekon_amount'] = 0;
					$sv['status_match'] = 'Waiting Rekon';
					$sv['no_hp'] = $no_hp;
					$sv['no_meter'] = $no_meter;
					$sv['id_product'] = $id_product;
					$sv['agen_nama'] = $detail_agen->nama;
					$sv['batch_last_user'] = $detail_agen->nama;
					$sv['agen_email'] = $detail_agen->email;
					$sv['agen_kode'] = $detail_agen->kode;
					$sv['agen_no_hp'] = $detail_agen->no_hp;
					$sv['agen_level'] = $detail_agen->status_agen;
					$sv['agen_referall'] = $detail_agen->kode_referall_agen;
					$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
					$sv['agen_status_aktif'] = $detail_agen->status_aktif;
					$sv['agen_nik'] = $detail_agen->nik;
					$sv['agen_tgl_register'] = $detail_agen->tgl_register;
					$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
					$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
					$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
					$sv['agen_agama'] = $detail_agen->agama;
					$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
					$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
					$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
					$sv['agen_prov'] = $detail_agen->prov;
					$sv['agen_kab'] = $detail_agen->kab;
					$sv['agen_kec'] = $detail_agen->kec;
					$sv['agen_kel'] = $detail_agen->kel;
					$sv['product_nama'] = $detail_product->product_name;
					$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$sv['product_komisi_basic'] = $detail_product->komisi_basic;
					$sv['product_komisi_premium'] = $detail_product->komisi_premium;
					$sv['product_amount'] = $amount+$detail_voucher->amount-($detail_product->margin+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3-$detail_product->potongan);
					$sv['product_margin'] = $detail_product->margin;
					$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
					$sv['product_biaya_admin_1'] = $detail_product->admin_1;
					$sv['product_biaya_admin_2'] = $detail_product->admin_2;
					$sv['product_biaya_admin_3'] = $detail_product->admin_3;
					$sv['product_potongan'] = $detail_product->potongan;
					$sv['voucher_nama'] = $detail_voucher->nama;
					$sv['voucher_expired_date'] = $detail_voucher->expired_date;
					$sv['voucher_amount'] = $detail_voucher->amount;
					$sv['voucher_tagline'] = $detail_voucher->tagline;
					$sv['id_voucher'] = $detail_voucher->id;
					$sv['voucher_description'] = $detail_voucher->description;
					$sv['no_ref_sobatku'] = $oldRefNum;
					$sv['transactionId_sobatku'] = $transactionId;
					$sv['status_match_sobatku'] = $status_match_sobatku;

					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pln');
					
					if($last_transaksi == 1) {
						/*reversal sobatku*/
						/*$start_time = time();
						while(true){
							sleep(5);
							$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
							$user = CRUDBooster::getsetting('user_sobatku_api');
							$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

							$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
							$keterangan = 'Reversal Payment';
							$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));

							$request = [
								'oldRefNum' => $oldRefNum,
								'referenceNumber' => $referenceNumber[0]->Prefix,
								'user' => $user,
								'hashCode' => hash('sha256', $oldRefNum.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
							];

							$data_toJson = json_encode( $request );
							$ch = curl_init( $serviceURL );
							curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
							curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
							curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
									'Content-Type: application/json',
									'Accept:application/json'
								)
							);
							$responsereversal = curl_exec( $ch );
							$responseerror = curl_errno($ch);
							if ($responseerror == 28){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',1));
								break;
							}else {
								$responseBody = json_decode( $responsereversal, true);													

								if($responseBody['responseCode'] == '00' ){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',0));
									break;
								}else if((time() - $start_time) > 300){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',0));
									break;
								}
							}
							
							curl_close($ch);	
						}*/
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					/*$save = DB::table('trans_pln')
						->insertGetId($sv);*/
					/*$saveTransPLN = DB::select('exec postTransPlnPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								, array_values($sv));
					$save = $saveTransPLN[0]->id;*/
				} catch(\Exception $e) {
					
					$kode = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));
					$sv['created_at'] = date('Y-m-d H:i:s');
					$sv['updated_at'] = date('Y-m-d H:i:s');
					$sv['created_user'] = Esta::user($id_agen);
					$sv['updated_user'] = Esta::user($id_agen);
					$sv['trans_no'] = $kode;
					$sv['ref_trans_no'] = $kode;
					$sv['trans_date'] = date('Y-m-d H:i:s');
					$sv['trans_desc'] = 'PLN PASCABAYAR';
					$sv['currency'] = 'IDR';
					$sv['trans_amount'] = $amount+$detail_voucher->amount-($detail_product->margin+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3-$detail_product->potongan);
					$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$sv['create_user'] = $detail_agen->nama;
					$sv['id_agen'] = $id_agen;
					$sv['status'] = 'Pending';
					$sv['rekon_amount'] = 0;
					$sv['status_match'] = 'Waiting Rekon';
					$sv['no_hp'] = $no_hp;
					$sv['no_meter'] = $no_meter;
					$sv['id_product'] = $id_product;
					$sv['agen_nama'] = $detail_agen->nama;
					$sv['batch_last_user'] = $detail_agen->nama;
					$sv['agen_email'] = $detail_agen->email;
					$sv['agen_kode'] = $detail_agen->kode;
					$sv['agen_no_hp'] = $detail_agen->no_hp;
					$sv['agen_level'] = $detail_agen->status_agen;
					$sv['agen_referall'] = $detail_agen->kode_referall_agen;
					$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
					$sv['agen_status_aktif'] = $detail_agen->status_aktif;
					$sv['agen_nik'] = $detail_agen->nik;
					$sv['agen_tgl_register'] = $detail_agen->tgl_register;
					$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
					$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
					$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
					$sv['agen_agama'] = $detail_agen->agama;
					$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
					$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
					$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
					$sv['agen_prov'] = $detail_agen->prov;
					$sv['agen_kab'] = $detail_agen->kab;
					$sv['agen_kec'] = $detail_agen->kec;
					$sv['agen_kel'] = $detail_agen->kel;
					$sv['product_nama'] = $detail_product->product_name;
					$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$sv['product_komisi_basic'] = $detail_product->komisi_basic;
					$sv['product_komisi_premium'] = $detail_product->komisi_premium;
					$sv['product_amount'] = $amount+$detail_voucher->amount-($detail_product->margin+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3-$detail_product->potongan);
					$sv['product_margin'] = $detail_product->margin;
					$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
					$sv['product_biaya_admin_1'] = $detail_product->admin_1;
					$sv['product_biaya_admin_2'] = $detail_product->admin_2;
					$sv['product_biaya_admin_3'] = $detail_product->admin_3;
					$sv['product_potongan'] = $detail_product->potongan;
					$sv['voucher_nama'] = $detail_voucher->nama;
					$sv['voucher_expired_date'] = $detail_voucher->expired_date;
					$sv['voucher_amount'] = $detail_voucher->amount;
					$sv['voucher_tagline'] = $detail_voucher->tagline;
					$sv['id_voucher'] = $detail_voucher->id;
					$sv['voucher_description'] = $detail_voucher->description;
					$sv['no_ref_sobatku'] = $oldRefNum;
					$sv['transactionId_sobatku'] = $transactionId;
					$sv['status_match_sobatku'] = $status_match_sobatku;

					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pln');
					
					if($last_transaksi == 1) {
						/*reversal sobatku*/
						/*$start_time = time();
						while(true){
							sleep(5);
							$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
							$user = CRUDBooster::getsetting('user_sobatku_api');
							$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

							$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
							$keterangan = 'Reversal Payment';
							$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));

							$request = [
								'oldRefNum' => $oldRefNum,
								'referenceNumber' => $referenceNumber[0]->Prefix,
								'user' => $user,
								'hashCode' => hash('sha256', $oldRefNum.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
							];

							$data_toJson = json_encode( $request );
							$ch = curl_init( $serviceURL );
							curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
							curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
							curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
									'Content-Type: application/json',
									'Accept:application/json'
								)
							);
							$responsereversal = curl_exec( $ch );
							$responseerror = curl_errno($ch);
							if ($responseerror == 28){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',1));
								break;
							}else {
								$responseBody = json_decode( $responsereversal, true);													

								if($responseBody['responseCode'] == '00' ){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',0));
									break;
								}else if((time() - $start_time) > 300){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$data_toJson,$responsereversal,'Reversal Payment',0));
									break;
								}
							}
							
							curl_close($ch);	
						}*/
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					/*$save = DB::table('trans_pln')
						->insertGetId($sv);*/
					/*$saveTransPLN = DB::select('exec postTransPlnPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								, array_values($sv));
					$save = $saveTransPLN[0]->id;*/
				}

				/*Get Id Table Trans*/
				$save = DB::table('trans_pln')
							->where('transactionId_sobatku',$transactionId)
							->first()->id;

				/*potong saldo*/
				$tr_out = Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN','Transaksi PLN '.$detail_product->product_name,'Out','Transaksi','trans_pln',$save,$saldo_sebelum);
				$cb_in = Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi PLN','Komisi Transaksi PLN '.$detail_product->product_name,'In','Komisi','trans_pln',$save,$saldo_sebelum-$trans_amount);

				/*send iso*/
				$send_iso = Esta::send_iso_pln_pascabayar($id_product,$no_meter,'0200',$id_transaksi,'Purchase');

				/*print_r($send_iso);
				exit();*/

				if($send_iso['39'] != '97') {
					if($send_iso['39'] != '00' || $send_iso['status'] == 'Reversal' || $send_iso['status'] == 'Reversal Repeat' || $send_iso['status'] == 'Reversal Repeat 2') { /*jika respon error*/
						/*reversal sobatku*/
						$start_time = time();
						//$refrn = 0;
						while(true){
							
							//$refrn++;
							$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
							//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
							$user = CRUDBooster::getsetting('user_sobatku_api');
							$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

							$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
							$keterangan = 'Reversal Payment';
							$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
							$referenceNumber = $referenceNumber[0]->Prefix;
							$request = [
								'oldRefNum' => $oldRefNum,
								'referenceNumber' => $referenceNumber,
								'user' => $user,
								'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
							];

							$data_toJson = json_encode( $request );
							$ch = curl_init( $serviceURL );
							curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
							curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
							curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
							curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
									'Content-Type: application/json',
									'Accept:application/json'
								)
							);
							curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
							curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
							$responsereversal = curl_exec( $ch );
							$responseerror = curl_errno($ch);
							if ($responseerror == 28){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
								break;
							}else {
								$responseBody = json_decode( $responsereversal, true);													

								if($responseBody['responseCode'] == '00' ){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
									/*kembalikan saldo*/
									Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN','Return Transaksi PLN '.$detail_product->product_name,'In','Transaksi','trans_pln',$save,$saldo_sebelum-$trans_amount);
									Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi PLN','Return Komisi Transaksi PLN '.$detail_product->product_name,'Out','Komisi','trans_pln',$save,$saldo_sebelum);
									break;
								}elseif($responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3' || $responseBody['responseCode'] == 'P1'){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
									break;
								}elseif((time() - $start_time) > 60){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
									break;
								}else{
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
								}
							}
							
							curl_close($ch);
							sleep(10);
						}
						/*kembalikan saldo*/
						$up['id'] = $save;
						$up['return_saldo'] = '';
						if($send_iso['39'] == '00' || $send_iso['39'] == '94' || $send_iso['39'] == '61') {
							$up['return_saldo'] = 'Yes';
						}

						$up['status'] = 'Error';
						$up['error_code'] = $send_iso['39'];
						$up['jpa_ref'] = $send_iso['pln_psc_no_ref'];
						$up['product_margin'] = $detail_product->margin*$send_iso['jml_tagihan'];
						$up['product_biaya_admin_edn'] = $detail_product->admin_edn*$send_iso['jml_tagihan'];
						$up['product_biaya_admin_1'] = $detail_product->admin_1*$send_iso['jml_tagihan'];
						$up['product_biaya_admin_2'] = $detail_product->admin_2*$send_iso['jml_tagihan'];
						$up['product_biaya_admin_3'] = $detail_product->admin_3*$send_iso['jml_tagihan'];
						$up['product_potongan'] = $detail_product->potongan*$send_iso['jml_tagihan'];
						$b_admin = ($detail_product->margin+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3-$detail_product->potongan)*$send_iso['jml_tagihan'];
						$up['product_amount'] = $amount+$detail_voucher->amount-($b_admin);

						/*$update = DB::table('trans_pln')
							->where('id',$save)
							->update($up);*/
							
						if(!empty($id_voucher)) {
							$uv['used'] = 'No';

							$up_voucher = DB::table('trans_voucher_child')
								->where('id',$id_voucher_child)
								->where('id_agen',$id_agen)
								->update($uv);
						}

						$update = DB::statement('exec updateTransPlnPascabayarIsoError ?,?,?,?,?,?,?,?,?,?,?,?', array_values($up));

						$header_msg = $send_iso['header_msg'];

						$response['api_status']  = ($header_msg == 'Yes' ? '2' : '4');
				    	$response['api_message'] = ($header_msg == 'Yes' ?Esta::show_error('PLN Postpaid',$send_iso['39'],$no_meter) : ($send_iso['39'] == '96' ? 'Transaksi sedang diproses, Mohon hubungi customer service' : 'Transaksi Gagal'));
				    	$response['id_transaksi']  = 0;
				    	$response['type_dialog']  = 'Informasi';

				    	$response['pln_psc_jml_tagihan'] = $send_iso['pln_psc_jml_tagihan'].' Bulan';
						$response['pln_psc_tarif'] = str_replace(' ', '', $send_iso['pln_psc_tarif']);
						$response['pln_psc_daya'] = $send_iso['pln_psc_daya'];
						$response['pln_psc_no_ref'] = $send_iso['pln_psc_no_ref'];
						$response['pln_psc_jml_tunggakan'] = ($send_iso['pln_psc_jml_tunggakan'] >= 1 ? ltrim($send_iso['pln_psc_jml_tunggakan'],0) : 0);
						$response['stand_meter'] = str_replace(',', '', $send_iso['pln_psc_lwbp_sebelum'].' - '.$send_iso['pln_psc_lwbp_sesudah']);

						$response['pln_psc_blth'] = $send_iso['pln_psc_blth'];
						$response['info'] = str_replace('###', '"', $send_iso['62']);
						$tgl_lunas = $send_iso['pln_psc_tgl_lunas'].$send_iso['pln_psc_jam_lunas'];
						$response['pln_psc_tgl_lunas'] = substr($tgl_lunas,6,2).'/'.substr($tgl_lunas,4,2).'/'.substr($tgl_lunas,2,2).' '.substr($tgl_lunas,8,2).':'.substr($tgl_lunas,10,1).substr($tgl_lunas,11,1);
						$response['pln_psc_jam_lunas'] = $send_iso['pln_psc_jam_lunas'];
						$response['nominal_voucher'] = 'Rp '.number_format($detail_voucher->amount,0,',','.');

						$response['val_rp_transaksi'] = $send_iso['rp_transaksi'];
						$response['val_pln_psc_biaya_tagihan_pln'] = $send_iso['pln_psc_biaya_tagihan_pln'];
						$response['val_pln_psc_biaya_admin'] = ($sv['product_biaya_admin_edn']+$sv['product_biaya_admin_1']+$sv['product_biaya_admin_2']+$sv['product_biaya_admin_3']+$sv['product_margin']-$sv['product_potongan'])*$send_iso['jml_tagihan'];
						$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
						$response['val_total_bayar'] = $response['val_rp_transaksi']+$response['val_pln_psc_biaya_admin'];
						//$response['val_total_pembayaran'] = $response['val_total_bayar']-$response['val_komisi'];

						$val_total_pembayaran = $send_iso['rp_transaksi']+$response['val_pln_psc_biaya_admin']-$response['val_komisi']-$detail_voucher->amount;
						$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

						$response['pln_psc_id_pel'] = $send_iso['pln_psc_id_pel'];
						$response['pln_psc_nama'] = $send_iso['pln_psc_nama'];
						$response['rp_transaksi'] = 'Rp '.number_format($send_iso['rp_transaksi'],0,',','.');
						$response['pln_psc_biaya_tagihan_pln'] = 'Rp '.number_format($send_iso['pln_psc_biaya_tagihan_pln'],0,',','.');
						$response['pln_psc_biaya_admin'] = 'Rp '.number_format($response['val_pln_psc_biaya_admin'],0,',','.');
						//$total_pembayaran = 'Rp '.number_format($response['val_total_bayar']-$response['val_komisi']-$detail_voucher->amount,0,',','.');
						$response['total_pembayaran'] = 'Rp '.number_format(($response['val_total_pembayaran'] <= 0 ? 0 : $response['val_total_pembayaran']),0,',','.');
						$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
						$response['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');

						$up2['id'] = $save;
						$up2['jpa_ref'] = $send_iso['pln_psc_no_ref'];
						$up2['struk_info'] = $response['info'];
						$up2['struk_jml_tunggakan'] = $response['pln_psc_jml_tunggakan'];
						

						$up2['struk_nama_pelanggan'] = $response['pln_psc_nama'];
						$up2['struk_tarif'] = $response['pln_psc_tarif'];
						$up2['struk_daya'] = $response['pln_psc_daya'];
						$up2['struk_admin_bank'] = $response['val_pln_psc_biaya_admin'];
						$up2['struk_total_pembayaran'] = $response['val_total_pembayaran'];
						$up2['struk_tgl_lunas'] = $response['pln_psc_tgl_lunas'];
						$up2['struk_id_pelanggan'] = $response['pln_psc_id_pel'];
						$up2['struk_blth'] = $response['pln_psc_blth'];
						$up2['struk_stand_meter'] = $response['stand_meter'];
						$up2['struk_rp_tag_pln'] = $response['val_rp_transaksi'];
						$up2['struk_total_bayar'] = $response['val_total_bayar'];
						$up2['struk_total_lbr_tagihan'] = $send_iso['pln_psc_jml_tagihan'];

						/*$update2 = DB::table('trans_pln')
							->where('id',$save)
							->update($up2);*/
						$update2 = DB::statement('exec updateTransPLNStruk ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', array_values($up2));

				    	return $response;
				    	exit();
					}
				}

				$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_pln');
				if($send_sms_transaksi == 'Yes') {
					$msg = CRUDBooster::getsetting('sukses_transaksi_pln');
					$msg = str_replace('[no_hp]', $no_hp, $msg);
					$msg = str_replace('[nama]', $detail_agen->nama, $msg);
					$msg = str_replace('[komisi]', $sv['komisi'], $msg);
					Esta::send_sms($no_hp, $msg);
				}

				if(!empty($id_voucher)) {
					$uv['used'] = 'Yes';

					$up_voucher = DB::table('trans_voucher_child')
						->where('id',$id_voucher_child)
						->where('id_agen',$id_agen)
						->update($uv);
				}

				/*if($send_iso['39'] == '97') {
					$id_log = $send_iso['id_log'];
					$detail_log = DB::table('log_jatelindo_bit')
						->where('id',$id_log)
						->first();
					$detail_inq = DB::table('log_jatelindo_bit')
						->where('id',$detail_log->id_transaksi)
						->where('status','Inquiry')
						->where('jenis','res')
						->first();

					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi PLN berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['id_transaksi']  = $save;
			    	$response['pln_psc_jml_tagihan'] = $detail_inq->pln_psc_jml_tagihan.' Bulan';
					$response['pln_psc_tarif'] = str_replace(' ', '', $detail_inq->pln_psc_tarif);
					$response['pln_psc_daya'] = $detail_inq->pln_psc_daya;
					$response['pln_psc_no_ref'] = $detail_log->pln_psc_trx_id;
					$response['pln_psc_jml_tunggakan'] = ($detail_inq->pln_psc_jml_tunggakan >= 1 ? ltrim($detail_inq->pln_psc_jml_tunggakan,0) : 0);
					$response['stand_meter'] = str_replace(',', '', $detail_inq->pln_psc_lwbp_sebelum.' - '.$detail_inq->pln_psc_lwbp_sesudah);

					$response['pln_psc_blth'] = $detail_inq->pln_psc_blth;
					$response['info'] = str_replace('###', '"', $detail_inq->bit62);
					$tgl_lunas = '-';
					$response['pln_psc_tgl_lunas'] = date( "d/m/y H:i", strtotime($detail_log->created_at));
					$response['nominal_voucher'] = 'Rp '.number_format($detail_voucher->amount,0,',','.');

					$response['val_rp_transaksi'] = $detail_inq->rp_transaksi;
					$response['val_pln_psc_biaya_tagihan_pln'] = $detail_inq->pln_psc_biaya_tagihan_pln;
					$response['val_pln_psc_biaya_admin'] = $sv['product_biaya_admin_edn']+$sv['product_biaya_admin_1']+$sv['product_biaya_admin_2']+$sv['product_biaya_admin_3']+$sv['product_margin']-$sv['product_potongan'];
					$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					$val_total_pembayaran = $detail_inq->rp_transaksi+$response['val_pln_psc_biaya_admin']-$response['val_komisi']-$detail_voucher->amount;
					$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);
					$response['val_total_bayar'] = $response['val_rp_transaksi']+$response['val_pln_psc_biaya_admin'];

					$response['pln_psc_id_pel'] = $detail_inq->pln_psc_id_pel;
					$response['pln_psc_nama'] = $detail_inq->pln_psc_nama;
					$response['rp_transaksi'] = 'Rp '.number_format($detail_inq->rp_transaksi,0,',','.');
					$response['pln_psc_biaya_tagihan_pln'] = 'Rp '.number_format($detail_inq->pln_psc_biaya_tagihan_pln,0,',','.');
					$response['pln_psc_biaya_admin'] = 'Rp '.number_format($sv['product_biaya_admin_edn'],0,',','.');
					$response['total_pembayaran'] = 'Rp '.number_format($response['val_total_pembayaran'],0,',','.');
					$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
					$response['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');

					$up2['jpa_ref'] = $detail_log->pln_psc_trx_id;
					$up2['struk_info'] = $response['info'];
					$up2['struk_jml_tunggakan'] = $response['pln_psc_jml_tunggakan'];

				} else {*/
					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi PLN berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['id_transaksi']  = $save;
			    	$response['pln_psc_jml_tagihan'] = $send_iso['pln_psc_jml_tagihan'].' Bulan';
					$response['pln_psc_tarif'] = str_replace(' ', '', $send_iso['pln_psc_tarif']);
					$response['pln_psc_daya'] = $send_iso['pln_psc_daya'];
					$response['pln_psc_no_ref'] = $send_iso['pln_psc_no_ref'];
					$response['pln_psc_jml_tunggakan'] = ($send_iso['pln_psc_jml_tunggakan'] >= 1 ? ltrim($send_iso['pln_psc_jml_tunggakan'],0) : 0);
					$response['stand_meter'] = str_replace(',', '', $send_iso['pln_psc_lwbp_sebelum'].' - '.$send_iso['pln_psc_lwbp_sesudah']);

					$response['pln_psc_blth'] = $send_iso['pln_psc_blth'];
					$response['info'] = str_replace('###', '"', $send_iso['62']);
					$tgl_lunas = $send_iso['pln_psc_tgl_lunas'].$send_iso['pln_psc_jam_lunas'];
					$response['pln_psc_tgl_lunas'] = substr($tgl_lunas,6,2).'/'.substr($tgl_lunas,4,2).'/'.substr($tgl_lunas,2,2).' '.substr($tgl_lunas,8,2).':'.substr($tgl_lunas,10,1).substr($tgl_lunas,11,1);
					$response['pln_psc_jam_lunas'] = $send_iso['pln_psc_jam_lunas'];
					$response['nominal_voucher'] = 'Rp '.number_format($detail_voucher->amount,0,',','.');

					$response['val_rp_transaksi'] = $send_iso['rp_transaksi'];
					$response['val_pln_psc_biaya_tagihan_pln'] = $send_iso['pln_psc_biaya_tagihan_pln'];
					$response['val_pln_psc_biaya_admin'] = ($sv['product_biaya_admin_edn']+$sv['product_biaya_admin_1']+$sv['product_biaya_admin_2']+$sv['product_biaya_admin_3']+$sv['product_margin']-$sv['product_potongan'])*$send_iso['jml_tagihan'];
					$response['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					$response['val_total_bayar'] = $response['val_rp_transaksi']+$response['val_pln_psc_biaya_admin'];
					//$response['val_total_pembayaran'] = $response['val_total_bayar']-$response['val_komisi'];

					$val_total_pembayaran = $send_iso['rp_transaksi']+$response['val_pln_psc_biaya_admin']-$response['val_komisi']-$detail_voucher->amount;
					$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

					$response['pln_psc_id_pel'] = $send_iso['pln_psc_id_pel'];
					$response['pln_psc_nama'] = $send_iso['pln_psc_nama'];
					$response['rp_transaksi'] = 'Rp '.number_format($send_iso['rp_transaksi'],0,',','.');
					$response['pln_psc_biaya_tagihan_pln'] = 'Rp '.number_format($send_iso['pln_psc_biaya_tagihan_pln'],0,',','.');
					$response['pln_psc_biaya_admin'] = 'Rp '.number_format($response['val_pln_psc_biaya_admin'],0,',','.');
					//$total_pembayaran = 'Rp '.number_format($response['val_total_bayar']-$response['val_komisi']-$detail_voucher->amount,0,',','.');
					$response['total_pembayaran'] = 'Rp '.number_format(($response['val_total_pembayaran'] <= 0 ? 0 : $response['val_total_pembayaran']),0,',','.');
					$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
					$response['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');

					$jpa_ref = $send_iso['pln_psc_no_ref'];
					$struk_info = $response['info'];
					$status = 'Clear';
					$struk_jml_tunggakan = $response['pln_psc_jml_tunggakan'];
				//}

				$struk_nama_pelanggan = $response['pln_psc_nama'];
				$struk_tarif = $response['pln_psc_tarif'];
				$struk_daya = $response['pln_psc_daya'];
				$struk_admin_bank = $response['val_pln_psc_biaya_admin'];
				$struk_total_pembayaran = $response['val_total_pembayaran'];
				$struk_tgl_lunas = $response['pln_psc_tgl_lunas'];
				$struk_id_pelanggan = $response['pln_psc_id_pel'];
				$struk_blth = $response['pln_psc_blth'];
				$struk_stand_meter = $response['stand_meter'];
				$struk_rp_tag_pln = $response['val_rp_transaksi'];
				$struk_total_bayar = $response['val_total_bayar'];
				$struk_total_lbr_tagihan = $send_iso['pln_psc_jml_tagihan'];
				$product_margin = $detail_product->margin*$send_iso['jml_tagihan'];
				$product_biaya_admin_edn = $detail_product->admin_edn*$send_iso['jml_tagihan'];
				$product_biaya_admin_1 = $detail_product->admin_1*$send_iso['jml_tagihan'];
				$product_biaya_admin_2 = $detail_product->admin_2*$send_iso['jml_tagihan'];
				$product_biaya_admin_3 = $detail_product->admin_3*$send_iso['jml_tagihan'];
				$product_potongan = $detail_product->potongan*$send_iso['jml_tagihan'];
				$b_admin = ($detail_product->margin+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3-$detail_product->potongan)*$send_iso['jml_tagihan'];
				$product_amount = $amount+$detail_voucher->amount-($b_admin);

				/*$update2 = DB::table('trans_pln')
					->where('id',$save)
					->update($up2);*/
				$update2 = DB::statement('exec updateTransPLNStrukSukses ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
							array($save,$jpa_ref,$struk_info,$status,$struk_jml_tunggakan,$struk_nama_pelanggan,$struk_tarif,$struk_daya,$struk_admin_bank,$struk_total_pembayaran,$struk_tgl_lunas,$struk_id_pelanggan,$struk_blth,$struk_stand_meter,$struk_rp_tag_pln,$struk_total_bayar,$struk_total_lbr_tagihan,$product_margin,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_admin_3,$product_potongan,$product_amount));

				/*email*/

				$view     = view('struk/struk_pln_postpaid',$response)->render();
				$filename = "Struk-PLN-Postpaid-".$response['pln_psc_id_pel'];
				$pdf      = App::make('dompdf.wrapper');

				$path = storage_path('app/uploads/'.$filename.'.pdf');

				$pdf->loadHTML($view);
				$pdf->setPaper('A4','landscape');
				$output = $pdf->output();

				file_put_contents($path, $output);

				$attachments = [$path];
				$email = $detail_agen->email;
				if(!empty($email) && $detail_agen->notif_email != 'No') {
				    //Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pln_postpaid','attachments'=>$attachments]);
					try{
					  Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pln_postpaid','attachments'=>$attachments]);
				    }catch(\Exception $e){               	
				    }
				}

		return $response;
	}
	
}