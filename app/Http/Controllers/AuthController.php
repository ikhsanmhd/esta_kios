<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\CLient;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
//use App\Agen;
use DB;
use CRUDBooster;


class AuthController extends ApiController
{
    public function tokenAuth(Request $request){
    	$http = new Client();
        $nohp = $request->no_hp;
        $password = $request->password;
        //dd(url(''));
    	try{
    		$response = $http->post(url('').'/oauth/token', [
    			'form_params' => [
    				'grant_type' => 'password',
    				'client_id' => config('app.oauth.client_id'),
    				'client_secret' => config('app.oauth.secret'),
    				'username' => $nohp,
    				'password' => $password,
    				'scope' => '',
    			],
    		]);
    	}catch(GuzzleException $e){
			//dd($e);
    		return $this->respondWithError('Terdapat kesalahan dalam mengirimkan data : '.$e);
    	}

    	$response_obj = json_decode((string) $response->getBody(), true);
        //dd($response_obj);

    	if(isset($response_obj['error'])){
    		return $this->responWithError('Data yang dimasukkan tidak sesuai');
    	}

        $data['access_token'] = $response_obj['access_token'];
        $data['refresh_token'] = $response_obj['refresh_token'];
        $expiry_date = Carbon::now()->addSecond($response_obj['expires_in'], 'second')->format('Y-m-d H:i:s');

        $data['expiry_date'] = $expiry_date;

		//$agen_id=Request::get('id_agen');
        //$agen = Auth::user();
		$agen = DB::table('agen')->where('no_hp',$nohp)->whereNull('deleted_at')->first();
        $agen_id = $agen->id;
		$data['agen_id'] = $agen_id;
		$data['npwp'] = $agen->npwp;
    	$checkagen = DB::select('exec CheckAgenKios ?',array($agen_id));  
        $getAgen = DB::select('exec getAgenById ?', array($agen_id))[0];
        $getVerify = DB::table('agen_verify_premium')->where('id_agen',$agen_id);
		
        $getAgenVerify = DB::table('agen_verify_premium')->where('id_agen',$agen_id)->first();
        if($getAgen->status_agen == 'Premium'){
            $is_premium = 1;
        }elseif($getVerify->exists()){
            if($getAgenVerify->foto_ktp_status == 'Rejected' && $getAgenVerify->foto_rekening_status == 'Rejected' && $getAgenVerify->foto_selfie_status == 'Rejected'){
                $is_premium = 3;
            }else{
                $is_premium = 2;
            }
        }else{
            $is_premium = 0;
        }
        $getLimitCart = DB::table('mst_duedate')->where('parameter','=','limit_keep_data_on_cart')->first();
		$shareProduct = CRUDBooster::getsetting('share_product');
		$shareTransactionKios = CRUDBooster::getsetting('Share_Transaction_Kios');
    	if($checkagen[0]->status == 1){
            $data['is_agen'] = 1;
            $data['nama_kios'] = $checkagen[0]->nama_kios;
            $data['kode_kios'] = $checkagen[0]->agenkios_code;
			$data['is_premium'] = $is_premium;
            $data['cart_autoremove_day'] = $getLimitCart->nilai_limit;
			$data['share_product_description'] = $shareProduct;
			$data['share_transaction_pos_description'] = $shareTransactionKios;
            //return $this->respondWithDataAndMessage($data, "Agen Kios");
    	}elseif($checkagen[0]->status == 2) {
            $data['is_agen'] = 2;
            $data['nama_kios'] = $checkagen[0]->nama_kios;
            $data['kode_kios'] = $checkagen[0]->agenkios_code;
			$data['is_premium'] = $is_premium;
            $data['cart_autoremove_day'] = $getLimitCart->nilai_limit;
			$data['share_product_description'] = $shareProduct;
			$data['share_transaction_pos_description'] = $shareTransactionKios;
            //return $this->respondWithDataAndMessage($data, "Agen belum menjadi Agen Kios, Mohon menunggu verifikasi");
        }else {
            $data['is_agen'] = 0;
            $data['nama_kios'] = '';
            $data['kode_kios'] = '';
			$data['is_premium'] = $is_premium;
            $data['cart_autoremove_day'] = $getLimitCart->nilai_limit;
			$data['share_product_description'] = $shareProduct;
			$data['share_transaction_pos_description'] = $shareTransactionKios;
            //return $this->respondWithDataAndMessage($data, "Agen Belum terdaftar sebagai Agen Kios");
        }

        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function refreshToken(Request $request){
        $validation = Validator::make($request->all(), [
            'refresh_token' => 'required',
        ]);

        if($validation->fails()){
            return $this->respondWithError("Refresh token tidak boleh kosong");
        }

        $refresh_token = $request->input('refresh_token');

        $http = new Client();

        try{
            $response=$http->post(url('http://192.168.19.6/Live3/API_KIOS/public').'/oauth/token', [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'client_id' => config('app.oauth.client_id'),
                    'client_secret' => config('app.oauth.secret'),
                    'refresh_token' => $refresh_token,
                    'scope' => '',
                ],
            ]);
        }catch(GuzzleException $e){
            return $this->respondWithError("Terdapat kesalahan dalam mengirimkan data atau token yang dikirimkan tidak valid");
        }

        $response_obj = json_decode((string) $response->getBody(),true);
        //dd($response_obj);
        $data['access_token'] = $response_obj['access_token'];
        $data['refresh_token'] = $response_obj['refresh_token'];
        $expiry_date = Carbon::now()->addSecond($response_obj['expires_in'], 'second')->format('Y-m-d H:i:s');
        //dd(Carbon::now()->addSecond($response_obj['expires_in'], 'second')->format('Y-m-d H:i:s'));
        $data['expiry_date'] = $expiry_date;

        return $this->respondWithDataAndMessage($data, "Token berhasil diperbaharui");
    }

    public function details(){
        $agen = Auth::user();
        return $this->respondWithDataAndMessage($agen, "Success");
    }
}
