<?php

namespace App\Http\Controllers;

use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Storage;
use Esta;
use Illuminate\Support\Facades\Auth;
use Hash;

class RegistrasiEstaAPIv2 extends Controller
{
	public function inquiryActivation(){
		$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/inquiryActivation';
		$referenceNumber = Request::get('referenceNumber');
		$idCardNumber = Request::get('idCardNumber');
		$phoneNumber = Request::get('phoneNumber');
		$birthDate = Request::get('birthDate');
		$birthDate = str_replace(' ','-',$birthDate);
		$birthDate = date('Y-m-d' , strtotime($d));
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		$prefixActivation = CRUDBooster::getsetting('prefix_reference_activation');
		$keterangan = 'Inquiry Activation';
		$referenceNumber = DB::select('exec CreateRefNumberSobatku ?,?', array($prefixActivation, $keterangan));
		$request = [
			'referenceNumber' => $referenceNumber[0]->Prefix,
			'idCardNumber' => $idCardNumber,
			'phoneNumber' => $phoneNumber,
			'birthDate' => $birthDate,
			'user' => $user,
			'hashCode' => hash('sha256', $referenceNumber[0]->Prefix.$idCardNumber.$phoneNumber.$birthDate.$user.$hashCodeKey)
		];

		$data_toJson = json_encode( $request );
		$ch = curl_init( $serviceURL );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept:application/json'
			)
		);
		$response = curl_exec( $ch );
		$responseBody = json_decode( $response,true );
		if($responseBody['responseCode'] == 'RA'){
			$responseBody['urlActivate'] = CRUDBooster::getsetting('parent_url_sobatku_api').'/walletPage/activate?phoneNumber='.$phoneNumber.'&sessionId='.$responseBody['sessionId'].'&user='.$user;
			$responseBody['is_active'] = 1;
		}else{
			$responseBody['is_active'] = 0; 
		}
		//dd($responseBody);
		curl_close($ch);
		
		$saveLog = DB::statement('exec CreateLogSobatku ?,?,?,?,?', array($phoneNumber,$referenceNumber[0]->Prefix,$data_toJson,$response,$keterangan));
		return $responseBody;		
	}

	public function postRegistrasi() {

		$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/register';
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		$idCardFile = $_FILES['idCardFile']['tmp_name'];
		$pasPhotoFile = $_FILES['pasPhotoFile']['tmp_name'];
		$name = Request::get('name');
		$email = Request::get('email');
		$phoneNumber = Request::get('mobilePhone');
		$birthDate = Request::get('birthDate');
		$birthDate = str_replace(' ','-',$birthDate);
		$birthDate = date('Y-m-d' , strtotime($birthDate));
		$idCardNumber = Request::get('idCardNumber');
		$motherName = Request::get('motherName');
		$is_active = Request::get('is_active');
		$regid = Request::get('regid');
		$buku_rekening = Request::get('buku_rekening');
		$storage = storage_path("app/uploads/verify_agen/");

		//$referenceNumber = Request::get('referenceNumber');
		/*$jsonRegistration = Request::get('jsonRegistration');
		$jsonRegistration = str_replace("\n","",$jsonRegistration);
		$jsonRegistration = str_replace("\t","",$jsonRegistration);
		$jsonRegistration = json_decode($jsonRegistration,true);
		$name = $jsonRegistration['name'];
		$email = $jsonRegistration['email'];
		$phoneNumber = $jsonRegistration['mobilePhone'];
		$birthDate = $jsonRegistration['birthDate'];
		$idCardNumber = $jsonRegistration['idCardNumber'];
		$motherName = $jsonRegistration['motherName'];
		$referenceNumber = $jsonRegistration['referenceNumber'];*/

		$id_agen = Request::get('id_agen');

		/*if($_FILES['idCardFile']['error'] == 1 || $_FILES['pasPhotoFile']['error'] == 1){
			return $this->respondWithError("Pastikan File yang di upload tidak melebihi ukuran 2 MB");
		}*/		
		
		$temp = DB::table('agen_temp')
			->where('id',$id_agen)
			->first();

		$cek_no_hp = DB::table('agen')
			->where('no_hp',$phoneNumber)
			->first();
			//dd($cek_no_hp);
		if(!empty($cek_no_hp->id) && $cek_no_hp->is_existing != 1) {
			$response['api_status']  = 0;
	        $response['api_message'] = 'No HP sudah terdaftar';
	        $response['type_dialog']  = 'Error';
        	return response()->json($response);
	    }

	 


		$sv['tgl_register'] = $temp->tgl_register;
		$sv['tgl_otp_terkirim'] = $temp->tgl_otp_terkirim;
		$sv['tgl_verifikasi_otp'] = $temp->tgl_verifikasi_otp;
		$sv['kode_otp'] = $temp->kode_otp;
		$sv['no_hp'] = $temp->no_hp;
		$sv['created_at'] = date('Y-m-d H:i:s');

		$sv['nama'] = $name;
		$sv['no_hp'] = $phoneNumber;
		$sv['email'] = $email;
		$sv['photo'] = 'uploads/profile_agen/avatar.jpg';
		$sv['kode_relation_referall'] = Request::get('kode_relation_referall');
		$sv['password'] = Hash::make(Request::get('password'));

		$kode = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
		/*echo $kode;
		exit();*/
		$sv['kode'] = $kode;
		$sv['kode_referall_agen'] = Esta::kode_referall();
		$sv['status_agen'] = 'Basic';
		$sv['status_aktif'] = 'Aktif';
		$sv['status_verifikasi'] = 'Pending';
		$sv['notif_email'] = 'Yes';
		$sv['created_user'] = $sv['nama'];
        $sv['updated_user'] = '';
        $sv['regid'] = $regid;

		if(!empty($sv['kode_relation_referall'])) {
			$check_referall = DB::table('agen')->where('kode_referall_agen',$sv['kode_relation_referall'])->first();
			$setting_ref = DB::table('voucher_referall')->whereNull('deleted_at')->where('aktif','Yes')->first();

			if($check_referall->id >= 1) {
				
			} else {
				$response['api_status']  = 2;
			    $response['api_message'] = 'Kode referal yang anda pakai tidak terdaftar';
			    $response['type_dialog']  = 'Error';
			    return response()->json($response);
			    exit();
			}
		}
		//Save Image to FTP
		$idCardFileFoto = Request::file('idCardFile');
		$file_nameidCardFile = 'Sobatku/idCardFoto-'.time().'-'.$name.".jpg";
		Storage::disk('ftp')->put($file_nameidCardFile, file_get_contents($idCardFileFoto));
		$pasPhotoFileFoto = Request::file('pasPhotoFile');
		$file_namepasPhotoFile = 'Sobatku/pasPhotoFile-'.time().'-'.$name.".jpg";
		Storage::disk('ftp')->put($file_namepasPhotoFile, file_get_contents($pasPhotoFileFoto));
		//dd($idCardFileFoto);

		if($is_active == 0){
			//PROCCESS TO SOBATKU		
			$prefixActivation = CRUDBooster::getsetting('prefix_reference_registrasi');
			$keterangan = 'Registrasion';
			$referenceNumber = DB::select('exec CreateRefNumberSobatku ?,?', array($prefixActivation, $keterangan));
				
			$jsonRegistration = [
				'name' => $name,
				'email' => $email,
				'mobilePhone' => $phoneNumber,
				'birthDate' => $birthDate,
				'idCardNumber' => $idCardNumber,
				'motherName' => $motherName,
				'referenceNumber' => $referenceNumber[0]->Prefix,
				'user' => $user,
				'regid' => $regid,
				'hashCode' => hash('sha256', $phoneNumber.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
			];

			$request = [
				'idCardFile' => curl_file_create($idCardFile, 'image/jpg', $file_nameidCardFile),
				'pasPhotoFile' => curl_file_create($idCardFile, 'image/jpg', $file_namepasPhotoFile),
				'jsonRegistration' => json_encode($jsonRegistration)
			];
			
			//dd($request);

			$ch = curl_init($serviceURL);
			curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $request );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
					'Accept:application/json',
					'Content-Type:multipart/form-data'
				)
			);
			$responsecurl = curl_exec($ch);
			$responseBody = json_decode( $responsecurl,true );
			curl_close($ch);
			$saveLog = DB::statement('exec CreateLogSobatku ?,?,?,?,?', array($phoneNumber,$referenceNumber[0]->Prefix,json_encode( $request ),$responsecurl,$keterangan));

			if($responseBody['responseCode'] != '00'){
				$item['responseSobatku'] = $responseBody;
				$response['api_status']  = 0;
				$response['api_message'] = 'Gagal';
				$response['type_dialog']  = 'Informasi';
				$response['item'] = $item;
				return response()->json($response);
			}
			
			$responseBody['urlCompleteRegister'] = 
				CRUDBooster::getsetting('parent_url_sobatku_api').'/walletPage/register?phoneNumber='.$phoneNumber.'&sessionId='.$responseBody['sessionId'].'&user='.$user;
		}
		//---
		$sv['idCardNumber'] = $idCardNumber;
		$sv['motherName'] = $motherName;
		$sv['pasPhotoFile'] = $file_namepasPhotoFile;
		$sv['idCardFile'] = $file_nameidCardFile;
		$sv['tglpengajuansobatku'] = date('Y-m-d H:i:s');
		$sv['tgl_lahir'] = $birthDate;
		$sv['is_sobatkuregis'] = 0;

		$update = DB::table('agen')
			//->where('id',$id_agen)
			->insertGetId($sv);

			

    	if($update) {
    		$id_agen = $update;
    		if(!empty($idCardFile)) {
				$file_nameidCardFile = 'Sobatku/idCardFoto-'.time().'-'.$name.".jpg";
		        	$up['foto_ktp'] = $file_nameidCardFile;
		        	$up['foto_ktp_status'] = 'Submitted';
		        	$up['foto_ktp_note'] = 'Menunggu verifikasi';
		        	$up['foto_ktp_last_submit'] = date('Y-m-d H:i:s');
				
			}

			if(!empty($buku_rekening)) {
				$filename_buku_rekening  = 'buku_rekening-'.date('ymdhis').'-'.$id_agen.".jpg";
		        	$up['foto_rekening'] = $filename_buku_rekening;
		        	$up['foto_rekening_status'] = 'Submitted';
		        	$up['foto_rekening_note'] = 'Menunggu verifikasi';
		        	$up['foto_rekening_last_submit'] = date('Y-m-d H:i:s');
				
			}

			if(!empty($pasPhotoFile)) {
				$pasPhotoFileName  = 'selfie-'.date('ymdhis').'-'.$id_agen.".jpg";
		        	$up['foto_selfie'] = $pasPhotoFileName;
		        	$up['foto_selfie_status'] = 'Submitted';
		        	$up['foto_selfie_note'] = 'Menunggu verifikasi';
		        	$up['foto_selfie_last_submit'] = date('Y-m-d H:i:s');
				
			}

			$check = DB::table('verify_agen')
		        ->where('id_agen',$id_agen)
		        ->first();

		    $up['id_agen'] = $id_agen;
		    $up['created_user'] = Esta::user($id_agen);
		    $up['updated_user'] = Esta::user($id_agen);
		    $up_agn['updated_at'] = date('Y-m-d H:i:s');
		    if(!empty($check)) {
				$update = DB::table('verify_agen')
					->where('id',$check->id)
					->update($up);	    	
		    } else {
		    	$up['created_at'] = date('Y-m-d H:i:s');
		    	$update = DB::table('verify_agen')
					->insert($up);	
		    }

			/*$up_agn['updated_user'] = Esta::user($id_agen);
		    $up_agen = DB::table('agen')
		    	->where('id',$id_agen)
		    	->update($up_agn);*/


			// if($update){
			// 	Esta::log_money($id_agen,0,date('Y-m-d H:i:s'),'Agen Update Request Premium','Agen Update Request Premium ','','Riwayat Agen','','');
	  //   		$response['api_status']  = 1;
	  //       	$response['api_message'] = 'Kami akan melakukan verifikasi akun anda terlebih dahulu dalam  1x24 jam';
	  //       	$response['type_dialog']  = 'Informasi';
			// } else {
			// 	$response['api_status']  = 0;
		 //        $response['api_message'] = 'Gagal request premium';
		 //        $response['type_dialog']  = 'Error';
			// }
    		//SAVE DATA SOBATKU TO DB
    		// $saveSobatku = DB::select('exec PostRegistrasiSobatku ?,?,?,?,?,?,?,?,?', array($id_agen,$name,$email,$phoneNumber,$birthDate,$idCardNumber,$motherName,$tglpengajuansobatku,$tglverifikasisobatku,$tglaktivasisobatku,$file_nameidCardFile,$file_namepasPhotoFile));
			//dd($saveSobatku);
    		/*kode agen*/
			/*$cek = DB::table('agen')
				->where('kode',$sv['kode'])
				->get();
			$kode1 = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
			$sv2['kode'] = $kode1;
			if(!empty($cek)) {
				$update_lagi = DB::table('agen')
					->where('id',$id_agen)
					->update($sv2);
			}

			$cek2 = DB::table('agen')
				->where('kode',$kode1)
				->get();
			if(!empty($cek2)) {
				$kode2 = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
				$sv3['kode'] = $kode2;
				$update_lagi2 = DB::table('agen')
					->where('id',$id_agen)
					->update($sv3);
			}*/
			/*kode agen*/

    		$banks = DB::table('bank')
    			->where('prefix_va','!=',NULL)
    			->whereNull('deleted_at')
    			->get();
    		foreach($banks as $bank) {
    			$flag_genva = $bank->flag_genva;

    			$c['created_at'] = date('Y-m-d H:i:s');
    			$c['updated_at'] = date('Y-m-d H:i:s');
    			$c['created_user'] = Esta::user($id_agen);
    			$c['updated_user'] = Esta::user($id_agen);
    			$c['id_agen'] = $id_agen;
    			$c['id_bank'] = $bank->id;
    			if($flag_genva == 0) {
	    			$c['no_va'] = $bank->prefix_va.$sv['no_hp'];
	    		} else {
	    			$c['no_va'] = $bank->prefix_va.str_replace(CRUDBooster::getsetting('kode_agen'), '', $kode);
	    		}

    			$in = DB::table('agen_va')
    				->insert($c);
    		}

			$merchants = DB::table('merchant')
    			->whereNull('deleted_at')
    			//->groupby('group')
    			->where('kode','Alfamart')
    			->get();
    		foreach($merchants as $merchant) {
    			$cm['created_at'] = date('Y-m-d H:i:s');
    			$cm['updated_at'] = date('Y-m-d H:i:s');
    			$cm['created_user'] = Esta::user($id_agen);
    			$cm['updated_user'] = Esta::user($id_agen);
    			$cm['id_agen'] = $id_agen;
    			$cm['merchant'] = $merchant->group;
	    		$cm['no_va'] = $merchant->prefix_va.$sv['no_hp'];

    			$in = DB::table('agen_va')
    				->insert($cm);
    		}    		

    		Esta::log_money($id_agen,0,date('Y-m-d H:i:s'),'Agen Registrasi','Agen Registrasi','','Riwayat Agen','','');

    		if($check_referall->id >= 1) {
	    		$detail_voucher_referall = DB::table('voucher_referall')->whereNull('deleted_at')->first();
	    		$voucher_referall_aktif = $detail_voucher_referall->aktif;
	    		if($voucher_referall_aktif == 'Yes') {
		    		$voucher_pemakai = $detail_voucher_referall->referall_pemakai;
		    		$voucher_dipakai = $detail_voucher_referall->referall_dipakai;

					$detail_voucher_pemakai = DB::table('voucher')->where('id',$voucher_pemakai)->first();
					$detail_voucher_dipakai = DB::table('voucher')->where('id',$voucher_dipakai)->first();

					$save_child['created_at']       = date('Y-m-d H:i:s');
					$save_child['created_user'] = Esta::user($id_agen);
					$save_child['updated_user'] = Esta::user($id_agen);
					$save_child['id_agen']          = $check_referall->id;
					$save_child['id_voucher']       = $voucher_dipakai;
					$save_child['product']          = $detail_voucher_dipakai->product;
					$save_child['voucher_nama']          = $detail_voucher_dipakai->nama;
					$save_child['voucher_expired']          = $detail_voucher_dipakai->expired_date;
					$save_child['voucher_amount']          = $detail_voucher_dipakai->amount;
					$save_child['voucher_image']          = $detail_voucher_dipakai->image;
					$save_child['voucher_product']          = $detail_voucher_dipakai->product;
					$save_child['voucher_description']          = $detail_voucher_dipakai->description;
					$save_child['id_trans_voucher'] = '';
					$save_child['used']             = 'No';
					DB::table('trans_voucher_child')->insert($save_child);

					$save_notif['created_at'] = $detail_voucher_dipakai->expired_date;
					$save_notif['created_user'] = Esta::user($id_agen);
					$save_notif['updated_user'] = Esta::user($id_agen);
					$save_notif['title'] = $detail_voucher_dipakai->nama;
					$save_notif['description'] = $detail_voucher_dipakai->description;
					$save_notif['description_short'] = $detail_voucher_dipakai->description;
					$save_notif['image'] = $detail_voucher_dipakai->image;
					$save_notif['id_agen'] = $check_referall->id;
					$save_notif['read'] = 'No';
					$save_notif['flag'] = 'Voucher';
					$save_notif['syarat_ketentuan'] = $detail_voucher_dipakai->syarat_ketentuan;
					$save_notif['id_voucher'] = $voucher_dipakai;
					DB::table('notification')->insert($save_notif);

					$save_child2['created_at']       = date('Y-m-d H:i:s');
					$save_child2['created_user'] = Esta::user($id_agen);
					$save_child2['updated_user'] = Esta::user($id_agen);
					$save_child2['id_agen']          = $id_agen;
					$save_child2['id_voucher']       = $voucher_pemakai;
					$save_child2['product']          = $detail_voucher_pemakai->product;
					$save_child2['voucher_nama']          = $detail_voucher_pemakai->nama;
					$save_child2['voucher_expired']          = $detail_voucher_pemakai->expired_date;
					$save_child2['voucher_amount']          = $detail_voucher_pemakai->amount;
					$save_child2['voucher_image']          = $detail_voucher_pemakai->image;
					$save_child2['voucher_product']          = $detail_voucher_pemakai->product;
					$save_child2['voucher_description']          = $detail_voucher_pemakai->description;
					$save_child2['id_trans_voucher'] = '';
					$save_child2['used']             = 'No';
					DB::table('trans_voucher_child')->insert($save_child2);

					$save_notif2['created_at'] = $detail_voucher_pemakai->expired_date;
					$save_notif2['created_user'] = Esta::user($id_agen);
					$save_notif2['updated_user'] = Esta::user($id_agen);
					$save_notif2['title'] = $detail_voucher_pemakai->nama;
					$save_notif2['description'] = $detail_voucher_pemakai->description;
					$save_notif2['description_short'] = $detail_voucher_pemakai->description;
					$save_notif2['image'] = $detail_voucher_pemakai->image;
					$save_notif2['id_agen'] = $id_agen;
					$save_notif2['read'] = 'No';
					$save_notif2['flag'] = 'Voucher';
					$save_notif2['syarat_ketentuan'] = $detail_voucher_pemakai->syarat_ketentuan;
					$save_notif2['id_voucher'] = $voucher_pemakai;
					DB::table('notification')->insert($save_notif2);

					if($check_referall->regid != NULL) {
						$datafcm['title'] = CRUDBooster::getsetting('notification_voucher_header');
						$datafcm['content'] = CRUDBooster::getsetting('notification_voucher_referall');
						$regid[] = $check_referall->regid;
						Esta::sendFCM($regid,$datafcm);
					}
				}
			}

    		$item['nama'] = $name;
    		$item['no_hp'] = $phoneNumber;
    		$item['kode_referall_agen'] = $sv['kode_referall_agen'];
    		$item['status_agen'] = $sv['status_agen'];
    		$item['photo'] = env('BACKEND_URL').'uploads/profile_agen/avatar.jpg';
    		$item['responseSobatku'] = $responseBody;
    		$item['id_agen'] = $id_agen;

    		$response['api_status']  = 1;
	        $response['api_message'] = 'Registrasi berhasil';
	        $response['type_dialog']  = 'Informasi';
	        $response['item'] = $item;
    	} else {
	    	$response['api_status']  = 0;
	        $response['api_message'] = 'Gagal';
	        $response['type_dialog']  = 'Error';
	    }

        return response()->json($response);
	}

	public function infoTarikTunai(){
		$agen = Auth::user();
        $id_agen = $agen->id;
		$item['informasi_tarik_tunai'] = CRUDBooster::getsetting('informasi_tarik_tunai');
		// $item['info_bpjs'] = (CRUDBooster::getsetting('info_bpjs') != '' ? CRUDBooster::getsetting('info_bpjs') : '');
		// $item['info_tarik_tunai'] = (CRUDBooster::getsetting('info_tarik_tunai') != '' ? str_replace(array('[param1]','[param2]'), array(number_format(CRUDBooster::getsetting('tarik_tunai_tanpa_biaya_admin'),0,',','.'),number_format(CRUDBooster::getsetting('biaya_admin_tarik_tunai'),0,',','.')),CRUDBooster::getsetting('info_tarik_tunai')) : '');

		$response['api_status']  = 1;
	    $response['api_message'] = 'Success';
	    $response['type_dialog']  = 'Informasi';
	 //    $response['item'] = $item;
		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$item['saldo'] = ($detail_agen->saldo <= 0 ? 0 : $detail_agen->saldo);
	 	$response['item'] = $item;

	    return response()->json($response);
	}
}