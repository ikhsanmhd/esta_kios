<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;

use Illuminate\Support\Facades\Auth;

class GetEsta extends ApiController
{

    public function getEsta(){     
    	return $this->respondWithSuccess("Success");
    }
	public function checkConnection(){
		$serviceURL = 'https://172.18.21.3/sobatkuapi/rest/wallet/checkConnection';
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		$request = [
			'user' => $user,
			'hashCode' => hash('sha256', $user.$hashCodeKey)
		];

		$data_toJson = json_encode( $request );
		$ch = curl_init( $serviceURL );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept:application/json'
			)
		);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
    	curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		$response = curl_exec( $ch );
		$err = curl_error($ch);
		//dd($err);
		if ($err) {
			return "cURL Error #:" . $err;
		}
		$responseBody = json_decode( $response,true );
		curl_close($ch);

		return $responseBody;
	}
}