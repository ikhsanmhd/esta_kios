<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Request;
use Route;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;
use Storage;
use Esta;
use App;
use Illuminate\Support\Facades\Auth;

class PulsaAPI extends Controller
{
	public function postPulsaTransaksiPending() {
		$agen = Auth::user();
        $id_agen = $agen->id;

		$arrays = DB::table('trans_pulsa')
			->where('id_agen',$id_agen)
			//->where('deleted_at','!=',NULL)
			->where('status','Pending')
			->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'))
			->orderBy('id','DESC')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$date_tr = Esta::change_date_format($array->created_at,'d M Y');
			$rest['id'] = $array->id;
			$rest['datetime'] = $array->created_at;
			$rest['trans_no'] = $array->trans_no;
			$rest['no_hp'] = $array->no_hp;
			$rest['amount'] = 'Rp '.number_format($array->trans_amount,0,',','.');
			$rest['nama_operator'] = $array->product_kode_biller;
			$rest['nama_product'] = $array->product_nama;
			$rest['id_kode_biller'] = DB::table('pan_kode_biller')->where('nama',$array->product_kode_biller)->first()->kode;
			$rest['status'] = ($date_tr == date('d M Y') ? 'Pending' : 'Expired');
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postVoucherPulsa() {
		$agen = Auth::user();
        $id_agen = $agen->id;

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen));

		/*$arrays = DB::table('trans_voucher_child')
			->whereIN('product',['Pulsa','All'])
			->where('id_agen',$id_agen)
			->where('used','No')
			->where('voucher_expired','>=',date('Y-m-d H:i:s'))
			->orderBy('id','desc')
			->get();*/

		$arrays = DB::select('exec getVoucherWithParams ?,?', array($id_agen,'Pulsa'));

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->voucher_nama;
			$rest['amount'] = $array->voucher_amount;
			$rest['masa_berlaku'] = date("Y-m-d H:i:s", strtotime($array->voucher_expired));
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postProductPulsa() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$id_kode_biller = Request::get('id_kode_biller');
		$id_nama_operator = Request::get('id_nama_operator');
		$status = Request::get('status');
		$id_kode_layanan = Request::get('id_kode_layanan'); /*1 prabayar, 2 pasca bayar*/

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen))[0];

		/*$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();*/

		$arrays = DB::table('pan_product')
			->where('id_kode_biller',$id_kode_biller)
			->where('id_kode_layanan',$id_kode_layanan)
			->where('id_prefix_nama_operator',$id_nama_operator)
			->orderBy('amount','asc')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->product_name;
			$rest['amount'] = Esta::amount_product($array->id);
			$rest['komisi'] = ($detail_agen->status_agen == 'Basic' ? $array->komisi_basic : $array->komisi_premium);
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postPrefixOperator() {
		$status = Request::input('status');

		$prefixs = DB::table('prefix_operator')
			->join('pan_kode_biller', 'prefix_operator.id_kode_biller', '=', 'pan_kode_biller.id');
			if($status == 'Prabayar') {
				$prefixs = $prefixs->where('pan_kode_biller.nama','LIKE','%Prepaid%');
			} elseif($status == 'Pascabayar') {
				$prefixs = $prefixs->where('pan_kode_biller.nama','LIKE','%Postpaid%');
			}
			$prefixs = $prefixs
			->whereNull('prefix_operator.deleted_at')
			->get();

		$rest_prefix = array();
	  	foreach($prefixs as $prefix) {
			$rest['id'] = $prefix->id;
			$rest['id_kode_biller'] = $prefix->id_kode_biller;
			$rest['operator'] = DB::table('pan_kode_biller')->where('id',$prefix->id_kode_biller)->first()->nama;
			$rest['prefix'] = $prefix->prefix;
			$rest['id_nama_operator'] = ($prefix->id_nama_operator >= 1 ? $prefix->id_nama_operator : 0);
			array_push($rest_prefix, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_prefix;

	  	return response()->json($response);
	}

	public function postCheckTagihanPascabayarPulsa() {
		$no_hp = Request::get('no_hp');
		$agen = Auth::user();
        $id_agen = $agen->id;
		$id_product = Request::get('id_product');

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen))[0];

		$detail_product = DB::table('pan_product')
			->where('id',$id_product)
			->first();

		/*send iso*/
		$send_iso = Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0200','Inquiry',0);

		if($send_iso['39'] == '00') {
			//'Rp '.number_format(ltrim($send_iso['jml_tagihan'],0));
			$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
			$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
			$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
			$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
			$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
			$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
			$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
			$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
			$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
			$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
			$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
			$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
			$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
			$rest['val_total_bayar'] = $send_iso['total_bayar'];
			$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
			$rest['id_transaksi'] = $send_iso['id_log'];

			$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
			$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
			$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
			$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
			$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
			$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');


			/*$rest['val_jml_tagihan'] = str_replace(' ', '',ltrim($send_iso['jml_tagihan'],0));
			$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$rest['val_biaya_admin'] = str_replace(' ', '',ltrim($send_iso['pls_psc_biaya_admin']+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3+$detail_product->margin-$detail_product->potongan,0));
			$rest['biaya_admin'] = 'Rp '.number_format(ltrim($rest['val_biaya_admin'],0));
			$rest['jml_tagihan'] = $rest['val_jml_tagihan'];
			$rest['val_total_bayar'] = $rest['val_jml_tagihan']+$rest['val_biaya_admin'];
			$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
			$rest['blth'] = $send_iso['blth'];
			$rest['jml_bulan'] = $send_iso['pls_psc_total_bill'].' BLN';
			$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
			$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');*/

			$response['api_status']  = 1;
	    	$response['api_message'] = 'Berhasil';
	    	$response['type_dialog'] = 'Informasi';
	    	$response['item']  = $rest;
		} else {
			$rest['jml_tagihan'] = 0;
			$rest['komisi'] = 0;

			$response['api_status']  = 0;
	    	$response['api_message'] = Esta::show_error('Pulsa',$send_iso['39'],'0');;
	    	$response['type_dialog']  = 'Error';
	    	$response['item']  = $rest;
		}
		return response()->json($response);
	}

	public function postPulsaPrabayarSubmit() {
		/*$agen = Auth::user();
        $id_agen = $agen->id;*/
        $id_agen = Request::get('id_agen');
        $request_transaksi = Request::get('request_transaksi');
        $transactionId = Request::get('transactionId');
        $request_transaksi = json_decode( $request_transaksi,true );
		$regid = $request_transaksi['regid'];
		$token = $request_transaksi['token'];
		//$id_agen = Request::get('id_agen');
		$kode_paket = $request_transaksi['kode_paket'];
		$cek_regid = Esta::cek_regid($id_agen,$regid);

        $getlog_transaction = DB::table('log_sobatku_payment')
            ->where('transactionId',$transactionId)
            ->where('keterangan','Request Payment')
            ->first();
        $oldRefNum = $getlog_transaction->ref_number;
        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

        $ut['is_used'] = 1;
        DB::table('temp_trans_payment')
			->where('transactionId',$transactionId)
			->update($ut);

		/*if($cek_regid == 0) {
			$response['api_status']  = 2;
		    $response['api_message'] = 'Terjadi gangguan pada koneksi internet atau Server. Silahkan coba beberapa saat lagi';//'Akun Anda terdeteksi login di HP lain, Silahkan logout dan login kembali untuk melanjutkan transaksi.';
		    $response['type_dialog']  = 'Error';
		    //Esta::add_fraud($id_agen);
		    return response()->json($response);
		}*/

		$no_hp = $request_transaksi['no_hp'];
		$id_product = $request_transaksi['id_product'];
		$pin =$request_transaksi['pin'];
		$id_voucher_child = $request_transaksi['id_voucher'];
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		if($pin != 'estakiosprosespaket'){

			/*if(hash::check($pin, $detail_agen->password)){
				
			} else {
				$response['api_status']  = 2;
		    	$response['api_message'] = 'PIN anda salah';
		    	$response['type_dialog']  = 'Error';
		    	return response()->json($response);
		    	exit();
			}*/
		}

			$detail_product = DB::table('pan_product')
				->where('id',$id_product)
				->first();

			$detail_voucher = DB::table('voucher')
				->where('id',$id_voucher)
				->first();

			$kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));

			$trans_amount = Esta::amount_product_trans($id_product,$id_agen,$id_voucher)-($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			if($trans_amount <= 0) {
				$trans_amount = 0;
			} else {
				$trans_amount = $trans_amount;
			}
			
			$status_match_sobatku = 'Waiting Rekon';
			/*if($saldo_sebelum >= $trans_amount) {*/
				try{
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'PULSA PRABAYAR';
					$currency = 'IDR';
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					$trans_amount = $trans_amount;
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Pending';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $detail_product->amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$id_voucher = $detail_voucher->id;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					//$kode_paket = $kode_paket;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pulsa');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					/*$saveTransPulsa = DB::select('exec postTransPulsaPrabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
											,array($created_at,$updated_at,$created_user,$updated_user
											,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$komisi,$trans_amount,$id_agen,$status,$rekon_amount
											,$status_match,$no_hp,$id_product,$agen_nama,$agen_no_hp,$agen_email,$agen_kode,$agen_level,$agen_referall
											,$agen_referall_relation,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir
											,$agen_jenis_kelamin,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov
											,$agen_kab,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan
											,$product_kode_biller,$product_komisi_basic,$product_komisi_premium,$product_amount,$product_margin
											,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3
											,$product_potongan,$voucher_nama,$voucher_expired_date,$voucher_amount,$id_voucher,$voucher_tagline
											,$voucher_description,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
					$save = $saveTransPulsa[0]->id;*/
				} catch(\Exception $e) {
					//return $e->getMessage();
					//sleep(1);
					$kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));
					/*$response['api_status']  = 0;
			    	$response['api_message'] = 'Transaksi gagal, silahkan coba lagi';
			    	$response['type_dialog']  = 'Error';
			    	$response['id_transaksi']  = 0;
			    	return response()->json($response);
					exit();*/
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'PULSA PRABAYAR';
					$currency = 'IDR';
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					$trans_amount = $trans_amount;
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Pending';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $detail_product->amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$id_voucher = $detail_voucher->id;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					//$kode_paket = $kode_paket;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pulsa');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					/*$saveTransPulsa = DB::select('exec postTransPulsaPrabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
											,array($created_at,$updated_at,$created_user,$updated_user
											,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$komisi,$trans_amount,$id_agen,$status,$rekon_amount
											,$status_match,$no_hp,$id_product,$agen_nama,$agen_no_hp,$agen_email,$agen_kode,$agen_level,$agen_referall
											,$agen_referall_relation,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir
											,$agen_jenis_kelamin,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov
											,$agen_kab,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan
											,$product_kode_biller,$product_komisi_basic,$product_komisi_premium,$product_amount,$product_margin
											,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3
											,$product_potongan,$voucher_nama,$voucher_expired_date,$voucher_amount,$id_voucher,$voucher_tagline
											,$voucher_description,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
					$save = $saveTransPulsa[0]->id;*/
				}

				/*Get Id Table Trans*/
				$save = DB::table('trans_pulsa')
							->where('transactionId_sobatku',$transactionId)
							->first()->id;

				if($save) {
					/*potong saldo*/
					//sleep(1);
					$tr_out = Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi Pulsa Prabayar','Transaksi Pulsa Prabayar '.$detail_product->product_name,'Out','Transaksi','trans_pulsa',$save,$saldo_sebelum);
					$cb_in = Esta::log_money($id_agen,$komisi,date('Y-m-d H:i:s'),'Cashback Transaksi Pulsa Prabayar','Cashback Transaksi Pulsa Prabayar '.$detail_product->product_name,'In','Komisi','trans_pulsa',$save,$saldo_sebelum-$trans_amount);
					
					
						/*send iso*/
						$send_iso = Esta::send_iso_pulsa_prabayar($id_product,$no_hp,'0200','Purchase',$save);
						//dd($send_iso);

					if($send_iso['39'] == 'Pending' || $send_iso['39'] == '06' || $send_iso['39'] == '09' || $send_iso['39'] == '18' || $send_iso['39'] == '22') {
						$up3['status'] = 'Pending';
						$up3['error_code'] = $send_iso['39'];
						if($send_iso['no_ref'] > 0) {
							$up3['jpa_ref'] = $send_iso['no_ref'];
						}

						$update = DB::table('trans_pulsa')
							->where('id',$save)
							->update($up3);

						$response['api_status']  = 3;
				    	$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
				    	$response['type_dialog']  = 'Informasi';
				    	$response['id_transaksi']  = 0;

				    	$response['sn']  = $send_iso['sn'];
				    	$response['rc']  = $send_iso['39'];
				    	$response['pls_psc_no_ref_switching']  = $send_iso['no_ref'];
				    	$response['tanggal']  = $send_iso['tanggal'];
				    	$response['id_transaksi']  = $save;
				    	$response['product_amount']  = Esta::amount_product($id_product);
				    	$response['product_amount_rp']  = 'Rp '.number_format(Esta::amount_product($id_product),0,',','.');
				    	$response['komisi'] = $komisi;
				    	$response['product_nama'] = $product_nama;
				    	$response['no_hp'] = $no_hp;
				    	$response['voucher_amount'] = $detail_voucher->amount;
				    	$total_pembayaran = $response['product_amount']-$response['komisi']-$response['voucher_amount'];
				    	$response['total_pembayaran'] = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);

				    	$struk_tgl_lunas = $response['tanggal'];
						$struk_sn = $response['sn'];
						$jpa_ref = $send_iso['no_ref'];
						$struk_total_bayar = $response['product_amount'];
						$struk_total_pembayaran = $response['total_pembayaran'];

						$update = DB::statement('exec updateTransPulsaPrabayarStruk ?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_sn,$jpa_ref,$struk_total_bayar,$struk_total_pembayaran));

						return $response;
						exit();
					}
					
					if($send_iso['39'] != '00') { /*jika respon error*/
						/*kembalikan saldo*/
						if($send_iso['39'] != '18' || $send_iso['39'] != '06' || $send_iso['39'] != '09') {
							/*reversal sobatku*/
							$start_time = time();
							//$refrn = 0;
							while(true){
								sleep(10);
								//$refrn++;
								$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
								//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
								$user = CRUDBooster::getsetting('user_sobatku_api');
								$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

								$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
								$keterangan = 'Reversal Payment';
								$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
								$referenceNumber = $referenceNumber[0]->Prefix;
								$request = [
									'oldRefNum' => $oldRefNum,
									'referenceNumber' => $referenceNumber,
									'user' => $user,
									'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
								];

								$data_toJson = json_encode( $request );
								$ch = curl_init( $serviceURL );
								curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
								curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
								curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
								curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
										'Content-Type: application/json',
										'Accept:application/json'
									)
								);
								curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
								curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
								$responsereversal = curl_exec( $ch );
								$responseerror = curl_errno($ch);
								if ($responseerror == 28){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
									break;
								}else {
									$responseBody = json_decode( $responsereversal, true);													

									if($responseBody['responseCode'] == '00' ){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										/*kembalikan saldo*/
										Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi Pulsa Prabayar','Return Transaksi Pulsa Prabayar '.$detail_product->product_name,'In','Transaksi','trans_pulsa',$save,$saldo_sebelum-$trans_amount);
										Esta::log_money($id_agen,$komisi,date('Y-m-d H:i:s'),'Cashback Transaksi Pulsa Prabayar','Return Cashback Transaksi Pulsa Prabayar '.$detail_product->product_name,'Out','Komisi','trans_pulsa',$save,$saldo_sebelum);
										break;
									}elseif($responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3' || $responseBody['responseCode'] == 'P1'){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										break;
									}elseif((time() - $start_time) > 60){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										break;
									}else{
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
									}
								}
								
								curl_close($ch);	
							}
							$up['return_saldo'] = 'Yes';
						}
						$up['status'] = 'Error';
						//$up['status_match'] = 'Error';
						$up['error_code'] = $send_iso['39'];
						if($send_iso['no_ref'] > 0) {
							$up['jpa_ref'] = $send_iso['no_ref'];
						}

						$update = DB::table('trans_pulsa')
							->where('id',$save)
							->update($up);

						$response['api_status']  = 2;
				    	$response['api_message'] = Esta::show_error('Pulsa',$send_iso['39'],'0');
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;

				    	$response['sn']  = $send_iso['sn'];
				    	$response['rc']  = $send_iso['39'];
				    	$response['pls_psc_no_ref_switching']  = $send_iso['no_ref'];
				    	$response['tanggal']  = $send_iso['tanggal'];
				    	$response['id_transaksi']  = $save;
				    	$response['product_amount']  = Esta::amount_product($id_product);
				    	$response['product_amount_rp']  = 'Rp '.number_format(Esta::amount_product($id_product),0,',','.');
				    	$response['komisi'] = $komisi;
				    	$response['product_nama'] = $product_nama;
				    	$response['no_hp'] = $no_hp;
				    	$response['voucher_amount'] = $detail_voucher->amount;
				    	$total_pembayaran = $response['product_amount']-$response['komisi']-$response['voucher_amount'];
				    	$response['total_pembayaran'] = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);

				    	$struk_tgl_lunas = $response['tanggal'];
				    	$struk_sn = $response['sn'];
					    $jpa_ref = $send_iso['no_ref'];
				    	$struk_total_bayar = $response['product_amount'];
				    	$struk_total_pembayaran = $response['total_pembayaran'];

						$update = DB::statement('exec updateTransPulsaPrabayarStruk ?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_sn,$jpa_ref,$struk_total_bayar,$struk_total_pembayaran));

				    	return $response;
				    	exit();
					}

					$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_pulsa');
					if($send_sms_transaksi == 'Yes') {
						$msg = CRUDBooster::getsetting('sukses_transaksi_pulsa');
						$msg = str_replace('[no_hp]', $no_hp, $msg);
						$msg = str_replace('[nama]', $detail_agen->nama, $msg);
						$msg = str_replace('[komisi]', $komisi, $msg);
						Esta::send_sms($no_hp, $msg);
					}

					if(!empty($id_voucher)) {
						$uv['used'] = 'Yes';

						$up_voucher = DB::table('trans_voucher_child')
							->where('id',$id_voucher_child)
							->where('id_agen',$id_agen)
							->update($uv);
					}
					//exit();

					$up2['status'] = 'Clear';

					$update = DB::table('trans_pulsa')
						->where('id',$save)
						->update($up2);

					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi pulsa berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['sn']  = $send_iso['sn'];
			    	$response['rc']  = $send_iso['39'];
			    	$response['pls_psc_no_ref_switching']  = $send_iso['no_ref'];
			    	$response['tanggal']  = $send_iso['tanggal'];
			    	$response['id_transaksi']  = $save;
			    	$response['product_amount']  = Esta::amount_product($id_product);
			    	$response['product_amount_rp']  = 'Rp '.number_format(Esta::amount_product($id_product),0,',','.');
			    	$response['komisi'] = $komisi;
			    	$response['product_nama'] = $product_nama;
			    	$response['no_hp'] = $no_hp;
			    	$response['voucher_amount'] = $detail_voucher->amount;
			    	$total_pembayaran = $response['product_amount']-$response['komisi']-$response['voucher_amount'];
			    	$response['total_pembayaran'] = ($total_pembayaran <= 0 ? 0 : $total_pembayaran);
			    	$up_struk['error_code'] = $response['rc'];

			    	$struk_tgl_lunas = $response['tanggal'];
			    	$struk_sn = $response['sn'];
				    $jpa_ref = $send_iso['no_ref'];
			    	$struk_total_bayar = $response['product_amount'];
			    	$struk_total_pembayaran = $response['total_pembayaran'];

					$update = DB::statement('exec updateTransPulsaPrabayarStruk ?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_sn,$jpa_ref,$struk_total_bayar,$struk_total_pembayaran));

					$view     = view('struk/struk_pulsa_prepaid',$response)->render();
					$filename = "Struk-Pulsa-Prepaid-".$no_hp;
					$pdf      = App::make('dompdf.wrapper');

					$path = storage_path('app/uploads/'.$filename.'.pdf');

					$pdf->loadHTML($view);
					$pdf->setPaper('A4','landscape');
					$output = $pdf->output();

					file_put_contents($path, $output);

					$attachments = [$path];
					$email = $detail_agen->email;
					if(!empty($email) && $detail_agen->notif_email != 'No') {
                        //if($pin != 'estakiosprosespaket') {
                            Esta::kirimemail(['to' => $email, 'data' => $response, 'template' => 'email_transaksi_pulsa_prepaid', 'attachments' => $attachments]);
                        //}
					}
				} else {
					/*reversal sobatku*/
					$start_time = time();
					//$refrn = 0;
					while(true){
						sleep(10);
						//$refrn++;
						$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
								//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
						$user = CRUDBooster::getsetting('user_sobatku_api');
						$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

						$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
						$keterangan = 'Reversal Payment';
						$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
						$referenceNumber = $referenceNumber[0]->Prefix;
						$request = [
							'oldRefNum' => $oldRefNum,
							'referenceNumber' => $referenceNumber,
							'user' => $user,
							'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
						];

						$data_toJson = json_encode( $request );
						$ch = curl_init( $serviceURL );
						curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
						curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
						curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
								'Content-Type: application/json',
								'Accept:application/json'
							)
						);
						curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
						curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
						$responsereversal = curl_exec( $ch );
						$responseerror = curl_errno($ch);
						if ($responseerror == 28){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
							break;
						}else {
							$responseBody = json_decode( $responsereversal, true);													

							if($responseBody['responseCode'] == '00' || $responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3'){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
								break;
							}elseif((time() - $start_time) > 60){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
								break;
							}else{
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
							}
						}
						
						curl_close($ch);	
					}
					$response['api_status']  = 0;
			    	$response['api_message'] = 'Transaksi pulsa gagal';
			    	$response['type_dialog']  = 'Error';
			    	$response['id_transaksi']  = 0;
				}
			

		return $response;
	}

	public function postPulsaPascabayarSubmit() {
		$id_agen = Request::get('id_agen');
        $request_transaksi = Request::get('request_transaksi');
        $transactionId = Request::get('transactionId');
        $request_transaksi = json_decode( $request_transaksi,true );
		/*$agen = Auth::user();
        $id_agen = $agen->id;*/
		$regid = $request_transaksi['regid'];
		$token = $request_transaksi['token'];
		$cek_regid = Esta::cek_regid($id_agen,$regid);

        $getlog_transaction = DB::table('log_sobatku_payment')
            ->where('transactionId',$transactionId)
            ->where('keterangan','Request Payment')
            ->first();
        $oldRefNum = $getlog_transaction->ref_number;
        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

        $ut['is_used'] = 1;
        DB::table('temp_trans_payment')
					->where('transactionId',$transactionId)
					->update($ut);

		/*if($cek_regid == 0) {
			$response['api_status']  = 2;
		    $response['api_message'] = 'Terjadi gangguan pada koneksi internet atau Server. Silahkan coba beberapa saat lagi';//'Akun Anda terdeteksi login di HP lain, Silahkan logout dan login kembali untuk melanjutkan transaksi.';
		    $response['type_dialog']  = 'Error';
		    //Esta::add_fraud($id_agen);
		    return response()->json($response);
		}*/

		$no_hp = $request_transaksi['no_hp'];
		$id_product = $request_transaksi['id_product'];
		//$id_voucher = Request::get('id_voucher');
		$pin = $request_transaksi['pin'];
		$amount = $request_transaksi['amount'];
		$id_transaksi = $request_transaksi['id_transaksi'];

		$id_voucher_child = $request_transaksi['id_voucher'];
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		/*if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}*/

			$detail_product = DB::table('pan_product')
				->where('id',$id_product)
				->first();

			$detail_voucher = DB::table('voucher')
				->where('id',$id_voucher)
				->first();

			$kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));

			$detail_inquiry = DB::table('log_jatelindo_bit')
				->where('id',$id_transaksi)
				->where('type','Pulsa Postpaid')
				->where('status','Inquiry')
				->where('jenis','res')
				->first();

			$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);

			$biaya_admin = $detail_inquiry->pls_psc_biaya_admin;
			if($trans_amount == 0) {
				$trans_amount = 0;
			} else {
				$trans_amount = $trans_amount+$biaya_admin;
			}

			/*echo $trans_amount;
			exit();*/
			$status_match_sobatku = 'Waiting Rekon';
			//if($saldo >= $trans_amount) {
				try{
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'PULSA PASCABAYAR';
					$currency = 'IDR';
					$trans_amount = $amount;
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Pending';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$id_voucher = $detail_voucher->id;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pulsa');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					/*$saveTransPulsa = DB::select('exec postTransPulsaPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								,array($created_at,$updated_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc
									,$currency,$trans_amount,$komisi,$id_agen,$status,$rekon_amount,$status_match,$no_hp,$id_product
									,$agen_nama,$agen_no_hp,$agen_email,$agen_kode,$agen_level,$agen_referall,$agen_referall_relation
									,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin
									,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov,$agen_kab
									,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller
									,$product_komisi_basic,$product_komisi_premium,$product_amount,$product_margin,$product_biaya_admin_edn
									,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan
									,$voucher_nama,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description,$id_voucher
									,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
					$save = $saveTransPulsa[0]->id;*/
				} catch(\Exception $e) {
					//sleep(1);
					$kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'PULSA PASCABAYAR';
					$currency = 'IDR';
					$trans_amount = $amount;
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Pending';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$id_voucher = $detail_voucher->id;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pulsa');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					/*$saveTransPulsa = DB::select('exec postTransPulsaPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								,array($created_at,$updated_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc
									,$currency,$trans_amount,$komisi,$id_agen,$status,$rekon_amount,$status_match,$no_hp,$id_product
									,$agen_nama,$agen_no_hp,$agen_email,$agen_kode,$agen_level,$agen_referall,$agen_referall_relation
									,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin
									,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov,$agen_kab
									,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller
									,$product_komisi_basic,$product_komisi_premium,$product_amount,$product_margin,$product_biaya_admin_edn
									,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan
									,$voucher_nama,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description,$id_voucher
									,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
					$save = $saveTransPulsa[0]->id;*/
				}

				/*Get Id Table Trans*/
				$save = DB::table('trans_pulsa')
							->where('transactionId_sobatku',$transactionId)
							->first()->id;

				if($save) {
					//sleep(1);
					/*potong saldo*/
					$tr_out = Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi Pulsa Pascabayar','Transaksi Pulsa Pascabayar '.$detail_product->product_name,'Out','Transaksi','trans_pulsa',$save,$saldo_sebelum);
					$cb_in = Esta::log_money($id_agen,$komisi,date('Y-m-d H:i:s'),'Komisi Transaksi Pulsa Pascabayar','Komisi Transaksi Pulsa Pascabayar '.$detail_product->product_name,'In','Komisi','trans_pulsa',$save,$saldo_sebelum-$trans_amount);


					/*send iso*/
					$send_iso = Esta::send_iso_pulsa_pascabayar($id_product,$no_hp,'0200','Payment',$id_transaksi);

					if($send_iso['39'] != '97') {
						if($send_iso['status'] == 'Reversal' || $send_iso['status'] == 'Reversal Repeat' || $send_iso['status'] == 'Reversal Repeat 2' || $send_iso['status'] == 'Reversal Repeat 1') {

							if($send_iso['39'] == '00' || $send_iso['39'] == '94') { /*jika respon error*/
								/*reversal sobatku*/
								$start_time = time();
								//$refrn = 0;
								while(true){
									
									//$refrn++;
									$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
									//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
									$user = CRUDBooster::getsetting('user_sobatku_api');
									$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

									$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
									$keterangan = 'Reversal Payment';
									$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
									$referenceNumber = $referenceNumber[0]->Prefix;
									$request = [
										'oldRefNum' => $oldRefNum,
										'referenceNumber' => $referenceNumber,
										'user' => $user,
										'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
									];

									$data_toJson = json_encode( $request );
									$ch = curl_init( $serviceURL );
									curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
									curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
									curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
									curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
											'Content-Type: application/json',
											'Accept:application/json'
										)
									);
									curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
									curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
									$responsereversal = curl_exec( $ch );
									$responseerror = curl_errno($ch);
									if ($responseerror == 28){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
										break;
									}else {
										$responseBody = json_decode( $responsereversal, true);													

										if($responseBody['responseCode'] == '00' ){
											$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
											
											/*kembalikan saldo*/
											/*kembalikan saldo*/
											Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi Pulsa Pascabayar','Return Transaksi Pulsa Pascabayar '.$detail_product->product_name,'In','Transaksi','trans_pulsa',$save,$saldo_sebelum-$trans_amount);
											Esta::log_money($id_agen,$komisi,date('Y-m-d H:i:s'),'Komisi Transaksi Pulsa Pascabayar','Return Komisi Transaksi Pulsa Pascabayar '.$detail_product->product_name,'Out','Komisi','trans_pulsa',$save,$saldo_sebelum);
											break;
										}elseif($responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3' || $responseBody['responseCode'] == 'P1'){
											$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
											break;
										}elseif((time() - $start_time) > 60){
											$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
											break;
										}else{
											$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										}
									}
									
									curl_close($ch);	
									sleep(10);
								}
								
								$up['return_saldo'] = 'Yes';
								$up['status'] = 'Error';
								//$up['status_match'] = 'Error';
								$up['flag_reversal'] = 'Reversal Sukses';
								$up['error_code'] = $send_iso['39'];
								$up['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

								$update = DB::table('trans_pulsa')
									->where('id',$save)
									->update($up);

								$header_msg = $send_iso['header_msg'];

								$response['api_status']  = ($header_msg == 'Yes' ? '2' : '4');
						    	$response['api_message'] = ($header_msg == 'Yes' ?Esta::show_error('Pulsa Postpaid',$send_iso['39'],$no_meter) : ($send_iso['39'] == '96' ? 'Transaksi sedang diproses, Mohon hubungi customer service' : 'Transaksi Gagal, Mohon coba kembali'));
						    	$response['id_transaksi']  = 0;
						    	$response['type_dialog']  = ($header_msg == 'Yes' ? 'Informasi' : 'Error');

						    	$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
								$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
								$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
								$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
								$rest['pls_psc_no_hp'] = $no_hp;
								$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
								$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
								$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
								$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
								$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
								$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
								$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
								$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
								$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
								$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
								$rest['val_total_bayar'] = $send_iso['total_bayar'];
								$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
								$rest['id_transaksi'] = $send_iso['id_log'];

								$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
								$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
								$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
								$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
								$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
								$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
								$rest['biaya_admin'] = 'Rp '.number_format($rest['val_pls_psc_biaya_admin'],0,',','.');


								/*$response['api_status']  = 1;
						    	$response['api_message'] = 'Transaksi pulsa berhasil';
						    	$response['type_dialog']  = 'Informasi';
						    	$response['id_transaksi']  = $id_transaksi;*/
						    	$d = substr($send_iso['pls_psc_waktu_lunas'], 6,2);
						    	$m = substr($send_iso['pls_psc_waktu_lunas'], 4,2);
						    	$y = substr($send_iso['pls_psc_waktu_lunas'], 0,4);
						    	$h = substr($send_iso['pls_psc_waktu_lunas'], 8,2);
						    	$i = substr($send_iso['pls_psc_waktu_lunas'], 10,2);
						    	//$response['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;
						    	$rest['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;

						    	$struk_tgl_lunas = $rest['pls_psc_waktu_lunas'];
						    	$struk_nama_pelanggan = $rest['nama_pelanggan'];
						    	$struk_tag_bln_1 = $rest['pls_psc_blth_1'];
						    	$struk_tag_bln_2 = $rest['pls_psc_blth_2'];
						    	$struk_tag_bln_3 = $rest['pls_psc_blth_3'];
						    	$struk_tag_amount_1 = $rest['val_pls_psc_jml_tagihan_1'];
						    	$struk_tag_amount_2 = $rest['val_pls_psc_jml_tagihan_2'];
						    	$struk_tag_amount_3 = $rest['val_pls_psc_jml_tagihan_3'];
						    	$struk_total_tagihan = $rest['val_total_tagihan'];
						    	$struk_biaya_admin = $rest['val_pls_psc_biaya_admin'];
						    	$struk_total_bayar = $rest['val_total_bayar'];
						    	$struk_total_pembayaran = $rest['val_total_pembayaran'];
						    	$struk_total_bill_tagihan = $rest['pls_psc_total_bill'];
						    	$jpa_ref = $send_iso['pls_psc_no_ref_switching'];
						    	$trans_amount = $struk_total_pembayaran;

								$update = DB::statement('exec updateTransPulsaPascabayarStruk ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_nama_pelanggan,$struk_tag_bln_1,$struk_tag_bln_2,$struk_tag_bln_3,$struk_tag_amount_1,$struk_tag_amount_2,$struk_tag_amount_3,$struk_total_tagihan,$struk_biaya_admin,$struk_total_bayar,$struk_total_pembayaran,$struk_total_bill_tagihan,$jpa_ref,$trans_amount));

						    	return $response;
						    	exit();
							} else {
								$up3['status'] = 'Pending';
								$up3['stan'] = $send_iso['11'];
								$up3['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

								$update = DB::table('trans_pulsa')
									->where('id',$save)
									->update($up3);

								$response['api_status']  = '2';
						    	$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
						    	$response['id_transaksi']  = 0;
						    	$response['type_dialog']  = ($header_msg == 'Yes' ? 'Informasi' : 'Error');

						    	$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
								$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
								$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
								$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
								$rest['pls_psc_no_hp'] = $no_hp;
								$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
								$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
								$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
								$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
								$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
								$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
								$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
								$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
								$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
								$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
								$rest['val_total_bayar'] = $send_iso['total_bayar'];
								$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
								$rest['id_transaksi'] = $send_iso['id_log'];

								$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
								$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
								$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
								$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
								$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
								$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
								$rest['biaya_admin'] = 'Rp '.number_format($rest['val_pls_psc_biaya_admin'],0,',','.');


								/*$response['api_status']  = 1;
						    	$response['api_message'] = 'Transaksi pulsa berhasil';
						    	$response['type_dialog']  = 'Informasi';
						    	$response['id_transaksi']  = $id_transaksi;*/
						    	$d = substr($send_iso['pls_psc_waktu_lunas'], 6,2);
						    	$m = substr($send_iso['pls_psc_waktu_lunas'], 4,2);
						    	$y = substr($send_iso['pls_psc_waktu_lunas'], 0,4);
						    	$h = substr($send_iso['pls_psc_waktu_lunas'], 8,2);
						    	$i = substr($send_iso['pls_psc_waktu_lunas'], 10,2);
						    	//$response['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;
						    	$rest['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;

						    	$struk_tgl_lunas = $rest['pls_psc_waktu_lunas'];
						    	$struk_nama_pelanggan = $rest['nama_pelanggan'];
						    	$struk_tag_bln_1 = $rest['pls_psc_blth_1'];
						    	$struk_tag_bln_2 = $rest['pls_psc_blth_2'];
						    	$struk_tag_bln_3 = $rest['pls_psc_blth_3'];
						    	$struk_tag_amount_1 = $rest['val_pls_psc_jml_tagihan_1'];
						    	$struk_tag_amount_2 = $rest['val_pls_psc_jml_tagihan_2'];
						    	$struk_tag_amount_3 = $rest['val_pls_psc_jml_tagihan_3'];
						    	$struk_total_tagihan = $rest['val_total_tagihan'];
						    	$struk_biaya_admin = $rest['val_pls_psc_biaya_admin'];
						    	$struk_total_bayar = $rest['val_total_bayar'];
						    	$struk_total_pembayaran = $rest['val_total_pembayaran'];
						    	$struk_total_bill_tagihan = $rest['pls_psc_total_bill'];
						    	$jpa_ref = $send_iso['pls_psc_no_ref_switching'];
						    	$trans_amount = $struk_total_pembayaran;

								$update = DB::statement('exec updateTransPulsaPascabayarStruk ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_nama_pelanggan,$struk_tag_bln_1,$struk_tag_bln_2,$struk_tag_bln_3,$struk_tag_amount_1,$struk_tag_amount_2,$struk_tag_amount_3,$struk_total_tagihan,$struk_biaya_admin,$struk_total_bayar,$struk_total_pembayaran,$struk_total_bill_tagihan,$jpa_ref,$trans_amount));

						    	return $response;
							}
						}
					}


					if($send_iso['39'] != '97') {
						if($send_iso['39'] != '00' || $send_iso['status'] == 'Reversal' || $send_iso['status'] == 'Reversal Repeat' || $send_iso['status'] == 'Reversal Repeat 2' || $send_iso['status'] == 'Reversal Repeat 1') {


							if($send_iso['39'] == '00' || $send_iso['39'] == '94') { /*jika respon error*/
								$header_msg = $send_iso['header_msg'];
								/*reversal sobatku*/
								$start_time = time();
								//$refrn = 0;
								while(true){
									
									//$refrn++;
									$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
									//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
									$user = CRUDBooster::getsetting('user_sobatku_api');
									$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

									$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
									$keterangan = 'Reversal Payment';
									$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
									$referenceNumber = $referenceNumber[0]->Prefix;
									$request = [
										'oldRefNum' => $oldRefNum,
										'referenceNumber' => $referenceNumber,
										'user' => $user,
										'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
									];

									$data_toJson = json_encode( $request );
									$ch = curl_init( $serviceURL );
									curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
									curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
									curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
									curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
											'Content-Type: application/json',
											'Accept:application/json'
										)
									);
									curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
									curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
									$responsereversal = curl_exec( $ch );
									$responseerror = curl_errno($ch);
									if ($responseerror == 28){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
										break;
									}else {
										$responseBody = json_decode( $responsereversal, true);													

										if($responseBody['responseCode'] == '00' ){
											$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
											/*kembalikan saldo*/
											Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi Pulsa Pascabayar','Return Transaksi Pulsa Pascabayar '.$detail_product->product_name,'In','Transaksi','trans_pulsa',$save,$saldo_sebelum-$trans_amount);
											Esta::log_money($id_agen,$komisi,date('Y-m-d H:i:s'),'Komisi Transaksi Pulsa Pascabayar','Return Komisi Transaksi Pulsa Pascabayar '.$detail_product->product_name,'Out','Komisi','trans_pulsa',$save,$saldo_sebelum);
											break;
										}elseif($responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3' || $responseBody['responseCode'] == 'P1'){
											$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
											break;
										}elseif((time() - $start_time) > 60){
											$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
											break;
										}else{
											$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										}
									}
									
									curl_close($ch);
									sleep(10);
								}
								/*kembalikan saldo*/
								
								$up['return_saldo'] = 'Yes';
								$up['status'] = 'Error';
								//$up['status_match'] = 'Error';
								$up['flag_reversal'] = 'Reversal Sukses';
								$up['error_code'] = $send_iso['39'];
								$up['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

								$update = DB::table('trans_pulsa')
									->where('id',$save)
									->update($up);


								$response['api_status']  = ($header_msg == 'Yes' ? '2' : '4');
						    	$response['api_message'] = ($header_msg == 'Yes' ?Esta::show_error('Pulsa Postpaid',$send_iso['39'],$no_meter) : ($send_iso['39'] == '96' ? 'Transaksi sedang diproses, Mohon hubungi customer service' : 'Transaksi Gagal'));
						    	$response['id_transaksi']  = 0;
						    	$response['type_dialog']  = ($header_msg == 'Yes' ? 'Informasi' : 'Error');

						    	$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
								$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
								$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
								$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
								$rest['pls_psc_no_hp'] = $no_hp;
								$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
								$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
								$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
								$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
								$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
								$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
								$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
								$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
								$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
								$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
								$rest['val_total_bayar'] = $send_iso['total_bayar'];
								$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
								$rest['id_transaksi'] = $send_iso['id_log'];

								$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
								$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
								$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
								$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
								$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
								$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
								$rest['biaya_admin'] = 'Rp '.number_format($rest['val_pls_psc_biaya_admin'],0,',','.');


								/*$response['api_status']  = 1;
						    	$response['api_message'] = 'Transaksi pulsa berhasil';
						    	$response['type_dialog']  = 'Informasi';
						    	$response['id_transaksi']  = $id_transaksi;*/
						    	$d = substr($send_iso['pls_psc_waktu_lunas'], 6,2);
						    	$m = substr($send_iso['pls_psc_waktu_lunas'], 4,2);
						    	$y = substr($send_iso['pls_psc_waktu_lunas'], 0,4);
						    	$h = substr($send_iso['pls_psc_waktu_lunas'], 8,2);
						    	$i = substr($send_iso['pls_psc_waktu_lunas'], 10,2);
						    	//$response['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;
						    	$rest['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;

						    	$struk_tgl_lunas = $rest['pls_psc_waktu_lunas'];
						    	$struk_nama_pelanggan = $rest['nama_pelanggan'];
						    	$struk_tag_bln_1 = $rest['pls_psc_blth_1'];
						    	$struk_tag_bln_2 = $rest['pls_psc_blth_2'];
						    	$struk_tag_bln_3 = $rest['pls_psc_blth_3'];
						    	$struk_tag_amount_1 = $rest['val_pls_psc_jml_tagihan_1'];
						    	$struk_tag_amount_2 = $rest['val_pls_psc_jml_tagihan_2'];
						    	$struk_tag_amount_3 = $rest['val_pls_psc_jml_tagihan_3'];
						    	$struk_total_tagihan = $rest['val_total_tagihan'];
						    	$struk_biaya_admin = $rest['val_pls_psc_biaya_admin'];
						    	$struk_total_bayar = $rest['val_total_bayar'];
						    	$struk_total_pembayaran = $rest['val_total_pembayaran'];
						    	$struk_total_bill_tagihan = $rest['pls_psc_total_bill'];
						    	$jpa_ref = $send_iso['pls_psc_no_ref_switching'];
						    	$trans_amount = $struk_total_pembayaran;

								$update = DB::statement('exec updateTransPulsaPascabayarStruk ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_nama_pelanggan,$struk_tag_bln_1,$struk_tag_bln_2,$struk_tag_bln_3,$struk_tag_amount_1,$struk_tag_amount_2,$struk_tag_amount_3,$struk_total_tagihan,$struk_biaya_admin,$struk_total_bayar,$struk_total_pembayaran,$struk_total_bill_tagihan,$jpa_ref,$trans_amount));

						    	return $response;
						    	exit();
							}
						}
					}

					if($send_iso['39'] == 'Pending' || $send_iso['39'] == '06' || $send_iso['39'] == '09' || $send_iso['39'] == '18' || $send_iso['39'] == '22') {
						$up3['status'] = 'Pending';
						$up3['stan'] = $send_iso['11'];
						$up3['jpa_ref'] = $send_iso['pls_psc_no_ref_switching'];

						$update = DB::table('trans_pulsa')
							->where('id',$save)
							->update($up3);

						$response['api_status']  = 3;
				    	$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
				    	$response['type_dialog']  = 'Informasi';
				    	$response['id_transaksi']  = 0;

				    	$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
						$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
						$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
						$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
						$rest['pls_psc_no_hp'] = $no_hp;
						$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
						$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
						$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
						$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
						$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
						$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
						$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
						$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
						$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
						$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
						$rest['val_total_bayar'] = $send_iso['total_bayar'];
						$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
						$rest['id_transaksi'] = $send_iso['id_log'];

						$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
						$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
						$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
						$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
						$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
						$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
						$rest['biaya_admin'] = 'Rp '.number_format($rest['val_pls_psc_biaya_admin'],0,',','.');


						/*$response['api_status']  = 1;
				    	$response['api_message'] = 'Transaksi pulsa berhasil';
				    	$response['type_dialog']  = 'Informasi';
				    	$response['id_transaksi']  = $id_transaksi;*/
				    	$d = substr($send_iso['pls_psc_waktu_lunas'], 6,2);
				    	$m = substr($send_iso['pls_psc_waktu_lunas'], 4,2);
				    	$y = substr($send_iso['pls_psc_waktu_lunas'], 0,4);
				    	$h = substr($send_iso['pls_psc_waktu_lunas'], 8,2);
				    	$i = substr($send_iso['pls_psc_waktu_lunas'], 10,2);
				    	//$response['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;
				    	$rest['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;

				    	$struk_tgl_lunas = $rest['pls_psc_waktu_lunas'];
				    	$struk_nama_pelanggan = $rest['nama_pelanggan'];
				    	$struk_tag_bln_1 = $rest['pls_psc_blth_1'];
				    	$struk_tag_bln_2 = $rest['pls_psc_blth_2'];
				    	$struk_tag_bln_3 = $rest['pls_psc_blth_3'];
				    	$struk_tag_amount_1 = $rest['val_pls_psc_jml_tagihan_1'];
				    	$struk_tag_amount_2 = $rest['val_pls_psc_jml_tagihan_2'];
				    	$struk_tag_amount_3 = $rest['val_pls_psc_jml_tagihan_3'];
				    	$struk_total_tagihan = $rest['val_total_tagihan'];
				    	$struk_biaya_admin = $rest['val_pls_psc_biaya_admin'];
				    	$struk_total_bayar = $rest['val_total_bayar'];
				    	$struk_total_pembayaran = $rest['val_total_pembayaran'];
				    	$struk_total_bill_tagihan = $rest['pls_psc_total_bill'];
				    	$jpa_ref = $send_iso['pls_psc_no_ref_switching'];
				    	$trans_amount = $struk_total_pembayaran;

						$update = DB::statement('exec updateTransPulsaPascabayarStruk ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_nama_pelanggan,$struk_tag_bln_1,$struk_tag_bln_2,$struk_tag_bln_3,$struk_tag_amount_1,$struk_tag_amount_2,$struk_tag_amount_3,$struk_total_tagihan,$struk_biaya_admin,$struk_total_bayar,$struk_total_pembayaran,$struk_total_bill_tagihan,$jpa_ref,$trans_amount));

						return $response;
						exit();
					}

					$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_pulsa');
					if($send_sms_transaksi == 'Yes') {
						$msg = CRUDBooster::getsetting('sukses_transaksi_pulsa');
						$msg = str_replace('[no_hp]', $no_hp, $msg);
						$msg = str_replace('[nama]', $detail_agen->nama, $msg);
						$msg = str_replace('[komisi]', $komisi, $msg);
						Esta::send_sms($no_hp, $msg);
					}

					if(!empty($id_voucher)) {
						$uv['used'] = 'Yes';

						$up_voucher = DB::table('trans_voucher_child')
							->where('id',$id_voucher_child)
							->where('id_agen',$id_agen)
							->update($uv);
					}

					/*email*/
					$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
					$rest['nama_pelanggan'] = $send_iso['pls_psc_customer_name'];
					$rest['pls_psc_phone_no'] = $send_iso['pls_psc_phone_no'];
					$rest['pls_psc_no_hp'] = $no_hp;
					$rest['pls_psc_no_ref_switching'] = $send_iso['pls_psc_no_ref_switching'];
					$rest['pls_psc_total_bill'] = $send_iso['pls_psc_total_bill'];
					$rest['val_pls_psc_biaya_admin'] = $send_iso['pls_psc_biaya_admin'];
					$rest['pls_psc_blth_1'] = $send_iso['pls_psc_blth_1'];
					$rest['pls_psc_blth_2'] = ($rest['pls_psc_total_bill'] >= 2 ? $send_iso['pls_psc_blth_2'] : '');
					$rest['pls_psc_blth_3'] = ($rest['pls_psc_total_bill'] >= 3 ? $send_iso['pls_psc_blth_3'] : '');
					$rest['val_pls_psc_jml_tagihan_1'] = $send_iso['pls_psc_jml_tagihan_1'];
					$rest['val_pls_psc_jml_tagihan_2'] = $send_iso['pls_psc_jml_tagihan_2'];
					$rest['val_pls_psc_jml_tagihan_3'] = $send_iso['pls_psc_jml_tagihan_3'];
					$rest['val_total_tagihan'] = $send_iso['total_tagihan'];
					$rest['val_total_bayar'] = $send_iso['total_bayar'];
					$rest['val_total_pembayaran'] = $rest['val_total_bayar']-$rest['val_komisi'];
					$rest['id_transaksi'] = $send_iso['id_log'];

					$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_1'],0,',','.');
					$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_2'],0,',','.');
					$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($rest['val_pls_psc_jml_tagihan_3'],0,',','.');
					$rest['total_tagihan'] = 'Rp '.number_format($rest['val_total_tagihan'],0,',','.');
					$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
					$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');
					$rest['biaya_admin'] = 'Rp '.number_format($rest['val_pls_psc_biaya_admin'],0,',','.');

			    	$up_struk['status'] = 'Clear';
					$update = DB::table('trans_pulsa')
						->where('id',$save)
						->update($up_struk);

					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi pulsa berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['id_transaksi']  = $id_transaksi;
			    	$d = substr($send_iso['pls_psc_waktu_lunas'], 6,2);
			    	$m = substr($send_iso['pls_psc_waktu_lunas'], 4,2);
			    	$y = substr($send_iso['pls_psc_waktu_lunas'], 0,4);
			    	$h = substr($send_iso['pls_psc_waktu_lunas'], 8,2);
			    	$i = substr($send_iso['pls_psc_waktu_lunas'], 10,2);
			    	$response['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;
			    	$rest['pls_psc_waktu_lunas']  = $d.'-'.$m.'-'.$y.' '.$h.':'.$i;

			    	$struk_tgl_lunas = $rest['pls_psc_waktu_lunas'];
			    	$struk_nama_pelanggan = $rest['nama_pelanggan'];
			    	$struk_tag_bln_1 = $rest['pls_psc_blth_1'];
			    	$struk_tag_bln_2 = $rest['pls_psc_blth_2'];
			    	$struk_tag_bln_3 = $rest['pls_psc_blth_3'];
			    	$struk_tag_amount_1 = $rest['val_pls_psc_jml_tagihan_1'];
			    	$struk_tag_amount_2 = $rest['val_pls_psc_jml_tagihan_2'];
			    	$struk_tag_amount_3 = $rest['val_pls_psc_jml_tagihan_3'];
			    	$struk_total_tagihan = $rest['val_total_tagihan'];
			    	$struk_biaya_admin = $rest['val_pls_psc_biaya_admin'];
			    	$struk_total_bayar = $rest['val_total_bayar'];
			    	$struk_total_pembayaran = $rest['val_total_pembayaran'];
			    	$struk_total_bill_tagihan = $rest['pls_psc_total_bill'];
			    	$jpa_ref = $send_iso['pls_psc_no_ref_switching'];
			    	$trans_amount = $struk_total_pembayaran;

					$update = DB::statement('exec updateTransPulsaPascabayarStruk ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_nama_pelanggan,$struk_tag_bln_1,$struk_tag_bln_2,$struk_tag_bln_3,$struk_tag_amount_1,$struk_tag_amount_2,$struk_tag_amount_3,$struk_total_tagihan,$struk_biaya_admin,$struk_total_bayar,$struk_total_pembayaran,$struk_total_bill_tagihan,$jpa_ref,$trans_amount));

					$view     = view('struk/struk_pulsa_postpaid',$rest)->render();
					$filename = "Struk-Pulsa-Postpaid-".$no_hp;
					$pdf      = App::make('dompdf.wrapper');

					$path = storage_path('app/uploads/'.$filename.'.pdf');

					$pdf->loadHTML($view);
					$pdf->setPaper('A4','landscape');
					$output = $pdf->output();

					file_put_contents($path, $output);

					$attachments = [$path];
					$email = $detail_agen->email;
					if(!empty($email)) {
					    Esta::kirimemail(['to'=>$email,'data'=>$rest,'template'=>'email_transaksi_pulsa_postpaid','attachments'=>$attachments]);
					}

					/*email*/
				} else {
					/*reversal sobatku*/
					$start_time = time();
					//$refrn = 0;
					while(true){
						sleep(10);
						//$refrn++;
						$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
						//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
						$user = CRUDBooster::getsetting('user_sobatku_api');
						$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

						$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
						$keterangan = 'Reversal Payment';
						$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
						$referenceNumber = $referenceNumber[0]->Prefix;
						$request = [
							'oldRefNum' => $oldRefNum,
							'referenceNumber' => $referenceNumber,
							'user' => $user,
							'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
						];

						$data_toJson = json_encode( $request );
						$ch = curl_init( $serviceURL );
						curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
						curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
						curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
								'Content-Type: application/json',
								'Accept:application/json'
							)
						);
						curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
						curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
						$responsereversal = curl_exec( $ch );
						$responseerror = curl_errno($ch);
						if ($responseerror == 28){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
							break;
						}else {
							$responseBody = json_decode( $responsereversal, true);													

							if($responseBody['responseCode'] == '00' || $responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3'){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
								break;
							}elseif((time() - $start_time) > 60){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
								break;
							}else{
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
							}
						}
						
						curl_close($ch);	
					}
					$response['api_status']  = 0;
			    	$response['api_message'] = 'Transaksi pulsa gagal';
			    	$response['id_transaksi']  = 0;
				}
			/*} else {
				$is_reversalsuccess = 1;

				while($is_reversalsuccess = 1){
					sleep(5);
					$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
					$user = CRUDBooster::getsetting('user_sobatku_api');
					$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

					$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
					$keterangan = 'Reversal Payment';
					$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));

					$request = [
						'oldRefNum' => $oldRefNum,
						'referenceNumber' => $referenceNumber[0]->Prefix,
						'user' => $user,
						'hashCode' => hash('sha256', $oldRefNum.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
					];

					$data_toJson = json_encode( $request );
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response = curl_exec( $ch );
					$responseerror = curl_errno($ch);
					if ($responseerror == 28){
						$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$request,$response,'Reversal Payment',1));
						$is_reversalsuccess = 0;
					}else {
						$responseBody = json_decode( $response, true);													

						if($responseBody['responseCode'] == '00' ){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$request,$response,'Reversal Payment',0));
							$is_reversalsuccess = 0;
						}else{
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$request,$response,'Reversal Payment',0));
						}
					}
					
					curl_close($ch);	
				}
				$response['api_status']  = 3;
		    	$response['api_message'] = 'Saldo anda tidak mencukupi';
		    	$response['type_dialog']  = 'Error';
		    	$response['id_transaksi']  = 0;
			}*/

		return $response;
	}
	
	
}