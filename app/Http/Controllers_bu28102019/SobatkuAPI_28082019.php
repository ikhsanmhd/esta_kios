<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Storage;
use Esta;
use Illuminate\Support\Facades\Auth;

class SobatkuAPI extends ApiController
{

	public function activationResult(){
		$request = '';
		$phoneNumber = Request::get('phoneNumber');
		$sessionId = Request::get('sessionId');
		$responseCode = Request::get('responseCode');
		$responseDescription = Request::get('responseDescription');
		$hashCode = Request::get('hashCode');
		$response = [
			'phoneNumber' => $phoneNumber,
			'sessionId' => $sessionId,
			'responseCode' => $responseCode,
			'responseDescription' => $responseDescription,
			'hashCode' => $hashCode
		];

		$responseJson = json_encode($response);

		$saveResponse = DB::statement('exec CreateLogSobatku ?,?,?,?,?', array($phoneNumber,'',$request,$responseJson,'Activation Result'));

		return $this->respondWithDataAndMessage($response, "Success");
	}

	public function registrasiSobatku(){
		$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/register';
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		$idCardFile = $_FILES['idCardFile']['tmp_name'];
		$pasPhotoFile = $_FILES['pasPhotoFile']['tmp_name'];
		$jsonRegistration = Request::get('jsonRegistration');
		$jsonRegistration = str_replace("\n","",$jsonRegistration);
		$jsonRegistration = str_replace("\t","",$jsonRegistration);
		$jsonRegistration = json_decode($jsonRegistration,true);
		$name = $jsonRegistration['name'];
		$email = $jsonRegistration['email'];
		$phoneNumber = $jsonRegistration['mobilePhone'];
		$birthDate = $jsonRegistration['birthDate'];
		$idCardNumber = $jsonRegistration['idCardNumber'];
		$motherName = $jsonRegistration['motherName'];
		$referenceNumber = $jsonRegistration['referenceNumber'];

		$jsonRegistration['user'] = $user;
		$jsonRegistration['hashCode'] = hash('sha256', $jsonRegistration['mobilePhone'].$jsonRegistration['referenceNumber'].$user.$hashCodeKey);

		//Save Image to FTP
		$idCardFileFoto = Request::file('idCardFile');
		$file_nameidCardFile = 'Sobatku/idCardFoto-'.time().'-'.$jsonRegistration['name'].".jpg";
		Storage::disk('ftp')->put($file_nameidCardFile, file_get_contents($idCardFileFoto));
		$pasPhotoFileFoto = Request::file('pasPhotoFile');
		$file_namepasPhotoFile = 'Sobatku/pasPhotoFile-'.time().'-'.$jsonRegistration['name'].".jpg";
		Storage::disk('ftp')->put($file_namepasPhotoFile, file_get_contents($pasPhotoFileFoto));
//dd($idCardFileFoto);

		//$saveSobatku = DB::select('exec PostRegistrasiSobatku ?,?,?,?,?,?,?', array($name,$email,$phoneNumber,$birthDate,$idCardNumber,$motherName,$referenceNumber));

		$request = [
			'idCardFile' => curl_file_create($idCardFile, 'image/jpg', 'test.jpg'),
			'pasPhotoFile' => curl_file_create($idCardFile, 'image/jpg', 'test.jpg'),
			'jsonRegistration' => json_encode($jsonRegistration)
		];

		$ch = curl_init($serviceURL);
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $request );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Accept:application/json',
				'Content-Type:multipart/form-data'
			)
		);
		$response = curl_exec($ch);
		$responseBody = json_decode( $response,true );
		if($responseBody['responseCode'] == '00'){
			$responseBody['urlCompleteRegister'] = CRUDBooster::getsetting('parent_url_sobatku_api').'/walletPage/register?phoneNumber='.$phoneNumber.'&sessionId='.$responseBody['sessionId'];
		}
		curl_close($ch);

		return $responseBody;
		
	}

	public function registrasionResult(){
		$request = '';
		$phoneNumber = Request::get('phoneNumber');
		$sessionId = Request::get('sessionId');
		$responseCode = Request::get('responseCode');
		$responseDescription = Request::get('responseDescription');
		$hashCode = Request::get('hashCode');
		$response = [
			'phoneNumber' => $phoneNumber,
			'sessionId' => $sessionId,
			'responseCode' => $responseCode,
			'responseDescription' => $responseDescription,
			'hashCode' => $hashCode
		];

		$responseJson = json_encode($response);

		$saveResponse = DB::statement('exec CreateLogSobatku ?,?,?,?,?', array($phoneNumber,'',$request,$responseJson,'Registration Result'));

		if($response['responseCode'] == 'W1'){
			$getAgen = DB::select('exec getAgenByNohp ?', array($phoneNumber))[0];
			//dd($getAgen);
			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Registrasion Result';
			$save_notif['description'] = 'Waiting For KYC';
			$save_notif['description_short'] = 'Waiting For KYC';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
			}
			
			DB::table('notification')->insert($save_notif);
		}else{
			$getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];
			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Registrasion Result';
			$save_notif['description'] = 'Gagal Registrasi';
			$save_notif['description_short'] = 'Gagal Registrasi';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
			}
			$save_ver['tglpengajuansobatku'] = NULL;
			DB::table('agen')
				->where('id',$getAgen->id)
				->update($save_ver);
			DB::table('notification')->insert($save_notif);
		}

		return $this->respondWithDataAndMessage($response, "Success");
	}
	
	public function verificationManualResult(){
		$request = '';
		$phoneNumber = Request::get('phoneNumber');
		$sessionId = Request::get('sessionId');
		$responseCode = Request::get('responseCode');
		$responseDescription = Request::get('responseDescription');
		$hashCode = Request::get('hashCode');
		$response = [
			'phoneNumber' => $phoneNumber,
			'sessionId' => $sessionId,
			'responseCode' => $responseCode,
			'responseDescription' => $responseDescription,
			'hashCode' => $hashCode
		];

		$responseJson = json_encode($response);

		$saveResponse = DB::statement('exec CreateLogSobatku ?,?,?,?,?', array($phoneNumber,'',$request,$responseJson,'Verification Manual Result'));
		if($response['responseCode'] == '00'){
			$getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];
			//dd($getAgen);
			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Verification';
			$save_notif['description'] = 'Verification Result';
			$save_notif['description_short'] = 'Akun sukses di verifikasi';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
			}
			$save_ver['tglverifikasisobatku'] = date('Y-m-d H:i:s');
			$save_ver['is_existing'] = 0;
			$save_ver['regid'] = NULL;
			DB::table('agen')
				->where('id',$getAgen->id)
				->update($save_ver);
			
			DB::table('notification')->insert($save_notif);
		}
		return $this->respondWithDataAndMessage($response, "Success");
	}

	public function inquiryBalance(){
		$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/accountBalance';
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		$accountNumber = Request::get('accountNumber');
		// $referenceNumber = Request::get('referenceNumber');

		$prefixActivation = CRUDBooster::getsetting('prefix_reference_inquiryBalance');
			$keterangan = 'inquiryBalance';
			$referenceNumber = DB::select('exec CreateRefNumberSobatku ?,?', array($prefixActivation, $keterangan));
			// dd($referenceNumber[0]->Prefix);

		$request = [
			'accountNumber' => $accountNumber,
			'referenceNumber' => $referenceNumber[0]->Prefix,
			'user' => $user,
			'hashCode' => hash('sha256', $accountNumber.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
		];

		$data_toJson = json_encode( $request );
		$ch = curl_init( $serviceURL );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept:application/json'
			)
		);
		$response = curl_exec( $ch );
		$responseBody = json_decode( $response );
		curl_close($ch);

		return $response;
	}

	public function requestPayment(){
		$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/requestPayment';
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		$accountNumber = Request::get('accountNumber');
		$amount = Request::get('amount');
		// $referenceNumber = Request::get('referenceNumber');
		$description = Request::get('description');

		$prefixActivation = CRUDBooster::getsetting('prefix_reference_requestPayment');
			$keterangan = 'requestPayment';
			$referenceNumber = DB::select('exec CreateRefNumberSobatku ?,?', array($prefixActivation, $keterangan));
			// dd($referenceNumber[0]->Prefix);

		$request = [
			'accountNumber' => $accountNumber,
			'amount' => $amount,
			'referenceNumber' => $referenceNumber[0]->Prefix,
			'description' => $description,
			'user' => $user,
			'hashCode' => hash('sha256', $accountNumber.$amount.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
		];

		$data_toJson = json_encode( $request );
		$ch = curl_init( $serviceURL );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept:application/json'
			)
		);
		$response = curl_exec( $ch );
		$responseBody = json_decode( $response,true );
		if($responseBody['responseCode'] == '00'){
			$responseBody['urlPayment'] = CRUDBooster::getsetting('parent_url_sobatku_api').'/walletPage/payment?phoneNumber='.$accountNumber.'&transactionId='.$responseBody['transactionId'].'&sessionId='.$responseBody['sessionId'];
		}
		curl_close($ch);
		$saveResponse = DB::statement('exec CreateLogSobatku ?,?,?,?,?', array($accountNumber,$referenceNumber[0]->Prefix,$data_toJson,$response,'Request Payment'));
		return $responseBody;
	}

	public function paymentResult(){
		$request = '';
		$accountNumber = Request::get('accountNumber');
		$transactionId = Request::get('transactionId');
		$amount = Request::get('amount');
		$responseCode = Request::get('responseCode');
		$responseDescription = Request::get('responseDescription');
		$hashCode = Request::get('hashCode');

		$response = [
			'accountNumber' => $accountNumber,
			'transactionId' => $transactionId,
			'amount' => $amount,
			'responseCode' => $responseCode,
			'responseDescription' => $responseDescription,
			'hashCode' => $hashCode
		];

		$responseJson = json_encode($response);

		$saveResponse = DB::statement('exec CreateLogSobatku ?,?,?,?,?', array($accountNumber,'',$request,$responseJson,'Payment Result'));

		return $this->respondWithDataAndMessage($response, "Success");
	}

	
}