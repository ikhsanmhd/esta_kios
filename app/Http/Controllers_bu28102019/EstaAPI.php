<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;
use DB;
use Session;
use Request;
use Route;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;
use Storage;
use Esta;
use App;
use Illuminate\Support\Facades\Auth;

class EstaAPI extends ApiController
{
	public function postLogin() {
		$no_hp = Request::get('no_hp');
		$password = Request::get('password');
		$regid = Request::get('regid');

		$check_pengajuan = DB::table('txn_pengajuan_agen')
			->where('agen_no_hp',$no_hp)
			->where('kode_pengajuan_agen','!=',NULL)
			->whereNull('deleted_at')
			->orderBy('id', 'DESC')
			->first();
		$check_hp = DB::table('agen')
			->where('no_hp',$no_hp)
			->where('kode','!=',NULL)
			->whereNull('deleted_at')
			->first();	

		$check_kios = DB::table('txn_agenkios')
			->where('agen_id',$check_hp->id)
			->where('is_agen_kios','!=',1)
			->first();
			
		if(!empty($check_pengajuan)){
			if(hash::check($password, $check_pengajuan->agen_password)){
            
	        } else {
	            $response['api_status']  = 0;
	            $response['api_message'] = 'PIN anda salah';
	            $response['type_dialog']  = 'Error';
	            return response()->json($response);
	            exit();
	        }
		}elseif(!empty($check_hp)){
			if(hash::check($password, $check_hp->password)){
	            
	        } else {
	            $response['api_status']  = 0;
	            $response['api_message'] = 'PIN anda salah';
	            $response['type_dialog']  = 'Error';
	            return response()->json($response);
	            exit();
	        }
	    }
			
		if(!empty($check_pengajuan) && $check_pengajuan->flag_registrasi != 5 && $check_pengajuan->flag_registrasi != NULL && $check_pengajuan->flag_registrasi != 0){
			$item['nama'] = $check_pengajuan->agen_nama;
    		$item['no_hp'] = $check_pengajuan->agen_no_hp;
    		$item['email'] = ($check_pengajuan->agen_email == '' ? '-' : $check_pengajuan->agen_email);
    		$item['kode_referall_agen'] = $check_pengajuan->agen_kode_referall_agen;
    		$item['status_agen'] = $check_pengajuan->agen_status_agen;
    		$item['saldo'] = ($check_pengajuan->agen_saldo <= 0 ? 0 : $check_pengajuan->agen_saldo);
    		$item['photo'] = asset('').$check_pengajuan->agen_photo;
			$item['id_agen'] = $check_pengajuan->id;
			$item['flag_registrasi'] = $check_pengajuan->flag_registrasi;
    		$item['is_existing'] = $check_pengajuan->agen_is_existing;
			
			$checkagen = DB::select('exec CheckAgenKios ?',array($check_hp->id))[0];
			$item['is_agen_kios'] = $checkagen->status;
			
			$item['response_code_sobatku'] = ($check_pengajuan->agen_tglaktivasisobatku != NULL ? 'RA' : 'RB');
			$response['api_status']  = 1;
	        $response['api_message'] = 'Success';
	        $response['type_dialog']  = 'Informasi';
	        $response['item'] = $item;
			return response()->json($response);
		}elseif(!empty($check_hp) && $check_hp->flag_registrasi != 5 && $check_hp->flag_registrasi != NULL && $check_hp->flag_registrasi != 0){
			$item['id_agen'] = $check_hp->id;
			$item['flag_registrasi'] = $check_hp->flag_registrasi;
			$item['nama'] = $check_hp->nama;
    		$item['no_hp'] = $check_hp->no_hp;
    		$item['email'] = ($check_hp->email == '' ? '-' : $check_hp->email);
    		$item['kode_referall_agen'] = $check_hp->kode_referall_agen;
    		$item['status_agen'] = $check_hp->status_agen;
    		$item['saldo'] = ($check_hp->saldo <= 0 ? 0 : $check_hp->saldo);
    		$item['photo'] = asset('').$check_hp->photo;
			$item['is_existing'] = $check_hp->is_existing;
			
    		$checkagen = DB::select('exec CheckAgenKios ?',array($check_hp->id))[0];
			$item['is_agen_kios'] = $checkagen->status;
			
			$item['response_code_sobatku'] = ($check_hp->tglaktivasisobatku != NULL ? 'RA' : 'RB');
    		$response['api_status']  = 1;
	        $response['api_message'] = 'Success';
	        $response['type_dialog']  = 'Informasi';
			$response['item'] = $item;
    		return response()->json($response);
		}

		if((!empty($check_kios)) && $check_hp->is_agen_kios != 1 && $check_kios->status != 'Rejected' && $check_hp->tglverifikasisobatku == NULL &&
			$check_hp->tglaktivasisobatku == NULL){
			$response['api_status']  = 0;
		    $response['api_message'] = 'Akun anda dalam proses pengajuan agen kios. Mohon tunggu notifikasi aktivasi akun agen kios anda';
			$response['type_dialog']  = 'Informasi';
		    return response()->json($response);
		}

		if(!empty($check_pengajuan)){
			if($check_pengajuan->agen_tglpengajuansobatku != NULL && $check_pengajuan->agen_tglverifikasisobatku == NULL &&
			$check_pengajuan->agen_tglaktivasisobatku == NULL){
				$response['api_status']  = 0;
			    $response['api_message'] = 'Kami akan melakukan verifikasi akun anda terlebih dahulu dalam 1x24 jam kerja';
			    $response['type_dialog']  = 'Informasi';
				$response['id_pengajuan'] = $check_pengajuan->id;
			    return response()->json($response);
			}
		}

		if($check_hp->tglpengajuansobatku != NULL && $check_hp->tglverifikasisobatku == NULL &&
			$check_hp->tglaktivasisobatku == NULL){
			$response['api_status']  = 0;
		    $response['api_message'] = 'Kami akan melakukan verifikasi akun anda terlebih dahulu dalam 1x24 jam kerja';
			$response['type_dialog']  = 'Informasi';
		    return response()->json($response);
		}
		if(!empty($check_hp)) {
			if($check_hp->regid != NULL && $check_hp->is_existing == 0) {
				$response['api_status']  = 99;
			    $response['api_message'] = CRUDBooster::getsetting('pesan_multi_login');
			    $response['type_dialog']  = 'Error';
			    //Esta::send_sms($check_hp->no_hp, $response['api_message']);

			    /*$up['status_aktif'] = 'Tidak Aktif';
			    $upp = DB::table('agen')
			    	->where('id',$check_hp->id)
			    	->update($up);*/

			  	return response()->json($response);
			}
			if($check_hp->status_aktif == 'Tidak Aktif') {
				$response['api_status']  = 3;
		        $response['api_message'] = CRUDBooster::getsetting('pesan_paksa_logout');
		        $response['type_dialog']  = 'Error';
			} else {
				$check_pass = DB::table('agen')
					->where('no_hp',$no_hp)
					->where('kode','!=',NULL)
					->whereNull('deleted_at')
					->first();
				if(hash::check($password, $check_pass->password)){
					$up['regid'] = $regid;
					$update = DB::table('agen')
						->where('id',$check_pass->id)
						->update($up);

					$item['id_agen'] = $check_pass->id;
					$item['nama'] = $check_pass->nama;
		    		$item['no_hp'] = $check_pass->no_hp;
		    		$item['email'] = ($check_pass->email == '' ? '-' : $check_pass->email);
		    		$item['kode_referall_agen'] = $check_pass->kode_referall_agen;
		    		$item['status_agen'] = $check_pass->status_agen;
		    		$item['saldo'] = ($check_pass->saldo <= 0 ? 0 : $check_pass->saldo);
		    		$item['photo'] = asset('').$check_pass->photo;
		    		$item['is_existing'] = $check_pass->is_existing;
					
					$checkagen = DB::select('exec CheckAgenKios ?',array($check_hp->id))[0];
					$item['is_agen_kios'] = $checkagen->status;
					
					$item['flag_registrasi'] = $check_pass->flag_registrasi;
					$item['response_code_sobatku'] = ($check_pass->tglaktivasisobatku != NULL ? 'RA' : 'RB');
		    		$response['api_status']  = 1;
			        $response['api_message'] = 'Login berhasil';
			        $response['type_dialog']  = 'Informasi';
			        $response['item'] = $item;
				} else {
					$response['api_status']  = 2;
			        $response['api_message'] = 'Password salah';
			        $response['type_dialog']  = 'Error';
				}
			}
		} else {
			$response['api_status']  = 0;
		    $response['api_message'] = 'No HP tidak ditemukan';
		    $response['type_dialog']  = 'Error';
		}

		return response()->json($response);
	}
	
	/*public function posttariktunaimigrasi(){
		$agen = Auth::user();
        $id_agen = $agen->id;
        $nomor_rekening = Request::get('nomor_rekening');
    	$nama_akun_bank = Request::get('nama_akun_bank');
    	$bank = Request::get('bank');
    	$pin = Request::get('pin');
    	$nominal_penarikan = Request::get('nominal_penarikan');
        $created_at = date('Y-m-d H:i:sa');

        $detail_agen = DB::table('agen')
            ->where('id',$id_agen)
            ->first();

        if ($detail_agen->saldo < $nominal_penarikan) {
        	$response['api_status']  = 0;
            $response['api_message'] = 'Saldo Anda Tidak Cukup';
            $response['type_dialog']  = 'Error';
            return response()->json($response);
            exit();
        }

        if(hash::check($pin, $detail_agen->password)){
            
        } else {
            $response['api_status']  = 0;
            $response['api_message'] = 'PIN anda salah';
            $response['type_dialog']  = 'Error';
            return response()->json($response);
            exit();
        }
        
            $saveTarikTunai = DB::select('exec postTarikTunaiMigrasi ?,?,?,?,?,?',array($id_agen,$nomor_rekening,$nama_akun_bank,$bank,$nominal_penarikan,$created_at));

            if($saveTarikTunai){
               		$response['api_status']  = 1;
			        $response['api_message'] = 'Data Berhasil Di Submit';
			        $response['type_dialog']  = 'Informasi';
            }else{
            		$response['api_status']  = 2;
			        $response['api_message'] = 'Data Gagal Di Submit';
			        $response['type_dialog']  = 'Error';
        }
        return response()->json($response);
    }*/

    public function postTarikTunaiMigrasi(){
    	$agen = Auth::user();
        $id_agen = $agen->id;
		$tanpa_biaya_admin = CRUDBooster::getsetting('tarik_tunai_tanpa_biaya_admin');
		$biaya_admin = CRUDBooster::getsetting('biaya_admin_tarik_tunai');
		$minimal_tarik_tunai = CRUDBooster::getsetting('minimal_tarik_tunai');
		$nominal_penarikan = Request::get('nominal_penarikan');
		$nomor_rekening = Request::get('nomor_rekening');
		$bank = Request::get('bank');
		$nama_akun_bank = Request::get('nama_akun_bank');
		
		$pin = Request::get('pin');

		$detail = DB::select('exec getAgenById ?', array($id_agen))[0];
		$detail_bank = DB::table('i_banktariktunai')
			->where('nama',$bank)
			->first();
			
		if(hash::check($pin, $detail->password)){
            
        } else {
            $response['api_status']  = 0;
            $response['api_message'] = 'PIN anda salah';
            $response['type_dialog']  = 'Error';
            return response()->json($response);
            exit();
        }

		$id_agen = $id_agen;
		$id_bank = $detail_bank->id;
		$no_rek = $nomor_rekening;
		$bank_nama = $detail_bank->nama;
		$bank_kode = $detail_bank->kode_bank;
		$nama_akun_bank = $nama_akun_bank;
		$jml_tarik_tunai = $nominal_penarikan;
		$biaya_admin = ($jml_tarik_tunai < $tanpa_biaya_admin ? $biaya_admin : 0);
		$trans_amount = $jml_tarik_tunai-$biaya_admin;

		$kode = Esta::nomor_transaksi('trans_tarik_tunai',CRUDBooster::getsetting('transaksi_tarik_tunai'));
		//if($sv['jml_tarik_tunai'] > $minimal_tarik_tunai) {
			if($detail->saldo >= $trans_amount+$biaya_admin) {

				$trans_no = $kode;
				$created_user = Esta::user($id_agen);
				$updated_user = Esta::user($id_agen);
				$ref_trans_no = $kode;
				$trans_desc = 'TARIK TUNAI';
				//$status = 'Pending';
				//$create_user = $detail->nama;
				$created_at = date('Y-m-d H:i:s');
				$updated_at = date('Y-m-d H:i:s');
				$trans_date = date('Y-m-d H:i:s');
				$agen_nama = $detail->nama;
				$agen_email = $detail->email;
				$agen_kode = $detail->kode;
				$agen_level = $detail->status_agen;
				$agen_referall = $detail->kode_referall_agen;
				$agen_referall_relation = $detail->kode_relation_referall;
				$agen_status_aktif = $detail->status_aktif;
				$agen_nik = $detail->nik;
				$agen_tgl_register = $detail->tgl_register;
				$agen_tempat_lahir = $detail->tempat_lahir;
				$agen_tgl_lahir = $detail->tgl_lahir;
				$agen_jenis_kelamin = $detail->jenis_kelamin;
				$agen_agama = $detail->agama;
				$agen_no_hp = $detail->no_hp;
				$agen_status_perkawinan = $detail->status_perkawinan;
				$agen_pekerjaan = $detail->pekerjaan;
				$agen_kewarganegaraan = $detail->kewarganegaraan;
				$agen_prov = $detail->prov;
				$agen_kab = $detail->kab;
				$agen_kec = $detail->kec;
				$agen_kel = $detail->kel;

				/*$save = DB::table('trans_tarik_tunai')
					->insertGetId($sv);*/
				$save = DB::select('exec postTransTarikTunai ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								, array($id_agen,$id_bank,$no_rek,$bank_nama,$bank_kode,$nama_akun_bank,$jml_tarik_tunai,$biaya_admin,$trans_amount,$trans_no,$created_user,$updated_user,$ref_trans_no,$trans_desc,$created_at,$updated_at,$trans_date,$agen_nama,$agen_email,$agen_kode,$agen_level,$agen_referall,$agen_referall_relation,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin,$agen_agama,$agen_no_hp,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov,$agen_kab,$agen_kec,$agen_kel));
				if($save){
					Esta::log_money_old($id_agen,$jml_tarik_tunai+$biaya_admin,date('Y-m-d H:i:s'),'Tarik Tunai','Request tarik tunai','Out','Transaksi','trans_tarik_tunai',$save[0]->id);
					Esta::log_money_old($id_agen,0,date('Y-m-d H:i:s'),'Agen Tarik Tunai','Agen Tarik Tunai '.$save_trans['trans_no'],'','Riwayat Agen','trans_tarik_tunai',$save[0]->id);
		    		
					$response['api_status']  = 1;
		        	$response['api_message'] = 'Permintaan Tarik Tunai Anda akan diproses dalam 1x24 jam hari kerja';
		        	$response['type_dialog']  = 'Informasi';
		        	$response['saldo']  = DB::table('agen')->where('id',$id_agen)->first()->saldo;
				} else {
					$response['api_status']  = 0;
			        $response['api_message'] = 'Gagal request tarik dana';
			        $response['type_dialog']  = 'Error';
				}
			} else {
				$response['api_status']  = 2;
			    $response['api_message'] = 'Saldo anda tidak mencukupi';
			    $response['type_dialog']  = 'Error';
			}
		/*} else {
			$response['api_status']  = 3;
		    $response['api_message'] = 'Minimal tarik tunai Rp '.number_format($minimal_tarik_tunai,0,',','.');
		    $response['type_dialog']  = 'Error';
		}*/
	    return response()->json($response);
    }

	public function getBank(){
		$bankList = DB::select('exec get_iBankTarikTunai');
		$response['api_status']  = 1;
        $response['api_message'] = 'Success';
        $response['type_dialog']  = 'Informasi';
        $response['item'] = $bankList;
        return response()->json($response);
	}

    public function postSlider() {
		$sliders = DB::select('exec getSlider');

		$rest_slider = array();
	  	foreach($sliders as $slider) {
			$rest['id']         = $slider->id;
			$rest['datetime']         = $slider->created_at;
			$rest['redirect']         = ($slider->redirect != '' ? $slider->redirect : 'Tidak');
			$rest['tagline'] = '';//$slider->title;
			$rest['url']      = $slider->url;
			$rest['description']      = $slider->description;
			$rest['title']   = '';//$slider->tagline;
			$rest['image']      = asset('').$slider->image;
			array_push($rest_slider, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Slider';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_slider;

	  	return response()->json($response);
	}

	public function postAllVoucher() {
		$agen = Auth::user();
        $id_agen = $agen->id;

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen));

		/*$arrays = DB::table('trans_voucher_child')
			->where('id_agen',$id_agen)
			->where('used','No')
			->where('voucher_expired','>=',date('Y-m-d H:i:s'))
			->orderBy('id','desc')
			->get();*/

		$arrays = DB::select('exec getVoucherChild ?', array($id_agen));

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$detail_voucher = DB::table('voucher')
				->where('id',$array->id_voucher)
				->first();

			$rest['id'] = $array->id;
			$rest['nama'] = $detail_voucher->nama;
			$rest['expired_date'] = $detail_voucher->expired_date;
			$rest['amount'] = $detail_voucher->amount;
			$rest['description'] = $detail_voucher->description;
			$rest['tagline'] = $detail_voucher->tagline;
			$rest['syarat_ketentuan'] = $detail_voucher->syarat_ketentuan;
			$rest['image'] = asset('').$detail_voucher->image;
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postRiwayatTransaksi() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$tgl_awal = Request::get('tgl_awal');
		$tgl_akhir = Request::get('tgl_akhir');
		$limit = Request::get('limit');
		$offset = Request::get('offset');
		//$offset = ($offset == 0 ? 1 : $offset);

		$arrays = DB::select('exec getLogMoneyTransaksi ?,?,?', array($id_agen,$tgl_awal,$tgl_akhir));
//dd($arrays);

		$rest_json = array();

	  	foreach($arrays as $array) {
	  		$detail = DB::table($array->tbl_transaksi)
	  			->where('id',$array->id_transaksi)
	  			->first();

	  		$return = (strpos($array->description, 'Return') !== false ? 'RETURN ' : '');

	  		$st = ($detail->trans_desc == 'TARIK TUNAI' || $detail->trans_desc == NULL || $detail->trans_desc == 'Top up VA' || $detail->status == 'Delete' || $detail->status == 'Deleted' ? 'Clear' : $detail->status);
	  		$st2 = ($detail->status == 'Pending' && $detail->trans_desc == 'PULSA PRABAYAR' ? 'Clear' : 'No Clear');
	  		if($detail->trans_desc != NULL) {
	  			if($st == 'Clear' || $st == 'Received' || $st == 'Approved' || $st2 == 'Clear') {
				$rest['id'] = $array->id;
				$rest['datetime'] = Esta::change_date_format($array->created_at,'d M Y H:i');

				if($detail->bank_nama == 'Alfagroup') {
					$rest['trans_desc'] = 'Top up';
				} else {
					$rest['trans_desc'] = $detail->trans_desc;
				}
				

				if($rest['trans_desc'] == 'PLN PRABAYAR') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_rp_bayar,0,',','.');
					$rest['token'] = substr($detail->struk_token,0,-1);
					$rest['nomor'] = $detail->no_meter;
					$rest['type'] = 'PLN';
				} elseif($rest['trans_desc'] == 'PLN PASCABAYAR') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->no_meter;
					$rest['type'] = 'PLN';
				} elseif($rest['trans_desc'] == 'PULSA PRABAYAR') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->no_hp;
					$rest['type'] = 'Pulsa';
				} elseif($rest['trans_desc'] == 'PULSA PASCABAYAR') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->no_hp;
					$rest['type'] = 'Pulsa';
				} elseif($rest['trans_desc'] == 'PDAM PASCABAYAR') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->struk_norek;
					$rest['type'] = 'PDAM';
				} elseif($rest['trans_desc'] == 'BPJS Kesehatan') {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->struk_no_va_keluarga;
					$rest['type'] = 'BPJS';
				} elseif($detail->trans_desc == 'Top up VA' || $detail->trans_desc == 'Top Up') {
					$rest['amount'] = 'Rp '.number_format($detail->trans_amount-$detail->biaya_admin_bank-$detail->biaya_admin,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->agen_no_hp;
					$rest['type'] = 'Topup';
					//if($detail->bank_nama == 'Alfagroup') {
						$rest['trans_desc'] = 'Top up';
					//}
				} elseif($rest['trans_desc'] == 'TARIK TUNAI') {
					$rest['amount'] = 'Rp '.number_format($detail->trans_amount+$detail->biaya_admin,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = $detail->agen_no_hp;
					$rest['type'] = 'Tarik Tunai';
				} else {
					$rest['amount'] = 'Rp '.number_format($detail->struk_total_bayar,0,',','.');
					$rest['token'] = '';
					$rest['nomor'] = '';
					$rest['type'] = 'Pulsa';
				}

				if($rest['trans_desc'] == 'Top up VA') {
					$rest['cashback'] = '';
					$rest['voucher_nama'] = '';
					$rest['voucher_amount'] = '';
					$rest['total_pembayaran'] = '';
					$rest['product_nama'] = 'Top Up';
					$rest['trans_desc'] = 'TOP UP ESTAWALLET';
				} elseif($rest['trans_desc'] == 'TARIK TUNAI') {
					$rest['cashback'] = '';
					$rest['voucher_nama'] = '';
					$rest['voucher_amount'] = '';
					$rest['total_pembayaran'] = '';
					$rest['product_nama'] = 'Tarik Tunai';
					$rest['trans_desc'] = 'TARIK TUNAI';
				} else {
					$rest['cashback'] = 'Rp '.number_format($detail->komisi,0,',','.');
					$rest['voucher_nama'] = ($detail->voucher_nama != '' ? $detail->voucher_nama : '');
					$rest['voucher_amount'] = ($detail->voucher_amount >= 1 ? 'Rp '.number_format($detail->voucher_amount,0,',','.') : '');
					$rest['total_pembayaran'] = 'Rp '.number_format($detail->struk_total_pembayaran,0,',','.');
					$rest['trans_desc'] = $return.$detail->trans_desc;
					if($rest['trans_desc'] == 'PLN PASCABAYAR') {
						$rest['product_nama'] = 'Tagihan '.$detail->struk_blth;
					} elseif($rest['trans_desc'] == 'BPJS Kesehatan') {
						$rest['product_nama'] = 'Tagihan '.$detail->struk_periode.' BLN';
					} elseif($rest['trans_desc'] == 'PDAM') {
						$rest['product_nama'] = 'Tagihan '.$detail->struk_blth;
					} else {
						$rest['product_nama'] = $detail->product_nama;
					}
				}
				if($detail->bank_nama == 'Alfagroup') {
					$rest['trans_desc'] = 'Top Up Alfa';
				} else {
					$rest['trans_desc'] = $return.$detail->trans_desc;
				}
				array_push($rest_json, $rest);
		}
		}
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    if($offset >= 1) {
	    	$response['items'] = [];
	    } else {
		    $response['items'] = $rest_json;
		}

	  	return response()->json($response);
	}

	public function postRiwayatKomisi() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$tgl_awal = Request::get('tgl_awal');
		$tgl_akhir = Request::get('tgl_akhir');
		$limit = Request::get('limit');
		$offset = Request::get('offset');
		//$offset = ($offset == 0 ? 1 : $offset);
		$bulan_default = date('Y-m-d', strtotime(date('Y-m-d'). ' - 3 months'));

		$arrays = DB::table('log_money')
			->where('id_agen',$id_agen)
			->where('kategori','Komisi');
			if(!empty($tgl_awal)) {
				$arrays = $arrays->whereBetween('created_at',[$tgl_awal.' 01:00:00',$tgl_akhir.' 23:59:59']);
			} else {
				$arrays = $arrays->whereBetween('created_at',[$bulan_default.' 01:00:00',date('Y-m-d H:i:s')]);
			}
			//if($offset == 2) {
				/*$arrays = $arrays
				->offset($offset)
	            ->limit($limit);*/
	        /*} else {
		        $arrays = $arrays
		        ->limit($limit);
		    }*/
		    $arrays = $arrays
		    ->orderBy('created_at','DESC')
			->get();

		//dd($arrays);
		
		$rest_json = array();
		$total_komisi = 0;
	  	foreach($arrays as $array) {
	  		$tr = DB::table($array->tbl_transaksi)->where('id',$array->id_transaksi)->first();
	  		$total_komisi += $tr->komisi;

	  		/*$st = ($tr->trans_desc == 'TARIK TUNAI' || $tr->trans_desc == NULL || $tr->trans_desc == 'Top up VA' || $tr->status == 'Delete' || $tr->status == 'Deleted' ? 'Clear' : $tr->status);
	  		if($tr->trans_desc != NULL) {
	  			if($st == 'Clear' || $st == 'Received' || $st == 'Approved') {*/

	  		$st = ($tr->trans_desc == 'TARIK TUNAI' || $tr->trans_desc == NULL || $tr->trans_desc == 'Top up VA' || $tr->status == 'Delete' || $tr->status == 'Deleted' ? 'Clear' : $tr->status);
	  		$st2 = ($tr->status == 'Pending' && $tr->trans_desc == 'PULSA PRABAYAR' ? 'Clear' : 'No Clear');
	  		if($tr->trans_desc != NULL) {
	  			if($st == 'Clear' || $st == 'Received' || $st == 'Approved' || $st2 == 'Clear') {

	  		//if($array->type == 'In') {
					$rest['id'] = $array->id;
					$rest['datetime'] = $array->created_at;
					$return = (strpos($array->description, 'Return') !== false ? 'Return ' : '');
					$rest['description'] = $return.($tr->trans_desc == 'BPJS Ketenagakerjaan' ? 'Cashback Pembayaran' : (strpos($tr->trans_desc, 'PRABAYAR') !== false ? 'Cashback Pembelian' : 'Cashback Pembayaran'));
					
					if($return == 'Return ') {
						$rest['nominal'] = '(Rp '.($tr->komisi <= 0 ? 0 : $tr->komisi).')';
					} else {
						$rest['nominal'] = 'Rp '.($tr->komisi <= 0 ? 0 : $tr->komisi);
					}
					$rest['type'] = $array->type;


					if($tr->trans_desc == 'PLN PASCABAYAR') {
						$rest['nama_product'] = 'Tagihan Listrik '.$tr->struk_blth;
					} elseif($tr->trans_desc == 'PULSA PASCABAYAR') {
						$rest['nama_product'] = 'Tagihan '.$tr->product_nama;
					} elseif($tr->trans_desc == 'PDAM') {
						$rest['nama_product'] = 'Tagihan PDAM '.$tr->struk_blth;
					} elseif($tr->trans_desc == 'BPJS Kesehatan') {
						$rest['nama_product'] = 'Tagihan BPJS Kesehatan '.$tr->struk_periode.' BLN';
					} else {
						$rest['nama_product'] = $tr->product_nama;
					}

					array_push($rest_json, $rest);
			//}
				}
			}
	  	}
	  	
	  	$total = DB::table('log_money')
			->where('id_agen',$id_agen)
			->where('kategori','Komisi');
			if(!empty($tgl_awal)) {
				$total = $total->whereBetween('created_at',[$tgl_awal.' 01:00:00',$tgl_akhir.' 23:59:59']);
			}
			$total = $total
			->orderBy('id','DESC')
			->get();
		$totall = 0;
		foreach($total as $tl) {
			$trr = DB::table($tl->tbl_transaksi)->where('id',$tl->id_transaksi)->first();
			$return = (strpos($tl->description, 'Return') !== false ? 'Return ' : '');
			if($return == 'Return ') {
				$totall -= $trr->komisi;
			} else {
		  		$totall += $trr->komisi;
		  	}
		}

	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    if($offset >= 1) {
	    	$response['items'] = [];
	    } else {
		    $response['items'] = $rest_json;
		}
	    $response['total_komisi'] = ($totall <= 0 ? 0 : $totall);

	  	return response()->json($response);
	}

	public function postKirimEmail() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$id_transaksi = Request::get('id_transaksi');
		$email = Request::get('email');
		//$id_agen = Request::get('id_agen');
		$type = Request::get('type');
		$kategori = Request::get('kategori');

		switch ($type) {
			case 'pulsa_postpaid':
				$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
				$detail_jatelindo = DB::table('log_jatelindo_bit')
					->where('id_transaksi',$id_transaksi->id_transaksi)
					->where('type','Pulsa Postpaid')
					//->where('status','Purchase')
					->where('jenis','res')
					//->orWhere('status','Advice Manual')
					->orderBy('id','desc')
					->first();

				$detail_transaksi = DB::table('trans_pulsa')
					->where('trans_no',$detail_jatelindo->bit37)
					->first();

				$rest['pls_psc_waktu_lunas'] = $detail_transaksi->struk_tgl_lunas;
				$rest['pls_psc_no_hp'] = $detail_transaksi->no_hp;
				$rest['pls_psc_total_bill'] = $detail_transaksi->struk_total_bill_tagihan;
				$rest['nama_pelanggan'] = $detail_transaksi->struk_nama_pelanggan;
				$rest['pls_psc_blth_1'] = $detail_transaksi->struk_tag_bln_1;
				$rest['pls_psc_jml_tagihan_1'] = 'Rp '.number_format($detail_transaksi->struk_tag_amount_1,0,',','.');
				$rest['pls_psc_blth_2'] = $detail_transaksi->struk_tag_bln_2;
				$rest['pls_psc_jml_tagihan_2'] = 'Rp '.number_format($detail_transaksi->struk_tag_amount_2,0,',','.');
				$rest['pls_psc_blth_3'] = $detail_transaksi->struk_tag_bln_3;
				$rest['pls_psc_jml_tagihan_3'] = 'Rp '.number_format($detail_transaksi->struk_tag_amount_3,0,',','.');
				$rest['total_tagihan'] = 'Rp '.number_format($detail_transaksi->struk_total_tagihan,0,',','.');
				$rest['biaya_admin'] = 'Rp '.number_format($detail_transaksi->struk_biaya_admin,0,',','.');
				$rest['total_bayar'] = 'Rp '.number_format($detail_transaksi->struk_total_bayar,0,',','.');
				$rest['pls_psc_no_ref_switching'] = $detail_transaksi->jpa_ref;

				$view     = view('struk/struk_pulsa_postpaid',$rest)->render();
				$filename = "Struk-Pulsa-Postpaid-".$no_hp;
				$pdf      = App::make('dompdf.wrapper');

				$path = storage_path('app/uploads/'.$filename.'.pdf');

				$pdf->loadHTML($view);
				$pdf->setPaper('A4','landscape');
				$output = $pdf->output();

				file_put_contents($path, $output);

				$attachments = [$path];

				if(!empty($email)) {
				    Esta::kirimemail(['to'=>$email,'data'=>$rest,'template'=>'email_transaksi_pulsa_postpaid','attachments'=>$attachments]);
				}
				break;
			case 'pulsa_prepaid':
				/*$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
				$detail_jatelindo = DB::table('log_jatelindo_bit')
					->where('id_transaksi',$id_transaksi->id_transaksi)
					->where('type','Pulsa Prepaid')
					->where('jenis','res')
					->orderBy('id','desc')
					->first();*/
				/*print_r($id_transaksi);
				exit();*/

				$detail_transaksi = DB::table('trans_pulsa')
					->where('id',$id_transaksi)
					->first();

		    	$response['sn']  = $detail_transaksi->struk_sn;
		    	$response['tanggal']  = $detail_transaksi->struk_tgl_lunas;
		    	$response['product_nama']  = $detail_transaksi->product_nama;
		    	$response['product_amount_rp'] = 'Rp '.number_format($detail_transaksi->struk_total_bayar,0,',','.');
		    	$response['no_hp'] = $detail_transaksi->no_hp;


				$view     = view('struk/struk_pulsa_prepaid',$response)->render();
				$filename = "Struk-Pulsa-Prepaid-".$no_hp;
				$pdf      = App::make('dompdf.wrapper');

				$path = storage_path('app/uploads/'.$filename.'.pdf');

				$pdf->loadHTML($view);
				$pdf->setPaper('A4','landscape');
				$output = $pdf->output();

				file_put_contents($path, $output);

				$attachments = [$path];

				if(!empty($email) && $detail_agen->notif_email != 'No') {
				    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pulsa_prepaid','attachments'=>$attachments]);
				}
				break;
			case 'bpjs':
				$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
				$detail_jatelindo = DB::table('log_jatelindo_bit')
					->where('id_transaksi',$id_transaksi->id_transaksi)
					->where('type','BPJS Kesehatan')
					//->where('status','Purchase')
					->where('jenis','res')
					//->orWhere('status','Advice Manual')
					->orderBy('id','desc')
					->first();

				$detail_transaksi = DB::table('trans_bpjs')
					->where('trans_no',$detail_jatelindo->bit37)
					->first();

				$response['bpjsks_tgl_lunas'] = $detail_transaksi->struk_tgl_lunas;
				$response['bpjsks_jpa_refnum'] = $detail_transaksi->jpa_ref;
				$response['bpjsks_no_va_keluarga'] = $detail_transaksi->struk_no_va_keluarga;
				$response['bpjsks_no_va_kepala_keluarga'] = $detail_transaksi->struk_no_va_kepala_keluarga;
				$response['bpjsks_nama'] = $detail_transaksi->struk_nama_peserta;
				$response['bpjsks_jml_anggota_keluarga'] = $detail_transaksi->struk_jml_anggota_keluarga;
				$response['periode'] = $detail_transaksi->struk_periode;
				$response['bpjsks_total_premi'] = 'Rp '.number_format($detail_transaksi->struk_jml_tagihan,0,',','.');
				$response['bpjsks_biaya_admin'] = 'Rp '.number_format($detail_transaksi->struk_admin_bank,0,',','.');
				$response['total_bayar'] = 'Rp '.number_format($detail_transaksi->struk_total_bayar,0,',','.');
				$response['info'] = $detail_transaksi->struk_info;

				/*email*/
				$view     = view('struk/struk_bpjs_kesehatan',$response)->render();
				$filename = "Struk-BPJS-Kesehatan-".$no_hp;
				$pdf      = App::make('dompdf.wrapper');

				$path = storage_path('app/uploads/'.$filename.'.pdf');

				$pdf->loadHTML($view);
				$pdf->setPaper('A4','landscape');
				$output = $pdf->output();

				file_put_contents($path, $output);

				$attachments = [$path];

				if(!empty($email) && $detail_agen->notif_email != 'No') {
				    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_bpjs_kesehatan','attachments'=>$attachments]);
				}
				break;
			case 'pln':
			    switch ($kategori) {
			    	case 'Prepaid':
						//print_r($id_transaksi);
						//exit();
			    		$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
						$detail_jatelindo = DB::table('log_jatelindo_bit')
							->where('id_transaksi',$id_transaksi->id_transaksi)
							->where('type','PLN Prepaid')
							//->where('status','Purchase')
							->where('jenis','res')
							//->orWhere('status','Advice Manual')
							->orderBy('id','desc')
							->first();

						$detail_transaksi = DB::table('trans_pln')
							->where('trans_no',$detail_jatelindo->bit37)
							->first();


						$response['pln_pra_tgl_lunas'] = $detail_transaksi->struk_tgl_lunas;
						$response['pln_pra_meter_id'] = $detail_transaksi->no_meter;
						$response['pln_pra_id_pel'] = $detail_transaksi->struk_id_pelanggan;
						$response['pln_pra_nama'] = $detail_transaksi->struk_nama_pelanggan;
						$response['info'] = $detail_transaksi->struk_info;
						$response['pln_pra_tarif'] = $detail_transaksi->struk_tarif;
						$response['pln_pra_kategori_daya'] = $detail_transaksi->struk_daya;
						$response['pln_pra_ref_no'] = $detail_transaksi->jpa_ref;
						$response['rp_bayar'] = 'Rp '.number_format($detail_transaksi->struk_rp_bayar,0,',','.');
						$response['pln_pra_biaya_materai'] = 'Rp '.number_format($detail_transaksi->struk_materai,2,',','.');
						$response['pln_pra_ppn'] = 'Rp '.number_format($detail_transaksi->struk_ppn,2,',','.');
						$response['pln_pra_ppju'] = 'Rp '.number_format($detail_transaksi->struk_ppj,2,',','.');
						$response['pln_pra_angsuran'] = 'Rp '.number_format($detail_transaksi->struk_angsuran,2,',','.');
						$response['pln_pra_rp_stroom'] = 'Rp '.number_format($detail_transaksi->struk_rp_stroom,2,',','.');
						$response['pln_pra_jml_kwh'] = $detail_transaksi->struk_jml_kwh;
						$response['pln_pra_token_number'] = $detail_transaksi->struk_token;
						$response['pln_pra_biaya_admin'] = 'Rp '.number_format($detail_transaksi->struk_admin_bank,0,',','.');

						/*kirim email*/
			    		$view     = view('struk/struk_pln_prepaid',$response)->render();
						$filename = "Struk-PLN-Prepaid-".$response['pln_pra_id_pel'];
						$pdf      = App::make('dompdf.wrapper');

						$path = storage_path('app/uploads/'.$filename.'.pdf');

						$pdf->loadHTML($view);
						$pdf->setPaper('A4','landscape');
						$output = $pdf->output();

						file_put_contents($path, $output);

						$attachments = [$path];
						Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pln_prepaid','attachments'=>$attachments]);
			    		break;
			    	case 'Postpaid':
			    		/*$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
						$detail_jatelindo = DB::table('log_jatelindo_bit')
							->where('id_transaksi',$id_transaksi->id_transaksi)
							->where('type','PLN Postpaid')
							->where('status','Purchase')
							->where('jenis','res')
							->first();

						print_r($detail_jatelindo);
						exit();*/

						$detail_transaksi = DB::table('trans_pln')
							->where('id',$id_transaksi)
							->first();

						$response['pln_psc_tgl_lunas'] = $detail_transaksi->struk_tgl_lunas;
						$response['pln_psc_id_pel'] = $detail_transaksi->struk_id_pelanggan;
						$response['pln_psc_blth'] = $detail_transaksi->struk_blth;
						$response['pln_psc_nama'] = $detail_transaksi->struk_nama_pelanggan;
						$response['stand_meter'] = $detail_transaksi->struk_stand_meter;
						$response['pln_psc_tarif'] = $detail_transaksi->struk_tarif;
						$response['pln_psc_daya'] = $detail_transaksi->struk_daya;
						$response['rp_transaksi'] = 'Rp '.number_format($detail_transaksi->product_amount,0,',','.');
						$response['pln_psc_no_ref'] = $detail_transaksi->jpa_ref;
						$response['pln_psc_biaya_admin'] = 'Rp '.number_format($detail_transaksi->struk_admin_bank,0,',','.');
						$response['total_bayar'] = 'Rp '.number_format($detail_transaksi->struk_total_bayar,0,',','.');
						$response['info'] = $detail_transaksi->struk_info;
						$response['pln_psc_jml_tunggakan'] = $detail_transaksi->struk_jml_tunggakan;

						/*kirim email*/
			    		$view     = view('struk/struk_pln_postpaid',$response)->render();
						$filename = "Struk-PLN-Postpaid-".$response['pln_psc_id_pel'];
						$pdf      = App::make('dompdf.wrapper');

						$path = storage_path('app/uploads/'.$filename.'.pdf');

						$pdf->loadHTML($view);
						$pdf->setPaper('A4','landscape');
						$output = $pdf->output();

						file_put_contents($path, $output);

						$attachments = [$path];
						Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pln_postpaid','attachments'=>$attachments]);
			    		break;
			    }

			    
				break;
			case 'pdam':
				$id_transaksi = DB::table('log_jatelindo_bit')->where('id',$id_transaksi)->first();
				$detail_jatelindo = DB::table('log_jatelindo_bit')
					->where('id_transaksi',$id_transaksi->id_transaksi)
					->where('type','PDAM Postpaid')
					//->where('status','Purchase')
					->where('jenis','res')
					//->orWhere('status','Advice Manual')
					->orderBy('id','desc')
					->first();

				$detail_transaksi = DB::table('trans_pdam')
					->where('trans_no',$detail_jatelindo->bit37)
					->first();

				$response['tgl_lunas'] = $detail_transaksi->struk_tgl_lunas;
				$response['pdam_idpel'] = $detail_transaksi->id_pelanggan;
				$response['pdam_blth'] = $detail_transaksi->struk_blth;
				$response['nama_pelanggan'] = $detail_transaksi->struk_nama_pelanggan;
				$response['pdam_no_ref_biller'] = $detail_transaksi->struk_jpa_ref;
				$response['tagihan'] = 'Rp '.number_format($detail_transaksi->product_amount,0,',','.');
				$response['biaya_admin'] = 'Rp '.number_format($detail_transaksi->struk_biaya_admin,0,',','.');
				$response['total_bayar'] = 'Rp '.number_format($detail_transaksi->struk_total_bayar,0,',','.');
				//$response['pdam_no_ref_biller'] = $detail_transaksi->jpa_ref;

				/*email*/
				$view     = view('struk/struk_pdam',$response)->render();
				$filename = "Struk-PDAM-".$no_hp;
				$pdf      = App::make('dompdf.wrapper');

				$path = storage_path('app/uploads/'.$filename.'.pdf');

				$pdf->loadHTML($view);
				$pdf->setPaper('A4','landscape');
				$output = $pdf->output();

				file_put_contents($path, $output);

				$attachments = [$path];
				//$email = $detail_agen->email;
				if(!empty($email)) {
				    Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pdam','attachments'=>$attachments]);
				}
				break;
		}
		$response['api_status']  = 1;
	    $response['api_message'] = 'Email sukses terkirim';
	    $response['type_dialog']  = 'Informasi';

	  	return response()->json($response);
	}

	public function postPrivacyPolicy() {
		$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['content'] = CRUDBooster::getsetting('syarat_dan_ketentuan');

	    return response()->json($response);
	}

	public function postResendOtp() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$no_hp_baru = Request::get('no_hp_baru');
		$type = Request::get('type');
		$kode_otp = rand(11,99).date('s');

		$detail = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$msg = str_replace('[kode]', $kode_otp, CRUDBooster::getsetting('otp_registrasi'));
		if($type = 'update') {
			//echo Esta::send_sms($no_hp_baru,'EstaKios - '.$detail->kode_otp);
			Esta::send_sms($detail->no_hp,$msg);
		} else {
			Esta::send_sms($detail->no_hp,$msg);
		}
		//$up['tgl_otp_terkirim'] = date('Y-m-d H:i:s');
		$up['kode_otp'] = $kode_otp;
		$up['updated_user'] = Esta::user($id_agen);
		$update = DB::table('agen')
			->where('id',$id_agen)
			->update($up);

		$response['api_status']  = 1;
	    $response['api_message'] = 'Kode verifikasi berhasil dikirim ulang';
	    $response['kode_otp']  = $kode_otp;
	    $response['type_dialog']  = 'Informasi';
		
	    return response()->json($response);
	}
	
	public function postTarikTunai() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$tanpa_biaya_admin = CRUDBooster::getsetting('tarik_tunai_tanpa_biaya_admin');
		$biaya_admin = CRUDBooster::getsetting('biaya_admin_tarik_tunai');
		$minimal_tarik_tunai = CRUDBooster::getsetting('minimal_tarik_tunai');

		$detail = DB::select('exec getAgenById ?', array($id_agen))[0];

		$detail_bank = DB::table('m_bank')
			->where('id',$detail->rek_id_bank)
			->first();

		$id_agen = $id_agen;
		$id_bank = $detail->id_bank;
		$no_rek = $detail->rek_no;
		$bank_nama = $detail_bank->nama;
		$bank_kode = $detail_bank->kode_bank;
		$nama_akun_bank = $detail->rek_nama;
		$jml_tarik_tunai = Request::get('nominal');
		$biaya_admin = ($jml_tarik_tunai < $tanpa_biaya_admin ? $biaya_admin : 0);
		$trans_amount = $jml_tarik_tunai;

		$kode = Esta::nomor_transaksi('trans_tarik_tunai',CRUDBooster::getsetting('transaksi_tarik_tunai'));
		//if($sv['jml_tarik_tunai'] > $minimal_tarik_tunai) {
			if($detail->saldo >= $trans_amount+$biaya_admin) {

				$trans_no = $kode;
				$created_user = Esta::user($id_agen);
				$updated_user = Esta::user($id_agen);
				$ref_trans_no = $kode;
				$trans_desc = 'TARIK TUNAI';
				//$status = 'Pending';
				//$create_user = $detail->nama;
				$created_at = date('Y-m-d H:i:s');
				$updated_at = date('Y-m-d H:i:s');
				$trans_date = date('Y-m-d H:i:s');
				$agen_nama = $detail->nama;
				$agen_email = $detail->email;
				$agen_kode = $detail->kode;
				$agen_level = $detail->status_agen;
				$agen_referall = $detail->kode_referall_agen;
				$agen_referall_relation = $detail->kode_relation_referall;
				$agen_status_aktif = $detail->status_aktif;
				$agen_nik = $detail->nik;
				$agen_tgl_register = $detail->tgl_register;
				$agen_tempat_lahir = $detail->tempat_lahir;
				$agen_tgl_lahir = $detail->tgl_lahir;
				$agen_jenis_kelamin = $detail->jenis_kelamin;
				$agen_agama = $detail->agama;
				$agen_no_hp = $detail->no_hp;
				$agen_status_perkawinan = $detail->status_perkawinan;
				$agen_pekerjaan = $detail->pekerjaan;
				$agen_kewarganegaraan = $detail->kewarganegaraan;
				$agen_prov = $detail->prov;
				$agen_kab = $detail->kab;
				$agen_kec = $detail->kec;
				$agen_kel = $detail->kel;

				/*$save = DB::table('trans_tarik_tunai')
					->insertGetId($sv);*/
				$save = DB::select('exec postTransTarikTunai ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								, array($id_agen,$id_bank,$no_rek,$bank_nama,$bank_kode,$nama_akun_bank,$jml_tarik_tunai,$biaya_admin,$trans_amount,$trans_no,$created_user,$updated_user,$ref_trans_no,$trans_desc,$created_at,$updated_at,$trans_date,$agen_nama,$agen_email,$agen_kode,$agen_level,$agen_referall,$agen_referall_relation,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin,$agen_agama,$agen_no_hp,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov,$agen_kab,$agen_kec,$agen_kel));
				if($save){
					Esta::log_money_old($id_agen,$jml_tarik_tunai+$biaya_admin,date('Y-m-d H:i:s'),'Tarik Tunai','Request tarik tunai','Out','Transaksi','trans_tarik_tunai',$save[0]->id);
					Esta::log_money_old($id_agen,0,date('Y-m-d H:i:s'),'Agen Tarik Tunai','Agen Tarik Tunai '.$save_trans['trans_no'],'','Riwayat Agen','trans_tarik_tunai',$save[0]->id);
		    		$response['api_status']  = 1;
		        	$response['api_message'] = 'Permintaan Tarik Tunai Anda akan diproses dalam 2x24 Jam';
		        	$response['type_dialog']  = 'Informasi';
		        	$response['saldo']  = DB::table('agen')->where('id',$id_agen)->first()->saldo;
				} else {
					$response['api_status']  = 0;
			        $response['api_message'] = 'Gagal request tarik dana';
			        $response['type_dialog']  = 'Error';
				}
			} else {
				$response['api_status']  = 2;
			    $response['api_message'] = 'Saldo anda tidak mencukupi';
			    $response['type_dialog']  = 'Error';
			}
		/*} else {
			$response['api_status']  = 3;
		    $response['api_message'] = 'Minimal tarik tunai Rp '.number_format($minimal_tarik_tunai,0,',','.');
		    $response['type_dialog']  = 'Error';
		}*/
	    return response()->json($response);

	}

	public function postShowPremium() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$detail = DB::table('agen_verify_premium')
	        ->where('id_agen',$id_agen)
	        ->first();
	    if(!empty($detail)) {
	    	$item['foto_ktp'] = asset('').$detail->foto_ktp;
	    	$item['foto_ktp_status'] = (!empty($detail->foto_ktp_status) ? $detail->foto_ktp_status : 'Pastikan keseluruhan KTP terlihat jelas');
	    	$item['foto_ktp_note'] = ($detail->foto_ktp_status == 'Approved' ? '' : (!empty($detail->foto_ktp_note) ? $detail->foto_ktp_note : 'Pastikan keseluruhan KTP terlihat jelas'));
	    	$item['foto_rekening'] = asset('').$detail->foto_rekening;
	    	$item['foto_rekening_status'] = (!empty($detail->foto_rekening_status) ? $detail->foto_rekening_status : 'Pastikan keseluruhan buku rekening terlihat jelas');
	    	$item['foto_rekening_note'] = ($detail->foto_rekening_status == 'Approved' ? '' : (!empty($detail->foto_rekening_note) ? $detail->foto_rekening_note : 'Pastikan keseluruhan buku rekening terlihat jelas'));
	    	$item['foto_selfie'] = asset('').$detail->foto_selfie;
	    	$item['foto_selfie_status'] = (!empty($detail->foto_selfie_status) ? $detail->foto_selfie_status : 'Perlihatkan wajah dan KTP saat foto');
	    	$item['foto_selfie_note'] = ($detail->foto_selfie_status == 'Approved' ? '' : (!empty($detail->foto_selfie_note) ? $detail->foto_selfie_note : 'Perlihatkan wajah dan KTP saat foto'));

	    	$response['api_status']  = 1;
	        $response['api_message'] = 'Sudah ada request premium';
	        $response['type_dialog']  = 'Informasi';
	        $response['item'] = $item;
	    } else {
	    	$item['foto_ktp'] = '';
	    	$item['foto_ktp_status'] = '';
	    	$item['foto_ktp_note'] = 'Pastikan keseluruhan KTP terlihat jelas';
	    	$item['foto_rekening'] = '';
	    	$item['foto_rekening_status'] = '';
	    	$item['foto_rekening_note'] = 'Pastikan keseluruhan buku rekening terlihat jelas';
	    	$item['foto_selfie'] = '';
	    	$item['foto_selfie_status'] = '';
	    	$item['foto_selfie_note'] = 'Perlihatkan wajah dan KTP saat foto';

	    	$response['api_status']  = 0;
	        $response['api_message'] = 'Belum ada request premium';
	        $response['type_dialog']  = 'Informasi';
	        $response['item'] = $item;
		}
	    return response()->json($response);

	}

	public function postSubmitPremium() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$ktp = Request::get('ktp');
		$buku_rekening = Request::get('buku_rekening');
		$selfie = Request::get('selfie');

		$storage = storage_path("app/uploads/verifikasi_premium/");
		
		if(!empty($ktp)) {
			$ktp = base64_decode($ktp);
			$filename_ktp  = 'ktp-'.date('ymdhis').'-'.$id_agen.".jpg";
	        if(file_put_contents(($storage.$filename_ktp), $ktp)) {
	        	$foto_ktp = 'uploads/verifikasi_premium/'.$filename_ktp;
	        	$foto_ktp_status = 'Submitted';
	        	$foto_ktp_note = 'Menunggu verifikasi';
	        	$foto_ktp_last_submit = date('Y-m-d H:i:s');
			}
		}
		
		if(!empty($buku_rekening)) {
			$buku_rekening = base64_decode($buku_rekening);
			$filename_buku_rekening  = 'buku_rekening-'.date('ymdhis').'-'.$id_agen.".jpg";
	        if(file_put_contents(($storage.$filename_buku_rekening), $buku_rekening)) {
	        	$foto_rekening = 'uploads/verifikasi_premium/'.$filename_buku_rekening;
	        	$foto_rekening_status = 'Submitted';
	        	$foto_rekening_note = 'Menunggu verifikasi';
	        	$foto_rekening_last_submit = date('Y-m-d H:i:s');
			}
		}

		if(!empty($selfie)) {
			$selfie = base64_decode($selfie);
			$filename_selfie  = 'selfie-'.date('ymdhis').'-'.$id_agen.".jpg";
	        if(file_put_contents(($storage.$filename_selfie), $selfie)) {
	        	$foto_selfie = 'uploads/verifikasi_premium/'.$filename_selfie;
	        	$foto_selfie_status = 'Submitted';
	        	$foto_selfie_note = 'Menunggu verifikasi';
	        	$foto_selfie_last_submit = date('Y-m-d H:i:s');
			}
		}

		$check = DB::table('agen_verify_premium')
	        ->where('id_agen',$id_agen)
	        ->first();

	    $created_user = Esta::user($id_agen);
	    $updated_user = Esta::user($id_agen);
	    $up_agn['updated_at'] = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');
	    if(!empty($check)) {	
			$update = DB::statement('exec updateAgenVerifyPremium ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
							array($check->id,$foto_ktp,$foto_ktp_status,$foto_ktp_note,$foto_ktp_last_submit,$id_agen,$created_user,$updated_user,
									$foto_rekening,$foto_rekening_status,$foto_rekening_note,$foto_rekening_last_submit,$foto_selfie,$foto_selfie_status,
									$foto_selfie_note,$foto_selfie_last_submit,$updated_at));   	
	    } else {
	    	$up['created_at'] = date('Y-m-d H:i:s');
			$created_at = date('Y-m-d H:i:s');
			$update = DB::statement('exec postAgenVerifyPremium ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
							array($foto_ktp,$foto_ktp_status,$foto_ktp_note,$foto_ktp_last_submit,$id_agen,$created_user,$updated_user,
							$foto_rekening,$foto_rekening_status,$foto_rekening_note,$foto_rekening_last_submit,$foto_selfie,$foto_selfie_status,
							$foto_selfie_note,$foto_selfie_last_submit,$created_at)); 
	    }

		/*$up_agn['updated_user'] = Esta::user($id_agen);
	    $up_agen = DB::table('agen')
	    	->where('id',$id_agen)
	    	->update($up_agn);*/


		if($update){
			Esta::log_money_old($id_agen,0,date('Y-m-d H:i:s'),'Agen Update Request Premium','Agen Update Request Premium ','','Riwayat Agen','','');
    		$response['api_status']  = 1;
        	$response['api_message'] = 'Kami akan melakukan verifikasi akun anda terlebih dahulu dalam  1x24 jam';
        	$response['type_dialog']  = 'Informasi';
		} else {
			$response['api_status']  = 0;
	        $response['api_message'] = 'Gagal request premium';
	        $response['type_dialog']  = 'Error';
		}
	    return response()->json($response);
	}

	public function postTopupTutorial() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$banks = DB::select('exec getBank');
		$backend_url = CRUDBooster::getsetting('backend_url');

		$rest_topup = array();
	  	foreach($banks as $bank) {
	  		$layanans = DB::table('tu_tutorial')
	  			->where('id_bank',$bank->id)
				->whereNull('deleted_at')
				->get();

			$va = DB::table('agen_va')
	  			->where('id_bank',$bank->id)
	  			->where('id_agen',$id_agen)
				->first();
				
			$detail = DB::select('exec getAgenById ?', array($id_agen))[0];

			$rest_layanan = array();
		  	foreach($layanans as $layanan) {
		  		$rest2['id'] = $layanan->id;
		  		$rest2['layanan'] = $layanan->layanan;
		  		$rest2['description'] = str_replace('[no_hp]', $detail->no_hp, $layanan->description);
		  		array_push($rest_layanan, $rest2);
		  	}

			if(!empty($rest_layanan)){
				$rest['id'] = $bank->id;
				$rest['image'] = asset('').$bank->image;
				$rest['layanan'] = $rest_layanan;
				array_push($rest_topup, $rest);
			}
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_topup;

	  	return response()->json($response);
	}

	public function postTopupTutorialMerchant() {
		//exit();
		$agen = Auth::user();
        $id_agen = $agen->id;
		$arrays = DB::select('exec getMerchant');

		$rest_topup = array();
	  	foreach($arrays as $array) {
	  		$va = DB::table('agen_va')
	  			->where('merchant',$array->group)
	  			->where('id_agen',$id_agen)
				->first();
			$detail = DB::select('exec getAgenById ?', array($id_agen))[0];

			$rest['id'] = $array->id;
			$rest['image'] = asset('').$array->image;
			$rest['layanan'] = $array->nama;
			$rest['kode'] = $array->kode;
			$rest['no_va'] = $va->no_va;
			$rest['biaya_admin'] = $array->biaya_admin;
			$rest['biaya_admin_bank'] = $array->biaya_admin_bank;
			$rest['deskripsi'] = str_replace('[no_hp]', $detail->no_hp, $array->deskripsi);
			array_push($rest_topup, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_topup;

	  	return response()->json($response);
	}

	public function postUpdatePhotoProfile() {
		$agen = Auth::user();
        $id_agen = $agen->id;

		$storage = storage_path("app/uploads/profile_agen/");
		$image = Request::input('photo');
		$image = base64_decode($image);
		$file  = 'profile-'.date('ymdhis').'-'.$id_agen.".jpg";
        if(file_put_contents(($storage.$file), $image)) {
        	$photo = 'uploads/profile_agen/'.$file;
            $updated_user = Esta::user($id_agen);
			$update = DB::statement('exec postUpdatePhotoProfile ?,?,?', array($id_agen, $photo, $updated_user));

			if($update){
	    		$response['api_status']  = 1;
	        	$response['api_message'] = 'Update photo berhasil';
	        	$response['type_dialog']  = 'Informasi';
	        	$response['photo'] = asset('').$photo;
			} else {
				$response['api_status']  = 0;
		        $response['api_message'] = 'Update photo gagal';
		        $response['type_dialog']  = 'Error';
			}
		}

        return response()->json($response);
	}

	public function postChangePassword() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$old_pass = Request::get('old_pass');
		$new_pass = Request::get('new_pass');

		$detail = DB::table('agen')
			->where('id',$id_agen)
			->where('kode','!=',NULL)
			->first();

		if(hash::check($old_pass, $detail->password)){
			$password = Hash::make($new_pass);
			$updated_at = date('Y-m-d H:i:s');
			$updated_user = Esta::user($id_agen);
			/*$update = DB::table('agen')
				->where('id',$id_agen)
				->update($up);*/
			$update = DB::statement('exec postChangePassword ?,?,?,?', array($id_agen,$password,$updated_at,$updated_user));
	    	if($update) {
	    		$response['api_status']  = 1;
	        	$response['api_message'] = 'Kata sandi Anda berhasil diupdate';
	        	$response['type_dialog']  = 'Informasi';
	    	} else {
	    		$response['api_status']  = 2;
	        	$response['api_message'] = 'Kata sandi Anda gagal diupdate';
	        	$response['type_dialog']  = 'Error';
	    	}
		} else {
			$response['api_status']  = 0;
	        $response['api_message'] = 'Kata sandi lama Anda salah';
	        $response['type_dialog']  = 'Error';
		}

        return response()->json($response);
	}
	
	public function postEditProfile() {
		//UPDATE BY INDOCYBER
		//FOR PREVENT UPDATE ON HACKING DAY
		/*$response['api_status']  = 0;
		$response['api_message'] = 'Mohon Maaf, Saat ini proses update profile sedang dalam masa maintenance.';
		$response['type_dialog']  = 'Error';
		return response()->json($response);*/
		//--------------------------------
		
		$agen = Auth::user();
        $id_agen = $agen->id;
		$nama = Request::get('nama');
		$no_hp_new = Request::get('no_hp');
		$email = Request::get('email');
        $updated_user = Esta::user($id_agen);

		$cek_hp = DB::table('agen')
			->where('no_hp',$no_hp_new)
			->where('id','!=',$id_agen)
			->where('kode','!=',NULL)
			->first();

		if(!empty($cek_hp)) {
			$response['api_status']  = 2;
	        $response['api_message'] = 'No HP sudah terdaftar';
	        $response['type_dialog']  = 'Error';
        	return response()->json($response);
		exit();
		}

		/*$update = DB::table('agen')
			->where('id',$id_agen)
			->update($sv);*/
		$update = DB::statement('exec postEditProfile ?,?,?,?', array($id_agen,$nama,$email,$updated_user));
    	if($update) {
    		$detail = DB::table('agen')->where('id',$id_agen)->first();
    		if($no_hp_new != $detail->no_hp) {
    			$kode_otp = rand(11,99).date('s');
    			$msg = str_replace('[kode]', $kode_otp, CRUDBooster::getsetting('otp_registrasi'));
    			Esta::send_sms($no_hp_new,$msg);
    			$up['updated_at'] = date('Y-m-d H:i:s');
				//$up['tgl_otp_terkirim'] = date('Y-m-d H:i:s');
				$up['kode_otp'] = $kode_otp;

				$update = DB::table('agen')
					->where('id',$id_agen)
					->update($up);

    			$item['verifikasi_otp'] = 'Yes';
    			$item['kode_otp'] = $kode_otp;
    			$item['no_hp_baru'] = $no_hp_new;
    		} else {
    			$item['verifikasi_otp'] = 'No';
    			$item['kode_otp'] = 0;
    			$item['no_hp_baru'] = 0;
		    }
	    		$item['nama'] = Request::get('nama');
	    		$item['email'] = Request::get('email');
	    		$item['no_hp'] = Request::get('no_hp');
	    		$item['kode_referall_agen'] = $detail->kode_referall_agen;
	    		$item['status_agen'] = $detail->status_agen;
	    		$item['photo'] = asset('').$detail->photo;;

	    		$response['api_status']  = 1;
		        $response['api_message'] = 'Edit profile berhasil';
		        $response['type_dialog']  = 'Informasi';
		        $response['item'] = $item;
    	} else {
	    	$response['api_status']  = 0;
	        $response['api_message'] = 'Gagal';
	        $response['type_dialog']  = 'Error';
	    }

        return response()->json($response);
		
	}
	
	public function postRegistrasi() {
		$id_agen = Request::get('id_agen');
		$temp = DB::table('agen_temp')
			->where('id',$id_agen)
			->first();

		$cek_no_hp = DB::table('agen')
			->where('no_hp',Request::get('no_hp'))
			->first();
			//dd($cek_no_hp);
		if(!empty($cek_no_hp->id)) {
			$response['api_status']  = 0;
	        $response['api_message'] = 'No HP sudah terdaftar';
	        $response['type_dialog']  = 'Error';
        	return response()->json($response);
	    }


		$tgl_register = $temp->tgl_register;
		$tgl_otp_terkirim = $temp->tgl_otp_terkirim;
		$tgl_verifikasi_otp = $temp->tgl_verifikasi_otp;
		$kode_otp = $temp->kode_otp;
		$no_hp = $temp->no_hp;
		$created_at = date('Y-m-d H:i:s');

		$nama = Request::get('nama');
		$no_hp = Request::get('no_hp');
		$email = Request::get('email');
		$photo = 'uploads/profile_agen/avatar.jpg';
		$kode_relation_referall = Request::get('kode_relation_referall');
		$password = Hash::make(Request::get('password'));

		$kode = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
		/*echo $kode;
		exit();*/
		$kode = $kode;
		$kode_referall_agen = Esta::kode_referall();
		$status_agen = 'Basic';
		$status_aktif = 'Aktif';
		$status_verifikasi = 'Pending';
		$notif_email = 'Yes';
		$created_user = $nama;
        $updated_user = '';

		if(!empty($kode_relation_referall)) {
			$check_referall = DB::table('agen')->where('kode_referall_agen',$kode_relation_referall)->first();
			$setting_ref = DB::table('voucher_referall')->whereNull('deleted_at')->where('aktif','Yes')->first();

			if($check_referall->id >= 1) {
				
			} else {
				$response['api_status']  = 2;
			    $response['api_message'] = 'Kode referal yang anda pakai tidak terdaftar';
			    $response['type_dialog']  = 'Error';
			    return response()->json($response);
			    exit();
			}
		}

		/*$update = DB::table('agen')
			//->where('id',$id_agen)
			->insertGetId($sv);*/
		$update = DB::select('exec postRegistrasiBasic ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
					array($tgl_register,$tgl_otp_terkirim,$tgl_verifikasi_otp,$kode_otp,$no_hp,$created_at,$nama,$email,$photo,$kode_relation_referall
							,$password,$kode,$kode_referall_agen,$status_agen,$status_aktif,$status_verifikasi,$notif_email,$created_user,$updated_user));
    	if($update) {
    		$id_agen = $update[0]->id;
    		/*kode agen*/
			/*$cek = DB::table('agen')
				->where('kode',$sv['kode'])
				->get();
			$kode1 = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
			$sv2['kode'] = $kode1;
			if(!empty($cek)) {
				$update_lagi = DB::table('agen')
					->where('id',$id_agen)
					->update($sv2);
			}

			$cek2 = DB::table('agen')
				->where('kode',$kode1)
				->get();
			if(!empty($cek2)) {
				$kode2 = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
				$sv3['kode'] = $kode2;
				$update_lagi2 = DB::table('agen')
					->where('id',$id_agen)
					->update($sv3);
			}*/
			/*kode agen*/

    		$banks = DB::table('bank')
    			->where('prefix_va','!=',NULL)
    			->whereNull('deleted_at')
    			->get();
    		foreach($banks as $bank) {
    			$flag_genva = $bank->flag_genva;

    			$c['created_at'] = date('Y-m-d H:i:s');
    			$c['updated_at'] = date('Y-m-d H:i:s');
    			$c['created_user'] = Esta::user($id_agen);
    			$c['updated_user'] = Esta::user($id_agen);
    			$c['id_agen'] = $id_agen;
    			$c['id_bank'] = $bank->id;
    			if($flag_genva == 0) {
	    			$c['no_va'] = $bank->prefix_va.$no_hp;
	    		} else {
	    			$c['no_va'] = $bank->prefix_va.str_replace(CRUDBooster::getsetting('kode_agen'), '', $kode);
	    		}

    			$in = DB::table('agen_va')
    				->insert($c);
    		}

			$merchants = DB::table('merchant')
    			->whereNull('deleted_at')
    			//->groupby('group')
    			->where('kode','Alfamart')
    			->get();
    		foreach($merchants as $merchant) {
    			$cm['created_at'] = date('Y-m-d H:i:s');
    			$cm['updated_at'] = date('Y-m-d H:i:s');
    			$cm['created_user'] = Esta::user($id_agen);
    			$cm['updated_user'] = Esta::user($id_agen);
    			$cm['id_agen'] = $id_agen;
    			$cm['merchant'] = $merchant->group;
	    		$cm['no_va'] = $merchant->prefix_va.$no_hp;

    			$in = DB::table('agen_va')
    				->insert($cm);
    		}    		

    		Esta::log_money_old($id_agen,0,date('Y-m-d H:i:s'),'Agen Registrasi','Agen Registrasi','','Riwayat Agen','','');

    		if($check_referall->id >= 1) {
	    		$detail_voucher_referall = DB::table('voucher_referall')->whereNull('deleted_at')->first();
	    		$voucher_referall_aktif = $detail_voucher_referall->aktif;
	    		if($voucher_referall_aktif == 'Yes') {
		    		$voucher_pemakai = $detail_voucher_referall->referall_pemakai;
		    		$voucher_dipakai = $detail_voucher_referall->referall_dipakai;

					$detail_voucher_pemakai = DB::table('voucher')->where('id',$voucher_pemakai)->first();
					$detail_voucher_dipakai = DB::table('voucher')->where('id',$voucher_dipakai)->first();

					$save_child['created_at']       = date('Y-m-d H:i:s');
					$save_child['created_user'] = Esta::user($id_agen);
					$save_child['updated_user'] = Esta::user($id_agen);
					$save_child['id_agen']          = $check_referall->id;
					$save_child['id_voucher']       = $voucher_dipakai;
					$save_child['product']          = $detail_voucher_dipakai->product;
					$save_child['voucher_nama']          = $detail_voucher_dipakai->nama;
					$save_child['voucher_expired']          = $detail_voucher_dipakai->expired_date;
					$save_child['voucher_amount']          = $detail_voucher_dipakai->amount;
					$save_child['voucher_image']          = $detail_voucher_dipakai->image;
					$save_child['voucher_product']          = $detail_voucher_dipakai->product;
					$save_child['voucher_description']          = $detail_voucher_dipakai->description;
					$save_child['id_trans_voucher'] = '';
					$save_child['used']             = 'No';
					DB::table('trans_voucher_child')->insert($save_child);

					$save_notif['created_at'] = $detail_voucher_dipakai->expired_date;
					$save_notif['created_user'] = Esta::user($id_agen);
					$save_notif['updated_user'] = Esta::user($id_agen);
					$save_notif['title'] = $detail_voucher_dipakai->nama;
					$save_notif['description'] = $detail_voucher_dipakai->description;
					$save_notif['description_short'] = $detail_voucher_dipakai->description;
					$save_notif['image'] = $detail_voucher_dipakai->image;
					$save_notif['id_agen'] = $check_referall->id;
					$save_notif['read'] = 'No';
					$save_notif['flag'] = 'Voucher';
					$save_notif['syarat_ketentuan'] = $detail_voucher_dipakai->syarat_ketentuan;
					$save_notif['id_voucher'] = $voucher_dipakai;
					DB::table('notification')->insert($save_notif);

					$save_child2['created_at']       = date('Y-m-d H:i:s');
					$save_child2['created_user'] = Esta::user($id_agen);
					$save_child2['updated_user'] = Esta::user($id_agen);
					$save_child2['id_agen']          = $id_agen;
					$save_child2['id_voucher']       = $voucher_pemakai;
					$save_child2['product']          = $detail_voucher_pemakai->product;
					$save_child2['voucher_nama']          = $detail_voucher_pemakai->nama;
					$save_child2['voucher_expired']          = $detail_voucher_pemakai->expired_date;
					$save_child2['voucher_amount']          = $detail_voucher_pemakai->amount;
					$save_child2['voucher_image']          = $detail_voucher_pemakai->image;
					$save_child2['voucher_product']          = $detail_voucher_pemakai->product;
					$save_child2['voucher_description']          = $detail_voucher_pemakai->description;
					$save_child2['id_trans_voucher'] = '';
					$save_child2['used']             = 'No';
					DB::table('trans_voucher_child')->insert($save_child2);

					$save_notif2['created_at'] = $detail_voucher_pemakai->expired_date;
					$save_notif2['created_user'] = Esta::user($id_agen);
					$save_notif2['updated_user'] = Esta::user($id_agen);
					$save_notif2['title'] = $detail_voucher_pemakai->nama;
					$save_notif2['description'] = $detail_voucher_pemakai->description;
					$save_notif2['description_short'] = $detail_voucher_pemakai->description;
					$save_notif2['image'] = $detail_voucher_pemakai->image;
					$save_notif2['id_agen'] = $id_agen;
					$save_notif2['read'] = 'No';
					$save_notif2['flag'] = 'Voucher';
					$save_notif2['syarat_ketentuan'] = $detail_voucher_pemakai->syarat_ketentuan;
					$save_notif2['id_voucher'] = $voucher_pemakai;
					DB::table('notification')->insert($save_notif2);

					if($check_referall->regid != NULL) {
						$datafcm['title'] = CRUDBooster::getsetting('notification_voucher_header');
						$datafcm['content'] = CRUDBooster::getsetting('notification_voucher_referall');
						$regid[] = $check_referall->regid;
						Esta::sendFCM($regid,$datafcm);
					}
				}
			}

    		$item['nama'] = Request::get('nama');
    		$item['no_hp'] = Request::get('no_hp');
    		$item['kode_referall_agen'] = $kode_referall_agen;
    		$item['status_agen'] = $status_agen;
    		$item['photo'] = asset('').'uploads/profile_agen/avatar.jpg';

    		$response['api_status']  = 1;
	        $response['api_message'] = 'Registrasi berhasil';
	        $response['type_dialog']  = 'Informasi';
	        $response['item'] = $item;
    	} else {
	    	$response['api_status']  = 0;
	        $response['api_message'] = 'Gagal';
	        $response['type_dialog']  = 'Error';
	    }

        return response()->json($response);
	}
	
	public function postRegistrasiOtp() {
		$no_hp = Request::get('no_hp');
		$kode_otp = rand(11,99).date('s');


		$check = DB::table('agen')
			->where('no_hp',$no_hp)
			->where('kode','!=',NULL)
			->where('deleted_at',NULL)
			->first();

		if(!empty($check)) {
			$response['api_status']  = 0;
        	$response['api_message'] = 'Nomor HP sudah terdaftar';
        	$response['type_dialog']  = 'Error';
		} else {
			$no_hp = $no_hp;
			$created_at = date('Y-m-d H:i:s');
			$created_user = 'SYSTEM';
			$updated_user = '';
			$tgl_register = date('Y-m-d H:i:s');
			$tgl_otp_terkirim = date('Y-m-d H:i:s');
			$kode_otp = $kode_otp;

			/*$save = DB::table('agen_temp')
				->insertGetId($sv);*/

			$save = DB::select('exec postRegistrasiOtp ?,?,?,?,?,?,?', 
							array($no_hp,$created_at, $created_user, $updated_user, $tgl_register, $tgl_otp_terkirim, $kode_otp));

			if($save) {
				$kode = $kode_otp;
				//$msg = 'EstaKios - '.$kode;
				$msg = str_replace('[kode]', $kode, CRUDBooster::getsetting('otp_registrasi'));
				 Esta::send_sms($no_hp, $msg);

				$response['api_status']  = 1;
	        	$response['api_message'] = 'Registrasi no HP berhasil';
	        	$response['type_dialog']  = 'Informasi';
	        	$response['kode_otp'] = $kode_otp;
	        	$response['id_agen'] = $save[0]->id;
	        	$response['timer'] = CRUDBooster::getsetting('timer_resend_otp_detik');
	        	$response['resend'] = CRUDBooster::getsetting('maksimal_resend_otp');
			} else {
				$response['api_status']  = 2;
	        	$response['api_message'] = 'Gagal simpan no HP';
	        	$response['type_dialog']  = 'Error';
	        }
		}

        return response()->json($response);
	}

	public function postForgotPassOtp() {
		$no_hp = Request::get('no_hp');
		$kode_otp = rand(11,99).date('s');

		$check = DB::table('agen')
			->where('no_hp',$no_hp)
			->where('kode','!=',NULL)
			->first();

		if($check->regid != NULL && $check->is_existing == 0) {
			$response['api_status']  = 99;
		    $response['api_message'] = CRUDBooster::getsetting('pesan_multi_login');
		    $response['type_dialog']  = 'Error';
		    Esta::send_sms($check_hp->no_hp, $response['api_message']);
	  		return response()->json($response);
	  		exit();
	    }


		if(empty($check)) {
			$response['api_status']  = 0;
        	$response['api_message'] = 'Nomor HP tidak terdaftar';
        	$response['type_dialog']  = 'Error';
		} else {
			$updated_at = date('Y-m-d H:i:s');
			//$tgl_otp_terkirim = date('Y-m-d H:i:s');
			$kode_otp = $kode_otp;
			$created_user = Esta::user($check->id);
			$updated_user = Esta::user($check->id);

			/*$update = DB::table('agen')
				->where('id',$check->id)
				->update($sv);*/

			$update = DB::statement('exec postForgotPassOtp ?,?,?,?,?', 
							array($check->id, $updated_at, $kode_otp, $created_user, $updated_user));

			if($update) {
				$kode = $kode_otp;
				//$msg = 'EstaKios - '.$kode;
				$msg = str_replace('[kode]', $kode, CRUDBooster::getsetting('otp_registrasi'));
				Esta::send_sms($no_hp, $msg);

				$response['api_status']  = 1;
	        	$response['api_message'] = 'OTP berhasil dikirim';
	        	$response['type_dialog']  = 'Informasi';
	        	$response['kode_otp'] = $kode_otp;
	        	$response['id_agen'] = $check->id;
	        	$response['timer'] = CRUDBooster::getsetting('timer_resend_otp_detik');
	        	$response['resend'] = CRUDBooster::getsetting('maksimal_resend_otp');
			} else {
				$response['api_status']  = 2;
	        	$response['api_message'] = 'OTP gagal';
	        	$response['type_dialog']  = 'Error';
	        }
		}

        return response()->json($response);
	}

	public function postChangePassOtp() {
		$agen = Auth::user();
        $id = $agen->id;
		$kode_otp = rand(11,99).date('s');

		$check = DB::table('agen')
			->where('id',$id)
			//->where('kode','!=',NULL)
			->first();

		if(empty($check)) {
			$response['api_status']  = 0;
        	$response['api_message'] = 'Nomor HP tidak terdaftar';
        	$response['type_dialog']  = 'Error';
		} else {
			$updated_at = date('Y-m-d H:i:s');
			//$sv['tgl_otp_terkirim'] = date('Y-m-d H:i:s');
			$kode_otp = $kode_otp;
			$created_user = Esta::user($check->id);
			$updated_user = Esta::user($check->id);

			/*$update = DB::table('agen')
				->where('id',$check->id)
				->update($sv);*/

			$update = DB::statement('exec postChangePassOtp ?,?,?,?,?', 
							array($check->id, $updated_at, $kode_otp, $created_user, $updated_user));

			if($update) {
				$kode = $kode_otp;
				//$msg = 'EstaKios - '.$kode;
				$msg = str_replace('[kode]', $kode, CRUDBooster::getsetting('otp_registrasi'));
				Esta::send_sms($check->no_hp, $msg);

				$response['api_status']  = 1;
	        	$response['api_message'] = 'OTP berhasil dikirim';
	        	$response['type_dialog']  = 'Informasi';
	        	$response['kode_otp'] = $kode_otp;
	        	$response['id_agen'] = $check->id;
	        	$response['timer'] = CRUDBooster::getsetting('timer_resend_otp_detik');
	        	$response['resend'] = CRUDBooster::getsetting('maksimal_resend_otp');
			} else {
				$response['api_status']  = 2;
	        	$response['api_message'] = 'OTP gagal';
	        	$response['type_dialog']  = 'Error';
	        }
		}

        return response()->json($response);
	}

	public function postForgotChangePassword() {
		$id_agen = Request::get('id_agen');
		$new_pass = Request::get('new_pass');

		$detail = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$password = Hash::make($new_pass);
		$updated_at = date('Y-m-d H:i:s');
		$created_user = Esta::user($id_agen);
		$updated_user = Esta::user($id_agen);

		/*$update = DB::table('agen')
			->where('id',$id_agen)
			->update($up);*/

		$update = DB::statement('exec postForgotChangePassword ?,?,?,?,?', 
						array($id_agen,$password,$updated_at,$created_user,$updated_user));

    	if($update) {
    		$item['id_agen'] = $detail->id;
			$item['nama'] = $detail->nama;
    		$item['no_hp'] = $detail->no_hp;
    		$item['email'] = ($detail->email == '' ? '-' : $detail->email);
    		$item['kode_referall_agen'] = $detail->kode_referall_agen;
    		$item['status_agen'] = $detail->status_agen;
    		$item['saldo'] = ($detail->saldo <= 0 ? 0 : $detail->saldo);
    		$item['photo'] = asset('').$detail->photo;

    		$response['api_status']  = 1;
        	$response['api_message'] = 'Password updated';
        	$response['type_dialog']  = 'Informasi';
        	$response['item'] = $item;
    	} else {
    		$response['api_status']  = 2;
        	$response['api_message'] = 'Password update gagal';
        	$response['type_dialog']  = 'Error';
    	}

        return response()->json($response);
	}

	public function postLogout() {
		/*$agen = Auth::user();
        $id_agen = $agen->id;*/
        $no_hp = Request::get('no_hp');

		$regid = NULL;

		/*$update = DB::table('agen')
			->where('id',$id_agen)
			->update($up);*/

		$update = DB::statement('exec postLogout ?', array($no_hp));

		$response['api_status']  = 1;
		$response['api_message'] = 'Logout berhasil';
		$response['type_dialog']  = 'Informasi';

		return response()->json($response);
	}

	public function postVerifikasiOtp() {
		$type = Request::get('type');
		$no_hp_baru = Request::get('no_hp_baru');
		$kode_otp = Request::get('kode_otp');
		$id_agen = Request::get('id_agen');

		if($type == 'update') {
			$sv['no_hp'] = $no_hp_baru;
		}
		$sv['tgl_verifikasi_otp'] = date('Y-m-d H:i:s');
		$sv['updated_at'] = date('Y-m-d H:i:s');
		$sv['created_user'] = Esta::user($id_agen);
		$sv['updated_user'] = Esta::user($id_agen);

		/*if($type == 'update') {
			$no_hp = $no_hp_baru;
		}
		$tgl_verifikasi_otp = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');
		$created_user = Esta::user($id_agen);
		$updated_user = Esta::user($id_agen);*/

		if($type == 'update') {
			$update = DB::table('agen')
				->where('id',$id_agen)
				->where('kode_otp',$kode_otp)
				->update($sv);
		} else {
			$update = DB::table('agen_temp')
				->where('id',$id_agen)
				->where('kode_otp',$kode_otp)
				->update($sv);
		}
    	if($update) {
    		$response['api_status']  = 1;
	        $response['api_message'] = 'Sukses update tgl verifikasi';
	        $response['type_dialog']  = 'Informasi';
	        $response['kode_otp'] = $kode_otp;
	        $response['id_agen'] = $id_agen;
    	} else {
    		$update = DB::table('agen')
				->where('id',$id_agen)
				->where('kode_otp',$kode_otp)
				->update($sv);
			if($update) {
				$response['api_status']  = 1;
		        $response['api_message'] = 'Sukses update tgl verifikasi';
		        $response['type_dialog']  = 'Informasi';
		        $response['kode_otp'] = $kode_otp;
		        $response['id_agen'] = $id_agen;
	    	} else {
		    	$response['api_status']  = 0;
		        $response['api_message'] = 'Verifikasi OTP gagal. Kode OTP yang Anda gunakan sudah tidak valid';
		        $response['type_dialog']  = 'Error';
		    }
	    }

        return response()->json($response);
	}

	public function postChangeNotifEmail() {
		$status = Request::get('status');
		$agen = Auth::user();
        $id_agen = $agen->id;

		$notif_email = $status;
		$updated_at = date('Y-m-d H:i:s');
		$updated_user = Esta::user($id_agen);

		/*$update = DB::table('agen')
			->where('id',$id_agen)
			->update($sv);*/

		$update = DB::statement('exec postChangeNotifEmail ?,?,?,?', array($id_agen,$notif_email,$updated_at,$updated_user));

    	if($update) {
    		$response['api_status']  = 1;
	        $response['api_message'] = 'Update notifikasi berhasil';
	        $response['type_dialog']  = 'Informasi';
    	} else {
	    	$response['api_status']  = 0;
	        $response['api_message'] = 'Update notifikasi gagal';
	        $response['type_dialog']  = 'Error';
	    }

        return response()->json($response);
	}

	public function postSetting() {
		/* $agen = Auth::user();
        $id_agen = $agen->id; */
		$id_agen = Request::get('id_agen');
		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		$item['notif_email'] = ($detail_agen->notif_email == 'No' ? 'No' : 'Yes');
		/* $serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/accountBalance';
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		$accountNumber = $detail_agen->no_hp;
		// $referenceNumber = Request::get('referenceNumber');

		$prefixActivation = CRUDBooster::getsetting('prefix_reference_inquiryBalance');
			$keterangan = 'inquiryBalance';
			$referenceNumber = DB::select('exec CreateRefNumberSobatku ?,?', array($prefixActivation, $keterangan));
			// dd($referenceNumber[0]->Prefix);

		$request = [
			'accountNumber' => $accountNumber,
			'referenceNumber' => $referenceNumber[0]->Prefix,
			'user' => $user,
			'hashCode' => hash('sha256', $accountNumber.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
		];

		$data_toJson = json_encode( $request );
		$ch = curl_init( $serviceURL );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept:application/json'
			)
		);
		$responseSobatku = curl_exec( $ch );
		$responseBody = json_decode( $responseSobatku, true);
		$responseBody['balance'] = str_replace(',', '', $responseBody['balance']);
		$responseBody['balance'] = str_replace('.', '', $responseBody['balance']);
		//dd($balance);
		$responseBody['balance']=(double)$responseBody['balance'];
		curl_close($ch);

		$item['saldo'] = $responseBody['balance']; */
		$item['saldo'] = ($detail_agen->saldo <= 0 ? 0 : $detail_agen->saldo);
		//UPDATED BY INDOCYBER
        //18 APRIL 2019
        //DISPLAY STATUS AGEN
        //===================================================================================================================
		if($detail_agen->is_agen_kios == true){
			$item['status_agen'] = 'Agen Kios';
		}else{
			$item['status_agen'] = $detail_agen->status_agen;
		}
		//$item['status_agen'] = $detail_agen->status_agen;
		//===================================================================================================================
		$item['versi_aplikasi'] = CRUDBooster::getsetting('versi_aplikasi');
		$item['share_referal'] = str_replace('[kode]', $detail_agen->kode_referall_agen, CRUDBooster::getsetting('share_referal'));
		$item['link_play_store'] = CRUDBooster::getsetting('link_play_store');
		$item['share_struk'] = CRUDBooster::getsetting('share_struk');
		$item['paksa_update'] = CRUDBooster::getsetting('paksa_update');
		$item['maintenance_pulsa'] = CRUDBooster::getsetting('maintenance_pulsa');
		$item['maintenance_pln'] = CRUDBooster::getsetting('maintenance_pln');
		$item['maintenance_pdam'] = CRUDBooster::getsetting('maintenance_pdam');
		$item['maintenance_bpjs'] = CRUDBooster::getsetting('maintenance_bpjs');
		$item['maintenance_kios'] = CRUDBooster::getsetting('maintenance_kios');
		$item['maintenance_alfa'] = CRUDBooster::getsetting('maintenance_alfa');
		$item['maintenance_alfa_desc'] = CRUDBooster::getsetting('maintenance_alfa_desc');
		$item['minimal_tarik_tunai'] = CRUDBooster::getsetting('minimal_tarik_tunai');
		$item['tarik_tunai_tanpa_biaya_admin'] = CRUDBooster::getsetting('tarik_tunai_tanpa_biaya_admin');
		$item['biaya_admin_tarik_tunai'] = CRUDBooster::getsetting('biaya_admin_tarik_tunai');
		$item['bantuan_cs'] = CRUDBooster::getsetting('bantuan_cs');
		$item['logout'] = ($detail_agen->status_aktif == 'Tidak Aktif' ? 'Yes' : 'No');
		$item['logout_desc'] = CRUDBooster::getsetting('pesan_paksa_logout');
		$item['info_pulsa'] = (CRUDBooster::getsetting('info_pulsa') != '' ? CRUDBooster::getsetting('info_pulsa') : '');
		$item['info_pln'] = (CRUDBooster::getsetting('info_pln') != '' ? CRUDBooster::getsetting('info_pln') : '');
		$item['info_pdam'] = (CRUDBooster::getsetting('info_pdam') != '' ? CRUDBooster::getsetting('info_pdam') : '');
		$item['info_bpjs'] = (CRUDBooster::getsetting('info_bpjs') != '' ? CRUDBooster::getsetting('info_bpjs') : '');
		$item['info_tarik_tunai'] = (CRUDBooster::getsetting('info_tarik_tunai') != '' ? str_replace(array('[param1]','[param2]'), array(number_format(CRUDBooster::getsetting('tarik_tunai_tanpa_biaya_admin'),0,',','.'),number_format(CRUDBooster::getsetting('biaya_admin_tarik_tunai'),0,',','.')),CRUDBooster::getsetting('info_tarik_tunai')) : '');
		if($item['status_agen'] == 'Premium') {
			$item['tt_nama_bank'] = DB::table('m_bank')->where('id',$detail_agen->rek_id_bank)->first()->nama;
			$item['tt_nama_akun'] = $detail_agen->rek_nama;
			$item['tt_no_rek'] = $detail_agen->rek_no;
		} else {
			$item['tt_nama_bank'] = '';
			$item['tt_nama_akun'] = '';
			$item['tt_no_rek'] = '';
		}

		$latitude = Request::get('latitude');
		$longitude = Request::get('longitude');
        $g_alamat = Request::get('alamat');
        $g_provinsi = Request::get('provinsi');
        $g_kabupaten = Request::get('kabupaten');
        $g_kecamatan = Request::get('kecamatan');
        $g_kelurahan = Request::get('kelurahan');
        $g_kodepos = Request::get('kodepos');

		$up = DB::statement('exec postUpdateSetting ?,?,?,?,?,?,?,?,?', 
						array($id_agen,$latitude,$longitude,$g_alamat,$g_provinsi,$g_kabupaten,$g_kecamatan,$g_kelurahan,$g_kodepos));
		
		$response['api_status']  = 1;
	    $response['api_message'] = 'Api setting';
	    $response['type_dialog']  = 'Informasi';
	    $response['item'] = $item;

        return response()->json($response);
	}

	public function postNotification() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$notifs = DB::table('notification')
			->where('id_agen',$id_agen)
			->orderBy('id','desc')
			->get();

		$rest_slider = array();
	  	foreach($notifs as $notif) {
			$rest['id']         = $notif->id;
			$rest['datetime']         = $notif->created_at;
			$rest['description']      = $notif->description;
			$rest['description_short']      = strip_tags(($notif->description_short != '' ? $notif->description_short : ''));
			$rest['title']   = $notif->title;
			$rest['id_voucher']   = $notif->id_voucher;
			$rest['expired_date']   = $notif->expired_date;
			$rest['flag']   = $notif->flag;
			$rest['syarat_ketentuan']   = $notif->syarat_ketentuan;
			$rest['read']   = ($notif->read == 'Yes' ? 'Yes' : 'No');
			$rest['image']      = asset('').$notif->image;
			array_push($rest_slider, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Notif';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_slider;

	  	return response()->json($response);
	}

	public function postReadNotification() {
		$id_notif = Request::get('id_notif');

		$up['read'] = 'Yes';
		$update = DB::table('notification')
			->where('id',$id_notif)
			->update($up);
		if($update) {
			$response['api_status']  = 1;
	   	 	$response['api_message'] = 'Sukses';
	   	 	$response['type_dialog']  = 'Informasi';
		} else {
			$response['api_status']  = 0;
	   	 	$response['api_message'] = 'Gagal';
	   	 	$response['type_dialog']  = 'Error';
		}
		return response()->json($response);
	}
	
	public function postRegistrasiCheckHp() {
		$no_hp = Request::get('no_hp');
		$kode_otp = rand(11,99).date('s');

		$check = DB::table('agen')
			->where('no_hp',$no_hp)
			->where('kode','!=',NULL)
			->where('deleted_at',NULL)
			->first();

		$cek_pengajuan_agen = DB::table('txn_pengajuan_agen')
		      ->where('agen_no_hp',$no_hp)
		      ->whereNull('deleted_at')
		      ->first();

	    if($cek_pengajuan_agen->flag_registrasi == 5) {
	      	$response['api_status']  = 0;
	        $response['api_message'] = 'Pengajuan anda sedang dalam proses';
	        $response['type_dialog']  = 'Error';
	    }elseif(!empty($check) || !empty($cek_pengajuan_agen->id)) {
			$response['api_status']  = 0;
        	$response['api_message'] = 'Nomor HP sudah terdaftar';
        	$response['type_dialog']  = 'Error';
		}else{ /*else {
			$no_hp = $no_hp;
			$created_at = date('Y-m-d H:i:s');
			$created_user = 'SYSTEM';
			$updated_user = '';
			$tgl_register = date('Y-m-d H:i:s');
			$tgl_otp_terkirim = date('Y-m-d H:i:s');
			$kode_otp = $kode_otp;*/

			/*$save = DB::table('agen_temp')
				->insertGetId($sv);*/

			/*$save = DB::select('exec postRegistrasiOtp ?,?,?,?,?,?,?', 
							array($no_hp,$created_at, $created_user, $updated_user, $tgl_register, $tgl_otp_terkirim, $kode_otp));

			if($save) {
				$kode = $kode_otp;
				//$msg = 'EstaKios - '.$kode;
				$msg = str_replace('[kode]', $kode, CRUDBooster::getsetting('otp_registrasi'));
				 Esta::send_sms($no_hp, $msg);

				$response['api_status']  = 1;
	        	$response['api_message'] = 'Registrasi no HP berhasil';
	        	$response['type_dialog']  = 'Informasi';
	        	$response['kode_otp'] = $kode_otp;
	        	$response['id_agen'] = $save[0]->id;
	        	$response['timer'] = CRUDBooster::getsetting('timer_resend_otp_detik');
	        	$response['resend'] = CRUDBooster::getsetting('maksimal_resend_otp');
			} else {
				$response['api_status']  = 2;
	        	$response['api_message'] = 'Gagal simpan no HP';
	        	$response['type_dialog']  = 'Error';
	        }
		}*/
			$response['api_status']  = 1;
	    	$response['api_message'] = 'Registrasi no HP berhasil';
	    	$response['type_dialog']  = 'Informasi';
	    }

        return response()->json($response);
	}
	
	public function postForgotPassOtpNew() {
		$no_hp = Request::get('no_hp');
		$kode_otp = rand(11,99).date('s');

		$check_pengajuan = DB::table('txn_pengajuan_agen')
			->where('agen_no_hp',$no_hp)
			->where('kode_pengajuan_agen','!=',NULL)
			->whereNull('deleted_at')
			->orderBy('id', 'DESC')
			->first();

		$check = DB::table('agen')
			->where('no_hp',$no_hp)
			->where('kode','!=',NULL)
			->whereNull('deleted_at')
			->first();
			
		/* if(!empty($check_pengajuan)){
			if($check_pengajuan->agen_tglpengajuansobatku != NULL && $check_pengajuan->agen_tglverifikasisobatku == NULL &&
			$check_pengajuan->agen_tglaktivasisobatku == NULL){
				$item['id_pengajuan'] = $check_pengajuan->id;
				$response['api_status']  = 0;
			    $response['api_message'] = 'Kami akan melakukan verifikasi akun anda terlebih dahulu dalam 1x24 jam kerja';
			    $response['type_dialog']  = 'Informasi';
			    $response['item'] = $item;
			    return response()->json($response);
			}
		} */

		if($check->regid != NULL && $check->is_existing == 0) {
			$response['api_status']  = 99;
		    $response['api_message'] = CRUDBooster::getsetting('pesan_multi_login');
		    $response['type_dialog']  = 'Error';
		    Esta::send_sms($check_hp->no_hp, $response['api_message']);
	  		return response()->json($response);
	  		exit();
	    }

		if(empty($check) && empty($check_pengajuan)) {
			$response['api_status']  = 0;
        	$response['api_message'] = 'Nomor HP tidak terdaftar';
        	$response['type_dialog']  = 'Error';
			return response()->json($response);
	  		exit();
		}elseif(!empty($check_pengajuan)){
			$sv['updated_at'] = date('Y-m-d H:i:s');
			//$tgl_otp_terkirim = date('Y-m-d H:i:s');
			$sv['agen_kode_otp'] = $kode_otp;
			$sv['created_user'] = $check_pengajuan->agen_nama;
			$sv['updated_user'] = $check_pengajuan->agen_nama;

			$update_pengajuan = DB::table('txn_pengajuan_agen')
				->where('id',$check_pengajuan->id)
				->update($sv);
		}
		if(!empty($check)) {
			$updated_at = date('Y-m-d H:i:s');
			//$tgl_otp_terkirim = date('Y-m-d H:i:s');
			$kode_otp = $kode_otp;
			$created_user = Esta::user($check->id);
			$updated_user = Esta::user($check->id);

			/*$update = DB::table('agen')
				->where('id',$check->id)
				->update($sv);*/

			$update = DB::statement('exec postForgotPassOtp ?,?,?,?,?', 
							array($check->id, $updated_at, $kode_otp, $created_user, $updated_user));

		}

		if($update || $update_pengajuan) {
			$kode = $kode_otp;
			//$msg = 'EstaKios - '.$kode;
			$msg = str_replace('[kode]', $kode, CRUDBooster::getsetting('otp_registrasi'));
			Esta::send_sms($no_hp, $msg);

			$response['api_status']  = 1;
        	$response['api_message'] = 'OTP berhasil dikirim';
        	$response['type_dialog']  = 'Informasi';
        	$response['kode_otp'] = $kode_otp;
        	$response['id_agen'] = $check->id;
			if($response['id_agen'] == null){
				$response['id_agen'] = $check_pengajuan->id;
				
			}
        	$response['timer'] = CRUDBooster::getsetting('timer_resend_otp_detik');
        	$response['resend'] = CRUDBooster::getsetting('maksimal_resend_otp');
		} else {
			$response['api_status']  = 2;
        	$response['api_message'] = 'OTP gagal';
        	$response['type_dialog']  = 'Error';
        }

        return response()->json($response);
	}

	public function postVerifikasiOtpNew() {
		$type = Request::get('type');
		$no_hp_baru = Request::get('no_hp_baru');
		$kode_otp = Request::get('kode_otp');
		$id_agen = Request::get('id_agen');

		$check = DB::table('agen')
			->where('id',$id_agen)
			->where('kode','!=',NULL)
			->whereNull('deleted_at')
			->first();

		$check_pengajuan = DB::table('txn_pengajuan_agen')
			->where('agen_no_hp',$no_hp_baru)
			->where('kode_pengajuan_agen','!=',NULL)
			->whereNull('deleted_at')
			->orderBy('id', 'DESC')->first();

		if($type == 'update') {
			$sv['no_hp'] = $no_hp_baru;
		}
		$sv['tgl_verifikasi_otp'] = date('Y-m-d H:i:s');
		$sv['updated_at'] = date('Y-m-d H:i:s');
		$sv['created_user'] = Esta::user($id_agen);
		$sv['updated_user'] = Esta::user($id_agen);

		/*if($type == 'update') {
			$no_hp = $no_hp_baru;
		}
		$tgl_verifikasi_otp = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');
		$created_user = Esta::user($id_agen);
		$updated_user = Esta::user($id_agen);*/

		if($type == 'update') {
			$update = DB::table('agen')
				->where('id',$id_agen)
				->where('kode_otp',$kode_otp)
				->update($sv);
		} else {
			$update = DB::table('agen_temp')
				->where('id',$id_agen)
				->where('kode_otp',$kode_otp)
				->update($sv);
		}
    	if($update) {
    		$response['api_status']  = 1;
	        $response['api_message'] = 'Sukses update tgl verifikasi';
	        $response['type_dialog']  = 'Informasi';
	        $response['kode_otp'] = $kode_otp;
	        $response['id_agen'] = $id_agen;
    	} else {
    		if(!empty($check_pengajuan)){
    			$sv_pengajuan['agen_tgl_verifikasi_otp'] = date('Y-m-d H:i:s');
				$sv_pengajuan['updated_at'] = date('Y-m-d H:i:s');
				$sv_pengajuan['created_user'] = $check_pengajuan->agen_nama;
				$sv_pengajuan['updated_user'] = $check_pengajuan->agen_nama;
    			$update_pengajuan = DB::table('txn_pengajuan_agen')
					->where('agen_no_hp',$no_hp_baru)
					->whereNull('deleted_at')
					->where('agen_kode_otp',$kode_otp)
					->update($sv_pengajuan);
    		}
    		$update = DB::table('agen')
				->where('id',$id_agen)
				->where('kode_otp',$kode_otp)
				->update($sv);
			if($update || $update_pengajuan) {
				$response['api_status']  = 1;
		        $response['api_message'] = 'Sukses update tgl verifikasi';
		        $response['type_dialog']  = 'Informasi';
		        $response['kode_otp'] = $kode_otp;
		        $response['id_agen'] = $id_agen;
	    	} else {
		    	$response['api_status']  = 0;
		        $response['api_message'] = 'Verifikasi OTP gagal. Kode OTP yang Anda gunakan sudah tidak valid';
		        $response['type_dialog']  = 'Error';
		    }
	    }
        return response()->json($response);
	}

	public function postForgotChangePasswordNew() {
		$id_agen = Request::get('id_agen');
		$new_pass = Request::get('new_pass');
		$no_hp = Request::get('no_hp');
		$kode_otp = Request::get('kode_otp');
		$detail = DB::table('agen')
			->where('id',$id_agen)
			->where('kode_otp',$kode_otp)
			->first();

		$password = Hash::make($new_pass);
		$updated_at = date('Y-m-d H:i:s');
		$created_user = Esta::user($id_agen);
		$updated_user = Esta::user($id_agen);

		/*$update = DB::table('agen')
			->where('id',$id_agen)
			->update($up);*/

		$check_pengajuan = DB::table('txn_pengajuan_agen')
			->where('agen_no_hp',$no_hp)
			->where('kode_pengajuan_agen','!=',NULL)
			->whereNull('deleted_at')
			->orderBy('id', 'DESC')->first();
			

		if(!empty($check_pengajuan)){
			$sv_pengajuan['agen_password'] = Hash::make($new_pass);
			$sv_pengajuan['updated_at'] = date('Y-m-d H:i:s');
			$sv_pengajuan['created_user'] = $check_pengajuan->agen_nama;
			$sv_pengajuan['updated_user'] = $check_pengajuan->agen_nama;
			$update_pengajuan = DB::table('txn_pengajuan_agen')
				->where('agen_no_hp',$no_hp)
				->where('kode_pengajuan_agen','!=',NULL)
				->whereNull('deleted_at')
				->where('agen_kode_otp',$kode_otp)
				->update($sv_pengajuan);
		}
			
		if(!empty($detail)){
			$update = DB::statement('exec postForgotChangePassword ?,?,?,?,?,?', 
						array($id_agen,$password,$updated_at,$created_user,$updated_user,$kode_otp));
		}

    	if($update || $update_pengajuan) {
    		$item['id_agen'] = $detail->id;
			$item['nama'] = $detail->nama;
    		$item['no_hp'] = $detail->no_hp;
    		$item['email'] = ($detail->email == '' ? '-' : $detail->email);
    		$item['kode_referall_agen'] = $detail->kode_referall_agen;
    		$item['status_agen'] = $detail->status_agen;
    		$item['saldo'] = ($detail->saldo <= 0 ? 0 : $detail->saldo);
    		$item['photo'] = asset('').$detail->photo;

    		$response['api_status']  = 1;
        	$response['api_message'] = 'Password updated';
        	$response['type_dialog']  = 'Informasi';
        	$response['item'] = $item;
    	} else {
    		$response['api_status']  = 2;
        	$response['api_message'] = 'Password update gagal';
        	$response['type_dialog']  = 'Error';
    	}

        return response()->json($response);
	}
	
	public function postTutorialTarikTunai() {
        //exit();
        //$id_agen = Request::get('id_agen');
        $agen = Auth::user();
        $id_agen = $agen->id;
        $tariktunai_arr = DB::table('mst_tariktunai')
            ->where('deleted_at',NULL)
            ->get();
		$detail = DB::select('exec getAgenById ?', array($id_agen))[0];
		
        $rest_topup = array();
        foreach($tariktunai_arr as $tariktunai) {
            $layanans = DB::table('tu_tariktunai')
                ->where('id_tariktunai',$tariktunai->id)
                ->where('deleted_at',NULL)
                ->get();

            $rest_layanan = array();
            foreach($layanans as $layanan) {
                $rest2['id'] = $layanan->id;
                $rest2['layanan_code'] = $layanan->kode;
                $rest2['layanan'] = $layanan->layanan;
                //$rest2['image'] = $layanan->image;
                $rest2['description'] = str_replace('[no_hp]', $detail->no_hp, $layanan->description);
                array_push($rest_layanan, $rest2);
            }

            $rest['id'] = $tariktunai->id;
            $rest['tariktunai_code'] = $tariktunai->kode;
            $rest['tariktunai_name'] = $tariktunai->nama;
            $rest['image'] = config('app.esta_ftp').$tariktunai->image;
            $rest['layanan'] = $rest_layanan;
            array_push($rest_topup, $rest);
        }
        $response['api_status']  = 1;
        $response['api_message'] = 'Sukses';
        $response['type_dialog']  = 'Informasi';
        $response['items'] = $rest_topup;

        return response()->json($response);
    }
	
	public function postCancelPayment(){
    	$agen = Auth::user();
        $agen_id = $agen->id;
    	$transactionId = Request::get('transactionId');
    	$get_detail_transaction_temp = DB::table('temp_trans_payment')
					->where('transactionId',$transactionId)
					->first();
		$type_payment = $get_detail_transaction_temp->type;
		$up_error['status'] = 'Error';
		if($type_payment == 'bpjs_kesehatan'){
			$update = DB::table('trans_bpjs')
						->where('transactionId_sobatku',$transactionId)
						->update($up_error);
		}elseif($type_payment == 'pulsa_prabayar' || $type_payment == 'pulsa_pascabayar'){
			$update = DB::table('trans_pulsa')
						->where('transactionId_sobatku',$transactionId)
						->update($up_error);
		}elseif($type_payment == 'pln_prabayar' || $type_payment == 'pln_pascabayar'){
			$update = DB::table('trans_pln')
						->where('transactionId_sobatku',$transactionId)
						->update($up_error);
		}elseif($type_payment == 'pdam'){
			$update = DB::table('trans_pdam')
						->where('transactionId_sobatku',$transactionId)
						->update($up_error);
		}elseif($type_payment == 'kios'){
			$update = DB::table('txn_transaction')
						->where('transactionId_sobatku',$transactionId)
						->update($up_error);
		}
		if($update){
			$response['api_status']  = 1;
	        $response['api_message'] = 'Sukses';
	        $response['type_dialog']  = 'Informasi';

	        return response()->json($response);
		}
    }
}
