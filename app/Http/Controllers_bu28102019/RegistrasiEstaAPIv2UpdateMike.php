<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Storage;
use Esta;
use Illuminate\Support\Facades\Auth;
use Hash;

class RegistrasiEstaAPIv2UpdateMike extends ApiController
{
  //-------
  //updated by mike
  //28 aug 2019
  //-------
  public function postRegistrasiNew() {

    //todo:
    //simpan data agen ke table txn_pengajuan_agen (done)
    //simpan data va ke table txn_pengajuan_agen (done)
    //final checking - data added to table (done)

    $serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/register';
    $user = CRUDBooster::getsetting('user_sobatku_api');
    $hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
    $idCardFile = $_FILES['idCardFile']['tmp_name'];
    $pasPhotoFile = $_FILES['pasPhotoFile']['tmp_name'];
    $name = Request::get('name');
    $email = Request::get('email');
    $phoneNumber = Request::get('mobilePhone');
    $birthDate = Request::get('birthDate');
    $birthDate = str_replace(' ','-',$birthDate);
    $birthDate = date('Y-m-d' , strtotime($birthDate));
    $idCardNumber = Request::get('idCardNumber');
    $motherName = Request::get('motherName');
    $is_active = Request::get('is_active');
    $regid = Request::get('regid');
    $buku_rekening = Request::get('buku_rekening');
    $storage = storage_path("app/uploads/verify_agen/");
    $id_agen = Request::get('id_agen');

    $temp = DB::table('agen_temp')
      ->where('id',$id_agen)
      ->first();

    $cek_no_hp = DB::table('agen')
      ->where('no_hp',$phoneNumber)
      ->first();
      //dd($cek_no_hp);
    if(!empty($cek_no_hp->id) && $cek_no_hp->is_existing != 1) {
      $response['api_status']  = 0;
      $response['api_message'] = 'No HP sudah terdaftar';
      $response['type_dialog']  = 'Error';
      return response()->json($response);
    }

    //check pengajuan agen ada ayng pending atau Tidak
    $cek_pengajuan_agen = DB::table('txn_pengajuan_agen')
      ->where('agen_no_hp',$phoneNumber)
      ->whereNull('deleted_at')
      ->first();

      if(!empty($cek_pengajuan_agen->id)) {
        $response['api_status']  = 0;
        $response['api_message'] = 'Pengajuan anda sedang dalam proses';
        $response['type_dialog']  = 'Error';
        return response()->json($response);
      }


    $sv['agen_tgl_register'] = $temp->tgl_register;
    $sv['agen_tgl_otp_terkirim'] = $temp->tgl_otp_terkirim;
    $sv['agen_tgl_verifikasi_otp'] = $temp->tgl_verifikasi_otp;
    $sv['agen_kode_otp'] = $temp->kode_otp;
    $sv['agen_no_hp'] = $temp->no_hp;
    $sv['created_at'] = date('Y-m-d H:i:s');
    $sv['updated_at'] = date('Y-m-d H:i:s');

    $sv['agen_nama'] = $name;
    $sv['agen_no_hp'] = $phoneNumber;
    $sv['agen_email'] = $email;
    $sv['agen_photo'] = 'uploads/profile_agen/avatar.jpg';
    $sv['agen_kode_relation_referall'] = Request::get('kode_relation_referall');
    $sv['agen_password'] = Hash::make(Request::get('password'));

    $kode = Esta::nomor_transaksi('txn_pengajuan_agen',CRUDBooster::getsetting('kode_pengajuan_agen'));
    /*echo $kode;
    exit();*/
    $sv['kode_pengajuan_agen'] = $kode;
    $sv['agen_kode_referall_agen'] = Esta::kode_referall();
    $sv['agen_status_agen'] = 'Basic';
    $sv['agen_status_aktif'] = 'Tidak Aktif';
    $sv['agen_status_verifikasi'] = 'Pending';
    $sv['agen_notif_email'] = 'Yes';
    $sv['created_user'] = $sv['agen_nama'];
    $sv['updated_user'] = $sv['agen_nama'];
    $sv['agen_regid'] = $regid;

    if(!empty($sv['agen_kode_relation_referall'])) {
      $check_referall = DB::table('agen')->where('kode_referall_agen',$sv['agen_kode_relation_referall'])->first();
      //$setting_ref = DB::table('voucher_referall')->whereNull('deleted_at')->where('aktif','Yes')->first();

      if($check_referall->id >= 1) {

      } else {
        $response['api_status']  = 2;
          $response['api_message'] = 'Kode referal yang anda pakai tidak terdaftar';
          $response['type_dialog']  = 'Error';
          return response()->json($response);
          exit();
      }
    }

    //Save Image to FTP
    $idCardFileFoto = Request::file('idCardFile');
    $file_nameidCardFile = 'Sobatku/idCardFoto-'.time().'-'.$name.".jpg";
    Storage::disk('ftp')->put($file_nameidCardFile, file_get_contents($idCardFileFoto));
    $pasPhotoFileFoto = Request::file('pasPhotoFile');
    $file_namepasPhotoFile = 'Sobatku/pasPhotoFile-'.time().'-'.$name.".jpg";
    Storage::disk('ftp')->put($file_namepasPhotoFile, file_get_contents($pasPhotoFileFoto));
    //dd($idCardFileFoto);

    if($is_active == 0){
      //PROCCESS TO SOBATKU
      $prefixActivation = CRUDBooster::getsetting('prefix_reference_registrasi');
      $keterangan = 'Registrasion';
      $referenceNumber = DB::select('exec CreateRefNumberSobatku ?,?', array($prefixActivation, $keterangan));

      $jsonRegistration = [
        'name' => $name,
        'email' => $email,
        'mobilePhone' => $phoneNumber,
        'birthDate' => $birthDate,
        'idCardNumber' => $idCardNumber,
        'motherName' => $motherName,
        'referenceNumber' => $referenceNumber[0]->Prefix,
        'user' => $user,
        'regid' => $regid,
        'hashCode' => hash('sha256', $phoneNumber.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
      ];

      $request = [
        'idCardFile' => curl_file_create($idCardFile, 'image/jpg', $file_nameidCardFile),
        'pasPhotoFile' => curl_file_create($idCardFile, 'image/jpg', $file_namepasPhotoFile),
        'jsonRegistration' => json_encode($jsonRegistration)
      ];

      //dd($request);

      $ch = curl_init($serviceURL);
      curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
      curl_setopt( $ch, CURLOPT_POSTFIELDS, $request );
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
          'Accept:application/json',
          'Content-Type:multipart/form-data'
        )
      );
      $responsecurl = curl_exec($ch);
      $responseBody = json_decode( $responsecurl,true );
      curl_close($ch);
      $saveLog = DB::statement('exec CreateLogSobatku ?,?,?,?,?', array($phoneNumber,$referenceNumber[0]->Prefix,json_encode( $request ),$responsecurl,$keterangan));

      if($responseBody['responseCode'] != '00'){
        $item['responseSobatku'] = $responseBody;
        $response['api_status']  = 0;
        $response['api_message'] = 'Gagal';
        $response['type_dialog']  = 'Informasi';
        $response['item'] = $item;
        return response()->json($response);
      }

      $responseBody['urlCompleteRegister'] = CRUDBooster::getsetting('parent_url_sobatku_api').'/walletPage/register?phoneNumber='.$phoneNumber.'&sessionId='.$responseBody['sessionId'];
    }

    $sv['agen_idCardNumber'] = $idCardNumber;
    $sv['agen_motherName'] = $motherName;
    $sv['agen_pasPhotoFile'] = $file_namepasPhotoFile;
    $sv['agen_idCardFile'] = $file_nameidCardFile;
    $sv['agen_tglpengajuansobatku'] = date('Y-m-d H:i:s');
    $sv['agen_tgl_lahir'] = $birthDate;

    if(!empty($idCardFile)) {
      $file_nameidCardFile = 'Sobatku/idCardFoto-'.time().'-'.$name.".jpg";
      $sv['va_foto_ktp'] = $file_nameidCardFile;
    }

    if(!empty($pasPhotoFile)) {
      $pasPhotoFileName  = 'selfie-'.date('ymdhis').'-'.$id_agen.".jpg";
      $sv['va_foto_selfie'] = $pasPhotoFileName;
    }

    $update = DB::table('txn_pengajuan_agen')
    ->insertGetId($sv);

    if($is_active == 1){
      $update = $this->movePengajuanAgenToAgenByID($update);
    }

    if($update) {
      //get ID dari table txn_pengajuan_agen
      $id_agen = $update;

      $item['nama'] = $name;
      $item['no_hp'] = $phoneNumber;
      $item['kode_referall_agen'] = $sv['agen_kode_referall_agen'];
      $item['status_agen'] = $sv['agen_status_agen'];
      $item['photo'] = env('BACKEND_URL').'uploads/profile_agen/avatar.jpg';
      $item['responseSobatku'] = $responseBody;
      $item['id_agen'] = $id_agen;

      $response['api_status']  = 1;
      $response['api_message'] = 'Registrasi berhasil. Menunggu proses aktivasi dari sobatku.';
      $response['type_dialog']  = 'Informasi';
      $response['item'] = $item;
    } else {
      $response['api_status']  = 0;
      $response['api_message'] = 'Gagal';
      $response['type_dialog']  = 'Error';
    }

    return response()->json($response);

  }

  public function movePengajuanAgenToAgen(){

    $phoneNumber = Request::get('mobilePhone');

    $getAgen = DB::select('EXEC getPengajuanAgenByNohp ?',array($phoneNumber))[0];

    $sv['tgl_register'] = $getAgen->agen_tgl_register;
    $sv['tgl_otp_terkirim'] = $getAgen->agen_tgl_otp_terkirim;
    $sv['tgl_verifikasi_otp'] = $getAgen->agen_tgl_verifikasi_otp;
    $sv['kode_otp'] = $getAgen->agen_kode_otp;
    $sv['no_hp'] = $getAgen->agen_no_hp;
    $sv['created_at'] = date('Y-m-d H:i:s');

    $sv['nama'] = $getAgen->agen_nama;
    $sv['no_hp'] = $getAgen->agen_no_hp;
    $sv['email'] = $getAgen->agen_email;
    $sv['photo'] = $getAgen->agen_photo;
    $sv['kode_relation_referall'] = $getAgen->agen_kode_relation_referall;
    $sv['password'] = $getAgen->agen_password;

    $kode = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
    /*echo $kode;
    exit();*/
    $sv['kode'] = $kode;
    $sv['kode_referall_agen'] = Esta::kode_referall();
    $sv['status_agen'] = 'Basic';
    $sv['status_aktif'] = 'Aktif';
    $sv['status_verifikasi'] = 'Pending';
    $sv['notif_email'] = 'Yes';
    $sv['created_user'] = $sv['nama'];
        $sv['updated_user'] = '';
        $sv['regid'] = $regid;

    if(!empty($sv['kode_relation_referall'])) {
      $check_referall = DB::table('agen')->where('kode_referall_agen',$sv['kode_relation_referall'])->first();
      $setting_ref = DB::table('voucher_referall')->whereNull('deleted_at')->where('aktif','Yes')->first();
    }

    //---
    $sv['idCardNumber'] = $getAgen->agen_idCardNumber;
    $sv['motherName'] = $getAgen->agen_motherName;
    $sv['pasPhotoFile'] = $getAgen->agen_pasPhotoFile;
    $sv['idCardFile'] = $getAgen->agen_idCardFile;
    $sv['tglpengajuansobatku'] = $getAgen->agen_tglpengajuansobatku;
    $sv['tgl_lahir'] = $getAgen->agen_tgl_lahir;

    $update = DB::table('agen')
      //->where('id',$id_agen)
      ->insertGetId($sv);

      if($update) {

        DB::table('txn_pengajuan_agen')
        ->where('agen_no_hp', $getAgen->agen_no_hp)->delete();

        $id_agen = $update;

        $up['foto_ktp'] = $getAgen->va_foto_ktp;
        $up['foto_ktp_status'] = 'Submitted';
        $up['foto_ktp_note'] = 'Menunggu verifikasi';
        $up['foto_ktp_last_submit'] = date('Y-m-d H:i:s');

        $up['foto_selfie'] = $getAgen->va_foto_selfie;
        $up['foto_selfie_status'] = 'Submitted';
        $up['foto_selfie_note'] = 'Menunggu verifikasi';
        $up['foto_selfie_last_submit'] = date('Y-m-d H:i:s');

      $check = DB::table('verify_agen')
            ->where('id_agen',$id_agen)
            ->first();

        $up['id_agen'] = $id_agen;
        $up['created_user'] = Esta::user($id_agen);
        $up['updated_user'] = Esta::user($id_agen);
        $up_agn['updated_at'] = date('Y-m-d H:i:s');
        if(!empty($check)) {
        $update = DB::table('verify_agen')
          ->where('id',$check->id)
          ->update($up);
        } else {
          $up['created_at'] = date('Y-m-d H:i:s');
          $update = DB::table('verify_agen')
          ->insert($up);
        }

        $banks = DB::table('bank')
          ->where('prefix_va','!=',NULL)
          ->whereNull('deleted_at')
          ->get();
        foreach($banks as $bank) {
          $flag_genva = $bank->flag_genva;

          $c['created_at'] = date('Y-m-d H:i:s');
          $c['updated_at'] = date('Y-m-d H:i:s');
          $c['created_user'] = Esta::user($id_agen);
          $c['updated_user'] = Esta::user($id_agen);
          $c['id_agen'] = $id_agen;
          $c['id_bank'] = $bank->id;
          if($flag_genva == 0) {
            $c['no_va'] = $bank->prefix_va.$sv['no_hp'];
          } else {
            $c['no_va'] = $bank->prefix_va.str_replace(CRUDBooster::getsetting('kode_agen'), '', $kode);
          }

          $in = DB::table('agen_va')
            ->insert($c);
        }

      $merchants = DB::table('merchant')
          ->whereNull('deleted_at')
          //->groupby('group')
          ->where('kode','Alfamart')
          ->get();
        foreach($merchants as $merchant) {
          $cm['created_at'] = date('Y-m-d H:i:s');
          $cm['updated_at'] = date('Y-m-d H:i:s');
          $cm['created_user'] = Esta::user($id_agen);
          $cm['updated_user'] = Esta::user($id_agen);
          $cm['id_agen'] = $id_agen;
          $cm['merchant'] = $merchant->group;
          $cm['no_va'] = $merchant->prefix_va.$sv['no_hp'];

          $in = DB::table('agen_va')
            ->insert($cm);
        }

        Esta::log_money_old($id_agen,0,date('Y-m-d H:i:s'),'Agen Registrasi','Agen Registrasi','','Riwayat Agen','','');

        if($check_referall->id >= 1) {
          $detail_voucher_referall = DB::table('voucher_referall')->whereNull('deleted_at')->first();
          $voucher_referall_aktif = $detail_voucher_referall->aktif;
          if($voucher_referall_aktif == 'Yes') {
            $voucher_pemakai = $detail_voucher_referall->referall_pemakai;
            $voucher_dipakai = $detail_voucher_referall->referall_dipakai;

          $detail_voucher_pemakai = DB::table('voucher')->where('id',$voucher_pemakai)->first();
          $detail_voucher_dipakai = DB::table('voucher')->where('id',$voucher_dipakai)->first();

          $save_child['created_at']       = date('Y-m-d H:i:s');
          $save_child['created_user'] = Esta::user($id_agen);
          $save_child['updated_user'] = Esta::user($id_agen);
          $save_child['id_agen']          = $check_referall->id;
          $save_child['id_voucher']       = $voucher_dipakai;
          $save_child['product']          = $detail_voucher_dipakai->product;
          $save_child['voucher_nama']          = $detail_voucher_dipakai->nama;
          $save_child['voucher_expired']          = $detail_voucher_dipakai->expired_date;
          $save_child['voucher_amount']          = $detail_voucher_dipakai->amount;
          $save_child['voucher_image']          = $detail_voucher_dipakai->image;
          $save_child['voucher_product']          = $detail_voucher_dipakai->product;
          $save_child['voucher_description']          = $detail_voucher_dipakai->description;
          $save_child['id_trans_voucher'] = '';
          $save_child['used']             = 'No';
          DB::table('trans_voucher_child')->insert($save_child);

          $save_notif['created_at'] = $detail_voucher_dipakai->expired_date;
          $save_notif['created_user'] = Esta::user($id_agen);
          $save_notif['updated_user'] = Esta::user($id_agen);
          $save_notif['title'] = $detail_voucher_dipakai->nama;
          $save_notif['description'] = $detail_voucher_dipakai->description;
          $save_notif['description_short'] = $detail_voucher_dipakai->description;
          $save_notif['image'] = $detail_voucher_dipakai->image;
          $save_notif['id_agen'] = $check_referall->id;
          $save_notif['read'] = 'No';
          $save_notif['flag'] = 'Voucher';
          $save_notif['syarat_ketentuan'] = $detail_voucher_dipakai->syarat_ketentuan;
          $save_notif['id_voucher'] = $voucher_dipakai;
          DB::table('notification')->insert($save_notif);

          $save_child2['created_at']       = date('Y-m-d H:i:s');
          $save_child2['created_user'] = Esta::user($id_agen);
          $save_child2['updated_user'] = Esta::user($id_agen);
          $save_child2['id_agen']          = $id_agen;
          $save_child2['id_voucher']       = $voucher_pemakai;
          $save_child2['product']          = $detail_voucher_pemakai->product;
          $save_child2['voucher_nama']          = $detail_voucher_pemakai->nama;
          $save_child2['voucher_expired']          = $detail_voucher_pemakai->expired_date;
          $save_child2['voucher_amount']          = $detail_voucher_pemakai->amount;
          $save_child2['voucher_image']          = $detail_voucher_pemakai->image;
          $save_child2['voucher_product']          = $detail_voucher_pemakai->product;
          $save_child2['voucher_description']          = $detail_voucher_pemakai->description;
          $save_child2['id_trans_voucher'] = '';
          $save_child2['used']             = 'No';
          DB::table('trans_voucher_child')->insert($save_child2);

          $save_notif2['created_at'] = $detail_voucher_pemakai->expired_date;
          $save_notif2['created_user'] = Esta::user($id_agen);
          $save_notif2['updated_user'] = Esta::user($id_agen);
          $save_notif2['title'] = $detail_voucher_pemakai->nama;
          $save_notif2['description'] = $detail_voucher_pemakai->description;
          $save_notif2['description_short'] = $detail_voucher_pemakai->description;
          $save_notif2['image'] = $detail_voucher_pemakai->image;
          $save_notif2['id_agen'] = $id_agen;
          $save_notif2['read'] = 'No';
          $save_notif2['flag'] = 'Voucher';
          $save_notif2['syarat_ketentuan'] = $detail_voucher_pemakai->syarat_ketentuan;
          $save_notif2['id_voucher'] = $voucher_pemakai;
          DB::table('notification')->insert($save_notif2);

          if($check_referall->regid != NULL) {
            $datafcm['title'] = CRUDBooster::getsetting('notification_voucher_header');
            $datafcm['content'] = CRUDBooster::getsetting('notification_voucher_referall');
            $regid[] = $check_referall->regid;
            Esta::sendFCM($regid,$datafcm);
          }
        }
      }

      //send notif
      $save_notif['created_at'] = date('Y-m-d H:i:s');
      $save_notif['title'] = 'Verification';
      $save_notif['description'] = 'Verification Result';
      $save_notif['description_short'] = 'Akun sukses di verifikasi';
      $save_notif['image'] = '';
      $save_notif['flag'] = 'Notifikasi';
      $save_notif['id_agen'] = $getAgen->id;
      $save_notif['read'] = 'No';
      //$save_notif['id_header'] = $id;

      $detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
      if($detail_agen->regid != NULL) {
        $datafcm['title'] = $save_notif['title'];
        $datafcm['content'] = $save_notif['description'];
        $datafcm['type'] = 'Sobatku';
        $save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
      }
      $save_ver['tglverifikasisobatku'] = date('Y-m-d H:i:s');
      $save_ver['is_existing'] = 0;
      $save_ver['regid'] = NULL;
      DB::table('agen')
        ->where('id',$getAgen->id)
        ->update($save_ver);

      DB::table('notification')->insert($save_notif);


    }else{

        $response['api_status']  = 0;
        $response['api_message'] = 'Gagal membuat data agen';
        $response['type_dialog']  = 'Error';

    }

    return $this->respondWithDataAndMessage($response, "Success");

  }

  public function movePengajuanAgenToAgenByID($id){

    $getAgen = DB::table('txn_pengajuan_agen')
    ->where('id',$id)
    ->first();

    $sv['tgl_register'] = $getAgen->agen_tgl_register;
    $sv['tgl_otp_terkirim'] = $getAgen->agen_tgl_otp_terkirim;
    $sv['tgl_verifikasi_otp'] = $getAgen->agen_tgl_verifikasi_otp;
    $sv['kode_otp'] = $getAgen->agen_kode_otp;
    $sv['no_hp'] = $getAgen->agen_no_hp;
    $sv['created_at'] = date('Y-m-d H:i:s');

    $sv['nama'] = $getAgen->agen_nama;
    $sv['no_hp'] = $getAgen->agen_no_hp;
    $sv['email'] = $getAgen->agen_email;
    $sv['photo'] = $getAgen->agen_photo;
    $sv['kode_relation_referall'] = $getAgen->agen_kode_relation_referall;
    $sv['password'] = $getAgen->agen_password;

    $kode = Esta::nomor_transaksi('agen',CRUDBooster::getsetting('kode_agen'));
    /*echo $kode;
    exit();*/
    $sv['kode'] = $kode;
    $sv['kode_referall_agen'] = Esta::kode_referall();
    $sv['status_agen'] = 'Basic';
    $sv['status_aktif'] = 'Aktif';
    $sv['status_verifikasi'] = 'Pending';
    $sv['notif_email'] = 'Yes';
    $sv['created_user'] = $sv['nama'];
        $sv['updated_user'] = '';
        $sv['regid'] = $regid;

    if(!empty($sv['kode_relation_referall'])) {
      $check_referall = DB::table('agen')->where('kode_referall_agen',$sv['kode_relation_referall'])->first();
      $setting_ref = DB::table('voucher_referall')->whereNull('deleted_at')->where('aktif','Yes')->first();
    }

    //---
    $sv['idCardNumber'] = $getAgen->agen_idCardNumber;
    $sv['motherName'] = $getAgen->agen_motherName;
    $sv['pasPhotoFile'] = $getAgen->agen_pasPhotoFile;
    $sv['idCardFile'] = $getAgen->agen_idCardFile;
    $sv['tglpengajuansobatku'] = $getAgen->agen_tglpengajuansobatku;
    $sv['tgl_lahir'] = $getAgen->agen_tgl_lahir;

    $update = DB::table('agen')
      //->where('id',$id_agen)
      ->insertGetId($sv);

      if($update) {

        DB::table('txn_pengajuan_agen')
        ->where('agen_no_hp', $getAgen->agen_no_hp)->delete();

        $id_agen = $update;

        $up['foto_ktp'] = $getAgen->va_foto_ktp;
        $up['foto_ktp_status'] = 'Submitted';
        $up['foto_ktp_note'] = 'Menunggu verifikasi';
        $up['foto_ktp_last_submit'] = date('Y-m-d H:i:s');

        $up['foto_selfie'] = $getAgen->va_foto_selfie;
        $up['foto_selfie_status'] = 'Submitted';
        $up['foto_selfie_note'] = 'Menunggu verifikasi';
        $up['foto_selfie_last_submit'] = date('Y-m-d H:i:s');

      $check = DB::table('verify_agen')
            ->where('id_agen',$id_agen)
            ->first();

        $up['id_agen'] = $id_agen;
        $up['created_user'] = Esta::user($id_agen);
        $up['updated_user'] = Esta::user($id_agen);
        $up_agn['updated_at'] = date('Y-m-d H:i:s');
        if(!empty($check)) {
        $update = DB::table('verify_agen')
          ->where('id',$check->id)
          ->update($up);
        } else {
          $up['created_at'] = date('Y-m-d H:i:s');
          $update = DB::table('verify_agen')
          ->insert($up);
        }

        $banks = DB::table('bank')
          ->where('prefix_va','!=',NULL)
          ->whereNull('deleted_at')
          ->get();
        foreach($banks as $bank) {
          $flag_genva = $bank->flag_genva;

          $c['created_at'] = date('Y-m-d H:i:s');
          $c['updated_at'] = date('Y-m-d H:i:s');
          $c['created_user'] = Esta::user($id_agen);
          $c['updated_user'] = Esta::user($id_agen);
          $c['id_agen'] = $id_agen;
          $c['id_bank'] = $bank->id;
          if($flag_genva == 0) {
            $c['no_va'] = $bank->prefix_va.$sv['no_hp'];
          } else {
            $c['no_va'] = $bank->prefix_va.str_replace(CRUDBooster::getsetting('kode_agen'), '', $kode);
          }

          $in = DB::table('agen_va')
            ->insert($c);
        }

      $merchants = DB::table('merchant')
          ->whereNull('deleted_at')
          //->groupby('group')
          ->where('kode','Alfamart')
          ->get();
        foreach($merchants as $merchant) {
          $cm['created_at'] = date('Y-m-d H:i:s');
          $cm['updated_at'] = date('Y-m-d H:i:s');
          $cm['created_user'] = Esta::user($id_agen);
          $cm['updated_user'] = Esta::user($id_agen);
          $cm['id_agen'] = $id_agen;
          $cm['merchant'] = $merchant->group;
          $cm['no_va'] = $merchant->prefix_va.$sv['no_hp'];

          $in = DB::table('agen_va')
            ->insert($cm);
        }

        Esta::log_money_old($id_agen,0,date('Y-m-d H:i:s'),'Agen Registrasi','Agen Registrasi','','Riwayat Agen','','');

        if($check_referall->id >= 1) {
          $detail_voucher_referall = DB::table('voucher_referall')->whereNull('deleted_at')->first();
          $voucher_referall_aktif = $detail_voucher_referall->aktif;
          if($voucher_referall_aktif == 'Yes') {
            $voucher_pemakai = $detail_voucher_referall->referall_pemakai;
            $voucher_dipakai = $detail_voucher_referall->referall_dipakai;

          $detail_voucher_pemakai = DB::table('voucher')->where('id',$voucher_pemakai)->first();
          $detail_voucher_dipakai = DB::table('voucher')->where('id',$voucher_dipakai)->first();

          $save_child['created_at']       = date('Y-m-d H:i:s');
          $save_child['created_user'] = Esta::user($id_agen);
          $save_child['updated_user'] = Esta::user($id_agen);
          $save_child['id_agen']          = $check_referall->id;
          $save_child['id_voucher']       = $voucher_dipakai;
          $save_child['product']          = $detail_voucher_dipakai->product;
          $save_child['voucher_nama']          = $detail_voucher_dipakai->nama;
          $save_child['voucher_expired']          = $detail_voucher_dipakai->expired_date;
          $save_child['voucher_amount']          = $detail_voucher_dipakai->amount;
          $save_child['voucher_image']          = $detail_voucher_dipakai->image;
          $save_child['voucher_product']          = $detail_voucher_dipakai->product;
          $save_child['voucher_description']          = $detail_voucher_dipakai->description;
          $save_child['id_trans_voucher'] = '';
          $save_child['used']             = 'No';
          DB::table('trans_voucher_child')->insert($save_child);

          $save_notif['created_at'] = $detail_voucher_dipakai->expired_date;
          $save_notif['created_user'] = Esta::user($id_agen);
          $save_notif['updated_user'] = Esta::user($id_agen);
          $save_notif['title'] = $detail_voucher_dipakai->nama;
          $save_notif['description'] = $detail_voucher_dipakai->description;
          $save_notif['description_short'] = $detail_voucher_dipakai->description;
          $save_notif['image'] = $detail_voucher_dipakai->image;
          $save_notif['id_agen'] = $check_referall->id;
          $save_notif['read'] = 'No';
          $save_notif['flag'] = 'Voucher';
          $save_notif['syarat_ketentuan'] = $detail_voucher_dipakai->syarat_ketentuan;
          $save_notif['id_voucher'] = $voucher_dipakai;
          DB::table('notification')->insert($save_notif);

          $save_child2['created_at']       = date('Y-m-d H:i:s');
          $save_child2['created_user'] = Esta::user($id_agen);
          $save_child2['updated_user'] = Esta::user($id_agen);
          $save_child2['id_agen']          = $id_agen;
          $save_child2['id_voucher']       = $voucher_pemakai;
          $save_child2['product']          = $detail_voucher_pemakai->product;
          $save_child2['voucher_nama']          = $detail_voucher_pemakai->nama;
          $save_child2['voucher_expired']          = $detail_voucher_pemakai->expired_date;
          $save_child2['voucher_amount']          = $detail_voucher_pemakai->amount;
          $save_child2['voucher_image']          = $detail_voucher_pemakai->image;
          $save_child2['voucher_product']          = $detail_voucher_pemakai->product;
          $save_child2['voucher_description']          = $detail_voucher_pemakai->description;
          $save_child2['id_trans_voucher'] = '';
          $save_child2['used']             = 'No';
          DB::table('trans_voucher_child')->insert($save_child2);

          $save_notif2['created_at'] = $detail_voucher_pemakai->expired_date;
          $save_notif2['created_user'] = Esta::user($id_agen);
          $save_notif2['updated_user'] = Esta::user($id_agen);
          $save_notif2['title'] = $detail_voucher_pemakai->nama;
          $save_notif2['description'] = $detail_voucher_pemakai->description;
          $save_notif2['description_short'] = $detail_voucher_pemakai->description;
          $save_notif2['image'] = $detail_voucher_pemakai->image;
          $save_notif2['id_agen'] = $id_agen;
          $save_notif2['read'] = 'No';
          $save_notif2['flag'] = 'Voucher';
          $save_notif2['syarat_ketentuan'] = $detail_voucher_pemakai->syarat_ketentuan;
          $save_notif2['id_voucher'] = $voucher_pemakai;
          DB::table('notification')->insert($save_notif2);

          if($check_referall->regid != NULL) {
            $datafcm['title'] = CRUDBooster::getsetting('notification_voucher_header');
            $datafcm['content'] = CRUDBooster::getsetting('notification_voucher_referall');
            $regid[] = $check_referall->regid;
            Esta::sendFCM($regid,$datafcm);
          }
        }
      }

      //send notif
      $save_notif['created_at'] = date('Y-m-d H:i:s');
      $save_notif['title'] = 'Verification';
      $save_notif['description'] = 'Verification Result';
      $save_notif['description_short'] = 'Akun sukses di verifikasi';
      $save_notif['image'] = '';
      $save_notif['flag'] = 'Notifikasi';
      $save_notif['id_agen'] = $getAgen->id;
      $save_notif['read'] = 'No';
      //$save_notif['id_header'] = $id;

      $detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
      if($detail_agen->regid != NULL) {
        $datafcm['title'] = $save_notif['title'];
        $datafcm['content'] = $save_notif['description'];
        $datafcm['type'] = 'Sobatku';
        $save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
      }
      $save_ver['tglverifikasisobatku'] = date('Y-m-d H:i:s');
      $save_ver['is_existing'] = 0;
      $save_ver['regid'] = NULL;
      DB::table('agen')
        ->where('id',$getAgen->id)
        ->update($save_ver);

      DB::table('notification')->insert($save_notif);


    }else{

        $response['api_status']  = 0;
        $response['api_message'] = 'Gagal membuat data agen';
        $response['type_dialog']  = 'Error';

    }
    return $getAgen->id;
  }

  public function verificationManualResult(){
		$request = '';
		$phoneNumber = Request::get('phoneNumber');
		$sessionId = Request::get('sessionId');
		$responseCode = Request::get('responseCode');
		$responseDescription = Request::get('responseDescription');
		$hashCode = Request::get('hashCode');
		$response = [
			'phoneNumber' => $phoneNumber,
			'sessionId' => $sessionId,
			'responseCode' => $responseCode,
			'responseDescription' => $responseDescription,
			'hashCode' => $hashCode
		];

		$responseJson = json_encode($response);

		$saveResponse = DB::statement('exec CreateLogSobatkuVerification ?,?,?,?,?', array($phoneNumber,'',$request,$responseJson,'Verification Manual Result'));
		if($response['responseCode'] == '00'){

      $getPengajuanAgen = DB::select('EXEC getPengajuanAgenByNohp ?',array($phoneNumber))[0];
      if(!empty($getPengajuanAgen->id) ){
        $this->movePengajuanAgenToAgenByID($getPengajuanAgen->id);
      }

      $getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];


			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Verification';
			$save_notif['description'] = 'Verification Result';
			$save_notif['description_short'] = 'Akun sukses di verifikasi';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
			}
			$save_ver['tglverifikasisobatku'] = date('Y-m-d H:i:s');
			$save_ver['is_existing'] = 0;
			$save_ver['regid'] = NULL;
			DB::table('agen')
				->where('id',$getAgen->id)
				->update($save_ver);

			DB::table('notification')->insert($save_notif);
		}else{
			$getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];
			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Verification';
			$save_notif['description'] = $response['responseDescription'];
			$save_notif['description_short'] = 'Gagal Verification';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
			}
			$save_ver['tglpengajuansobatku'] = NULL;
			DB::table('agen')
				->where('id',$getAgen->id)
				->update($save_ver);
			DB::table('notification')->insert($save_notif);
		}

		return $this->respondWithDataAndMessage($response, "Success");
	}


//-========================
}
