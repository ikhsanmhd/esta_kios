<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Agen extends Authenticatable
{
	use Notifiable, HasApiTokens;
	

    protected $table = 'agen';

    public function findForPassport($nohp){
    	return $this->where('no_hp', $nohp)->first();
    }

    public function getAuthPassword(){
    	return $this->password;
    }

    public function getAuthIdentifier(){
    	return $this->id;
    }
}
