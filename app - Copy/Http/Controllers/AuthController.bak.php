<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\CLient;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
//use App\Agen;


class AuthController extends ApiController
{
    public function tokenAuth(Request $request){
    	$http = new Client();
        $nohp = $request->no_hp;
        $password = $request->password;

    	try{
    		$response = $http->post(url('').'/oauth/token', [
    			'form_params' => [
    				'grant_type' => 'password',
    				'client_id' => config('app.oauth.client_id'),
    				'client_secret' => config('app.oauth.secret'),
    				'username' => $nohp,
    				'password' => $password,
    				'scope' => '',
    			],
    		]);
    	}catch(GuzzleException $e){
			//dd($e);
    		return $this->respondWithError('Terdapat kesalahan dalam mengirimkan data : '.$e);
    	}

    	$response_obj = json_decode((string) $response->getBody(), true);
        //dd($response_obj);

    	if(isset($response_obj['error'])){
    		return $this->responWithError('Data yang dimasukkan tidak sesuai');
    	}

        $data['access_token'] = $response_obj['access_token'];
        $data['refresh_token'] = $response_obj['refresh_token'];
        $expiry_date = Carbon::now()->addSecond($response_obj['expires_in'], 'second')->format('Y-m-d H:i:s');

        $data['expiry_date'] = $expiry_date;

        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function refreshToken(Request $request){
        $validation = Validator::make($request->all(), [
            'refresh_token' => 'required',
        ]);

        if($validation->fails()){
            return $this->respondWithError("Refresh token tidak boleh kosong");
        }

        $refresh_token = $request->input('refresh_token');

        $http = new Client();

        try{
            $response=$http->post(url('').'/oauth/token', [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'client_id' => config('app.oauth.client_id'),
                    'client_secret' => config('app.oauth.secret'),
                    'refresh_token' => $refresh_token,
                    'scope' => '',
                ],
            ]);
        }catch(GuzzleException $e){
            return $this->respondWithError("Terdapat kesalahan dalam mengirimkan data atau token yang dikirimkan tidak valid");
        }

        $response_obj = json_decode((string) $response->getBody(),true);
        //dd($response_obj);
        $data['access_token'] = $response_obj['access_token'];
        $data['refresh_token'] = $response_obj['refresh_token'];
        $expiry_date = Carbon::now()->addSecond($response_obj['expires_in'], 'second')->format('Y-m-d H:i:s');
        //dd(Carbon::now()->addSecond($response_obj['expires_in'], 'second')->format('Y-m-d H:i:s'));
        $data['expiry_date'] = $expiry_date;

        return $this->respondWithDataAndMessage($data, "Token berhasil diperbaharui");
    }

    public function details(){
        $agen = Auth::user();
        return $this->respondWithDataAndMessage($agen, "Success");
    }
}
