<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;

class BuyItemAPI extends ApiController
{    
/*
    public function getCmsMarketplace(){
        $agen = Auth::user();
        $agen_id = $agen->id;
        $getCmsMarketplaceCategories = DB::select('exec getCMSCategory');
        //$categoriesCollection = collect($getCmsMarketplaceCategories);
        //$categories = $categoriesCollection->all();
        $categorylist = [];
        $categorylistextend = [];
        $i=0;
        foreach ($getCmsMarketplaceCategories as $category) {
            if($i<=1){
                $categorylist[] = [
                    'id' => $category->id,
                    'name' => $category->name,
                    'category_logo' => $category->category_logo
                ];
                $categorylist[] = [
                    'id' => $category->id2,
                    'name' => $category->name2,
                    'category_logo' => $category->category_logo2
                ];
                $categorylist[] = [
                    'id' => $category->id3,
                    'name' => $category->name3,
                    'category_logo' => $category->category_logo3
                ];
                $categorylist[] = [
                    'id' => $category->id4,
                    'name' => $category->name4,
                    'category_logo' => $category->category_logo4
                ];
            }else{
                $categorylistextend[] = [
                    'id' => $category->id,
                    'name' => $category->name,
                    'category_logo' => $category->category_logo
                ];
                $categorylistextend[] = [
                    'id' => $category->id2,
                    'name' => $category->name2,
                    'category_logo' => $category->category_logo2
                ];
                $categorylistextend[] = [
                    'id' => $category->id3,
                    'name' => $category->name3,
                    'category_logo' => $category->category_logo3
                ];
                $categorylistextend[] = [
                    'id' => $category->id4,
                    'name' => $category->name4,
                    'category_logo' => $category->category_logo4
                ];
            }
            $i++;
        }

        $getCmsSuppliers = DB::select('exec getCMSSupplier');
        //dd($getCmsSuppliers);
        $supplierlist = [];
        foreach ($getCmsSuppliers as $supplier) {
            $supplierdetails = DB::select('exec getCMSSupplierDetail ?,?', array($agen_id, $supplier->supplier_id));
            $supplierdetailList = [];
            foreach ($supplierdetails as $supplierdetail) {
                $supplierunits = DB::select('getCMSItemDetailById ?,?', array($agen_id, $supplierdetail->item_id));
                $units = [];
                foreach ($supplierunits as $supplierunit) {
                    $units[] = [
                        'marketplace_id' => $supplierunit->id,
                        'item_esta_code' => $supplierunit->item_esta_code,
                        'item_sku' => $supplierunit->item_sku,
                        'itemunit_id' => $supplierunit->itemunit_id,
                        'itemunit_name' => $supplierunit->itemunit_name,
                        'discount_value' => $supplierunit->discount_value,
                        'discount_type' => $supplierunit->discount_type,
                        'item_after_discount' => $supplierunit->item_after_discount,
                        'item_sale_price' => $supplierunit->item_sale_price,
                        'stock' => $supplierunit->stock,
                        'stock_converted' => $supplierunit->stock_converted,
                        'margin_type' => $supplierunit->margin_type,
                        'margin_value' => $supplierunit->margin_value,
                        'item_buy_price' => $supplierunit->item_buy_price
                    ];
                }

                $item_photoFTP = config('app.esta_ftp').$supplierdetail->item_photo;

                $supplierdetailList[] = [
                    'category_id' => $supplierdetail->category_id,
                    'category_code' => $supplierdetail->category_code,
                    'category_name' => $supplierdetail->category_name,
                    'subcategory_id' => $supplierdetail->subcategory_id,
                    'subcategory_name' => $supplierdetail->subcategory_name,
                    'subcategory_code' => $supplierdetail->subcategory_code,
                    'item_id' => $supplierdetail->item_id,
                    'item_name' => $supplierdetail->item_name,
                    'item_photo' => $item_photoFTP,
                    'supplier_id' => $supplierdetail->supplier_id,
                    'supplier_name' => $supplierdetail->supplier_name,
                    'supplier_code' => $supplierdetail->supplier_code,
                    'units' => $units
                ];
            }   

            $supplierlist[] = [
                'id' => $supplier->supplier_id,
                'name' => $supplier->name,
                'supplier_logo' => $supplier->supplier_logo,
                'details' => $supplierdetailList
            ];
        }

        $data['category'] = $categorylist;
        $data['extend_category'] = $categorylistextend;
        $data['supplier'] = $supplierlist;
        return $this->respondWithDataAndMessage($data, "Success");
    }
*/

	public function getCmsMarketplace(){
        $agen = Auth::user();  //UNTUK PRODUCTION JANGAN LUPA DI UNCOMMENT
        $agen_id = $agen->id;
		
		//ambil langsung dari table mst_cmsCategory. data yang dibutuhkan sudah ada di table mst_cmsCategory
        $getCmsMarketplaceCategories = DB::table('mst_cmsCategory')->whereNull('deleted_at')->get();
        $categorylist = [];
        $categorylistextend = [];
        $i=0;
        foreach ($getCmsMarketplaceCategories as $category) {
			
			//Logo
            $explode_logo = explode("/", $category->photo);
            $last_index = count($explode_logo)-1;
            $category_logoFTP = config('app.esta_ftp').$explode_logo[$last_index];
			
			//Check untuk 8 categori pertama dimasukan ke array categorylist
			if($i < 8){
				$categorylist[] = [
                    'id' => $category->id,
                    'name' => $category->name,
                    'category_logo' => $category_logoFTP
                ];
			}else{
				$categorylistextend[] = [
                    'id' => $category->id,
                    'name' => $category->name,
                    'category_logo' => $category_logoFTP
                ];
			}
            $i++;
        }

		//Get all supplier with result like SP getCMSSupplier
        $getCmsSuppliers = DB::table('mst_cmsSupplier')
							->select('mst_cmsSupplier.supplier_id as supplier_id'
									,'mst_supplier.name as name'
									,'mst_supplier.supplier_logo as supplier_logo'
									,'mst_cmsSupplier.item_id_1 as itemid_slot1'
									,'mst_cmsSupplier.item_id_2 as itemid_slot2'
									,'mst_cmsSupplier.item_id_3 as itemid_slot3'
									,'mst_cmsSupplier.item_id_4 as itemid_slot4'
									,'mst_cmsSupplier.item_id_5 as itemid_slot5'
									,'mst_cmsSupplier.item_id_6 as itemid_slot6'
									,'mst_cmsSupplier.item_id_7 as itemid_slot7'
									,'mst_cmsSupplier.item_id_8 as itemid_slot8'
									)
							->leftJoin('mst_supplier', 'mst_cmsSupplier.supplier_id', '=', 'mst_supplier.id')
							->whereNull('mst_cmsSupplier.deleted_at')
							->get();
        //dd($getCmsSuppliers);
        $supplierlist = [];
        foreach ($getCmsSuppliers as $supplier) {
            $supplierdetails = DB::select('exec getCMSSupplierDetail ?,?', array($agen_id, $supplier->supplier_id));
            $supplierdetailList = [];
            foreach ($supplierdetails as $supplierdetail) {
                $supplierunits = DB::select('getCMSItemDetailById ?,?', array($agen_id, $supplierdetail->item_id));
                $units = [];
                foreach ($supplierunits as $supplierunit) {
                    
                    $units[] = [
                        'marketplace_id' => $supplierunit->id,
                        'item_id' => $supplierunit->item_id,
                        'item_esta_code' => $supplierunit->item_esta_code,
                        'item_sku' => $supplierunit->item_sku,
                        'itemunit_id' => $supplierunit->itemunit_id,
                        'itemunit_name' => $supplierunit->itemunit_name,
                        'discount_value' => $supplierunit->discount_value,
                        'discount_type' => $supplierunit->discount_type,
                        'item_after_discount' => $supplierunit->item_after_discount,
                        'item_sale_price' => $supplierunit->item_sale_price,
                        'stock' => $supplierunit->stock,
                        'stock_converted' => $supplierunit->stock_converted,
                        'margin_type' => $supplierunit->margin_type,
                        'margin_value' => $supplierunit->margin_value,
                        'item_buy_price' => $supplierunit->item_buy_price
                    ];
                }
                
                $supplierDB = DB::select('exec getSupplierById ?', array($supplierdetail->supplier_id));
                $explode_logo = explode("/", $supplierDB[0]->supplier_logo);
                $last_index = count($explode_logo)-1;
                $supplier_logoFTP = config('app.esta_ftp').$explode_logo[$last_index];
                $item_photoFTP = config('app.esta_ftp').$supplierdetail->item_photo;
                $item_desc = DB::select('exec getDescripsiItemHome '.$supplierdetail->item_id.'');
                $supplierdetailList[] = [
                    'category_id' => $supplierdetail->category_id,
                    'category_code' => $supplierdetail->category_code,
                    'category_name' => $supplierdetail->category_name,
                    'subcategory_id' => $supplierdetail->subcategory_id,
                    'subcategory_name' => $supplierdetail->subcategory_name,
                    'subcategory_code' => $supplierdetail->subcategory_code,
                    'item_id' => $supplierdetail->item_id,
                    'item_name' => $supplierdetail->item_name,
                    'item_photo' => $item_photoFTP,
                    'item_detail' => $item_desc[0]->detail,
                    'supplier_id' => $supplierdetail->supplier_id,
                    'supplier_name' => $supplierdetail->supplier_name,
                    'supplier_code' => $supplierdetail->supplier_code,
                    'supplier_logo' => $supplier_logoFTP,
                    'units' => $units
                ];
            }   

            $explode_logo = explode("/", $supplier->supplier_logo);
            $last_index = count($explode_logo)-1;
            $logo_name = $explode_logo[$last_index];
            $supplier_logoFTP = config('app.esta_ftp').$logo_name;
            $supplierlist[] = [
                'id' => $supplier->supplier_id,
                'name' => $supplier->name,
                'supplier_logo' => $supplier_logoFTP,
                'details' => $supplierdetailList
            ];
        }

        $data['category'] = $categorylist;
        $data['extend_category'] = $categorylistextend;
        $data['supplier'] = $supplierlist;
        return $this->respondWithDataAndMessage($data, "Success");
    }
    public function getAllSupplier(){
        $suppliers = DB::select('exec getAllSupplier');
        $data = [];
        foreach ($suppliers as $supplier) {
            $explode_logo = explode("/", $supplier->supplier_logo);
            $last_index = count($explode_logo)-1;
            $logo_name = $explode_logo[$last_index];
            $supplier_logoFTP = config('app.esta_ftp').$logo_name;
            $data[] = [
                'id' => $supplier->id,
                'code' => $supplier->code,
                'name' => $supplier->name,
                'email' => $supplier->email,
                'phone' => $supplier->phone,
                'supplier_logo' => $supplier_logoFTP,
                'min_purchase_unit' => $supplier->min_purchase_unit,
                'min_purchase' => $supplier->min_purchase,
                'max_arrival_date' => $supplier->max_arrival_date,
                'delivery_price' => $supplier->delivery_price
            ];
        }
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getAllCategory(){
        $data = [];
            $marketplaces = DB::select('exec getAllCategory');
            foreach ($marketplaces as $category) {
                $data[] = [
                    'id' => $category->id,
                    'name' => $category->name,
                    'code' => $category->code,
                    'category_logo' => $category->photo                    
                ];
            }
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getItemByParams(){
        $agen = Auth::user();
        $agen_id = $agen->id;

        //$agen_id = '664';
        $supplier_id = Request::get('supplier_id');
        $category_id = Request::get('category_id');
        $subcategory_id = Request::get('subcategory_id');
        $query = Request::get('query');
        $limit = Request::get('limit');
        $page = Request::get('page');
        $sort = Request::get('sort');
        $filters = Request::get('filter');
        $min_price = $filters['min_price'];
        $max_price = $filters['max_price'];
        
        if($min_price > $max_price){
            $min_price = $filters['max_price'];
            $max_price = $filters['min_price'];
        }
        if($max_price != NULL){
            $filter['min_price'] = $min_price;
            $filter['max_price'] = $max_price;
        }else{
            $marketplace_Price = DB::select('exec getFilterPriceMarketplace ?,?,?,?,?,?,?,?,?,?,?', 
                array($agen_id,$supplier_id,$category_id,$subcategory_id,$query,$sort,$min_price,$max_price,$categoryArr,$supplierArr,$unitArr));
            $filter['min_price'] = '0';
            $filter['max_price'] = $marketplace_Price[0]->max_price;
        }
		
		$supplierById = DB::select('exec getSupplierMarketplaceById ?,?', array($agen_id,$supplier_id));
        $supplierFilter = [];
        foreach ($supplierById as $supplier) {
            $supplierFilter[] = [
                'id' => $supplier->supplier_id,
                'name' => $supplier->supplier_name
            ];
        }            
        $filter['suppliers'] = $supplierFilter;

        $units = DB::select('exec getUnitsMarketplaceById ?', array($agen_id));
        $unitsFilter = [];
        foreach ($units as $unit){
            $unitsFilter[] = [
                'id' => $unit->itemunit_id,
                'name' => $unit->itemunit_name
            ];
        }
        $filter['units'] = $unitsFilter;

        $categories = $filters['categories'];
        if($categories != NULL){
            $category_idarr = [];
            foreach ($categories as $category) {
                $id = $category['id'];
                array_push($category_idarr,$id);
            }                
            $categoryArr = implode(",",$category_idarr);
        }
        $suppliers = $filters['suppliers'];
        if($suppliers != NULL){            
            $supplier_idarr = [];
            foreach ($suppliers as $supplier) {
                $id = $supplier['id'];
                array_push($supplier_idarr,$id);
            }
            $supplierArr = implode(",",$supplier_idarr);
        }
        $units = $filters['units'];
        if($units != NULL){            
            $unit_idarr = [];
            foreach ($units as $unit) {
                $id = $unit['id'];
                array_push($unit_idarr, $id);
            }
            $unitArr = implode(",", $unit_idarr);
        }

        $txnMarketplaces = DB::select('exec getItemByParams ?,?,?,?,?,?,?,?,?,?,?', 
            array($agen_id,$supplier_id,$category_id,$subcategory_id,$query,$sort,$min_price,$max_price,$categoryArr,$supplierArr,$unitArr));
			//dd(array($agen_id,$supplier_id,$category_id,$subcategory_id,$query,$sort,$min_price,$max_price,$categoryArr,$supplierArr,$unitArr));

        $transactions = [];
        foreach ($txnMarketplaces as $txnMarketplace) {
            $marketplacedb = DB::select('exec getCategoryBySupplier ?,?', array($agen_id,$txnMarketplace->supplier_id));
            $categoryFilter = [];
            foreach ($marketplacedb as $category) {
                $categoryFilter[] = [
                    'id' => $category->category_id,
                    'name' => $category->category_name
                ];
            }            
            $filter['categories'] = $categoryFilter;            
            

            if($supplier_id != NULL){
                $categorylist = [];
                foreach ($marketplacedb as $category) {
                    $categorylist[] = [
                        'id' => $category->category_id,
                        'name' => $category->category_name,
                        'code' => $category->category_code
                    ];
                }                
            }
            if($category_id != NULL){
                $subcategorydb = DB::select('exec getSubcategoryById ?', array($txnMarketplace->category_id));
                $subcategorylist = [];
                foreach ($subcategorydb as $subcategory) {
                    $subcategorylist[] = [
                        'id' => $subcategory->id,
                        'name' => $subcategory->name,
                        'subcategory_logo' => $subcategory->photo,
                        'code' => $subcategory->code
                    ];
                }
            }

            $unitsdb = DB::select('exec getItemDetailByEstaCode ?,?', array($agen_id, $txnMarketplace->item_esta_code));
            $units = [];
            foreach ($unitsdb as $unit) {
                $units[] = [
                    'marketplace_id' => $unit->id,
                    'item_id' => $unit->item_id,
                    'item_esta_code' => $unit->item_esta_code,
                    'item_sku' => $unit->item_sku,
                    'itemunit_id' => $unit->itemunit_id,
                    'itemunit_name' => $unit->itemunit_name,
                    'discount_value' => $unit->discount_value,
                    'discount_type' => $unit->discount_type,
                    'item_after_discount' => $unit->item_after_discount,
                    'item_sale_price' => $unit->item_sale_price,
                    'stock' => $unit->stock,
                    'stock_converted' => $unit->stock_converted,
                    'margin_type' => $unit->margin_type,
                    'margin_value' => $unit->margin_value,
                    'item_buy_price' => $unit->item_buy_price
                ];
            }
            $item_desc = DB::select('exec getDescripsiItem '.$txnMarketplace->item_esta_code.'');
            $transactions[] = [                
                'item_name' => $txnMarketplace->item_name,
                'item_photo' => $txnMarketplace->item_photo,
                'supplier_id' => $txnMarketplace->supplier_id,
                'supplier_code' => $txnMarketplace->supplier_code,
                'supplier_name' => $txnMarketplace->supplier_name,
                'supplier_logo' => $txnMarketplace->supplier->supplier_logo,
                'category_id' => $txnMarketplace->category_id,
                'category_code' => $txnMarketplace->category_code,
                'category_name' => $txnMarketplace->category_name,
                'subcategory_id' => $txnMarketplace->subcategory_id,
                'subcategory_code' => $txnMarketplace->subcategory_code,

                'subcategory_name' => $txnMarketplace->subcategory_name,
                'item_detail' => $item_desc[0]->detail,
                'item_esta_code_last9digit' => $txnMarketplace->item_esta_code,
                'units' => $units
            ];     
        }        

        

        $data['filter'] = $filter;
        $data['category'] = $categorylist;
        $data['subcategory'] = $subcategorylist;
        
        $total = count($transactions);

        //$currentPage = LengthAwarePaginator::resolveCurrentPage();
        if($limit != NULL){
            $perPage = $limit;
            //$currentResults = $article->slice(($page - 1) * $perPage, $perPage)->all();
            // if($perPage * ($page - 1) * $perPage > $total) {
            //     $perPage = ($perPage * ($page - 1) * $perPage) - $total;
            //     echo ($perPage * ($page - 1) * $perPage) . ' ' . $total; exit();
            // }
            $currentResults = array_slice($transactions, ($page - 1) * $perPage, $perPage);
            $transactions = $currentResults;
            //$pageInf = ($currentResults, $article->count(), $perPage);
        }
        $data['items'] = $transactions;
        //$data['page'] = $pageInf;
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getItemByEstacode(Request $request, $esta_code9digit){
        $agen = Auth::user();
        $agen_id = $agen->id;

        //$agen_id = '664';

        $itemdb = DB::select('exec getItemByEstaCode ?', array($esta_code9digit));
        //$items = [];
        foreach ($itemdb as $item) {
            $unitsdb = DB::select('exec getItemDetailByEstaCode '.$agen_id.','.$esta_code9digit.'');
            $item_desc = DB::select('exec getDescripsiItem '.$esta_code9digit.'');
            //dd($unitsdb);        
            $units = [];
            foreach ($unitsdb as $unit) {
                $units[] = [
                    'marketplace_id' => $unit->id,
                    'item_id' => $unit->item_id,
                    'item_esta_code' => $unit->item_esta_code,
                    'item_sku' => $unit->item_sku,
                    'itemunit_id' => $unit->itemunit_id,
                    'itemunit_name' => $unit->itemunit_name,
                    'discount_value' => $unit->discount_value,
                    'discount_type' => $unit->discount_type,
                    'item_after_discount' => $unit->item_after_discount,
                    'item_sale_price' => $unit->item_sale_price,
                    'stock' => $unit->stock,
                    'stock_converted' => $unit->stock_converted,
                    'margin_type' => $unit->margin_type,
                    'margin_value' => $unit->margin_value,
                    'item_buy_price' => $unit->item_buy_price
                ];
            }
            $items['item_name'] = $item->item_name;
            $items['item_photo'] = $item->item_photo;
            $items['supplier_id'] = $item->supplier_id;
            $items['supplier_code'] = $item->supplier_code;
            $items['supplier_name'] = $item->supplier_name;
            $items['supplier_logo'] = $item->supplier->supplier_logo;
            $items['category_id'] = $item->category_id;
            $items['category_code'] = $item->category_code;
            $items['category_name'] = $item->category_name;
            $items['subcategory_id'] = $item->subcategory_id;
            $items['subcategory_code'] = $item->subcategory_code;
            $items['subcategory_name'] = $item->subcategory_name;
            $items['item_detail'] = $item_desc[0]->detail;
            $items['item_esta_code_last9digit'] = $item->item_esta_code;
            $items['units'] = $units;
        }
        return $this->respondWithDataAndMessage($items, "Success");
    }

    public function postBuyItem(){
        $agen = Auth::user();
        $agen_id = $agen->id;
        
        //$agen_id = Request::get('agen_id');
        $ongkir = Request::get('ongkir');
        $supplier_id = Request::get('supplier_id');
        $supplier_code = Request::get('supplier_code');
        $supplier_name = Request::get('supplier_name');
        $transaction_delivery = Request::get('transaction_delivery');
        $transaction_delivery_address = Request::get('transaction_delivery_address');
        $transaction_total_price = Request::get('transaction_total_price');
        $voucher_id = Request::get('voucher_id');
        $voucher_value = Request::get('voucher_value');
        $droppoint_id = Request::get('droppoint_id');
        $items = Request::get('items');

        $current_status = 'NEW TRANSACTION';

        $prefix_po = CRUDBooster::getsetting('po_number');
        $lastIncrement = DB::select('exec getTransactionReceiptLast');
        $po_number = $prefix_po.sprintf("%06s",substr($lastIncrement[0]->po_number, 6, -4)+1).date('y').date('m');
        //dd($po_number);
        $transactioncode = $lastIncrement[0]->transaction_code + 1;

        $transaction_save_id = DB::select('exec transactionBuyItemNew ?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($agen_id,$supplier_id,$supplier_code,$supplier_name
            ,$ongkir,$transaction_delivery,$transaction_delivery_address,$transaction_total_price
            ,$voucher_id,$voucher_value,$current_status,$po_number,$transactioncode,$droppoint_id));

        
        foreach ($items as $item) {   
            $transaction_id = $transaction_save_id[0]->id;      
            $marketplace_id = $item['marketplace_id'];
            $item_id = $item['item_id'];
            $item_esta_code = $item['item_esta_code'];
            $item_name = $item['item_name'];
            $item_sku = $item['item_sku'];
            $item_photo = $item['item_photo'];
            $buy_quantity = $item['buy_quantity'];
            $itemunit_id = $item['itemunit_id']; 
            $itemunit_name = $item['itemunit_name'];
            $item_sale_price = $item['item_sale_price'];
            $item_after_discount = $item['item_after_discount'];
            $discount_type = $item['discount_type'];
            $discount_value = $item['discount_value'];
            $margin_type = $item['margin_type'];
            $margin_value = $item['margin_value'];
            $item_buy_price = $item['item_buy_price'];
            $category_id = $item['category_id'];
            $category_code = $item['category_code'];
            $category_name = $item['category_name'];
            $subcategory_id = $item['subcategory_id'];
            $subcategory_code = $item['subcategory_code'];
            $subcategory_name = $item['subcategory_name'];
                             
            $transaction_detail_save = DB::statement('exec transactionBuyItemDetailNew ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
                , array($transaction_id,$marketplace_id,$item_id,$item_esta_code,$item_name,$item_sku,$item_photo
                ,$buy_quantity,$itemunit_id,$itemunit_name,$item_sale_price,$item_after_discount,$discount_type,$discount_value,$margin_type,$margin_value
                ,$item_buy_price,$category_id,$category_code,$category_name,$subcategory_id,$subcategory_code,$subcategory_name));

        }
        //dd($transaction_total_price);
        $data['id'] = $transaction_save_id[0]->id;
        if($transaction_detail_save){
            return $this->respondWithDataAndMessage($data,'Data Berhasil Di Submit');
        }

    }

    public function postVoucherKios() {
        //$id_agen = Request::get('id_agen');
        $agen = Auth::user();
        $id_agen = $agen->id;

        /*$detail_agen = DB::table('agen')
            ->where('id',$id_agen)
            ->first();*/

        $arrays = DB::table('trans_voucher_child')
            ->whereIN('product',['KIOS','All'])
            ->where('id_agen',$id_agen)
            ->where('used','No')
            ->where('voucher_expired','>=',date('Y-m-d H:i:s'))
            ->orderBy('id','desc')
            ->get();

        $rest_json = array();
        foreach($arrays as $array) {
            $rest['id'] = $array->id;
            $rest['nama'] = $array->voucher_nama;
            $rest['amount'] = $array->voucher_amount;
            $rest['masa_berlaku'] = date("Y-m-d H:i:s", strtotime($array->voucher_expired));
            array_push($rest_json, $rest);
        }
        //$response['items'] = $rest_json;

        return $this->respondWithDataAndMessage($rest_json, 'Success');
    }

    public function getDropPoint(){
        $droppoints = DB::select('exec getDropPoint');
        $droppointarr = [];
        foreach ($droppoints as $droppoint) {
            $jarak = '± '.substr($droppoint->jarak, 0, 4).' km';
            $droppointarr[] = [
                'id' => $droppoint->id,
                'code' => $droppoint->code,
                'nama_droppoint' => $droppoint->nama,
                'lat' => $droppoint->lat,
                'lng' => $droppoint->lng,
                'alamat_lengkap' => $droppoint->alamat_lengkap,
                'jarak' => $jarak,
                'rt' => $droppoint->rt,
                'rw' => $droppoint->rw,
                'address' => $droppoint->alamat,
                'kelurahan_id' => $droppoint->id_i_kelurahan,
                'kecamatan_id' => trim($droppoint->id_i_kecamatan),
                'kabupaten_id' => $droppoint->id_i_kabupaten,
                'provinsi_id' => $droppoint->id_i_provinsi
            ];
        }
        return $this->respondWithDataAndMessage($droppointarr, 'Success');
    }

    public function getCheckPoint(){
        $agenid = Auth::user();
        $agen_id = $agenid->id;

        $supplier_id = Request::get('supplier_id');

        $suppliers = DB::select('exec getSupplierById ?', array($supplier_id));
        foreach ($suppliers as $supplier) {
            $supparr['id'] = $supplier->id;
            $supparr['code'] = $supplier->code;
            $supparr['name'] = $supplier->name;
            $supparr['supplier_logo'] = $supplier->supplier_logo;
            $supparr['email'] = $supplier->email;
            $supparr['phone'] = $supplier->phone;
            $supparr['min_purchase_unit'] = $supplier->min_purchase_unit;
            $supparr['min_purchase'] = $supplier->min_purchase;
            $supparr['max_arrival_date'] = $supplier->max_arrival_date;
            $supparr['delivery_price'] = $supplier->delivery_price;
        }
        $agens = DB::select('exec getAgenKiosById ?', array($agen_id));
        //dd($agens);
        $agen = $agens[0];
        $agenarr['id_agen'] = $agen->agen_id;
        $agenarr['nama_agen'] = $agenid->nama;
        $agenarr['nama_kios'] = $agen->nama_kios;
        $agenarr['alamat_lengkap'] = $agen->store_address;
        $agenarr['lat'] = $agen->lat;
        $agenarr['lng'] = $agen->lng;
        $agenarr['rt'] = $agen->RT;
        $agenarr['rw'] = $agen->RW;
        //$agenarr['address'] = $agen->;
        $agenarr['kelurahan_id'] = $agen->kelurahan_id;
        $agenarr['kecamatan_id'] = $agen->kecamatan_id;
        $agenarr['kabupaten_id'] = $agen->kabupaten_id;
        $agenarr['provinsi_id'] = $agen->provinsi_id;

        $data['supplier'] = $supparr;
        $data['agen'] = $agenarr;
        return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function getSaldo(){
        $agen = Auth::user();
        $agen_id = $agen->id;
        $checksaldo = DB::table('saldo')->where('agen_id', $agen_id)->first();
        $data['balance'] = $checksaldo->balance;
        $data['points'] = $checksaldo->points;
        return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function getBantuanCS(){
        $noWa = DB::select('exec getBantuanCS');
        $data = $noWa;
        return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function getBantuanInformasi(){
        $bantuan_Informasi = DB::select('exec getBantuanInformasi');
        return $this->respondWithDataAndMessage($bantuan_Informasi, 'Success');
    }

    public function postHistoryNewTransaction(){
        $agen = Auth::user();
        $agen_id = $agen->id;
        $transaction_code = Request::get('transaction_code');
        if(Request::get('start_date') == NULL && Request::get('end_date') == NULL){
            $start_date = Request::get('start_date');
            $end_date = Request::get('end_date');
        }else{
            $start_date = "'".Request::get('start_date')."'";
            $end_date = "'".Request::get('end_date')."'";
        }

        $supplier_id = Request::get('supplier_id');
        //dd('exec getHistoryNewTransaction ?,?,?,?,?', array($agen_id,$transaction_code, $start_date, $end_date, $supplier_id));
        $transactionsSP = DB::select('exec getHistoryNewTransaction ?,?,?,?,?', array($agen_id,$transaction_code, $start_date, $end_date, $supplier_id));

        //dd($transactionsSP);
        $data=[];
        foreach ($transactionsSP as $newTransaction) {
            $data[] = [
                'id'=>$newTransaction->id,
                'transaction_date'=>$newTransaction->transaction_date,
                'transaction_code'=>$newTransaction->transaction_code,
                'current_status'=>$newTransaction->current_status,
                'jumlah_produk'=>$newTransaction->jumlah_produk,
                'supplier_name'=>$newTransaction->supplier_name,
                'total_price'=>$newTransaction->transaction_total_price
            ];
        }
        return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function postHistoryTransactionFinish(){
        $agen = Auth::user();
        $agen_id = $agen->id;
        $transaction_code = Request::get('transaction_code');
        if(Request::get('start_date') == NULL && Request::get('end_date') == NULL){
            $start_date = Request::get('start_date');
            $end_date = Request::get('end_date');
        }else{
            $start_date = "'".Request::get('start_date')."'";
            $end_date = "'".Request::get('end_date')."'";
        }

        $supplier_id = Request::get('supplier_id');
        $transactionsSP = DB::select('exec getHistoryTransactionFinish ?,?,?,?,?', array($agen_id,$transaction_code, $start_date, $end_date, $supplier_id));

        $data=[];
        foreach ($transactionsSP as $newTransaction) {
            $data[] = [
                'id'=>$newTransaction->id,
                'transaction_date'=>$newTransaction->transaction_date,
                'transaction_code'=>$newTransaction->transaction_code,
                'current_status'=>$newTransaction->current_status,
                'jumlah_produk'=>$newTransaction->jumlah_produk,
                'supplier_name'=>$newTransaction->supplier_name,
                'total_price'=>$newTransaction->transaction_total_price
            ];
        }
        return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function postHistoryTransactionCancel(){
        $agen = Auth::user();
        $agen_id = $agen->id;
        $transaction_code = Request::get('transaction_code');
        if(Request::get('start_date') == NULL && Request::get('end_date') == NULL){
            $start_date = Request::get('start_date');
            $end_date = Request::get('end_date');
        }else{
            $start_date = "'".Request::get('start_date')."'";
            $end_date = "'".Request::get('end_date')."'";
        }

        $supplier_id = Request::get('supplier_id');
        $transactionsSP = DB::select('exec getHistoryTransactionCancel ?,?,?,?,?', array($agen_id,$transaction_code, $start_date, $end_date, $supplier_id));

        $data=[];
        foreach ($transactionsSP as $newTransaction) {
            $data[] = [
                'id'=>$newTransaction->id,
                'transaction_date'=>$newTransaction->transaction_date,
                'transaction_code'=>$newTransaction->transaction_code,
                'current_status'=>$newTransaction->current_status,
                'jumlah_produk'=>$newTransaction->jumlah_produk,
                'supplier_name'=>$newTransaction->supplier_name,
                'total_price'=>$newTransaction->transaction_total_price
            ];
        }
        return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function postHistoryTransactionClaim(){
        $agen = Auth::user();
        $agen_id = $agen->id;
        $transaction_code = Request::get('transaction_code');
        if(Request::get('start_date') == NULL && Request::get('end_date') == NULL){
            $start_date = Request::get('start_date');
            $end_date = Request::get('end_date');
        }else{
            $start_date = "'".Request::get('start_date')."'";
            $end_date = "'".Request::get('end_date')."'";
        }

        $supplier_id = Request::get('supplier_id');
        $transactionsSP = DB::select('exec getHistoryTransactionClaim ?,?,?,?,?', array($agen_id,$transaction_code, $start_date, $end_date, $supplier_id));

        $data=[];
        foreach ($transactionsSP as $newTransaction) {
            $data[] = [
                'id'=>$newTransaction->transaction_id,
                'transaction_date'=>$newTransaction->transaction_date,
                'transaction_code'=>$newTransaction->transaction_code,
                'current_status'=>$newTransaction->current_status,
                'jumlah_produk'=>$newTransaction->jumlah_produk,
                'supplier_name'=>$newTransaction->transaction_supplier_name,
                'total_price'=>$newTransaction->transaction_total_price
            ];
        }
        return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function getTransactionById(Request $request, $id){
        $transactionDb = DB::select('exec getTransactionById ?', array($id));
        //dd($transactionDb);
        if($transactionDb[0]->transaction_delivery == 'DROP_POINT'){
            $delivery_name = $transactionDb[0]->droppoint_name;
            $delivery_lat = $transactionDb[0]->droppoint_lat;
            $delivery_lng = $transactionDb[0]->droppoint_lng;
            $delivery_address = $transactionDb[0]->droppoint_alamat;
            $delivery_provinsi_id = $transactionDb[0]->droppoint_id_i_provinsi;
            $delivery_kabupaten_id = $transactionDb[0]->droppoint_id_i_kabupaten;
            $delivery_kecamatan_id = $transactionDb[0]->droppoint_id_i_kecamatan;
            $delivery_kelurahan_id = $transactionDb[0]->droppoint_id_i_kelurahan;
            $delivery_rt = $transactionDb[0]->droppoint_rt;
            $delivery_rw = $transactionDb[0]->droppoint_rw;
        }else{
            $delivery_name = $transactionDb[0]->kios_name;
            $delivery_lat = $transactionDb[0]->kios_lat;
            $delivery_lng = $transactionDb[0]->kios_lng;
            $delivery_address = $transactionDb[0]->kios_address;
            $delivery_provinsi_id = $transactionDb[0]->kios_provinsi_id;
            $delivery_kabupaten_id = $transactionDb[0]->kios_kabupaten_id;
            $delivery_kecamatan_id = $transactionDb[0]->kios_kecamatan_id;
            $delivery_kelurahan_id = $transactionDb[0]->kios_kelurahan_id;
            $delivery_rt = $transactionDb[0]->kios_rt;
            $delivery_rw = $transactionDb[0]->kios_rw;
        }

        $transactionDetailDb = DB::select('exec getTransactionDetailById ?', array($id));
        $items = [];
        foreach ($transactionDetailDb as $transactionDetail) {
            $item_desc = DB::select('exec getDescripsiItem '.$transactionDetail->item_esta_code.'');
            $items[] = [
                'item_id' => $transactionDetail->item_id,
                'item_name' => $transactionDetail->item_name,
                'item_photo' => $transactionDetail->item_photo,
                'item_detail' => $item_desc[0]->detail,
                'buy_quantity' => $transactionDetail->buy_quantity,
                'itemunit_id' => $transactionDetail->itemunit_id,
                'itemunit_name' => $transactionDetail->itemunit_name,
                'item_sale_price' => $transactionDetail->item_sale_price,
                'item_after_discount' => $transactionDetail->item_after_discount,
                'discount_value' => $transactionDetail->discount_value,
                'discount_type' => $transactionDetail->discount_type
            ];
        }
       
        $data['id'] = $transactionDb[0]->id;
        $data['transaction_date'] = $transactionDb[0]->transaction_date;
        $data['transaction_code'] = $transactionDb[0]->transaction_code;
        $data['current_status'] = $transactionDb[0]->current_status;
        $data['jumlah_produk'] = $transactionDb[0]->jumlah_produk;
        $data['total_price'] = $transactionDb[0]->transaction_total_price;
        $data['supplier_id'] = $transactionDb[0]->supplier_id;
        $data['supplier_name'] = $transactionDb[0]->supplier_name;
        //$data['supplier_logo'] = $transactionDb[0]->supplier_logo;
        $data['delivery_name'] = $delivery_name;
        $data['delivery_lat'] = $delivery_lat;
        $data['delivery_lng'] = $delivery_lng;
        $data['delivery_address'] = $delivery_address;
        $data['delivery_provinsi_id'] = $delivery_provinsi_id;
        $data['delivery_kabupaten_id'] = $delivery_kabupaten_id;
        $data['delivery_kecamatan_id'] = $delivery_kecamatan_id;
        $data['delivery_kelurahan_id'] = $delivery_kelurahan_id;
        $data['delivery_rt'] = $delivery_rt;
        $data['delivery_rw'] = $delivery_rw;
        $data['transaction_delivery_address'] = $transactionDb[0]->delivery_address;
        $data['transaction_delivery'] = $transactionDb[0]->transaction_delivery;
        $data['ongkir'] = $transactionDb[0]->ongkir;
        $data['voucher_id'] = $transactionDb[0]->voucher_id;
        $data['voucher_name'] = $transactionDb[0]->voucher_name;
        $data['voucher_value'] = $transactionDb[0]->voucher_value;
        $data['items'] = $items;
        return $this->respondWithDataAndMessage($data, "Success");
    }

    public function getStock(){
        $stocks = Request::get('stock');
        //$stock_idarr = [];
        //$stock_unitidarr = [];
		$stockArr = [];
        foreach ($stocks as $stock) {
            $item_id = $stock['item_id'];
            $itemunit_id = $stock['itemunit_id'];
			
            $stocks = DB::select('exec getStock ?,?', array($item_id, $itemunit_id));
            foreach ($stocks as $stock) {
                $stockArr[] = [
                    'item_id' => $stock->item_id,
                    'itemunit_id' => $stock->itemunit_id,
                    'stock' => $stock->stock,
                    'stock_converted' => $stock->stock_converted
                ];
				//array_push($stockArr,$stockArrr);
            }
        }                

        /*$itemId = Request::get('item_id');
        $itemunitId = Request::get('itemunit_id');*/

        return $this->respondWithDataAndMessage($stockArr, "Success");
    }
}
