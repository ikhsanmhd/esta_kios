<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Request;
use Route;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;
use Storage;
use Esta;
use App;
use Illuminate\Support\Facades\Auth;

class PdamAPI extends Controller
{
	public function postVoucherPdam() {
		$agen = Auth::user();
        $id_agen = $agen->id;

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen));

		$arrays = DB::select('exec getVoucherWithParams ?,?', array($id_agen,'PDAM'));

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->voucher_nama;
			$rest['amount'] = $array->voucher_amount;
			$rest['masa_berlaku'] = date("Y-m-d H:i:s", strtotime($array->voucher_expired));
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postCheckTagihanPascabayarPdam() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$no_hp = Request::get('no_hp');
		$id_area = Request::get('id_area');
		$no_meter = Request::get('no_meter');

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen));

		$detail_product = DB::table('pan_product')
			->where('id',$id_area)
			->first();

		/*send iso*/
		$send_iso = Esta::send_iso_pdam($id_area,$no_hp,'0200','Inquiry',0, $no_meter);
		//dd($send_iso);		

		if($send_iso['39'] == '00') {
			$rest['jml_tagihan'] = 'Rp '.number_format(ltrim($send_iso['pdam_rupiah_tagihan'],0),0,',','.');
			$rest['komisi'] = 'Rp '.number_format(($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),0,',','.');
			$rest['nama_pelanggan'] = $send_iso['pdam_customer_name'];
			$rest['id_pelanggan'] = $send_iso['pdam_idpel'];
			$rest['id_transaksi'] = $send_iso['id_log'];

			$rest['val_jml_tagihan'] = ltrim($send_iso['pdam_rupiah_tagihan'],0);
			$rest['val_komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$rest['val_biaya_admin'] = ($send_iso['pdam_biaya_admin'] >= 1 ? ltrim($send_iso['pdam_biaya_admin'],0) : 0);
			$rest['biaya_admin'] = 'Rp '.number_format($rest['val_biaya_admin'],0,',','.');
			$rest['pdam_blth'] = $send_iso['pdam_blth'];
			$rest['jml_lembar_tagihan'] = ltrim($send_iso['jml_lembar_tagihan'],0).' BLN';
			$rest['val_total_bayar'] = $rest['val_jml_tagihan']+$rest['val_biaya_admin'];
			$val_total_pembayaran = $rest['val_total_bayar']-$rest['val_komisi'];
			$rest['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

			$rest['total_bayar'] = 'Rp '.number_format($rest['val_total_bayar'],0,',','.');
			$rest['total_pembayaran'] = 'Rp '.number_format($rest['val_total_pembayaran'],0,',','.');

			$response['api_status']  = 1;
	    	$response['api_message'] = 'Berhasil';
	    	$response['type_dialog']  = 'Informasi';
	    	$response['item']  = $rest;
		} else {
			$rest['jml_tagihan'] = 0;
			$rest['komisi'] = 0;

			$response['api_status']  = 0;
	    	$response['api_message'] = Esta::show_error('PDAM',$send_iso['39'],'rino');
	    	$response['type_dialog']  = 'Error';
	    	$response['item']  = $rest;
		}
		return response()->json($response);
	}

	public function postPdamArea() {
		$agen = Auth::user();
        $id_agen = $agen->id;
		$id_kode_biller = 1003;//Request::get('id_kode_biller');
		$id_kode_layanan = 2;//Request::get('id_kode_layanan'); /*1 prabayar, 2 pasca bayar*/

		$detail_agen = DB::select('exec getAgenById ?', array($id_agen))[0];

		$arrays = DB::select('exec getPanProductWithParams ?,?', array($id_kode_layanan,'PDAM'));

		$rest_json = array();
	  	foreach($arrays as $array) {
			$rest['id'] = $array->id;
			$rest['nama'] = $array->product_name;
			//$rest['amount'] = $array->amount;
			$rest['komisi'] = ($detail_agen->status_agen == 'Basic' ? $array->komisi_basic : $array->komisi_premium);
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog'] = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postPdamTransaksiPending() {
		$agen = Auth::user();
        $id_agen = $agen->id;

		$arrays = DB::table('trans_pdam')
			->where('id_agen',$id_agen)
			//->where('deleted_at','!=',NULL)
			->where('status','Pending')
			->whereDay('created_at',date('d'))
			->whereMonth('created_at',date('m'))
			->whereYear('created_at',date('Y'))
			->orderBy('id','DESC')
			->get();

		$rest_json = array();
	  	foreach($arrays as $array) {
	  		$date_tr = Esta::change_date_format($array->created_at,'d M Y');
			$rest['id'] = $array->id;
			$rest['datetime'] = $array->created_at;
			$rest['trans_no'] = $array->trans_no;
			$rest['stan'] = $array->stan;
			$rest['amount'] = 'Rp '.number_format($array->trans_amount,0,',','.');
			$rest['jenis_layanan'] = 'PDAM';
			$rest['nomor'] = $array->id_pelanggan;
			$rest['status'] = ($date_tr == date('d M Y') ? 'Pending' : 'Expired');
			array_push($rest_json, $rest);
	  	}
	  	$response['api_status']  = 1;
	    $response['api_message'] = 'Sukses';
	    $response['type_dialog']  = 'Informasi';
	    $response['items'] = $rest_json;

	  	return response()->json($response);
	}

	public function postPdamPascabayarSubmit() {
		$id_agen = Request::get('id_agen');
        $request_transaksi = Request::get('request_transaksi');
        $transactionId = Request::get('transactionId');
        $request_transaksi = json_decode( $request_transaksi,true );
		/*$agen = Auth::user();
        $id_agen = $agen->id;*/
		$regid = $request_transaksi['regid'];
		$token = $request_transaksi['token'];
		$cek_regid = Esta::cek_regid($id_agen,$regid);

		/*if($cek_regid == 0) {
			$response['api_status']  = 2;
		    $response['api_message'] = 'Terjadi gangguan pada koneksi internet atau Server. Silahkan coba beberapa saat lagi';//'Akun Anda terdeteksi login di HP lain, Silahkan logout dan login kembali untuk melanjutkan transaksi.';
		    $response['type_dialog']  = 'Error';
		    //Esta::add_fraud($id_agen);
		    return response()->json($response);
		}*/

		$no_hp = $request_transaksi['no_hp'];
		$id_product = $request_transaksi['id_area'];
		$id_pelanggan = $request_transaksi['id_pelanggan'];
		//$id_voucher = Request::get('id_voucher');
		$pin = $request_transaksi['pin'];
		$amount = $request_transaksi['amount'];
		$id_transaksi = $request_transaksi['id_transaksi'];

        $getlog_transaction = DB::table('log_sobatku_payment')
            ->where('transactionId',$transactionId)
            ->where('keterangan','Request Payment')
            ->first();
        $oldRefNum = $getlog_transaction->ref_number;
        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

        $ut['is_used'] = 1;
        DB::table('temp_trans_payment')
					->where('transactionId',$transactionId)
					->update($ut);

		$id_voucher_child = $request_transaksi['id_voucher'];
		$id_voucher = DB::table('trans_voucher_child')
			->where('id',$id_voucher_child)
			->first()->id_voucher;

		$detail_agen = DB::table('agen')
			->where('id',$id_agen)
			->first();

		/*if(hash::check($pin, $detail_agen->password)){
			
		} else {
			$response['api_status']  = 2;
	    	$response['api_message'] = 'PIN anda salah';
	    	$response['type_dialog']  = 'Error';
	    	return response()->json($response);
	    	exit();
		}*/

			$detail_product = DB::table('pan_product')
				->where('id',$id_product)
				->first();

			$detail_voucher = DB::table('voucher')
				->where('id',$id_voucher)
				->first();

			$kode = Esta::nomor_transaksi('trans_pdam',CRUDBooster::getsetting('transaksi_pdam'));

			$detail_inquiry = DB::table('log_jatelindo_bit')
				->where('id',$id_transaksi)
				->where('type','PDAM Postpaid')
				->where('status','Inquiry')
				->where('jenis','res')
				->first();

			$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);
			$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
			$biaya_admin = $detail_inquiry->pdam_biaya_admin;
			if($trans_amount == 0) {
				$trans_amount = 0;
			} else {
				$trans_amount = $trans_amount+$biaya_admin;
			}
			$trans_amount = $trans_amount-$komisi;

			$status_match_sobatku = 'Waiting Rekon';
			//if($saldo >= $trans_amount) {
				try{
					$sv['created_at'] = date('Y-m-d H:i:s');
					$sv['created_user'] = Esta::user($id_agen);
					$sv['updated_user'] = Esta::user($id_agen);
					$sv['trans_no'] = $kode;
					$sv['ref_trans_no'] = $kode;
					$sv['trans_date'] = date('Y-m-d H:i:s');
					$sv['trans_desc'] = 'PDAM PASCABAYAR';
					$sv['currency'] = 'IDR';
					$sv['trans_amount'] = $amount;
					$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$sv['create_user'] = $detail_agen->nama;
					$sv['id_agen'] = $id_agen;
					$sv['status'] = 'Pending';
					$sv['rekon_amount'] = 0;
					$sv['status_match'] = 'Waiting Rekon';
					$sv['no_hp'] = $no_hp;
					$sv['id_pelanggan'] = $id_pelanggan;
					$sv['id_product'] = $id_product;
					$sv['agen_nama'] = $detail_agen->nama;
					$sv['agen_email'] = $detail_agen->email;
					$sv['agen_no_hp'] = $detail_agen->no_hp;
					$sv['agen_kode'] = $detail_agen->kode;
					$sv['agen_level'] = $detail_agen->status_agen;
					$sv['agen_referall'] = $detail_agen->kode_referall_agen;
					$sv['agen_referall_relation'] = $detail_agen->kode_relation_refetrall;
					$sv['agen_status_aktif'] = $detail_agen->status_aktif;
					$sv['agen_nik'] = $detail_agen->nik;
					$sv['agen_tgl_register'] = $detail_agen->tgl_register;
					$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
					$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
					$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
					$sv['agen_agama'] = $detail_agen->agama;
					$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
					$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
					$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
					$sv['agen_prov'] = $detail_agen->prov;
					$sv['agen_kab'] = $detail_agen->kab;
					$sv['agen_kec'] = $detail_agen->kec;
					$sv['agen_kel'] = $detail_agen->kel;
					$sv['product_nama'] = $detail_product->product_name;
					$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$sv['product_komisi_basic'] = $detail_product->komisi_basic;
					$sv['product_komisi_premium'] = $detail_product->komisi_premium;
					$sv['product_amount'] = $amount;
					//$sv['product_amount'] = $trans_amount-$biaya_admin+$komisi;
					$sv['product_margin'] = $detail_product->margin;
					$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
					$sv['product_biaya_admin_1'] = $detail_product->admin_1;
					$sv['product_biaya_admin_2'] = $detail_product->admin_2;
					$sv['product_biaya_Admin_3'] = $detail_product->admin_3;
					$sv['product_potongan'] = $detail_product->potongan;
					$sv['voucher_nama'] = $detail_voucher->nama;
					$sv['voucher_expired_date'] = $detail_voucher->expired_date;
					$sv['voucher_amount'] = $detail_voucher->amount;
					$sv['voucher_tagline'] = $detail_voucher->tagline;
					$sv['voucher_description'] = $detail_voucher->description;
					$sv['id_voucher'] = $detail_voucher->id;
					$sv['no_ref_sobatku'] = $oldRefNum;
					$sv['transactionId_sobatku'] = $transactionId;
					$sv['status_match_sobatku'] = $status_match_sobatku;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pdam');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					/*$save = DB::table('trans_pdam')
						->insertGetId($sv);*/
					/* $saveTransPdamPascabayar = DB::select('exec postTransPdamPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								, array_values($sv));
					$save = $saveTransPdamPascabayar[0]->id; */
				} catch(\Exception $e) {
					//sleep(1);
					$kode = Esta::nomor_transaksi('trans_pdam',CRUDBooster::getsetting('transaksi_pdam'));
					$sv['created_at'] = date('Y-m-d H:i:s');
					$sv['created_user'] = Esta::user($id_agen);
					$sv['updated_user'] = Esta::user($id_agen);
					$sv['trans_no'] = $kode;
					$sv['ref_trans_no'] = $kode;
					$sv['trans_date'] = date('Y-m-d H:i:s');
					$sv['trans_desc'] = 'PDAM PASCABAYAR';
					$sv['currency'] = 'IDR';
					$sv['trans_amount'] = $amount;
					$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$sv['create_user'] = $detail_agen->nama;
					$sv['id_agen'] = $id_agen;
					$sv['status'] = 'Pending';
					$sv['rekon_amount'] = 0;
					$sv['status_match'] = 'Waiting Rekon';
					$sv['no_hp'] = $no_hp;
					$sv['id_pelanggan'] = $id_pelanggan;
					$sv['id_product'] = $id_product;
					$sv['agen_nama'] = $detail_agen->nama;
					$sv['agen_email'] = $detail_agen->email;
					$sv['agen_no_hp'] = $detail_agen->no_hp;
					$sv['agen_kode'] = $detail_agen->kode;
					$sv['agen_level'] = $detail_agen->status_agen;
					$sv['agen_referall'] = $detail_agen->kode_referall_agen;
					$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
					$sv['agen_status_aktif'] = $detail_agen->status_aktif;
					$sv['agen_nik'] = $detail_agen->nik;
					$sv['agen_tgl_register'] = $detail_agen->tgl_register;
					$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
					$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
					$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
					$sv['agen_agama'] = $detail_agen->agama;
					$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
					$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
					$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
					$sv['agen_prov'] = $detail_agen->prov;
					$sv['agen_kab'] = $detail_agen->kab;
					$sv['agen_kec'] = $detail_agen->kec;
					$sv['agen_kel'] = $detail_agen->kel;
					$sv['product_nama'] = $detail_product->product_name;
					$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$sv['product_komisi_basic'] = $detail_product->komisi_basic;
					$sv['product_komisi_premium'] = $detail_product->komisi_premium;
					$sv['product_amount'] = $amount;
					//$sv['product_amount'] = $trans_amount-$biaya_admin+$komisi;
					$sv['product_margin'] = $detail_product->margin;
					$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
					$sv['product_biaya_admin_1'] = $detail_product->admin_1;
					$sv['product_biaya_admin_2'] = $detail_product->admin_2;
					$sv['product_biaya_Admin_3'] = $detail_product->admin_3;
					$sv['product_potongan'] = $detail_product->potongan;
					$sv['voucher_nama'] = $detail_voucher->nama;
					$sv['voucher_expired_date'] = $detail_voucher->expired_date;
					$sv['voucher_amount'] = $detail_voucher->amount;
					$sv['voucher_tagline'] = $detail_voucher->tagline;
					$sv['voucher_description'] = $detail_voucher->description;
					$sv['id_voucher'] = $detail_voucher->id;
					$sv['no_ref_sobatku'] = $oldRefNum;
					$sv['transactionId_sobatku'] = $transactionId;
					$sv['status_match_sobatku'] = $status_match_sobatku;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pdam');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					/*$save = DB::table('trans_pdam')
						->insertGetId($sv);*/
					/* $saveTransPdamPascabayar = DB::select('exec postTransPdamPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								, array_values($sv));
					$save = $saveTransPdamPascabayar[0]->id; */
				}

				/*Get Id Table Trans*/
				$save = DB::table('trans_pdam')
							->where('transactionId_sobatku',$transactionId)
							->first()->id;

				if($save) {
					/*potong saldo*/
					//sleep(1);
					$tr_out = Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PDAM','Transaksi PDAM','Out','Transaksi','trans_pdam',$save,$saldo_sebelum-$trans_amount);
					$cb_in = Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi PDAM','Komisi Transaksi PDAM','In','Komisi','trans_pdam',$save,$saldo_sebelum);

					/*send iso*/
					$send_iso = Esta::send_iso_pdam($id_product,$no_hp,'0200','Payment',$id_transaksi, $no_meter);
					
					//dd($send_iso);
					/*print_r($send_iso);
					exit();*/

					if($send_iso['status'] == 'Reversal' || $send_iso['status'] == 'Reversal Repeat' || $send_iso['status'] == 'Reversal Repeat 1'/* || $send_iso['status'] == 'Payment'*/) {

						if($send_iso['39'] == '00' || $send_iso['39'] == '94') {
							/*reversal sobatku*/
							$start_time = time();
							//$refrn = 0;
							while(true){
								
								//$refrn++;
								$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
								//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
								$user = CRUDBooster::getsetting('user_sobatku_api');
								$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

								$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
								$keterangan = 'Reversal Payment';
								$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
								$referenceNumber = $referenceNumber[0]->Prefix;
								$request = [
									'oldRefNum' => $oldRefNum,
									'referenceNumber' => $referenceNumber,
									'user' => $user,
									'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
								];

								$data_toJson = json_encode( $request );
								$ch = curl_init( $serviceURL );
								curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
								curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
								curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
								curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
										'Content-Type: application/json',
										'Accept:application/json'
									)
								);
								curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
								curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
								$responsereversal = curl_exec( $ch );
								$responseerror = curl_errno($ch);
								if ($responseerror == 28){
									$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
									break;
								}else {
									$responseBody = json_decode( $responsereversal, true);													

									if($responseBody['responseCode'] == '00' ){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										
										/*kembalikan saldo*/
										Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PDAM','Return Transaksi PDAM','In','Transaksi','trans_pdam',$save,$saldo_sebelum-$trans_amount);
										Esta::log_money($id_agen,$sv['komisi'],date('Y-m-d H:i:s'),'Komisi Transaksi PDAM','Return Komisi Transaksi PDAM','Out','Komisi','trans_pdam',$save,$saldo_sebelum);
										break;
									}elseif($responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3' || $responseBody['responseCode'] == 'P1'){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										break;
									}elseif((time() - $start_time) > 60){
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
										break;
									}else{
										$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
									}
								}
								
								curl_close($ch);
								sleep(10);
							}

							$return_saldo = 'Yes';
							$flag_reversal = 'Reversal Sukses';
							$status = 'Pending';
							//$up['status_match'] = 'Pending';
							$error_code = $send_iso['39'];
							$stan = $send_iso['11'];
							$jpa_ref = $send_iso['pdam_no_ref_biller'];

							$update = DB::statement('exec updateTransPDAMPascabayarReversal ?,?,?,?,?,?,?', array($save,$return_saldo,$flag_reversal,$status,$error_code,$stan,$jpa_ref));
							
							if(!empty($id_voucher)) {
								$uv['used'] = 'No';

								$up_voucher = DB::table('trans_voucher_child')
									->where('id',$id_voucher_child)
									->where('id_agen',$id_agen)
									->update($uv);
							}

							$response['nama_pelanggan']  = $send_iso['pdam_customer_name'];
							$response['pdam_no_ref_biller']  = $send_iso['pdam_no_ref_biller'];
					    	//$response['pdam_blth']  = $send_iso['pdam_blth'];
					    	$response['tagihan']  = 'Rp '.number_format(str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0)),0,',','.');
					    	$response['biaya_admin']  = 'Rp '.number_format(ltrim($send_iso['pdam_biaya_admin'],0),0,',','.');

							$response['val_tagihan']  = str_replace('-', '', ltrim($send_iso['pdam_rupiah_tagihan'],0));
					    	$response['val_biaya_admin']  = ltrim($send_iso['pdam_biaya_admin'],0);
					    	$response['pdam_blth'] = $send_iso['pdam_blth'];
							$response['jml_lembar_tagihan'] = $send_iso['jml_lembar_tagihan'];
							$response['pdam_waktu_lunas'] = $send_iso['pdam_waktu_lunas'];
							$response['pdam_idpel'] = $send_iso['pdam_idpel'];

							$response['val_total_bayar'] = $response['val_tagihan']+$response['val_biaya_admin'];
							$val_total_pembayaran = $response['val_total_bayar']-$sv['komisi']-$sv['voucher_amount'];
							$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

							$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
							$response['total_pembayaran'] = 'Rp '.number_format($response['val_total_pembayaran'],0,',','.');
							$response['tgl_lunas'] = $send_iso['pdam_waktu_lunas'];


							$struk_tgl_lunas = $response['tgl_lunas'];
							$struk_norek = $response['pdam_idpel'];
							$struk_blth = $response['pdam_blth'];
							$struk_nama_pelanggan = $response['nama_pelanggan'];
							$struk_jpa_ref = $response['pdam_no_ref_biller'];
							$struk_rp_tag = str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0));
							$struk_biaya_admin = $response['val_biaya_admin'];
							$struk_total_bayar = $response['val_total_bayar'];
							$struk_total_pembayaran = $response['val_total_pembayaran'];
							$jpa_ref = $send_iso['pdam_no_ref_biller'];

							$update = DB::statement('exec updateTransPDAMPascabayarStruk ?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_norek,$struk_blth,$struk_nama_pelanggan,$struk_jpa_ref,$struk_rp_tag,$struk_biaya_admin,$struk_total_bayar,$struk_total_pembayaran,$jpa_ref,$status));

							$response['api_status']  = 2;
					    	$response['api_message'] = 'Transaksi gagal, Mohon coba kembali';
					    	//$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
					    	$response['type_dialog']  = 'Error';
					    	$response['id_transaksi']  = 0;

					    	return $response;
						} else {
							/*selain 00 dan 94 masukin ongoing*/
							$up['status'] = 'Pending';
							//$up['status_match'] = 'Pending';
							$up['error_code'] = $send_iso['39'];
							$up['stan'] = $send_iso['11'];
							$up['jpa_ref'] = $send_iso['pdam_no_ref_biller'];

							$update = DB::table('trans_pdam')
								->where('id',$save)
								->update($up);

							$response['api_status']  = 2;
					    	//$response['api_message'] = 'Transaksi gagal, Mohon coba kembali';
					    	$response['api_message'] = 'Transaksi sedang di proses, klik menu ongoing transaksi untuk melihat detail transaksi pending';
					    	$response['type_dialog']  = 'Error';
					    	$response['id_transaksi']  = 0;

					    	$response['nama_pelanggan']  = $send_iso['pdam_customer_name'];
							$response['pdam_no_ref_biller']  = $send_iso['pdam_no_ref_biller'];
					    	//$response['pdam_blth']  = $send_iso['pdam_blth'];
					    	$response['tagihan']  = 'Rp '.number_format(str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0)),0,',','.');
					    	$response['biaya_admin']  = 'Rp '.number_format(ltrim($send_iso['pdam_biaya_admin'],0),0,',','.');

							$response['val_tagihan']  = str_replace('-', '', ltrim($send_iso['pdam_rupiah_tagihan'],0));
					    	$response['val_biaya_admin']  = ltrim($send_iso['pdam_biaya_admin'],0);
					    	$response['pdam_blth'] = $send_iso['pdam_blth'];
							$response['jml_lembar_tagihan'] = $send_iso['jml_lembar_tagihan'];
							$response['pdam_waktu_lunas'] = $send_iso['pdam_waktu_lunas'];
							$response['pdam_idpel'] = $send_iso['pdam_idpel'];

							$response['val_total_bayar'] = $response['val_tagihan']+$response['val_biaya_admin'];
							$val_total_pembayaran = $response['val_total_bayar']-$sv['komisi']-$sv['voucher_amount'];
							$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

							$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
							$response['total_pembayaran'] = 'Rp '.number_format($response['val_total_pembayaran'],0,',','.');
							$response['tgl_lunas'] = $send_iso['pdam_waktu_lunas'];


							$struk_tgl_lunas = $response['tgl_lunas'];
							$struk_norek = $response['pdam_idpel'];
							$struk_blth = $response['pdam_blth'];
							$struk_nama_pelanggan = $response['nama_pelanggan'];
							$struk_jpa_ref = $response['pdam_no_ref_biller'];
							$struk_rp_tag = str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0));
							$struk_biaya_admin = $response['val_biaya_admin'];
							$struk_total_bayar = $response['val_total_bayar'];
							$struk_total_pembayaran = $response['val_total_pembayaran'];
							$jpa_ref = $send_iso['pdam_no_ref_biller'];
							$status = 'Pending';

							$update = DB::statement('exec updateTransPDAMPascabayarStruk ?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_norek,$struk_blth,$struk_nama_pelanggan,$struk_jpa_ref,$struk_rp_tag,$struk_biaya_admin,$struk_total_bayar,$struk_total_pembayaran,$jpa_ref,$status));

					    	return $response;
					    	exit();
					    }
					} else {
					
						if($send_iso['39'] == 'Pending' || $send_iso['39'] == '06' || $send_iso['39'] == '09' || $send_iso['39'] == '18' || $send_iso['39'] == '22') {
							$up3['status'] = 'Error';
							$up3['stan'] = $send_iso['11'];
							$up3['jpa_ref'] = $send_iso['pdam_no_ref_biller'];

							$update = DB::table('trans_pdam')
								->where('id',$save)
								->update($up3);
								
							if(!empty($id_voucher)) {
								$uv['used'] = 'No';

								$up_voucher = DB::table('trans_voucher_child')
									->where('id',$id_voucher_child)
									->where('id_agen',$id_agen)
									->update($uv);
							}

							$response['api_status']  = 3;
					    	//$response['api_message'] = 'Transaksi sedang di proses';
					    	$response['api_message'] = 'Transaksi gagal, Mohon coba kembali';
					    	$response['type_dialog']  = 'Informasi';
					    	$response['id_transaksi']  = 0;

					    	$response['nama_pelanggan']  = $send_iso['pdam_customer_name'];
							$response['pdam_no_ref_biller']  = $send_iso['pdam_no_ref_biller'];
					    	//$response['pdam_blth']  = $send_iso['pdam_blth'];
					    	$response['tagihan']  = 'Rp '.number_format(str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0)),0,',','.');
					    	$response['biaya_admin']  = 'Rp '.number_format(ltrim($send_iso['pdam_biaya_admin'],0),0,',','.');

							$response['val_tagihan']  = str_replace('-', '', ltrim($send_iso['pdam_rupiah_tagihan'],0));
					    	$response['val_biaya_admin']  = ltrim($send_iso['pdam_biaya_admin'],0);
					    	$response['pdam_blth'] = $send_iso['pdam_blth'];
							$response['jml_lembar_tagihan'] = $send_iso['jml_lembar_tagihan'];
							$response['pdam_waktu_lunas'] = $send_iso['pdam_waktu_lunas'];
							$response['pdam_idpel'] = $send_iso['pdam_idpel'];

							$response['val_total_bayar'] = $response['val_tagihan']+$response['val_biaya_admin'];
							$val_total_pembayaran = $response['val_total_bayar']-$sv['komisi']-$sv['voucher_amount'];
							$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

							$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
							$response['total_pembayaran'] = 'Rp '.number_format($response['val_total_pembayaran'],0,',','.');
							$response['tgl_lunas'] = $send_iso['pdam_waktu_lunas'];


							$struk_tgl_lunas = $response['tgl_lunas'];
							$struk_norek = $response['pdam_idpel'];
							$struk_blth = $response['pdam_blth'];
							$struk_nama_pelanggan = $response['nama_pelanggan'];
							$struk_jpa_ref = $response['pdam_no_ref_biller'];
							$struk_rp_tag = str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0));
							$struk_biaya_admin = $response['val_biaya_admin'];
							$struk_total_bayar = $response['val_total_bayar'];
							$struk_total_pembayaran = $response['val_total_pembayaran'];
							$jpa_ref = $send_iso['pdam_no_ref_biller'];
							$status = 'Error';

							$update = DB::statement('exec updateTransPDAMPascabayarStruk ?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_norek,$struk_blth,$struk_nama_pelanggan,$struk_jpa_ref,$struk_rp_tag,$struk_biaya_admin,$struk_total_bayar,$struk_total_pembayaran,$jpa_ref,$status));

							return $response;
							exit();
						}
					}

					$send_sms_transaksi = CRUDBooster::getsetting('send_sms_transaksi_pdam');
					if($send_sms_transaksi == 'Yes') {
						$msg = CRUDBooster::getsetting('sukses_transaksi_pdam');
						$msg = str_replace('[no_hp]', $no_hp, $msg);
						$msg = str_replace('[nama]', $detail_agen->nama, $msg);
						$msg = str_replace('[komisi]', $sv['komisi'], $msg);
						Esta::send_sms($no_hp, $msg);
					}

					if(!empty($id_voucher)) {
						$uv['used'] = 'Yes';

						$up_voucher = DB::table('trans_voucher_child')
							->where('id',$id_voucher_child)
							->where('id_agen',$id_agen)
							->update($uv);
					}

					$response['api_status']  = 1;
			    	$response['api_message'] = 'Transaksi PDAM berhasil';
			    	$response['type_dialog']  = 'Informasi';
			    	$response['id_transaksi']  = $send_iso['id_log'];
			    	$response['nama_pelanggan']  = $send_iso['pdam_customer_name'];
					$response['pdam_no_ref_biller']  = $send_iso['pdam_no_ref_biller'];
			    	//$response['pdam_blth']  = $send_iso['pdam_blth'];
			    	$response['tagihan']  = 'Rp '.number_format(str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0)),0,',','.');
			    	$response['biaya_admin']  = 'Rp '.number_format(ltrim($send_iso['pdam_biaya_admin'],0),0,',','.');

					$response['val_tagihan']  = str_replace('-', '', ltrim($send_iso['pdam_rupiah_tagihan'],0));
			    	$response['val_biaya_admin']  = ltrim($send_iso['pdam_biaya_admin'],0);
			    	$response['pdam_blth'] = $send_iso['pdam_blth'];
					$response['jml_lembar_tagihan'] = $send_iso['jml_lembar_tagihan'];
					$response['pdam_waktu_lunas'] = $send_iso['pdam_waktu_lunas'];
					$response['pdam_idpel'] = $send_iso['pdam_idpel'];

					$response['val_total_bayar'] = $response['val_tagihan']+$response['val_biaya_admin'];
					$val_total_pembayaran = $response['val_total_bayar']-$sv['komisi']-$sv['voucher_amount'];
					$response['val_total_pembayaran'] = ($val_total_pembayaran <= 0 ? 0 : $val_total_pembayaran);

					$response['total_bayar'] = 'Rp '.number_format($response['val_total_bayar'],0,',','.');
					$response['total_pembayaran'] = 'Rp '.number_format($response['val_total_pembayaran'],0,',','.');
					$response['tgl_lunas'] = $send_iso['pdam_waktu_lunas'];

					$struk_tgl_lunas = $response['tgl_lunas'];
					$struk_norek = $response['pdam_idpel'];
					$struk_blth = $response['pdam_blth'];
					$struk_nama_pelanggan = $response['nama_pelanggan'];
					$struk_jpa_ref = $response['pdam_no_ref_biller'];
					$struk_rp_tag = str_replace('-', '',ltrim($send_iso['pdam_rupiah_tagihan'],0));
					$struk_biaya_admin = $response['val_biaya_admin'];
					$struk_total_bayar = $response['val_total_bayar'];
					$struk_total_pembayaran = $response['val_total_pembayaran'];
					$jpa_ref = $send_iso['pdam_no_ref_biller'];
					$status = 'Clear';

					$update = DB::statement('exec updateTransPDAMPascabayarStruk ?,?,?,?,?,?,?,?,?,?,?,?', array($save,$struk_tgl_lunas,$struk_norek,$struk_blth,$struk_nama_pelanggan,$struk_jpa_ref,$struk_rp_tag,$struk_biaya_admin,$struk_total_bayar,$struk_total_pembayaran,$jpa_ref,$status));

					/*email*/
					$view     = view('struk/struk_pdam',$response)->render();
					$filename = "Struk-PDAM-".$no_hp;
					$pdf      = App::make('dompdf.wrapper');

					$path = storage_path('app/uploads/'.$filename.'.pdf');

					$pdf->loadHTML($view);
					$pdf->setPaper('A4','landscape');
					$output = $pdf->output();

					file_put_contents($path, $output);

					$attachments = [$path];
					$email = $detail_agen->email;
					if(!empty($email) && $detail_agen->notif_email != 'No') {
					    
						try{
						  Esta::kirimemail(['to'=>$email,'data'=>$response,'template'=>'email_transaksi_pdam','attachments'=>$attachments]);
						}catch(\Exception $e){               	
						}
					}
				} else {
					/*reversal sobatku*/
					$start_time = time();
					//$refrn = 0;
					while(true){
						
						//$refrn++;
						$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
								//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/reversalPayment';
						$user = CRUDBooster::getsetting('user_sobatku_api');
						$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

						$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
						$keterangan = 'Reversal Payment';
						$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));
						$referenceNumber = $referenceNumber[0]->Prefix;
						$request = [
							'oldRefNum' => $oldRefNum,
							'referenceNumber' => $referenceNumber,
							'user' => $user,
							'hashCode' => hash('sha256', $oldRefNum.$referenceNumber.$user.$hashCodeKey)
						];

						$data_toJson = json_encode( $request );
						$ch = curl_init( $serviceURL );
						curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
						curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
						curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
								'Content-Type: application/json',
								'Accept:application/json'
							)
						);
						curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
						curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
						$responsereversal = curl_exec( $ch );
						$responseerror = curl_errno($ch);
						if ($responseerror == 28){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',1));
							break;
						}else {
							$responseBody = json_decode( $responsereversal, true);													

							if($responseBody['responseCode'] == '00' || $responseBody['responseCode'] == 'P2' || $responseBody['responseCode'] == 'P3'){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
								break;
							}elseif((time() - $start_time) > 60){
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
								break;
							}else{
								$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber,$data_toJson,$responsereversal,'Reversal Payment',0));
							}
						}
						
						curl_close($ch);
						sleep(10);
					}
					$response['api_status']  = 0;
			    	$response['api_message'] = 'Transaksi PDAM gagal';
			    	$response['type_dialog']  = 'Error';
			    	$response['id_transaksi']  = 0;
			    	$response['pdam_no_ref_biller']  = $send_iso['pdam_no_ref_biller'];
				}
			/*} else {
				$is_reversalsuccess = 1;

				while($is_reversalsuccess = 1){
					sleep(5);
					$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/reversalPayment';
					$user = CRUDBooster::getsetting('user_sobatku_api');
					$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');

					$prefixActivation = CRUDBooster::getsetting('prefix_reference_reversal');
					$keterangan = 'Reversal Payment';
					$referenceNumber = DB::select('exec CreateReferenceReversalSobatku ?,?', array($prefixActivation, $keterangan));

					$request = [
						'oldRefNum' => $oldRefNum,
						'referenceNumber' => $referenceNumber[0]->Prefix,
						'user' => $user,
						'hashCode' => hash('sha256', $oldRefNum.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
					];

					$data_toJson = json_encode( $request );
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response = curl_exec( $ch );
					$responseerror = curl_errno($ch);
					if ($responseerror == 28){
						$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$request,$response,'Reversal Payment',1));
						$is_reversalsuccess = 0;
					}else {
						$responseBody = json_decode( $response, true);													

						if($responseBody['responseCode'] == '00' ){
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$request,$response,'Reversal Payment',0));
							$is_reversalsuccess = 0;
						}else{
							$saveResponse = DB::statement('exec CreateLogSobatkuReversal ?,?,?,?,?,?', array($oldRefNum,$referenceNumber[0]->Prefix,$request,$response,'Reversal Payment',0));
						}
					}
					
					curl_close($ch);	
				}
				$response['api_status']  = 3;
		    	$response['api_message'] = 'Saldo anda tidak mencukupi';
		    	$response['type_dialog']  = 'Error';
		    	$response['id_transaksi']  = 0;
			}*/

		return $response;
	}
	
}