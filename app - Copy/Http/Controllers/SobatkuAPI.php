<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Storage;
use Esta;
use Illuminate\Support\Facades\Auth;
use App\Helpers\TransaksiSubmit;
use App\Http\Controllers\BpjsAPI;
use App\Http\Controllers\PulsaAPI;
use App\Http\Controllers\PlnAPI;
use App\Http\Controllers\PdamAPI;
use App\Http\Controllers\BuyItemAPI;
use Carbon\Carbon;

class SobatkuAPI extends ApiController
{

	public function activationResult(){
		$request = '';
		$phoneNumber = Request::get('phoneNumber');
		$sessionId = Request::get('sessionId');
		$responseCode = Request::get('responseCode');
		$responseDescription = Request::get('responseDescription');
		$hashCode = Request::get('hashCode');
		$response = [
			'phoneNumber' => $phoneNumber,
			'sessionId' => $sessionId,
			'responseCode' => $responseCode,
			'responseDescription' => $responseDescription,
			'hashCode' => $hashCode
		];

		$responseJson = json_encode($response);

		$saveResponse = DB::statement('exec CreateLogSobatkuActivation ?,?,?,?,?', array($phoneNumber,'',$request,$responseJson,'Activation Result'));
		if($response['responseCode'] == '00' || $response['responseCode'] == 'A1'){
			$getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];
			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Activation Result';
			$save_notif['description'] = 'Sobatku Active';
			$save_notif['description_short'] = 'Sobatku Active';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku Active';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
			}
			
			DB::table('log_notification_sobatku')->insert($save_notif);
			//$save_ver['tglverifikasisobatku'] = date('Y-m-d H:i:s');
			$detail = DB::select('exec getAgenById ?', array($id_agen))[0];
			$save_ver['tglaktivasisobatku'] = date('Y-m-d H:i:s');
			$save_ver['regid'] = NULL;
			$save_ver['flag_registrasi'] = 5;
			$save_ver['response_sobatku'] = $response['responseCode'];
			$save_ver['alasan_reject_sobatku'] = NULL;
			$save_ver['is_existing'] = 0;
			$save_ver['tglaktivasimembershipsobatku'] = date('Y-m-d H:i:s');
			DB::table('agen')
				->where('id',$getAgen->id)
				->update($save_ver);
		}else if($response['responseCode'] != '00' || $response['responseCode'] != 'A1'){
			$getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];
			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Activation Result';
			$save_notif['description'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);
			$save_notif['description_short'] = 'Gagal Aktivasi';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku Gagal';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
			}
			DB::table('log_notification_sobatku')->insert($save_notif);
			//$save_ver['deleted_at'] = NULL;
			$save_ver['response_sobatku'] = $response['responseCode'];
			$save_ver['alasan_reject_sobatku'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);
			$save_ver['flag_registrasi'] = 1;
			DB::table('agen')
				->where('id',$getAgen->id)
				->update($save_ver);
		}

		return $this->respondWithDataAndMessage($response, "Success");
	}

	public function registrasionResult(){
		$request = '';
		$phoneNumber = Request::get('phoneNumber');
		$sessionId = Request::get('sessionId');
		$responseCode = Request::get('responseCode');
		$responseDescription = Request::get('responseDescription');
		$hashCode = Request::get('hashCode');
		$response = [
			'phoneNumber' => $phoneNumber,
			'sessionId' => $sessionId,
			'responseCode' => $responseCode,
			'responseDescription' => $responseDescription,
			'hashCode' => $hashCode
		];

		$responseJson = json_encode($response);

		$saveResponse = DB::select('exec CreateLogSobatkuRegistration ?,?,?,?,?', array($phoneNumber,'',$request,$responseJson,'Registration Result'));
		if($response['responseCode'] == 'W1'){
			$getAgen = DB::select('EXEC getPengajuanAgenByNohp ?',array($phoneNumber))[0];
			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Registrasion Result';
			$save_notif['description'] = 'Waiting For KYC';
			$save_notif['description_short'] = 'Waiting For KYC';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('txn_pengajuan_agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->agen_regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->agen_regid],$datafcm);
			}
			
			DB::table('log_notification_sobatku')->insert($save_notif);
			$save_ver['agen_response_sobatku'] = 'W1';
			$save_ver['flag_registrasi'] = 5;
			DB::table('txn_pengajuan_agen')
				->where('id',$getAgen->id)
				->update($save_ver);
				
			$detail_agen_existing = DB::table('agen')->where('no_hp',$phoneNumber)->first();
			if($detail_agen_existing){
				if($detail_agen_existing->regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$datafcm['type'] = 'Sobatku';
					$save_notif['regid'] = Esta::sendFCM([$detail_agen_existing->regid],$datafcm);
				}
				
				DB::table('log_notification_sobatku')->insert($save_notif);
				$save_ver_existing['response_sobatku'] = 'W1';
				$save_ver_existing['flag_registrasi'] = 5;
				DB::table('agen')
					->where('no_hp',$phoneNumber)
					->update($save_ver_existing);
			}
		}else{
			$getAgen = DB::select('EXEC getPengajuanAgenByNohp ?',array($phoneNumber))[0];
			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Registrasion Result';
			$save_notif['description'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);
			$save_notif['description_short'] = 'Gagal Registrasi';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('txn_pengajuan_agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->agen_regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku Gagal';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->agen_regid],$datafcm);
			}
			//$save_ver['deleted_at'] = date('Y-m-d H:i:s');
			$save_ver['agen_response_sobatku'] = $response['responseCode'];
			//$save_ver['agen_alasan_reject'] = $response['responseDescription'];
			DB::table('txn_pengajuan_agen')
				->where('id',$getAgen->id)
				->update($save_ver);
			DB::table('log_notification_sobatku')->insert($save_notif);
		}

		return $this->respondWithDataAndMessage($response, "Success");
	}

	public function verificationManualResult(){
		
		$request = '';
		$phoneNumber = Request::get('phoneNumber');
		$sessionId = Request::get('sessionId');
		$responseCode = Request::get('responseCode');
		$responseDescription = Request::get('responseDescription');
		$hashCode = Request::get('hashCode');
		$response = [
			'phoneNumber' => $phoneNumber,
			'sessionId' => $sessionId,
			'responseCode' => $responseCode,
			'responseDescription' => $responseDescription,
			'hashCode' => $hashCode
		];

		$responseJson = json_encode($response);

		$saveResponse = DB::statement('exec CreateLogSobatkuVerification ?,?,?,?,?', array($phoneNumber,'',$request,$responseJson,'Verification Manual Result'));
		if($response['responseCode'] == 'R1'){
			$getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];


			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Verification';
			$save_notif['description'] = 'Verification Result';
			$save_notif['description_short'] = 'Akun sukses di verifikasi';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
			}
			$save_ver['tglverifikasisobatku'] = date('Y-m-d H:i:s');
			$save_ver['is_existing'] = 0;
			$save_ver['regid'] = NULL;
			DB::table('agen')
				->where('id',$getAgen->id)
				->update($save_ver);
			
			DB::table('log_notification_sobatku')->insert($save_notif);
		}else{
			$getAgen = DB::select('EXEC getAgenByNohp ?',array($phoneNumber))[0];
			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Verification';
			$save_notif['description'] = $response['responseDescription'];
			$save_notif['description_short'] = 'Gagal Verification';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($detail_agen->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = 'Gagal Verification : '.$save_notif['description'];
				$datafcm['type'] = 'Sobatku';
				$save_notif['regid'] = Esta::sendFCM([$detail_agen->regid],$datafcm);
			}
			$save_ver['tglpengajuansobatku'] = NULL;
			DB::table('agen')
				->where('id',$getAgen->id)
				->update($save_ver);
			DB::table('log_notification_sobatku')->insert($save_notif);
		}

		return $this->respondWithDataAndMessage($response, "Success");
	}

	public function inquiryBalance(){
		$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/accountBalance';
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		$accountNumber = Request::get('accountNumber');

		$prefixActivation = CRUDBooster::getsetting('prefix_reference_inquiryBalance');
		//$keterangan = 'inquiryBalance';
		$referenceNumber = $this->generateReferenceNumber();

		$request = [
			'accountNumber' => $accountNumber,
			'referenceNumber' => $referenceNumber,
			'user' => $user,
			'hashCode' => hash('sha256', $accountNumber.$referenceNumber.$user.$hashCodeKey)
		];

		$data_toJson = json_encode( $request );
		$ch = curl_init( $serviceURL );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept:application/json'
			)
		);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		$response = curl_exec( $ch );
		$responseBody = json_decode( $response, true);
		$responseBody['balance'] = str_replace(',', '', $responseBody['balance']);
		$responseBody['balance'] = str_replace('.', '', $responseBody['balance']);
		$responseBody['balance']=(double)$responseBody['balance'];
		
		curl_close($ch);

		return $responseBody;
	}
	
	private function generateReferenceNumber() {
		// Ambil 5 digit terakhir time()
		//$time = substr("" . time(), -5);
		$time = Carbon::now()->timestamp;
		// Generate 6 karakter random
		$keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$pieces = [];
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < 3; $i++) {
			$pieces[] = $keyspace[mt_rand(0, $max)];
		}
		return $time . implode('', $pieces);
	}

	public function requestPayment(){
		$agen = Auth::user();
        $id_agen = $agen->id;
		
		$respondAgenAktif = $this->validasiAgenAktif();
        if( $respondAgenAktif != NULL){
            return $respondAgenAktif;
            exit();
        }
		
		$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/requestPayment';
		//$serviceURL = CRUDBooster::getsetting('sobatku_url').'/api/requestPayment';
		$serviceURLBalance = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/accountBalance';
		//$serviceURLBalance = CRUDBooster::getsetting('sobatku_url').'api/inquiryBalance';
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		$accountNumber = Request::get('accountNumber');
		$amount = Request::get('amount');
		$type = Request::get('type');
		$regid = Request::get('regid');
		$requestTransaksi = Request::get('request');
		//$referenceNumber = Request::get('referenceNumber');
		$requestTransaksiCheck = json_decode( $requestTransaksi,true );
		$voucher_id_check = $requestTransaksiCheck['voucher_id'] == '' ? $requestTransaksiCheck['id_voucher'] : $requestTransaksiCheck['voucher_id'];
		$check_voucher = DB::table('trans_voucher_child')
                ->where('id',$voucher_id_check)
                ->where('id_agen',$id_agen)
                ->first();

        if($check_voucher->used == 'Pen'){
        	return $this->respondWithError("Gagal Transaksi, Voucher yang anda gunakan sedang di proses. Mohon Hubungi CS : ".CRUDBooster::getsetting('bantuan_cs'));
			exit();
        }elseif($check_voucher->used == 'Yes'){
			return $this->respondWithError("Gagal Transaksi, Voucher yang anda gunakan sudah pernah digunakan.");
			exit();
		}
		
		//UPDATE CEK AGEN MEMBERSHIP
		//15-10-2020 BY INDOCYBER
		$getAgen = DB::table('agen')
			->where('id',$id_agen)
			->first();	

		if(empty($getAgen->tglaktivasimembershipsobatku)){
			return $this->respondWithError("Gagal Transaksi, Agen belum teraktivasi sebagai membership.");
			exit();
		}

		//Get Saldo--------------------------------------------------------------------------------------------------
		$referenceNumberBalance = $this->generateReferenceNumber();
		$requestBalance = [
			'accountNumber' => $accountNumber,
			'referenceNumber' => $referenceNumberBalance,
			'user' => $user,
			'hashCode' => hash('sha256', $accountNumber.$referenceNumberBalance.$user.$hashCodeKey)
		];

		$data_toJson = json_encode( $requestBalance );
		$ch = curl_init( $serviceURLBalance );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept:application/json'
			)
		);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		$responseSaldo = curl_exec( $ch );
		$responseBodySaldo = json_decode( $responseSaldo, true);
		$responseBodySaldo['balance'] = str_replace(',', '', $responseBodySaldo['balance']);
		$responseBodySaldo['balance'] = str_replace('.', '', $responseBodySaldo['balance']);
		$responseBodySaldo['balance']=(double)$responseBodySaldo['balance'];
		curl_close($ch);
		//-----------------------------------------------------------------------------------------------------------
		$saldo_sebelum = $responseBodySaldo['balance'];

		$description = Request::get('description');
		$prefixActivation = CRUDBooster::getsetting('prefix_reference_requestpayment');
			$keterangan = 'Request Payment';
			$referenceNumber = DB::select('exec CreateReferencePaymentSobatku ?,?', array($prefixActivation, $keterangan));
			// dd($referenceNumber[0]->Prefix);

		$request = [
			'accountNumber' => $accountNumber,
			'amount' => $amount,
			'referenceNumber' => $referenceNumber[0]->Prefix,
			'description' => $description,
			'user' => $user,
			'hashCode' => hash('sha256', $accountNumber.$amount.$referenceNumber[0]->Prefix.$user.$hashCodeKey)
		];

		$data_toJson = json_encode( $request );

		$saveResponse = DB::select('exec CreateLogSobatkuPayment ?,?,?,?,?,?,?,?', array($accountNumber,$transactionId,$amount,$saldo_sebelum,$referenceNumber[0]->Prefix,$data_toJson,'','Request Payment'));
		$save_id = $saveResponse[0]->id;

		$ch = curl_init( $serviceURL );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept:application/json'
			)
		);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		$response_curl_service = curl_exec( $ch );
		$responseBody = json_decode( $response_curl_service,true );
		$uv['response'] = $response_curl_service;
		$uv['transactionId'] = $responseBody['transactionId'];
		$update_response = DB::table('log_sobatku_payment')
							->where('id',$save_id)
							->update($uv);
		$transactionId = '';
		//$responseBody['responseCode'] = '00';
		if($responseBody['responseCode'] == '00'){
			$responseBody['urlPayment'] = CRUDBooster::getsetting('webview_url_sobatku_api').'/walletPage/payment?phoneNumber='.$accountNumber.'&transactionId='.$responseBody['transactionId'].'&sessionId='.$responseBody['sessionId'].'&user='.$user;
			$transactionId = $responseBody['transactionId'];
			$createTempTrans = DB::select('exec CreateTempTransPayment ?,?,?,?,?,?,?', 
									array($id_agen,$type,$transactionId,$requestTransaksi,$response,$referenceNumber[0]->Prefix,$regid));
			$requestTransaksi = json_decode( $requestTransaksi,true );
			if(!empty($voucher_id_check)) {
				$uvoucher['used'] = 'Pen';

				$up_voucher = DB::table('trans_voucher_child')
					->where('id',$voucher_id_check)
					->where('id_agen',$id_agen)
					->update($uvoucher);
			}
			if($type == 'bpjs_kesehatan'){
				$oldRefNum = $requestTransaksi['oldRefNum'];
				$saldo = $requestTransaksi['saldo'];
				$regid = $requestTransaksi['regid'];
				$cek_regid = Esta::cek_regid($id_agen,$regid);

				$getlog_transaction = DB::table('log_sobatku_payment')
					->where('transactionId',$transactionId)
					->where('keterangan','Request Payment')
					->first();
				$oldRefNum = $getlog_transaction->ref_number;
				$saldo_sebelum = $getlog_transaction->saldo_sebelum;
				
				$type = $requestTransaksi['type'];
				$no_hp = $requestTransaksi['no_hp'];
				$id_pelanggan = $requestTransaksi['id_pelanggan'];
				$pin = $requestTransaksi['pin'];
				$amount = $requestTransaksi['amount'];
				$id_transaksi = $requestTransaksi['id_transaksi'];

				$id_voucher_child = $requestTransaksi['id_voucher'];
				$id_voucher = DB::table('trans_voucher_child')
					->where('id',$id_voucher_child)
					->first()->id_voucher;

				$tipe_layanan = DB::table('pan_tipe_layanan')->where('kode','13')->first()->id;
				$kode_layanan = DB::table('pan_kode_layanan')->where('kode','4')->first()->id;
				$id_product = DB::table('pan_product')->where('id_tipe_layanan',$tipe_layanan)->where('id_kode_layanan',$kode_layanan)->first()->id;

				$detail_agen = DB::table('agen')
					->where('id',$id_agen)
					->first();

				$detail_voucher = DB::table('voucher')
					->where('id',$id_voucher)
					->first();

				$detail_product = DB::table('pan_product')
					->where('id',$id_product)
					->first();

				//UPDATE 23102019
				$detail_inquiry_jat = DB::table('log_jatelindo_bit')
					->where('id',$id_transaksi)
					->first();
				$check_table_trans = DB::table('trans_bpjs')
					->where('trans_no',$detail_inquiry_jat->bit37)
					->first();
				if(!empty($check_table_trans->id)){
					$kode = $check_table_trans->trans_no;
				}else{
					//$kode = Esta::nomor_transaksi('trans_bpjs',CRUDBooster::getsetting('transaksi_bpjs'));
					$kode = $detail_inquiry_jat->bit37;
				}
				//


				$detail_inquiry = DB::select('exec getLogJatelindoBitByParams ?,?,?,?', array($id_transaksi,'BPJS Kesehatan','Inquiry','res'))[0];

				$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);
				$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

				$biaya_admin = $detail_inquiry->bpjsks_biaya_admin;
				if($trans_amount == 0) {
					$trans_amount = 0;
				} else {
					$trans_amount = $trans_amount+$biaya_admin;
				}
				$trans_amount = $trans_amount-$komisi;

				$status_match_sobatku = 'Waiting Rekon';
				try{
					$created_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'BPJS Kesehatan';
					$currency = 'IDR';
					$trans_amount = $trans_amount;
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Error';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_pelanggan = $id_pelanggan;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$id_voucher = $detail_voucher->id;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_bpjs');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
						$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
						$response['type_dialog']  = 'Error';
						$response['id_transaksi']  = 0;
						return $response;
						exit();
					}

					$saveTransBPJS = DB::select('exec postTransBPJS ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
						array($created_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$trans_amount,$komisi,$id_agen,$status,$rekon_amount,$status_match,$no_hp,$id_pelanggan,$id_product,$agen_nama,$agen_email,$agen_kode,$agen_no_hp,$agen_level,$agen_referall,$agen_referall_relation ,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan ,$agen_prov,$agen_kab,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller,$product_komisi_basic,$product_komisi_premium,$product_amount ,$product_margin,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan,$voucher_nama,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description,$id_voucher,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
					$save = $saveTransBPJS[0]->id;
				} catch(\Exception $e) {
					//sleep(1);
					//$kode = Esta::nomor_transaksi('trans_bpjs',CRUDBooster::getsetting('transaksi_bpjs'));
					$kode = $detail_inquiry_jat->bit37;
					$created_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'BPJS Kesehatan';
					$currency = 'IDR';
					$trans_amount = $trans_amount;
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Error';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_pelanggan = $id_pelanggan;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$id_voucher = $detail_voucher->id;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_bpjs');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
						$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
						$response['type_dialog']  = 'Error';
						$response['id_transaksi']  = 0;
						return $response;
						exit();
					}

					$saveTransBPJS = DB::select('exec postTransBPJS ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
						array($created_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$trans_amount,$komisi,$id_agen,$status,$rekon_amount,$status_match,$no_hp,$id_pelanggan,$id_product,$agen_nama,$agen_email,$agen_kode,$agen_no_hp,$agen_level,$agen_referall,$agen_referall_relation ,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan ,$agen_prov,$agen_kab,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller,$product_komisi_basic,$product_komisi_premium,$product_amount ,$product_margin,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan,$voucher_nama,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description,$id_voucher,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
					$save = $saveTransBPJS[0]->id;
				}
			}elseif($type == 'pulsa_prabayar'){
				$regid = $requestTransaksi['regid'];
				$token = $requestTransaksi['token'];
				//$id_agen = Request::get('id_agen');
				$kode_paket = $requestTransaksi['kode_paket'];
				$cek_regid = Esta::cek_regid($id_agen,$regid);

		        $getlog_transaction = DB::table('log_sobatku_payment')
		            ->where('transactionId',$transactionId)
		            ->where('keterangan','Request Payment')
		            ->first();
		        $oldRefNum = $getlog_transaction->ref_number;
		        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

				$no_hp = $requestTransaksi['no_hp'];
				$id_product = $requestTransaksi['id_product'];
				$pin =$requestTransaksi['pin'];
				$id_voucher_child = $requestTransaksi['id_voucher'];
				$id_voucher = DB::table('trans_voucher_child')
					->where('id',$id_voucher_child)
					->first()->id_voucher;

				$detail_agen = DB::table('agen')
					->where('id',$id_agen)
					->first();

				if($pin != 'estakiosprosespaket'){

				}

				$detail_product = DB::table('pan_product')
					->where('id',$id_product)
					->first();

				$detail_voucher = DB::table('voucher')
					->where('id',$id_voucher)
					->first();

				$kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));

				$trans_amount = Esta::amount_product_trans($id_product,$id_agen,$id_voucher)-($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
				if($trans_amount <= 0) {
					$trans_amount = 0;
				} else {
					$trans_amount = $trans_amount;
				}
				
				$status_match_sobatku = 'Waiting Rekon';
				try{
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'PULSA PRABAYAR';
					$currency = 'IDR';
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					$trans_amount = $trans_amount;
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Error';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $detail_product->amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$id_voucher = $detail_voucher->id;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$kode_paket = $kode_paket;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pulsa');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					$saveTransPulsa = DB::select('exec postTransPulsaPrabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
											,array($created_at,$updated_at,$created_user,$updated_user
											,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$komisi,$trans_amount,$id_agen,$status,$rekon_amount
											,$status_match,$no_hp,$id_product,$agen_nama,$agen_no_hp,$agen_email,$agen_kode,$agen_level,$agen_referall
											,$agen_referall_relation,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir
											,$agen_jenis_kelamin,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov
											,$agen_kab,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan
											,$product_kode_biller,$product_komisi_basic,$product_komisi_premium,$product_amount,$product_margin
											,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3
											,$product_potongan,$voucher_nama,$voucher_expired_date,$voucher_amount,$id_voucher,$voucher_tagline
											,$voucher_description,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku,$kode_paket));
					$save = $saveTransPulsa[0]->id;
				} catch(\Exception $e) {
					$kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));
			
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'PULSA PRABAYAR';
					$currency = 'IDR';
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					$trans_amount = $trans_amount;
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Error';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $detail_product->amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$id_voucher = $detail_voucher->id;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$kode_paket = $kode_paket;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pulsa');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					$saveTransPulsa = DB::select('exec postTransPulsaPrabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
											,array($created_at,$updated_at,$created_user,$updated_user
											,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$komisi,$trans_amount,$id_agen,$status,$rekon_amount
											,$status_match,$no_hp,$id_product,$agen_nama,$agen_no_hp,$agen_email,$agen_kode,$agen_level,$agen_referall
											,$agen_referall_relation,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir
											,$agen_jenis_kelamin,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov
											,$agen_kab,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan
											,$product_kode_biller,$product_komisi_basic,$product_komisi_premium,$product_amount,$product_margin
											,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3
											,$product_potongan,$voucher_nama,$voucher_expired_date,$voucher_amount,$id_voucher,$voucher_tagline
											,$voucher_description,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku,$kode_paket));
					$save = $saveTransPulsa[0]->id;
				}
			}elseif($type == 'pulsa_pascabayar'){
				$regid = $requestTransaksi['regid'];
				$token = $requestTransaksi['token'];
				$cek_regid = Esta::cek_regid($id_agen,$regid);

		        $getlog_transaction = DB::table('log_sobatku_payment')
		            ->where('transactionId',$transactionId)
		            ->where('keterangan','Request Payment')
		            ->first();
		        $oldRefNum = $getlog_transaction->ref_number;
		        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

				/*if($cek_regid == 0) {
					$response['api_status']  = 2;
				    $response['api_message'] = 'Terjadi gangguan pada koneksi internet atau Server. Silahkan coba beberapa saat lagi';//'Akun Anda terdeteksi login di HP lain, Silahkan logout dan login kembali untuk melanjutkan transaksi.';
				    $response['type_dialog']  = 'Error';
				    //Esta::add_fraud($id_agen);
				    return response()->json($response);
				}*/

				$no_hp = $requestTransaksi['no_hp'];
				$id_product = $requestTransaksi['id_product'];
				//$id_voucher = Request::get('id_voucher');
				$pin = $requestTransaksi['pin'];
				$amount = $requestTransaksi['amount'];
				$id_transaksi = $requestTransaksi['id_transaksi'];

				$id_voucher_child = $requestTransaksi['id_voucher'];
				$id_voucher = DB::table('trans_voucher_child')
					->where('id',$id_voucher_child)
					->first()->id_voucher;

				$detail_agen = DB::table('agen')
					->where('id',$id_agen)
					->first();

				/*if(hash::check($pin, $detail_agen->password)){
					
				} else {
					$response['api_status']  = 2;
			    	$response['api_message'] = 'PIN anda salah';
			    	$response['type_dialog']  = 'Error';
			    	return response()->json($response);
			    	exit();
				}*/

				$detail_product = DB::table('pan_product')
					->where('id',$id_product)
					->first();

				$detail_voucher = DB::table('voucher')
					->where('id',$id_voucher)
					->first();
					
				//UPDATE 23102019
				$detail_inquiry_jat = DB::table('log_jatelindo_bit')
					->where('id',$id_transaksi)
					->first();
				$check_table_trans = DB::table('trans_pulsa')
					->where('trans_no',$detail_inquiry_jat->bit37)
					->first();
				if(!empty($check_table_trans->id)){
					$kode = $check_table_trans->trans_no;
				}else{
					//$kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));
					$kode = $detail_inquiry_jat->bit37;
				}
				//

				$detail_inquiry = DB::table('log_jatelindo_bit')
					->where('id',$id_transaksi)
					->where('type','Pulsa Postpaid')
					->where('status','Inquiry')
					->where('jenis','res')
					->first();

				$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);

				$biaya_admin = $detail_inquiry->pls_psc_biaya_admin;
				if($trans_amount == 0) {
					$trans_amount = 0;
				} else {
					$trans_amount = $trans_amount+$biaya_admin;
				}

				/*echo $trans_amount;
				exit();*/
				$status_match_sobatku = 'Waiting Rekon';
				//if($saldo >= $trans_amount) {
				try{
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'PULSA PASCABAYAR';
					$currency = 'IDR';
					$trans_amount = $amount;
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Error';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$id_voucher = $detail_voucher->id;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pulsa');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					$saveTransPulsa = DB::select('exec postTransPulsaPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								,array($created_at,$updated_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc
									,$currency,$trans_amount,$komisi,$id_agen,$status,$rekon_amount,$status_match,$no_hp,$id_product
									,$agen_nama,$agen_no_hp,$agen_email,$agen_kode,$agen_level,$agen_referall,$agen_referall_relation
									,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin
									,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov,$agen_kab
									,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller
									,$product_komisi_basic,$product_komisi_premium,$product_amount,$product_margin,$product_biaya_admin_edn
									,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan
									,$voucher_nama,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description,$id_voucher
									,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
					$save = $saveTransPulsa[0]->id;
				} catch(\Exception $e) {
					//sleep(1);
					//$kode = Esta::nomor_transaksi('trans_pulsa',CRUDBooster::getsetting('transaksi_pulsa'));
					$kode = $detail_inquiry_jat->bit37;
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'PULSA PASCABAYAR';
					$currency = 'IDR';
					$trans_amount = $amount;
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Error';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$id_voucher = $detail_voucher->id;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pulsa');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					$saveTransPulsa = DB::select('exec postTransPulsaPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								,array($created_at,$updated_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc
									,$currency,$trans_amount,$komisi,$id_agen,$status,$rekon_amount,$status_match,$no_hp,$id_product
									,$agen_nama,$agen_no_hp,$agen_email,$agen_kode,$agen_level,$agen_referall,$agen_referall_relation
									,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin
									,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov,$agen_kab
									,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller
									,$product_komisi_basic,$product_komisi_premium,$product_amount,$product_margin,$product_biaya_admin_edn
									,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan
									,$voucher_nama,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description,$id_voucher
									,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku));
					$save = $saveTransPulsa[0]->id;
				}
			}elseif($type == 'pln_prabayar'){
				$regid = $requestTransaksi['regid'];
				$token = $requestTransaksi['token'];
				$kode_paket = $requestTransaksi['kode_paket'];
				$cek_regid = Esta::cek_regid($id_agen,$regid);

		        $getlog_transaction = DB::table('log_sobatku_payment')
		            ->where('transactionId',$transactionId)
		            ->where('keterangan','Request Payment')
		            ->first();
		        $oldRefNum = $getlog_transaction->ref_number;
		        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

		        

				$no_hp = $requestTransaksi['no_hp'];
				$id_product = $requestTransaksi['id_product'];
				$id_transaksi = $requestTransaksi['id_transaksi'];
				$no_meter = $requestTransaksi['no_meter'];
				$pin = $requestTransaksi['pin'];
				//$id_voucher = Request::get('id_voucher');
				$amount = $requestTransaksi['amount'];
				$unsold = $requestTransaksi['unsold'];
				$unsold_nominal = $requestTransaksi['unsold_nominal'];

				$id_voucher_child = $requestTransaksi['id_voucher'];
				$id_voucher = DB::table('trans_voucher_child')
					->where('id',$id_voucher_child)
					->first()->id_voucher;

				$detail_agen = DB::table('agen')
					->where('id',$id_agen)
					->first();

				$detail_product = DB::table('pan_product')
					->where('id',$id_product)
					->first();

				$product_amount = ($unsold == 1 ? $unsold_nominal : $detail_product->amount);

				$detail_voucher = DB::table('voucher')
					->where('id',$id_voucher)
					->first();

				//UPDATE 23102019
				$detail_inquiry = DB::table('log_jatelindo_bit')
					->where('id',$id_transaksi)
					->first();
				$check_table_trans = DB::table('trans_pln')
					->where('trans_no',$detail_inquiry->bit37)
					->first();
				if(!empty($check_table_trans->id)){
					$kode = $check_table_trans->trans_no;
				}else{
					$kode = $detail_inquiry->bit37;
				}
				//
				
				if($unsold == 1) {
					$detail_product_unsold_1 = DB::table('pan_product')->where('product_name','Token Unsold 1')->first();
					$voucher = DB::table('voucher')->where('id',$id_voucher)->first();
					$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product_unsold_1->komisi_basic : $detail_product_unsold_1->komisi_premium);

					$trans_amount = $product_amount+$detail_product_unsold_1->admin_edn+$detail_product_unsold_1->admin_1+$detail_product_unsold_1->admin_2+$detail_product_unsold_1->admin_3+$detail_product_unsold_1->margin-$komisi-$voucher->amount;
				} else {
					$trans_amount = Esta::amount_product_trans_pln_prabayar($id_product,$id_agen,$id_voucher,0);
				}

				/*echo $trans_amount.'-'.($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);

				Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN Prabayar','Transaksi PLN Prabayar '.$detail_product->product_name,'Out','Transaksi');
				Esta::log_money($id_agen,($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium),date('Y-m-d H:i:s'),'Komisi Transaksi PLN Prabayar','Komisi Transaksi PLN Prabayar '.$detail_product->product_name,'In','Komisi');
				exit();*/
				$status_match_sobatku = 'Waiting Rekon';
				try{
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'PLN PRABAYAR';
					$currency = 'IDR';
					$trans_amount = $product_amount;
					if($unsold == 1) {
						$komisi = $komisi;
					} else {
						$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					}
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Error';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$no_meter = $no_meter;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $product_amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$id_voucher = $detail_voucher->id;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$status_jatelindo = 'Pending';
					$kode_paket = $kode_paket;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pln');
					
					//dd($last_transaksi);
					if($last_transaksi == 1) {
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					$saveTransPLN = DB::select('exec postTransPlnPrabayar 
						?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
						,array($created_at,$updated_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$trans_amount,$komisi,$id_agen
								,$status,$rekon_amount,$status_match,$no_hp,$no_meter,$id_product,$agen_nama,$agen_email,$agen_kode,$agen_no_hp,$agen_level
								,$agen_referall,$agen_referall_relation,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin
								,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov,$agen_kab,$agen_kec,$agen_kel
								,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller,$product_komisi_basic,$product_komisi_premium
								,$product_amount,$product_margin,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan
								,$voucher_nama,$id_voucher,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description
								,$status_jatelindo,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku,$kode_paket));
					$save = $saveTransPLN[0]->id;
				} catch(\Exception $e) {
					
					//$kode = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));
					$kode = $detail_inquiry->bit37;
					$created_at = date('Y-m-d H:i:s');
					$updated_at = date('Y-m-d H:i:s');
					$created_user = Esta::user($id_agen);
					$updated_user = Esta::user($id_agen);
					$trans_no = $kode;
					$ref_trans_no = $kode;
					$trans_date = date('Y-m-d H:i:s');
					$trans_desc = 'PLN PRABAYAR';
					$currency = 'IDR';
					$trans_amount = $product_amount;
					if($unsold == 1) {
						$komisi = $komisi;
					} else {
						$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					}
					//$create_user = $detail_agen->nama;
					$id_agen = $id_agen;
					$status = 'Error';
					$rekon_amount = 0;
					$status_match = 'Waiting Rekon';
					$no_hp = $no_hp;
					$no_meter = $no_meter;
					$id_product = $id_product;
					$agen_nama = $detail_agen->nama;
					$agen_email = $detail_agen->email;
					$agen_kode = $detail_agen->kode;
					$agen_no_hp = $detail_agen->no_hp;
					$agen_level = $detail_agen->status_agen;
					$agen_referall = $detail_agen->kode_referall_agen;
					$agen_referall_relation = $detail_agen->kode_relation_referall;
					$agen_status_aktif = $detail_agen->status_aktif;
					$agen_nik = $detail_agen->nik;
					$agen_tgl_register = $detail_agen->tgl_register;
					$agen_tempat_lahir = $detail_agen->tempat_lahir;
					$agen_tgl_lahir = $detail_agen->tgl_lahir;
					$agen_jenis_kelamin = $detail_agen->jenis_kelamin;
					$agen_agama = $detail_agen->agama;
					$agen_status_perkawinan = $detail_agen->status_perkawinan;
					$agen_pekerjaan = $detail_agen->pekerjaan;
					$agen_kewarganegaraan = $detail_agen->kewarganegaraan;
					$agen_prov = $detail_agen->prov;
					$agen_kab = $detail_agen->kab;
					$agen_kec = $detail_agen->kec;
					$agen_kel = $detail_agen->kel;
					$product_nama = $detail_product->product_name;
					$product_tipe_layanan = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$product_kode_layanan = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$product_kode_biller = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$product_komisi_basic = $detail_product->komisi_basic;
					$product_komisi_premium = $detail_product->komisi_premium;
					$product_amount = $product_amount;
					$product_margin = $detail_product->margin;
					$product_biaya_admin_edn = $detail_product->admin_edn;
					$product_biaya_admin_1 = $detail_product->admin_1;
					$product_biaya_admin_2 = $detail_product->admin_2;
					$product_biaya_Admin_3 = $detail_product->admin_3;
					$product_potongan = $detail_product->potongan;
					$voucher_nama = $detail_voucher->nama;
					$id_voucher = $detail_voucher->id;
					$voucher_expired_date = $detail_voucher->expired_date;
					$voucher_amount = $detail_voucher->amount;
					$voucher_tagline = $detail_voucher->tagline;
					$voucher_description = $detail_voucher->description;
					$status_jatelindo = 'Pending';
					$kode_paket = $kode_paket;
					$no_ref_sobatku = $oldRefNum;
					$transactionId_sobatku = $transactionId;

					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pln');
					
					if($last_transaksi == 1) {
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					$saveTransPLN = DB::select('exec postTransPlnPrabayar 
						?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
						,array($created_at,$updated_at,$created_user,$updated_user,$trans_no,$ref_trans_no,$trans_date,$trans_desc,$currency,$trans_amount,$komisi,$id_agen,$status,$rekon_amount,$status_match,$no_hp,$no_meter,$id_product,$agen_nama,$agen_email,$agen_kode,$agen_no_hp,$agen_level,$agen_referall,$agen_referall_relation,$agen_status_aktif,$agen_nik,$agen_tgl_register,$agen_tempat_lahir,$agen_tgl_lahir,$agen_jenis_kelamin,$agen_agama,$agen_status_perkawinan,$agen_pekerjaan,$agen_kewarganegaraan,$agen_prov,$agen_kab,$agen_kec,$agen_kel,$product_nama,$product_tipe_layanan,$product_kode_layanan,$product_kode_biller,$product_komisi_basic,$product_komisi_premium,$product_amount,$product_margin,$product_biaya_admin_edn,$product_biaya_admin_1,$product_biaya_admin_2,$product_biaya_Admin_3,$product_potongan,$voucher_nama,$id_voucher,$voucher_expired_date,$voucher_amount,$voucher_tagline,$voucher_description,$status_jatelindo,$no_ref_sobatku,$transactionId_sobatku,$status_match_sobatku,$kode_paket));
					$save = $saveTransPLN[0]->id;
				}	

				$last_transaksi2 = Esta::last_transaksi($id_agen,'trans_pln');
				if($last_transaksi2 == 1) {
					Esta::add_fraud($id_agen);
					$response['api_status']  = 2;
			    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
			    	$response['type_dialog']  = 'Error';
			    	$response['id_transaksi']  = 0;
			    	return $response;
					exit();
				}
			}elseif($type == 'pln_pascabayar'){
				$regid = $requestTransaksi['regid'];
				$token = $requestTransaksi['token'];
				$cek_regid = Esta::cek_regid($id_agen,$regid);

		        $getlog_transaction = DB::table('log_sobatku_payment')
		            ->where('transactionId',$transactionId)
		            ->where('keterangan','Request Payment')
		            ->first();
		        $oldRefNum = $getlog_transaction->ref_number;
		        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

				$no_hp = $requestTransaksi['no_hp'];
				$no_meter = $requestTransaksi['no_meter'];
				//$id_voucher = Request::get('id_voucher');
				$pin = $requestTransaksi['pin'];
				$amount = $requestTransaksi['amount'];
				$id_transaksi = $requestTransaksi['id_transaksi'];

				$tipe_layanan = DB::table('pan_tipe_layanan')->where('kode','00')->first()->id;
				$kode_layanan = DB::table('pan_kode_layanan')->where('kode','4')->first()->id;
				$id_product = DB::table('pan_product')->where('id_tipe_layanan',$tipe_layanan)->where('id_kode_layanan',$kode_layanan)->first()->id;

				$id_voucher_child = $requestTransaksi['id_voucher'];
				$id_voucher = DB::table('trans_voucher_child')
					->where('id',$id_voucher_child)
					->first()->id_voucher;

				$detail_agen = DB::table('agen')
					->where('id',$id_agen)
					->first();

				$detail_product = DB::table('pan_product')
					->where('id',$id_product)
					->first();

				$detail_voucher = DB::table('voucher')
					->where('id',$id_voucher)
					->first();
					
				//UPDATE 23102019
				$detail_inquiry_jat = DB::table('log_jatelindo_bit')
					->where('id',$id_transaksi)
					->first();
				$check_table_trans = DB::table('trans_pln')
					->where('trans_no',$detail_inquiry_jat->bit37)
					->first();
				if(!empty($check_table_trans->id)){
					$kode = $check_table_trans->trans_no;
				}else{
					//$kode = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));
					$kode = $detail_inquiry_jat->bit37;
				}
				//				

				$trans_amount = Esta::amount_product_pln_pascabayar($id_product,$id_agen,$id_voucher,$amount);
				
				/*echo $trans_amount.'-'.($trans_amount <= 0 ? 0 : ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium));

				Esta::log_money($id_agen,$trans_amount,date('Y-m-d H:i:s'),'Transaksi PLN','Transaksi PLN '.$detail_product->product_name,'Out','Transaksi');
				Esta::log_money($id_agen,($trans_amount <= 0 ? 0 : ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium)),date('Y-m-d H:i:s'),'Komisi Transaksi PLN','Komisi Transaksi PLN '.$detail_product->product_name,'In','Komisi');
				exit();*/
				$status_match_sobatku = 'Waiting Rekon';
				try{
					$sv['created_at'] = date('Y-m-d H:i:s');
					$sv['updated_at'] = date('Y-m-d H:i:s');
					$sv['created_user'] = Esta::user($id_agen);
					$sv['updated_user'] = Esta::user($id_agen);
					$sv['trans_no'] = $kode;
					$sv['ref_trans_no'] = $kode;
					$sv['trans_date'] = date('Y-m-d H:i:s');
					$sv['trans_desc'] = 'PLN PASCABAYAR';
					$sv['currency'] = 'IDR';
					$sv['trans_amount'] = $amount+$detail_voucher->amount-($detail_product->margin+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3-$detail_product->potongan);
					$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$sv['create_user'] = $detail_agen->nama;
					$sv['id_agen'] = $id_agen;
					$sv['status'] = 'Error';
					$sv['rekon_amount'] = 0;
					$sv['status_match'] = 'Waiting Rekon';
					$sv['no_hp'] = $no_hp;
					$sv['no_meter'] = $no_meter;
					$sv['id_product'] = $id_product;
					$sv['agen_nama'] = $detail_agen->nama;
					$sv['batch_last_user'] = $detail_agen->nama;
					$sv['agen_email'] = $detail_agen->email;
					$sv['agen_kode'] = $detail_agen->kode;
					$sv['agen_no_hp'] = $detail_agen->no_hp;
					$sv['agen_level'] = $detail_agen->status_agen;
					$sv['agen_referall'] = $detail_agen->kode_referall_agen;
					$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
					$sv['agen_status_aktif'] = $detail_agen->status_aktif;
					$sv['agen_nik'] = $detail_agen->nik;
					$sv['agen_tgl_register'] = $detail_agen->tgl_register;
					$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
					$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
					$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
					$sv['agen_agama'] = $detail_agen->agama;
					$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
					$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
					$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
					$sv['agen_prov'] = $detail_agen->prov;
					$sv['agen_kab'] = $detail_agen->kab;
					$sv['agen_kec'] = $detail_agen->kec;
					$sv['agen_kel'] = $detail_agen->kel;
					$sv['product_nama'] = $detail_product->product_name;
					$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$sv['product_komisi_basic'] = $detail_product->komisi_basic;
					$sv['product_komisi_premium'] = $detail_product->komisi_premium;
					$sv['product_amount'] = $amount+$detail_voucher->amount-($detail_product->margin+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3-$detail_product->potongan);
					$sv['product_margin'] = $detail_product->margin;
					$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
					$sv['product_biaya_admin_1'] = $detail_product->admin_1;
					$sv['product_biaya_admin_2'] = $detail_product->admin_2;
					$sv['product_biaya_admin_3'] = $detail_product->admin_3;
					$sv['product_potongan'] = $detail_product->potongan;
					$sv['voucher_nama'] = $detail_voucher->nama;
					$sv['voucher_expired_date'] = $detail_voucher->expired_date;
					$sv['voucher_amount'] = $detail_voucher->amount;
					$sv['voucher_tagline'] = $detail_voucher->tagline;
					$sv['id_voucher'] = $detail_voucher->id;
					$sv['voucher_description'] = $detail_voucher->description;
					$sv['no_ref_sobatku'] = $oldRefNum;
					$sv['transactionId_sobatku'] = $transactionId;
					$sv['status_match_sobatku'] = $status_match_sobatku;

					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pln');
					
					if($last_transaksi == 1) {
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					$saveTransPLN = DB::select('exec postTransPlnPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								, array_values($sv));
					$save = $saveTransPLN[0]->id;
				} catch(\Exception $e) {
					
					//$kode = Esta::nomor_transaksi('trans_pln',CRUDBooster::getsetting('transaksi_pln'));
					$kode = $detail_inquiry_jat->bit37;
					$sv['created_at'] = date('Y-m-d H:i:s');
					$sv['updated_at'] = date('Y-m-d H:i:s');
					$sv['created_user'] = Esta::user($id_agen);
					$sv['updated_user'] = Esta::user($id_agen);
					$sv['trans_no'] = $kode;
					$sv['ref_trans_no'] = $kode;
					$sv['trans_date'] = date('Y-m-d H:i:s');
					$sv['trans_desc'] = 'PLN PASCABAYAR';
					$sv['currency'] = 'IDR';
					$sv['trans_amount'] = $amount+$detail_voucher->amount-($detail_product->margin+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3-$detail_product->potongan);
					$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$sv['create_user'] = $detail_agen->nama;
					$sv['id_agen'] = $id_agen;
					$sv['status'] = 'Error';
					$sv['rekon_amount'] = 0;
					$sv['status_match'] = 'Waiting Rekon';
					$sv['no_hp'] = $no_hp;
					$sv['no_meter'] = $no_meter;
					$sv['id_product'] = $id_product;
					$sv['agen_nama'] = $detail_agen->nama;
					$sv['batch_last_user'] = $detail_agen->nama;
					$sv['agen_email'] = $detail_agen->email;
					$sv['agen_kode'] = $detail_agen->kode;
					$sv['agen_no_hp'] = $detail_agen->no_hp;
					$sv['agen_level'] = $detail_agen->status_agen;
					$sv['agen_referall'] = $detail_agen->kode_referall_agen;
					$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
					$sv['agen_status_aktif'] = $detail_agen->status_aktif;
					$sv['agen_nik'] = $detail_agen->nik;
					$sv['agen_tgl_register'] = $detail_agen->tgl_register;
					$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
					$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
					$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
					$sv['agen_agama'] = $detail_agen->agama;
					$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
					$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
					$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
					$sv['agen_prov'] = $detail_agen->prov;
					$sv['agen_kab'] = $detail_agen->kab;
					$sv['agen_kec'] = $detail_agen->kec;
					$sv['agen_kel'] = $detail_agen->kel;
					$sv['product_nama'] = $detail_product->product_name;
					$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$sv['product_komisi_basic'] = $detail_product->komisi_basic;
					$sv['product_komisi_premium'] = $detail_product->komisi_premium;
					$sv['product_amount'] = $amount+$detail_voucher->amount-($detail_product->margin+$detail_product->admin_edn+$detail_product->admin_1+$detail_product->admin_2+$detail_product->admin_3-$detail_product->potongan);
					$sv['product_margin'] = $detail_product->margin;
					$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
					$sv['product_biaya_admin_1'] = $detail_product->admin_1;
					$sv['product_biaya_admin_2'] = $detail_product->admin_2;
					$sv['product_biaya_admin_3'] = $detail_product->admin_3;
					$sv['product_potongan'] = $detail_product->potongan;
					$sv['voucher_nama'] = $detail_voucher->nama;
					$sv['voucher_expired_date'] = $detail_voucher->expired_date;
					$sv['voucher_amount'] = $detail_voucher->amount;
					$sv['voucher_tagline'] = $detail_voucher->tagline;
					$sv['id_voucher'] = $detail_voucher->id;
					$sv['voucher_description'] = $detail_voucher->description;
					$sv['no_ref_sobatku'] = $oldRefNum;
					$sv['transactionId_sobatku'] = $transactionId;
					$sv['status_match_sobatku'] = $status_match_sobatku;

					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pln');
					
					if($last_transaksi == 1) {
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					$saveTransPLN = DB::select('exec postTransPlnPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								, array_values($sv));
					$save = $saveTransPLN[0]->id;
				}
			}elseif($type == 'pdam'){
				$regid = $requestTransaksi['regid'];
				$token = $requestTransaksi['token'];
				$cek_regid = Esta::cek_regid($id_agen,$regid);

				$no_hp = $requestTransaksi['no_hp'];
				$id_product = $requestTransaksi['id_area'];
				$id_pelanggan = $requestTransaksi['id_pelanggan'];
				//$id_voucher = Request::get('id_voucher');
				$pin = $requestTransaksi['pin'];
				$amount = $requestTransaksi['amount'];
				$id_transaksi = $requestTransaksi['id_transaksi'];

		        $getlog_transaction = DB::table('log_sobatku_payment')
		            ->where('transactionId',$transactionId)
		            ->where('keterangan','Request Payment')
		            ->first();
		        $oldRefNum = $getlog_transaction->ref_number;
		        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

				$id_voucher_child = $requestTransaksi['id_voucher'];
				$id_voucher = DB::table('trans_voucher_child')
					->where('id',$id_voucher_child)
					->first()->id_voucher;

				$detail_agen = DB::table('agen')
					->where('id',$id_agen)
					->first();

				$detail_product = DB::table('pan_product')
					->where('id',$id_product)
					->first();

				$detail_voucher = DB::table('voucher')
					->where('id',$id_voucher)
					->first();
					
				//UPDATE 23102019
				$detail_inquiry_jat = DB::table('log_jatelindo_bit')
					->where('id',$id_transaksi)
					->first();
				$check_table_trans = DB::table('trans_pdam')
					->where('trans_no',$detail_inquiry_jat->bit37)
					->first();
				if(!empty($check_table_trans->id)){
					$kode = $check_table_trans->trans_no;
				}else{
					//$kode = Esta::nomor_transaksi('trans_pdam',CRUDBooster::getsetting('transaksi_pdam'));
					$kode = $detail_inquiry_jat->bit37;
				}
				//				

				$detail_inquiry = DB::table('log_jatelindo_bit')
					->where('id',$id_transaksi)
					->where('type','PDAM Postpaid')
					->where('status','Inquiry')
					->where('jenis','res')
					->first();

				$trans_amount = Esta::amount_product_trans_pascabayar($id_product,$id_agen,$id_voucher,$amount);
				$komisi = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
				$biaya_admin = $detail_inquiry->pdam_biaya_admin;
				if($trans_amount == 0) {
					$trans_amount = 0;
				} else {
					$trans_amount = $trans_amount+$biaya_admin;
				}
				$trans_amount = $trans_amount-$komisi;

				$status_match_sobatku = 'Waiting Rekon';
				try{
					$sv['created_at'] = date('Y-m-d H:i:s');
					$sv['created_user'] = Esta::user($id_agen);
					$sv['updated_user'] = Esta::user($id_agen);
					$sv['trans_no'] = $kode;
					$sv['ref_trans_no'] = $kode;
					$sv['trans_date'] = date('Y-m-d H:i:s');
					$sv['trans_desc'] = 'PDAM PASCABAYAR';
					$sv['currency'] = 'IDR';
					$sv['trans_amount'] = $amount;
					$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$sv['create_user'] = $detail_agen->nama;
					$sv['id_agen'] = $id_agen;
					$sv['status'] = 'Error';
					$sv['rekon_amount'] = 0;
					$sv['status_match'] = 'Waiting Rekon';
					$sv['no_hp'] = $no_hp;
					$sv['id_pelanggan'] = $id_pelanggan;
					$sv['id_product'] = $id_product;
					$sv['agen_nama'] = $detail_agen->nama;
					$sv['agen_email'] = $detail_agen->email;
					$sv['agen_no_hp'] = $detail_agen->no_hp;
					$sv['agen_kode'] = $detail_agen->kode;
					$sv['agen_level'] = $detail_agen->status_agen;
					$sv['agen_referall'] = $detail_agen->kode_referall_agen;
					$sv['agen_referall_relation'] = $detail_agen->kode_relation_refetrall;
					$sv['agen_status_aktif'] = $detail_agen->status_aktif;
					$sv['agen_nik'] = $detail_agen->nik;
					$sv['agen_tgl_register'] = $detail_agen->tgl_register;
					$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
					$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
					$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
					$sv['agen_agama'] = $detail_agen->agama;
					$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
					$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
					$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
					$sv['agen_prov'] = $detail_agen->prov;
					$sv['agen_kab'] = $detail_agen->kab;
					$sv['agen_kec'] = $detail_agen->kec;
					$sv['agen_kel'] = $detail_agen->kel;
					$sv['product_nama'] = $detail_product->product_name;
					$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$sv['product_komisi_basic'] = $detail_product->komisi_basic;
					$sv['product_komisi_premium'] = $detail_product->komisi_premium;
					$sv['product_amount'] = $amount;
					//$sv['product_amount'] = $trans_amount-$biaya_admin+$komisi;
					$sv['product_margin'] = $detail_product->margin;
					$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
					$sv['product_biaya_admin_1'] = $detail_product->admin_1;
					$sv['product_biaya_admin_2'] = $detail_product->admin_2;
					$sv['product_biaya_Admin_3'] = $detail_product->admin_3;
					$sv['product_potongan'] = $detail_product->potongan;
					$sv['voucher_nama'] = $detail_voucher->nama;
					$sv['voucher_expired_date'] = $detail_voucher->expired_date;
					$sv['voucher_amount'] = $detail_voucher->amount;
					$sv['voucher_tagline'] = $detail_voucher->tagline;
					$sv['voucher_description'] = $detail_voucher->description;
					$sv['id_voucher'] = $detail_voucher->id;
					$sv['no_ref_sobatku'] = $oldRefNum;
					$sv['transactionId_sobatku'] = $transactionId;
					$sv['status_match_sobatku'] = $status_match_sobatku;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pdam');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					$saveTransPdamPascabayar = DB::select('exec postTransPdamPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								, array_values($sv));
					$save = $saveTransPdamPascabayar[0]->id;
				} catch(\Exception $e) {
					//sleep(1);
					//$kode = Esta::nomor_transaksi('trans_pdam',CRUDBooster::getsetting('transaksi_pdam'));
					$kode = $detail_inquiry_jat->bit37;
					$sv['created_at'] = date('Y-m-d H:i:s');
					$sv['created_user'] = Esta::user($id_agen);
					$sv['updated_user'] = Esta::user($id_agen);
					$sv['trans_no'] = $kode;
					$sv['ref_trans_no'] = $kode;
					$sv['trans_date'] = date('Y-m-d H:i:s');
					$sv['trans_desc'] = 'PDAM PASCABAYAR';
					$sv['currency'] = 'IDR';
					$sv['trans_amount'] = $amount;
					$sv['komisi'] = ($detail_agen->status_agen == 'Basic' ? $detail_product->komisi_basic : $detail_product->komisi_premium);
					//$sv['create_user'] = $detail_agen->nama;
					$sv['id_agen'] = $id_agen;
					$sv['status'] = 'Error';
					$sv['rekon_amount'] = 0;
					$sv['status_match'] = 'Waiting Rekon';
					$sv['no_hp'] = $no_hp;
					$sv['id_pelanggan'] = $id_pelanggan;
					$sv['id_product'] = $id_product;
					$sv['agen_nama'] = $detail_agen->nama;
					$sv['agen_email'] = $detail_agen->email;
					$sv['agen_no_hp'] = $detail_agen->no_hp;
					$sv['agen_kode'] = $detail_agen->kode;
					$sv['agen_level'] = $detail_agen->status_agen;
					$sv['agen_referall'] = $detail_agen->kode_referall_agen;
					$sv['agen_referall_relation'] = $detail_agen->kode_relation_referall;
					$sv['agen_status_aktif'] = $detail_agen->status_aktif;
					$sv['agen_nik'] = $detail_agen->nik;
					$sv['agen_tgl_register'] = $detail_agen->tgl_register;
					$sv['agen_tempat_lahir'] = $detail_agen->tempat_lahir;
					$sv['agen_tgl_lahir'] = $detail_agen->tgl_lahir;
					$sv['agen_jenis_kelamin'] = $detail_agen->jenis_kelamin;
					$sv['agen_agama'] = $detail_agen->agama;
					$sv['agen_status_perkawinan'] = $detail_agen->status_perkawinan;
					$sv['agen_pekerjaan'] = $detail_agen->pekerjaan;
					$sv['agen_kewarganegaraan'] = $detail_agen->kewarganegaraan;
					$sv['agen_prov'] = $detail_agen->prov;
					$sv['agen_kab'] = $detail_agen->kab;
					$sv['agen_kec'] = $detail_agen->kec;
					$sv['agen_kel'] = $detail_agen->kel;
					$sv['product_nama'] = $detail_product->product_name;
					$sv['product_tipe_layanan'] = DB::table('pan_tipe_layanan')->where('id',$detail_product->id_tipe_layanan)->first()->nama;
					$sv['product_kode_layanan'] = DB::table('pan_kode_layanan')->where('id',$detail_product->id_kode_layanan)->first()->nama;
					$sv['product_kode_biller'] = DB::table('pan_kode_biller')->where('id',$detail_product->id_kode_biller)->first()->nama;
					$sv['product_komisi_basic'] = $detail_product->komisi_basic;
					$sv['product_komisi_premium'] = $detail_product->komisi_premium;
					$sv['product_amount'] = $amount;
					//$sv['product_amount'] = $trans_amount-$biaya_admin+$komisi;
					$sv['product_margin'] = $detail_product->margin;
					$sv['product_biaya_admin_edn'] = $detail_product->admin_edn;
					$sv['product_biaya_admin_1'] = $detail_product->admin_1;
					$sv['product_biaya_admin_2'] = $detail_product->admin_2;
					$sv['product_biaya_Admin_3'] = $detail_product->admin_3;
					$sv['product_potongan'] = $detail_product->potongan;
					$sv['voucher_nama'] = $detail_voucher->nama;
					$sv['voucher_expired_date'] = $detail_voucher->expired_date;
					$sv['voucher_amount'] = $detail_voucher->amount;
					$sv['voucher_tagline'] = $detail_voucher->tagline;
					$sv['voucher_description'] = $detail_voucher->description;
					$sv['id_voucher'] = $detail_voucher->id;
					$sv['no_ref_sobatku'] = $oldRefNum;
					$sv['transactionId_sobatku'] = $transactionId;
					$sv['status_match_sobatku'] = $status_match_sobatku;

					//sleep(1);
					$last_transaksi = Esta::last_transaksi($id_agen,'trans_pdam');
					//sleep(2);
					if($last_transaksi == 1) {
						
						Esta::add_fraud($id_agen);
						$response['api_status']  = 2;
				    	$response['api_message'] = 'Sedang menyelesaikan transaksi sebelumnya.';
				    	$response['type_dialog']  = 'Error';
				    	$response['id_transaksi']  = 0;
				    	return $response;
						exit();
					}

					$saveTransPdamPascabayar = DB::select('exec postTransPdamPascabayar ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
								, array_values($sv));
					$save = $saveTransPdamPascabayar[0]->id;
				}
			}elseif($type == 'kios'){
				//$agen_id = Request::get('agen_id');
		        $ongkir = $requestTransaksi['ongkir'];
		        $supplier_id = $requestTransaksi['supplier_id'];
		        $supplier_code = $requestTransaksi['supplier_code'];
		        $supplier_name = $requestTransaksi['supplier_name'];
		        $transaction_delivery = $requestTransaksi['transaction_delivery'];
		        $transaction_delivery_address = $requestTransaksi['transaction_delivery_address'];
		        $transaction_total_price = $requestTransaksi['transaction_total_price'];
		        $voucher_id = $requestTransaksi['voucher_id'];
		        $voucher_value = $requestTransaksi['voucher_value'];
		        $droppoint_id = $requestTransaksi['droppoint_id'];
		        $items = $requestTransaksi['items'];
		        $pin = $requestTransaksi['pin'];
		        $transaction_uid = $requestTransaksi['transaction_uid'];
		        
		        //$transactionId = Request::get('transactionId');
		        
		        $getlog_transaction = DB::table('log_sobatku_payment')
		            ->where('transactionId',$transactionId)
		            ->first();

		        $oldRefNum = $getlog_transaction->ref_number;
		        $saldo_sebelum = $getlog_transaction->saldo_sebelum;

		        $detail_agen = DB::table('agen')
		            ->where('id',$id_agen)
		            ->first();
		        
		        foreach ($items as $item) { 
		            $item_id = $item['item_id'];
		            $itemunit_id = $item['itemunit_id'];
		            $stocks = DB::select('exec getStock ?,?,?', array($item_id, $itemunit_id, $id_agen))[0]; 
		            $checkItemMarketplace = DB::select('exec checkItemMarketplaceExists ?,?,?', array($id_agen, $item_id, $itemunit_id))[0];
		            if($stocks->stock == 0 || $stocks->stock == NULL){
		                $response['api_status']  = 2;
		                $response['api_message'] = 'Stock untuk item '.$item['item_name'].' tidak tersedia';
		                $response['type_dialog']  = 'Error';
		                return $response;
		                exit();
		            }else if($stocks->stock < $item['buy_quantity']){
		                $response['api_status']  = 2;
		                $response['api_message'] = 'Pembelian untuk item '.$item['item_name'].' melebihi stock pada marketplace';
		                $response['type_dialog']  = 'Error';
		                return $response;
		                exit();
		            }else if($checkItemMarketplace == NULL && $checkItemMarketplace->is_flashsale == '1'){
		                $response['api_status']  = 2;
		                $response['api_message'] = 'Item '.$item['item_name'].' sudah tidak tersedia di Flashsale';
		                $response['type_dialog']  = 'Error';
		                return $response;
		                exit();
		            }
		        }

		        $current_status = 'NEW PO';

		        $prefix_po = CRUDBooster::getsetting('po_number');
		        $lastIncrement = DB::select('exec getTransactionReceiptLast');
		        $po_number = $prefix_po.sprintf("%06s",substr($lastIncrement[0]->po_number, 6, -4)+1).date('y').date('m');
		        //dd($po_number);
		        $transactioncode = CRUDBooster::getsetting('prefix_transaction_code');
		        $status_match = 'Waiting Rekon';
				$status = 'Pending';

		        $transaction_save_id = DB::select('exec transactionBuyItemNew ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', array($id_agen,$supplier_id,$supplier_code,$supplier_name
		            ,$ongkir,$transaction_delivery,$transaction_delivery_address,$transaction_total_price
		            ,$voucher_id,$voucher_value,$current_status,$prefix_po,$droppoint_id,$transaction_uid,$saldo_sebelum,$oldRefNum,$transactionId,$status_match,$status));
		            
		        
		        
		        foreach ($items as $item) {   
		            $transaction_id = $transaction_save_id[0]->id;      
		            $marketplace_id = $item['marketplace_id'];
		            $item_id = $item['item_id'];
		            $item_esta_code = $item['item_esta_code'];
		            $item_name = $item['item_name'];
		            $item_sku = $item['item_sku'];
		            $item_photo = $item['item_photo'];
		            $buy_quantity = $item['buy_quantity'];
		            $itemunit_id = $item['itemunit_id']; 
		            $itemunit_name = $item['itemunit_name'];
		            $item_sale_price = $item['item_sale_price'];
		            $item_after_discount = $item['item_after_discount'];
		            $discount_type = $item['discount_type'];
		            $discount_value = $item['discount_value'];
		            $margin_type = $item['margin_type'];
		            $margin_value = $item['margin_value'];
		            $item_buy_price = $item['item_buy_price'];
		            $category_id = $item['category_id'];
		            $category_code = $item['category_code'];
		            $category_name = $item['category_name'];
		            $subcategory_id = $item['subcategory_id'];
		            $subcategory_code = $item['subcategory_code'];
		            $subcategory_name = $item['subcategory_name'];
		                             
		            $transaction_detail_save = DB::statement('exec transactionBuyItemDetailNew ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?'
		                , array($transaction_id,$marketplace_id,$item_id,$item_esta_code,$item_name,$item_sku,$item_photo
		                ,$buy_quantity,$itemunit_id,$itemunit_name,$item_sale_price,$item_after_discount,$discount_type,$discount_value,$margin_type,$margin_value
		                ,$item_buy_price,$category_id,$category_code,$category_name,$subcategory_id,$subcategory_code,$subcategory_name));

		        }
		        
		        $data['id'] = $transaction_save_id[0]->id;
			}
		}else{
			$responseBody['responseDescription'] = Esta::responseSobatku($responseBody['responseCode'], $responseBody['responseDescription']);
		}
		$responseBody['endUrlPayment'] = CRUDBooster::getsetting('setting_url_processing_request_sobatku');
		$responseBody['timeoutEndUrlPayment'] = CRUDBooster::getsetting('setting_url_processing_request_sobatku_timeout_dalam_detik');
		curl_close($ch);
		return $this->respondWithDataAndMessage($responseBody, "Success");
	}

	public function paymentResult(){
		$request = '';
		$accountNumber = Request::get('accountNumber');
		$transactionId = Request::get('transactionId');
		$amount = Request::get('amount');
		$responseCode = Request::get('responseCode');
		$responseDescription = Request::get('responseDescription');
		$hashCode = Request::get('hashCode');

		$response = [
			'accountNumber' => $accountNumber,
			'transactionId' => $transactionId,
			'amount' => $amount,
			'responseCode' => $responseCode,
			'responseDescription' => $responseDescription,
			'hashCode' => $hashCode
		];

		$responseJson = json_encode($response);

		$saveResponse = DB::statement('exec CreateLogSobatkuPayment ?,?,?,?,?,?,?,?', array($accountNumber,$transactionId,$amount,'','',$request,$responseJson,'Payment Result'));
		
		$check_log_payment = DB::table('temp_trans_payment')
			->where('is_used',1)
			->where('transactionId',$transactionId)
			->first();
		if(!empty($check_log_payment)){
			return $this->respondWithError("Gagal Transaksi, Transaksi Id sudah digunakan!");
		}
		//dd($response);
		if($response['responseCode'] == '00'){
			sleep(1);
			//try{
				$get_tempTransPayment = DB::table('temp_trans_payment')
					->where('transactionId',$transactionId)
					->first();
				$request_submit = [
					'id_agen' => $get_tempTransPayment->agen_id,
					'request_transaksi' => $get_tempTransPayment->request_transaksi,
					'transactionId' => $transactionId
				];
				$data_toJson = json_encode( $request_submit );
				if($get_tempTransPayment->type == 'bpjs_kesehatan'){
					//$submit_bpjs = BpjsAPI::postBpjsSubmitTrans($get_tempTransPayment->agen_id,$get_tempTransPayment->request_transaksi,$transactionId);					
					$serviceURL = CRUDBooster::getsetting('estakios_url').'api/bpjs-submit-trans';
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response_curl = curl_exec( $ch );
					$response_curl = json_decode( $response_curl,true );
					$response_transaksi = $response_curl;
				}elseif($get_tempTransPayment->type == 'pulsa_prabayar'){
					//$submit_pulsa_prabayar = PulsaAPI::postPulsaPrabayarSubmitTrans($get_tempTransPayment->agen_id,$get_tempTransPayment->request_transaksi,$transactionId);
					$serviceURL = CRUDBooster::getsetting('estakios_url').'api/pulsa-prabayar-submit-trans';
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response_curl = curl_exec( $ch );
					$response_curl = json_decode( $response_curl,true );
					$response_transaksi = $response_curl;
				}elseif($get_tempTransPayment->type == 'pulsa_pascabayar'){
					//$submit_pulsa_pascabayar = PulsaAPI::postPulsaPascabayarSubmitTrans($get_tempTransPayment->agen_id,$get_tempTransPayment->request_transaksi,$transactionId);
					$serviceURL = CRUDBooster::getsetting('estakios_url').'api/pulsa-pascabayar-submit-trans';
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response_curl = curl_exec( $ch );
					$response_curl = json_decode($response_curl , true);
					$response_transaksi = $response_curl;
				}elseif($get_tempTransPayment->type == 'pln_prabayar'){
					//$submit_pln_prabayar = PlnAPI::postPlnPrabayarSubmitTrans($get_tempTransPayment->agen_id,$get_tempTransPayment->request_transaksi,$transactionId);
					$serviceURL = CRUDBooster::getsetting('estakios_url').'api/pln-prabayar-submit-trans';
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response_curl = curl_exec( $ch );
					$response_curl = json_decode($response_curl , true);
					
					$response_transaksi = $response_curl;
				}elseif($get_tempTransPayment->type == 'pln_pascabayar'){
					//$submit_pln_pascabayar = PlnAPI::postPlnPascabayarSubmitTrans($get_tempTransPayment->agen_id,$get_tempTransPayment->request_transaksi,$transactionId);
					$serviceURL = CRUDBooster::getsetting('estakios_url').'api/pln-pascabayar-submit-trans';
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response_curl = curl_exec( $ch );
					$response_curl = json_decode($response_curl , true);
					$response_transaksi = $response_curl;
				}elseif($get_tempTransPayment->type == 'pdam'){
					//$submit_pdam = PdamAPI::postPdamPascabayarSubmitTrans($get_tempTransPayment->agen_id,$get_tempTransPayment->request_transaksi,$transactionId);
					$serviceURL = CRUDBooster::getsetting('estakios_url').'api/pdam-submit-trans';
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response_curl = curl_exec( $ch );
					$response_curl = json_decode($response_curl , true);
					$response_transaksi = $response_curl;
				}elseif($get_tempTransPayment->type == 'kios'){
					//$submit_kios = BuyItemAPI::postBuyItemTrans($get_tempTransPayment->agen_id,$get_tempTransPayment->request_transaksi,$transactionId,$response['responseCode']);
					$request_submit_kios = [
						'id_agen' => $get_tempTransPayment->agen_id,
						'request_transaksi' => $get_tempTransPayment->request_transaksi,
						'transactionId' => $transactionId,
						'response_code' => $response['responseCode']
					];
					$data_toJson_kios = json_encode( $request_submit_kios );
					$serviceURL = CRUDBooster::getsetting('estakios_url').'api/kios-submit-trans';
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson_kios );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response_curl = curl_exec( $ch );
					$response_curl = json_decode($response_curl , true);
					$response_transaksi = $response_curl;
				}else{
					return $this->respondWithDataAndMessage($response, "Success");
				}
				$response['response_transaksi']=$response_transaksi;
			/* }catch(\Exception $e){
				$getAgen = DB::select('EXEC getAgenByNohp ?',array($accountNumber))[0];
				//send notif
				$save_notif['created_at'] = date('Y-m-d H:i:s');
				$save_notif['title'] = 'Payment';
				$save_notif['description'] = 'Terjadi gangguan pada koneksi Internet atau Server. Silahkan coba beberapa saat lagi.';
				$save_notif['description_short'] = 'Payment Gagal';
				$save_notif['image'] = '';
				$save_notif['flag'] = 'Notifikasi';
				$save_notif['id_agen'] = $getAgen->id;
				$save_notif['read'] = 'No';
				//$save_notif['id_header'] = $id;

				$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
				if($get_tempTransPayment->regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$datafcm['type'] = 'Sobatku Gagal';
					$save_notif['regid'] = Esta::sendFCM([$get_tempTransPayment->regid],$datafcm);
				}
				
				DB::table('log_notification_sobatku')->insert($save_notif);
				return $this->respondWithError("Payment Gagal :".$e->getMessage());
			} */
			if($response_transaksi == null){
				$getAgen = DB::select('EXEC getAgenByNohp ?',array($accountNumber))[0];
				//send notif
				$save_notif['created_at'] = date('Y-m-d H:i:s');
				$save_notif['title'] = 'Payment';
				$save_notif['description'] = 'Terjadi gangguan pada koneksi Internet atau Server. Silahkan coba beberapa saat lagi.';
				$save_notif['description_short'] = 'Payment Gagal';
				$save_notif['image'] = '';
				$save_notif['flag'] = 'Notifikasi';
				$save_notif['id_agen'] = $getAgen->id;
				$save_notif['read'] = 'No';
				$save_notif['transactionId'] = $transactionId;
				//$save_notif['id_header'] = $id;

				$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
				if($get_tempTransPayment->regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$datafcm['type'] = 'Sobatku Gagal';
					$save_notif['regid'] = Esta::sendFCM([$get_tempTransPayment->regid],$datafcm);
				}
				
				DB::table('log_notification_sobatku')->insert($save_notif);
				return $this->respondWithError("Terjadi gangguan pada koneksi Internet atau Server. Silahkan coba beberapa saat lagi.");
			}
			$getAgen = DB::select('EXEC getAgenByNohp ?',array($accountNumber))[0];
			//send notif
			$save_notif['created_at'] = date('Y-m-d H:i:s');
			$save_notif['title'] = 'Payment';
			$save_notif['description'] = json_encode($response_transaksi);
			$save_notif['description_short'] = 'Payment Sukses';
			$save_notif['image'] = '';
			$save_notif['flag'] = 'Notifikasi';
			$save_notif['id_agen'] = $getAgen->id;
			$save_notif['read'] = 'No';
			$save_notif['transactionId'] = $transactionId;
			//$save_notif['id_header'] = $id;

			$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
			if($get_tempTransPayment->regid != NULL) {
				$datafcm['title'] = $save_notif['title'];
				$datafcm['content'] = $save_notif['description'];
				$datafcm['type'] = 'Sobatku Sukses';
				$save_notif['regid'] = Esta::sendFCM([$get_tempTransPayment->regid],$datafcm);
			}
			
			DB::table('log_notification_sobatku')->insert($save_notif);
		}else{
			$get_tempTransPayment = DB::table('temp_trans_payment')
				->where('transactionId',$transactionId)
				->first();
			if($get_tempTransPayment->regid != NULL){
				if($get_tempTransPayment->type == 'kios'){
					//$submit_kios = BuyItemAPI::postBuyItemTrans($get_tempTransPayment->agen_id,$get_tempTransPayment->request_transaksi,$transactionId,$response['responseCode']);
					$request_submit_kios = [
						'id_agen' => $get_tempTransPayment->agen_id,
						'request_transaksi' => $get_tempTransPayment->request_transaksi,
						'transactionId' => $transactionId,
						'response_code' => $response['responseCode']
					];
					$data_toJson_kios = json_encode( $request_submit_kios );
					$serviceURL = CRUDBooster::getsetting('estakios_url').'api/kios-submit-trans';
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson_kios );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response_curl = curl_exec( $ch );
					$response_curl = json_decode($response_curl , true);
					$response_transaksi = $response_curl;
				}
				$getAgen = DB::select('EXEC getAgenByNohp ?',array($accountNumber))[0];
				//send notif
				$save_notif['created_at'] = date('Y-m-d H:i:s');
				$save_notif['title'] = 'Payment';
				$save_notif['description'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);
				$save_notif['description_short'] = 'Gagal Payment';
				$save_notif['image'] = '';
				$save_notif['flag'] = 'Notifikasi';
				$save_notif['id_agen'] = $getAgen->id;
				$save_notif['read'] = 'No';
				$save_notif['transactionId'] = $transactionId;
				//$save_notif['id_header'] = $id;

				$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
				if($get_tempTransPayment->regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$datafcm['type'] = 'Sobatku Gagal';
					$save_notif['regid'] = Esta::sendFCM([$get_tempTransPayment->regid],$datafcm);
				}
				DB::table('log_notification_sobatku')->insert($save_notif);
			}else{
				$getAgen = DB::select('EXEC getAgenByNohp ?',array($accountNumber))[0];
				//send notif
				$save_notif['created_at'] = date('Y-m-d H:i:s');
				$save_notif['title'] = 'Payment';
				$save_notif['description'] = Esta::responseSobatku($response['responseCode'], $response['responseDescription']);
				$save_notif['description_short'] = 'Gagal Payment';
				$save_notif['image'] = '';
				$save_notif['flag'] = 'Notifikasi';
				$save_notif['id_agen'] = $getAgen->id;
				$save_notif['read'] = 'No';
				$save_notif['transactionId'] = $transactionId;
				//$save_notif['id_header'] = $id;

				$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
				if($getAgen->regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$datafcm['type'] = 'Sobatku Gagal';
					$save_notif['regid'] = Esta::sendFCM([$getAgen->regid],$datafcm);
				}
				DB::table('log_notification_sobatku')->insert($save_notif);
			}			
		}

		return $this->respondWithDataAndMessage($response, "Success");
	}
	
	public function checkStatusPayment(){
		//sleep(35);
		$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/checkStatus';
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		//$oldRefNum = Request::get('oldRefNum');
		$transactionId = Request::get('transactionId');
		$accountNumber = Request::get('accountNumber');
		$keterangan = 'Check Payment';

		$get_tempTransPayment = DB::table('temp_trans_payment')
				->where('transactionId',$transactionId)
				->first();

		$oldRefNum = $get_tempTransPayment->ref_number;
		$prefixActivation = CRUDBooster::getsetting('prefix_reference_check_payment');
		$referenceNumber = DB::select('exec CreateReferenceCheckPaymentSobatku ?,?', array($prefixActivation, $keterangan));
		$referenceNumber = $referenceNumber[0]->Prefix;
		$request = [
			'oldRefNum' => $oldRefNum,
			'accountNumber' => $accountNumber,
			'referenceNumber' => $referenceNumber,
			'user' => $user,
			'hashCode' => hash('sha256', $oldRefNum.$accountNumber.$referenceNumber.$user.$hashCodeKey)
		];

		$data_toJson = json_encode( $request );
		$saveLog = DB::select('exec CreateLogSobatkuCheckPayment ?,?,?,?,?,?', array($oldRefNum,$accountNumber,$referenceNumber,$data_toJson,'',$keterangan));
		$save_id = $saveLog[0]->id;
		$ch = curl_init( $serviceURL );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 
				'Content-Type: application/json',
				'Accept:application/json'
			)
		);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		
		$response = curl_exec( $ch );
		$responseBody = json_decode( $response, true);
		$uv['response'] = $response;
		$update_response = DB::table('log_sobatku_checkstatus')
						->where('id',$save_id)
						->update($uv);
		curl_close($ch);		

		$get_logNotificationSobatku = DB::table('log_notification_sobatku')
				->where('transactionId',$transactionId)
				->first();
		if($responseBody['responseCode'] == '00'){
			if($get_tempTransPayment->is_used == 0){
				$responseBody['transactionId'] = $get_tempTransPayment->transactionId;
				$requestToPayment = json_encode( $responseBody);
				$serviceURL = CRUDBooster::getsetting('estakios_url').'api/paymentResult';
				$cp = curl_init( $serviceURL );
				curl_setopt( $cp, CURLOPT_CUSTOMREQUEST, "POST" );
				curl_setopt( $cp, CURLOPT_POSTFIELDS, $requestToPayment );
				curl_setopt( $cp, CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $cp, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Accept:application/json'
					)
				);
				$response_curl = curl_exec( $cp );
				$response_curl = json_decode( $response_curl,true );
				$response_transaksi = $response_curl;
				curl_close($cp);
				return $response_transaksi;
			}else{
				$getAgen = DB::select('EXEC getAgenByNohp ?',array($accountNumber))[0];
				//send notif
				$save_notif['created_at'] = date('Y-m-d H:i:s');
				$save_notif['title'] = 'Payment Check';
				$save_notif['description'] = $get_logNotificationSobatku->description;
				$save_notif['description_short'] = $get_logNotificationSobatku->description_short;
				$save_notif['image'] = '';
				$save_notif['flag'] = 'Notifikasi';
				$save_notif['id_agen'] = $getAgen->id;
				$save_notif['read'] = 'No';
				$save_notif['transactionId'] = $transactionId;

				if($getAgen->regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$datafcm['type'] = 'Sobatku Sukses';
					$save_notif['regid'] = Esta::sendFCM([$getAgen->regid],$datafcm);
				}
				DB::table('log_notification_sobatku')->insert($save_notif);
				
				$responseBody['transactionId'] = $get_tempTransPayment->transactionId;
				$responseBody['response_transaksi'] = json_decode($get_logNotificationSobatku->description,true);
				return $this->respondWithDataAndMessage($responseBody, "Success");
			}
		}elseif($responseBody['responseCode'] != '00'){
			if($get_tempTransPayment->regid != NULL){
				if($get_tempTransPayment->type == 'kios' && $get_tempTransPayment->is_used == 0){
					//$submit_kios = BuyItemAPI::postBuyItemTrans($get_tempTransPayment->agen_id,$get_tempTransPayment->request_transaksi,$transactionId,$response['responseCode']);
					$request_submit_kios = [
						'id_agen' => $get_tempTransPayment->agen_id,
						'request_transaksi' => $get_tempTransPayment->request_transaksi,
						'transactionId' => $transactionId,
						'response_code' => $responseBody['responseCode']
					];
					$data_toJson_kios = json_encode( $request_submit_kios );
					$serviceURL = CRUDBooster::getsetting('estakios_url').'api/kios-submit-trans';
					$ch = curl_init( $serviceURL );
					curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson_kios );
					curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
							'Content-Type: application/json',
							'Accept:application/json'
						)
					);
					$response_curl = curl_exec( $ch );
					$response_curl = json_decode($response_curl , true);
					$response_transaksi = $response_curl;
				}
				$getAgen = DB::select('EXEC getAgenByNohp ?',array($accountNumber))[0];
				//send notif
				$save_notif['created_at'] = date('Y-m-d H:i:s');
				$save_notif['title'] = 'Payment Check';
				$save_notif['description'] = Esta::responseSobatku($responseBody['responseCode'], $responseBody['responseDescription']);
				$save_notif['description_short'] = 'Gagal Payment';
				$save_notif['image'] = '';
				$save_notif['flag'] = 'Notifikasi';
				$save_notif['id_agen'] = $getAgen->id;
				$save_notif['read'] = 'No';
				$save_notif['transactionId'] = $transactionId;
				//$save_notif['id_header'] = $id;

				$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
				if($get_tempTransPayment->regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$datafcm['type'] = 'Sobatku Gagal';
					$save_notif['regid'] = Esta::sendFCM([$get_tempTransPayment->regid],$datafcm);
				}
				DB::table('log_notification_sobatku')->insert($save_notif);
			}else{
				$getAgen = DB::select('EXEC getAgenByNohp ?',array($accountNumber))[0];
				//send notif
				$save_notif['created_at'] = date('Y-m-d H:i:s');
				$save_notif['title'] = 'Payment Check';
				$save_notif['description'] = Esta::responseSobatku($responseBody['responseCode'], $responseBody['responseDescription']);
				$save_notif['description_short'] = 'Gagal Payment';
				$save_notif['image'] = '';
				$save_notif['flag'] = 'Notifikasi';
				$save_notif['id_agen'] = $getAgen->id;
				$save_notif['read'] = 'No';
				$save_notif['transactionId'] = $transactionId;
				//$save_notif['id_header'] = $id;

				$detail_agen = DB::table('agen')->where('id',$save_notif['id_agen'])->first();
				if($getAgen->regid != NULL) {
					$datafcm['title'] = $save_notif['title'];
					$datafcm['content'] = $save_notif['description'];
					$datafcm['type'] = 'Sobatku Gagal';
					$save_notif['regid'] = Esta::sendFCM([$getAgen->regid],$datafcm);
				}
				DB::table('log_notification_sobatku')->insert($save_notif);
			}
			
			return $this->respondWithDataAndMessage($responseBody, "Success");
		}
		
		return $this->respondWithDataAndMessage($responseBody, "Success");

		//return $responseBody;
	}

	/*public function checkStatusAccount(){
		$serviceURL = CRUDBooster::getsetting('parent_url_sobatku_api').'/rest/wallet/checkStatusAccount';
		$user = CRUDBooster::getsetting('user_sobatku_api');
		$hashCodeKey = CRUDBooster::getsetting('hashcode_key_sobatku_api');
		$idCardNumber = Request::get('idCardNumber');
		$accountNumber = Request::get('accountNumber');
		$keterangan = 'Check Account';

		$prefixActivation = CRUDBooster::getsetting('prefix_reference_check_account');
		$referenceNumber = DB::select('exec CreateReferenceCheckAccountSobatku ?,?', array($prefixActivation, $keterangan));
		$referenceNumber = $referenceNumber[0]->Prefix;
		$request = [
			'idCardNumber' => $idCardNumber,
			'accountNumber' => $accountNumber,
			'referenceNumber' => $referenceNumber,
			'user' => $user,
			'hashCode' => hash('sha256', $idCardNumber.$accountNumber.$referenceNumber.$user.$hashCodeKey)
		];

		$data_toJson = json_encode( $request );
		$saveLog = DB::select('exec CreateLogSobatkuCheckAccount ?,?,?,?,?,?', array($idCardNumber,$accountNumber,$referenceNumber,$data_toJson,'',$keterangan));
		$save_id = $saveLog[0]->id;
		$ch = curl_init( $serviceURL );
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data_toJson );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept:application/json'
			)
		);
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		$response = curl_exec( $ch );
		$responseBody = json_decode( $response, true);
		$uv['response'] = $response;
		$update_response = DB::table('log_sobatku_checkstatus')
						->where('id',$save_id)
						->update($uv);
		curl_close($ch);

		return $responseBody;
	} */

	
}