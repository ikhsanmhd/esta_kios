<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Response;

class ApiController extends Controller
{
    protected $statusCode = 200;

    public function __construct(){

    }

    public function getStatusCode(){
    	return $this->statusCode;
    }

    public function setStatusCode($statusCode){
    	$this->statusCode = $statusCode;

    	return $this;
    }

    public function respond($data, $headers = []){
    	return Response::json($data, $this->getStatusCode(), $headers);
    }

    public function respondCreated($message, $data = []){
    	return $this->respond([
    		'api_status' => $this->getStatusCode(),
    		'type_dialog' => 'Informasi',
    		'api_message' => $message,
    		'data' => $data
    	]);
    }

    public function respondNotFound($message = 'Not Found!'){
    	return $this->setStatusCode(404)->respond([
    		'api_status' => $this->getStatusCode(),
    		'type_dialog' => 'Error!',
    		'api_message' => $message
    		//'data' => []
    	]);
    }

    public function respondWithDataAndMessage($data = [], $message){
    	return $this->respond([
    		'api_status' => 1,
    		'type_dialog' => 'Informasi',
    		'api_message' => $message,
    		'data' => $data
    	]);
    }

    public function respondWithSuccess($message){
    	return $this->respond([
    		'api_status' => 1,
    		'type_dialog' => 'Informasi',
    		'api_message' => $message
    		//'data' => []
    	]);
    }

    public function respondWithError($message){
    	return $this->respond([
    		'api_status' => 0,
    		'type_dialog' => 'Error!',
    		'api_message' => $message
    		//'data' => []
    	]);
    }
	
	public function validasiAgenAktif(){
        $agen = $agen = Auth::user();
        $checkAgen = $agen->status_aktif;
        if($checkAgen != 'Aktif'){
            $response['api_status']  = 2;
            $response['api_message'] = 'Akun Anda Di Blokir, Silahkan Hubungi Customer Service kami';
            $response['type_dialog']  = 'Error';
            return response()->json($response);
            
        }
        return null;
    }
}
