<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Session;
use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Settings;
use PHPExcel_Style_Border;
use PHPExcel_Style_Alignment;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Style_Fill;
use PHPExcel_Worksheet_PageSetup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Esta;
use Hash;
use Illuminate\Support\Facades\Crypt;
use File;

class SendMail extends ApiController
{    

	public function generateExcelFileByTransactionId($id){
		$data = DB::select('EXEC getTransactionDetail '.$id.'')[0];
		$items = DB::select('EXEC getTransactionItemList '.$id.'');
		$supplier = DB::table('mst_supplier')->where('id',$data->t_supplier_id)->first();
		$row = 1;
		
		//generate excel file
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(26);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(3);


		//Block Header
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(20);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':H'.$row);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row ,"Purchase Order");
		$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(50);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
		$row++;

		//Add Image
		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('test_img');
		$objDrawing->setDescription('test_img');
		$objDrawing->setPath(public_path("").'\\logo_esta_po.png');
		$objDrawing->setCoordinates('A1');                      
		//setOffsetX works properly
		$objDrawing->setOffsetX(10); 
		$objDrawing->setOffsetY(10);                
		//set width, height
		$objDrawing->setWidth(101); 
		$objDrawing->setHeight(47); 
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		//Block PO Info
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row ," ");
		$row++;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,"PO No");
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$row ,": ".$data->t_po_number);
		$row++;

		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,"Tanggal PO");
		if($data->t_transaction_date != null && $data->t_transaction_date != "1970-01-01"){
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row ,": ".date('d M Y', strtotime($data->t_transaction_date)));
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row ,": - ");
		}
		$row++;

		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':C'.$row);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,"Tanggal Request Pengiriman DO");
		if($data->t_transaction_date != null && $data->t_transaction_date != "1970-01-01"){
			if($data->t_supplier_max_arrival_date >= 0)
			{
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row ,": ".date('d M Y', strtotime($data->t_transaction_date.' + '.$data->t_supplier_max_arrival_date.' days'))); 
			}else{
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$row ,": ".date('d M Y', strtotime($data->t_transaction_date))); 
			}
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row ,": - ");
		}
		$row++;


		//Block Header Kepada
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row ," ");
		$row++;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EFEFEF');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':H'.$row);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row ,"Kepada Supplier:");
		$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$row++;

		//Block Kepada
		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,$data->t_supplier_name);
		$row++;

		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$addressSupplier = str_replace("<br/>","\n",$data->t_supplier_address);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,$addressSupplier);
		$numRowsSupp = substr_count($addressSupplier,"\n");
		$splitAddressSupplier = explode("\n", $addressSupplier);
		$additionalSupplierHeight = 0;
		foreach($splitAddressSupplier as $key) {
			$additionalSupplierHeight += (strlen($key)/80)*9;
		}
		$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight($additionalSupplierHeight+15+($numRowsSupp*9));
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setWrapText(true);
		$row++;

		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,"( ".$data->t_supplier_phone." )");
		$row++;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row ," ");

		//Block Header Dikirim
		$row++;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EFEFEF');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$row.':H'.$row);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row ,"Dikirim Ke:");
		$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$row++;

		//Block Dikirim
		$kiosName = "";
		$kiosAddress = "";
		$agenHP = "";
		if($data->t_transaction_delivery == "KIOS")
		{
			$kiosName = $data->t_kios_name;
			$kiosAddress = str_replace("<br/>","\n",$data->t_delivery_address);
			$agenHP = $data->t_agen_no_hp;
		}else{
			$kiosName = $data->t_droppoint_name;
			$kiosAddress = str_replace("<br/>","\n",$data->t_delivery_address);
			$agenHP = $data->t_agen_no_hp;
		}
		
		$splitAddressKios = explode("\n", $kiosAddress);
		$additionalKiosHeight = 0;
		foreach($splitAddressKios as $key) {
			$additionalKiosHeight += (strlen($key)/90)*9;
		}

		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,$kiosName);
		$row++;

		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,$kiosAddress);
		$numRows = substr_count($kiosAddress,"\n");
		$objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight($additionalKiosHeight + 15+($numRows*9));
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setWrapText(true);
		$row++;

		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,"( ".$agenHP." )");
		$row++;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row ," ");
		$row++;

		//Informasi TOP
		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':G'.$row);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,"Terms of Payment (TOP): ".$data->t_supplier_term_of_payment." Hari");
		$row++;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row ," ");
		$row++;

		//Table produk
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row , 'No');
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$row , 'SKU');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$row , 'Nama Item');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$row , 'Qty');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$row , 'Price/Unit');
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$row , 'Total');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':G'.$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$row++;

		$counter = 1;
		$totalAll = 0;
		foreach($items as $item) {
			$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':G'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row , $counter);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row , $item->i_item_sku);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$row , $item->i_item_name);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$row , number_format($item->i_buy_quantity));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$row , "Rp ".number_format($item->i_item_buy_price));
			$totalPerItem =$item->i_buy_quantity * $item->i_item_buy_price;
			$totalAll += $totalPerItem;
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$row , "Rp ".number_format($totalPerItem));
			
			//$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setIndent(1);
			//$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->getAlignment()->setIndent(1);
			//$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setIndent(1);
			//$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setIndent(1);
			//$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->getAlignment()->setIndent(1);
			//$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->getAlignment()->setIndent(1);
			
			$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':G'.$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$row++;
			$counter++;
		}
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row ," ");
		$row++;

		//Catatan + Order
		$objPHPExcel->getActiveSheet()->mergeCells('B'.$row.':D'.($row+1));
		$objPHPExcel->getActiveSheet()->getStyle('A'.$row.':H'.$row)->getFont()->setSize(9);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$row ,"Catatan: ".str_replace("<br/>","\n",$supplier->remark));
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->getAlignment()->setWrapText(true);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':D'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		//$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':D'.($row+1))->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		$objPHPExcel->getActiveSheet()->getStyle('E'.$row.':G'.$row)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->mergeCells('E'.$row.':F'.$row);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('EFEFEF');
		$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$row ,"ORDER TOTAL");
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$row , "Rp ".number_format($totalAll));
		//$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->getAlignment()->setIndent(1);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$row.':G'.$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


		$row++;
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$row ," ");
		$row++;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row ," ");
		
		//BORDER ALL AREA
		$objPHPExcel->getActiveSheet()
			->getStyle('A1:H'.$row)
			->getBorders()
			->getOutline()
			->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		//save excel file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$name = public_path("excels")."\\".$data->t_po_number."_".date('ymd_his').'.xlsx';
		$objWriter->save($name);

		//return excel file
		return $name;
		
	}

	public function generatePDFFileByTransactionId($id){

		// instantiate and use the dompdf class
		$dompdf = new Dompdf();

		$path = public_path("").'\\logo_esta_po.jpg';
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$data = file_get_contents($path);
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

		$data = DB::select('EXEC getTransactionDetail '.$id.'')[0];
		$items = DB::select('EXEC getTransactionItemList '.$id.'');
		$supplier = DB::table('mst_supplier')->where('id',$data->t_supplier_id)->first();

		$tglPO = "- ";
		if($data->t_transaction_date != null && $data->t_transaction_date != "1970-01-01"){
			$tglPO = date('d M Y', strtotime($data->t_transaction_date));
		}

		$tglReqPengiriman = "- ";
		if($data->t_transaction_date != null && $data->t_transaction_date != "1970-01-01"){
			if($data->t_supplier_max_arrival_date >= 0){
				$tglReqPengiriman = date('d M Y', strtotime($data->t_transaction_date.' + '.$data->t_supplier_max_arrival_date.' days' ));
			}else{
				$tglReqPengiriman = date('d M Y', strtotime($data->t_transaction_date));
			}
		}

		$addressSupplier = str_replace("<br/>","\n",$data->t_supplier_address);

		$kiosName = "";
		$kiosAddress = "";
		$agenHP = "";
		if($data->t_transaction_delivery == "KIOS")
		{
			$kiosName = $data->t_kios_name;
			$kiosAddress = str_replace("<br/>","\n",$data->t_delivery_address);
			$agenHP = $data->t_agen_no_hp;
		}else{
			$kiosName = $data->t_droppoint_name;
			$kiosAddress = str_replace("<br/>","\n",$data->t_delivery_address);
			$agenHP = $data->t_agen_no_hp;
		}

		$html = "
		<div style='border: 2px solid black; z-index:1000'>
			<table style='table-layout:fixed;width:100%;border-spacing: 0px;border-collapse: separate;'>
				<tr>
					<td style='width:2%'></td>
					<td style='width:2%'></td>
					<td style='width:15%'></td>
					<td style='width:35%'></td>
					<td style='width:8%'></td>
					<td style='width:18%'></td>
					<td style='width:18%'></td>
					<td style='width:2%'></td>
				</tr>
				<tr>
					<td colspan='8' style='padding:10px 155px 23px 50px;text-align:center;width:100%;font-weight:bolder;font-size:20px; border-bottom: 6px double black;'>
						<img style='float:left' src='".$base64."' width='115px' height='47px'/>
						<div style='padding-top:12px'>Purchase Order</div>
					</td>
				</tr>
				<tr>
					<td colspan='8' style='padding-top:15px;'>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan='2' style='font-weight:bold;font-size:9px'>PO No</td>
					<td style='font-weight:bold;font-size:9px'>: ".$data->t_po_number."</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td colspan='2' style='font-weight:bold;font-size:9px'>Tanggal PO</td>
					<td style='font-weight:bold;font-size:9px'>: ".$tglPO."</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td colspan='2' style='font-weight:bold;font-size:9px'>Tanggal Request Pengiriman DO</td>
					<td style='font-weight:bold;font-size:9px'>: ".$tglReqPengiriman."</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan='8' style='padding-top:15px;'>
					</td>
				</tr>
				<tr>
					<td colspan='8' style='padding:10px;font-weight:bold;font-size:9px; background-color:#dfdfdf; border-top:1px solid black; border-bottom:1px solid black;'>
						Kepada Supplier:
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan='6' style='font-weight:bold;font-size:9px;'>
						".$data->t_supplier_name."
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td colspan='6' style='font-size:9px;'>
						".$addressSupplier."
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td colspan='6' style='font-size:9px;'>
						( ".$data->t_supplier_phone." )
					</td>
					<td></td>
				</tr>
				<tr>
					<td colspan='8' style='padding-top:15px;'>
					</td>
				</tr>
				<tr>
					<td colspan='8' style='padding:10px;font-weight:bold;font-size:9px; background-color:#dfdfdf; border-top:1px solid black; border-bottom:1px solid black;'>
						Dikirim Ke:
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan='6' style='font-weight:bold;font-size:9px;'>
						".$kiosName."
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td colspan='6' style='font-size:9px;'>
						".$kiosAddress."
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td colspan='6' style='font-size:9px;'>
						( ".$agenHP." )
					</td>
					<td></td>
				</tr>
				<tr>
					<td colspan='8' style='padding-top:15px;'>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan='6' style='font-size:9px;'>
						Terms of Payment (TOP): ".$data->t_supplier_term_of_payment." Hari
					</td>
					<td></td>
				</tr>
				<tr>
					<td colspan='8' style='padding-top:15px;'>
					</td>
				</tr>
				<tr>
					<td></td>
					<td style='font-weight:bold;font-size:9px;text-align:center; border:1px solid black; border-right:0.5px solid black'; border-bottom:0.5px solid black'>No</td>
					<td style='font-weight:bold;font-size:9px;text-align:center; border:1px solid black; border-left:0.5px solid black; border-right:0.5px solid black'; border-bottom:0.5px solid black'>SKU</td>
					<td style='font-weight:bold;font-size:9px;text-align:center; border:1px solid black; border-left:0.5px solid black; border-right:0.5px solid black'; border-bottom:0.5px solid black'>Nama Item</td>
					<td style='font-weight:bold;font-size:9px;text-align:center; border:1px solid black; border-left:0.5px solid black; border-right:0.5px solid black'; border-bottom:0.5px solid black'>Qty</td>
					<td style='font-weight:bold;font-size:9px;text-align:center; border:1px solid black; border-left:0.5px solid black; border-right:0.5px solid black'; border-bottom:0.5px solid black'>Price/Unit</td>
					<td style='font-weight:bold;font-size:9px;text-align:center; border:1px solid black; border-left:0.5px solid black'; border-bottom:0.5px solid black'>Total</td>
					<td></td>
				</tr>
				";

				$counter = 1;
				$totalAll = 0;
				foreach($items as $item) {
					$totalPerItem =$item->i_buy_quantity * $item->i_item_buy_price;
					$totalAll += $totalPerItem;
					$html .="
					<tr>
						<td></td>
						<td style='padding:2px 5px; font-size:9px;text-align:center; border:1px solid black; border-top:0.5px solid black; border-right:0.5px solid black'; border-bottom:0.5px solid black'>".$counter."</td>
						<td style='padding:2px 5px; font-size:9px;text-align:left; border:1px solid black; border-top:0.5px solid black; border-left:0.5px solid black; border-right:0.5px solid black'; border-bottom:0.5px solid black'>".$item->i_item_sku."</td>
						<td style='padding:2px 5px; font-size:9px;text-align:left; border:1px solid black; border-top:0.5px solid black; border-left:0.5px solid black; border-right:0.5px solid black'; border-bottom:0.5px solid black'>".$item->i_item_name."</td>
						<td style='padding:2px 5px; font-size:9px;text-align:right; border:1px solid black; border-top:0.5px solid black; border-left:0.5px solid black; border-right:0.5px solid black'; border-bottom:0.5px solid black'>".number_format($item->i_buy_quantity)."</td>
						<td style='padding:2px 5px; font-size:9px;text-align:right; border:1px solid black; border-top:0.5px solid black; border-left:0.5px solid black; border-right:0.5px solid black'; border-bottom:0.5px solid black'>Rp ".number_format($item->i_item_buy_price)."</td>
						<td style='padding:2px 5px; font-size:9px;text-align:right; border:1px solid black; border-top:0.5px solid black; border-left:0.5px solid black'; border-bottom:0.5px solid black'>Rp ".number_format($totalPerItem)."</td>
						<td></td>
					</tr>
					";
					$counter++;
				}

				$html .="
				<tr>
					<td colspan='8' style='padding-top:15px;'>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan='3' rowspan='2' style='padding:2px 5px; font-size:9px; vertical-align:top'>Catatan: ".str_replace("<br/>","\n",$supplier->remark)."</td>
					<td colspan='2' style='padding:2px 0px;text-align:center; font-weight:bold; font-size:9px; background:#dfdfdf;border:1px solid black;border-right:0.5px solid black;'>ORDER TOTAL</td>
					<td style='padding:2px 5px; font-weight:bold; font-size:9px; text-align:right;border:1px solid black;border-left:0.5px solid black;'>Rp ".number_format($totalAll)."</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan='8' style='padding-top:15px;'>
					</td>
				</tr>
			</table>
		</div>
		";

		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();

		$name = $data->t_po_number."_".date('ymd_his').'.pdf';

		// Output the generated PDF to Browser
		$dompdf->stream($name);

	}
	
	public function InfoSupplier($id){
		try{
			$id = decrypt($id);
			$excelPath = $this->generateExcelFileByTransactionId($id);
			$transDetail = DB::select('EXEC getTransactionDetail '.$id.'')[0];
			if($transDetail->t_supplier_email != ""){
				//send email [todo: create template, get email sender, get suplier email, test gmail server]
				$files = [];
				$files[] = $excelPath;
				$data = [
					'no_po' => $transDetail->t_po_number,
					'supplier_name' => $transDetail->t_supplier_name,
					'supplier_address' => $transDetail->t_supplier_address,
					'supplier_phone' => $transDetail->t_supplier_phone,
				];
				//EstaIglo::sendEmail(['to'=>$transDetail->t_supplier_email,'data'=>$data,'template'=>'email_transaksi_info_supplier','attachments'=>$files]);
				Esta::kirimemail(['to'=>$transDetail->t_supplier_email,'data'=>$data,'template'=>'email_transaksi_info_supplier','attachments'=>$files]);
				
				//delete file
				if(File::exists($excelPath)) {
					File::delete($excelPath);
				}
				return "true";
			}else{
				return "false";
			}
		}catch(Exception $e){
			return "false";
		}catch(\Swift_TransportException $e){
			return "false";
		}
	}
	
	public function RefundInfoSupplier($id){
		try{
			$id = decrypt($id);
			$transDetail = DB::select('EXEC getTransactionRefundDetail '.$id.'')[0];
			if($transDetail->r_transaction_supplier_email != ""){
				//send email 
				$files = [];
				$imgs = "";
				$claimPhotos = explode(',',$transDetail->r_claim_photo);
				for($i = 0; $i < count($claimPhotos); $i++){
					$image = explode('~',$claimPhotos[$i]);
					if($image[0] != ""){
						$date = date_create();
						$ts = date_format($date, 'U').rand(1, 1000);
						$imgSrc = str_replace(" ","%20",config('app.esta_ftp').$image[0]);
						$imgDest = public_path("img_temp_email")."\\img_".$transDetail->r_claim_code."_".$ts.".jpg";
						copy($imgSrc,$imgDest);
						$files[$i] = $imgDest;
					}
				}
				
				$data = [
					'no_po' => $transDetail->r_transaction_po_number,
					'supplier_name' => $transDetail->r_transaction_supplier_name,
					'supplier_address' => $transDetail->r_transaction_supplier_address,
					'supplier_phone' => $transDetail->r_transaction_supplier_phone,
					'items' => $items,
					'no_claim' =>$transDetail->r_claim_code,
					'tgl_claim' => date('d M Y', strtotime($transDetail->r_exchange_request_date)),
					'no_do' =>$transDetail->r_transaction_process_to_agen_do_number,
					'tgl_do' =>date('d M Y', strtotime($transDetail->r_transaction_date_do)),
					'no_inv' =>$transDetail->r_transaction_process_to_agen_invoice_number,
					'tgl_inv' =>date('d M Y', strtotime($transDetail->r_transaction_date_invoice)),
					'reason' => $transDetail->r_exchange_reason,
					'img_attachment' => $imgs
				];
				//EstaIglo::sendEmail(['to'=>$transDetail->r_transaction_supplier_email,'data'=>$data,'template'=>'email_refund_info_supplier','attachments'=>$files]);
				Esta::kirimemail(['to'=>$transDetail->r_transaction_supplier_email,'data'=>$data,'template'=>'email_refund_info_supplier','attachments'=>$files]);

				DB::table('txn_transactionrefund')->where('id',$transDetail->r_id)->update(['notify_supplier_date'=>date('Y-m-d H:i:s')]);
				return "true";
			}else{
				return "false";
			}
		}catch(Exception $e){
			return "false";
		}catch(\Swift_TransportException $e){
			return "false";
		}
	}

	public function RefundItemSent($id){
		try{
			$id = decrypt($id);
			$transDetail = DB::select('EXEC getTransactionRefundDetail '.$id.'')[0];
			if($transDetail->r_transaction_supplier_email != ""){
				//send email 
				
				$files = [];
				$imgs = "";
				$lastIdx = 0;
				$claimPhotos = explode(',',$transDetail->r_claim_photo);
				for($idx = 0; $idx < count($claimPhotos); $idx++){
					$image = explode('~',$claimPhotos[$idx]);
					if($image[0] != ""){
						$date = date_create();
						$ts = date_format($date, 'U').rand(1, 1000);
						$imgSrc = str_replace(" ","%20",config('app.esta_ftp').$image[0]);
						$imgDest = public_path("img_temp_email")."\\img_".$transDetail->r_claim_code."_".$ts.".jpg";
						copy($imgSrc,$imgDest);
						$files[$lastIdx] = $imgDest;
						$lastIdx++;
					}
				}

				$deliveryPhotos = explode(',',$transDetail->r_delivery_photo);
				for($idx = 0; $idx < count($deliveryPhotos); $idx++){
					$image = explode('~',$deliveryPhotos[$idx]);
					if($image[0] != ""){
						$date = date_create();
						$ts = date_format($date, 'U').rand(1, 1000);
						$imgSrc = str_replace(" ","%20",config('app.esta_ftp').$image[0]);
						$imgDest = public_path("img_temp_email")."\\img_".$transDetail->r_claim_code."_".$ts.".jpg";
						copy($imgSrc,$imgDest);
						$files[$lastIdx] = $imgDest;
						$lastIdx++;
					}
				}


				$data = [
					'no_po' => $transDetail->r_transaction_po_number,
					'supplier_name' => $transDetail->r_transaction_supplier_name,
					'supplier_address' => $transDetail->r_transaction_supplier_address,
					'supplier_phone' => $transDetail->r_transaction_supplier_phone,
					'items' => $transDetail->r_exchange_items,
					'no_claim' =>$transDetail->r_claim_code,
					'tgl_claim' => date('d M Y', strtotime($transDetail->r_refund_request_date)),
					'no_do' =>$transDetail->r_transaction_process_to_agen_do_number,
					'tgl_do' =>date('d M Y', strtotime($transDetail->r_transaction_date_do)),
					'no_inv' =>$transDetail->r_transaction_process_to_agen_invoice_number,
					'tgl_inv' =>date('d M Y', strtotime($transDetail->r_transaction_date_invoice)),
					'reason' => $transDetail->r_exchange_reason,
					'img_attachment' => $imgs,
					'notes' => $transDetail->r_refund_supplier_info_additional_notes,
					'no_refund' => $transDetail->r_refund_code,
					'tgl_send' => date('d M Y', strtotime($transDetail->r_refund_agen_sent_item_date)),
					'nama_kios' => $transDetail->r_transaction_kios_name,
					'alamat_kios' => $transDetail->r_transaction_kios_address,
					'no_hp' => $transDetail->r_transaction_agen_no_hp
				];
				//EstaIglo::sendEmail(['to'=>$transDetail->r_transaction_supplier_email,'data'=>$data,'template'=>'email_refund_info_supplier_item_sent','attachments'=>$files]);
				Esta::kirimemail(['to'=>$transDetail->r_transaction_supplier_email,'data'=>$data,'template'=>'email_refund_info_supplier_item_sent','attachments'=>$files]);
				return "true";
			}else{
				return "false";
			}
		}catch(Exception $e){
			return "false";
		}catch(\Swift_TransportException $e){
			return "false";
		}
	}
	
	public function ExchangeInfoSupplier($id){
		try{
			$id = decrypt($id);
			$transDetail = DB::select('EXEC getTransactionExchangeDetail '.$id.'')[0];
			if($transDetail->e_transaction_supplier_email != ""){
				//send email 
				$files = [];
				$imgs = "";
				$claimPhotos = explode(',',$transDetail->e_claim_photo);
				for($i = 0; $i < count($claimPhotos); $i++){
					$image = explode('~',$claimPhotos[$i]);
					if($image[0] != ""){
						$date = date_create();
						$ts = date_format($date, 'U').rand(1, 1000);
						$imgSrc = str_replace(" ","%20",config('app.esta_ftp').$image[0]);
						$imgDest = public_path("img_temp_email")."\\img_".$transDetail->e_claim_code."_".$ts.".jpg";
						copy($imgSrc,$imgDest);
						$files[$i] = $imgDest;
					}
				}
				
				$data = [
					'no_po' => $transDetail->e_transaction_po_number,
					'supplier_name' => $transDetail->e_transaction_supplier_name,
					'supplier_address' => $transDetail->e_transaction_supplier_address,
					'supplier_phone' => $transDetail->e_transaction_supplier_phone,
					'items' => $transDetail->e_exchange_items,
					'no_claim' =>$transDetail->e_claim_code,
					'tgl_claim' => date('d M Y', strtotime($transDetail->e_exchange_request_date)),
					'no_do' =>$transDetail->e_transaction_process_to_agen_do_number,
					'tgl_do' =>date('d M Y', strtotime($transDetail->e_transaction_date_do)),
					'no_inv' =>$transDetail->e_transaction_process_to_agen_invoice_number,
					'tgl_inv' =>date('d M Y', strtotime($transDetail->e_transaction_date_invoice)),
					'reason' => $transDetail->e_exchange_reason,
					'img_attachment' => $imgs
				];
				//EstaIglo::sendEmail(['to'=>$transDetail->e_transaction_supplier_email,'data'=>$data,'template'=>'email_exchange_info_supplier','attachments'=>$files]);
				Esta::kirimemail(['to'=>$transDetail->e_transaction_supplier_email,'data'=>$data,'template'=>'email_exchange_info_supplier','attachments'=>$files]);
				
				DB::table('txn_transactionexchange')->where('id',$transDetail->e_id)->update(['notify_supplier_date'=>date('Y-m-d H:i:s')]);
				return "true";
			}else{
				return "false";
			}
		}catch(Exception $e){
			return "false";
		}catch(\Swift_TransportException $e){
			return "false";
		}
	}
	
	public function ExchangeItemSent($id){
		try{
			$id = decrypt($id);
			$transDetail = DB::select('EXEC getTransactionExchangeDetail '.$id.'')[0];
			if($transDetail->e_transaction_supplier_email != ""){
				//send email 
				
				$files = [];
				$imgs = "";
				$lastIdx = 0;
				$claimPhotos = explode(',',$transDetail->e_claim_photo);
				for($idx = 0; $idx < count($claimPhotos); $idx++){
					$image = explode('~',$claimPhotos[$idx]);
					if($image[0] != ""){
						$date = date_create();
						$ts = date_format($date, 'U').rand(1, 1000);
						$imgSrc = str_replace(" ","%20",config('app.esta_ftp').$image[0]);
						$imgDest = public_path("img_temp_email")."\\img_".$transDetail->e_claim_code."_".$ts.".jpg";
						copy($imgSrc,$imgDest);
						$files[$lastIdx] = $imgDest;
						$lastIdx++;
					}
				}

				$deliveryPhotos = explode(',',$transDetail->e_delivery_photo);
				for($idx = 0; $idx < count($deliveryPhotos); $idx++){
					$image = explode('~',$deliveryPhotos[$idx]);
					if($image[0] != ""){
						$date = date_create();
						$ts = date_format($date, 'U').rand(1, 1000);
						$imgSrc = str_replace(" ","%20",config('app.esta_ftp').$image[0]);
						$imgDest = public_path("img_temp_email")."\\img_".$transDetail->e_claim_code."_".$ts.".jpg";
						copy($imgSrc,$imgDest);
						$files[$lastIdx] = $imgDest;
						$lastIdx++;
					}
				}


				$data = [
					'no_po' => $transDetail->e_transaction_po_number,
					'supplier_name' => $transDetail->e_transaction_supplier_name,
					'supplier_address' => $transDetail->e_transaction_supplier_address,
					'supplier_phone' => $transDetail->e_transaction_supplier_phone,
					'items' => $transDetail->e_exchange_items,
					'no_claim' =>$transDetail->e_claim_code,
					'tgl_claim' => date('d M Y', strtotime($transDetail->e_exchange_request_date)),
					'no_do' =>$transDetail->e_transaction_process_to_agen_do_number,
					'tgl_do' =>date('d M Y', strtotime($transDetail->e_transaction_date_do)),
					'no_inv' =>$transDetail->e_transaction_process_to_agen_invoice_number,
					'tgl_inv' =>date('d M Y', strtotime($transDetail->e_transaction_date_invoice)),
					'reason' => $transDetail->e_exchange_reason,
					'img_attachment' => $imgs,
					'notes' => $transDetail->e_exchange_supplier_info_additional_notes,
					'no_exchange' => $transDetail->e_code,
					'tgl_send' => date('d M Y', strtotime($transDetail->e_exchange_agen_sent_item_date)),
					'nama_kios' => $transDetail->e_transaction_kios_name,
					'alamat_kios' => $transDetail->e_transaction_kios_address,
					'no_hp' => $transDetail->e_transaction_agen_no_hp
				];
				//EstaIglo::sendEmail(['to'=>$transDetail->e_transaction_supplier_email,'data'=>$data,'template'=>'email_exchange_info_supplier_item_sent','attachments'=>$files]);
				Esta::kirimemail(['to'=>$transDetail->e_transaction_supplier_email,'data'=>$data,'template'=>'email_exchange_info_supplier_item_sent','attachments'=>$files]);
				return "true";
			}else{
				return "false";
			}
		}catch(Exception $e){
			return "false";
		}catch(\Swift_TransportException $e){
			return "false";
		}
	}
}
