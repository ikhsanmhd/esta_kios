<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Esta;
use DB;
use App\Http\Controllers\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function token() {
    	$token = app('request')->input('token');
    	$token_time = app('request')->input('token_time');

    	$key = 'estakios2018';
		$token_create = md5($key.$token_time);
		if($token == $token_create) {
			$check = DB::table('token')
				->where('token',$token)
				->first();

			if(empty($check)) {
				$post['created_at'] = date('Y-m-d H:i:s');
				$post['token'] = $token;
				$post['token_create'] = $token_create;
				$post['token_time'] = $token_time;

				$save = DB::table('token')
					->insert($post);
			} else {
				exit();
			}
		} else {
			exit();
		}
    }
}
