<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Esta;

class PosAPI extends ApiController
{
    public function postTransactionKios(){
    	$agen = Auth::user();
        $agen_id = $agen->id;

        $transactions = Request::get('transactions');
        //$check_transaction_kios = DB::table('txn_transactionkios')->limit(1)->where('agen_id',$agen_id)->orderBy('id','DESC')->first();
        //dd($transaction['id'][1]);
        foreach ($transactions as $transaction) {
        	$transaction_date = $transaction['transaction_date'];
	        $receipt_number = $transaction['receipt_number'];
	        $total_transaction = $transaction['transaction_total'];
	        $tipe_transaction = $transaction['transaction_type'];
	        $payment = $transaction['payment'];
	        $change = $transaction['change'];
	        $transaction_debit_credit = 'debit';
	        $details = $transaction['details'];

		    $transaction_kios_save = DB::select('exec updateTransactionKios ?,?,?,?,?,?,?,?'
		        ,[$agen_id,$transaction_date,$receipt_number,$total_transaction,$tipe_transaction,$payment,$change,$transaction_debit_credit]);

		    if($transaction_kios_save[0]->id != 0){
			    foreach ($details as $detail) {
			        $transactionkios_id = $transaction_kios_save[0]->id;
			        $sale_quantity = $detail['sale_quantity'];
			        $item_photo = $detail['item_photo'];
			        $sale_price = $detail['sale_price'];
			        $item_name = $detail['item_name'];
			        $item_sku = $detail['item_sku'];
			        $category_id = $detail['category_id'];
			        $category_code = $detail['category_code'];
			        $category_name = $detail['category_name'];
			        $subcategory_id = $detail['subcategory_id'];
			        $subcategory_name = $detail['subcategory_name'];
			        $subcategory_code = $detail['subcategory_code'];
			        $itemunit_id = $detail['itemunit_id'];
			        $itemunit_name = $detail['itemunit_name'];
			        $transaction_kios_detail_save = DB::update('exec updateTransactionDetailKios ?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
			        	[$transactionkios_id,$sale_quantity,$item_photo,$sale_price,$item_name,$item_sku,$category_id,$category_name,$category_code,$subcategory_id,$subcategory_name,$subcategory_code,$itemunit_id,$itemunit_name]);
			    }
			}
		    $data['id'] = $transaction_kios_save[0]->id;
        }    
        return $this->respondWithSuccess("Data Berhasil Di Submit");

    }

    public function historyTransactionPos(){
    	$agen = Auth::user();
        $agen_id = $agen->id;

        if(Request::get('start_date') == NULL && Request::get('end_date') == NULL){
            $start_date = Request::get('start_date');
            $end_date = Request::get('end_date');
        }else{
            $start_date = "'".Request::get('start_date')."'";
            $end_date = "'".Request::get('end_date')."'";
        }
        $receipt_number = Request::get('receipt_number');

        $transactionpos_DB = DB::select('exec getHistoryTransactionPOS ?,?,?,?', [$agen_id,$start_date,$end_date,$receipt_number]);

        $data = [];
        foreach ($transactionpos_DB as $transactionpos) {
        	$data[] = [
        		'id' => $transactionpos->id,
        		'transaction_date' => $transactionpos->transaction_date,
        		'receipt_number' => $transactionpos->receipt_number,
        		'transaction_total' => $transactionpos->transaction_total,
        		'jumlah_produk' => $transactionpos->jumlah_produk,
				'jumlah_barang' => $transactionpos->jumlah_barang
        	];
        }
        return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function historyTransactionPosByReceiptNumber(Request $request, $receipt_number){
    	$transaction_kios_header = DB::select('exec getHistoryTransactionPOSByReceiptNumber ?', [$receipt_number])[0];

    	$transaction_kios_details = DB::select('exec getHistoryTransactionPOSDetailByTransactionId ?', [$transaction_kios_header->id]);
    	$details = [];
    	foreach ($transaction_kios_details as $transaction_kios_detail) {
    		$details[] = [
    			'sale_quantity' => $transaction_kios_detail->sale_quantity,
    			'sale_price' => $transaction_kios_detail->sale_price,
    			'item_name' => $transaction_kios_detail->item_name,
    			'item_sku' => $transaction_kios_detail->item_sku,
    			'category_id' => $transaction_kios_detail->category_id,
    			'category_name' => $transaction_kios_detail->category_name,
    			'category_code' => $transaction_kios_detail->category_code,
    			'subcategory_id' => $transaction_kios_detail->subcategory_id,
    			'subcategory_name' => $transaction_kios_detail->subcategory_name,
    			'subcategory_code' => $transaction_kios_detail->subcategory_code,
    			'itemunit_id' => $transaction_kios_detail->itemunit_id,
    			'itemunit_name' => $transaction_kios_detail->itemunit_name,
    			'item_photo' => $transaction_kios_detail->item_photo
    		];
    	}
    	$data['id'] = $transaction_kios_header->id;
    	$data['transaction_date'] = $transaction_kios_header->transaction_date;
    	$data['receipt_number'] = $transaction_kios_header->receipt_number;
    	$data['transaction_total'] = $transaction_kios_header->transaction_total;
    	$data['payment'] = $transaction_kios_header->payment;
    	$data['change'] = $transaction_kios_header->change;
    	$data['jumlah_produk'] = $transaction_kios_header->jumlah_produk;
		$data['jumlah_barang'] = $transaction_kios_header->jumlah_barang;
    	$data['details'] = $details;

    	return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function inventoryItem(){
    	$agen = Auth::user();
        $agen_id = $agen->id;
    	$query = Request::get('query');
    	$barcode = Request::get('barcode');

    	$get_inventories = DB::select('exec getPosStock ?,?,?', [$agen_id, $query, $barcode]);

    	$data = [];
    	foreach ($get_inventories as $get_inventory) {
			$is_stock_low = false;
    		if($get_inventory->stock <= CRUDBooster::getsetting('setting_produk_akan_kehabisan_stok')){
    			$is_stock_low = true;
    		}
    		$get_bestsellers = DB::select('exec getBestSeller ?', [$agen_id]);
    		$is_bestseller = false;
    		foreach ($get_bestsellers as $get_bestseller) {
    			//$is_bestseller = false;
    			if($get_bestseller->item_sku == $get_inventory->item_sku){
    				$is_bestseller = true;
    			}
    		}
    		$data[] = [
    			'sale_price' => $get_inventory->sale_price,
				'buy_price' => $get_inventory->buy_price,
    			'item_name' => $get_inventory->item_name,
    			'item_sku' => $get_inventory->item_sku,
    			'category_id' => $get_inventory->category_id,
    			'category_name' => $get_inventory->category_name,
    			'category_code' => $get_inventory->category_code,
    			'subcategory_id' => $get_inventory->subcategory_id,
    			'subcategory_name' => $get_inventory->subcategory_name,
    			'subcategory_code' => $get_inventory->subcategory_code,
    			'itemunit_id' => $get_inventory->itemunit_id,
    			'itemunit_name' => $get_inventory->itemunit_name,
    			'stock' => $get_inventory->stock,
    			'last_update' => $get_inventory->last_update,
    			'item_photo' => $get_inventory->item_photo,
    			'is_bestseller' => $is_bestseller,
    			'is_stock_low' => $is_stock_low
    		];
    	}
    	return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function inventoryItemBySku(Request $request, $item_sku){
    	$agen = Auth::user();
        $agen_id = $agen->id;

        $get_inventories = DB::select('exec getPosStock ?,?,?', [$agen_id, '', $item_sku]);

        $data = [];
    	foreach ($get_inventories as $get_inventory) {
    		$is_stock_low = false;
    		if($get_inventory->stock <= CRUDBooster::getsetting('setting_produk_akan_kehabisan_stok')){
    			$is_stock_low = true;
    		}
    		$get_bestsellers = DB::select('exec getBestSeller ?', [$agen_id]);
    		$is_bestseller = false;
    		foreach ($get_bestsellers as $get_bestseller) {
    			$is_bestseller = false;
    			if($get_bestseller->item_sku == $get_inventory->item_sku){
    				$is_bestseller = true;
    			}
    		}
    		$data[] = [
    			'sale_price' => $get_inventory->sale_price,
    			'item_name' => $get_inventory->item_name,
    			'item_sku' => $get_inventory->item_sku,
    			'category_id' => $get_inventory->category_id,
    			'category_name' => $get_inventory->category_name,
    			'category_code' => $get_inventory->category_code,
    			'subcategory_id' => $get_inventory->subcategory_id,
    			'subcategory_name' => $get_inventory->subcategory_name,
    			'subcategory_code' => $get_inventory->subcategory_code,
    			'itemunit_id' => $get_inventory->itemunit_id,
    			'itemunit_name' => $get_inventory->itemunit_name,
    			'stock' => $get_inventory->stock,
    			'last_update' => $get_inventory->last_update,
    			'item_photo' => $get_inventory->item_photo,
    			'is_bestseller' => $is_bestseller,
    			'is_stock_low' => $is_stock_low
    		];
    	}
    	return $this->respondWithDataAndMessage($data, 'Success');
    }

    public function inventoryItemChangePrice(){
    	$agen = Auth::user();
        $agen_id = $agen->id;
        $item_sku = Request::get('item_sku');
        $new_sale_price = Request::get('new_sale_price');
        $old_sale_price = Request::get('old_sale_price');

        $updateItemChangePrice = DB::update('exec updateInventoryItemChangePrice ?,?,?', [$agen_id, $item_sku, $new_sale_price]);
        if($updateItemChangePrice == 0){
            return $this->respondWithError("Data Gagal Di Submit. Silahkan Mencoba kembali");
        }else{                
            return $this->respondWithSuccess("Data Berhasil Di Submit");
        }
    }

    public function postinventoryItem(){
    	$agen = Auth::user();
        $agen_id = $agen->id;
        $inventories = Request::get('inventory');
        $sale_price = Request::get('sale_price');
        $stock = Request::get('stock');

        foreach ($inventories as $inventory) {
        	$item_sku = $inventory['item_sku'];
        	$sale_price = $inventory['sale_price'];
        	$stock = $inventory['stock'];

        	$res = DB::update('exec updatePOSStockSync ?,?,?,?', [$agen_id,$item_sku,$sale_price,$stock]);
        }
        return $this->respondWithSuccess("Data Berhasil Di Submit");
    }
	
	public function reportDaytoDaySales(){
    	$agen = Auth::user();
        $agen_id = $agen->id;

        if(Request::get('start_date') == NULL && Request::get('end_date') == NULL){
            $start_date = Request::get('start_date');
            $end_date = Request::get('end_date');
        }else{
            $start_date = "'".Request::get('start_date')."'";
            $end_date = "'".Request::get('end_date')."'";
        }

        $dayToDaySalesSP = DB::select('exec getReportDayToDaySalesAPI ?,?,?', [$agen_id,$start_date,$end_date]);

        $data = [];
        foreach ($dayToDaySalesSP as $dayToDaySales) {
        	$data[] = [
        		'transaction_date' => $dayToDaySales->date_convert,
        		'jumlah_transaksi' => $dayToDaySales->total_transactions,
        		'total_transaksi' => $dayToDaySales->total_transaction_amount,
				'jumlah_barang' => $dayToDaySales->jumlah_barang
        	];
        }
        return $this->respondWithDataAndMessage($data, 'Success');
    }
	
	public function reportDaytoDaySalesByDate(Request $request, $date){
    	$agen = Auth::user();
        $agen_id = $agen->id;
        
        $dayToDaySalesSP = DB::select('exec getReportDayToDaySalesByDateAPI ?,?', [$agen_id,$date])[0];

		$data['transaction_date'] = $dayToDaySalesSP->transaction_date;
		$data['jumlah_transaksi'] = $dayToDaySalesSP->total_transactions;
		$data['total_transaksi'] = $dayToDaySalesSP->total_transaction_amount;
		$data['jumlah_barang'] = $dayToDaySalesSP->jumlah_barang;
		
		$bestsellerArr = [];
		$bestseller_SP = DB::select('exec getBestSeller ?', [$agen_id]);
		foreach ($bestseller_SP as $bestseller) {
			$bestsellerArr[] = [
				'item_sku' => $bestseller->item_sku,
				'item_photo' => $bestseller->item_photo,
				'item_name' => $bestseller->item_name
			];
		}
		$data['bestseller'] = $bestsellerArr;
        
        $details = [];
		$date = "'".$date."'";
        $transactionpos_DB = DB::select('exec getHistoryTransactionPOS ?,?,?,?', [$agen_id,$date,$date,'']);
        foreach ($transactionpos_DB as $transactionpos) {
        	$details[] = [
        		'id' => $transactionpos->id,
        		'transaction_date' => $transactionpos->transaction_date,
        		'receipt_number' => $transactionpos->receipt_number,
        		'transaction_total' => $transactionpos->transaction_total,
        		'jumlah_produk' => $transactionpos->jumlah_produk,
				'jumlah_barang' => $transactionpos->jumlah_barang
        	];
        }
		$data['details'] = $details;
		return $this->respondWithDataAndMessage($data, 'Success');
    }
	
	public function reportPosProfitLoss(){
    	$agen = Auth::user();
        $agen_id = $agen->id;
            $start_month = Request::get('start_month');
            $end_month = Carbon::parse(Request::get('end_month'))->endOfMonth();
        $reportPosProfitLoss = DB::select('exec agenProfitLossGetOverviewAPI ?,?,?', [$start_month, $end_month, $agen_id]);
		
        $data = [];
        foreach ($reportPosProfitLoss as $profitLoss) {
			$rugi = $profitLoss->total_purchases - $profitLoss->total_sales - $profitLoss->total_adjustment;
        	$laba = $profitLoss->total_sales + $profitLoss->total_adjustment - $profitLoss->total_purchases;
        	$data[] = [
        		'transaction_date' => $profitLoss->year.'-'.$profitLoss->month,
        		'total_pendapatan' => $profitLoss->total_sales,
        		'total_pengeluaran' => $profitLoss->total_purchases,
        		'adjustment' => $profitLoss->total_adjustment,
        		'rugi' => $rugi <= 0 ? '0' : $rugi,
				'laba' => $laba
        	];
        }
        return $this->respondWithDataAndMessage($data, 'Success');
    }
	
	public function inventoryItemChangeStock(){
    	$agen = Auth::user();
        $agen_id = $agen->id;
        $item_sku = Request::get('item_sku');
        $new_stock = Request::get('new_stock');
        $old_stock = Request::get('old_stock');

        $update_posstock = DB::update('exec changePOSStock ?,?,?', [$agen_id, $new_stock,$item_sku]);
        return $this->respondWithSuccess("Data Berhasil Di Submit");
    }
	
	public function reportPosAdjustment(){
    	$agen = Auth::user();
        $agen_id = $agen->id;
        $pos_adjustment = Request::get('pos_adjustment');
        foreach ($pos_adjustment as $adjustment) {
        	$item_sku = $adjustment['item_sku'];
        	$last_stock = $adjustment['last_stock'];
        	$getPosStock = DB::select('exec getPosStock ?,?,?', [$agen_id, '', $item_sku])[0];
        	$transaction_date = Carbon::now()->toDateTimeString();
        	if($getPosStock->stock - $last_stock > 0){
        		$transaction_type = 'adjustment';
        		$transaction_debit_credit = 'credit';
        		$transaction_total = $getPosStock->sale_price * ($getPosStock->stock - $last_stock);
	        	$transaction_kios_save = DB::select('exec updateTransactionKios ?,?,?,?,?,?,?,?'
			        ,[$agen_id,$transaction_date,NULL,$transaction_total,$transaction_type,0,0,$transaction_debit_credit]);
				if($transaction_kios_save[0]->id != 0){
			        $transactionkios_id = $transaction_kios_save[0]->id;
			        $sale_quantity = $getPosStock->stock - $last_stock;
			        $item_photo = $getPosStock->item_photo;
			        $sale_price = $getPosStock->sale_price;
			        $item_name = $getPosStock->item_name;
			        $item_sku = $getPosStock->item_sku;
			        $category_id = $getPosStock->category_id;
			        $category_code = $getPosStock->category_code;
			        $category_name = $getPosStock->category_name;
			        $subcategory_id = $getPosStock->subcategory_id;
			        $subcategory_name = $getPosStock->subcategory_name;
			        $subcategory_code = $getPosStock->subcategory_code;
			        $itemunit_id = $getPosStock->itemunit_id;
			        $itemunit_name = $getPosStock->itemunit_name;
			        $transaction_kios_detail_save = DB::update('exec updateTransactionDetailKiosAdjustment ?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
			        	[$transactionkios_id,$sale_quantity,$item_photo,$sale_price,$item_name,$item_sku,$category_id,$category_name,$category_code,$subcategory_id,$subcategory_name,$subcategory_code,$itemunit_id,$itemunit_name]);
				}
        	}else if($getPosStock->stock - $last_stock < 0){
        		$transaction_type = 'adjustment';
        		$transaction_debit_credit = 'debit';
        		$transaction_total = $getPosStock->sale_price * ($last_stock - $getPosStock->stock);
	        	$transaction_kios_save = DB::select('exec updateTransactionKios ?,?,?,?,?,?,?,?'
			        ,[$agen_id,$transaction_date,NULL,$transaction_total,$transaction_type,0,0,$transaction_debit_credit]);	
				if($transaction_kios_save[0]->id != 0){
			        $transactionkios_id = $transaction_kios_save[0]->id;
			        $sale_quantity = $last_stock - $getPosStock->stock;
			        $item_photo = $getPosStock->item_photo;
			        $sale_price = $getPosStock->sale_price;
			        $item_name = $getPosStock->item_name;
			        $item_sku = $getPosStock->item_sku;
			        $category_id = $getPosStock->category_id;
			        $category_code = $getPosStock->category_code;
			        $category_name = $getPosStock->category_name;
			        $subcategory_id = $getPosStock->subcategory_id;
			        $subcategory_name = $getPosStock->subcategory_name;
			        $subcategory_code = $getPosStock->subcategory_code;
			        $itemunit_id = $getPosStock->itemunit_id;
			        $itemunit_name = $getPosStock->itemunit_name;
			        $transaction_kios_detail_save = DB::update('exec updateTransactionDetailKiosAdjustment ?,?,?,?,?,?,?,?,?,?,?,?,?,?', 
			        	[$transactionkios_id,$sale_quantity,$item_photo,$sale_price,$item_name,$item_sku,$category_id,$category_name,$category_code,$subcategory_id,$subcategory_name,$subcategory_code,$itemunit_id,$itemunit_name]);
				}
        	}
        	//dd($transaction_total);
    		
        	$res = DB::update('exec changePOSStock ?,?,?', [$agen_id, $last_stock, $item_sku]);
        }
        return $this->respondWithSuccess("Data Berhasil Di Submit");
    }
	
	public function inventoryItemDelete(Request $request, $item_sku){
    	$agen = Auth::user();
        $agen_id = $agen->id;

        $deleteInventoryPOS = DB::update('exec deletePOSStock ?,?', [$agen_id, $item_sku]);
        return $this->respondWithSuccess("Data Berhasil Di Delete");
    }
	
	public function reportPosProfitLossByMonth(Request $request, $month){
    	$agen = Auth::user();
        $agen_id = $agen->id;
        $getReportSales = DB::select('exec getReportSalesByMonthAPI ?,?', [$agen_id,$month])[0];
	
		if($getReportSales == NULL){
			$profit['transaction_date'] = Carbon::now()->year.'-'.Carbon::now()->month;
		}else{
			$profit['transaction_date'] = $getReportSales->year.'-'.$getReportSales->month;
			$profit['jumlah_transaksi'] = $getReportSales->total_transactions;
			$profit['total_transaksi'] = $getReportSales->total_transaction_amount;
			$profit['jumlah_barang'] = $getReportSales->jumlah_barang;
		}

        $bestsellerArr = [];
		$bestseller_SP = DB::select('exec getBestSeller ?', [$agen_id]);
		foreach ($bestseller_SP as $bestseller) {
			$bestsellerArr[] = [
				'item_sku' => $bestseller->item_sku,
				'item_photo' => $bestseller->item_photo,
				'item_name' => $bestseller->item_name
			];
		}
		$profit['bestseller'] = $bestsellerArr;

		$details = [];
		$start_date = "'".Carbon::parse($month)->startOfMonth()."'";
		$end_date = "'".Carbon::parse($month)->endOfMonth()."'";
        $transactionpos_DB = DB::select('exec getHistoryTransactionPOS ?,?,?,?', [$agen_id,$start_date,$end_date,'']);
        foreach ($transactionpos_DB as $transactionpos) {
        	$details[] = [
        		'id' => $transactionpos->id,
        		'transaction_date' => $transactionpos->transaction_date,
        		'receipt_number' => $transactionpos->receipt_number,
        		'transaction_total' => $transactionpos->transaction_total,
        		'jumlah_produk' => $transactionpos->jumlah_produk,
        		'jumlah_barang' => $transactionpos->jumlah_barang
        	];
        }
		$profit['details'] = $details;
		$data['profit'] = $profit;

		$getReportPurchases = DB::select('exec getReportPurchasesByMonthAPI ?,?', [$agen_id,$month])[0];
		if($getReportPurchases == NULL){
			$loss['transaction_date'] = Carbon::now()->year.'-'.Carbon::now()->month;
		}else{
			$loss['transaction_date'] = $getReportPurchases->year.'-'.$getReportPurchases->month;
			$loss['jumlah_transaksi'] = $getReportPurchases->transaction_count;
			$loss['total_transaksi']= $getReportPurchases->total_closed;
		}

		$bestpurchasesArr = [];
		$bestpurchases_SP = DB::select('exec getBestPurchases ?', [$agen_id]);
		foreach ($bestpurchases_SP as $bestpurchases) {
			$bestpurchasesArr[] = [
				'item_sku' => $bestpurchases->item_sku,
				'item_photo' => $bestpurchases->item_photo,
				'item_name' => $bestpurchases->item_name
			];
		}
		$loss['bestpurchases'] = $bestpurchasesArr;

		$details = [];
		$detailPurchases = DB::select('exec agenProfitLossGetPurchases ?,?', [$month,$agen_id]);
		foreach ($detailPurchases as $detailPurchase) {
			$details[] = [
				'id' => $detailPurchase->t_id,
				'transaction_date' => $detailPurchase->t_transaction_date,
				'transaction_code' => $detailPurchase->t_transaction_code,
				'total_price' => $detailPurchase->t_transaction_total,
				'transaction_total' => $detailPurchase->t_dtl_total_pesanan,
				'jumlah_produk' => $detailPurchase->t_dtl_jumlah_produk,
				'jumlah_barang' => $detailPurchase->t_dtl_quantity
			];
		}
		$loss['details'] = $details;
		$data['loss'] = $loss;

		return $this->respondWithDataAndMessage($data, 'Success');
    }
}
