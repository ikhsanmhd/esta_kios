<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController as ApiController;

use Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;

class TestMike extends ApiController
{  

	public function testHeader(){
		$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
		if($Android){
			return '{"data":"android"}';
		}else{
			return '{"data":"test-> '.$_SERVER['HTTP_USER_AGENT'].'"}';
		}
	}

	//TEST API UNTUK CMS MARKETPLACE BARU
	public function getCmsMarketplaceNew(){
        //$agen = Auth::user();  //UNTUK PRODUCTION JANGAN LUPA DI UNCOMMENT
        $agen_id = $agen->id;
		
		//ambil langsung dari table mst_cmsCategory. data yang dibutuhkan sudah ada di table mst_cmsCategory
        $getCmsMarketplaceCategories = DB::table('mst_cmsCategory')->whereNull('deleted_at')->get();
        $categorylist = [];
        $categorylistextend = [];
        $i=0;
        foreach ($getCmsMarketplaceCategories as $category) {
			
			//Logo
            $explode_logo = explode("/", $category->photo);
            $last_index = count($explode_logo)-1;
            $category_logoFTP = config('app.esta_ftp').$explode_logo[$last_index];
			
			//Check untuk 8 categori pertama dimasukan ke array categorylist
			if($i < 8){
				$categorylist[] = [
                    'id' => $category->id,
                    'name' => $category->name,
                    'category_logo' => $category_logoFTP
                ];
			}else{
				$categorylistextend[] = [
                    'id' => $category->id,
                    'name' => $category->name,
                    'category_logo' => $category_logoFTP
                ];
			}
            $i++;
        }

		//Get all supplier with result like SP getCMSSupplier
        $getCmsSuppliers = DB::table('mst_cmsSupplier')
							->select('mst_cmsSupplier.supplier_id as supplier_id'
									,'mst_supplier.name as name'
									,'mst_supplier.supplier_logo as supplier_logo'
									,'mst_cmsSupplier.item_id_1 as itemid_slot1'
									,'mst_cmsSupplier.item_id_2 as itemid_slot2'
									,'mst_cmsSupplier.item_id_3 as itemid_slot3'
									,'mst_cmsSupplier.item_id_4 as itemid_slot4'
									,'mst_cmsSupplier.item_id_5 as itemid_slot5'
									,'mst_cmsSupplier.item_id_6 as itemid_slot6'
									,'mst_cmsSupplier.item_id_7 as itemid_slot7'
									,'mst_cmsSupplier.item_id_8 as itemid_slot8'
									)
							->leftJoin('mst_supplier', 'mst_cmsSupplier.supplier_id', '=', 'mst_supplier.id')
							->whereNull('mst_cmsSupplier.deleted_at')
							->get();
        //dd($getCmsSuppliers);
        $supplierlist = [];
        foreach ($getCmsSuppliers as $supplier) {
            $supplierdetails = DB::select('exec getCMSSupplierDetail ?,?', array($agen_id, $supplier->supplier_id));
            $supplierdetailList = [];
            foreach ($supplierdetails as $supplierdetail) {
                $supplierunits = DB::select('getCMSItemDetailById ?,?', array($agen_id, $supplierdetail->item_id));
                $units = [];
                foreach ($supplierunits as $supplierunit) {
                    
                    $units[] = [
                        'marketplace_id' => $supplierunit->id,
                        'item_id' => $supplierunit->item_id,
                        'item_esta_code' => $supplierunit->item_esta_code,
                        'item_sku' => $supplierunit->item_sku,
                        'itemunit_id' => $supplierunit->itemunit_id,
                        'itemunit_name' => $supplierunit->itemunit_name,
                        'discount_value' => $supplierunit->discount_value,
                        'discount_type' => $supplierunit->discount_type,
                        'item_after_discount' => $supplierunit->item_after_discount,
                        'item_sale_price' => $supplierunit->item_sale_price,
                        'stock' => $supplierunit->stock,
                        'stock_converted' => $supplierunit->stock_converted,
                        'margin_type' => $supplierunit->margin_type,
                        'margin_value' => $supplierunit->margin_value,
                        'item_buy_price' => $supplierunit->item_buy_price
                    ];
                }
                
                $supplierDB = DB::select('exec getSupplierById ?', array($supplierdetail->supplier_id));
                $explode_logo = explode("/", $supplierDB[0]->supplier_logo);
                $last_index = count($explode_logo)-1;
                $supplier_logoFTP = config('app.esta_ftp').$explode_logo[$last_index];
                $item_photoFTP = config('app.esta_ftp').$supplierdetail->item_photo;
                $item_desc = DB::select('exec getDescripsiItemHome '.$supplierdetail->item_id.'');
                $supplierdetailList[] = [
                    'category_id' => $supplierdetail->category_id,
                    'category_code' => $supplierdetail->category_code,
                    'category_name' => $supplierdetail->category_name,
                    'subcategory_id' => $supplierdetail->subcategory_id,
                    'subcategory_name' => $supplierdetail->subcategory_name,
                    'subcategory_code' => $supplierdetail->subcategory_code,
                    'item_id' => $supplierdetail->item_id,
                    'item_name' => $supplierdetail->item_name,
                    'item_photo' => $item_photoFTP,
                    'item_detail' => $item_desc[0]->detail,
                    'supplier_id' => $supplierdetail->supplier_id,
                    'supplier_name' => $supplierdetail->supplier_name,
                    'supplier_code' => $supplierdetail->supplier_code,
                    'supplier_logo' => $supplier_logoFTP,
                    'units' => $units
                ];
            }   

            $explode_logo = explode("/", $supplier->supplier_logo);
            $last_index = count($explode_logo)-1;
            $logo_name = $explode_logo[$last_index];
            $supplier_logoFTP = config('app.esta_ftp').$logo_name;
            $supplierlist[] = [
                'id' => $supplier->supplier_id,
                'name' => $supplier->name,
                'supplier_logo' => $supplier_logoFTP,
                'details' => $supplierdetailList
            ];
        }

        $data['category'] = $categorylist;
        $data['extend_category'] = $categorylistextend;
        $data['supplier'] = $supplierlist;
        return $this->respondWithDataAndMessage($data, "Success");
    }
}

?>