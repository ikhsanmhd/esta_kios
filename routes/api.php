<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::group(['middleware'=>['auth:api']->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('authenticate','AuthController@tokenAuth');
Route::post('refresh-token','AuthController@refreshToken');

Route::group(['middleware' => ['auth:api']], function () {

	Route::post('check-agen-kios','RegistrasiAgenPremiumAPI@checkAgenPremium');
	Route::post('details','AuthController@details');
	Route::post('saldo','BuyItemAPI@getSaldo');
	Route::get('registrasi-form','RegistrasiAgenPremiumAPI@getRegistrasiForm');
	
	Route::get('home-marketplace','BuyItemAPI@getCmsMarketplace');
	Route::post('item','BuyItemAPI@getItemByParams');
	Route::get('item/{esta_code9digit}','BuyItemAPI@getItemByEstacode');
	
	Route::post('voucher','BuyItemAPI@postVoucherKios');
	Route::get('checkpoint','BuyItemAPI@getCheckPoint');

	Route::post('buy-item','BuyItemAPI@postBuyItem');
	Route::get('transaction-pending/{uid}','BuyItemAPI@checkTransactionPending');
	Route::get('droppoint','BuyItemAPI@getDropPoint');
	
	Route::post('stock','BuyItemAPI@getStock');
	
	Route::post('history-new-transaction','BuyItemAPI@postHistoryNewTransaction');
	Route::post('history-transaction-finish','BuyItemAPI@postHistoryTransactionFinish');
	Route::post('history-transaction-cancel','BuyItemAPI@postHistoryTransactionCancel');
	Route::post('history-transaction-claim','BuyItemAPI@postHistoryTransactionClaim');
	Route::post('history-transaction-delivery','BuyItemAPI@postHistoryTransactionDelivery');
	
	Route::post('transaction-finish/{id}','BuyItemAPI@postFinishTransaction');
	Route::post('transaction-claim','BuyItemAPI@postTransactionClaim');
	Route::post('transaction-claim-send','BuyItemAPI@postTransactionClaimSend');
	Route::post('transaction-finish-claim/{id}','BuyItemAPI@postFinishClaimTransaction');
	
	Route::post('transaction-pos-sync','PosAPI@postTransactionKios');
	Route::get('history-transaction-pos','PosAPI@historyTransactionPos');
	Route::get('history-transaction-pos/{receipt_number}','PosAPI@historyTransactionPosByReceiptNumber');

	Route::get('inventory-item','PosAPI@inventoryItem');
	Route::get('inventory-item/{item_sku}','PosAPI@inventoryItemBySku');
	Route::post('inventory-item-change-price','PosAPI@inventoryItemChangePrice');
	Route::post('inventory-item-sync','PosAPI@postinventoryItem');
	Route::post('inventory-item-change-stock','PosAPI@inventoryItemChangeStock');
	Route::delete('inventory-item/{item_sku}','PosAPI@inventoryItemDelete');
	
	Route::post('report-pos-day-to-day-sales','PosAPI@reportDaytoDaySales');
	Route::post('report-pos-day-to-day-sales/{date}','PosAPI@reportDaytoDaySalesByDate');
	Route::post('report-pos-profit-loss','PosAPI@reportPosProfitLoss');
	Route::get('report-pos-profit-loss/{month}','PosAPI@reportPosProfitLossByMonth');
	Route::post('report-pos-adjustment','PosAPI@reportPosAdjustment');

	Route::post('setting','RegistrasiAgenPremiumAPI@postSetting');
	
	Route::get('supplier','BuyItemAPI@getAllSupplier');
	Route::get('category','BuyItemAPI@getAllCategory');
	Route::get('bantuan-cs','BuyItemAPI@getBantuanCS');
	Route::get('bantuan-informasi','BuyItemAPI@getBantuanInformasi');

	Route::get('history-transaction/{id}','BuyItemAPI@getTransactionById');

	Route::get('popular-keyword','BuyItemAPI@getSearchQuery');

	//----------------------------------------------------------------------------------------------------------------------------//
	//New API
	Route::post('tarik-tunai-migrasi','EstaAPI@postTarikTunaiMigrasi');
	Route::post('slider','EstaAPI@postSlider');
	Route::post('all-voucher','EstaAPI@postAllVoucher');
	//Route::post('voucher-pdam','EstaAPI@postVoucherPdam');
	Route::post('riwayat-transaksi','EstaAPI@postRiwayatTransaksi');
	Route::post('riwayat-komisi','EstaAPI@postRiwayatKomisi');
	Route::post('kirim-email','EstaAPI@postKirimEmail');
	Route::post('resend-otp','EstaAPI@postResendOtp');
	Route::post('tarik-tunai','EstaAPI@postTarikTunai');
	Route::post('show-premium','EstaAPI@postShowPremium');
	Route::post('submit-premium','EstaAPI@postSubmitPremium');
	Route::post('topup-tutorial','EstaAPI@postTopupTutorial');
	Route::post('topup-tutorial-merchant','EstaAPI@postTopupTutorialMerchant');
	Route::post('update-photo-profile','EstaAPI@postUpdatePhotoProfile');
	Route::post('change-password','EstaAPI@postChangePassword');
	Route::post('edit-profile','EstaAPI@postEditProfile');
	Route::post('change-pass-otp','EstaAPI@postChangePassOtp');
	Route::post('forgot-change-password','EstaAPI@postForgotChangePassword');
	
	Route::post('change-notif-email','EstaAPI@postChangeNotifEmail');
	
	Route::post('notification','EstaAPI@postNotification');
	Route::post('read-notification','EstaAPI@postReadNotification');
	Route::post('infoTarikTunai','RegistrasiEstaAPIv2@infoTarikTunai');
	
	//BPJS
	Route::post('voucher-bpjs','BpjsAPI@postVoucherBpjs');
	Route::post('check-tagihan-bpjs','BpjsAPI@postCheckTagihanBpjs');
	Route::post('bulan-bpjs','BpjsAPI@postBulanBpjs');
	Route::post('bpjs-submit','BpjsAPI@postBpjsSubmit');
	Route::post('bpjs-transaksi-pending','BpjsAPI@postBpjsTransaksiPending');
	Route::post('bpjs-advice-manual','BpjsAPI@postBpjsAdviceManual');

	//PDAM
	Route::post('voucher-pdam','PdamAPI@postVoucherPdam');
	Route::post('check-tagihan-pascabayar-pdam','PdamAPI@postCheckTagihanPascabayarPdam');
	Route::post('pdam-area','PdamAPI@postPdamArea');
	Route::post('pdam-transaksi-pending','PdamAPI@postPdamTransaksiPending');
	Route::post('pdam-pascabayar-submit','PdamAPI@postPdamPascabayarSubmit');

	//PULSA
	Route::post('pulsa-transaksi-pending','PulsaAPI@postPulsaTransaksiPending');
	Route::post('voucher-pulsa','PulsaAPI@postVoucherPulsa');
	Route::post('product-pulsa','PulsaAPI@postProductPulsa');
	Route::post('prefix-operator','PulsaAPI@postPrefixOperator');
	Route::post('check-tagihan-pascabayar-pulsa','PulsaAPI@postCheckTagihanPascabayarPulsa');
	Route::post('pulsa-prabayar-submit','PulsaAPI@postPulsaPrabayarSubmit');
	Route::post('pulsa-pascabayar-submit','PulsaAPI@postPulsaPascabayarSubmit');

	//PLN
	Route::post('pln-produk','PlnAPI@postPlnProduk');
	Route::post('voucher-pln','PlnAPI@postVoucherPln');
	Route::post('check-tagihan-pascabayar-pln','PlnAPI@postCheckTagihanPascabayarPln');
	Route::post('inquiry-prabayar-pln','PlnAPI@postInquiryPrabayarPln');
	Route::post('pln-prepaid-advice-manual','PlnAPI@postPlnPrepaidAdviceManual');
	Route::post('pln-transaksi-advice-manual','PlnAPI@postPlnTransaksiAdviceManual');
	Route::post('pln-prabayar-submit','PlnAPI@postPlnPrabayarSubmit');
	Route::post('pln-pascabayar-submit','PlnAPI@postPlnPascabayarSubmit');
	
	//SALDO SOBATKU
	//Route::post('inquiryBalance','SobatkuAPI@inquiryBalance');
	
	Route::post('requestPayment','SobatkuAPI@requestPayment');
	Route::post('postInfoTarikTunaiTutorialMerchant','EstaAPI@postTutorialTarikTunai');
	Route::post('cancelPayment','EstaAPI@postCancelPayment');
	Route::post('checkStatusPayment','SobatkuAPI@checkStatusPayment');
	Route::post('checkStatusAccount','SobatkuAPI@checkStatusAccount');
	Route::get('getReferralById/{id}','ReferralEstaAPI@getReferralById');
	Route::get('getFaq','ReferralEstaAPI@getFAQ');
	Route::post('postRiwayat','ReferralEstaAPI@postRiwayat');
	Route::post('postListSukuBunga','ReferralEstaAPI@postListSukuBunga');
	Route::post('postListTenor','ReferralEstaAPI@postListTenor');	
	Route::post('postTipeSertifikat','ReferralEstaAPI@postTipeSertifikat');
	Route::post('postJenisSertifikat','ReferralEstaAPI@postJenisSertifikat');
	Route::post('postDokumenPendukung','ReferralEstaAPI@postDokumenPendukung');
	
	//REFERRAL
	//Route::post('getMenuReferral','ReferralEstaAPI@getMenuReferral');
	Route::post('postSubmitMotor','ReferralMotor@postSubmitMotor');
	Route::post('postSubmitMobil','ReferralMobil@postSubmitMobil');
	Route::post('postSubmitSertifikat','ReferralSertifikat@postSubmitSertifikat');
	Route::post('postSubmitDeposito','ReferralDeposito@postSubmitDeposito');	
	Route::post('getEstimasiPembayaran','ReferralEstaAPI@getEstimasiPembayaran');
	Route::post('postHistoryReferral','ReferralEstaAPI@postHistoryReferral');
	
	Route::post('post_ocr','ReferralEstaAPI@postOCR');
	//Route::post('post_custom_ocr','ReferralEstaAPI@postCustomOCR');	
	
	//ALAMAT AGEN
	Route::post('postAddAlamatAgen','EstaAPI@postAddAlamatAgen');
	Route::post('postEditAlamatAgen','EstaAPI@postEditAlamatAgen');
	Route::get('getAlamatAgen','EstaAPI@getAlamatAgen');
	Route::get('getAlamatUtama','EstaAPI@getAlamatUtama');
	Route::post('postAlamatUtama','EstaAPI@postAlamatUtama');

	//INSENTIF
	Route::post('riwayatPendingInsentif','ReferralEstaAPI@riwayatPendingInsentif');
	Route::post('riwayatInsentif','ReferralEstaAPI@riwayatInsentif');
});

Route::group(['middleware' => 'throttle:10,0.2'], function () {
	
	Route::get('quiz','RegistrasiAgenPremiumAPI@getQuiz');
});
Route::post('setting-esta','EstaAPI@postSetting');
Route::post('inquiryBalance','SobatkuAPI@inquiryBalance');
//Route::post('check-agen-kios','RegistrasiAgenPremiumAPI@checkAgenPremium');
Route::get('provinsi','RegistrasiAgenPremiumAPI@getProvinsi');
Route::get('kabupaten/{provinsi_id}','RegistrasiAgenPremiumAPI@getKabupaten');
Route::get('kecamatan/{kabupaten_id}','RegistrasiAgenPremiumAPI@getKecamatan');
Route::get('kelurahan/{kecamatan_id}','RegistrasiAgenPremiumAPI@getKelurahan');
Route::get('location','RegistrasiAgenPremiumAPI@getLocation');

Route::post('submit-pengajuan','RegistrasiAgenPremiumAPI@postRegistrasiForm');
Route::post('logout','EstaAPI@postLogout');

//EMAIL
Route::get('TransactionInfoSupplier/{id}','SendMail@InfoSupplier');
Route::get('RefundInfoSupplier/{id}','SendMail@RefundInfoSupplier');
Route::get('RefundItemSent/{id}','SendMail@RefundItemSent');
Route::get('ExchangeInfoSupplier/{id}','SendMail@ExchangeInfoSupplier');
Route::get('ExchangeItemSent/{id}','SendMail@ExchangeItemSent');
Route::get('esta_images','BuyItemAPI@getEstaImages');
Route::get('RefundInfoSupplierRefund/{id}','SendMail@RefundInfoSupplierRefund');

//SOBATKU
Route::post('inquiryActivation','RegistrasiEstaAPIv2@inquiryActivation');
Route::post('activationResult','SobatkuAPI@activationResult');
//Route::post('register','SobatkuAPI@registrasiSobatku');
Route::post('registrasi','RegistrasiEstaAPIv2@postRegistrasiNew');
Route::post('registrationResult','SobatkuAPI@registrasionResult');
//Route::post('requestPayment','SobatkuAPI@requestPayment');
//Route::post('paymentResult','SobatkuAPI@paymentResultTest');
Route::post('paymentResult','SobatkuAPI@paymentResult');
Route::post('verificationManualResult','RegistrasiEstaAPIv2@verificationManualResult');
Route::post('movePengajuanAgenToAgen','RegistrasiEstaAPIv2@movePengajuanAgenToAgen');
//Route::post('registrasiNew','RegistrasiEstaAPIv2UpdateMike@postRegistrasiNew');

//Route::post('postTopupTutorial','BuyItemAPI@postTopupTutorial');
Route::post('postTopupTutorialMerchant','BuyItemAPI@postTopupTutorialMerchant');

Route::post('notifTest','SobatkuAPI@notifTest');

Route::get('getBank','EstaAPI@getBank');

//Project Update API
Route::post('privacy-policy','EstaAPI@postPrivacyPolicy');
Route::post('login','EstaAPI@postLogin');
Route::post('registrasi-esta','EstaAPI@postRegistrasi');
Route::post('registrasi-otp','EstaAPI@postRegistrasiOtp');

/* Route::post('forgot-pass-otp','EstaAPI@postForgotPassOtp');
Route::post('verifikasi-otp','EstaAPI@postVerifikasiOtp');
Route::post('forgot-change-password','EstaAPI@postForgotChangePassword'); */

//Lupa Password
Route::post('forgot-pass-otp','EstaAPI@postForgotPassOtpNew');
Route::post('verifikasi-otp','EstaAPI@postVerifikasiOtpNew');
Route::post('forgot-change-password','EstaAPI@postForgotChangePasswordNew');

Route::post('post-registrasi-check-hp','EstaAPI@postRegistrasiCheckHp');

Route::post('getDataRegistrasi','RegistrasiEstaAPIv2@getDataRegistrasi');
Route::post('saveFlagRegistrasi','RegistrasiEstaAPIv2@saveFlagRegistrasi');
	
//SUBMIT TRANSACTION SETELAH PROSES PAYMENT
Route::post('bpjs-submit-trans','BpjsAPI@postBpjsSubmit');
Route::post('pulsa-prabayar-submit-trans','PulsaAPI@postPulsaPrabayarSubmit');
Route::post('pulsa-pascabayar-submit-trans','PulsaAPI@postPulsaPascabayarSubmit');
Route::post('pln-prabayar-submit-trans','PlnAPI@postPlnPrabayarSubmit');
Route::post('pln-pascabayar-submit-trans','PlnAPI@postPlnPascabayarSubmit');
Route::post('pdam-submit-trans','PdamAPI@postPdamPascabayarSubmit');
Route::post('kios-submit-trans','BuyItemAPI@postBuyItem');

//Get Esta
Route::get('get-esta','GetEsta@getEsta');
//Check Connection
Route::get('checkConnection','GetEsta@checkConnection');

//REFERRAL
Route::get('getCoBranding','ReferralEstaAPI@getCoBranding');
Route::get('getCoBrandingData/{id_cobranding}','ReferralEstaAPI@getCoBrandingData');
Route::get('getAO/{id_cabang}','ReferralEstaAPI@getCbAo');
Route::get('getCoBrandingByParam/{query}','ReferralEstaAPI@getCoBrandingByParam');
Route::get('getDompetDigital','ReferralEstaAPI@getDompetDigital');
Route::get('getDompetNotes','ReferralEstaAPI@getDompetNotes');

//ALAMAT
Route::get('getProvinsi','ReferralEstaAPI@getProvinsi');
Route::get('getKabupatenByProv/{id}','ReferralEstaAPI@getKabupatenByProv');
Route::get('getKecamatanByKab/{id}','ReferralEstaAPI@getKecamatanByKab');
Route::get('getKodeposByKec/{id}','ReferralEstaAPI@getKodeposByKec');
Route::post('getSyaratKetentuanReferral','ReferralEstaAPI@getSyaratKetentuanReferral');
Route::get('getKelurahanByKec/{id}','ReferralEstaAPI@getKelurahanByKec');

//CONFIG REFERRAL
Route::get('getDataDeposito','ReferralDeposito@getDataDeposito');
Route::get('getDataSertifikat','ReferralSertifikat@getDataSertifikat');
Route::get('getDataMobil','ReferralMobil@getDataMobil');
Route::get('getDataMotor','ReferralMotor@getDataMotor');

//DOMPET LAIN
Route::post('registrasi-dompetlain','RegistrasiEstaAPIv2@postRegistrasiDompetLain');
