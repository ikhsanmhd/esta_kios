<div style="border: 1px solid #DDD; padding: 5px;">
<table class="table table-bordered" style="width: 1000px;">
    <!-- <tr>
      <td colspan="6" style="text-align: center;"><b>BUKTI PEMBAYARAN TAGIHAN TELKOM</b></td>
    </tr> -->
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="width: 150px;">TGL LUNAS</td>
      <td style="width: 10px;">:</td>
      <td>{{$pls_psc_waktu_lunas}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>TIPE PEMBAYARAN</td>
      <td>:</td>
      <td>PULSA PASCABAYAR</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>BUKTI PEMBAYARAN TAGIHAN HALO</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NO PELANGGAN</td>
      <td>:</td>
      <td>{{$pls_psc_no_hp}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NAMA</td>
      <td>:</td>
      <td>{{$nama_pelanggan}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NO REF</td>
      <td>:</td>
      <td>{{$pls_psc_no_ref_switching}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>DETAIL TAGIHAN</td>
      <td>:</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TAGIHAN {{$pls_psc_blth_1}} : {{$pls_psc_jml_tagihan_1}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    @if($pls_psc_total_bill >= 2)
    <tr>
      <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TAGIHAN {{$pls_psc_blth_2}} : {{$pls_psc_jml_tagihan_2}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    @endif
    @if($pls_psc_total_bill >= 3)
    <tr>
      <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TAGIHAN {{$pls_psc_blth_3}} : {{$pls_psc_jml_tagihan_3}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    @endif
    <tr>
      <td>TOTAL TAGIHAN</td>
      <td>:</td>
      <td>{{$total_tagihan}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>BIAYA ADMIN</td>
      <td>:</td>
      <td>{{$biaya_admin}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>TOTAL BAYAR</td>
      <td>:</td>
      <td>{{$total_bayar}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>MOHON STRUK INI DISIMPAN UNTUK BUKTI PEMBAYARAN YANG SAH.</b></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center; font-size: 19px;">Terima Kasih</td>
    </tr>
    <!-- <tr>
      <td colspan="6" style="text-align: center;">{{$info}}</td>
    </tr> -->
</table>
</div>