<div style="border: 1px solid #DDD; padding: 5px;">
<table class="table table-bordered" style="width: 1000px;">
    <!-- <tr>
      <td colspan="6" style="text-align: center;"><b>BUKTI PEMBAYARAN TAGIHAN TELKOM</b></td>
    </tr> -->
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><img src="http://202.83.123.158/estakios/public/logo_esta.png" style="width: 120px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="width: 150px;">TGL LUNAS</td>
      <td style="width: 10px;">:</td>
      <td>{{$tgl_lunas}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>BUKTI PEMBAYARAN TAGIHAN PDAM</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NO REK</td>
      <td>:</td>
      <td>{{$pdam_idpel}}</td>
      <td>RP TAG : {{$tagihan}}</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>BLN/TH</td>
      <td>:</td>
      <td>{{$pdam_blth}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NAMA</td>
      <td>:</td>
      <td>{{$nama_pelanggan}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>TGL LUNAS</td>
      <td>:</td>
      <td>{{$tgl_lunas}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>JPA REF</td>
      <td>:</td>
      <td>{{$pdam_no_ref_biller}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>PDAM menyatakan struk ini sebagai bukti pembayaran yang sah, mohon disimpan.</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>BIAYA ADMIN</td>
      <td>:</td>
      <td>{{$biaya_admin}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>TOTAL BAYAR</td>
      <td>:</td>
      <td>{{$total_bayar}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <!-- <tr>
      <td colspan="6" style="text-align: center;">{{$info}}</td>
    </tr> -->
</table>
</div>