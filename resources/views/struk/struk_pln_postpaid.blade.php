<div style="border: 1px solid #DDD; padding: 5px;">
<table class="table table-bordered" style="width: 1000px;">
    <tr>
      <td colspan="6" style="text-align: left;"><b>BANK MANDIRI</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="width: 140px;">TGL</td>
      <td style="width: 10px;">:</td>
      <td>{{$pln_psc_tgl_lunas}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>TIPE PEMBAYARAN</td>
      <td>:</td>
      <td>PLN PASCABAYAR</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>STRUK PEMBAYARAN TAGIHAN LISTRIK</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>IDPEL</td>
      <td>:</td>
      <td>{{$pln_psc_id_pel}}</td>
      <td>BL/TH:</td>
      <td>:</td>
      <td>{{$pln_psc_blth}}</td>
    </tr>
    <tr>
      <td>NAMA</td>
      <td>:</td>
      <td>{{$pln_psc_nama}}</td>
      <td style="width: 120px;">STAND METER</td>
      <td style="width: 10px;">:</td>
      <td>{{$stand_meter}}</td>
    </tr>
    <tr>
      <td>TARIF/DAYA</td>
      <td>:</td>
      <td>{{$pln_psc_tarif.'/'.$pln_psc_daya.' VA'}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>RP TAG PLN</td>
      <td>:</td>
      <td>{{$rp_transaksi}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NO REF</td>
      <td>:</td>
      <td>{{$pln_psc_no_ref}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>PLN menyatakan struk ini sebagai bukti pembayaran yang sah.</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>ADMIN BANK</td>
      <td>:</td>
      <td>{{$pln_psc_biaya_admin}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>TOTAL BAYAR</td>
      <td>:</td>
      <td>{{$total_bayar}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    @if($pln_psc_jml_tunggakan <= 0)
    <tr>
      <td colspan="6" style="text-align: center;">Terima Kasih</td>
    </tr>
    @else
    <tr>
      <td colspan="6" style="text-align: center;">Anda masih memiliki sisa tunggakan {{$pln_psc_jml_tunggakan}} Bulan</td>
    </tr>
    @endif
    <tr>
      <td colspan="6" style="text-align: center;">{{$info}}</td>
    </tr>
</table>
</div>