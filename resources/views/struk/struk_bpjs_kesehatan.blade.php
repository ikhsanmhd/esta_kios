<div style="border: 1px solid #DDD; padding: 5px;">
<table class="table table-bordered" style="width: 1000px;">
    <!-- <tr>
      <td colspan="6" style="text-align: center;"><b>BUKTI PEMBAYARAN TAGIHAN TELKOM</b></td>
    </tr> -->
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>ESTAKIOS</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="width: 150px;">BANK MANDIRI</td>
      <td style="width: 10px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>STRUK PEMBAYARAN IURAN BPJS KESEHATAN</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NO REF</td>
      <td>:</td>
      <td>{{$bpjsks_jpa_refnum}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="width: 150px;">TANGGAL</td>
      <td style="width: 10px;">:</td>
      <td>{{$bpjsks_tgl_lunas}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NO. VA KELUARGA</td>
      <td>:</td>
      <td>{{$bpjsks_no_va_keluarga}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NO. VA KEPALA KELUARGA</td>
      <td>:</td>
      <td>{{$bpjsks_no_va_kepala_keluarga}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>NAMA PESERTA</td>
      <td>:</td>
      <td>{{$bpjsks_nama}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>JML ANGGOTA KELUARGA</td>
      <td>:</td>
      <td>{{$bpjsks_jml_anggota_keluarga}} Orang</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>PERIODE</td>
      <td>:</td>
      <td>{{$periode}} Bulan</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>JML TAGIHAN</td>
      <td>:</td>
      <td>{{$bpjsks_total_premi}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align: center;"><b>BPJS KESEHATAN MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH</b></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>BIAYA ADMIN</td>
      <td>:</td>
      <td>{{$bpjsks_biaya_admin}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>TOTAL BAYAR</td>
      <td>:</td>
      <td>{{$total_bayar}}</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td style="height: 20px;"></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
   <!--  <tr>
      <td colspan="6" style="text-align: center;">Terima Kasih</td>
    </tr> -->
    <tr>
      <td colspan="6" style="text-align: center;">{{$info}}</td>
    </tr>
</table>
</div>